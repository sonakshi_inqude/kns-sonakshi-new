<?php
/* Database connection information */
$GLOBALS['db_conn_handle'] = NULL;

function filter_array_data($array, $key){
  $temp_array = array();
  $i = 0;
  $key_array = array();

  foreach($array as $val) {
      if (!in_array($val[$key], $key_array)) {
          $key_array[$i] = $val[$key];
          $temp_array[$i] = $val;
      }
      $i++;
  }
  return $temp_array;
}

function create_json_file($project_id,$json)
{
  $directoryName = "./gantchart_json/".date("d-m-Y");
  //Check if the directory already exists.
  if(!is_dir($directoryName)){
    mkdir($directoryName, 0755);
    // $dir = "./gantchart_json" ;
    $filename = $directoryName. '/project_'.$project_id.'.json';
    $myfile = fopen($filename, "w") or die("Unable to open file!");
    fwrite($myfile, json_encode($json,JSON_PARTIAL_OUTPUT_ON_ERROR));
    fclose($myfile);
  }
  else {
    $filename = $directoryName. '/project_'.$project_id.'.json';
    $myfile = fopen($filename, "w") or die("Unable to open file!");
    fwrite($myfile, json_encode($json,JSON_PARTIAL_OUTPUT_ON_ERROR));
    fclose($myfile);
  }
}

function query($query){
  $results = mysqli_query($GLOBALS['db_conn_handle'], $query);
  $rows = array();
  while($row = mysqli_fetch_array($results, MYSQLI_ASSOC)){
      $rows[] = $row;
  }
  return $rows;
}

function get_conn_handle()
{
  $db     = 'kns_erp_live';
	$dbhost = 'localhost';
	$dbuser = 'root';
	$dbpass = '';

	try
	{
    $link = mysqli_connect($dbhost, $dbuser, $dbpass, $db);
    if (!$link) {
        echo "Error: Unable to connect to MySQL." . PHP_EOL;
        echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }
  }
  catch (Exception $e)
	{
		$link = -102;
		$error = $e->getMessage();
	}
  return $link;
}
$GLOBALS['db_conn_handle'] = get_conn_handle();


//Get project list
// Project data

// $time_start = microtime(true);
$query_for_project = 'SELECT * from project_management_project_master where project_master_active =1 and project_management_master_id=27';
$query_for_project_data = query($query_for_project);
$total_work_to_be_completed_for_project = 0;
$wish_dates = array();
for($project_count = 0 ;$project_count<count($query_for_project_data); $project_count++)
{
  //Completion Percentage
  $gantchart_data = array();
  $wish_dates = array();
  $query_for_project_process = 'SELECT * from project_process_list PPL inner join project_process_master PPM on PPM.project_process_master_id=PPL.process_master_id  where `project_id` ='.$query_for_project_data[$project_count]["project_management_master_id"].' order by PPL.process_order ASC' ;
  // $query_for_project_process = 'SELECT * from project_process_list PPL inner join project_process_master PPM on PPM.project_process_master_id=PPL.process_master_id  where `project_id` ='.$query_for_project_data[$project_count]["project_management_master_id"].' and plan_process_id=148 order by PPL.process_order ASC' ;
  $query_for_project_process_data = query($query_for_project_process);

  //Wish Start Date
  $query_for_project_wish_dates = 'SELECT * from project_plan_wish PPW inner join users U on U.user_id=PPW.project_plan_wish_added_by where `project_plan_wish_project_id` ='.$query_for_project_data[$project_count]["project_management_master_id"];
  $query_for_project_wish_dates_data = query($query_for_project_wish_dates);
  for($wish_date_count =0 ; $wish_date_count < count($query_for_project_wish_dates_data) ; $wish_date_count++){
  $wish_dates_data  = array("wish_end_date"=>date('d-m-Y',strtotime($query_for_project_wish_dates_data[$wish_date_count]["project_plan_wish_end_date"])),
                            "created_by"=>$query_for_project_wish_dates_data[$wish_date_count]["user_name"],
                            "created_on"=>date('d-m-Y',strtotime($query_for_project_wish_dates_data[$wish_date_count]["project_plan_wish_added_on"])));
  array_push($wish_dates, $wish_dates_data);
  }
  $process_count = count($query_for_project_process_data);
  $overall_percentage = 0 ;
  $process_planned_start_date = "";
  $process_planned_end_date = "";
  $process_actual_start_date = "";
  $process_actual_end_date = "";
  $actual_end_date_for_process = "";
  $total_work_to_be_completed_for_process = 0;
  $total_process_percentage = 0;
  for($pcount=0 ; $pcount < count($query_for_project_process_data) ; $pcount++)
  {
    $query_for_project_process_task = 'SELECT project_task_master_name, project_task_master_id, project_process_task_id,project_process_task_location_id from project_plan_process_task PPPT inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type where project_process_id ='.$query_for_project_process_data[$pcount]["plan_process_id"].' and  project_process_task_active=1 order by PTM.project_task_master_order ASC';
    // $query_for_project_process_task = 'SELECT project_task_master_name,project_task_master_id,project_process_task_id,project_process_task_location_id from project_plan_process_task PPPT inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type where project_process_id = 148 and  project_process_task_active=1 and project_process_task_id=1593';
    $query_for_project_process_task_data = query($query_for_project_process_task);

    $task_count = count($query_for_project_process_task_data);
    // $total_avg_percentage = 0 ;
    $task_planned_start_date = "";
    $task_planned_end_date = "";
    $task_actual_start_date = "";
    $task_actual_end_date = "";
    $actual_end_date_for_task = "";
    $total_work_to_be_completed_for_task = 0;
    $total_percentage_completed_for_task = 0 ;
    $process_uniq_id = $query_for_project_process_data[$pcount]["plan_process_id"].uniqid() ;
    for($tcount=0 ; $tcount < count($query_for_project_process_task_data) ; $tcount++)
    {
      $task_name = array();
      $query_for_project_process_task_road = 'SELECT project_task_planning_task_id,project_task_planning_no_of_roads,project_site_location_mapping_master_name from task_planning PPPT where project_task_planning_task_id ='.$query_for_project_process_task_data[$tcount]["project_process_task_id"].' and  project_task_planning_active=1 order by project_site_location_mapping_master_order ASC';
      // $query_for_project_process_task_road = 'SELECT project_task_planning_task_id,project_task_planning_no_of_roads,project_site_location_mapping_master_name from task_planning PPPT where project_task_planning_task_id =1593 and  project_task_planning_active=1 order by project_site_location_mapping_master_order ASC';
      $query_for_project_process_task_road_data = query($query_for_project_process_task_road);
      $road_actual_start_date = "";
      $road_actual_end_date = "";
      $road_planned_start_date = "";
      $road_planned_end_date = "";
      $completion_percentage_data = get_percentage($query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_dates_data = get_dates_for_task_road_level($query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_budget_amount = get_budget_amount(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_rw_budget_amount = get_rework_budget_amount(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_measurement_data = get_actaul_measurement(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_planned_measurement = get_planned_measurement(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_planned_budget_amount = get_planned_budget_amount(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_dates = get_planned_date_changed_history(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],0);
      $task_planned_start_date = $task_dates_data["planned_start_date"];
      $task_planned_end_date = $task_dates_data["planned_end_date"];
      $task_actual_start_date = $task_dates_data["actual_start_date"];
      $task_actual_end_date = $task_dates_data["actual_end_date"];
      $task_regular_msmrt = $task_measurement_data["total_regular_msmrt"];
      $task_rework_msmrt = $task_measurement_data["total_rework_msmrt"];
      $task_name[$tcount] = $query_for_project_process_task_data[$tcount]["project_task_master_name"];

      if(($task_actual_start_date == "30-11-2099 12:00" && $task_actual_end_date == "30-11--0001 12:00") || ($task_actual_end_date == "01-01-1970 01:00"))
      {
        $task_unscheduled = true;
      }
      else {
        $task_unscheduled = false ;
      }
      $total_work_to_be_completed_for_road = 0;
      $total_road_completion_percentage = 0;
      for($rcount=0 ; $rcount < count($query_for_project_process_task_road_data) ; $rcount++)
      {
          $task_id = $query_for_project_process_task_road_data[$rcount]['project_task_planning_task_id'];
          $road_id = $query_for_project_process_task_road_data[$rcount]['project_task_planning_no_of_roads'];
          $query_for_project_process_task_road_shadow = "SELECT * from project_task_planning_shadow PTPS inner join users U on U.user_id=PTPS.project_task_planning_shadow_changed_by where project_task_planning_shadow_task_id =".$task_id." and project_task_planning_shadow_road_id='".$road_id."'";
          $query_for_project_process_task_road_shadow_data = query($query_for_project_process_task_road_shadow);
          $road_shadow_data= array();
          $road_dates = "";
          $road_end_date = "";
          for($r=0 ; $r < count($query_for_project_process_task_road_shadow_data) ; $r++)
          {
              $road_dates = array('old_start_date'=> date("d-m-Y h:i", strtotime($query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_old_start_date"])),
                                'old_end_date'=> date("d-m-Y h:i", strtotime($query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_old_end_date"])),
                                'start_date' => date("d-m-Y h:i", strtotime($query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_new_start_date"])),
                                'end_date'=>date("d-m-Y h:i", strtotime($query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_new_end_date"])),
                                'changed_by'=>$query_for_project_process_task_road_shadow_data[$r]["user_name"],
                                'changed_on'=>date("d-m-Y h:i", strtotime($query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_changed_on"])));
              $road_end_date =  $query_for_project_process_task_road_shadow_data[$r]["project_task_planning_shadow_new_end_date"];
            array_push($road_shadow_data,$road_dates);
          }

          $time_array = explode(" ",microtime());
          $time_microseconds = ($time_array[0] * 100000000);
          $time_unix_epoch   = $time_array[1];
          $roadIdStartIndex = $time_unix_epoch.$time_microseconds;
          $completion_road_percentage_data = get_percentage($query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"]);
          $task_dates_data = get_dates_for_task_road_level($query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"]);
          $task_road_budget_amount = get_budget_amount(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"]);
          $task_road_budget_rw_amount = get_rework_budget_amount(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"]);
          $task_road_msmrt = get_actaul_measurement(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"]);
          $task_road_planned_budget_amount = get_planned_budget_amount(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"]);
          $task_road_planned_measurement = get_planned_measurement(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"]);
          $task_road_planned_start_date = $task_dates_data["planned_start_date"];
          $task_road_planned_end_date = $task_dates_data["planned_end_date"];
          $task_road_actual_start_date = $task_dates_data["actual_start_date"];
          $task_road_actual_end_date = $task_dates_data["actual_end_date"];
          $manpower_road_percentage = $completion_road_percentage_data["manpower_percenatage"];
          $machine_road_percentage = $completion_road_percentage_data["machine_percentage"];
          $contract_road_percentage = $completion_road_percentage_data["contract_percentage"];
          $road_level_avg_count = $completion_road_percentage_data["avg_count"];
          $road_regular_msmrt = $task_road_msmrt["total_regular_msmrt"];
          $road_rework_msmrt = $task_road_msmrt["total_rework_msmrt"];
          if(($road_regular_msmrt != 0) && ($task_road_planned_measurement!=0))
          {
            // $total_road_percentage = ($manpower_road_percentage + $machine_road_percentage + $contract_road_percentage)/$road_level_avg_count ;
            $total_road_percentage = $road_regular_msmrt / $task_road_planned_measurement;
          }
          else {
            $total_road_percentage = 0 ;
          }
          if($query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"] == "No Roads")
          {
            $road_name = "No Roads";
            $road_id = $roadIdStartIndex + $rcount ;
          }
          else {
            $road_name = $query_for_project_process_task_road_data[$rcount]["project_site_location_mapping_master_name"] ;
            $road_id =  $roadIdStartIndex;
          }
          if(($task_road_actual_start_date == "30-11-2099 12:00" && $task_road_actual_end_date == "30-11--0001 12:00") || ($task_road_actual_start_date == "30-11-2099 12:00" && $task_road_actual_end_date == "01-01-1970 01:00"))
          {
            $road_unscheduled = true;
          }
          else {
            $road_unscheduled = false ;
          }
          if(round($total_road_percentage) == 1)
          {
              $actual_end_date_for_road = $task_road_actual_end_date ;
          }
          else if($task_road_actual_start_date != "30-11-2099 12:00" && $task_road_actual_end_date="30-11--0001 12:00") {
              $actual_end_date_for_road = date("d-m-Y H:i:s") ;
          }
          else {
            $actual_end_date_for_road = "30-11--0001 12:00" ;
          }
          //Work supoose to be completed caluclualtion
          if($task_road_planned_budget_amount != 0 && $task_road_planned_measurement != 0)
          {
            $amount_per_msmrt = ($task_road_planned_budget_amount/$task_road_planned_measurement);
          }
          else {
            $amount_per_msmrt = 0;
          }
          if($amount_per_msmrt !=0 && $task_road_budget_amount !=0)
          {
            // echo $task_road_budget_amount.'/'.$amount_per_msmrt.', ';
            $work_to_be_completed = $task_road_budget_amount/$amount_per_msmrt ;
          }
          else {
            $work_to_be_completed = 0;
          }
          $total_work_to_be_completed_for_road += $work_to_be_completed;
          $total_road_completion_percentage += $total_road_percentage;
          $road_pause_days = get_pause_days(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],$query_for_project_process_task_road_data[$rcount]["project_task_planning_no_of_roads"],$task_road_planned_start_date,$road_end_date,$actual_end_date_for_road);
          $road_data = array(
            "start_date"=>$task_road_actual_start_date,
            "end_date"=>$actual_end_date_for_road,
            "planned_start_date"=>$task_road_planned_start_date,
            "planned_end_date"=>$task_road_planned_end_date,
            "parent"=>($query_for_project_process_task_data[$tcount]["project_task_master_id"]*333333),
            "type"=>'task',
            "progress"=>$total_road_percentage,
            "text"=>$road_name,
            "id"=>$road_id,
            "unscheduled"=>$road_unscheduled,
            "planned_budget_amount"=>round($task_road_planned_budget_amount),
            "budget_amount"=>round($task_road_budget_amount),
            "rw_budget_amount"=>$task_road_budget_rw_amount,
            "reg_msmrt"=>$road_regular_msmrt,
            "rw_msmrt"=>$road_rework_msmrt,
            "road_dates"=>$road_shadow_data,
            "planned_msmrt"=>$task_road_planned_measurement,
            "project_id"=>$query_for_project_data[$project_count]["project_management_master_id"],
            "work_to_be_completed"=>round($work_to_be_completed),
            "process_master_id"=>$query_for_project_process_data[$pcount]["project_process_master_id"],
            "pause_days"=>$road_pause_days
          );
          array_push($gantchart_data, $road_data);
      }
      $task_manpower_percentage = $completion_percentage_data["manpower_percenatage"];
      $task_machine_percentage = $completion_percentage_data["machine_percentage"];
      $task_contract_percentage = $completion_percentage_data["contract_percentage"];
      $avg_count = $completion_percentage_data["avg_count"];

      if(count($query_for_project_process_task_road_data) != 0)
      {
        $total_work_to_be_completed_for_task = ($total_work_to_be_completed_for_road/count($query_for_project_process_task_road_data));
        $task_completion_percentage = ($total_road_completion_percentage/count($query_for_project_process_task_road_data));
      }
      else {
        $total_work_to_be_completed_for_task = 0;
        $task_completion_percentage = 0;
      }
      if(round($task_completion_percentage) == 1)
      {
          $actual_end_date_for_task = $task_actual_end_date ;
      }
      else if($task_actual_start_date != "30-11-2099 12:00" && $task_actual_end_date="30-11--0001 12:00") {
          $actual_end_date_for_task = date("d-m-Y H:i:s") ;
      }
      else {
        $actual_end_date_for_task = "30-11--0001 12:00" ;
      }
      if($total_road_percentage == 1)
      {
        $actual_task_end_date = $task_dates_data["actual_end_date"];
      }
      else {
      $actual_task_end_date = "000-00-00";
      }
      $task_pause_days = get_pause_days(0,0,$query_for_project_process_task_data[$tcount]["project_process_task_id"],'',$task_planned_start_date,$task_dates[0]["end_date"],$actual_task_end_date);
      $total_work_to_be_completed_for_process +=$total_work_to_be_completed_for_task ;
      $total_percentage_completed_for_task +=$task_completion_percentage ;
      $task_data = array(
        "start_date"=>$task_actual_start_date,
        "end_date"=>$actual_end_date_for_task,
        "planned_start_date"=>$task_planned_start_date,
        "planned_end_date"=>$task_planned_end_date,
        "parent"=>$process_uniq_id,
        "type"=>'task',
        "progress"=>$task_completion_percentage,
        "text"=>$query_for_project_process_task_data[$tcount]["project_task_master_name"],
        "id"=>($query_for_project_process_task_data[$tcount]["project_task_master_id"]*333333),
        "unscheduled"=>$task_unscheduled,
        "planned_budget_amount"=>round($task_planned_budget_amount),
        "budget_amount"=>round($task_budget_amount),
        "rw_budget_amount"=>$task_rw_budget_amount,
        "reg_msmrt"=>$task_regular_msmrt,
        "rw_msmrt"=>$task_rework_msmrt,
        "planned_msmrt"=>$task_planned_measurement,
        "project_id"=>$query_for_project_data[$project_count]["project_management_master_id"],
        "work_to_be_completed"=>round($total_work_to_be_completed_for_task),
        "process_master_id"=>$query_for_project_process_data[$pcount]["project_process_master_id"],
        "task_dates"=>$task_dates,
        "pause_days"=>$task_pause_days
      );
      array_push($gantchart_data, $task_data);
    }
    // $total_overall += $overall_percentage;
    $process_dates_data = get_dates("-1",$query_for_project_process_data[$pcount]["plan_process_id"]);
    $process_budget_amount = get_budget_amount(0,$query_for_project_process_data[$pcount]["plan_process_id"],0,0,0);
    $process_budget_rw_amount = get_rework_budget_amount(0,$query_for_project_process_data[$pcount]["plan_process_id"],0,0,0);
    $process_msmrt = get_actaul_measurement(0,$query_for_project_process_data[$pcount]["plan_process_id"],0,0,0);
    $process_planned_budget_amount = get_planned_budget_amount(0,$query_for_project_process_data[$pcount]["plan_process_id"],0,0);
    $process_planned_msmrt = get_planned_measurement(0,$query_for_project_process_data[$pcount]["plan_process_id"],0,0);
    $process_dates = get_planned_date_changed_history(0,$query_for_project_process_data[$pcount]["plan_process_id"],0,0);
    $process_planned_start_date = $process_dates_data["planned_start_date"];
    $process_planned_end_date = $process_dates_data["planned_end_date"];
    $process_actual_start_date = $process_dates_data["actual_start_date"];
    $process_actual_end_date = $process_dates_data["actual_end_date"];
    $process_regular_msmrt = $process_msmrt["total_regular_msmrt"];
    $process_rework_msmrt = $process_msmrt["total_rework_msmrt"];
    if(count($query_for_project_process_task_data) != 0)
    {
      $total_work_to_be_completed_for_process_count = ($total_work_to_be_completed_for_process/count($query_for_project_process_task_data));
      $process_percentage = ($total_percentage_completed_for_task/count($query_for_project_process_task_data));
    }
    else
     {
      $total_work_to_be_completed_for_process_count = 0;
      $process_percentage = 0;
    }
    if(round($process_percentage) == 1)
    {
        $actual_end_date_for_process = $process_actual_end_date ;
    }
    else if($process_actual_start_date != "30-11-2099 12:00" && $process_actual_end_date="30-11--0001 12:00") {
        $actual_end_date_for_process = date("d-m-Y H:i:s") ;
    }
    else {
      $actual_end_date_for_process = "30-11--0001 12:00" ;
    }
    if(($process_actual_start_date == "30-11-2099 12:00" && $actual_end_date_for_process == "30-11--0001 12:00") || ($process_actual_start_date == "01-01-1970 01:00" && $process_actual_end_date == "30-11--0001 12:00") || $query_for_project_process_data[$pcount]["project_process_master_id"] == 32 || $query_for_project_process_data[$pcount]["project_process_master_id"] == 33 )
    {
      $process_unscheduled = true;
    }
    else {
      $process_unscheduled = false ;
    }
    $total_work_to_be_completed_for_project +=$total_work_to_be_completed_for_process_count ;
    if($query_for_project_process_data[$pcount]["project_process_master_id"] != 32 && $query_for_project_process_data[$pcount]["project_process_master_id"] != 33)
    {
      $total_process_percentage +=$process_percentage ;
    }
    else {
      $total_process_percentage += 0;
    }
    $process_pause_days = get_pause_days('',$query_for_project_process_data[$pcount]["plan_process_id"],'','',$process_planned_start_date,$process_dates[0]["end_date"],$actual_end_date_for_process);
    $process_data = array(
      "start_date"=>$process_actual_start_date,
      "end_date"=>$actual_end_date_for_process,
      "planned_start_date"=>$process_planned_start_date,
      "planned_end_date"=>$process_planned_end_date,
      "parent"=>$query_for_project_data[$project_count]["project_management_master_id"],
      "type"=>'task',
      "progress"=>$process_percentage,
      "text"=>$query_for_project_process_data[$pcount]["process_name"],
      "id"=>$process_uniq_id,
      "unscheduled"=>$process_unscheduled,
      "planned_budget_amount"=>round($process_planned_budget_amount),
      "budget_amount"=>round($process_budget_amount),
      "rw_budget_amount"=>$process_budget_rw_amount,
      "reg_msmrt"=>$process_regular_msmrt,
      "rw_msmrt"=>$process_rework_msmrt,
      "planned_msmrt"=>$process_planned_msmrt,
      "project_id"=>$query_for_project_data[$project_count]["project_management_master_id"],
      "work_to_be_completed"=>round($total_work_to_be_completed_for_process_count),
      "process_master_id"=>$query_for_project_process_data[$pcount]["project_process_master_id"],
      "process_dates"=>$process_dates,
      "pause_days"=>$process_pause_days
    );
    array_push($gantchart_data, $process_data);
  }
  $project_dates_data = get_dates($query_for_project_data[$project_count]["project_management_master_id"],"-1");
  $planned_start_date = $project_dates_data["planned_start_date"];
  $planned_end_date = $project_dates_data["planned_end_date"];
  $actual_start_date = $project_dates_data["actual_start_date"];
  $actual_end_date = $project_dates_data["actual_end_date"];
  $project_budget_amount = get_budget_amount($query_for_project_data[$project_count]["project_management_master_id"],0,0,0);
  $project_budget_rw_amount = get_rework_budget_amount($query_for_project_data[$project_count]["project_management_master_id"],0,0,0);
  $project_msmrt = get_actaul_measurement($query_for_project_data[$project_count]["project_management_master_id"],0,0,0);
  $project_regular_msmrt = $project_msmrt["total_regular_msmrt"];
  $project_rework_msmrt = $project_msmrt["total_rework_msmrt"];
  $project_planned_budget_amount = get_planned_budget_amount($query_for_project_data[$project_count]["project_management_master_id"],0,0,0);
  $project_planned_msmrt = get_planned_measurement($query_for_project_data[$project_count]["project_management_master_id"],0,0,0);
  $project_dates = get_planned_date_changed_history($query_for_project_data[$project_count]["project_management_master_id"],0,0,0);

  if($actual_start_date == "30-11-2099 12:00" && $actual_end_date == "30-11--0001 12:00")
  {
    $project_unscheduled = true;
  }
  else {
    $project_unscheduled = false ;
  }
  if(count($query_for_project_process_data) != 0)
  {
    $project_work = $total_work_to_be_completed_for_project/count($query_for_project_process_data) ;
    $project_completion_percenatge = $total_process_percentage/count($query_for_project_process_data) ;
  }
  else {
    $project_work = 0 ;
    $project_completion_percenatge = 0 ;
  }
  if(round($project_completion_percenatge) == 1)
  {
    $actual_project_end_date = $project_dates_data["actual_end_date"];
  }
  else {
  $actual_project_end_date = "000-00-00";
  }

  $project_pause_days = get_pause_days($query_for_project_data[$project_count]["project_management_master_id"],'','','',$planned_start_date,$project_dates[0]["end_date"],$actual_project_end_date);
  $project_data = array(
    "start_date"=>$actual_start_date,
    "end_date"=>$actual_end_date,
    "planned_start_date"=>$planned_start_date,
    "planned_end_date"=>$planned_end_date,
    "parent"=>'0',
    "type"=>'project',
    "progress"=>$project_completion_percenatge,
    "text"=>$query_for_project_data[$project_count]["project_master_name"],
    "id"=>$query_for_project_data[$project_count]["project_management_master_id"],
    "unscheduled"=>$project_unscheduled,
    "budget_amount"=>round($project_budget_amount),
    "planned_budget_amount"=>round($project_planned_budget_amount),
    "rw_budget_amount"=>$project_budget_rw_amount,
    "reg_msmrt"=>$project_regular_msmrt,
    "rw_msmrt"=>$project_rework_msmrt,
    "wish_date"=>$wish_dates,
    "planned_msmrt"=>$project_planned_msmrt,
    "work_to_be_completed"=>round($project_work),
    "project_dates"=>$project_dates,
    "pause_days"=>$project_pause_days
  );
  array_push($gantchart_data, $project_data);

  create_json_file($query_for_project_data[$project_count]["project_management_master_id"],$gantchart_data);
}

function get_dates($project_id,$process_id)
{
  //Actuals
  //ManPower
  $query_for_manpower_date = 'SELECT cp,min(date) as min_start_date , max(date) as max_end_date from actual_manpower where process_id ='. $process_id.' or project_id ='.$project_id.' and active = 1';
  ini_set('max_execution_time', -1);
  $manpower_date_data = query($query_for_manpower_date);
  // $mp_start_date = $manpower_date_data[0]["min_start_date"] ? date('Y-m-d',strtotime($manpower_date_data[0]["min_start_date"])) : "30-11-2099";
  $mp_start_date = start_date_format($manpower_date_data[0]["min_start_date"]);
  // $mp_end_date =  $manpower_date_data[0]["max_end_date"] ? date('Y-m-d',strtotime($manpower_date_data[0]["max_end_date"])) : "0000-00-00" ;
  $mp_end_date = end_date_format($manpower_date_data[0]["max_end_date"]);
  //Machine
  $query_for_machine_date = 'SELECT cp,min(machine_start_date) as machine_start_date,max(machine_end_date) as machine_end_date FROM `actual_machine` where process_id ='. $process_id.' or project_id ='.$project_id.' and active = 1';
  ini_set('max_execution_time', -1);
  $machine_date_data = query($query_for_machine_date);
  // $mc_start_date = $machine_date_data[0]["machine_start_date"] ? date('Y-m-d',strtotime($machine_date_data[0]["machine_start_date"])) :  "30-11-2099";
  $mc_start_date = start_date_format($machine_date_data[0]["machine_start_date"]);
  // $mc_end_date =   $machine_date_data[0]["machine_end_date"] ? date('Y-m-d',strtotime($machine_date_data[0]["machine_end_date"])) : "0000-00-00" ;
  $mc_end_date = end_date_format($machine_date_data[0]["machine_end_date"]);

  //Contract
  $query_for_contract_date = 'SELECT project_task_actual_boq_completion,min(date) as min_date, max(date) as max_date FROM `actual_contract` where process_id ='. $process_id.' or project_id ='.$project_id.' and active = 1';
  ini_set('max_execution_time', -1);
  $contract_date_data = query($query_for_contract_date );
  // $cw_start_date = $contract_date_data[0]["min_date"] ? date('Y-m-d',strtotime($contract_date_data[0]["min_date"])) : "30-11-2099";
  $cw_start_date = start_date_format($contract_date_data[0]["min_date"]);
  // $cw_end_date =   $contract_date_data[0]["max_date"] ? date('Y-m-d',strtotime($contract_date_data[0]["max_date"])) : "0000-00-00" ;
  $cw_end_date = end_date_format($contract_date_data[0]["max_date"]);

  //else
  $query_for_cur_planned_date = 'SELECT min(NULLIF(planned_start_date,0)) as min_start_date,max(planned_end_date) as max_end_date from task_planning_list where  plan_process_id ='.$process_id.' or project_id ='.$project_id.' and planned_start_date != "0000-00-00"';
  ini_set('max_execution_time', -1);
  $query_for_cur_planned_date_data = query($query_for_cur_planned_date);
  $cur_planned_start_date = $query_for_cur_planned_date_data[0]["min_start_date"];
  $cur_planned_end_date = $query_for_cur_planned_date_data[0]["max_end_date"];

  $query_for_planned_date_history = 'SELECT old_end_date from task_planning_shadow_list  where  process_id ='.$process_id.' or project_id ='.$project_id.' order by changed_on ASC';
  $query_for_planned_date_history_data = query($query_for_planned_date_history);
  if(empty($query_for_planned_date_history_data))
  {
    $planned_old_end_dates = "0000-00-00" ;
  }
  else {
    $planned_old_end_dates = $query_for_planned_date_history_data[0]["old_end_date"];
  }
  if($planned_old_end_dates == "0000-00-00")
  {
    $planned_dates_updated = $cur_planned_end_date ;
  }
  else {
    $planned_dates_updated = $planned_old_end_dates ;
  }

  $min_dates = array($mp_start_date,$mc_start_date,$cw_start_date);
  $max_dates = array($mp_end_date,$mc_end_date,$cw_end_date);
  $actual_min_start_date = min(array_filter($min_dates));
  $actual_max_end_date = max(array_filter($max_dates));

  if($actual_max_end_date == 0000-00-00)
  {
    $actual_end_date = date("d-m-Y h:i");
  }
    else {
    $actual_end_date = $actual_max_end_date ;
    }

    if($actual_min_start_date != "30-11-2099 12:00")
    {
      $actual_start = date("d-m-Y h:i", strtotime($actual_min_start_date));
    }
    else {
      $actual_start = "30-11-2099 12:00";
    }

    $planned_dates = array("actual_start_date"=>$actual_start,
                           "actual_end_date"=>date("d-m-Y h:i",strtotime($actual_max_end_date)),
                           "planned_start_date"=>date("d-m-Y h:i",strtotime($cur_planned_start_date)),
                           "planned_end_date"=>date("d-m-Y h:i",strtotime($planned_dates_updated)));
    return ($planned_dates);
}

function get_percentage($task_id,$road_id)
{
  // $mysqli = get_conn_handle();
  $a = ""; $b = ""; $c = "";
  if($road_id != 0) {
    $a =  ' and project_task_actual_manpower_road_id ='. $road_id;
    $b =  ' and project_task_actual_machine_plan_road_id ='. $road_id;
    $c =  ' and project_task_actual_boq_road_id ='. $road_id;
  }
  //Manpower
  $avg_count = 0;
  $query_for_project_process_task_mp = 'SELECT project_task_actual_manpower_task_id,max(project_task_actual_manpower_completion_percentage)as max_mp_percentage from project_task_actual_manpower where project_task_actual_manpower_task_id ='.$task_id.$a;
  ini_set('max_execution_time', -1);
  $query_for_project_process_task_mp_data = query($query_for_project_process_task_mp);
  $manpower_percentage = $query_for_project_process_task_mp_data[0]["max_mp_percentage"];
  $manpower_task_id = $query_for_project_process_task_mp_data[0]["project_task_actual_manpower_task_id"];
  if($manpower_task_id != NULL)
  {
    $avg_count = $avg_count + 1;
  }
  //Machine
  $query_for_project_process_task_mc = 'SELECT project_task_actual_machine_plan_task_id,max(project_task_actual_machine_completion)as max_mc_percentage from project_task_actual_machine_plan where project_task_actual_machine_plan_task_id ='.$task_id . $b;
  ini_set('max_execution_time', -1);
  $query_for_project_process_task_mc_data = query($query_for_project_process_task_mc);
  $machine_percentage = $query_for_project_process_task_mc_data[0]["max_mc_percentage"];
  $machine_task_id = $query_for_project_process_task_mc_data[0]["project_task_actual_machine_plan_task_id"];
  if($machine_task_id != NULL)
  {
    $avg_count = $avg_count + 1;
  }
  //Contract
  $query_for_project_process_task_cw = 'SELECT project_boq_actual_task_id, max(project_task_actual_boq_completion)as max_cw_mpercentage from project_task_boq_actuals where project_boq_actual_task_id='.$task_id .$c;
  $query_for_project_process_task_cw_data = query($query_for_project_process_task_cw);
  $contract_percentage = $query_for_project_process_task_cw_data[0]["max_cw_mpercentage"];
  $contract_task_id = $query_for_project_process_task_cw_data[0]["project_boq_actual_task_id"];
  if($contract_task_id != NULL)
  {
    $avg_count = $avg_count + 1;
  }
  $completion_percentage = array("manpower_percenatage"=>$manpower_percentage,"machine_percentage"=>$machine_percentage,"contract_percentage"=>$contract_percentage,"avg_count"=>$avg_count);
  return $completion_percentage ;
}

function get_dates_for_task_road_level($task_id,$road_id)
{
  $a = "" ;
  if($road_id != 0) {
  $a =  ' and road_id ='. $road_id;
  }
  $query_for_manpower_date = 'SELECT cp,min(date) as min_start_date , max(date) as max_end_date from actual_manpower where task_id='.$task_id .$a.' and active=1';
  $manpower_date_data = query($query_for_manpower_date);
  $mp_start_date = start_date_format($manpower_date_data[0]["min_start_date"]);
  $mp_end_date = end_date_format($manpower_date_data[0]["max_end_date"]);
  //Machine
  $query_for_machine_date = 'SELECT cp,min(machine_start_date) as machine_start_date,max(machine_end_date) as machine_end_date FROM `actual_machine` where task_id='.$task_id .$a.' and active=1';
  $machine_date_data = query($query_for_machine_date);
  $mc_start_date = start_date_format($machine_date_data[0]["machine_start_date"]);
  $mc_end_date = end_date_format($machine_date_data[0]["machine_end_date"]);

  //Contract
  $query_for_contract_date = 'SELECT project_task_actual_boq_completion,min(date) as min_date, max(date) as max_date FROM `actual_contract` where task_id='.$task_id .$a.' and active=1';
  $contract_date_data = query($query_for_contract_date );
  $cw_start_date = start_date_format($contract_date_data[0]["min_date"]);
  $cw_end_date = end_date_format($contract_date_data[0]["max_date"]);

  //Planned Start and End Dates
  $query_for_cur_planned_date = 'SELECT min(planned_start_date) as min_start_date,max(planned_end_date) as max_end_date from task_planning_list where task_id='.$task_id .$a.' and active=1';
  $query_for_cur_planned_date_data = query($query_for_cur_planned_date);
  $cur_planned_start_date = $query_for_cur_planned_date_data[0]["min_start_date"];
  $cur_planned_end_date = $query_for_cur_planned_date_data[0]["max_end_date"];

  $query_for_planned_date_history = 'SELECT old_end_date from task_planning_shadow_list where task_id='.$task_id .$a.' order by changed_on ASC';
  $query_for_planned_date_history_data = query($query_for_planned_date_history);
  if(empty($query_for_planned_date_history_data))
  {
    $planned_old_end_dates = "0000-00-00" ;
  }
  else {
    $planned_old_end_dates = $query_for_planned_date_history_data[0]["old_end_date"];
  }
  if($planned_old_end_dates == "0000-00-00")
  {
    $planned_dates_updated = $cur_planned_end_date ;
  }
  else {
    $planned_dates_updated = $planned_old_end_dates ;
  }

  $min_dates = array($mp_start_date,$mc_start_date,$cw_start_date);
  $max_dates = array($mp_end_date,$mc_end_date,$cw_end_date);
  $actual_min_start_date = min(array_filter($min_dates));
  $actual_max_end_date = max(array_filter($max_dates));

  if($actual_min_start_date != "30-11-2099 12:00")
  {
    $actual_start = date("d-m-Y h:i", strtotime($actual_min_start_date));
  }
  else {
    $actual_start = "30-11-2099 12:00";
  }
  $task_road_level_data = array("actual_start_date"=>$actual_start,
                                "actual_end_date"=>date("d-m-Y h:i", strtotime($actual_max_end_date)),
                                "planned_start_date"=>date("d-m-Y h:i", strtotime($cur_planned_start_date)),
                                "planned_end_date"=>date("d-m-Y h:i", strtotime($planned_dates_updated)));
  return $task_road_level_data ;
}

function get_budget_amount($project_id,$process_id,$task_id,$road_id)
{
  // $mysqli = get_conn_handle();
  if($project_id != 0) {
  $query =  ' where project_id ='. $project_id.' and active=1';
  $query_for_material =  ' where actual_material_project_id ='. $project_id;
  }
  elseif($process_id != 0)
  {
    $query =  ' where process_id ='. $process_id.' and active=1';
    $query_for_material =  ' where actual_material_process_id ='. $process_id;
  }
  elseif ($task_id != 0 && $road_id != 0) {
    $query =  ' where task_id ='. $task_id.' and road_id='.$road_id.' and active=1';
    $query_for_material =  ' where actual_material_task_id ='. $task_id.' and actual_material_road_id='.$road_id;
  }
  else {
    $query =  ' where task_id ='. $task_id.' and active=1';
    $query_for_material =  ' where actual_material_task_id ='. $task_id;
  }
  //Manpower
  $query_for_manpower_budget_amount = 'SELECT work_type,sum(total_cost) as manpower_total from actual_manpower'.$query;
  $manpower_budget_amount_data = query($query_for_manpower_budget_amount);
  $manpower_total_amount = $manpower_budget_amount_data[0]["manpower_total"];
  if($manpower_budget_amount_data[0]["work_type"] == "Rework")
  {
    $mp_rework_amount = $manpower_budget_amount_data[0]["manpower_total"];
  }
  else {
    $mp_rework_amount = 0 ;
  }
  //Machine
  $query_for_machine_budget_amount = 'SELECT machine_cost,fuel_charge,machine_start_date,machine_end_date,off_time,work_type from actual_machine'.$query;
  $machine_budget_amount_data = query($query_for_machine_budget_amount);
  $machine_total_amount = 0 ;
  $mc_rework_amount = 0 ;

  for($mp_count = 0 ; $mp_count < count($machine_budget_amount_data) ; $mp_count++)
  {
    if($machine_budget_amount_data[$mp_count]["work_type"] == "Rework")
    {
      $mc_rework_amount = $mc_rework_amount + $machine_budget_amount_data[$mp_count]["machine_cost"];
    }
    else {
      $mc_rework_amount = 0;
    }
    $machine_rate = $machine_budget_amount_data[$mp_count]['fuel_charge'];
    if($machine_budget_amount_data[$mp_count]["machine_end_date"] != "0000-00-00 00:00:00")
    {
      $start_date_time = strtotime($machine_budget_amount_data[$mp_count]["machine_start_date"]);
      $end_date_time = strtotime($machine_budget_amount_data[$mp_count]["machine_end_date"]);
      $date_diff = $end_date_time - $start_date_time;
      $no_hrs_worked = $date_diff/3600;
    }
    else
    {
      $start_date_time = strtotime($machine_budget_amount_data[$mp_count]["machine_start_date"]);
      $end_date_time = strtotime(date("Y-m-d H:i:s"));
      $date_diff = $end_date_time - $start_date_time;
      $no_hrs_worked = $date_diff/3600;
    }
    // Off time related calculation
    $off_time = $machine_budget_amount_data[$mp_count]["off_time"]/60;
    $eff_hrs = $no_hrs_worked - $off_time;
    $actual_mc_cost = ($machine_rate * $eff_hrs) + $machine_budget_amount_data[$mp_count]["machine_cost"];
    $machine_total_amount = $machine_total_amount + $actual_mc_cost ;
  }

  //Contract
  $query_for_contract_budget_amount = 'SELECT sum(total_amount) as contract_total,work_type from actual_contract'.$query;
  $contract_budget_amount_data = query($query_for_contract_budget_amount);
  $contract_total_amount = $contract_budget_amount_data[0]["contract_total"];
  if($contract_budget_amount_data[0]["work_type"] == "Rework")
  {
      $cw_rework_amount = $contract_budget_amount_data[0]["contract_total"];
  }
  else {
    $cw_rework_amount = 0;
  }

  //Material
  $query_for_material_budget_amount = 'SELECT *,sum(total_amt) as total_amt from actual_material'.$query_for_material;
  $material_budget_amount_data = query($query_for_material_budget_amount);
  $material_total_amount = $material_budget_amount_data[0]["total_amt"];
  $total_budget_cost = $manpower_total_amount + $machine_total_amount + $contract_total_amount + $material_total_amount ;
  return $total_budget_cost ;
}
function get_planned_budget_amount($project_id,$process_id,$task_id,$road_id)
{
  // $mysqli = get_conn_handle();
  if($project_id != 0) {
  $query =  ' where planned_project_id ='. $project_id;
  }
  elseif($process_id != 0)
  {
    $query =  ' where planned_process_id ='. $process_id;
  }
  elseif ($task_id != 0 && $road_id != 0) {
    $query =  ' where planned_task_id ='. $task_id.' and planned_road_id='.$road_id;
  }
  else {
    $query =  ' where planned_task_id ='. $task_id;
  }
  $query_for_planned_budget_amount = 'SELECT * from budget_planning'.$query;
  $planned_budget_amount_data = query($query_for_planned_budget_amount);
  // print_r($query_for_planned_budget_amount); exit;
  $total_planned_mp_cost = 0;
  $total_planned_mc_cost = 0;
  $total_planned_cw_cost = 0;
  $total_planned_material_cost = 0;
  for($plan_count = 0 ; $plan_count < count($planned_budget_amount_data) ; $plan_count++)
  {
    $total_planned_mp_cost = $total_planned_mp_cost + $planned_budget_amount_data[$plan_count]["planned_manpower"];
    $total_planned_mc_cost = $total_planned_mc_cost + $planned_budget_amount_data[$plan_count]["planned_machine"];
    $total_planned_cw_cost = $total_planned_cw_cost + $planned_budget_amount_data[$plan_count]["planned_contract"];
    $total_planned_material_cost = $total_planned_material_cost + $planned_budget_amount_data[$plan_count]["planned_material"];
  }
  $total_planned_budget_cost = $total_planned_mp_cost + $total_planned_mc_cost + $total_planned_cw_cost + $total_planned_material_cost;
  return $total_planned_budget_cost;
}
function get_actaul_measurement($project_id,$process_id,$task_id,$road_id)
{
  // $mysqli = get_conn_handle();
  if($project_id != 0) {
  $query =  ' where project_id ='. $project_id.' and active=1';
  }
  elseif($process_id != 0)
  {
    $query =  ' where process_id ='. $process_id.' and active=1';
  }
  elseif ($task_id != 0 && $road_id != 0) {
    $query =  ' where task_id ='. $task_id.' and road_id='.$road_id.' and active=1';
  }
  else {
    $query =  ' where task_id ='. $task_id.' and active=1';
  }
  //Manpower
  $query_for_manpower_msmrt = 'SELECT sum(msmrt) as manpower_msmrt,work_type from actual_manpower'.$query;
  $manpower_msmrt_data = query($query_for_manpower_msmrt);
  $manpower_reg_total_msmrt = 0 ;
  $manpower_rw_total_msmrt = 0 ;
  if($manpower_msmrt_data[0]["work_type"] == "Regular")
  {
    $manpower_reg_total_msmrt = $manpower_msmrt_data[0]["manpower_msmrt"];
  }
  elseif ($manpower_msmrt_data[0]["work_type"] == "Rework") {
    $manpower_rw_total_msmrt = $manpower_msmrt_data[0]["manpower_msmrt"];
  }

  //Machine
  $query_for_machine_msmrt = 'SELECT sum(msmrt) as machine_msmrt,work_type from actual_machine'.$query;
  $machine_msmrt_data = query($query_for_machine_msmrt);
  $machine_reg_total_msmrt = 0 ;
  $machine_rw_total_msmrt = 0 ;
  if($machine_msmrt_data[0]["work_type"] == "Regular")
  {
    $machine_reg_total_msmrt = $machine_msmrt_data[0]["machine_msmrt"];
  }
  elseif ($machine_msmrt_data[0]["work_type"] == "Rework") {
    $machine_rw_total_msmrt = $machine_msmrt_data[0]["machine_msmrt"];
  }
  //Contract
  $query_for_contract_msmrt = 'SELECT sum(msmrt) as contract_msmrt,work_type from actual_contract'.$query;
  $contract_msmrt_data = query($query_for_contract_msmrt);
  $contract_reg_total_msmrt = 0;
  $contract_rw_total_msmrt = 0;
  if($contract_msmrt_data[0]["work_type"] == "Regular")
  {
    $contract_reg_total_msmrt = $contract_msmrt_data[0]["contract_msmrt"];
  }
  elseif ($contract_msmrt_data[0]["work_type"] == "Rework") {
    $contract_rw_total_msmrt = $contract_msmrt_data[0]["contract_msmrt"];
  }
  $total_regular_msmrt = $manpower_reg_total_msmrt + $machine_reg_total_msmrt + $contract_reg_total_msmrt ;
  $total_rework_msmrt = $manpower_rw_total_msmrt + $machine_rw_total_msmrt + $contract_rw_total_msmrt ;
  $task_msmrt_data = array("total_regular_msmrt"=>$total_regular_msmrt,
                                "total_rework_msmrt"=>$total_rework_msmrt);
  return $task_msmrt_data ;
}
function get_rework_budget_amount($project_id,$process_id,$task_id,$road_id)
{
  // $mysqli = get_conn_handle();
  if($project_id != 0) {
  $query =  ' where project_id ='. $project_id.' and active=1 and work_type="Rework"';
  }
  elseif($process_id != 0)
  {
    $query =  ' where process_id ='. $process_id.' and active=1 and work_type="Rework"';
  }
  elseif ($task_id != 0 && $road_id != 0) {
    $query =  ' where task_id ='. $task_id.' and road_id='.$road_id.' and active=1 and work_type="Rework"';
  }
  else {
    $query =  ' where task_id ='. $task_id.' and active=1 and work_type="Rework"';
  }
  //Manpower
  $query_for_manpower_budget_amount = 'SELECT work_type,sum(total_cost) as manpower_total from actual_manpower'.$query;
  $manpower_budget_amount_data = query($query_for_manpower_budget_amount);
  $manpower_rw_total_amount = $manpower_budget_amount_data[0]["manpower_total"];

  //Machine
  $query_for_machine_budget_amount = 'SELECT machine_cost,fuel_charge,machine_start_date,machine_end_date,off_time,work_type from actual_machine'.$query;
  $machine_budget_amount_data = query($query_for_machine_budget_amount);
  $machine_rw_total_amount = 0 ;
  for($mp_count = 0 ; $mp_count < count($machine_budget_amount_data) ; $mp_count++)
  {
    $machine_rate = $machine_budget_amount_data[$mp_count]['fuel_charge'];
    if($machine_budget_amount_data[$mp_count]["machine_end_date"] != "0000-00-00 00:00:00")
    {
      $start_date_time = strtotime($machine_budget_amount_data[$mp_count]["machine_start_date"]);
      $end_date_time = strtotime($machine_budget_amount_data[$mp_count]["machine_end_date"]);
      $date_diff = $end_date_time - $start_date_time;
      $no_hrs_worked = $date_diff/3600;
    }
    else
    {
      $start_date_time = strtotime($machine_budget_amount_data[$mp_count]["machine_start_date"]);
      $end_date_time = strtotime(date("Y-m-d H:i:s"));
      $date_diff = $end_date_time - $start_date_time;
      $no_hrs_worked = $date_diff/3600;
    }
    // Off time related calculation
    $off_time = $machine_budget_amount_data[$mp_count]["off_time"]/60;
    $eff_hrs = $no_hrs_worked - $off_time;
    $actual_mc_cost = ($machine_rate * $eff_hrs) + $machine_budget_amount_data[$mp_count]["machine_cost"];
    $machine_rw_total_amount = $machine_rw_total_amount + $actual_mc_cost ;
  }

  //Contract
  $query_for_contract_budget_amount = 'SELECT sum(total_amount) as contract_total,work_type from actual_contract'.$query;
  $contract_budget_amount_data = query($query_for_contract_budget_amount);
  $contract_rw_total_amount = $contract_budget_amount_data[0]["contract_total"];
  $total_rework_budget_cost = $manpower_rw_total_amount + $machine_rw_total_amount + $contract_rw_total_amount ;
  return $total_rework_budget_cost ;
}
function get_planned_measurement($project_id,$process_id,$task_id,$road_id)
{
  // $mysqli = get_conn_handle();
  if($project_id != 0) {
  $query =  ' where project_id ='. $project_id.' and active=1';
  }
  elseif($process_id != 0)
  {
    $query =  ' where plan_process_id ='. $process_id.' and active=1';
  }
  elseif ($task_id != 0 && $road_id != 0) {
    $query =  ' where task_id ='. $task_id.' and road_id='.$road_id.' and active=1';
  }
  else {
    $query =  ' where task_id ='. $task_id.' and active=1';
  }
  //Manpower
  $query_for_planned_msmrt = 'SELECT sum(planned_msmrt) as total_planned_msmrt from task_planning_list'.$query;
  $manpower_planned_msmrt_data = query($query_for_planned_msmrt);
  $planned_total_msmrt = $manpower_planned_msmrt_data[0]["total_planned_msmrt"];
  return $planned_total_msmrt ;
}
function start_date_format($date)
{
  if(($date != "0000-00-00") && ($date != NULL))
  {
    $start_date = date('Y-m-d',strtotime($date)) ;
  }
  else {
    $start_date = "30-11-2099 12:00";
  }
  return $start_date ;
}
function end_date_format($date)
{
  if(($date != "0000-00-00") && ($date != NULL))
  {
    $start_date = date('Y-m-d',strtotime($date)) ;
  }
  else {
    $start_date = "0000-00-00";
  }
  return $start_date ;
}
function get_planned_date_changed_history($project_id,$process_id,$task_id,$road_id)
{
  $planned_dates_history_array = array();
  if($project_id != 0) {
  $query =  ' where project_id ='. $project_id.' and process_name NOT IN("MAINTENANCE","MISCELLANEOUS")';
  }
  elseif($process_id != 0)
  {
    $query =  ' where process_id ='. $process_id;
  }
  elseif ($task_id != 0 && $road_id != 0) {
    $query =  ' where task_id ='. $task_id.' and road_id='.$road_id;
  }
  else {
    $query =  ' where task_id ='. $task_id;
  }
  //Manpower
  $query_for_planned_date_history = 'SELECT old_end_date, min(new_start_date) as start_date,max(new_end_date) as end_date,old_start_date from task_planning_shadow_list'.$query;
  $query_for_planned_date_history_data = query($query_for_planned_date_history);

  $planned_old_start_dates = $query_for_planned_date_history_data[0]["old_start_date"];
  $planned_old_new_dates = $query_for_planned_date_history_data[0]["old_end_date"];
  $planned_updated_start_date = $query_for_planned_date_history_data[0]["start_date"];
  $planned_updated_end_date = $query_for_planned_date_history_data[0]["end_date"];

  $planned_dates_history = array("old_start_date"=>end_date_format($planned_old_start_dates),
                                "old_end_date"=>end_date_format($planned_old_new_dates),
                                "start_date"=>end_date_format($planned_updated_start_date),
                                "end_date"=>end_date_format($planned_updated_end_date));
                                array_push($planned_dates_history_array,$planned_dates_history);
  return $planned_dates_history_array ;
}

function get_pause_days($project_id,$process_id,$task_id,$road_id,$planned_start_date,$changed_end_date,$actual_end_date)
{
  if($actual_end_date != "0000-00-00")
  {
    $date_query = " AND `delay_reason_start_date` >= '". $planned_start_date."' AND `delay_reason_end_date` <= '". $changed_end_date."'";
}
  else {
    $date_query = " AND `delay_reason_start_date` >= '". $actual_end_date."' AND `delay_reason_end_date` <= '". $changed_end_date."'";
  }

  $planned_dates_history_array = array();
  if($project_id != 0) {
  $query =  ' where project_id ='. $project_id;
  }
  elseif($process_id != 0)
  {
    $query =  ' where process_id ='. $process_id;
  }
  elseif ($task_id != 0 && $road_id != 0) {
    $query =  ' where task_id ='. $task_id.' and road_id='.$road_id;
  }
  else {
    $query =  ' where task_id ='. $task_id;
  }

  //Manpower
  $query_for_planned_date_history = 'SELECT min(delay_reason_start_date) as start_date,max(delay_reason_end_date) as end_date from pause_list'.$query.$date_query;
  $query_for_planned_date_history_data = query($query_for_planned_date_history);
  $delay_start_date = $query_for_planned_date_history_data[0]["start_date"];
  $delay_end_date = $query_for_planned_date_history_data[0]["end_date"];
  $date1 = date_create($delay_start_date);
  $date2 = date_create($delay_end_date);

  //difference between two dates
  $pause_days = date_diff($date1,$date2);
  return $pause_days->format("%a") ;
}
// echo json_encode($gantchart_data, JSON_PRETTY_PRINT);
?>
