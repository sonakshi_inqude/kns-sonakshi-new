<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_transactions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_site_visit.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');

/*
PURPOSE : To add project
INPUT 	: Project, Customer Name, Customer Mobile No, Customer Email, Company, Location, Source, Remarks, Interest Status, Walk In, Follow Up Date, Assigned To, Added By, Is Preloaded
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_enquiry($project_id,$name,$cell,$email,$company,$location,$source,$remarks,$interest_status,$walk_in,$follow_up_date,$assigned_to,$added_by,$is_preload='0')
{
	$ccell = substr($cell, -10, 10);
	if($is_preload == '0')
	{
		$enquiry_list = i_get_enquiry_list('','','','','',$ccell,'','','','','','','','','','','','','','');
		if($enquiry_list["status"] != SUCCESS)
		{
			$allow_enquiry = true;
		}
		else
		{
			$allow_enquiry = false;
		}
	}
	else if($is_preload == '1')
	{
		$allow_enquiry = true;
	}
	
	if($allow_enquiry == true)
	{
		$last_enquiry_id_data = i_get_enquiry_list('','','','','','','','','','','','','','','','','','desc','0','1');
		
		if($last_enquiry_id_data["status"] == SUCCESS)
		{
			$enquiry_latest_id = $last_enquiry_id_data["data"][0]["enquiry_id"] + 1;
		}
		else
		{
			$enquiry_latest_id = 1;
		}
		
		$enquiry_added_user_name = substr($_SESSION["loggedin_user_name"],0,3);
		if($is_preload == '0')
		{
			$enquiry_id = strtoupper("KNS-".date("M")."-".$enquiry_added_user_name."-".$enquiry_latest_id);
		}
		else if($is_preload == '1')
		{
			$enquiry_id = strtoupper("KNS-R-".date("M")."-".$enquiry_added_user_name."-".$enquiry_latest_id);
		}
		$enquiry_iresult = db_add_enquiry($enquiry_id,$project_id,$name,$cell,$email,$company,$location,$source,$remarks,$interest_status,$walk_in,$follow_up_date,$assigned_to,$added_by);
			
		if($enquiry_iresult['status'] == SUCCESS)
		{
			$return["data"]["message"] = "Enquiry Successfully Added. Enquiry Number: ".$enquiry_id;
			$return["data"]["value"]   = $enquiry_iresult["data"];
			$return["status"]          = SUCCESS;	
			
			$enquiry_fup_iresult = i_add_enquiry_fup($enquiry_iresult["data"],$remarks,$interest_status,$follow_up_date,$added_by);
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "A customer with this mobile number already exists. Enquiry ID:".$enquiry_list["data"][0]["enquiry_number"].". Assigned To:".$enquiry_list["data"][0]["user_name"];
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get enquiry list
INPUT 	: Enquiry ID, Enquiry No, Project, Site ID, Name, Cell, Email, Company, Source, Interest Status, FUP Date, Assigned To, Added By,Start Date, End Date, Assigned Start Date, Assigned End Date, Order, Limit - Start, Limit - Count, Enquiry Source Type, Project Mapped User
OUTPUT 	: Enquiry List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_enquiry_list($enq_id,$enq_number,$project,$site_id,$name,$cell,$email,$company,$source,$interest_status,$follow_up_date,$assigned_to,$added_by,$start_date,$end_date,$assigned_start_date,$assigned_end_date,$order,$limit_start,$limit_count,$source_type='',$project_user='')
{
	$enquiry_sresult = db_get_enquiry_list($enq_id,$enq_number,$project,$site_id,$name,$cell,$email,$company,$source,$interest_status,$follow_up_date,$assigned_to,$added_by,$start_date,$end_date,$assigned_start_date,$assigned_end_date,$order,$limit_start,$limit_count,$source_type,$project_user);
	
	if($enquiry_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $enquiry_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No enquiries!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add project
INPUT 	: Project Name, Address, Client, Location, Authority, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_enquiry_fup($enquiry_id,$remarks,$interest_status,$follow_up_date,$added_by)
{
	$enquiry_fup_iresult = db_add_enquiry_follow_up($enquiry_id,$remarks,$interest_status,$follow_up_date,$added_by);
		
	if($enquiry_fup_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Enquiry Follow Up Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get enquiry follow up list
INPUT 	: Enquiry ID, Follow Up Date, Added By, Order, Start, Count, Start Date, End Date (of added on), Relationship of the user to enquiry, FUP Date Start, FUP Date End, Project Mapped User
OUTPUT 	: Enquiry Follow Up List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_enquiry_fup_list($enq_id,$follow_up_date,$added_by,$order,$limit_start,$limit_count,$start_date="",$end_date="",$relationship="",$fup_date_start="",$fup_date_end="",$project_user='')
{
	$enquiry_fup_sresult = db_get_enquiry_fup_list($enq_id,$follow_up_date,$added_by,$start_date,$end_date,$order,$limit_start,$limit_count,$relationship,$fup_date_start,$fup_date_end,$project_user);
	
	if($enquiry_fup_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $enquiry_fup_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No follow up added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update enquiry
INPUT 	: Enquiry ID, Assignee
OUTPUT 	: Enquiry ID or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_enquiry_assignee($enquiry_id,$assignee)
{
	$enquiry_uresult = db_update_enquiry_assignee($enquiry_id,$assignee);
	
	if($enquiry_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $enquiry_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add site visit plan
INPUT 	: Enquiry ID, Site Visit Date, Call Time, Project, Confirm Status, Drive Type, Location, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_site_visit_plan($enquiry_id,$site_visit_date,$site_visit_time,$site_project,$confirm_status,$drive_type,$location,$added_by)
{
	// De-activate all older site visits
	$site_visit_plan_uresult = db_update_site_visit_plan($enquiry_id,'0','');
	
	if($site_visit_plan_uresult['status'] == SUCCESS)
	{
		// Add a new site visit
		$site_visit_plan_iresult = db_add_site_visit_plan($enquiry_id,$site_visit_date,$site_visit_time,$site_project,$confirm_status,$drive_type,$location,$added_by);
			
		if($site_visit_plan_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $site_visit_plan_iresult["data"];
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get site visit plan
INPUT 	: Enquiry ID, Follow Up Date, Project, Status, Added On, Start Date, End Date, Order in which entries have to be sorted (Default: Descending), SVP ID, Project Mapped User
OUTPUT 	: Enquiry Follow Up List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_visit_plan_list($enquiry,$date,$project,$status,$added_by,$start_date,$end_date,$order,$svp_id="",$project_user="")
{
	$site_visit_plan_sresult = db_get_site_visit_plan_list($enquiry,$date,$project,$status,$added_by,$start_date,$end_date,$order,$svp_id,$project_user);
	
	if($site_visit_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_visit_plan_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site visit plan added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get filtered site visit plan
INPUT 	: Enquiry ID, Site Visit Planned Start Date, Site Visit Planned End Date, Project, Added By, Start Date, End Date, Order in which entries have to be sorted (Default: Descending)
OUTPUT 	: Enquiry Follow Up List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_visit_plan_filtered_list($enquiry,$sv_plan_start_date,$sv_plan_end_date,$project,$added_by,$start_date,$end_date,$order)
{
	$site_visit_plan_sresult = db_get_site_visit_plan_filtered_list($enquiry,$sv_plan_start_date,$sv_plan_end_date,$project,$added_by,$start_date,$end_date,$order);
	
	if($site_visit_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_visit_plan_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site visit plan added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add site travel plan
INPUT 	: Site Visit Plan ID, Cab ID, Project, Date Time, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_site_travel_plan($svp_id,$cab_id,$project,$date_time,$added_by)
{
	$site_travel_plan_iresult = db_add_site_travel_plan($svp_id,$cab_id,$project,$date_time,$added_by);
		
	if($site_travel_plan_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $site_travel_plan_iresult["data"];
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get site travel plan
INPUT 	: Travel Plan ID, Site Visit ID, Enquiry ID, Cab, Project, Date, Status, Assigned To, Added By, Relationship, Travel Start Date Filter, Travel End Date Filter, Project Mapped User
OUTPUT 	: Site Travel Plan List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_travel_plan_list($travel_plan_id,$site_visit_id,$enquiry_id,$cab,$project,$date,$status,$assigned_to,$added_by,$relationship="",$travel_start_date="",$travel_end_date="",$project_user="")
{
	$site_travel_plan_sresult = db_get_site_travel_plan($travel_plan_id,$site_visit_id,$enquiry_id,$cab,$project,$date,$status,$assigned_to,$added_by,'','',$relationship,$travel_start_date,$travel_end_date,$project_user);
	
	if($site_travel_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_travel_plan_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site travel plan added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add site travel actuals
INPUT 	: Site Travel Date, Enquiry ID, Project, Vehicle, Opening Km, Closing Km, Driver, Number of Passengers, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_site_travel_actual($site_travel_date,$enquiry_id,$project,$vehicle,$opening_km,$closing_km,$driver,$num_passengers,$remarks,$added_by)
{
	$site_travel_actual_iresult = db_add_site_travel_actual($site_travel_date,$enquiry_id,$project,$vehicle,$opening_km,$closing_km,$driver,$num_passengers,$remarks,$added_by);
		
	if($site_travel_actual_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Site Travel Details have been Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To update site travel plan details
INPUT 	: Travel Plan ID, Status, Cab
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_site_travel_plan($travel_plan_id,$status,$cab,$cab_id)
{
	$site_travel_plan_uresult = db_update_site_travel_plan($travel_plan_id,$status,$cab,$cab_id);
		
	if($site_travel_plan_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Site Travel Details have been Successfully Updated";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To mark an enquiry as unqualified
INPUT 	: Enquiry ID, Remarks, Reason, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_unqualified_enquiry($enquriy_id,$remarks,$reason,$added_by)
{
	$site_travel_plan_uresult = db_add_unqualified_enquiry($enquriy_id,$remarks,$reason,$added_by);
		
	if($site_travel_plan_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Enquiry marked as unqualified!";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get site travel actual
INPUT 	: Travel Actual ID, Date, Enquiry ID, Project, Added By, Start Date, End Date
OUTPUT 	: Actual Site Travel List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_travel_actual_list($travel_actual_id,$travel_date,$enquiry_id,$project,$added_by,$start_date,$end_date)
{
	$site_travel_actual_sresult = db_get_site_travel_actual($travel_actual_id,$travel_date,$enquiry_id,$project,$added_by,$start_date,$end_date);
	
	if($site_travel_actual_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_travel_actual_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site travel added!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get unqualified leads
INPUT 	: Unqualified Enquiry ID, Enquiry ID, Reason, Enquiry Added By, Added By, Start Date, End Date, Enquiry Start Date, Enquiry End Date, Enquiry No, Cell No, STM, Project Mapped User
OUTPUT 	: Unqualified Lead List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_unqualified_leads($unq_enq_id,$enquiry_id,$reason,$enq_added_by,$added_by,$start_date,$end_date,$enq_start_date="",$enq_end_date="",$enquiry_no='',$cell_no='',$assigned_to='',$project_user='')
{
	$unq_lead_sresult = db_get_unqualified_leads($unq_enq_id,$enquiry_id,$reason,$enq_added_by,$added_by,$start_date,$end_date,$enq_start_date,$enq_end_date,$enquiry_no,$cell_no,'1',$assigned_to,$project_user);
	
	if($unq_lead_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $unq_lead_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No unqualified leads!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add a prospective client profile
INPUT 	: Enquiry No, Occupation, Other Income, No. of Dependents, Annual Income, Loan Types, EMI, Is Rejected for Loan before, Current living, Current Rent, Buying Interest, Funding Source, Loan Eligibility Amount, Loan Tenure, Duration, Tentative Closure Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_prospective_profile($enq_no,$occupation,$other_income,$num_dependents,$annual_income,$loan_types,$emi,$rejected,$current_living,$rent,$buying_interest,$funding_source,$loan_eligibility,$loan_tenure,$duration,$tentative_closure_date,$remarks,$added_by)
{
	$prospective_profile_iresult = db_add_prospective_client($enq_no,$occupation,$other_income,$num_dependents,$annual_income,$loan_types,$emi,$rejected,$current_living,$rent,$buying_interest,$funding_source,$loan_eligibility,$loan_tenure,$duration,$tentative_closure_date,$remarks,$added_by);
		
	if($prospective_profile_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Prospective Client Profile Added!";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get prospective clients
INPUT 	: Profile ID, Enquiry ID, Added By, Start Date, End Date, Project Mapped User
OUTPUT 	: Unqualified Lead List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_prospective_clients($profile_id,$enquiry_id,$added_by,$start_date,$end_date,$project_user='')
{
	$unq_lead_sresult = db_get_prospective_client_list($profile_id,$enquiry_id,$added_by,$start_date,$end_date,$project_user);
	
	if($unq_lead_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $unq_lead_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No unqualified leads!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get latest enquiry follow up list
INPUT 	: Enquiry ID, Follow Up Date, Status, Status Operation, Assigned To, Added By, Order, Relationship, Fup Date Start, Fup Date End, Source, Project Mapped User, Status Filter Negation, Is Booked, Is unqualified, Start, Count, Mobile No
OUTPUT 	: Enquiry Follow Up List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_enquiry_fup_latest($enq_id,$follow_up_date,$status,$status_operation,$assigned_to,$added_by,$start_date,$end_date,$order,$relationship,$follow_up_date_start,$follow_up_date_end,$source='',$project_user='',$status_filter_out='',$is_booked='',$is_unqualified='',$start='',$limit='',$mobile='')
{
	$enquiry_fup_latest_sresult = db_get_enquiry_fup_latest($enq_id,$follow_up_date,$status,$status_operation,$assigned_to,$added_by,$start_date,$end_date,$order,$relationship,$follow_up_date_start,$follow_up_date_end,$source,$project_user,$status_filter_out,$is_booked,$is_unqualified,$start,$limit,$mobile);
	
	if($enquiry_fup_latest_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $enquiry_fup_latest_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No follow up for the given date!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update enquiry details
INPUT 	: Enquiry ID, Name, Cell, Email, Company, Location, Source, Walk In, User
OUTPUT 	: Enquiry ID or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_edit_enquiry($enquiry_id,$name,$cell,$email,$company,$location,$source,$walk_in,$user)
{
	$enquiry_uresult = db_update_enquiry($enquiry_id,$name,$cell,$email,$company,$location,$source,$walk_in);
	
	if($enquiry_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $enquiry_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get enquiries with site visit completed
INPUT 	: Added By, Start Date, End Date, Project Mapped User
OUTPUT 	: Enquiry List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_visit_completed_enquiries($added_by,$start_date,$end_date,$project_user='')
{
	$sv_completed_sresult = db_get_site_travel_plan_completed($added_by,$start_date,$end_date,$project_user);
	
	if($sv_completed_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $sv_completed_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No enquiries!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update prospective client profile
INPUT 	: Profile ID, Closure Date
OUTPUT 	: Profile ID or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_prospective_client($profile_id,$closure_date)
{
	$profile_uresult = db_update_prospective_profile($profile_id,$closure_date);
	
	if($profile_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $profile_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

function i_update_site_visit_plan($enquiry_id,$status)
{
	$site_visit_plan_uresult = db_update_site_visit_plan($enquiry_id,$status);
	
	if($site_visit_plan_uresult["status"] == SUCCESS)
	{
		$return["data"]   = "Site Visit PLan updated successfully";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "There was an internal error. Please try again later!";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To get distinct enquiry list
INPUT 	: Added by, Assigned To, Followup Start Date, Followup End Date, Added on Start Date, Added On End Date, Order of sorting, Record Starting Point, Record Count
OUTPUT 	: Distinct Enquiry List; Success or Failure message
BY 		: Nitin Kashyap
*/
function i_get_distinct_enquiry_list($added_by,$assigned_to,$follow_up_start_date,$follow_up_end_date,$start_date,$end_date,$order,$limit_start,$limit_count)
{
	$distinct_enquiry_sresult = db_get_distinct_enquiries($added_by,$assigned_to,$follow_up_start_date,$follow_up_end_date,$start_date,$end_date,$order,$limit_start,$limit_count);
	
	if($distinct_enquiry_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $distinct_enquiry_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No distinct enquiry for this duration!"; 
    }
	
	return $return;
}

function i_update_site_visit($svp_id,$sv_data)
{
	$site_visit_plan_uresult = db_update_site_visit($svp_id,$sv_data);
	
	if($site_visit_plan_uresult["status"] == SUCCESS)
	{
		$return["data"]   = "Site Visit Plan updated successfully";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "There was an internal error. Please try again later!";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To update unqualified enqiry status
INPUT 	: Unqualified Enquiry ID, Status
OUTPUT 	: Unqualified Enquiry ID or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_unqualified_enquiry($unq_enquiry_id,$status)
{
	$unq_enquiry_uresult = db_update_unqualified_enquiry($unq_enquiry_id,$status);
	
	if($unq_enquiry_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $unq_enquiry_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get enquiry count
INPUT 	: Enquiry Filter Array
OUTPUT 	: Number of leads entered into system
BY 		: Nitin Kashyap
*/
function i_get_enquiry_count($enquiry_filter_array)
{
	$enquiry_count_sresult = db_get_enquiries_count($enquiry_filter_array);
	
	if($enquiry_count_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {		
		$return = $enquiry_count_sresult['data'][0]['enq_count'];
    }
    else
    {
	    $return = 0;
    }
	
	return $return;
}

/*
PURPOSE : To get site visit count
INPUT 	: Site Visit Filter Array
OUTPUT 	: Number of leads entered into system
BY 		: Nitin Kashyap
*/
function i_get_sv_count($sv_filter_array)
{
	$sv_count_sresult = db_get_sv_count($sv_filter_array);
	
	if($sv_count_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {		
		$return = $sv_count_sresult['data'][0]['sv_count'];
    }
    else
    {
	    $return = 0;
    }
	
	return $return;
}

/*
PURPOSE : To get enquiry fup count
INPUT 	: FUP Filter Array
OUTPUT 	: Number of follow up
BY 		: Nitin Kashyap
*/
function i_get_enquiry_fup_count($fup_filter_array)
{
	$fup_count_sresult = db_get_enquiry_fup_count($fup_filter_array);
	
	if($fup_count_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {		
		$return = $fup_count_sresult['data'][0]['fup_count'];
    }
    else
    {
	    $return = 0;
    }
	
	return $return;
}

/*
PURPOSE : To get count of prospective profiles
INPUT 	: Sales User
OUTPUT 	: Count of prospective profiles
BY 		: Nitin Kashyap
*/
function i_get_prospective_count($prospective_filter_array)
{
	$prospective_count = 0;
	$prospective_client_list = i_get_prospective_clients('','',$prospective_filter_array['added_by'],'','');	
	if($prospective_client_list["status"] == SUCCESS)
	{
		$prospective_client_list_data = $prospective_client_list["data"];
		
		for($count = 0; $count < count($prospective_client_list_data); $count++)
		{
			// Check if there is an active booking for this customer
			$cp_is_booking_exists = i_get_site_booking('','','',$prospective_client_list_data[$count]["crm_prospective_profile_id"],'','1','','','','','','','','');
			
			if(($cp_is_booking_exists["status"] == SUCCESS) && (($cp_is_booking_exists["data"][0]["crm_booking_status"] == "0") || ($cp_is_booking_exists["data"][0]["crm_booking_status"] == "1")))
			{
				// Already booked
			}
			else
			{
				// Is the enquiry unqualified
				$unqualified_data = i_get_unqualified_leads('',$prospective_client_list_data[$count]["enquiry_id"],'','','','','');
				if($unqualified_data["status"] == SUCCESS)
				{
					// Enquiry is unqualified
				}
				else
				{
					$prospective_count++;
				}
			}
		}		
	}
	else
	{
		$prospective_count = 0;
	}
	
	return $prospective_count;
}
?>
