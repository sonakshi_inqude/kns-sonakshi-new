<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing
	
	$project_object_output_search_data = array("active"=>'1');
	$project_object_output_master_list = i_get_project_object_output_task($project_object_output_search_data);
	if($project_object_output_master_list["status"] == SUCCESS)
	{
		$project_object_output_master_list_data = $project_object_output_master_list["data"];
		for($object_count = 0 ; $object_count < count($project_object_output_master_list_data); $object_count++)
		{
			$result[$object_count] = $project_object_output_master_list_data[$object_count]["project_object_output_object_type"];
		}
	}
	echo json_encode($result);
	//echo (json_encode($result));
}
else
{
	header("location:login.php");
}	