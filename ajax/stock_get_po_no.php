<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$search  = $_POST["search"];
	$stock_purchase_order_search_data = array("order_number"=>$search,"order_number_check"=>'2');
	$po_list_result =  i_get_stock_purchase_order_list($stock_purchase_order_search_data);
	if($po_list_result["status"] == SUCCESS)
	{		
		$po_list = '';

		for($count = 0; $count < count($po_list_result['data']); $count++)
		{
			$po_list = $po_list.'<a style="display:block; width:100%; border-bottom:1px solid #efefef; text-decoration:none; cursor:pointer; padding:5px;" onclick="return select_po_no(\''.$po_list_result['data'][$count]['stock_purchase_order_id'].'\',\''.$po_list_result['data'][$count]['stock_purchase_order_number'].'\');">'.$po_list_result['data'][$count]['stock_purchase_order_number'].'</a>';
		}
	}
	else
	{
		$po_list = 'FAILURE';
	}
	
	echo $po_list;
}
else
{
	header("location:login.php");
}
?>