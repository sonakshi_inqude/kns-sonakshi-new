<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['user_id'])) {
        $user_id = $_REQUEST['user_id'];
    } else {
        $user_id = "";
    }
    if (isset($_REQUEST['start_date'])) {
        $start_date = $_REQUEST['start_date'];
    } else {
        $start_date = "";
    }
    if (isset($_REQUEST['end_date'])) {
        $end_date = $_REQUEST['end_date'];
    } else {
        $end_date = "";
    }

    $user_list = db_get_mobile_deduction($user_id, $start_date, $end_date);
    if ($user_list['status'] == SUCCESS) {
        $user_list_data = $user_list['data'];
    }

    echo json_encode($user_list_data);
} else {
    header("location:login.php");
}
