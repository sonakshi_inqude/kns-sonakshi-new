<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$payment_id  = $_GET["payment_id"];
	$issued_amount = 0;
	$deduction = 0;
	$project_man_power_issue_payment_search_data = array("active"=>'1',"man_power_id"=>$payment_id);
	$project_man_power_issue_payment_list = i_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data);
	if($project_man_power_issue_payment_list["status"] == SUCCESS)
	{
		$project_actual_manpower_payment_issue_list_data = $project_man_power_issue_payment_list["data"];
		for($issue_count = 0 ; $issue_count < count($project_actual_manpower_payment_issue_list_data) ; $issue_count++)
		{
			$issued_amount = $issued_amount + $project_actual_manpower_payment_issue_list_data[$issue_count]["project_man_power_issue_payment_amount"];
			$deduction = $deduction + $project_actual_manpower_payment_issue_list_data[$issue_count]["project_man_power_issue_payment_deduction"];
		}
		$output = array("instrument_details"=>$project_actual_manpower_payment_issue_list_data[0]["project_man_power_issue_payment_instrument_details"],
	 "remarks"=>$project_actual_manpower_payment_issue_list_data[0]["project_man_power_issue_payment_remarks"],
	 "payment_added_on"=>$project_actual_manpower_payment_issue_list_data[0]["project_man_power_issue_payment_added_on"],
	 "payment_mode"=>$project_actual_manpower_payment_issue_list_data[0]["payment_mode_name"],"deduction"=>$deduction,
	 "payment_added_by"=>$project_actual_manpower_payment_issue_list_data[0]["user_name"],"issued_amount"=>$issued_amount);
	}
	else
	{
		$issued_amount = 0;
		$deduction = 0;
		$output = array("issued_amount"=>$issued_amount,"deduction"=>$deduction);
	}
  echo json_encode($output);
}
else
{
	header("location:login.php");
}
?>
