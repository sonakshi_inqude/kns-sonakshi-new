<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$order_id    = $_POST["po_id"];
	$status       = 'Approved';
	$approved_on  = date("Y-m-d H:i:s");
	$approved_by  = $user;
	
	// Update PO
	$purchase_order_update_data = array('status'=>$status);
	$po_approve_uresult = i_update_purchase_order($order_id,$purchase_order_update_data);
	
	if($po_approve_uresult['status'] == SUCCESS)
	{
		// Update PO items
		$purchase_order_items_update_data = array("status"=>$status,"approved_on"=>$approved_on,"approved_by"=>$approved_by);
		$approve_files_result = i_update_purchase_order_items('',$order_id,$purchase_order_items_update_data);
		
		if($approve_files_result["status"] == FAILURE)
		{
			echo $approve_files_result["data"];
		}
		else
		{
			echo "SUCCESS";
		}
	}
	else
	{
		echo $po_approve_uresult['data'];
	}
}
else
{
	header("location:login.php");
}
?>