<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$task_id      = $_REQUEST["task_id"];
	$road_id      = $_REQUEST["road_id"];

	$total_planned_mp_cost = 0 ;
	$total_planned_mc_cost = 0 ;
	$total_planned_cw_cost = 0 ;
	$total_planned_material_cost = 0 ;
	$project_planned_data = array("task_id"=>$task_id,"road_id"=>$road_id);
	$get_planned_data = db_get_planned_data($project_planned_data);
	if($get_planned_data["status"] == DB_RECORD_ALREADY_EXISTS)
	{
			for($plan_count = 0 ; $plan_count < count($get_planned_data["data"]) ; $plan_count++)
			{
				$total_planned_mp_cost = $total_planned_mp_cost + $get_planned_data["data"][$plan_count]["planned_manpower"];
				$total_planned_mc_cost = $total_planned_mc_cost + $get_planned_data["data"][$plan_count]["planned_machine"];
				$total_planned_cw_cost = $total_planned_cw_cost + $get_planned_data["data"][$plan_count]["planned_contract"];
				$total_planned_material_cost = $total_planned_material_cost + $get_planned_data["data"][$plan_count]["planned_material"];
			}
	}
	else {
		$total_actual_mp_cost = "";
	}

	//Actuals budget amount
	//Manpowert
	$total_actual_mp_cost = 0 ;
	$project_budget_manpower_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>"1");
	$budget_manpower_list =  db_get_project_budget_manpower($project_budget_manpower_search_data);
	if($budget_manpower_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
			$total_actual_mp_cost = $total_actual_mp_cost + $budget_manpower_list["data"][0]["total_sum"];
	}
	else {
		$total_actual_mp_cost = "";
	}

	//Machine
	$total_actual_mc_cost = 0 ;
	$project_budget_machine_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>"1");
	$budget_machine_list =  db_get_budget_machine($project_budget_machine_search_data);
	if($budget_machine_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
			$budget_machine_list_data = $budget_machine_list["data"];
			for($mp_count = 0 ; $mp_count < count($budget_machine_list["data"]) ; $mp_count++)
			{
				$machine_rate = $budget_machine_list_data[$mp_count]['fuel_charge'];
				if($budget_machine_list_data[$mp_count]["machine_end_date"] != "0000-00-00 00:00:00")
				{
					$start_date_time = strtotime($budget_machine_list_data[$mp_count]["machine_start_date"]);
					$end_date_time = strtotime($budget_machine_list_data[$mp_count]["machine_end_date"]);
					$date_diff = $end_date_time - $start_date_time;
					$no_hrs_worked = $date_diff/3600;
				}
				else
				{
					$start_date_time = strtotime($budget_machine_list_data[$mp_count]["machine_start_date"]);
					$end_date_time = strtotime(date("Y-m-d H:i:s"));
					$date_diff = $end_date_time - $start_date_time;
					$no_hrs_worked = $date_diff/3600;
				}
				// Off time related calculation
				$off_time = $budget_machine_list_data[$mp_count]["off_time"]/60;
				$eff_hrs = $no_hrs_worked - $off_time;
				$actual_mc_cost = ($machine_rate * $eff_hrs) + $budget_machine_list_data[$mp_count]["machine_cost"];
				$total_actual_mc_cost = $total_actual_mc_cost + $actual_mc_cost ;
			}

	}
	else {
			$total_actual_mc_cost = 0;
	}

	//Contract
	$total_actual_cw_cost = 0 ;
	$project_budget_contract_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>"1");
	$budget_cw_list =  db_get_project_budget_contract($project_budget_contract_search_data);
	if($budget_cw_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$total_actual_cw_cost = $total_actual_cw_cost + $budget_cw_list["data"][0]["total_sum"];
	}
	else {
		$total_actual_cw_cost = 0;
	}

	//Get Material actuals
	$total_actual_material_cost = 0 ;
	$project_budget_planned_matreial_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>"1");
	$budget_material_list = db_get_project_budget_planned_material($project_budget_planned_matreial_search_data);
	if($budget_material_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
			for($material_count = 0 ; $material_count < count($budget_material_list["data"]) ; $material_count++)
			{
				$total_actual_material_cost = $total_actual_material_cost + $budget_material_list["data"][$material_count]["total_amt"];
			}
	}
	else {
		$total_actual_material_cost = 0;
	}
	$actual_data = array() ;
	 $planned_data = array("planned_mp"=>round($total_planned_mp_cost),
	 											 "planned_mc"=>round($total_planned_mc_cost),
											 	 "planned_cw"=>round($total_planned_cw_cost),
										 		 "planned_material"=>round($total_planned_material_cost),
												 "total_mp_cost"=>round($total_actual_mp_cost),
									       "total_mc_cost"=>round($total_actual_mc_cost),
												 "total_actual_cw_cost"=>round($total_actual_cw_cost),
												 "total_actual_material_cost"=>round($total_actual_material_cost)) ;
	echo json_encode($planned_data);
}
else
{
	header("location:login.php");
}
?>
