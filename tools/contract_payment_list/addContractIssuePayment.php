<?php
$base = $_SERVER['DOCUMENT_ROOT'];
session_start();
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

$user = $_SESSION["loggedin_user"];
$payment_manpower_id  = $_POST["hd_payment_id"];
$amount               = $_POST["amount"];
$balance_amount       = $_POST["hd_balance_amount"];
$vendor_id            = $_POST["hd_vendor_id"];
$payment_mode         = $_POST["ddl_mode"];
$instrument_details   = $_POST["txt_details"];
$remarks               = $_POST["txt_remarks"];

$search_vendor  = $vendor_id;

    // Get Project  Payment Ctract modes already added


	$project_actual_contract_payment_search_data = array("active"=>'1',"status"=>"Approved","secondary_status"=>"Accepted","vendor_id"=>$search_vendor);
	$project_actual_contract_payment_list = i_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
	if($project_actual_contract_payment_list['status'] == SUCCESS)
	{
		$project_actual_contract_payment_list_data = $project_actual_contract_payment_list['data'];
	}


	if($project_actual_contract_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_issued_amount = 0;
					$total_deduction = 0;
					$total_balance = 0;
					$total_amount = 0;
					for($count = 0; $count < count($project_actual_contract_payment_list_data); $count++)
					{
						$sl_no++;
						//Get Delay
						$start_date = date("Y-m-d");
						$end_date = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_approved_on"];
						$delay = get_date_diff($end_date,$start_date);

						//Get total amount
						$amount1 = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_amount"];

						//Get security deposit
						$security_deposit = $project_actual_contract_payment_list_data[$count]["project_actual_contract_deposit_amount"];

            $contract_tds = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_tds"];
            $tds_amount = ($contract_tds/100) * $amount1;
            $amount1 = $amount1 - $tds_amount;
						//Get Project Machine Vendor master List
						$issued_amount = 0;
						$deduction = 0;
						$project_contract_issue_payment_search_data = array("active"=>'1',"contract_id"=>$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]);
						$project_contract_issue_payment_list = i_get_project_contract_issue_payment($project_contract_issue_payment_search_data);
						if($project_contract_issue_payment_list["status"] == SUCCESS)
						{
							$project_contract_issue_payment_list_data = $project_contract_issue_payment_list["data"];
							for($issue_count = 0 ; $issue_count < count($project_contract_issue_payment_list_data) ; $issue_count++)
							{
								$issued_amount = $issued_amount + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_amount"];
								$deduction = $deduction + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_deduction"];
							}
						}
						else
						{
							$issued_amount = 0;
							$deduction = 0;
						}
						$balance_amount = round(($amount1 - $issued_amount),2);

						$final_payment[$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]] = $balance_amount ;

					}
				}


foreach($final_payment as $key => $value){
    if($value > 0 && $amount > 0){
        if($amount >= $value){
            $project_contract_issue_payment_iresult = i_add_project_contract_issue_payment($key,$value,'',$vendor_id,$payment_mode,$instrument_details,$remarks,$user);
            $project_actual_payment_manpower_update_data = array("status"=>'Payment Issued');
            db_update_project_actual_contract_payment($key,$project_actual_payment_manpower_update_data);
            $amount = $amount - $value;
        }else{
           $project_contract_issue_payment_iresult = i_add_project_contract_issue_payment($key,$amount,'',$vendor_id,$payment_mode,$instrument_details,$remarks,$user);
           $amount = $amount - $amount;
        }
    }
}

      $project_actual_payment_contract_update_data = array("status"=>'Payment Issued');
      echo json_encode($project_actual_payment_contract_update_data);

?>
