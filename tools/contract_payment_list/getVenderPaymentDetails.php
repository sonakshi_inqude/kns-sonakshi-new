
<?php


/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_actual_contract_payment_list.php
CREATED ON	: 09-May-2017
CREATED BY	: Ashwini
PURPOSE     : List of project for actual contract payment
*/

/*
TBD:
*/

/* DEFINES - START */
define('PROJECT_CONTRACT_PROJECT_ACCEPT_PAYMENT_FUNC_ID','268');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_ACCEPT_PAYMENT_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_ACCEPT_PAYMENT_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_ACCEPT_PAYMENT_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_ACCEPT_PAYMENT_FUNC_ID,'1','1');
	$approve_perms_list = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_ACCEPT_PAYMENT_FUNC_ID,'6','1');

	// Query String Data
	// Nothing

	$search_vendor  = "";
	$search_project = "";

	$search_vendor  = $_GET["search_vendor"];

	// Get Project Actual Contract Payment modes already added
	$project_actual_contract_payment_search_data = array("active"=>'1',"vendor_id"=>$search_vendor);
	$project_actual_contract_payment_list = i_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
	if($project_actual_contract_payment_list['status'] == SUCCESS)
	{
		$project_actual_contract_payment_list_data = $project_actual_contract_payment_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_actual_contract_payment_list["data"];
	}

	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}
	 else
	{

	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}
}
else
{
	header("location:login.php");
}


      if($project_actual_contract_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_issued_amount = 0;
					$total_deduction = 0;
					$total_balance = 0;
					$total_amount = 0;
					for($count = 0; $count < count($project_actual_contract_payment_list_data); $count++)
					{
						if($project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_status"]=='Accepted' ||
						$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_status"]=='Approved' ||
						$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_status"]=='Payment Issued'){
						$sl_no++;
						//Get Delay
						$start_date = date("Y-m-d");
						$end_date = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_approved_on"];
						$delay = get_date_diff($end_date,$start_date);

						//Get total amount
						//$amount = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_amount"];

						$amount_before_tds = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_amount"];
						$contract_tds = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_tds"];
						$tds_amount = ($contract_tds/100) * $amount_before_tds;
						$amount = $amount_before_tds - $tds_amount;
						//Get security deposit
						$security_deposit = $project_actual_contract_payment_list_data[$count]["project_actual_contract_deposit_amount"];


						//Get Project Machine Vendor master List
						$issued_amount = 0;
						$deduction = 0;
						$project_contract_issue_payment_search_data = array("active"=>'1',"contract_id"=>$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]);
						$project_contract_issue_payment_list = i_get_project_contract_issue_payment($project_contract_issue_payment_search_data);
						if($project_contract_issue_payment_list["status"] == SUCCESS)
						{
							$project_contract_issue_payment_list_data = $project_contract_issue_payment_list["data"];
							for($issue_count = 0 ; $issue_count < count($project_contract_issue_payment_list_data) ; $issue_count++)
							{
								$issued_amount = $issued_amount + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_amount"];
								$deduction = $deduction + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_deduction"];
                                $total_issued_amount = $total_issued_amount + $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_amount"];
							}
						}
						else
						{
							$issued_amount = 0;
							$deduction = 0;
						}
						$balance_amount = round(($amount - $issued_amount),2);

						if($balance_amount != 0)
						{
							// Get Project details
							$project_payment_contract_mapping_search_data = array('payment_id'=>$project_actual_contract_payment_list_data[$count]['project_actual_contract_payment_id']);
							$pay_cont_mapping_sresult = i_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data);
							if($pay_cont_mapping_sresult['status'] == SUCCESS)
							{
								$project_id   = $pay_cont_mapping_sresult['data'][0]['project_plan_project_id'];
								$project_name = $pay_cont_mapping_sresult['data'][0]['project_master_name'];
							}
							else
							{
								$project_id   = '-1';
								$project_name = 'NOT VALID';
							}

							if(($search_project == $project_id) || ($search_project == ""))
							{
								$total_issued_amount = $total_issued_amount;
								$total_deduction  = $total_deduction + $deduction;
								$total_balance  = $total_balance + $balance_amount;
								$total_amount = $total_amount + $amount;

							}
						}
						 $vendor = $project_actual_contract_payment_list_data[$count]["project_manpower_agency_name"];
					 }
					}

				}


    ?>
                </tbody>
              </table>



                  <div class="widget ">

                      <div class="widget-header">
                          <i class="icon-user"></i>
                          <h3>Vendor : <?php echo $vendor ; ?>&nbsp;&nbsp;&nbsp;&nbsp;Total Amount : <?php echo $total_amount ; ?>&nbsp;&nbsp;&nbsp;&nbsp;Total Issued Amount : <?php echo $total_issued_amount; ?>&nbsp;&nbsp;&nbsp;&nbsp;Balance: <?php echo $total_balance; ?> </h3>
                      </div> <!-- /widget-header -->

											<div  style="margin-bottom: 14px;" class="widget-content">

									      <div class="tabbable">

									        <div class="tab-content">
									          <div class="tab-pane active" id="formcontrols">
									            <form id="add_project_actual_contract_issue_payment_frm" class="form-horizontal" method="post">
									              <input type="hidden" value="<?php echo $payment_manpower_id; ?>" name="hd_payment_id" />
									              <input type="hidden" value="<?php echo $search_vendor; ?>" name="hd_vendor_id" />
									              <input type="hidden" value="<?php echo $total_balance; ?>" name="hd_balance_amount" />
									              <div class="row">
									                <div class="col-md-6">
									                  <div style="margin-left:16px;width:60%;" class="form-group">
									                    <label class="control-label" for="amount">Amount</label>
									                    <input type="number" class="form-control" required name="amount" min="0.01" step="0.01" id="amount" onkeyup="return check_validity('<?php echo $total_amount ;?>','<?php echo $total_issued_amount ;?>');">
									                  </div>
									                  <!-- /control-group -->
									                </div>
									                <div class="col-md-6">
									                  <div style="margin-left:16px;width:60%;" class="form-group">
									                    <label class="control-label" for="ddl_mode">Payment Mode*</label>
									                    <select class="form-control" name="ddl_mode" required="required">
									                        <option value="1">Online-NEFT</option>
									                        <option value="2">Cheque</option>
									                        <option value="3">Demand Draft</option>
									                        <option value="4">Cash</option>
									                        <option value="5">Other</option>
									                        <option value="6">TDS (via challan)</option>
									                        <option value="7">Online-RTGS</option>
									                   </select>
									                  </div>
									                  <!-- /control-group -->
									                </div>
									              </div>

									              <div class="row">
									                <div class="col-md-6">
									                  <div style="margin-left:16px;width:60%;" class="form-group">
									                    <label class="control-label" for="instrument_details">Instrument Details</label>
									                    <input type="text" class="form-control" required name="txt_details">
									                  </div>
									                </div>

									                <div class="col-md-6">
									                  <div style="margin-left:16px;width:60%;" class="form-group">
									                    <label class="control-label" for="txt_remarks">Remarks</label>
									                    <input type="text" class="form-control" required name="txt_remarks">
									                  </div>
									                </div>
									              </div>
																<?php
																if($approve_perms_list['status']==SUCCESS) {
																 ?>
																	<div class="form-actions">
																		<input type="submit" class="btn btn-primary pull-right" value="Pay Now" />
																	</div>
																<?php } ?>
									                <!-- /form-actions -->
									            </form>
									          </div>
									        </div>

									      </div>
									      <!-- /widget-content -->

                </div> <!-- /widget -->

               <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Project</th>
                    <th>Bill NO</th>
                    <th>Vendor Name</th>
                    <th>Amount</th>
                    <th>Mode</th>
                    <th>Instrument</th>
                    <th>Remarks</th>
                    <th>Date</th>
                    <th>By</th>
                </tr>
                </thead>
                <tbody>
                <?php


				if($project_actual_contract_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_issued_amount = 0;
					$total_deduction = 0;
					$total_balance = 0;
					$total_amount = 0;
					for($count = 0; $count < count($project_actual_contract_payment_list_data); $count++)
					{
						$sl_no++;
						//Get Delay
						$start_date = date("Y-m-d");
						$end_date = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_approved_on"];
						$delay = get_date_diff($end_date,$start_date);

						//Get total amount
						$amount = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_amount"];

						//Get security deposit
						$security_deposit = $project_actual_contract_payment_list_data[$count]["project_actual_contract_deposit_amount"];


						//Get Project Machine Vendor master List
						$issued_amount = 0;
						$deduction = 0;
						$project_contract_issue_payment_search_data = array("active"=>'1',"contract_id"=>$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]);
						$project_contract_issue_payment_list = i_get_project_contract_issue_payment($project_contract_issue_payment_search_data);
						if($project_contract_issue_payment_list["status"] == SUCCESS)
						{
							$project_contract_issue_payment_list_data = $project_contract_issue_payment_list["data"];
							for($issue_count = 0 ; $issue_count < count($project_contract_issue_payment_list_data) ; $issue_count++)
							{

                            $user_list = i_get_user_list($project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_added_by"],'','','','');


                            if($user_list["status"] == SUCCESS)
                            {
                                $user_list_data = $user_list["data"];
                            }

                            // Get Project details
                            $project_payment_contract_mapping_search_data = array('payment_id'=>$project_actual_contract_payment_list_data[$count]['project_actual_contract_payment_id']);
                            $pay_cont_mapping_sresult = i_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data);
                            if($pay_cont_mapping_sresult['status'] == SUCCESS)
                            {
                                $project_id   = $pay_cont_mapping_sresult['data'][0]['project_plan_project_id'];
                                $project_name = $pay_cont_mapping_sresult['data'][0]['project_master_name'];
                            }
                            else
                            {
                                $project_id   = '-1';
                                $project_name = 'NOT VALID';
                            }
								            ?>
                            <tr>
														<td>
																<?php echo $project_name;?>
														</td>
														<td>
															<?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_bill_no"] ?>
														</td>
                            <td>
															 <?php echo $project_actual_contract_payment_list_data[$count]["project_manpower_agency_name"]?>
														</td>
                            <td><?php echo $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_amount"]?></td>
                            <td><?php echo $project_contract_issue_payment_list_data[$issue_count]["payment_mode_name"]?></td>
                            <td><?php echo $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_instrument_details"]?></td>
                            <td><?php echo $project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_remarks"]?></td>
                            <td><?php echo date('Y-M-d',strtotime($project_contract_issue_payment_list_data[$issue_count]["project_contract_issue_payment_added_on"]))?></td>
                            <td>
															<?php echo $user_list_data[0]['user_name']?>
														</td>

                            </tr>
                                <?php
							}
						}

						$balance_amount = round(($amount - $issued_amount),2);
					}
				}
    ?>
  </tbody>
  </table>


<script type="text/javascript">
$("input[name*='amount'], input[name*='instrument_details'], input[name*='txt_details'], input[name*='deduction']").on("keypress", function(evt) {
  var keycode = evt.charCode || evt.keyCode;
  if (event.which == 45 || event.which == 189) {
     $(this).val('');
        alert('please enter proper value');
    return false;
  }
});

    $("#add_project_actual_contract_issue_payment_frm").submit(function(e){
        e.preventDefault();
        $('#ajax_loading').show();

        formdata = new FormData($("#add_project_actual_contract_issue_payment_frm")[0]);
             $.ajax({
                     url: "tools/contract_payment_list/addContractIssuePayment.php",
                     data: formdata,
                     processData: false,
                     contentType: false,
                     type: 'POST',
                     dataType:"JSON",
                     success: function(result){
                             location.reload();
                     }
            });
    });
</script>
    <script src="js/base.js"></script>
<script>
function check_validity(total_amount,issued_amount,deduction)
{
	var amount = parseInt(document.getElementById('amount').value);

	var cur_amount = parseInt(amount);
	var total_amount  = parseInt(total_amount);
	var deduction  = parseInt(deduction);
	var issued_amount      = parseInt(issued_amount);
	if((cur_amount + issued_amount + deduction) > total_amount)
	{
		document.getElementById('amount').value = 0;
		alert('Cannot release greater than total value');
	}
}
</script>
