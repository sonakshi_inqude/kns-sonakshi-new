<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 14-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	if(isset($_GET['task_id']))
	{
		$task_id = $_GET['task_id'];
	}
	else
	{
		// Do nothing
	}

	if(isset($_GET['material_id']))
	{
		$material_id = $_GET['material_id'];
	}
	else
	{
		// Do nothing
	}

	// Capture the form data
	if(isset($_POST["add_task_required_item_submit"]))
	{

		$material_id          = $_POST["hd_material_id"];
		$task_id              = $_POST["hd_task_id"];
		$quantity             = $_POST["quantity"];
		$rate                 = $_POST["material_rate"];
		$remarks 	          = $_POST["txt_remarks"];

		// Check for mandatory fields
		if(($material_id != "") && ($task_id != "") && ($quantity != ""))
		{
			$project_task_required_item_iresult = i_add_project_task_required_item($material_id,$task_id,$quantity,$rate,$remarks,$user);

			if($project_task_required_item_iresult["status"] == SUCCESS)

			{
				$alert_type = 1;
			}

			$alert = $project_task_required_item_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	if(isset($_POST['ddl_process']))
	{
		$process_id = $_POST['ddl_process'];

		//Get Project Task Master modes already added
		$project_task_master_search_data = array("active"=>'1',"process"=>$process_id);
		$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
		if($project_task_master_list['status'] == SUCCESS)
		{
			$project_task_master_list_data = $project_task_master_list['data'];
		}
		else
		{
			$alert = $project_task_master_list["data"];
			$alert_type = 0;
		}

	}
	else
	{
		$process_id = "";
	}

	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}

	// Get Stock Material Master modes already added
	$stock_material_search_data = array();
	$stock_material_master_list = i_get_stock_material_master_list($stock_material_search_data);
	if($stock_material_master_list['status'] == SUCCESS)
	{
		$stock_material_master_list_data = $stock_material_master_list["data"];
	}
	else
	{
		$alert = $stock_material_master_list["data"];
		$alert_type = 0;
	}

    // Get Project Task Required Item modes already added
	$project_task_required_item_search_data = array("active"=>'1',"task_id"=>$task_id);
	$project_task_required_item_list = i_get_project_task_required_item($project_task_required_item_search_data);
	if($project_task_required_item_list['status'] == SUCCESS)
	{
		$project_task_required_item_list_data = $project_task_required_item_list['data'];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Add Project Task Required Item</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Project Task Required Item</h3><span style="float:right; padding-right:20px;"><a href="project_task_required_item_list.php">Project Task Required Item List</a></span>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Task Required Item</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_add_task_required_item_form" class="form-horizontal" method="post" action="project_add_task_required_item.php">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
									<fieldset>

									<input type="hidden" name="hd_material_id" id="hd_material_id" value="" />
									<input type="hidden" name="hd_uom_id" id="hd_uom_id" value="" />

										<div class="control-group">
											<label class="control-label" for="skills">Material</label>
											<div class="controls dropdown">
											<span id="material_code" style="display:block"></span>
												<input type="text" name="stxt_material" class="span6" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" />
												<div id="search_results" class=" -content"></div>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="quantity">Quantity</label>
											<div class="controls">
												<input type="number" name="quantity" class="span6" placeholder="Quantity" required="required"><span id="material_uom" style="display:inline"></span>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<input type="hidden" name="material_rate" id="material_rate">
										<div class="control-group">
											<label class="control-label" for="skills">Rate*</label>
											<div class="controls">
											<span id="material_rate_span" style="display:block"></span>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->


										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_task_required_item_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>

							</div>
							<table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
					<th>Process</th>
					<th>Task Name</th>
					<th>Material Name</th>
					<th>Material Code</th>
					<th>UOM</th>
					<th>Quantity</th>
					<th>Item Rate</th>
					<th>Total Value</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_task_required_item_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_task_required_item_list_data); $count++)
					{
						$qty = $project_task_required_item_list_data[$count]["project_task_required_item_quantity"];
						$rate = $project_task_required_item_list_data[$count]["project_task_required_item_rate"];
						$total_value = $qty*$rate;
						$sl_no++;

						//Get Task user ID from Task user mapping
						$project_task_master_search_data = array("task_master_id"=>$project_task_required_item_list_data[$count]["project_task_required_item_task_id"]);
						$task_master_list = i_get_project_task_master($project_task_master_search_data);
						if($task_master_list["status"] == SUCCESS)
						{
							$task_master_list_data = $task_master_list["data"];
							$task_master_id = $task_master_list_data[0]["project_task_master_id"];
							$process_id = $task_master_list_data[0]["project_task_master_process"];
						}
						else
						{
							$task_master_id = '-1';
						}

						//Get Task user ID from Task user mapping
						$project_task_user_mapping_search_data = array("task_id"=>$task_master_id,"active"=>'1');
						$task_user_mapping_list = i_get_project_task_user_mapping($project_task_user_mapping_search_data);
						if($task_user_mapping_list["status"] == SUCCESS)
						{
							$task_user_mapping_list_data = $task_user_mapping_list["data"];
							$task_user_id = $task_user_mapping_list_data[0]["project_task_user_id"];
						}
						else
						{
							$task_user_id = "";
						}
						// Get user
						$user_list = i_get_user_list($task_user_id,'','','');
						if($user_list['status'] == SUCCESS)
						{
							$user_list_data = $user_list['data'];
							$user_name = $user_list_data[0]["user_name"];
						}

						//Get Project plan Process
						$project_plan_process_search_data = array("process_id"=>$process_id,"active"=>'1');
						$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
						if($project_plan_process_list["status"] == SUCCESS)
						{
							$project_plan_process_list_data = $project_plan_process_list["data"];
							$plan_id = $project_plan_process_list_data[0]["project_plan_process_plan_id"];
						}

						//Get project plan
						$project_plan_search_data = array("plan_id"=>$plan_id,"active"=>'1');
						$project_plan_list = i_get_project_plan($project_plan_search_data);
						if($project_plan_list["status"] == SUCCESS)
						{
							$project_plan_list_data = $project_plan_list["data"];
							$project_id = $project_plan_list_data[0]["project_plan_project_id"];
						}

						//Get project Master
						$project_management_master_search_data = array("project_id"=>$project_id,"active"=>'1', "user_id"=>$user);
						$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
						if($project_management_master_list["status"] == SUCCESS)
						{
							$project_management_master_list_data = $project_management_master_list["data"];
							$project_name = $project_management_master_list_data[0]["project_master_name"];
						}

					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["stock_material_name"]; ?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["stock_material_code"]; ?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["stock_unit_name"]; ?></td>
					<td><?php echo $qty ; ?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["project_task_required_item_rate"]; ?></td>
					<td><?php echo $total_value ;?></td>
					<td><?php echo $project_task_required_item_list_data[$count]["project_task_required_item_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_task_required_item_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_required_item_list_data[$count][
					"project_task_required_item_added_on"])); ?></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function get_material_list()
{
	var searchstring = document.getElementById('stxt_material').value;

	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
			}
		}

		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);
	}
	else
	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_material(material_id,material_name,material_code,material_uom)
{
	document.getElementById('hd_material_id').value 	= material_id;
	document.getElementById('stxt_material').value = material_name;

	document.getElementById('search_results').style.display = 'none';
	if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				var object = JSON.parse(xmlhttp.responseText);
					document.getElementById('material_rate').value = object.material_rate;
					document.getElementById('material_rate_span').innerHTML = object.material_rate;
					document.getElementById('material_uom').innerHTML = object.material_uom;
					var name_code = object.material_name.concat('-',object.material_code);
					document.getElementById('material_code').innerHTML = name_code;
					document.getElementById('material_name').value = object.material_name;


			}
		}

		xmlhttp.open("POST", "ajax/stock_get_material_price.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("material_id=" + material_id);
}


</script>


  </body>

</html>
