<?php
session_start();
$_SESSION['module'] = 'PM Masters';

define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '');

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '6', '1');

    $project_id = '';
    if (isset($_GET["project_id"])) {
      $project_id   = $_GET["project_id"];
    }
    $search_requested_by = '';
    if (isset($_GET["requested_by"])) {
      $search_requested_by   = $_GET["requested_by"];
    }

    //Get Project List
  	$stock_project_search_data = array();
  	$project_list = i_get_project_list($stock_project_search_data);
  	if($project_list["status"] == SUCCESS)
  	{
  		$project_list_data = $project_list["data"];
  	}
  	else
  	{
  		$alert = $project_list["data"];
  		$alert_type = 0;
  	}

    // Get User List
    $user_list = i_get_user_list('','','','','1');
    if($user_list["status"] == SUCCESS)
    {
      $user_list_data = $user_list["data"];
    }
} else {
    header("location:login.php");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Stock_Issued List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
	  <script src="datatable/project_actual_material.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
  </head>
  <body>
  <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>
  <div class="main margin-top">
    <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="widget widget-table action-table">
            <div class="widget-header">
              <h3>Stock Issued List</h3>
            </div>

            <div class="widget-header widget-toolbar">
              <form method="get" class="form-inline">
                <select name="project_id" id="project_id" class="form-control">
                 <option value="">- - Select Project - -</option>
              <?php
                for ($project_count = 0; $project_count < count($project_list_data); $project_count++) { ?>
                 <option value="<?php echo $project_list_data[$project_count]["stock_project_id"]; ?>"
                   <?php if ($project_id == $project_list_data[$project_count]["stock_project_id"]) {
                    ?> selected="selected" <?php } ?>>
                    <?php echo $project_list_data[$project_count]["stock_project_name"]; ?>
                 </option>
              <?php } ?>
              </select>
              <select name="requested_by" id="requested_by" class="form-control">
    				  <option value="">- - Select Indent By - -</option>
    				  <?php
    				  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
    				  {
    				  ?>
    				  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_requested_by == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
    				  <?php
    				  }
    				  ?>
      				  </select>
              <input type="submit" class="btn btn-primary" />
              </form>
            </div>
          </div>
            <div class="widget-content">
             <table class="table table-striped table-bordered display nowrap" id="example">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Project</th>
                   <th>Process</th>
                   <th>Task</th>
                   <th>Road</th>
                   <th>Indent No</th>
                   <th>Indent Date</th>
                   <th>Indent By</th>
                   <th>Issued By</th>
                   <th>Material Name</th>
                   <th>Material Code</th>
                   <th>UOM</th>
                   <th>Issued Qty</th>
                   <th>Indent Qty</th>
                   <th>Item Rate</th>
                   <th>Total Value</th>
                   <th>Machine</th>
                   <th>Machine Number</th>
                   <th>Issue No</th>
                   <th>Issued Date</th>
                   <th>Action</th>
                </tr>
             </thead>
               </tbody>
             </table>
            </div>
            <!-- widget-content -->
            </div>
          </div>
        </div>
      </div>
  </body>
</html>
