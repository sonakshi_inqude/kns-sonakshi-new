<?php

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

// What is the date today
$start_date = date("Y-m-d 00:00:00");
$end_date = date("Y-m-d 23:59:59");
// Get list of approved bookings with no profile
$max_wait_days = 30;
// Get Project Task BOQ modes already added

// Project data
$project_management_master_search_data = array("active"=>'1',"status"=>"all");
$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
if($project_management_master_list["status"] == SUCCESS)
{
	$project_management_master_list_data = $project_management_master_list["data"];
}

	for($pcount = 0 ; $pcount < count($project_management_master_list_data) ; $pcount++)
	{
		$sl_no = 0;
		$project_email_search_data = array("project_id"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$email_list = db_get_project_email_list($project_email_search_data);
		if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
				$email_list_data = $email_list["data"];
				$to  = $email_list_data[0]["project_email_details_to"];
				$cc  = $email_list_data[0]["project_email_details_cc"];
				$bcc = $email_list_data[0]["project_email_details_bcc"];
		}
		//Contract Data
		$project_task_boq_actual_search_data = array("active"=>'1',"boq_start_date"=>$start_date,"boq_end_date"=>$end_date,"project"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
		if($project_task_boq_actual_list['status'] == SUCCESS)
		{
			$total_amount = 0 ;
			$message_heading = "" ;
			$message_content = "" ;
			$heading = "" ;
			// $message_end = "" ;
			$project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
			$subject = $project_management_master_list_data[$pcount]["project_master_name"].'  '.'Contract Work List';
			$message = 'Dear Sir/Madam,<br><br>Daily Contract work list:<br><br>';

			$message_heading .="<table border='1' style='border-collapse:collapse; border-width:2px;'>";
				// Header row - start
			$message_heading .= "<tr style='border-width:2px;text-align:center;'>
			<td style='border-width:2px;text-align:center;'><strong>SL No.</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Agency</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Process</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Uom</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>WorkType</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Road</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Contract Task</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Location</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Total Measurement</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Total</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Remarks</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Added By</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Added On</strong></td>";

			$message_heading = $message_heading.'</tr>';
			for($count = 0; $count < count($project_task_boq_actual_list_data); $count++)
			{
				$sl_no++;
				// Compose the message
				$project	  = $project_task_boq_actual_list_data[$count]["project_master_name"];
				$process  	    = $project_task_boq_actual_list_data[$count]["project_process_master_name"];
				$task	  	  = $project_task_boq_actual_list_data[$count]["project_task_master_name"];
				$road	  	  = $project_task_boq_actual_list_data[$count]["project_site_location_mapping_master_name"];
				$vendor	  = $project_task_boq_actual_list_data[$count]["project_manpower_agency_name"];
				$contract_process	  		      = $project_task_boq_actual_list_data[$count]["project_contract_process_name"];
				$contract_task	  	  = $project_task_boq_actual_list_data[$count]["project_contract_rate_master_work_task"];
				if($project_task_boq_actual_list_data[$count]["project_task_actual_boq_road_id"] == "No Roads")
				{
					$location = "No Roads";
				}
				else {
					$location	 = $project_task_boq_actual_list_data[$count]["location_name"];
				}
				$work_type		  	    = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_work_type"];
				$uom	  	  = $project_task_boq_actual_list_data[$count]["stock_unit_name"];
				$measurment	  	  = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_total_measurement"];
				$rate	  	  = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_amount"];
				$amount = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_lumpsum"];
				$remarks = $project_task_boq_actual_list_data[$count]["project_task_actual_boq_remarks"];
				$user	  	  = $project_task_boq_actual_list_data[$count]["added_by"];
				$added_on	  	  = date("d-M-Y",strtotime($project_task_boq_actual_list_data[$count]["project_task_actual_boq_added_on"]));

				$total_amount += $amount ;
				if($work_type == "Rework")
				{
						$css = "yellow";
				}
				else {
					$css = "";
				}

				$message_content .= "<tr style='border-width:2px; text-align:center;'>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$sl_no."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$vendor."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$process."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$uom."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$work_type."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$road."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$contract_task."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$location."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$measurment."</td>
				<td style='border-width:2px;text-align:center;background-color:$css;'>".$amount."</td>
				<td style='border-width:2px;text-align:center;background-color:$css;'>".$remarks."</td>
				<td style='border-width:2px;text-align:center;background-color:$css;'>".$user."</td>
				<td style='border-width:2px;text-align:center;background-color:$css;'>".$added_on."</td>
				</tr>";
			}
			$heading = $heading."<br>
			<table style='border: 1px solid black;'>
			<tr>
				<td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Project</td>
				<td style='border: 1px solid #ddd;width:20%'>".$project_management_master_list_data[$pcount]["project_master_name"] ."</td>
				<td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Date</td>
				<td style='border: 1px solid #ddd;width:20%'>".date("d-M-Y")."</td>
			</tr>
			<tr>
				<td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Total Value</td>
				<td style='border: 1px solid #ddd;width:20%'>".$total_amount."</span></td>
				<td style='color:#00ba8b;border: 1px solid #ddd;width:20%'>&nbsp;</td>
				<td style='border: 1px solid #ddd;width:20%'>&nbsp;</td>
			</tr>
			</table><br>";
			$message_end = "</table>";
			$message_end = $message_end.'<br>Regards,<br>KNS ERP';

			$message .= $heading ;
			$message .= $message_heading ;
			$message .= $message_content;
			$message .= $message_end;
			$cc = explode(',',$cc);
			$cc_string = '[';
			for($count = 0; $count < count($cc); $count++)
			{
				$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
			}
			$cc_string = trim($cc_string,',');
			$cc_string = $cc_string.']';

			$bcc = explode(',',$bcc);
				$bcc_string = '[';
				for($count = 0; $count < count($bcc); $count++)
				{
					$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
				}
				$bcc_string = trim($bcc_string,',');
				$bcc_string = $bcc_string.']';

			$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
				"personalizations": [
					{
						"to": [
							{
								"email": "'.$to.'"
							}
						],
						"cc": '.$cc_string.',
						"bcc": '.$bcc_string.',
						"subject": "'.$subject.'"
					}
				],
				"from": {
					"email": "venkataramanaiah@knsgroup.in"
				},
				"content": [
					{
						"type": "text/html",
						"value": "'.$message.'"
					}
				]
			}'));
			$apiKey = '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';
			$sg = new \SendGrid($apiKey);
			// $response = $sg->client->mail()->send()->post($request_body);
		}
	}
?>
