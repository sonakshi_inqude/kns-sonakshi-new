<?php
session_start();
$_SESSION['module'] = 'PM Masters';

define('PROJECT_BUDGET_LIST_FUNC_ID', '365');

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_masters.php');

require("utilities/sendgrid/sendgrid-php.php");

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_BUDGET_LIST_FUNC_ID, '6', '1');
    ?>

    <script>
      window.permissions = {
          view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
          delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      }
    </script>

    <?php
		if(isset($_GET['hd_material_id']))
		{
			if($_GET["stxt_material"] != '')
			{
				$search_material 	  = $_GET["hd_material_id"];
				$search_material_name = $_GET["stxt_material"];
			}
			else
			{
				$search_material 	  = "";
				$search_material_name = "";
			}
		}
		else
		{
			$search_material 	  = "";
			$search_material_name = "";
		}
    if(isset($_POST["percentage_id"]))
  	{
  		$percentage_id       = $_POST["percentage_id"];
  		$percentage 	   = $_POST["percentage"];
  		$remarks 	   = $_POST["remarks"];
  		// Check for mandatory fields
  		if(($percentage_id != ""))
  		{
        $material_percentage_update_data = array("percentage"=>$percentage,"remarks"=>$remarks);
  			$stock_material_percentage_uresult = db_update_material_percentage_list($percentage_id,$material_percentage_update_data);
  			if($stock_material_percentage_uresult["status"] == SUCCESS)
  			{
  				$alert_type = 1;
          echo "SUCCESS";
  			}
  		}
  		else
  		{
  			// $alert = "Please fill all the mandatory fields";
  			// $alert_type = 0;
  		}
  	}
} else {
    header("location:login.php");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Material Percentage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
	  <script src="datatable/stock_material_percentage.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
  </head>
  <body>
  <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Update Material Percentage</h4>
      </div>
      <div class="widget-header" style="height:auto;">
        <div style="border-bottom: 1px solid #C0C0C0;">
          <span class="header-label">Project Name: </span><span id="header_material_name"></span>
            </div>
            <div style="border-bottom: 1px solid #C0C0C0;">
          <span class="header-label">Process: </span><span id="header_material_code"></span>
          <span class="header-label">Uom: </span><span id="header_material_uom"></span>
        </div>
      </div>

      <div class="modal-body">
        <form  name="project_budget_plan_form">
        <input type="hidden" id="selected_row_id">
        <span id="percentage_id"></span>
        <input type="hidden" id="view_perm" value="<?= $view_perms_list['status'];?>">
        <input type="hidden" id="edit_perm" value="<?= $edit_perms_list['status'];?>">

        <div class="form-group">
          <label class="control-label" for="mp_budget">Percentage</label>
          <div class="controls">
            <input type="number" class="form-control" name="mp_budget" id="percentage">
          </div>
          </div>

          <div class="form-group">
            <label class="control-label" for="remarks">Remarks</label>
            <div class="controls">
              <textarea name="remarks" id="remarks" class="form-control" rows="4" cols="50" minlength="15" required></textarea>
            </div> <!-- /controls -->
          </div> <!-- /form-group -->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
          <button type="button" class="btn btn-default"  onclick="submitData()" data-dismiss="modal">Submit</button>
        </div>
        </form>
        </div>
        </div>
     </div>
  </div>
  <div class="main margin-top">
    <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="widget widget-table action-table">
            <div class="widget-header">
              <h3>Material Percentage</h3><span style="float:right; padding-right:20px;"><a href="stock_add_material_percentage.php" target="_blank">Add Percentage</a></span>
            </div>
            <div class="widget-header widget-toolbar">
              <form method="get" class="form-inline">
                 <input type="hidden" name="hd_material_id" id="hd_material_id" value="<?php echo $search_material; ?>" />

         			  <span style="padding-right:15px; float:left;">

         					<input type="text" name="stxt_material" class="form-control" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search Material by name or code" value="<?php echo $search_material_name; ?>" />

         					<div id="search_results" class="dropdown-content"></div>

         			  </span>
                <button id="submit_button" type="button" class="btn btn-primary" onclick="redrawTable()">Submit</button>
              </form>
            </div>
            <br>
          </div>
            <div class="widget-content">
             <table class="table table-striped table-bordered display nowrap" id="example" style="width:100%">
               <thead>

                 <tr>
                   <th>#</th>
                   <th>Material Name</th>
                   <th>Material Code</th>
                   <th>Material Uom</th>
                   <th>Stock percentage</th>
                   <th>Remarks</th>
                   <th>user</th>
                   <th>Added on</th>
                   <th>Edit</th>
                </tr>
             </thead>
               </tbody>
             </table>
            </div>
            <!-- widget-content -->
            </div>
          </div>
        </div>
      </div>
<script>

function submitData()
{
  var percentage_id = document.getElementById("percentage_id").innerHTML ;
  var percentage = document.getElementById("percentage").value ;
  var remarks = document.getElementById("remarks").value ;
  console.log(percentage_id);
  if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
       redrawTable();
		}
	}
	xmlhttp.open("POST", "stock_material_percentage.php");
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("percentage_id=" + percentage_id + "&percentage=" +percentage + "&remarks=" +remarks);
	return false;

}
function get_material_list()
{
	var searchstring = document.getElementById('stxt_material').value;
	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				if(xmlhttp.responseText != 'FAILURE')
				{
					document.getElementById('search_results').style.display = 'block';
					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}
			}
		}
		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("search=" + searchstring);
	}
	else
	{
		document.getElementById('search_results').style.display = 'none';
	}
}
function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;



	document.getElementById('search_results').style.display = 'none';

}
</script>
</body>
</html>
