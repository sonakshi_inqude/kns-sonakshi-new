<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 25-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'survey'.DIRECTORY_SEPARATOR.'survey_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String
	if(isset($_GET["survey_id"]))
	{
		$survey_id = $_GET["survey_id"];		
	}	
	else
	{
		$survey_id = "";
	}

	// Capture the form data
	if(isset($_POST["add_survey_single_process_submit"]))
	{
		$survey_id             = $_POST["hd_survey_id"];
		$process_id            = $_POST["ddl_process_id"];
		$process_start_date    = date("Y-m-d");
		$remarks               = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($process_id != ""))
		{
			$survey_single_process_iresult = i_add_survey_single_process($survey_id,$process_id,$process_start_date,'',$remarks,$user);
			
			if($survey_single_process_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
			}
			
			$alert = $survey_single_process_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

    // Get list of process for this process type	
	$survey_process_master_search_data = array("active"=>'1');
	$survey_process_master_list = i_get_survey_process_master($survey_process_master_search_data);
	if($survey_process_master_list["status"] == SUCCESS)
	{
		$survey_process_master_list_data = $survey_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$survey_process_master_list["data"];
		$alert_type = 0; // Failure
	}	
	
	// Get Survey Single Process  modes already added
	$survey_single_process_search_data = array("active"=>'1',"survey_id"=>$survey_id);
	$survey_single_process_list = i_get_survey_single_process($survey_single_process_search_data);
	if($survey_single_process_list['status'] == SUCCESS)
	{
		$survey_single_process_list_data = $survey_single_process_list['data'];
	}	
}
else
{
	header("location:login.php");
}		
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Survey - Add Single Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Survey - Add Single Process</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Survey - Add Single Process</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="survey_add_single_process_form" class="form-horizontal" method="post" action="survey_add_single_process.php">
								<input type="hidden" name="hd_survey_id" value="<?php echo $survey_id; ?>" />
									<fieldset>										
																
											<div class="control-group">											
											<label class="control-label" for="ddl_process_id">Process*</label>
											<div class="controls">
												<select class="span6" name="ddl_process_id" required>
												<option>- - Select Process - -</option>
												<?php
												for($count = 0; $count < count($survey_process_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $survey_process_master_list_data[$count]["survey_process_master_id"]; ?>"><?php echo $survey_process_master_list_data[$count]["survey_process_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
								
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_survey_single_process_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							<table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Process</th>
					<th>Process Start Date</th>
					<th>Process End Date</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($survey_single_process_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($survey_single_process_list_data); $count++)
					{
						$sl_no++;
						$process_end_date = $survey_single_process_list_data[$count]["survey_single_process_end_date"];
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $survey_single_process_list_data[$count]["survey_process_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_single_process_list_data[$count][
					"survey_single_process_start_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($process_end_date,"d-M-Y"); ?></td>
					<td><?php echo $survey_single_process_list_data[$count]["survey_single_process_remarks"]; ?></td>
					<td><?php echo $survey_single_process_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($survey_single_process_list_data[$count][
					"survey_single_process_added_on"])); ?></td>
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_survey_edit_single_process('<?php echo $survey_single_process_list_data[$count]["survey_single_process_survey_id"]; ?>','<?php echo $survey_single_process_list_data[$count]["survey_single_process_id"]; ?>');">Edit</a></td>-->
					<td><?php if(($survey_single_process_list_data[$count]["survey_single_process_active"] == "1")){?><a href="#" onclick="return survey_delete_singel_process('<?php echo $survey_single_process_list_data[$count]["survey_single_process_id"]; ?>','<?php echo $survey_single_process_list_data[$count]["survey_single_process_survey_id"]; ?>');">Delete</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Survey Single Process added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function survey_delete_singel_process(survey_process_id,survey_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "survey_add_single_process.php?survey_id=" +survey_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/survey_delete_single_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("survey_process_id=" + survey_process_id + "&action=0");
		}
	}	
}
function go_to_survey_edit_single_process(survey_process_id,survey_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "survey_edit_single_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","survey_process_id");
	hiddenField1.setAttribute("value",survey_process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","survey_id");
	hiddenField2.setAttribute("value",survey_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>

							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
