<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10-Jan-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
define('APF_DOCUMENT_LIST_FUNC_ID','324');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'apf_masters'.DIRECTORY_SEPARATOR.'apf_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',APF_DOCUMENT_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',APF_DOCUMENT_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',APF_DOCUMENT_LIST_FUNC_ID,'4','1');


	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	
	if(isset($_REQUEST['process_id']))
	{
		$process_id = $_REQUEST['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	if(isset($_REQUEST['apf_id']))
	{
		$apf_id = $_REQUEST['apf_id'];
	}
	else
	{
		$apf_id = "";
	}
	
	$search_project   = "";
	$search_process   = "";
	$search_bank      = "";
	
	if(isset($_POST['file_search_submit']))
	{
		$search_project    = $_POST["search_project"];
		$search_process    = $_POST["search_process"];
		$search_bank       = $_POST["search_bank"];
		$apf_id            = $_POST["hd_apf_id"];
	}
	
	// Get APF Process Master modes already added
	$apf_process_master_search_data = array("active"=>'1');
	$apf_process_master_list = i_get_apf_process_master($apf_process_master_search_data);
	if($apf_process_master_list['status'] == SUCCESS)
	{
		$apf_process_master_list_data = $apf_process_master_list['data'];
	}	

	else
	{
		$alert = $apf_process_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Bank Master modes already added
	$apf_bank_master_search_data = array("active"=>'1');
	$bank_master_list = i_get_apf_bank_master($apf_bank_master_search_data);
	if($bank_master_list['status'] == SUCCESS)
	{
		$bank_master_list_data = $bank_master_list["data"];
	}
	else
	{
		$alert = $bank_master_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Master already added
	$apf_project_master_search_data = array("active"=>'1');
	$apf_project_master_list = i_get_apf_project_master($apf_project_master_search_data);
	if($apf_project_master_list['status'] == SUCCESS)
	{
		$apf_project_master_list_data = $apf_project_master_list['data'];
	}
	else
	{
		$alert = $apf_project_master_list["data"];
		$alert_type = 0;
	}

	
	//Get Document List
	$apf_document_search_data = array("active"=>'1',"process_id"=>$process_id,"project_name"=>$search_project,"bank_name"=>$search_bank,"process_name"=>$search_process);
	$apf_document_list = i_get_apf_document($apf_document_search_data);
	if($apf_document_list["status"] == SUCCESS)
	{
		$apf_document_list_data = $apf_document_list["data"];
	}
	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add APF Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>APF Document List</h3>
	  				</div> <!-- /widget-header -->
					<div class="widget-header" style="height:50px; padding-top:10px;">		
			<form method="post" id="file_search_form" action="apf_process_list.php">
			<input type="hidden" name="hd_apf_id" value="<?php echo $apf_id; ?>" />
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_project_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_id"]; ?>" <?php if($search_project == $apf_project_master_list_data[$apf_count]["apf_project_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_project_master_list_data[$apf_count]["apf_project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			   <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($apf_count = 0; $apf_count < count($apf_process_master_list_data); $apf_count++)
			  {
			  ?>
			  <option value="<?php echo $apf_process_master_list_data[$apf_count]["apf_process_master_id"]; ?>" <?php if($search_process == $apf_process_master_list_data[$apf_count]["apf_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $apf_process_master_list_data[$apf_count]["apf_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_bank">
			  <option value="">- - Select Bank - -</option>
			  <?php
			  for($bank_count = 0; $bank_count < count($bank_master_list_data); $bank_count++)
			  {
			  ?>
			  <option value="<?php echo $bank_master_list_data[$bank_count]["apf_bank_master_id"]; ?>" <?php if($search_bank == $bank_master_list_data[$bank_count]["apf_bank_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $bank_master_list_data[$bank_count]["apf_bank_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
			  </div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
			
			 <div class="widget-content">
			<?php
			if($view_perms_list['status'] == SUCCESS)
			{
			?>		
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
				    <th>Bank</th>
					<th>Document</th>
					<th>Added By</th>				
					<th>Added On</th>
                    <th colspan="1" style="text-align:center;">Actions</th>						
				</tr>
				</thead>
				<tbody>	
				<?php
				if($apf_document_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($apf_document_list_data); $count++)
					{
						$sl_no++;
						$doc = $apf_document_list_data[$count]["apf_document_file_path"];
						if($doc != 0)
						{
							?>
							<?php
							if(($edit_perms_list['status'] == SUCCESS) || ($apf_document_list_data[$count]["apf_document_added_by"] == $user))  
							{
						        ?>
								<tr>
								<td><?php echo $sl_no; ?></td>
								<td><?php echo $apf_document_list_data[$count]["apf_project_master_name"]; ?></td>
								<td><?php echo $apf_document_list_data[$count]["apf_process_master_name"]; ?></td>
								<td><?php echo $apf_document_list_data[$count]["apf_bank_master_name"]; ?></td>
								<td><a href="documents/<?php echo $apf_document_list_data[$count]["apf_document_file_path"]; ?>" target="_blank"><?php echo $apf_document_list_data[$count]["apf_document_file_path"]; ?></a></td>
								<td><?php echo $apf_document_list_data[$count]["user_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($apf_document_list_data[$count][
								"apf_document_added_on"])); ?></td>
								<td><?php if(($apf_document_list_data[$count]["apf_document_active"] == "1") && ($delete_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return apf_delete_document(<?php echo $apf_document_list_data[$count]["apf_document_id"]; ?>);">Delete</a><?php } ?></td>
								</tr>
								<?php
							}
						}
						
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Documents added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			<?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function apf_delete_document(document_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "apf_document_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/apf_delete_document.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("document_id=" + document_id + "&action=0");
		}
	}	
}

</script>

  </body>

</html>
