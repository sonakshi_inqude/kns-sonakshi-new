<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 22nd March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["request"]))
	{
		$request_id = $_GET["request"];
	}
	else
	{
		$request_id = "-1";
	}
	
	if(isset($_GET["case"]))
	{
		$case_id = $_GET["case"];
	}
	else
	{
		$case_id = "-1";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_court_pay_submit"]))
	{
		$request_id  = $_POST["hd_request_id"];	
		$case_id     = $_POST["hd_case_id"];
		$amount      = $_POST["num_amount"];
		$mode        = $_POST["ddl_pay_mode"];
		$remarks     = $_POST["txt_remarks"];
		$paid_to     = $_POST["stxt_paid_to"];
		$added_by    = $user;
		
		// Check for mandatory fields
		if(($request_id != '') && ($amount != ""))
		{
			$pay_release_iresult = i_add_court_payment_release($request_id,$amount,$mode,$remarks,$paid_to,$added_by);
			
			if($pay_release_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert = "Payment Release Details successfully updated";		

				header("location:court_payment_release_list.php?case=".$case_id);
			}
			else
			{
				$alert_type = 0;
				$alert = $pay_release_iresult["data"];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Payment Mode
	$payment_mode_list = i_get_payment_mode_list('','');
	if($payment_mode_list["status"] == SUCCESS)
	{
		$payment_mode_list_data = $payment_mode_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$payment_mode_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get already paid amount to this task
	$payment_total = 0;
	$pay_release_data = array("case"=>$case_id);
	$pay_release_list = i_get_court_payment_release_list($pay_release_data);
	if($pay_release_list["status"] == SUCCESS)
	{
		$pay_release_list_data = $pay_release_list["data"];
		for($count = 0; $count < count($pay_release_list_data); $count++)
		{
			$payment_total = $payment_total + $pay_release_list_data[$count]['legal_court_payment_issued_amount'];
		}
	}
	else
	{
		$alert = $alert."Alert: ".$pay_release_list["data"]."<br />";
		$alert_type = -1;
		$payment_total = 0;
	}
	
	// Get request details
	$request_data = i_get_court_payment_request_list('','','','','','','','','',$request_id);
	if($request_data["status"] == SUCCESS)
	{
		$request_data_details = $request_data["data"];				
	}
	else
	{
		header('location:court_case_payment_request_list.php');
	}
	
	// Get case details
	$case_details = array('legal_court_case_id'=>$case_id);
	$court_case_details = i_get_legal_court_case($case_details);
	if($court_case_details["status"] == SUCCESS)
	{
		$court_case_details_data = $court_case_details["data"];
	}
	else
	{
		$alert = $court_case_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Court Case - Release payment</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3><?php
						if($court_case_details["status"] == SUCCESS)
						{
							?>
							Case No: <?php echo $court_case_details_data[0]["legal_court_case_number"]; ?>&nbsp;&nbsp;&nbsp;Establishment: <?php echo $court_case_details_data[0]['bd_court_establishment_name']; ?>&nbsp;&nbsp;&nbsp;Case Type: <?php echo $court_case_details_data[0]['bd_court_case_type_master_type']; ?>&nbsp;&nbsp;&nbsp;Amount Paid: <span id="amount_paid_already"><?php echo $payment_total; ?></span>
							<?php
						}
						else
						{
							echo "Invalid task";
						}
						?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Issue Payment</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_court_payment_form" class="form-horizontal" method="post" action="court_payment_release.php">
									<fieldset>
										<input type="hidden" name="hd_request_id" value="<?php echo $request_id; ?>" />
										<input type="hidden" name="hd_case_id" value="<?php echo $case_id; ?>" />
																				
										<div class="control-group">											
											<label class="control-label" for="num_amount">Amount</label>
											<div class="controls">
												<input type="number" class="span6" name="num_amount" placeholder="Amount needed" required="required" value="<?php echo $request_data_details[0]['bd_court_payment_requests_amount']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_pay_mode">Payment Mode*</label>
											<div class="controls">
												<select name="ddl_pay_mode" class="span6" required>
												<?php 
												for($count = 0; $count < count($payment_mode_list_data); $count++)
												{
												?>
												<option value="<?php echo $payment_mode_list_data[$count]["payment_mode_id"]; ?>" <?php 
												if($payment_mode_list_data[$count]["payment_mode_id"] == 3)
												{
												?>
												selected="selected"
												<?php
												}?>><?php echo $payment_mode_list_data[$count]["payment_mode_name"]; ?></option>												
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_paid_to">Paid To</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_paid_to" placeholder="Person to whom payment was done" required="required" value="<?php echo $request_data_details[0]['bd_court_payment_requests_payable_to']; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6"></textarea>												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_court_pay_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																								
							</div>
														  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
