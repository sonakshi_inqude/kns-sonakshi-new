<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_grn_accont_list.php

CREATED ON	: 29-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of grn account for customer withdrawals

*/



/*

TBD: 

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_history_function.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Query String Data	

	// Nothing

	

	// Temp data

	$search_vendor        = '';

	$search_vendor_name   = '';

	$search_material      = '';

	$search_material_name = '';

	$po_id 				  = '';

	$po_no 				  = '';

	if(isset($_POST['po_search_submit']))

	{		

		$search_vendor_name   = $_POST["stxt_vendor"];		

		$search_vendor        = $_POST["hd_vendor_id"];

		$search_material_name = $_POST["stxt_material"];

		$search_material      = $_POST["hd_material_id"];

		$po_id                = $_POST['hd_po_id'];

		$po_no                = $_POST['po_number'];	

	}

	else

	{

		$search_vendor_name   = "";

		$search_vendor        = "";

		$search_material_name = "";

		$search_material      = "";

		$po_id                = "";

		$po_no                = "";

	}

	

	// Get Purchase Order Items List

	$stock_purchase_order_items_search_data = array();

	if($search_material != "")

	{

		$stock_purchase_order_items_search_data['item'] = $search_material;

	}

	if($po_id != "")

	{

		$stock_purchase_order_items_search_data['order_id'] = $po_id;

	}

	if($search_vendor != "")

	{

		$stock_purchase_order_items_search_data['vendor'] = $search_vendor;

	}

	$stock_po_item_list = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);

	if($stock_po_item_list["status"] == SUCCESS)

	{

		$stock_po_item_list_data  = $stock_po_item_list["data"];

	}

	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Stock Completed Purchase Report</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

    <link href="css/style1.css" rel="stylesheet">

    

    

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>PO Completed Items Report</h3>

            </div>

            <!-- /widget-header -->

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="vendor_item_search_form" action="stock_completed_po_report.php">

			  <input type="hidden" name="hd_po_id" id="hd_po_id" value="<?php echo $po_id; ?>" />

			  <input type="hidden" name="hd_vendor_id" id="hd_vendor_id" value="<?php echo $search_vendor; ?>" />	

			  <input type="hidden" name="hd_material_id" id="hd_material_id" value="<?php echo $search_material; ?>" />	

			  <div style="padding-right:20px; padding-left:20px; width:200px; float:left;	">										

					<input type="text" name="po_number" autocomplete="off" id="po_number" onkeyup="return get_po_list();" value="<?php echo $po_no; ?>" placeholder="Search by PO No" />

					<div id="search_results" class="dropdown-content"></div>

			  </div>			

			  <div style="padding-right:20px; padding-left:20px; width:200px; float:left;">										

					<input type="text" name="stxt_vendor" autocomplete="off" id="stxt_vendor" onkeyup="return get_vendor_list();" placeholder="Search by vendor name" value="<?php echo $search_vendor_name; ?>" />

					<div id="search_results_vendor" class="dropdown-content"></div>

			  </div>

			  <div style="padding-right:20px; padding-left:20px; width:200px; float:left;">										

					<input type="text" name="stxt_material" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search by material" value="<?php echo $search_material_name; ?>" />

					<div id="search_results_material" class="dropdown-content"></div>

			  </div>

			  <div style="padding-right:20px; padding-left:20px; width:200px; float:left;">				

			  <input type="submit" name="po_search_submit" />

			  </div>

			  </form>			  

            </div>

            <div class="widget-content">

			

              <table class="table table-bordered">

                <thead>

                  <tr>

				    <th>SL No</th>

					<th>Po No</th>

					<th>Po Date</th>

					<th>Item Name</th>

					<th>Item Code</th>

					<th>Ordered Qty</th>		

					<th>Recived Qty</th>	

					<th>Vendor</th>

					<th>Last Grn</th>	

					<th>Due Date</th>										

				</tr>

				</thead>

				<tbody>							

				<?php

				if($stock_po_item_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($stock_po_item_list_data); $count++)

					{

						$sl_no ++;					

						

						$po_item_qty = $stock_po_item_list_data[$count]['stock_purchase_order_item_quantity'];
						$po_status = $stock_po_item_list_data[$count]['stock_purchase_order_status'];

						// Get Grn Inspection List

						$stock_grn_engineer_inspection_search_data = array("order_id"=>$stock_po_item_list_data[$count]["stock_purchase_order_id"],"material"=>$stock_po_item_list_data[$count]["stock_purchase_order_item"]);

						$stock_grn_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);

						if($stock_grn_inspection_list['status'] == SUCCESS)

						{

							$accepted_qty = 0 ;

							$stock_grn_inspection_list_data = $stock_grn_inspection_list['data'];

							for($qty = 0 ; $qty < count($stock_grn_inspection_list_data) ; $qty++)

							{

								$accepted_qty = $accepted_qty + $stock_grn_inspection_list_data[$qty]["stock_grn_engineer_inspection_approved_quantity"];

								$last_grn_no = $stock_grn_inspection_list_data[$qty]["stock_grn_no"];

							}	

						}	

						else

						{

							$accepted_qty  = 0;
							$last_grn_no   = "";

						}

						

						$pending_qty = ($po_item_qty - $accepted_qty);

						if($po_status == 'Completed')
						{
							?>

							<tr style="color:<?php echo $color; ?>">

							<td><?php echo $sl_no; ?></td>

							<td><?php echo $stock_po_item_list_data[$count]["stock_purchase_order_number"] ;?></td>

							<td><?php echo date('d-M-Y',strtotime($stock_po_item_list_data[$count]["stock_purchase_order_added_on"])) ;?></td>

							<td><?php echo $stock_po_item_list_data[$count]["stock_material_name"] ;?></td>

							<td><?php echo $stock_po_item_list_data[$count]["stock_material_code"] ;?></td>

							<td><?php echo $po_item_qty ; ?></td>

							<td><?php echo $accepted_qty ; ?></td>

							<td><?php echo $stock_po_item_list_data[$count]["stock_vendor_name"].' ('.$stock_po_item_list_data[$count]["stock_vendor_code"].')'; ?></td>

							<td><?php echo $last_grn_no ;?></td>

							<td><?php echo date('d-M-Y',strtotime($stock_po_item_list_data[$count]["stock_purchase_order_due_date"])) ;?></td>

							</tr>

							<?php	
								
						}

						}

					}

				else

				{

				?>

				<td colspan="8"></td>

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_account(account_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_grn_account_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_account.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("account_id=" + account_id + "&action=0");

		}

	}	

}

function go_to_po_item(po_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_purchase_order_items.php");

	

	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","po_id");

	hiddenField2.setAttribute("value",po_id);

	

	form.appendChild(hiddenField2);

	

	document.body.appendChild(form);

    form.submit();

}



function get_po_list()

{ 

	var searchstring = document.getElementById('po_number').value;

	

	if(searchstring.length >= 1)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

				else

				{

					document.getElementById('search_results').style.display = 'none';

				}

			}

		}



		xmlhttp.open("POST", "ajax/stock_get_po_no.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

		document.getElementById('hd_po_id').value  				= '';

	}

}



function select_po_no(po_id,$order_no)

{	

	document.getElementById('hd_po_id').value  = po_id;

	document.getElementById('po_number').value = $order_no;

	

	document.getElementById('search_results').style.display = 'none';

}



function get_vendor_list()

{ 

	var searchstring = document.getElementById('stxt_vendor').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{					

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results_vendor').style.display = 'block';

					document.getElementById('search_results_vendor').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/stock_get_vendor.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results_vendor').style.display = 'none';

		document.getElementById('hd_vendor_id').value 				   = '';

	}

}



function select_vendor(vendor_id,search_vendor)

{

	document.getElementById('hd_vendor_id').value = vendor_id;

	document.getElementById('stxt_vendor').value = search_vendor;

	

	document.getElementById('search_results_vendor').style.display = 'none';

}



function get_material_list()

{ 

	var searchstring = document.getElementById('stxt_material').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results_material').style.display = 'block';

					document.getElementById('search_results_material').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results_material').style.display = 'none';

		document.getElementById('hd_material_id').value 				 = '';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;

	

	document.getElementById('search_results_material').style.display = 'none';

}

</script>



  </body>



</html>

