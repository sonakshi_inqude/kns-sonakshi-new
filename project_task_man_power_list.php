<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_man_power_list.php
CREATED ON	: 1-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing

	$search_process   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_process   = $_POST["search_process"];
	}

	$search_project   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_project   = $_POST["search_project"];
	}

	$search_user   	 = "";

	if(isset($_POST["file_search_submit"]))
	{
		$search_user   = $_POST["search_user"];
	}

	 // Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}

	// Get Project Process Master modes already added
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list['status'] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $project_process_master_list["data"];
		$alert_type = 0;
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	// Temp data
	$project_task_man_power_search_data = array("active"=>'1',"process"=>$search_process,"user_id"=>$search_user,"project_id"=>$search_project);
	$project_task_man_power_list = i_get_project_task_man_power($project_task_man_power_search_data);
	if($project_task_man_power_list["status"] == SUCCESS)
	{
		$project_task_man_power_list_data = $project_task_man_power_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_man_power_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task Man Power List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Task Man Power List</h3><span style="float:right; padding-right:20px;"><a href="project_add_task_man_power.php">Project Add Task Man Power</a></span>
            </div>
            </div>
            <!-- /widget-header -->

			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_task_man_power_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			   </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>

            <div class="widget-content">

              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
					<th>process</th>
					<th>Task</th>
					<th>Type</th>
					<th>Hours</th>
					<th>Remarks</th>
					<th>user</th>
					<th>Added On</th>
					<th colspan="2" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_task_man_power_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_task_man_power_list_data); $count++)
					{
						$sl_no++;

						//Get Task user ID from Task user mapping
						$project_task_master_search_data = array("task_master_id"=>$project_task_man_power_list_data[$count]["project_task_man_power_task_id"]);
						$task_master_list = i_get_project_task_master($project_task_master_search_data);
						if($task_master_list["status"] == SUCCESS)
						{
							$task_master_list_data = $task_master_list["data"];
							$task_master_id = $task_master_list_data[0]["project_task_master_id"];
							$process_id = $task_master_list_data[0]["project_task_master_process"];
						}
						else
						{
							$task_master_id = '-1';
						}

						//Get Task user ID from Task user mapping
						$project_task_user_mapping_search_data = array("task_id"=>$task_master_id,"active"=>'1');
						$task_user_mapping_list = i_get_project_task_user_mapping($project_task_user_mapping_search_data);
						if($task_user_mapping_list["status"] == SUCCESS)
						{
							$task_user_mapping_list_data = $task_user_mapping_list["data"];
							$task_user_id = $task_user_mapping_list_data[0]["project_task_user_id"];
						}
						else
						{
							$task_user_id = "";
						}
						// Get user
						$user_list = i_get_user_list($task_user_id,'','','');
						if($user_list['status'] == SUCCESS)
						{
							$user_list_data = $user_list['data'];
							$user_name = $user_list_data[0]["user_name"];
						}

						//Get Project plan Process
						$project_plan_process_search_data = array("process_id"=>$process_id,"active"=>'1');
						$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
						if($project_plan_process_list["status"] == SUCCESS)
						{
							$project_plan_process_list_data = $project_plan_process_list["data"];
							$plan_id = $project_plan_process_list_data[0]["project_plan_process_plan_id"];
						}

						//Get project plan
						$project_plan_search_data = array("plan_id"=>$plan_id,"active"=>'1');
						$project_plan_list = i_get_project_plan($project_plan_search_data);
						if($project_plan_list["status"] == SUCCESS)
						{
							$project_plan_list_data = $project_plan_list["data"];
							$project_id = $project_plan_list_data[0]["project_plan_project_id"];
						}

						//Get project Master
						$project_management_master_search_data = array("project_id"=>$project_id,"active"=>'1', "user_id"=>$user);
						$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
						if($project_management_master_list["status"] == SUCCESS)
						{
							$project_management_master_list_data = $project_management_master_list["data"];
							$project_name = $project_management_master_list_data[0]["project_master_name"];
						}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_name; ?></td>
					<td><?php echo $project_task_man_power_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_man_power_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_task_man_power_list_data[$count]["project_man_power_name"]; ?></td>
					<td><?php echo $project_task_man_power_list_data[$count]["project_task_man_power_hours"]; ?></td>
					<td><?php echo $project_task_man_power_list_data[$count]["project_task_man_power_remarks"]; ?></td>
					<td><?php echo $user_name; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_man_power_list_data[$count][
					"project_task_man_power_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_task_man_power('<?php echo $project_task_man_power_list_data[$count]["project_task_man_power_id"]; ?>');">Edit </a></div></td>
					<td><?php if(($project_task_man_power_list_data[$count]["project_task_man_power_active"] == "1")){?><a href="#" onclick="return project_delete_task_man_power(<?php echo $project_task_man_power_list_data[$count]["project_task_man_power_id"]; ?>);">Delete</a><?php } ?></td>
					</tr>
					<?php
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_man_power(man_power_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_man_power_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_man_power.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
		}
	}
}
function go_to_project_edit_task_man_power(man_power_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_task_man_power.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","man_power_id");
	hiddenField1.setAttribute("value",man_power_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
