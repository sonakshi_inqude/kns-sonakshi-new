<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['remarks_id'])) {
        $remarks_id = $_REQUEST['remarks_id'];
    } else {
        $remarks_id = "";
    }

    if (isset($_REQUEST['project_id'])) {
        $project_id = $_REQUEST['project_id'];
    } else {
        $project_id = "";
    }

		$process_remarks_update_data = array("status"=>"Completed");
		$payment_manpower_mapping_iresult = db_update_process_remarks_task($remarks_id,$process_remarks_update_data);
		header("location:project_completion_report.php?search_project=$project_id");

    echo json_encode($process_remarks_list_data);
} else {
    header("location:login.php");
}
