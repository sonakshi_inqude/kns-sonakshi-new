<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '253');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');


if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '6', '1');

    // Query String Data
    // Nothing

    $alert_type = -1;
    $alert = "";

    $search_department = "";

    if (isset($_POST["search_department"])) {
        $search_department   = $_POST["search_department"];
    // set here
    } else {
        $search_department = "";
    }

    $department_list = i_get_department_list('', '');

    if ($department_list["status"] == SUCCESS) {
        $department_list_data = $department_list["data"];
    } else {
        $alert = $alert."Alert: ".$department_list["data"];
      }

    // Process Master
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_process_master_list["data"];
    }

} else {
    header("location:login.php");
}
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Pending Loan List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/bootstrap.js"></script>
		<script data-jsfiddle="common" src="js/handsontable-master/demo/js/moment/moment.js"></script>
    <link rel="stylesheet" type="text/css" href="tools/datatables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="tools/datatables/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="datatable.css">

     <script src="tools/datatables/js/jquery.dataTables.min.js"></script>
		 <script src="datatable/pli_list.js"></script>

  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
    ?>

  <div class="main">
    <div class="main-inner">
      <div class="container">
        <div class="row">

            <div class="span6" style="width:100%;">

            <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Pending Loan Amount List</h3>
              </div>
              <!-- /widget-header -->

              <div class="widget-content">

								<?php
                                if ($view_perms_list['status'] == SUCCESS) {
                                    ?>
								<div class="widget-header" style="height:80px; padding-top:10px;">
								  <form method="post" id="file_search_form" action="#">

								  <span style="padding-left:20px; padding-right:20px;">

                    <select name="search_department">
                   <option value="">- - Select Department - -</option>
                   <?php
                                    for ($count = 0; $count < count($department_list_data); $count++) {
                                        ?>
                   <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>" <?php if ($search_department == $department_list_data[$count]["general_task_department_id"]) {
                                            ?> selected="selected" <?php
                                        } ?>><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
                   <?php
                                    } ?>
                   </select>
								  </span>

								  <input type="submit" name="manpower_search_submit" />

									<script>
									// var formElements = [];
									// formElements['search_department'] = <?=  json_encode($search_department); ?>;
									setVars(<?= $search_department; ?>);
									</script>

								  </form>
					            </div>
								 <?php
                                } else {
                                    echo 'You are not authorized to view this page';
                                }
                                ?>
                                <?php
                                      if ($view_perms_list['status'] == SUCCESS) {
                                          ?>
                <table class="table table-bordered" cellpadding="0" cellspacing="0" border="0" class="display dataTable" id="example" aria-describedby="example_info">
                  <thead>
                <tr style="background-color: #f2f2f2">
                <th style="word-wrap:break-word;">#</th>
                <th style="word-wrap:break-word;">Department</th>
                <th style="word-wrap:break-word;" >Name</th>
                <th style="word-wrap:break-word;" >PLI<br/> Amount</th>
                <th style="word-wrap:break-word;" >PLI<br/> Effective Date</th>
                <th style="word-wrap:break-word;" >Remarks</th>
                <!-- <th style="word-wrap:break-word;" >Action</th> -->
                 <!-- Added body
                 Added On -->
                </tr>
                </thead>
                  </tbody>
                </table>
                <?php
                   } else {
                       echo 'You are not authorized to view this page';
                   }
                   ?>
              </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
</html>
