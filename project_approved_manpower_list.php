<?php

/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID', '289');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_APPROVED_MANPOWER_LIST_FUNC_ID, '6', '1');
?>

    <script>
      window.permissions = {
        view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
        ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
        delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
        aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      }
    </script>

       <?php
    $alert_type = -1;
    $alert = "";

    // Process Master
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_process_master_list["data"];
    }

    // Project Manpower Vendor Master List
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_vendor_master_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_vendor_master_list["status"] == SUCCESS) {
        $project_manpower_vendor_master_list_data = $project_manpower_vendor_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_manpower_vendor_master_list["data"];
    }

    //Get Location List
    $stock_location_master_search_data = array();
    $location_list = i_get_stock_location_master_list($stock_location_master_search_data);
    if ($location_list["status"] == SUCCESS) {
        $location_list_data = $location_list["data"];
    } else {
        $alert = $location_list["data"];
        $alert_type = 0;
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1',"user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
    //Get Company List
  } else {
  	header("location:login.php");
  }
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
    <script type="text/javascript" src="./datatable/project_approved_mp_list.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
    <style media="screen">
      table.dataTable {
        margin-top: 0px !important;
      }
    </style>
  </head>
  <body>
    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>

    <div class="main margin-top">
        <div class="main-inner">
          <div class="container">
            <div class="row">

                <div class="span6" style="width:100%;">

                <div class="widget widget-table action-table">
                  <div class="widget-header">
                    <h3>Project Task Actual Man Power List</h3>
                  </div>

                  <div class="widget-header widget-toolbar">
                    <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                      <form class="form-inline" method="post" id="file_search_form">

                      <select id="search_vendor" name="search_vendor" class="form-control">
                      <option value="">- - Select Vendor - -</option>
                      <?php
                        for ($project_count = 0; $project_count < count($project_manpower_vendor_master_list_data); $project_count++) {
                      ?>
  								          <option value="<?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_id"]; ?>">
                              <?php echo $project_manpower_vendor_master_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
    								  <?php } ?>
                      </select>

                      <select id="search_project" name="search_project" class="form-control">
                        <option value="">- - Select Project - -</option>
      								  <?php
                        for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                          ?>
      								  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>">
                          <?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
      								  <?php
                        } ?>
      								  </select>

                      <input type="date" id="start_date" name="dt_start_date"  class="form-control"/>
                      <input type="date" id="end_date" name="dt_end_date" class="form-control"/>
                      <button type="button" class="btn btn-primary" onclick="drawTable()">Submit</button>
                      </form>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="widget-content">
                    <table id="example" class="table table-striped table-bordered display nowrap">
                      <thead>
                        <!-- <tr>
                          <th colspan="9"></th>
                          <th colspan="3" class="center">Men</th>
                          <th colspan="3" class="center">Women</th>
                          <th colspan="3" class="center">Mason</th>
                          <th colspan="6"></th>
                        </tr> -->
                        <tr>
                          <th>#</th>
                          <th>Agency</th>
                          <th>Project</th>
                          <th>Process</th>
                          <th>Task Name</th>
                          <th>Road Name</th>
                          <th>Work Type</th>
                          <th>%</th>
                          <th>Msrmt</th>
                          <th>Hour</th>
                          <th>Rate</th>
                          <th>Men</th>

                          <th>Hour</th>
                          <th>Rate</th>
                          <th>Women</th>

                          <th>Hour</th>
                          <th>Rate</th>
                          <th>Mason</th>
                          <th>No of People</th>
                          <th>Amount</th>
                          <th>Remarks</th>
                          <th>Added By</th>
                          <th>Added On</th>
                          <th>Date</th>
                        </thead>
                    </tbody>
                  </table>
                </div>
                <!-- /widget-content -->

                </div>
                <!-- /widget -->

                </div>
                <!-- /widget -->
              </div>
              <!-- /span6 -->
            </div>
            <!-- /row -->
          </div>
          <!-- /container -->
</body>
</html>
