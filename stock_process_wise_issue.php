<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	$process_id = "";
	$road_id = "";

	if (isset($_REQUEST["project_id"])) {
		$project_id = $_REQUEST["project_id"];
	}

	if (isset($_REQUEST["material_name"])) {
		$material_name = $_REQUEST["material_name"];
	}

	if (isset($_REQUEST["material_code"])) {
		$material_code = $_REQUEST["material_code"];
	}

	if (isset($_REQUEST["indent_id"])) {
		$indent_id = $_REQUEST["indent_id"];
	}
	else {
		$indent_id = $_REQUEST["hd_indent_id"];
	}

	if (isset($_REQUEST["indent_item_id"])) {
		$indent_item_id = $_REQUEST["indent_item_id"];
	}
	else {
		$indent_item_id = $_REQUEST["hd_indent_item_id"];
	}
	$project_stock_module_mapping_search_data = array("stock_project_id"=>$project_id);
	$project_stock_mapping_list = i_get_project_stock_module_mapping($project_stock_module_mapping_search_data);

	if ($project_stock_mapping_list["status"] == SUCCESS) {
			$pm_project = $project_stock_mapping_list["data"][0]["project_stock_module_mapping_mgmnt_project_id"];
	} else {
			$pm_project = "";
	}
	// Get Already added process
	$project_process_master_list = db_get_project_process_list('project_id',$pm_project);
	if ($project_process_master_list["status"] == DB_RECORD_ALREADY_EXISTS) {
			$project_process_master_list_data = $project_process_master_list["data"];
	}
	// Get Indent Item Details
	$stock_indent_search_data = array("status"=>'Approved' ,"active"=>'1',"item_id"=>$indent_item_id);
	$indent_item_list = i_get_indent_items_list($stock_indent_search_data);
	if($indent_item_list["status"] == SUCCESS)
	{
		$indent_item_list_data = $indent_item_list["data"];
		$indent_qty   = $indent_item_list_data[0]["stock_indent_item_quantity"];
		// Get Stock list
		$material_stock_search_data = array("material_id" => $indent_item_list_data[0]["stock_indent_item_material_id"],"project" => $indent_item_list_data[0]["stock_indent_project"]);
		$stock_material = i_get_material_stock($material_stock_search_data);
		if ($stock_material["status"] == SUCCESS) {
				$qunatity = 0;
				for ($stock_qty = 0; $stock_qty < count($stock_material["data"]); $stock_qty++) {
						$stock_material_data = $stock_material["data"];
						$qunatity            = $qunatity + $stock_material_data[$stock_qty]["material_stock_quantity"];
						$material            = $stock_material_data[$stock_qty]["material_id"];
				}
		} else {
				$alert    = $alert . "Alert: " . $stock_material["data"];
				$qunatity = "0";
		}

		//Get Stock Issue List
		$issued_qty  = 0;
		$stock_issue_item_search_data = array("indent_id" => $indent_id,"material_id" => $material);
		$stock_issue_item_list        = i_get_stock_issue_item($stock_issue_item_search_data);
		if ($stock_issue_item_list["status"] == SUCCESS) {
				$issued_qty = $stock_issue_item_list["data"][0]["total_issued_item_quantity"];
		} else {
				$issued_qty = 0;
		}
	}
	else
	{
	$alert = $alert."Alert: ".$indent_item_list["data"];
	}
	// Capture the form data
	if(isset($_POST["issue_submit"]))
	{
		$process     = $_POST["ddl_process"];
		$task     	 = $_POST["search_task"];
		$road        = $_POST["ddl_road"];
		$indent_id   = $_POST["hd_indent_id"];
		$material_id = $_POST["hd_indent_material_id"];
		$qty         = $_POST["issued_qty"];
		$project     = $_POST["pm_project_id"];
		$remarks     = $_POST["stxt_remarks"];
		$indent_qty  = $_POST["hd_indent_qty"];
		$issued_qty  = $_POST["hd_issued_qty"];
		$machine_id  = $_POST["hd_machine_id"];
		$remarks     = $_POST["stxt_remarks"];
		$project_id  = $_POST["project_id"];
		// Check for mandatory fields
		if (($material_id == "461") || ($material_id == "462")) {
				if (($qty != "" && $qty != '0') && ($machine_id != "")) {
						$allow = true;
				} else {
						$allow = false;
				}
		} else {
				if (($qty != "" && $qty != '0')) {
						$allow = true;
				} else {
						$allow = false;
				}
		}
		$stock_issue_iresults = i_add_stock_issue('', $indent_id, '', '', '', $user, '');
		if ($stock_issue_iresults["status"] == SUCCESS) {
		$issue_id = $stock_issue_iresults["data"];
		if (($issued_qty + $qty) <= $indent_qty) {
				if ($allow == true) {
						$stock_issue_item_irsults = i_add_stock_issue_item($issue_id, $material_id, $qty, $machine_id, $project_id, $remarks, $user);
						if ($stock_issue_item_irsults["status"] == SUCCESS)
						{
								$stock_issue_id  = $stock_issue_item_irsults["data"];
								//Get stock quantity
								$material_stock_search_data = array("material_id" => $material_id,"project" => $project_id);
								$stock_material_data        = i_get_material_stock($material_stock_search_data);
								if ($stock_material_data["status"] == SUCCESS) {
										$material_stock_qty = $stock_material_data["data"][0]["material_stock_quantity"];
								} else {
										$material_stock_qty = "0";
								}
								$total_qty  = (string) ($material_stock_qty - $qty);
								//update stock
								$material_stock_update_data = array("quantity" => ($total_qty),"material_id" => $material_id);
								$material_update_stock_data = i_update_material_stock($material_id, $project_id, $material_stock_update_data);
								//Add qty to stock history table
								$stock_history_data         = i_add_stock_history($material_id, 'Issue', $project_id, $qty, '', $user, $stock_issue_id);
								// Update item to completed
								if (($issued_qty + $qty) >= $indent_qty) {
										$indent_items_update_data = array("status" => 'Completed');
										$approve_files_result  = i_update_indent_items($indent_item_id, '', $indent_items_update_data);
								}
								$actual_material = db_add_actual_material($indent_id,$stock_issue_id,$pm_project,$process,$task,$road,$material_id,$machine_id,$qty,$remarks,$user);
								if($actual_material["status"] == SUCCESS)
								{

								}
								// header('location:stock_indent_issue_list.php');
								$alert_type = 1;
								$alert      = "Item Issued Successfully";
						}
				} else {
						$alert_type = 0;
						$alert      = "Quantity cannot be 0";
				}
		} else {
				$alert_type = 0;
				$alert      = "Issue quantity exceeding indent quantity";
		}
}
		echo 'RESPONSE';
		exit;
	}
}
else
{
	header("location:login.php");
}
?>
