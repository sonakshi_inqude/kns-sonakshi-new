<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Query String Data
	if(isset($_REQUEST['project_process_task_id']))
	{
		$task_id = $_REQUEST['project_process_task_id'];
	}
	else
	{
		$task_id =  $_REQUEST['hd_task_id'];
	}

	if(isset($_REQUEST['process_id']))
	{
		$process_id = $_REQUEST['process_id'];
	}
	else
	{
		$process_id = '';
	}

	if(isset($_REQUEST['status']))
	{
		$status = $_REQUEST['status'];
	}
	else
	{
		$status = '';
	}

	if(isset($_REQUEST['road_id']))
	{
		$road_id = $_REQUEST['road_id'];
	}
	else
	{
		$road_id = $_REQUEST['hd_road_id'] ;
	}

	if(isset($_REQUEST['project_id']))
	{
		$project_id = $_REQUEST['project_id'];
	}
	else
	{
		$project_id = $_REQUEST['hd_project_id'];
	}

	//Get Regular Measurement
	$get_details_data = i_get_details('Regular', $task_id, $road_id);
	$reg_mp_msmrt = $get_details_data["mp_msmrt"];
	$planned_msmrt = $get_details_data["planned_msmrt"];
	$uom = $get_details_data["uom"];
	$reg_overall_msmrt = $get_details_data["overall_msmrt"];
	$road_name = $get_details_data["road"];
	$reg_mc_msmrt = $get_details_data["mc_msmrt"];
	$reg_cw_msmrt = $get_details_data["cw_msmrt"];
	$project = $get_details_data["project"];
	$process_name = $get_details_data["process"];
	$task = $get_details_data["task"];

	//Get Rework Measurement
	$get_details_data = i_get_details('Rework', $task_id, $road_id);
	$rework_mp_msmrt = $get_details_data["mp_msmrt"];
	$rework_mc_msmrt = $get_details_data["mc_msmrt"];
	$rework_cw_msmrt = $get_details_data["cw_msmrt"];
	$rework_overall_msmrt = $get_details_data["overall_msmrt"];

	//Get Email list
	$project_email_search_data = array("project_id"=>$project_id);
	$email_list = db_get_project_email_list($project_email_search_data);
	if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
			$email_list_data = $email_list["data"];
			$to = $email_list_data[0]["project_email_details_to"];
			$cc = $email_list_data[0]["project_email_details_cc"];
			$bcc = $email_list_data[0]["project_email_details_bcc"];
	}
	// Capture the form data
	if(isset($_POST["add_delay_reason_submit"]))
	{
		$task_id     = $_POST["hd_task_id"];
		$road_id     = $_POST["hd_road_id"];
		$project_id  = $_POST["hd_project_id"];
		$process_id     = $_POST["hd_process_id"];
		$name	 	 = $_POST["ddl_reason_id"];
		$reason_name	 	 = $_POST["ddl_reason_id"];
		$start_date	 = date("Y-m-d H:i:s");
		$remarks     = $_POST["txt_remarks"];
		$reason = explode(",",$name);

		// Check for mandatory fields
		if(($name != ""))
		{
			$delay_reason_iresult = i_add_project_delay_reason($task_id,$road_id ,$reason[0],$start_date,'',$remarks,$user);

			if($delay_reason_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$project_process_task_update_data = array("status"=>'Pause',"road_id"=>$road_id);
				$process_task = i_update_project_process_task($task_id,$project_process_task_update_data);
				$subject = '[Notice] Project : '.$project . ' has been paused';

// 				$message = 'Project : ' . $project. ',<br /><br />Process : '. $process_name.
// 				',<br /><br />Task : ' . $task.     ',<br /><br />Road : '.  $road_name.',<br /><br />Paused By : '. $loggedin_name.',<br /><br />Paused By : '.date("d-M-Y h:i:s").
// ',<br /><br />Reason : '. $reason[1];
					$heading = "<h3>
						Paused By : <span style='color:#00ba8b;'>".$loggedin_name ."</span>
						<h3>
						<?h3>
						Paused On: <span style='color:#00ba8b;'>". date('d-m-y h:m A') ."</span>
						</h3>
						<br/>";
				$table = "<table  style=border-collapse:collapse;width:50%>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099; text-align:center;'>Project</td>
				<td style='border: 1px solid #ddd;padding: 8px; text-align:center;'>".$project."</td>
				</tr>
		    <tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Process</td>
			 <td style='border: 1px solid #ddd;padding: 8px; text-align:center;'>".$process_name."</td>
				</tr>
		    <tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Task</td>
 			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$task."</td>
				</tr>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Road</td>
 			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$road_name."</td>
				</tr>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Reason</td>
 			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$reason[1]."</td>
				</tr>
				<tr>
				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Remarks</td>
 			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$remarks."</td>
				</tr>";
					$message = $heading;
						$message .= $table;

				$cc = explode(',',$cc);

	      $cc_string = '[';
	    	for($count = 0; $count < count($cc); $count++)
	    	{
	    		$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
	    	}
	    	$cc_string = trim($cc_string,',');
	    	$cc_string = $cc_string.']';

				$bcc = explode(',',$bcc);
				$bcc_string = '[';
	    	for($count = 0; $count < count($bcc); $count++)
	    	{
	    		$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
	    	}
	    	$bcc_string = trim($bcc_string,',');
	    	$bcc_string = $bcc_string.']';

	    	$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
	    		"personalizations": [
	    			{
	    				"to": [
	    					{
	    					  "email": "'.$to.'"
	    					}
	    				],
	    				"bcc": '.$bcc_string.',
	    				"cc": '.$cc_string.',
	    				"subject": "'.$subject.'"
	    			}
	    		],
	    		"from": {
	    			"email": "venkataramanaiah@knsgroup.in"
	    		},
	    		"content": [
	    			{
	    				"type": "text/html",
	    				"value": "'.$message.'"
	    			}
	    		]
	    	}'));

	    	$apiKey = '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';
	    	$sg = new \SendGrid($apiKey);


	    	$response = $sg->client->mail()->send()->post($request_body);
			}
			header("location:project_task_planning_list.php");
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Delay Reason Master
	$project_reason_master_search_data = array('active'=>'1');
	$project_reason_master_list = i_get_project_reason_master($project_reason_master_search_data);
	if($project_reason_master_list["status"] == SUCCESS)
	{
		$project_reason_master_list_data = $project_reason_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_reason_master_list["data"];
	}

	// Get Project Delay Reason
	$delay_reason_search_data = array("task_id"=>$task_id);
	$project_delay_reason_list = i_get_project_delay_reason($delay_reason_search_data);
	if($project_delay_reason_list["status"] == SUCCESS)
	{
		$project_delay_reason_list_data = $project_delay_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_delay_reason_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Master - Add Delay Reason</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->.

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">
							<div class="widget-header custom-header">
								<i class="icon-user"></i>
								<h3>
									<span class="header-label">Project :</span> <?= $project; ?>
									<span class="header-label">Process :</span> <?= $process_name; ?>
									<span class="header-label">Task:</span>  <?= $task; ?>
								</h3>
								<h3 style="margin-left:25px">
									<span class="header-label">Road:</span>  <?= $road_name; ?>
									<span class="header-label">Planned Msrmnt:</span> <?= $planned_msmrt; ?>
									<span class="header-label">UOM</span> <?= $uom; ?>
								</h3>
							</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Master Add Delay Reason Master</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_master_add_process_form" class="form-horizontal" method="post" action="project_update_delay_reason_master.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
								<input type="hidden" name="hd_road_id" value="<?php echo $road_id; ?>" />
								<input type="hidden" name="hd_project_id" value="<?php echo $project_id; ?>" />
									<?php if($status == "Pause")
									{?>
									<fieldset>

											<div class="control-group">
											<label class="control-label" for="ddl_reason_id">Delay Reason*</label>
											<div class="controls">
												<select name="ddl_reason_id" required >
												<option value="">- - Select Delay - -</option>
												<?php
												for($count = 0; $count < count($project_reason_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_reason_master_list_data[$count]["project_reason_master_id"] .','. $project_reason_master_list_data[$count]["project_reason_master_name"]; ?> "><?php echo $project_reason_master_list_data[$count]["project_reason_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="start_date">Start Date</label>
											<div class="controls">
												<input type="datetime" class="span6" value="<?php echo date("d-M-Y H:i:s") ;?>" disabled name="start_date">
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea type="text" class="span6" name="txt_remarks" placeholder="Remarks"></textarea>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />


										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_delay_reason_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								<?php
									}
									?>
								</div>

							</div>
							<table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Task</th>
					<th>Road</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Reason</th>
					<th>Remarks</th>
					<th colspan="1" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_delay_reason_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_delay_reason_list_data); $count++)
					{
						if(($project_delay_reason_list_data[$count]["project_task_delay_reason_end_date"] == "") || (($project_delay_reason_list_data[$count]["project_task_delay_reason_end_date"] == "0000-00-00 00:00:00")))
						{
							$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_delay_reason_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_delay_reason_list_data[$count]["project_site_location_mapping_master_name"]; ?></td>
					<td><?php echo date("d-M-Y H:i:s",strtotime($project_delay_reason_list_data[$count]["project_task_delay_reason_start_date"])); ?></td>
					<td><?php echo get_formatted_date(($project_delay_reason_list_data[$count]["project_task_delay_reason_end_date"]),"d-M-Y"); ?></td>
					<td><?php echo $project_delay_reason_list_data[$count]["project_reason_master_name"]; ?></td>
					<td><?php echo $project_delay_reason_list_data[$count]["project_task_delay_reason_remarks"]; ?></td>
					<td><?php if(($project_delay_reason_list_data[$count]["project_task_delay_reason_active"] == "1")){?><a style="padding-right:10px" href="#" onclick="return go_to_release_reason_master('<?php echo $project_delay_reason_list_data[$count]["project_task_delay_reason_task_id"]; ?>','<?php echo $project_delay_reason_list_data[$count]["project_task_delay_reason_id"] ;?>','<?php echo $process_id; ?>','<?php echo $project_id ;?>','<?php echo $project_delay_reason_list_data[$count]["project_task_delay_reason_road_id"] ; ?>');">Release</a><?php } ?></td>
					</tr>
					<?php
					}
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function go_to_release_reason_master(project_process_task_id,reason_id,process_id,project_id,road_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_release_task_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","reason_id");
	hiddenField2.setAttribute("value",reason_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","process_id");
	hiddenField3.setAttribute("value",process_id);

	var hiddenField4 = document.createElement("input");
	hiddenField4.setAttribute("type","hidden");
	hiddenField4.setAttribute("name","project_id");
	hiddenField4.setAttribute("value",project_id);

	var hiddenField5 = document.createElement("input");
	hiddenField5.setAttribute("type","hidden");
	hiddenField5.setAttribute("name","road_id");
	hiddenField5.setAttribute("value",road_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	form.appendChild(hiddenField4);
	form.appendChild(hiddenField5);

	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>
