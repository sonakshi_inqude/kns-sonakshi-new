<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'CRM Transactions';

/* DEFINES - START */
define('CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID', '111');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list    = i_get_user_perms($user, '', CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID, '2', '1');
    $edit_perms_list    = i_get_user_perms($user, '', CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID, '3', '1');
    $delete_perms_list  = i_get_user_perms($user, '', CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', CRM_UNQUALIFIABLE_ENQUIRY_LIST_FUNC_ID, '6', '1');

?>
  <script>
    window.permissions = {
      view: <?php echo ($view_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      edit: <?php echo ($edit_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      ok: <?php echo ($ok_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      delete: <?php echo ($delete_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      aprove: <?php echo ($approve_perms_list['status'] == 0)? 'true' : 'false'; ?>,
      user: <?php echo $user ?>,
      role: <?php echo $role ?>,
    }
  </script>
  <?php
    // Query String Data
    // Nothing

    $alert_type = -1;
    $alert = "";

    $crm_user_project_mapping_search_data =  array("user_id"=>$user,"project_active"=>'1',"active"=>'1');
	  $project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
    if($project_list["status"] == SUCCESS)
    {
      $project_list_data = $project_list["data"];
    }
    else
    {
      $alert = $alert."Alert: ".$project_list["data"];
    }

    // User data
    if($approve_perms_list['status'] == SUCCESS)
  	{
  		$manager = '';
  	}
    else{
  		$manager = $user;
    }
    $user_list = i_get_user_list('','','','','1','1',$manager);
  	if($user_list["status"] == SUCCESS)
  	{
  		$user_list_data = $user_list["data"];
  	}
  	else
  	{
  		$alert = $alert."Alert: ".$user_list["data"];
  		$alert_type = 0; // Failure
  	}

  	// Source List
  	$source_list = i_get_enquiry_source_list('','1');
  	if($source_list["status"] == SUCCESS)
  	{
  		$source_list_data = $source_list["data"];
  	}
  	else
  	{
  		$alert = $alert."Alert: ".$source_list["data"];
  	}
} else {
    header("location:login.php");
}
?>


    <html>

    <head>
      <meta charset="utf-8">
      <title>Enquiries marked as unqualifiable</title>

      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <link href="./css/style.css" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
      <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
      <link href="./bootstrap_aku.min.css" rel="stylesheet">
      <link href="css/style1.css" rel="stylesheet">

      <style media="screen">
        table.dataTable {
          margin-top: 0px !important;
        }
      </style>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js?22062018"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.js?21062018"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
      <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
      <script src="datatable/crm_enquiry_list_unq_datatable.js?<?php echo time(); ?>"></script>
    </head>

    <body>

      <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
    ?>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
            </div>
          </div>
        </div>
        <div class="main margin-top">
          <div class="main-inner">
            <div class="container">
              <div class="row">

                <div class="span6">

                  <div class="widget widget-table action-table">
                    <div class="widget-header">
                      <h3>Enquiries marked as unqualifiable </h3>
                    </div>
                    <div style="height:91px !important" class="widget-header widget-toolbar">
                      <?php
                          if ($view_perms_list['status'] == SUCCESS) {
                              ?>
                        <form class="form-inline" id="file_search_form">
                          <select id="ddl_project" name="ddl_project" class="form-control input-sm" style="max-width: 250px;">
                        <option value="">- - Select Project - -</option>
                        <?php
                        for($count = 0; $count < count($project_list_data); $count++)
                				{
                					?>
                					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>">
                            <?php echo $project_list_data[$count]["project_name"]; ?></option>
                					<?php
                				}
                      		  ?>
                			  </select>

                        <input type="text" placeholder="Search user phone number" id="cell" name="cell" class="form-control input-sm"/>
                        <input type="text" placeholder="Search Enquiry Number" id="enquiry_number" name="enquiry_number" class="form-control input-sm"/>
                          <select id="ddl_search_source" name="ddl_search_source" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select Source - -</option>
                        <?php

                       for($count = 0; $count < count($source_list_data); $count++)
                       {
                         ?>
                         <option value="<?php echo $source_list_data[$count]["enquiry_source_master_id"]; ?>">
                         <?php echo $source_list_data[$count]["enquiry_source_master_name"]; ?></option>
                         <?php
                       }
                           ?>
                        </select>

                          <select id="ddl_search_assigned_to" name="ddl_search_assigned_to" class="form-control input-sm" style="max-width: 150px;">
                        <option value="">- - Select User - -</option>
                        <?php
                        for($count = 0; $count < count($user_list_data); $count++)
                        {
                          ?>
                          <option value="<?php echo $user_list_data[$count]["user_id"]; ?>">
                             <?php echo $user_list_data[$count]["user_name"]; ?></option>
                          <?php
                        }
                            ?>
                        </select>
                          <button type="button" onclick="drawTable()"class="btn btn-primary">Submit</button>
                        </form>
                        <?php } ?>
                    </div>
                  </div>
                  <?php if($view_perms_list['status']==SUCCESS){ ?>
                  <div class="widget-content" style="margin-top:15px;">
                    <table id="example" class="table table-striped table-bordered display nowrap">
                      <thead>
                        <tr>
                          <th>SL No</th>
                          <th>Name</th>
                          <th>Mobile</th>
                          <th>Enquiry No</th>
                          <th>Project</th>
                          <th>Reason For Unqualified</th>
                          <th>Source</th>
                          <th>Enquiry Date</th>
                          <th>SV count</th>
                          <th>F count</th>
                          <th>Walk In</th>
                          <th>Assignee</th>
                          <th>Requested On</th>
                          <th>Unqualified Requested By</th>
                          <th>F</th>
                        </tr>
                      </thead>
                      </tbody>
                    </table>
                  </div>
                  <?php
                }
                else{ ?>
                  <div class="widget-content">
                   <h1>Access Denied</h1>
                   <h3>You dont have permissions to view the Document</h3>
                 </div>
                 <?php
                }
                  ?>
                </div>
                <!-- /widget-content -->
              </div>
              <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
        <!-- </div> -->
        <!-- /container -->
        <!-- </div> -->

    </body>

    <div class="extra">

      <div class="extra-inner">

        <div class="container">

          <div class="row">

          </div>
          <!-- /row -->

        </div>
        <!-- /container -->

      </div>
      <!-- /extra-inner -->

    </div>
    <!-- /extra -->




    <div class="footer">

      <div class="footer-inner">

        <div class="container">

          <div class="row">

            <div class="span12">
              &copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
            </div>
            <!-- /span12 -->

          </div>
          <!-- /row -->

        </div>
        <!-- /container -->

      </div>
      <!-- /footer-inner -->

    </div>
    <!-- /footer -->

    </html>
