<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 7th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Query string
	if(isset($_REQUEST["enquiry"]))
	{
		$enquiry_id = $_REQUEST["enquiry"];
	}
	else
	{
		$enquiry_id = "";
	}
	// Capture the form data
	if(isset($_POST["add_assign_enquiry_submit"]))
	{
		$enquiry_id = $_POST["hd_enquiry"];
		$assignee   = $_POST["ddl_assigned_to"];

		// Check for mandatory fields
		if(($enquiry_id !="") && ($assignee !=""))
		{
			$enquiry_uresult = i_update_enquiry_assignee($enquiry_id,$assignee);

			if($enquiry_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;

				if($role == 1)
				{
					header("location:crm_enquiry_list.php");
				}
				else if($role == 5)
				{
					header("location:crm_enquiry_list_unq.php");
				}
				else
				{
					header("location:crm_enquiry_fup_latest.php");
				}
			}
			else
			{
				$alert_type = 0;
			}

			$alert = $enquiry_uresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}

	// Enquiry List
	$enquiry_list = i_get_enquiry_list($enquiry_id,'','','','','','','','','','','','','','','','','','','');
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}
?>


							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Assign Enquiry</h4>
							</div>
							<div class="modal-body">
								<form id="assign_enquiry">
									<input type="hidden" id="hd_enquiry" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
										 <div class="form-group">
											 <label class="control-label" for="ddl_project">Enquiry Details</label>
											 <div class="form-group">
												 Enquiry Number: <strong><?php echo $enquiry_list_data[0]["enquiry_number"]; ?></strong><br/>
												 Customer Name: <strong><?php echo $enquiry_list_data[0]["name"]; ?></strong><br/>
												 Customer Mobile: <strong><?php echo $enquiry_list_data[0]["cell"]; ?></strong><br/>
											 </div>
										 </div>
			 								<div class="form-group">
												<label class="control-label" for="ddl_assigned_to">Assigned To*</label>
			 									<select id="ddl_assigned_to" name="ddl_assigned_to" class="form-control" required>
			 									<?php
												foreach ($user_list_data as $item) { ?>
			 										<option value="<?php echo $item["user_id"]; ?>" >
													<?php echo $item["user_name"]; ?>
												</option>
			 										<?php
			 									}
			 									?>
			 									</select>
			 								</div> <!-- /controls -->
									</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary" onclick="enquiry_submit()">Submit</button>
							</div>
