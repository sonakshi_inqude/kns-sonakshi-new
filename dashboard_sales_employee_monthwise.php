<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Search parameters
	if(isset($_POST["sales_results_employeewise_filter_submit"]))
	{		
		$selected_date = $_POST['dt_select_date'];			
	}
	else
	{	
		$selected_date = date('Y-m-d');		
	}
	
	// Get list of sales department employees
	$user_result = i_get_user_list('','','','','1','1');	
	if($user_result['status'] == SUCCESS)
	{
		$user_result_data = $user_result['data'];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Sales Report - Monthly Performance</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px; padding-top:10px;"> <i class="icon-th-list"></i>
              <h3>Employeewise Marketing Report</h3>		  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="sales_daywise_filter_form" action="dashboard_sales_employee_monthwise.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_select_date" value="<?php echo $selected_date; ?>" />
			  </span>			  
			  <input type="submit" name="sales_results_employeewise_filter_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">Employee</th>
					<th style="word-wrap:break-word;">Yesterday</th>
					<th style="word-wrap:break-word;">This Week</th>				
					<?php 
					for($count = 0; $count < 12; $count++)
					{						
						$month_start = get_month_start_date(date('Y-m-d'),$count);
					?>
					<th style="word-wrap:break-word;"><?php echo date('M-Y',strtotime($month_start)); ?></th>					
					<?php
					}
					?>
					<th style="word-wrap:break-word;">Total</th>
				</tr>
				</thead>
				<tbody>
				<?php									
				if($user_result["status"] == SUCCESS)
				{
					for($count = 0; $count < count($user_result['data']); $count++)
					{	
						// No of sales - past year
						$year_month = date('m-d',strtotime(get_month_start_date($selected_date,-1)));
						$year_year  = (date('Y',strtotime(get_month_start_date($selected_date,-1)))) - 1;
						$year_start_date = $year_year.'-'.$year_month;
						$year_end_date   = $selected_date;
						$booking_filter_data = array('booking_start_date'=>$year_start_date,'booking_end_date'=>$year_end_date,'added_by'=>$user_result['data'][$count]['user_id'],'status'=>'1');
						$sales_result = i_get_booking_summary_data($booking_filter_data);
						$sales_count  = $sales_result['data']['salecount'];
						$sales_area   = $sales_result['data']['salearea'];
						$user_result['data'][$count]['pcount'] = $sales_count;
					}
					
					array_sort_conditional($user_result['data'],'sort_on_number');
					?>
					<tr>
					<?php
					for($ucount = 0; $ucount < count($user_result['data']); $ucount++)
					{	
						// No of sales - yesterday
						$yesterday_date = date('Y-m-d',strtotime($selected_date.' -1 days'));
						$booking_filter_data = array('booking_start_date'=>$yesterday_date,'booking_end_date'=>$yesterday_date,'added_by'=>$user_result['data'][$ucount]['user_id'],'status'=>'1');
						$sales_result = i_get_booking_summary_data($booking_filter_data);
						$yest_sales_count  = $sales_result['data']['salecount'];
						$yest_sales_area   = $sales_result['data']['salearea'];
						
						// No of sales - this week
						// Start and end date of week	
						$week_start_date = $current_date;
						$week_end_date   = $current_date;
						
						while(date("D",strtotime($week_start_date)) != "Mon")
						{
							$week_start_date = date("Y-m-d",strtotime($week_start_date." -1 days"));
						}
						while(date("D",strtotime($week_end_date)) != "Sun")
						{
							$week_end_date = date("Y-m-d",strtotime($week_end_date." +1 days"));
						}
						$booking_filter_data = array('booking_start_date'=>$week_start_date,'booking_end_date'=>$week_end_date,'added_by'=>$user_result['data'][$ucount]['user_id'],'status'=>'1');
						$sales_result = i_get_booking_summary_data($booking_filter_data);
						$week_sales_count  = $sales_result['data']['salecount'];
						$week_sales_area   = $sales_result['data']['salearea'];
						?>						
						<td style="word-wrap:break-word;"><?php echo $user_result['data'][$ucount]['user_name']; ?></td>
						<td style="word-wrap:break-word;"><?php echo $yest_sales_count; ?></td>
						<td style="word-wrap:break-word;"><?php echo $week_sales_count; ?></td>
						<?php
						$total_count = 0;
						for($count = 0; $count < 12; $count++)
						{						
							$month_start = get_month_start_date(date('Y-m-d'),$count);
							$month_end   = date('Y-m',strtotime($month_start)).'-31';													
							
							// No of sales - this month
							$booking_filter_data = array('booking_start_date'=>$month_start,'booking_end_date'=>$month_end,'added_by'=>$user_result['data'][$ucount]['user_id'],'status'=>'1');
							$sales_result = i_get_booking_summary_data($booking_filter_data);
							$sales_count  = $sales_result['data']['salecount'];
							$sales_area   = $sales_result['data']['salearea'];
							
							$total_count = $total_count + $sales_count;
							
							?>						
							<td style="word-wrap:break-word;"><?php echo $sales_count; ?></th>
							<?php
						}
						?>
						<td style="word-wrap:break-word;"><?php echo $total_count; ?></th>
						</tr>
						<?php
					}
						?>					
				<?php
				}
				else
				{
				?>
				<td colspan="4">No marketing source type added!</td>
				<?php
				}
				?>	

                </tbody>
              </table>	
			  <script>
			  document.getElementById('total_leads').innerHTML = <?php echo $total_num_leads; ?>;
			  </script>
            </div>
			
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    
<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
