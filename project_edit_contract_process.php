<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 05-May-2017
// LAST UPDATED BY: Ashwini
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["contract_process_id"]))
	{		
		$contract_process_id = $_GET["contract_process_id"];				
	}
	else
	{
		$contract_process_id = "-1";
	}

	// Capture the form data
	if(isset($_POST["edit_project_contract_process_submit"]))
	{
		$contract_process_id = $_POST["hd_contract_process_id"];
		$name	             = $_POST["txt_name"];
		$remarks             = $_POST["txt_remarks"];
	
		// Check for mandatory fields
		if(($name != ""))
		{
			$project_contract_process_update_data = array("name"=>$name,"remarks"=>$remarks);
			$project_contract_process_update_iresult = i_update_project_contract_process($contract_process_id,$project_contract_process_update_data);
			
			
			if($project_contract_process_update_iresult["status"] == SUCCESS)
				
			{	
			  $alert_type = 1;
			 header("location:project_contract_process_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;	
			}
			
			$alert = $project_contract_process_update_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			
		}
	}
	
	
	// Get project_contract_process modes already added
	$project_contract_process_search_data = array("contract_process_id"=>$contract_process_id,"active"=>'1');
	$project_contract_process_list = i_get_project_contract_process($project_contract_process_search_data);
	//var_dump($project_contract_process_list); 
	if($project_contract_process_list['status'] == SUCCESS)
	{
		$project_contract_process_list_data = $project_contract_process_list['data'];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project Contract Process Master</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Contract Process Master Condition</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Contract Process Master Condition</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_project_contract_process_form" class="form-horizontal" method="post" action="project_edit_contract_process.php">
								<input type="hidden" name="hd_contract_process_id" value="<?php echo $contract_process_id; ?>" />
									<fieldset>											
																				
										<div class="control-group">											
											<label class="control-label" for="txt_name">Name*</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_name" value="<?php echo $project_contract_process_list_data[0]
												["project_contract_process_name"] ;?>" placeholder="Name" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
											<input type="text" class="span6" value="<?php echo $project_contract_process_list_data[0]["project_contract_process_remarks"] ;?>" 
											name="txt_remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                        <br />
																					
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_contract_process_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
						</div>					
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->  
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
