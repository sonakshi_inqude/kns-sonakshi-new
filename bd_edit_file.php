<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 12th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
/* INCLUDES - END */
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1; // No alert
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["file"]))
	{
		$bd_file_id = $_GET["file"];
	}
	else
	{
		$bd_file_id = "";
	}
	if(isset($_GET["src"]))
	{
		$source = $_GET["src"];
	}
	else
	{
		$source = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["bd_edit_file_submit"]))
	{
		$bd_file_id       = $_POST["hd_bd_file_id"];
		$source           = $_POST["hd_source"];
		$project_id       = $_POST["ddl_project"];
		$survey_alias     = $_POST["stxt_survey_alias"];
		$survey_alias_date= $_POST["stxt_survey_alias_date"];
		$survey_no        = $_POST["stxt_survey_no"];
		$owner            = $_POST["stxt_owner"];
		$owner_address    = $_POST["txt_owner_addres"];
		$owner_phone	  = $_POST["num_phone_no"];
		$village          = $_POST["ddl_village"];
		$extent           = $_POST["stxt_extent"];		
		$cost             = $_POST["stxt_cost"];
		$broker_name	  = $_POST["stxt_name"];
		$broker_address	  = $_POST["txt_address"];
		$brokerage_amount = $_POST["num_brokerage_amt"];
		$process_status   = $_POST["ddl_process_status"];
		$own_account      = $_POST["ddl_own_account"];
		$jd_share_percent = $_POST["stxt_share_percent"];
		$remarks          = $_POST["txt_remarks"];
		if(isset($_POST["cb_active"]))
		{
			$active = 1;
		}
		else
		{
			$active = 0;
		}
		// Check for mandatory fields
		if(($project_id !="") && ($survey_no !="") && ($owner !="") && ($village !="") && ($extent !=""))
		{
			$edit_bd_file_result =  i_edit_bd_file($bd_file_id,$project_id,$survey_no,$owner,$owner_address,$owner_phone,$village,$extent,'',$cost,$process_status,$broker_name,$broker_address,$jd_share_percent,$own_account,$remarks,$active,$brokerage_amount,'sales_deed_'.$survey_alias,$survey_alias_date);
			
			if($edit_bd_file_result["status"] == SUCCESS)
			{
				$alert = $edit_bd_file_result["data"];
				$alert_type = 1;
				
				if($active == 1)
				{
					$file_enable_result = i_add_file_to_project($bd_file_id,$user);
				}
				else
				{
					$remove_file_result = i_remove_file_from_project($bd_file_id,$user);
				}
				
				if($source == 'completed')
				{
					header("location:completed_file_list.php");
				}
				
				else
				{
					header("location:bd_file_list.php");
				}
			}
			else
			{
				$alert = $edit_bd_file_result["data"];
				$alert_type = 0;
			}	
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get file details	
	$bd_file_list = i_get_bd_files_list($bd_file_id,'','','','','','','');	
	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_file_list["data"];
	}	
	
	// Get list of BD projects
	$bd_project_list = i_get_bd_project_list('','','','');
	if($bd_project_list["status"] == SUCCESS)
	{
		$bd_project_list_data = $bd_project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$bd_project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of villages
	$village_list = i_get_village_list('');
	if($village_list["status"] == SUCCESS)
	{
		$village_list_data = $village_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$village_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of owner status
	$owner_status_list = i_get_owner_status_list('');
	if($owner_status_list["status"] == SUCCESS)
	{
		$owner_status_list_data = $owner_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$owner_status_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of process status
	$process_status_list = i_get_process_type_list('','');
	if($process_status_list["status"] == SUCCESS)
	{
		$process_status_list_data = $process_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_status_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get list of KNS accounts
	$own_account_list = i_get_account_list('','1');
	if($own_account_list["status"] == SUCCESS)
	{
		$own_account_list_data = $own_account_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$own_account_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit BD File</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit BD File</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit BD File</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_bd_file_form" class="form-horizontal" method="post" action="bd_edit_file.php">
								<input type="hidden" name="hd_bd_file_id" value="<?php echo $bd_file_id; ?>" />
								<input type="hidden" name="hd_source" value="<?php echo $source; ?>" />
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label" for="ddl_project">Project *</label>
											<div class="controls">
												<select name="ddl_project" required>
												<?php
												for($count = 0; $count < count($bd_project_list_data); $count++)
												{
												?>
												<option value="<?php echo $bd_project_list_data[$count]["bd_project_id"]; ?>" <?php if($bd_project_list_data[$count]["bd_project_id"] == $bd_file_list_data[0]["bd_mapped_project_id"]){ ?> selected="selected" <?php } ?>><?php echo $bd_project_list_data[$count]["bd_project_name"]; ?></option>								
												<?php
												}
												?>														
												</select>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_survey_no">Survey Number *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_survey_no" placeholder="Dont use spaces. Use only / symbol" required="required" value="<?php echo $bd_file_list_data[0]["bd_file_survey_no"]; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->								
										
										<?php 
										$survey_alias = $bd_file_list_data[0]["bd_file_survey_sale_deed"];
										$alias_num = explode("_", $survey_alias);
										?>
										<div class="control-group">											

											<label class="control-label" for="stxt_survey_alias">Survey Alias *</label>

											<div class="controls">

												<input type="number" class="span6" name="stxt_survey_alias" placeholder="Sales Deed :" required="required"
												value="<?php echo $alias_num[2]; ?>">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										
										<div class="control-group">											

											<label class="control-label" for="stxt_survey_alias_date">Survey Alias Date *</label>

											<div class="controls">

												<input type="date" class="span6" name="stxt_survey_alias_date" required="required" value="<?php echo $bd_file_list_data[0]["bd_file_survey_sale_deed_date"]; ?>">

											</div> <!-- /controls -->					

										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_owner">Land Owner *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_owner" value="<?php echo $bd_file_list_data[0]["bd_file_owner"]; ?>" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_owner_addres">Land Owner Address</label>
											<div class="controls">
												<textarea name="txt_owner_addres"><?php echo $bd_file_list_data[0]["bd_file_owner_address"]; ?></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_phone_no">Land Owner Phone No *</label>
											<div class="controls">
												<input type="number" class="span6" name="num_phone_no" value="<?php echo $bd_file_list_data[0]["bd_file_owner_phone_no"]; ?>" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_village">Village *</label>
											<div class="controls">
												<select name="ddl_village" required>
												<?php
												for($count = 0; $count < count($village_list_data); $count++)
												{
												?>
												<option value="<?php echo $village_list_data[$count]["village_id"]; ?>" <?php if($village_list_data[$count]["village_id"] == $bd_file_list_data[0]["bd_file_village"])
												{
												?>												
												selected="selected"
												<?php
												}
												?>><?php echo $village_list_data[$count]["village_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_extent">Extent *</label>
											<div class="controls">
												<input type="number" class="span6" name="stxt_extent" value="<?php echo $bd_file_list_data[0]["bd_file_extent"]; ?>" required="required" min="0" step="0.01">
												<p class="help-block">Enter in guntas</p>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->																				
										
										<div class="control-group">											
											<label class="control-label" for="ddl_owner_status">Land Status</label>
											<div class="controls">
												<?php echo $bd_file_list_data[0]["bd_file_owner_status_name"]; ?>											
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_cost">Cost</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_cost" value="<?php echo $bd_file_list_data[0]["bd_file_land_cost"]; ?>" >
												<p class="help-block">Full land cost. Not per acre cost.<br /> <span style="color:red;">To be finalized by finance team</span></p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_name">Broker Name *</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_name" value="<?php echo $bd_file_list_data[0]["bd_file_broker_name"]; ?>" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_address">Broker Address *</label>
											<div class="controls">
												<textarea name="txt_address"><?php echo $bd_file_list_data[0]["bd_file_broker_address"]; ?></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_brokerage_amt">Brokerage Amount *</label>
											<div class="controls">
												<textarea name="num_brokerage_amt"><?php echo $bd_file_list_data[0]["bd_file_brokerage_amount"]; ?></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_process_status">Process Status *</label>
											<div class="controls">
												<select name="ddl_process_status" required>
												<?php
												for($count = 0; $count < count($process_status_list_data); $count++)
												{
												?>
												<option value="<?php echo $process_status_list_data[$count]["process_master_id"]; ?>" <?php if($process_status_list_data[$count]["process_master_id"] == $bd_file_list_data[0]["bd_file_process_status"])
												{
												?>												
												selected="selected"
												<?php
												}
												?>><?php echo $process_status_list_data[$count]["process_name"]; ?></option>								
												<?php
												}
												?>														
												</select>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_own_account">Account</label>
											<div class="controls">
												<select name="ddl_own_account" required>
												<option value=""> - - Select Account - -</option>
												<?php
												for($count = 0; $count < count($own_account_list_data); $count++)
												{
												?>
												<option value="<?php echo $own_account_list_data[$count]["bd_own_accunt_master_id"]; ?>" <?php if($bd_file_list_data[0]["bd_file_own_account"] == $own_account_list_data[$count]["bd_own_accunt_master_id"]) { ?> selected <?php } ?>><?php echo $own_account_list_data[$count]["bd_own_account_master_account_name"]; ?></option>								
												<?php
												}
												?>														
												</select>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_share_percent">JD %</label>
											<div class="controls">
												<input type="number" class="span6" name="stxt_share_percent" value="<?php echo $bd_file_list_data[0]["bd_project_file_jda_share_percent"]; ?>" min="0" step="0.01">%	
												<p class="help-block">DONT include % symbol. Only mention the number.<br /> <span style="color:red;">To be finalized by finance team</span></p>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks"><?php echo $bd_file_list_data[0]["bd_project_file_remarks"]; ?></textarea>
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_active">Active</label>
											<div class="controls">
												<input type="checkbox" name="cb_active" <?php if($bd_file_list_data[0]["bd_project_file_mapping_active"] == "1") {?> checked <?php } ?> />
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="bd_edit_file_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>