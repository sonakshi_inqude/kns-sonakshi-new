<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$alert_type = -1;
	$alert      = '';

	// Query String Data
	if(isset($_GET['case']))
	{
		$case = $_GET['case'];
	}
	else
	{
		header('location:bd_court_case_list.php');
	}

	// Get list of Documents
	$documents_search_data = array('case_id'=>$case);
	$document_list = i_get_court_case_documents_list($documents_search_data);

	if($document_list["status"] == SUCCESS)
	{
		$document_list_data = $document_list["data"];
	}
	else
	{
		$alert_Type = 0;
		$alert      = $alert."Alert: ".$document_list["data"];
	}
	
	// Get case details
	$case_details = array('legal_court_case_id'=>$case);
	$court_case_details = i_get_legal_court_case($case_details);
	if($court_case_details["status"] == SUCCESS)
	{
		$court_case_details_data = $court_case_details["data"];
	}
	else
	{
		$alert = $court_case_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Document List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:80px;"> <i class="icon-th-list"></i>
              <h3>Case Document List</h3><span style="float:right; padding-right:10px;"><a href="court_case_add_documents.php?case=<?php echo $case; ?>">Add Document</a></span>
			  <h3>Case Type: <?php echo $court_case_details_data[0]["bd_court_case_type_master_type"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Case No: <?php echo $court_case_details_data[0]["legal_court_case_number"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Survey No: <?php echo $court_case_details_data[0]["legal_court_case_survey_no"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Village: <?php echo $court_case_details_data[0]["village_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Filing Date: <?php echo date('d-M-Y',strtotime($court_case_details_data[0]["legal_court_case_date"])); ?>&nbsp;&nbsp;&nbsp;&nbsp;Jurisdiction: <?php echo $court_case_details_data[0]["bd_court_establishment_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Case Status: <?php echo $court_case_details_data[0]["bd_court_status_name"]; ?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Subject</th>
					<th>Added by</th>
				    <th>Added on</th>							
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$sl_no = 0;
				if($document_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($document_list_data); $count++)
					{						
						$sl_no++;
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $document_list_data[$count]["legal_court_case_documents_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $document_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($document_list_data[$count]["legal_court_case_documents_added_on"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><a href="legal_court_case_document/<?php echo $document_list_data[$count]["legal_court_case_documents_file_path"]; ?>" target="_blank">Download</a></td>
						
						
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="5">No Documents added!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
