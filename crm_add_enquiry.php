<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 5th Sep 2015

// LAST UPDATED BY: Nitin Kashyap

/* FILE HEADER - END */

define('CRM_ADD_ENQUIRY_FUNC_ID','100');


/* TBD - START */

/* TBD - END */
$_SESSION['module'] = 'CRM Transactions';


/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

/* INCLUDES - END */



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',CRM_ADD_ENQUIRY_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',CRM_ADD_ENQUIRY_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',CRM_ADD_ENQUIRY_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',CRM_ADD_ENQUIRY_FUNC_ID,'4','1');



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */



	// Capture the form data

	if(isset($_POST["add_enquiry_submit"]))

	{

		$project_id      = $_POST["ddl_project"];

		$name            = $_POST["stxt_cust_name"];

		$cell            = $_POST["stxt_cust_cell"];

		$email           = $_POST["stxt_cust_email"];

		$company         = $_POST["stxt_cust_company"];

		$location        = $_POST["stxt_cust_location"];

		$source          = $_POST["ddl_enq_src"];

		$remarks         = $_POST["txt_remarks"];

		$interest_status = $_POST["ddl_interest_status"];

		if(isset($_POST["cb_walk_in"]))

		{

			$walk_in      = 1;

			$walk_in_date = $_POST["dt_walk_in"];

			$walk_in_time = $_POST["stxt_walk_in_time"];

		}

		else

		{

			$walk_in      = 0;

			$walk_in_date = "";

			$walk_in_time = "";

		}

		$follow_up_date  = $_POST["dt_follow_up"];

		$assigned_to     = $_POST["ddl_assigned_to"];



		$is_preloaded    = $_POST['hd_is_preload'];

		$pl_unqual_id    = $_POST['hd_pl_unq_id'];



		// Check for mandatory fields

		if(($project_id !="") && ($name !="") && ($cell !="") && ($source !="") && ($interest_status !="") && ($follow_up_date !="") && ($assigned_to !=""))

		{

			if(strtotime($follow_up_date) < strtotime(date("Y-m-d")))

			{

				$alert_type = 0;

				$alert      = "Follow Up date cannot be lesser than current date time";

			}

			else

			{

				$enquiry_iresult = i_add_enquiry($project_id,$name,$cell,$email,$company,$location,$source,$remarks,$interest_status,$walk_in,$follow_up_date,$assigned_to,$user,$is_preloaded);



				if($enquiry_iresult["status"] == SUCCESS)

				{

					$alert_type = 1;



					// Update unqualified enquiry entry to inactive

					$unqual_reset_uresult = i_update_unqualified_enquiry($pl_unqual_id,'0');



					if($walk_in == 1)

					{

						// Add a site visit plan

						$svp_result = i_add_site_visit_plan($enquiry_iresult["data"]["value"],$walk_in_date,$walk_in_time,$project_id,'1','1','Walk In',$user);



						// Add a site travel plan

						$stp_result = i_add_site_travel_plan($svp_result["data"],'',$project_id,$walk_in_date." 00:00:00",$user);



						// Update the site travel plan as completed

						$stp_update_result = i_update_site_travel_plan($stp_result["data"],'2','');

					}

					$alert = $enquiry_iresult["data"]["message"];

				}

				else

				{

					$alert_type = 0;

					$alert      = $enquiry_iresult["data"];

				}

			}

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}



	// Project
	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');
	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}



	// Source

	$enq_source_list = i_get_enquiry_source_list('','1');

	if($enq_source_list["status"] == SUCCESS)

	{

		$enq_source_list_data = $enq_source_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$enq_source_list["data"];

		$alert_type = 0; // Failure

	}



	// Interest Status

	$interest_status_list = i_get_interest_status_list('','1');

	if($interest_status_list["status"] == SUCCESS)

	{

		$interest_status_list_data = $interest_status_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$interest_status_list["data"];

		$alert_type = 0; // Failure

	}



	// User List

	$user_list = i_get_user_list('','','','','1','1');

	if($user_list["status"] == SUCCESS)

	{

		$user_list_data = $user_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$user_list["data"];

		$alert_type = 0; // Failure

	}



	// To pre-load (pl) the fields

	$pl_name       = '';

	$pl_mobile     = '';

	$pl_email      = '';

	$pl_company    = '';

	$pl_location   = '';

	$pl_source     = '';

	$pl_int_status = '';

	$is_preload    = '0';

	$pl_enquiry_id = '';

	$pl_unqual_id  = '';

	if(isset($_POST['hd_navigate_enquiry_id']))

	{

		$pl_enquiry_id = $_POST['hd_navigate_enquiry_id'];

		$pl_unqual_id  = $_POST['hd_navigate_unqual_id'];



		// Get the enquiry details if it is a previously unqualified enquiry being renewed

		$enquiry_details = i_get_enquiry_list($pl_enquiry_id,'','','','','','','','','','','','','','','','','','','');

		if($enquiry_details['status'] == SUCCESS)

		{

			$pl_name       = $enquiry_details['data'][0]['name'];

			$pl_mobile     = $enquiry_details['data'][0]['cell'];

			$pl_email      = $enquiry_details['data'][0]['email'];

			$pl_company    = $enquiry_details['data'][0]['company'];

			$pl_location   = $enquiry_details['data'][0]['location'];

			$pl_source     = $enquiry_details['data'][0]['source'];

			$pl_int_status = '6';

			$is_preload    = '1';

		}

	}

}

else

{

	header("location:login.php");

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Add Enquiry</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



<div class="main">



	<div class="main-inner">



	    <div class="container">



	      <div class="row">



	      	<div class="span12">



	      		<div class="widget ">



	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				 <h3>New Enquiry</h3><?php if($view_perms_list['status'] == SUCCESS){?><span style="float:right; padding-right:20px;"><a href="crm_enquiry_list.php">
						Enquiry List</a></span><?php } ?>

	  				</div> <!-- /widget-header -->



					<div class="widget-content">







						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Add Enquiry</a>

						  </li>

						</ul>



						<br>

							<div class="control-group">

								<div class="controls">

								<?php

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>



								<?php

								if($alert_type == 1) // Success

								{

								?>

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="add_enquiry" class="form-horizontal" method="post" action="crm_add_enquiry.php">

								<input type="hidden" name="hd_is_preload" value="<?php echo $is_preload; ?>" />

								<input type="hidden" name="hd_pl_enq_id" value="<?php echo $pl_enquiry_id; ?>" />

								<input type="hidden" name="hd_pl_unq_id" value="<?php echo $pl_unqual_id; ?>" />

									<fieldset>



										<div class="control-group">

											<label class="control-label" for="ddl_project">Project*</label>

											<div class="controls">

												<select name="ddl_project">

												<option value="">- - Select a project - -</option>

												<?php

												for($count = 0; $count < count($project_list_data); $count++)

												{

												?>

												<option value="<?php echo $project_list_data[$count]["project_id"]; ?>"><?php echo $project_list_data[$count]["project_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="stxt_cust_name">Customer Name*</label>

											<div class="controls">

												<input type="text" pattern="^[a-zA-Z .]*$" class="span6" name="stxt_cust_name" placeholder="Customer Name (alphbets only)" value="<?php echo $pl_name; ?>" required="required">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->

                                                                                                                                                           										    <div class="control-group">

											<label class="control-label" for="stxt_cust_cell">Mobile*</label>

											<div class="controls">

												<input type="text" pattern="[0-9]*" class="span6" name="stxt_cust_cell" placeholder="10 digit mobile no." value="<?php echo $pl_mobile; ?>" required="required" step="1" min="0">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="stxt_cust_email">Email</label>

											<div class="controls">

												<input type="email" class="span6" name="stxt_cust_email" placeholder="Valid Customer Email ID" value="<?php echo $pl_email; ?>">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="stxt_cust_company">Company</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_cust_company" placeholder="Company of the Customer" value="<?php echo $pl_company; ?>">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="stxt_cust_location">Location</label>

											<div class="controls">

												<input type="text" class="span6" name="stxt_cust_location" placeholder="Location of the customer" value="<?php echo $pl_location; ?>">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="ddl_enq_src">Enquiry Source*</label>

											<div class="controls">

												<select name="ddl_enq_src" required>

												<?php

												for($count = 0; $count < count($enq_source_list_data); $count++)

												{

												?>

												<option value="<?php echo $enq_source_list_data[$count]["enquiry_source_master_id"]; ?>" <?php if($pl_source == $enq_source_list_data[$count]["enquiry_source_master_id"]){ ?> selected <?php } ?>><?php echo $enq_source_list_data[$count]["enquiry_source_master_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="cb_walk_in">Walk In</label>

											<div class="controls">

												<input type="checkbox" name="cb_walk_in" id="cb_walk_in" onclick="enable_disable_walkin_date();" />

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="dt_walk_in">Walk In Date</label>

											<div class="controls">

												<input type="date" name="dt_walk_in" id="dt_walk_in" disabled class="span6" />

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="stxt_walk_in_time">Walk In Time</label>

											<div class="controls">

												<input type="text" name="stxt_walk_in_time" id="stxt_walk_in_time" disabled class="span6" />

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">

												<textarea name="txt_remarks" class="span6"></textarea>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="ddl_interest_status">Interest Status*</label>

											<div class="controls">

												<select name="ddl_interest_status" required>

												<?php

												for($count = 0; $count < count($interest_status_list_data); $count++)

												{

												?>

												<option value="<?php echo $interest_status_list_data[$count]["crm_cust_interest_status_id"]; ?>" <?php if($pl_int_status == $interest_status_list_data[$count]["crm_cust_interest_status_id"]){ ?> selected <?php } ?>><?php echo $interest_status_list_data[$count]["crm_cust_interest_status_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="dt_follow_up">Follow Up Date*</label>

											<div class="controls">

												<input type="date" class="span6" name="dt_follow_up" required="required">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="ddl_assigned_to">Assigned To*</label>

											<div class="controls">

												<select name="ddl_assigned_to" required>

												<?php

												for($count = 0; $count < count($user_list_data); $count++)

												{



												?>

												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php

												if($user == $user_list_data[$count]["user_id"])

												{

												?>

												selected="selected"

												<?php

												}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>

												<?php


												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->





										<div class="form-actions">
										<?php
										if($add_perms_list['status'] == SUCCESS)
										{
										?>

											<input type="submit" class="btn btn-primary" name="add_enquiry_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>
											<?php
										}
										else
										{
											echo 'You are not authorized to view this page';
										}
										?>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>



							</div>





						</div>











					</div> <!-- /widget-content -->



				</div> <!-- /widget -->



		    </div> <!-- /span8 -->









	      </div> <!-- /row -->



	    </div> <!-- /container -->



	</div> <!-- /main-inner -->



</div> <!-- /main -->









<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->









<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function enable_disable_walkin_date()

{

	if((document.getElementById("cb_walk_in").checked) == true)

	{

		document.getElementById("dt_walk_in").disabled=false;

		document.getElementById("dt_walk_in").required=true;

		document.getElementById("stxt_walk_in_time").disabled= false;

		document.getElementById("stxt_walk_in_time").required= true;

	}

	else

	{

		document.getElementById("dt_walk_in").disabled=true;

		document.getElementById("dt_walk_in").required=false;

		document.getElementById("stxt_walk_in_time").disabled= true;

		document.getElementById("stxt_walk_in_time").required= false;

	}

}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


  </body>



</html>
