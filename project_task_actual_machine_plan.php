<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_task_actual_manpower_list.php
CREATED ON	: 05-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    $alert_type = -1;
    $alert = "";

    // Query String Data
    // Nothing

    if (isset($_GET['project_process_task_id'])) {
        $task_id = $_GET['project_process_task_id'];
    } else {
        $task_id = $_POST["hd_task_id"];
    }
    if (isset($_POST["hd_machine_id"])) {
        $search_machine = $_POST["hd_machine_id"];
    } else {
        $search_machine = "";
    }
    if (isset($_GET["location_id"])) {
        $road_id = $_GET["location_id"];
    } else {
        $road_id = $_POST["hd_road_id"];
    }
    $machine_type_id = "";

    //Get Regular Measurement
    $get_details_data = i_get_details('Regular', $task_id, $road_id);
    $reg_mp_msmrt = $get_details_data["mp_msmrt"];
    $planned_msmrt = $get_details_data["planned_msmrt"];
    $uom = $get_details_data["uom"];
    $reg_overall_msmrt = $get_details_data["overall_msmrt"];
    $road_name = $get_details_data["road"];
    $reg_mc_msmrt = $get_details_data["mc_msmrt"];
    $reg_cw_msmrt = $get_details_data["cw_msmrt"];
    $project = $get_details_data["project"];
    $process_name = $get_details_data["process"];
    $task_name = $get_details_data["task"];

    //Get Rework Measurement
    $get_details_data = i_get_details('Rework', $task_id, $road_id);
    $rework_mp_msmrt = $get_details_data["mp_msmrt"];
    $rework_mc_msmrt = $get_details_data["mc_msmrt"];
    $rework_cw_msmrt = $get_details_data["cw_msmrt"];
    $rework_overall_msmrt = $get_details_data["overall_msmrt"];

    if (isset($_POST["add_tasks_submit"])) {
        // Capture all form data
        $task_id                     = $_POST["hd_task_id"];
        $road_id                     = $_POST["hd_road_id"];
        $machine_id                  = $_POST['hd_machine_id'];
        $start_date_time             = $_POST['start_date_time'];
        $vendor											 = $_POST['ddl_vendor'];
        $additional_cost             = $_POST['additional_cost'];
        $machine_rate								 = $_POST['hd_machine_rate'];
        $machine_bata                = $_POST['machine_bata'];
        $work_type                   = $_POST['work_type'];
        $remarks 	                   = $_POST['txt_remarks'];

        $manpower_status = $get_details_data["manpower_status"];
        $contract_status = $get_details_data["contract_status"];
        $machine_status = $get_details_data["machine_status"];

        if (($manpower_status == "NotStarted") && ($machine_status == "NotStarted") && ($contract_status == "NotStarted")) {
            $project_process_task_update_data = array("actual_start_date"=>$start_date_time);
            $task_update_start_date_list = i_update_project_process_task($task_id, $project_process_task_update_data);
        } else {
            //Do nothing
        }

        if ($_POST['rb_machine_type'] == 'own') {
            $fuel_qty  					 = $_POST['machine_issued_fuel'];
        } else {
            $fuel_qty = 0;
        }

        $allow_entry = true;
        /* Start date time validation */
        if ((strtotime(date('Y-m-d H:i:s')) - strtotime($start_date_time)) > 7200) {
            $allow_entry = true;
            $alert       = 'Machine start time cannot be earlier than 2 hours from now';
        } elseif (strtotime($start_date_time) > strtotime(date('Y-m-d H:i:s'))) {
            $allow_entry = false;
            $alert       = 'Machine start time cannot be later than now';
        }

        if ($allow_entry) {
            // Add task
            $task_iresult = i_add_actual_machine_plan($task_id, $road_id, $machine_id, $start_date_time, '', $additional_cost, $vendor, '', $machine_rate, '', $machine_bata, $fuel_qty, '', $remarks, $user, $work_type);
            if ($task_iresult["status"] == SUCCESS) {

                if ($work_type == "Rework") {
                    $macine_rework_iresults= i_add_project_machine_rework($task_id, $vendor, $machine_id, $start_date_time, '', '', $additional_cost, '', $machine_rate, $machine_rate, $machine_bata, $fuel_qty, '', '', '', '', $remarks, '', '', '', '', $user);
                    $msg = "Rework has been started" ;
                    send_mail($project_id,$task_id,'manpower','',$loggedin_name,$road_id,$remarks.$msg);
                    header("location:project_actual_machine_planning_list.php");
                }
                  header("location:project_actual_machine_planning_list.php");
            } else {
                $alert_type = 1;
                $alert      = $task_iresult['data'];
            }
        } else {
            $alert_type = 0;
        }
    } elseif (isset($_POST["ddl_machine_type_id"])) {
        $machine_type_id   = $_POST["ddl_machine_type_id"];
        $task_id           = $_POST["hd_task_id"];
        $road_id           = $_REQUEST["hd_road_id"];
        $work_type         = $_REQUEST["work_type"];

        // Get Project Machine Master modes already added
        $project_machine_master_search_data = array("active"=>'1',"machine_type"=>$machine_type_id);
        $project_machine_master_list = i_get_project_machine_master($project_machine_master_search_data);
        if ($project_machine_master_list['status'] == SUCCESS) {
            $project_machine_master_list_data = $project_machine_master_list["data"];
        } else {
            $alert = $project_machine_master_list["data"];
            $alert_type = 0;
        }
    }

    // Machine Planning data
    $actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$task_id);
    $actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
    if ($actual_machine_plan_list["status"] == SUCCESS) {
        $actual_machine_plan_list_data = $actual_machine_plan_list["data"];
    } else {
        //
    }

    // Get project_machine_vendor_master modes already added
    $project_machine_vendor_master_search_data = array("active"=>'1');
    $project_machine_vendor_master_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
    if ($project_machine_vendor_master_list['status'] == SUCCESS) {
        $project_machine_vendor_master_list_data = $project_machine_vendor_master_list['data'];
    } else {
        $alert = $alert.$project_machine_vendor_master_list["data"];
        $alert_type = 0;
    }


    // Get Project Machine Type Master modes already added
    $project_machine_type_master_search_data = array("active"=>'1');
    $project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
    if ($project_machine_type_master_list['status'] == SUCCESS) {
        $project_machine_type_master_list_data = $project_machine_type_master_list["data"];
    } else {
        $alert = $alert.$project_machine_type_master_list["data"];
        $alert_type = 0;
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Task Actual Machine Plan</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
                <h3>
									<span class="header-label">Project :</span> <?= $project; ?>
									<span class="header-label">Process :</span> <?= $process_name; ?>
									<span class="header-label">Task:</span>  <?= $task_name; ?>
								</h3>
								<h3 style="margin-left:25px">
									<span class="header-label">Road:</span>  <?= $road_name; ?>
									<span class="header-label">Planned Msrmnt:</span> <?= $planned_msmrt; ?>
									<span class="header-label">UOM</span> <?= $uom; ?>
								</h3>
								<h3 >
									<span class="header-label">Regular</span> <?= $reg_overall_msmrt; ?> (MP : <?= $reg_mp_msmrt; ?>  MC : <?= $reg_mc_msmrt; ?> CW : <?= $reg_cw_msmrt; ?>)
									<span class="header-label">Rework</span> <?= $rework_overall_msmrt; ?> (MP : <?= $rework_mp_msmrt; ?>   MC : <?= $rework_mc_msmrt; ?> CW : <?= $rework_cw_msmrt; ?>)
                								</h3>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Project Task Actual Machine</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
                                if ($alert_type == 0) { // Failure?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>

								<?php
                                if ($alert_type == 1) { // Success
                                ?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
                                }
                                ?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
			<div class="tab-content">

			<div class="tab-pane active" id="formcontrols">
			<form id="add_tasks" class="form-horizontal" method="post" action="project_task_actual_machine_plan.php">
			<fieldset>
					<input type="hidden" name="hd_task_master_count" id="hd_task_master_count" value="<?php echo count($project_plan_process_task_list_data); ?> /">
					<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
					<input type="hidden" name="hd_road_id" value="<?php echo $road_id; ?>" />
					<input type="hidden" name="work_type" value="<?php echo $work_type; ?>" />


					<div class="control-group">
						<label class="control-label" for="ddl_machine_type_id">Machine Type*</label>
							<div class="controls">
							<select name="ddl_machine_type_id" class="span6" id="ddl_machine_type_id" required onchange="this.form.submit();" value="1">
							<option value="-1">- - Select Machine Type - -</option>
							<?php
                            for ($count = 0; $count < count($project_machine_type_master_list_data); $count++) {
                                ?>
							<option value="<?php echo $project_machine_type_master_list_data[$count]["project_machine_type_master_id"]; ?>"<?php if ($project_machine_type_master_list_data[$count]["project_machine_type_master_id"] == $machine_type_id) {
                                    ?> selected="selected" <?php
                                } ?>><?php echo $project_machine_type_master_list_data[$count]["project_machine_type_master_name"]; ?></option>
							<?php
                            }
                            ?>
							</select><br><br>
						</div> <!-- /controls -->
					 </div> <!-- /control-group -->

						<div class="control-group">
							<label class="control-label" for="hd_machine_id">Machine*</label>
								<div class="controls">
								<input type="hidden" name="hd_machine_id" id="hd_machine_id" value="" />
								  <span style="padding-right:20px;">
								  <input type="text" name="stxt_machine"  class="span6" autocomplete="off" id="stxt_machine" onkeyup="return get_machine_list(<?php echo $machine_type_id ;?>);" placeholder="Search Machine by name or code" />
								  <div id="search_results" class="dropdown-content"></div>
					</span><br><br>
						</div> <!-- /controls -->
					 </div> <!-- /control-group -->



					<div class="control-group">
						<label class="control-label">Machine vendor</label>
							<div class="controls">
								<span id="machine_vendor" style="width:10%" ></span>
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
					<label class="control-label" for="ddl_vendor">Billing Vendor*</label>
					<div class="controls">
						<select name="ddl_vendor" class="span6" required>
						<option value="">- - Select Machine Vendor - -</option>
						<?php
                        for ($count = 0; $count < count($project_machine_vendor_master_list_data); $count++) {
                            ?>
						<option value="<?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_id"]; ?>"><?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_name"]; ?></option>
						<?php
                        }
                        ?>
						</select>
					</div> <!-- /controls -->
				</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="start_date_time">Start Date Time</label>
							<div class="controls">
								<input type="datetime-local" name="start_date_time" id="start_date_time"></td>
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="rb_machine_type" >Fuel Provider</label>
						<div class="controls">
							<input type="radio" name="rb_machine_type" id="rb_machine_type" value="own" onclick="return fuel_type('0')">Fuel By KNS&nbsp;&nbsp;&nbsp;
							<input type="radio" name="rb_machine_type" id="rb_machine_type" value="rent" onclick="return fuel_type('1')">Fuel By Vendor&nbsp;&nbsp;&nbsp;
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="machine_rate">Machine Rate</label>
						<div class="controls">
							<input type="hidden" name="hd_machine_rate" id="hd_machine_rate" value="" />
							<span id="machine_rate" name="machine_rate" ></span>
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="additional_cost">Additional Cost</label>
						<div class="controls">
							<input type="number" name="additional_cost" id="additional_cost">
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="machine_issued_fuel">Fuel Value (in Rs.)</label>
						<div class="controls">
							<input type="number" name="machine_issued_fuel" min="0.01" step="0.01" value="0" id="machine_issued_fuel">
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="machine_bata">Driver Bata</label>
						<div class="controls">
							<input type="number" name="machine_bata"  id="machine_bata">
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="txt_remarks">Remarks</label>
						<div class="controls">
							<textarea name="txt_remarks" id="txt_remarks"></textarea>
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="work_type" >Work Type</label>
						<div class="controls">
							<table class="table table-bordered" style="width:55%">
							<tr>

							<td style="font-size:15px;color:blue" ><input type="radio" name="work_type" id="work_type" <?php if ($reg_overall_msmrt >= $planned_msmrt) {
                            ?> disabled <?php
                        } ?>   required value="Regular">Regular</td>
							<td style="font-size:15px;color:red"><input type="radio" name="work_type" id="work_type" <?php if ($reg_mc_msmrt <= 0) {
                            ?> disabled <?php
                        } ?> required value="Rework">Rework</td>
							</tr>
							</table>
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

			  <div class="form-actions">
						<button type="submit" class="btn btn-primary" name="add_tasks_submit" id="add_tasks_submit">Submit</button>
						<button type="reset" class="btn">Cancel</button>
					</div> <!-- /form-actions -->
				</fieldset>
			</form>
			</div>
			<table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Task Name</th>
					<th>Machine</th>
					<th>Actual Start Date Time</th>
					<th>Actual End date Tme</th>
					<th>Additional Cost</th>
					<th>Fuel Value (in Rs.)</th>
					<th>Machine Rate</th>
					<th>Machine Bata</th>
          <th>Remarks</th>
          <th>Added By</th>
          <th>Added On</th>
					<th colspan="2" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
                if ($actual_machine_plan_list["status"] == SUCCESS) {
                    $sl_no = 0;
                    for ($count = 0; $count < count($actual_machine_plan_list_data); $count++) {
                        $sl_no++; ?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $actual_machine_plan_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $actual_machine_plan_list_data[$count]["project_machine_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s", strtotime($actual_machine_plan_list_data[$count][
                    "project_task_actual_machine_plan_start_date_time"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($actual_machine_plan_list_data[$count]
                    ["project_task_actual_machine_plan_end_date_time"], "d-M-Y"); ?></td>
					<td><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_additional_cost"]; ?></td>
					<td><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_issued_fuel"]; ?></td>
					<td><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_with_fuel_charges"]; ?></td>
					<td><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_bata"]; ?></td>

					<td><?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_remarks"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $actual_machine_plan_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y", strtotime($actual_machine_plan_list_data[$count]
                    ["project_task_actual_machine_plan_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_task_actual_machine_plan('<?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_id"]; ?>','<?php echo $task_id; ?>');">Edit </a></div></td>
					<td><?php if (($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_active"] == "1")) {
                        ?><a href="#" onclick="return project_delete_task_actual_manpower(<?php echo $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_id"]; ?>);">Delete</a><?php
                    } ?></td>
					</tr>
					<?php
                    }
                } else {
                    ?>
				<td colspan="14">No Machine Actuals added yet!</td>

				<?php
                }
                 ?>

                </tbody>
              </table>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_task_actual_manpower(man_power_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_task_actual_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_task_actual_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
		}
	}
}

function go_to_project_edit_task_actual_machine_plan(plan_id,task_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_task_actual_machine_plan.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","plan_id");
	hiddenField1.setAttribute("value",plan_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","task_id");
	hiddenField2.setAttribute("value",task_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}

function get_rate()
{

	var machine_id = document.getElementById("ddl_machine_id").value;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			var object = JSON.parse(xmlhttp.responseText);
			document.getElementById("machine_vendor").innerHTML = object.vendor;
		}
	}

	xmlhttp.open("POST", "ajax/project_get_rate.php");   //
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("machine_id=" + machine_id);
}

function fuel_type(status)
{
	var machine_id = document.getElementById("hd_machine_id").value;
	if(status == '1')
	{
		document.getElementById("machine_issued_fuel").disabled = true;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				var object = JSON.parse(xmlhttp.responseText);
				document.getElementById('machine_rate').innerHTML = object.vendor_fuel_charge;
				document.getElementById('hd_machine_rate').value  = object.vendor_fuel_charge;
				document.getElementById("machine_bata").value     = object.bata;
				var machine_rate = object.vendor_fuel_charge;
				if(machine_rate <= 0)
				{
					document.getElementById("add_tasks_submit").disabled = true;
					alert("Machine Rate is not added for this vendor.")
				}
				else
				{
					document.getElementById("add_tasks_submit").disabled = false;
				}
			}
		}

		xmlhttp.open("POST", "ajax/project_get_fuel_rate.php");   //
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("machine_id=" + machine_id);
	}
	else
	{
		document.getElementById("machine_issued_fuel").disabled = false;
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				var object = JSON.parse(xmlhttp.responseText);
				document.getElementById('machine_rate').innerHTML    = object.kns_fuel_charge;
				document.getElementById('hd_machine_rate').value = object.kns_fuel_charge;
				document.getElementById("machine_bata").value = object.bata;
			}
		}

		xmlhttp.open("POST", "ajax/project_get_fuel_rate.php");   //
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("machine_id=" + machine_id);
	}
}
function get_machine_list(machine_type_id)
{
	var searchstring = document.getElementById('stxt_machine').value;

	if(searchstring.length >= 3)
	{
		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}

		xmlhttp.onreadystatechange = function()

		{

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{
				if(xmlhttp.responseText != 'FAILURE')

				{
					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
				}

			}

		}

		xmlhttp.open("POST", "ajax/project_get_machine.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring + "&machine_type_id=" + machine_type_id);
	}

	else

	{
		document.getElementById('search_results').style.display = 'none';
	}
}

function select_machine(machine_id,search_machine,number,search_vendor)
{
	document.getElementById('hd_machine_id').value 	= machine_id;
	//document.getElementById('stxt_machine').value = search_machine;
	document.getElementById('machine_vendor').innerHTML = search_vendor;
	var name_code = search_machine.concat('-',number);
	document.getElementById('stxt_machine').value = name_code;
	document.getElementById('search_results').style.display = 'none';
}
</script>

  </body>

</html>
