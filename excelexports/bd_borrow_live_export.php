<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: Attendance Report
CREATED ON	: 26-Mar-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Attendance Report
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_payment_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];


	// Temp data
	$alert      = "";
	$alert_type = "";		
	
	// Query String
	if(isset($_GET["village"]))
	{
		$village = $_GET["village"];
	}
	else
	{
		$village = "";
	}
	if(isset($_GET["survey"]))
	{
		$survey_no = $_GET["survey"];
	}
	else
	{
		$survey_no = "";
	}
	if(isset($_GET["custody"]))
	{
		$custody = $_GET["custody"];
	}
	else
	{
		$custody = "";
	}
	
	// Land Bank Data
	$bd_land_bank_search_data = array('survey_no'=>$survey_no,'village'=>$village,'active'=>'1');
	$bd_file_list = i_get_bd_land_bank_list($bd_land_bank_search_data);
	
	/* Create excel sheet and write the column headers - START */
	// Instantiate a new PHPExcel object
	$objPHPExcel = new PHPExcel(); 
	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
	// Initialise the Excel row number
	$row_count = 1; 
	// Excel column identifier array
	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O');
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "SURVEY NO");
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "BORROWER"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "KNS ACCOUNT"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "VILLAGE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "EXTENT (GUNTAS)"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "EXTENT (ACRES)"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "EXTENT (SQ. FT)"); 	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "CUSTODY"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "BANK"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "MORTGAGE DATE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "LEAD TIME"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "LOAN VALUE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, "RELEASED DATE"); 
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, "REPAID VALUE"); 		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, "REMARKS"); 
	
	$style_array = array('font' => array('bold' => true));
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[14].$row_count)->applyFromArray($style_array);
	$row_count++;
	if($bd_file_list["status"] == SUCCESS)
	{
		$bd_file_list_data = $bd_file_list['data'];
		$sl_no           = 0;
		
		for($land_bank_count = 0 ; $land_bank_count < count($bd_file_list_data); $land_bank_count++)
		{
			$sl_no++;
			
			$repaid_value = 0;
			
			$dont_show = false;
			
			$bd_file_borrow_list = i_get_bd_borrow_list('',$bd_file_list_data[$land_bank_count]["bd_project_file_id"],'','','','','','','','');
				
			if($bd_file_borrow_list['status'] == SUCCESS)
			{
				if(($custody != '') && ($custody != $bd_file_borrow_list["data"][0]["bd_file_borrow_custody"]))
				{
					$dont_show = true;
				}

				if($bd_file_borrow_list["data"][0]["bd_file_borrow_custody"] == 1)
				{
					$custody_name  = "Mortgage";
					$mortgage_date = date('d-M-Y',strtotime($bd_file_borrow_list["data"][0]["bd_file_borrow_mortgage"]));
					if($bd_file_borrow_list["data"][0]["bd_file_borrow_released_date"] != '0000-00-00')
					{
						$released_date = date('d-M-Y',strtotime($bd_file_borrow_list["data"][0]["bd_file_borrow_released_date"]));
					}
					else
					{
						$released_date = '';
					}
					
					// Calculate lead time from mortgage
					$lead_time_data = get_date_diff($bd_file_borrow_list["data"][0]["bd_file_borrow_mortgage"],date('Y-m-d'));
					$lead_time = $lead_time_data['data'];
					
					$bank_name  = $bd_file_borrow_list["data"][0]["crm_bank_name"];								
					$loan_value	= $bd_file_borrow_list["data"][0]["bd_file_borrow_recieved_loan_value"];					$borrower 		     = $bd_file_borrow_list["data"][0]["bd_file_borrower"];					$remarks 		     = $bd_file_borrow_list["data"][0]["bd_file_borrow_remarks"];
									
					$payment_request_data = array("borrow_id"=>$bd_file_borrow_list["data"][0]["bd_file_borrow_id"]);
					$borrow_payment_list = i_get_borrow_bd_payment_list($payment_request_data);
					if($borrow_payment_list["status"] == SUCCESS)
					{
						for($item_count = 0; $item_count < count($borrow_payment_list["data"]); $item_count++)
						{
							$repaid_value = $repaid_value + $borrow_payment_list["data"][$item_count]["bd_file_borrow_payment_amount"] ;
						}
					}
					else
					{
						$repaid_value = '0';							
					}
				}
				else
				{
					$custody_name  = "Not Mortgage";
					$mortgage_date = '';
					$released_date = '';
					$lead_time     = '';
					$bank_name     = '';
					$loan_value	   = '';
					$repaid_value  = '0';
				}
			}
			else
			{
				$custody_name  = "Not Mortgage";
				$mortgage_date = '';
				$released_date = '';
				$lead_time     = '';
				$bank_name     = '';
				$loan_value	   = '';
				$repaid_value  = '0';
			}						
			
			$extent_in_acres = (($bd_file_list_data[$land_bank_count]["bd_file_extent"])/GUNTAS_PER_ACRE);
			$extent_in_sq_ft = $extent_in_acres * SQUARE_FOOT_PER_ACRE;
			
			if($dont_show == false)
			{
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $bd_file_list_data[$land_bank_count]['bd_file_survey_no']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $borrower); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $bd_file_list_data[$land_bank_count]['bd_own_account_master_account_name']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $bd_file_list_data[$land_bank_count]['village_name']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $bd_file_list_data[$land_bank_count]['bd_file_extent']); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $extent_in_acres); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $extent_in_sq_ft); 			
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $custody_name);
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, $bank_name); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $mortgage_date); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $lead_time); 	
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $loan_value); 			
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[12].$row_count, $released_date); 
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[13].$row_count, $repaid_value);								$objPHPExcel->getActiveSheet()->SetCellValue($column_array[14].$row_count, $remarks);
				
				$row_count++;
			}
		}
	}
	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[14].$row_count)->getAlignment()->setWrapText(true); 	
	
	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[14].$row_count);
	$row_count++;
	
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[14].($row_count - 1))->getAlignment()->setWrapText(true); 	
	/* Create excel sheet and write the column headers - END */
	
	
	header('Content-Type: application/vnd.ms-excel'); 
	header('Content-Disposition: attachment;filename="LandBank - Live.xls"'); 
	header('Cache-Control: max-age=0'); 
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
	$objWriter->save('php://output');
}		
else
{
	header("location:login.php");
}	
?>