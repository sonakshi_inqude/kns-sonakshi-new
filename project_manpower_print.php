<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

// Session Data
$user 		   = $_SESSION["loggedin_user"];
$role 		   = $_SESSION["loggedin_role"];
$loggedin_name = $_SESSION["loggedin_user_name"];

// Temp Data
$alert_type = -1;
$alert = "";

$manpower_payment_id = $_GET["manpower_payment_id"];
$manpower_print_status='';
if($_GET["print_status"]){
  $manpower_print_status = $_GET["print_status"];
}
// Get Project  Payment ManPower modes already added
$project_actual_payment_manpower_search_data = array("manpower_id"=>$manpower_payment_id,"start"=>'-1');
$project_actual_payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
if ($project_actual_payment_manpower_list['status'] == SUCCESS) {
    $project_actual_payment_manpower_list_data = $project_actual_payment_manpower_list['data'];
    $men_hrs				 = $project_actual_payment_manpower_list_data[0]["project_actual_payment_men_hrs"];
    $women_hrs		 	 = $project_actual_payment_manpower_list_data[0]["project_actual_payment_women_hrs"];
    $mason_hrs			 = $project_actual_payment_manpower_list_data[0]["project_actual_payment_mason_hrs"];
    $bill_no		 		 = $project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_bill_no"];
    $billing		 		 = $project_actual_payment_manpower_list_data[0]["stock_company_master_name"];
    $billing_address = $project_actual_payment_manpower_list_data[0]["stock_company_master_address"];
    $vendor					 = $project_actual_payment_manpower_list_data[0]["project_manpower_agency_name"];
    $amount			 		 = $project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_amount"];
    $bill_date	     = date("d-M-Y", strtotime($project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_approved_on"]));
    $from_date		   = date("d-M-Y", strtotime($project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_from_date"]));
    $to_date		 		 = date("d-M-Y", strtotime($project_actual_payment_manpower_list_data[0]["project_actual_payment_manpower_to_date"]));
} else {
    // Do nothing
}
$project_payment_manpower_mapping_search_data = array("payment_id"=>$manpower_payment_id);
$payment_manpower_mapping_list= i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
if ($payment_manpower_mapping_list["status"] == SUCCESS) {
    $payment_manpower_mapping_list_data = $payment_manpower_mapping_list["data"];
    $man_power_id = $payment_manpower_mapping_list_data[0]["project_payment_manpower_mapping_manpower_id"];
    $man_power_search_data = array("man_power_id"=>$man_power_id);
    $actual_payment_manpower_list = i_get_man_power_list($man_power_search_data);
    if ($actual_payment_manpower_list["status"] == SUCCCES) {
        $project = $actual_payment_manpower_list["data"][0]["project_master_name"];
        $project_loaction = $actual_payment_manpower_list["data"][0]["project_master_remarks"];
        $site_engineer = $actual_payment_manpower_list["data"][0]["user_name"];
        $man_power_men_rate = $actual_payment_manpower_list["data"][0]["project_task_actual_manpower_men_rate"];
        $man_power_women_rate = $actual_payment_manpower_list["data"][0]["project_task_actual_manpower_women_rate"];
        $man_power_mason_rate = $actual_payment_manpower_list["data"][0]["project_task_actual_manpower_mason_rate"];
    }
}
$no_of_men = $men_hrs/8;
$no_of_women = $women_hrs/8;
$no_of_mason = $mason_hrs/8;

        // Temp data
    $project_payment_manpower_mapping_search_data = array("payment_id"=>$manpower_payment_id,"active"=>"1");
    $man_power_list = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);

    if ($man_power_list["status"] == SUCCESS) {
        $man_power_list_data = $man_power_list["data"];
    } else {
        $alert = $alert."Alert: ".$man_power_list["data"];
    }


$po_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
if($manpower_print_status=="duplicate"){
$po_print_format=$po_print_format.'<title>Manpower Bill (Duplicate copy)</title>';
}
else{
  $po_print_format=$po_print_format.'<title>Manpower Bill</title>';
}
$po_print_format=$po_print_format.'</head>

<body>

  <table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; border-top:3px solid #000">
  <tr>
    <td colspan="7  align="center"><h2 style="margin:0px;">'.$billing.'</h2>
    <span style="font-size:12px;">'.$billing_address.'</span>
	</td>

    </tr>';
    if($manpower_print_status=="duplicate"){
    $po_print_format=$po_print_format.'<tr>
        <td colspan="7" align="center"><strong>MANPOWER BILL (Duplicate Copy)</strong></td>
      </tr>';
    }
    else{
      $po_print_format=$po_print_format.'  <tr>
          <td colspan="7" align="center"><strong>MANPOWER BILL</strong></td>
        </tr>';
    }
    $po_print_format=$po_print_format.'<tr>
    <td colspan="3">Project Name :</td>
    <td colspan="2">'.$project.'</td>
	<td colspan="1">Site Engineer :</td>
    <td colspan="1">'.$site_engineer.'</td>
  </tr>
  <tr>
    <td colspan="3">Project Location :</td>
    <td colspan="2">'.$project_loaction.'</td>
    <td colspan="1">Bill.No :</td>
	<td colspan="1">'.$bill_no.'</td>
  </tr>
  <tr>
    <td colspan="3">Contractee :</td>
    <td colspan="2">&nbsp;</td>
    <td colspan="1">Bill Date :</td>
	<td colspan="1">'.$bill_date.'</td>
  </tr>
  <tr>
    <td colspan="3">Contractor :</td>
    <td colspan="2">'.$vendor.'</td>
    <td colspan="1">Bill Period :</td>
	<td colspan="1">'.$from_date.' to '.$to_date.'</td>
  </tr>


 <tr>
 <td colspan="7">
 <table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; border-top:3px solid #000">
 <tr>
   <td width="4%" align="center"><strong>Sl.no</strong></td>
   <td width="17%" align="center"><strong>Labour Type</strong></td>
   <td width="10%" align="center"><strong>Unit</strong></td>
   <td width="9%" align="center"><strong>QTY</strong></td>
   <td width="14%" align="center"><strong>Rate</strong><strong>Rate/PH</strong></td>
   <td width="14%" align="center"><strong>Amount</strong></td>
   <td width="32%" align="center"><strong>Remarks</strong></td>
   </tr>';
   for ($count = 0; $count < count($project_actual_payment_manpower_list_data); $count++) {
       $men_rate = $men_hrs * $man_power_men_rate;
       $women_rate = $women_hrs * $man_power_women_rate;
       $mason_rate = $mason_hrs * $man_power_mason_rate;
       $total = round(($men_rate + $women_rate + $mason_rate), 2);
       $amount_words = convert_num_to_words($total);

       $po_print_format = $po_print_format.'<tr>
   <tr>
   <td align="center">'.($count + 1).'</td>
   <td align="center">Men</td>
    <td align="center">Nos</td>
   <td align="center">'.number_format((float)$no_of_men, 2, '.', '').'</td>
   <td align="center">'.number_format((float)$man_power_men_rate, 2, '.', '').'</td>
   <td align="center">'.number_format((float)$men_rate, 2, '.', '').'</td>
   <td rowspan="3">'.$project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_remarks"].'</td>
   </tr>
   <tr>
   <td align="center">'.($count + 2).'</td>
   <td align="center">Women</td>
    <td align="center">Nos</td>
   <td align="center">'.number_format((float)$no_of_women, 2, '.', '').'</td>
   <td align="center">'.number_format((float)$man_power_women_rate, 2, '.', '').'</td>
   <td align="center">'.number_format((float)$women_rate, 2, '.', '').'</td>
   </tr>
   <tr>
   <td align="center">'.($count + 3).'</td>
   <td align="center">Mason</td>
    <td align="center">Nos</td>
   <td align="center">'.number_format((float)$no_of_mason, 2, '.', '').'</td>
  <td align="center">'.number_format((float)$man_power_mason_rate, 2, '.', '').'</td>
   <td align="center">'.number_format((float)$mason_rate, 2, '.', '').'</td>
   </tr>
 </tr>';
   };
 $po_print_format = $po_print_format.'</table>
 </td>

 </tr>
 <tr>
 <td colspan="6">Grand Total:</td>
 <td>'.$total.'</td>
 </td>
 </tr>
<tr>
 <td colspan="7">
   Amount in words :
 '.$amount_words.'</td>
 </tr>

<tr>
  <td colspan="7" align="center">&nbsp;</td>
  </tr>
 <tr>
  <td colspan="5" align="center">(Prepared by)</td>
  <td colspan="2" align="center">(Checked by)</td>
  </tr>
<tr>
  <td colspan="5" align="center">'.$man_power_list_data[0]["adder"].'</td>
  <td colspan="2" align="center">'.$man_power_list_data[0]["oker"].'</td>
  </tr>
<tr>
  <td colspan="5" align="center">Executive</td>
  <td colspan="2" align="center">Project Engineer</td>
  </tr>
<tr>
  <td colspan="7" align="center">&nbsp;</td>
</tr>
<tr>
  <td colspan="4" align="center">(Verified by)</td>
  <td colspan="2" align="center">(Signature)</td>
  <td align="center">(Signature)</td>
</tr>
<tr>
  <td colspan="4" align="center">'.$man_power_list_data[0]["approver"].'</td>
  <td colspan="2" align="center">&nbsp;</td>
  <td align="center">&nbsp;</td>
</tr>
<tr>
  <td colspan="4" align="center">Project Planning Engineer</td>
  <td colspan="2" align="center">GM - Civil</td>
  <td align="center">Accounts</td>
</tr>
</table>';
 $po_print_format = $po_print_format.'<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; border-top:3px solid #000">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
					<th>Task Name</th>
					<th>Road</th>
					<th>Msmrt</th>
					<th>UOM</th>
					<th>Type</th>
					<th>Date</th>
					<th>Agency</th>
					<th>Men Hrs</th>
					<th>Women Hrs</th>
					<th>Mason Hrs</th>
					<th>Others Hrs</th>
					<th>No of People</th>
					<th>Amount</th>

				</tr>
				</thead>
				<tbody>';
                if ($man_power_list["status"] == SUCCESS) {
                    $sl_no = 0;
                    $total_cost = 0;
                    $grand_total  = 0;
                    for ($count = 0; $count < count($man_power_list_data); $count++) {
                        if (($man_power_list_data[$count]["project_task_actual_manpower_road_id"] != "No Roads")) {
                            $road_name = $man_power_list_data[$count]["project_site_location_mapping_master_name"];
                        } else {
                            $road_name = "No Roads";
                        }
                        $get_details_data = i_get_details('', $man_power_list_data[$count]["project_task_actual_manpower_task_id"], $man_power_list_data[$count]["project_task_actual_manpower_road_id"]);
                        $uom = $get_details_data["uom"];
                        $project_payment_manpower_mapping_search_data = array("manpower_id"=>$man_power_list_data[$count]["project_task_actual_manpower_id"]);
                        $payment_manpower_mapping_list = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
                        if ($payment_manpower_mapping_list["status"] == SUCCESS) {
                            $payment_manpower = true;
                        } else {
                            $payment_manpower = false;
                        }


                        $total_men_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"] * $man_power_men_rate;
                        $total_women_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"] * $man_power_women_rate;
                        $total_mason_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"] * $man_power_mason_rate;
                        $total_cost = $total_men_cost + $total_women_cost + $total_mason_cost;
                        $men_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"];
                        $women_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"];
                        $mason_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"];

                        $no_of_men = $men_hrs/8;
                        $no_of_women = $women_hrs/8;
                        $no_of_mason = $mason_hrs/8;
                        $grand_total = $grand_total + $total_cost;
                        $total_hrs = $men_hrs + $women_hrs + $mason_hrs;
                        $no_of_people = $total_hrs/8;

                        $sl_no++;
                        $po_print_format = $po_print_format.'<tr>
						 <td align="center">'.$sl_no.'</td>
						 <td align="center">'.$man_power_list_data[$count]["project_master_name"].'</td>
						 <td align="center">'.$man_power_list_data[$count]["project_process_master_name"].'</td>
						 <td align="center">'.$man_power_list_data[$count]["project_task_master_name"].'</td>
						 <td align="center">'.$road_name.'</td>
						 <td align="center">'.$man_power_list_data[$count]["project_task_actual_manpower_completed_msmrt"].'</td>
						 <td align="center">'.$uom.'</td>
						 <td align="center">'.$man_power_list_data[$count]["project_task_actual_manpower_work_type"].'</td>

						<td style="word-wrap:break-word;">'.date("d-M-Y", strtotime($man_power_list_data[$count][
                        "project_task_actual_manpower_date"])).'</td>


						 <td align="center">'.$man_power_list_data[$count]["project_manpower_agency_name"].'</td>


						<td style="word-wrap:break-word;">'.$man_power_list_data[$count]["project_task_actual_manpower_no_of_men"] .'
					  &nbsp;&nbsp;&nbsp; Rate : '.$man_power_men_rate .'
					  Men '.$no_of_men .'</td>


						<td style="word-wrap:break-word;">'.$man_power_list_data[$count]["project_task_actual_manpower_no_of_women"] .'
					&nbsp;&nbsp;&nbsp; Rate : '.$man_power_women_rate.' Women :'. $no_of_women.'</td>


						<td style="word-wrap:break-word;">'.$man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"] .'
					 &nbsp;&nbsp;&nbsp; Rate : '.$man_power_mason_rate .'Mason :'. $no_of_mason.'</td>


						 <td align="center">'.$man_power_list_data[$count]["project_task_actual_manpower_no_of_others"].'</td>

						<td style="word-wrap:break-word;">'. $no_of_people .'</td>

						 <td align="center">'. ($total_cost) .'</td>
					</tr>';
                        $po_print_format = $po_print_format.'<tr><td colspan="16">REMARKS: '.$man_power_list_data[$count]["project_task_actual_manpower_remarks"].'</td></tr>';
                    }
                    $po_print_format = $po_print_format.' <tr>
					<td colspan="15">Grand Total </td>
					<td>'.$grand_total.' </td>
					</tr>';
                } else {
                    $po_print_format = $po_print_format.'<td colspan="6">No Project Master condition added yet!</td>';
                }

               $po_print_format = $po_print_format.' </tbody>
              </table>';

              $po_print_format = $po_print_format.'</body>
</html>';
echo $po_print_format;
//$mpdf = new mPDF();
//$mpdf->WriteHTML($po_print_format);
//$mpdf->Output('invoice_'.$invoice_no.'.pdf','I');
