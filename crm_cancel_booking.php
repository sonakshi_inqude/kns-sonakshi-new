<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 23th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_cancel_booking_submit"]))
	{
		$booking_id  = $_POST["hd_booking_id"];
		$site_id     = $_POST["hd_site_id"];
		$cancel_date = $_POST["dt_cancel_date"];
		$reason      = $_POST["ddl_reason"];		
		$remarks     = $_POST["txt_remarks"];		
		
		// Check for mandatory fields
		if(($booking_id !="") && ($cancel_date !="") && ($reason != ""))
		{
			if((strtotime($cancel_date)) <= (strtotime(date("Y-m-d"))))
			{
				$add_booking_cancel_result = i_add_site_booking_cancellation($booking_id,$cancel_date,$reason,$remarks,$user);
				
				if($add_booking_cancel_result["status"] == SUCCESS)
				{
					$alert_type = 1;
					$alert      = $add_booking_cancel_result["data"];
					
					// Update Booking Status
					$booking_uresult = i_update_booking($booking_id,BOOKING_CANCELLED,''); 
					
					if($booking_uresult["status"] == SUCCESS)
					{						
						$site_status_update_result = i_update_site_status($site_id,AVAILABLE_STATUS,$user);
						if($site_status_update_result["status"] == SUCCESS)
						{
							header("location:crm_approved_booking_list.php");						
						}
						else
						{
							$alert_type = 0;
							$alert      = $site_status_update_result["data"];
						}
					}
					else
					{
						$alert_type = 0;
						$alert      = $booking_uresult["data"];
					}
				}
				else
				{
					$alert_type = 0;
					$alert      = $add_booking_cancel_result["data"];
				}
			}
			else
			{
				$alert_type = 0;
				$alert      = "Cancellation Date cannot be greater than today";
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $alert.$cust_details["data"];
		$alert_type = 0;
	}
	
	// Get booking details
	$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
	
	// Get booking details
	$cancel_reason_list = i_get_cancel_reason_list('','1');
	if($cancel_reason_list["status"] == SUCCESS)
	{
		$cancel_reason_list_data = $cancel_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$cancel_reason_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Cancel Booking</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php 
						if($cust_details["status"] == SUCCESS)
						{
						?>
						<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]." (".$cust_details_list[0]["crm_customer_contact_no_one"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}
						else
						{
							$alert_type = 0;
							$alert      = "Customer Profile not added!";?>
	      				<h3>Project: <?php echo $booking_list_data[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $booking_list_data[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $booking_list_data[0]["name"]." (".$booking_list_data[0]["cell"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}?>
						
						<span style="float:right; padding-right:10px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_list_data[0]["crm_booking_id"]; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Cancellation Details</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="cancel_booking" class="form-horizontal" method="post" action="crm_cancel_booking.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />
								<input type="hidden" name="hd_site_id" value="<?php echo $booking_list_data[0]["crm_site_id"]; ?>" />
									<fieldset>										
										
										<div class="control-group">											
											<label class="control-label" for="dt_cancel_date">Cancellation Date*</label>
											<div class="controls">
												<input type="date" name="dt_cancel_date" class="span6" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="ddl_reason">Reason*</label>
											<div class="controls">
												<select name="ddl_reason" required>
												<?php
												for($count = 0; $count < count($cancel_reason_list_data); $count++)
												{																							
												?>
												<option value="<?php echo $cancel_reason_list_data[$count]["crm_cancellation_reason_id"]; ?>"><?php echo $cancel_reason_list_data[$count]["crm_cancellation_reason"]; ?></option>								
												<?php												
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_cancel_booking_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
