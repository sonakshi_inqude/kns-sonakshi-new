<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. In db_get_legal_file_type_list, name search should be wildcard search
*/

/*
PURPOSE : To get legal file type list
INPUT 	: Type Name, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of file types for legal
BY 		: Nitin Kashyap
*/
function db_get_legal_file_type_list($type_name,$active,$added_by,$start_date,$end_date)
{
	$get_file_type_list_squery_base = "select * from legal_file_type_master";
	
	$get_file_type_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_file_type_list_sdata = array();
	
	if($type_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." where legal_file_type_name=:file_type_name";								
		}
		else
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." and legal_file_type_name=:file_type_name";				
		}
		
		// Data
		$get_file_type_list_sdata[':file_type_name']  = $type_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." where legal_file_type_active=:active";								
		}
		else
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." and legal_file_type_active=:active";				
		}
		
		// Data
		$get_file_type_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." where legal_file_type_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." and legal_file_type_added_by=:added_by";				
		}
		
		// Data
		$get_file_type_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." where legal_file_type_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." and legal_file_type_added_on >= :start_date";				
		}
		
		//Data
		$get_file_type_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." where legal_file_type_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_file_type_list_squery_where = $get_file_type_list_squery_where." and legal_file_type_added_on <= :end_date";				
		}
		
		//Data
		$get_file_type_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_file_type_list_squery = $get_file_type_list_squery_base.$get_file_type_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_file_type_list_sstatement = $dbConnection->prepare($get_file_type_list_squery);
		
		$get_file_type_list_sstatement -> execute($get_file_type_list_sdata);
		
		$get_file_type_list_sdetails = $get_file_type_list_sstatement -> fetchAll();
		
		if(FALSE === $get_file_type_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_file_type_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_file_type_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get legal file list
INPUT 	: File Number, BD File ID, File Type, Survey No, Land Owner, PAn Number, Extent, Village, Process Type, Task Type, Added By, Start Date(for added on), End Date(for added on), Ascending or Descending, File ID, Status (1 if active, 0 if inactive), BD Project, Last Bulk Process, Last Bulk Task, Process Type
OUTPUT 	: List of files for legal
BY 		: Nitin Kashyap
*/
function db_get_legal_file_list($file_number,$bd_file_id,$file_type,$survey_number,$land_owner,$pan_number,$extent,$village,$process_type,$task_type,$added_by,$start_date,$end_date,$order,$file_id="",$status="1",$bd_project="",$bprocess="",$btask="",$process_master='')
{
	$get_file_list_squery_base = "select *,PM.process_master_id as process_type_id,PM.process_name as process_name,TTM.task_type_name as task_type_name,TPL.task_plan_actual_start_date as actual_start_date, BTPL.task_plan_actual_start_date as bactual_start_date,BPM.process_master_id as bulk_process_type_id,BPM.process_name as bulk_process,BTTM.task_type_name as bulk_task from files F inner join legal_file_type_master FTM on FTM.legal_file_type_id = F.file_type left outer join process_plan_legal PPL on PPL.process_plan_legal_id = F.file_last_process left outer join process_master PM on PM.process_master_id = PPL.process_plan_type left outer join task_plan_legal TPL on TPL.task_plan_legal_id = F.file_last_task left outer join task_type_master TTM on TTM.task_type_id = TPL.task_plan_legal_type left outer join village_master VM on VM.village_id = F.file_village left outer join bd_project_files BPF on BPF.bd_project_file_id = F.file_bd_file_id left outer join bd_projects_master BDPM on BDPM.bd_project_id = BPF.bd_mapped_project_id left outer join legal_bulk_process LBP on LBP.legal_bulk_process_id = F.file_last_bulk_process left outer join process_master BPM on BPM.process_master_id = LBP.legal_bulk_process_type left outer join task_plan_legal BTPL on BTPL.task_plan_legal_id = F.file_last_bulk_task left outer join task_type_master BTTM on BTTM.task_type_id = BTPL.task_plan_legal_type left outer join bd_file_owner_status BFOS on BFOS.bd_file_owner_status_id = BPF.bd_file_owner_status left outer join bd_own_account_master BOAM on BOAM.bd_own_accunt_master_id = BPF.bd_file_own_account";
	
	$get_file_list_squery_where = "";
	
	$get_file_list_sorder = "";
	
	$filter_count = 0;
	
	// Data
	$get_file_list_sdata = array();
	
	if($file_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_number=:file_number";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_number=:file_number";				
		}
		
		// Data
		$get_file_list_sdata[':file_number']  = $file_number;
		
		$filter_count++;
	}
	
	if($bd_file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_bd_file_id=:bd_file";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_bd_file_id=:bd_file";				
		}
		
		// Data
		$get_file_list_sdata[':bd_file']  = $bd_file_id;
		
		$filter_count++;
	}
	
	if($bd_project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where BPF.bd_mapped_project_id = :bd_project";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and BPF.bd_mapped_project_id = :bd_project";				
		}
		
		// Data
		$get_file_list_sdata[':bd_project']  = $bd_project;
		
		$filter_count++;
	}
	
	if($file_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_type=:file_type";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_type=:file_type";				
		}
		
		// Data
		$get_file_list_sdata[':file_type']  = $file_type;
		
		$filter_count++;
	}
	
	if($survey_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_survey_number=:survey_number";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_survey_number=:survey_number";				
		}
		
		// Data
		$get_file_list_sdata[':survey_number']  = $survey_number;
		
		$filter_count++;
	}
	
	if($land_owner != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_land_owner=:land_owner";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_land_owner=:land_owner";				
		}
		
		// Data
		$get_file_list_sdata[':land_owner']  = $land_owner;
		
		$filter_count++;
	}
	
	if($pan_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_pan_number=:pan_no";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_pan_number=:pan_no";				
		}
		
		// Data
		$get_file_list_sdata[':pan_no']  = $pan_number;
		
		$filter_count++;
	}
	
	if($extent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_extent=:extent";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_extent=:extent";				
		}
		
		// Data
		$get_file_list_sdata[':extent']  = $file_extent;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_village=:village";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_village=:village";				
		}
		
		// Data
		$get_file_list_sdata[':village']  = $village;
		
		$filter_count++;
	}
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_id=:file_id";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_id=:file_id";				
		}
		
		// Data
		$get_file_list_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($process_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_last_process = :process";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_last_process = :process";				
		}
		
		// Data
		$get_file_list_sdata[':process']  = $process_type;
		
		$filter_count++;
	}
	
	if($task_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_last_task = :task";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_last_task = :task";				
		}
		
		// Data
		$get_file_list_sdata[':task']  = $task_type;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_added_by=:added_by";				
		}
		
		// Data
		$get_file_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_added_on >= :start_date";				
		}
		
		//Data
		$get_file_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_added_on <= :end_date";				
		}
		
		//Data
		$get_file_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_status = :status";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_status = :status";				
		}
		
		//Data
		$get_file_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($bprocess != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_last_bulk_process = :bprocess";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_last_bulk_process = :bprocess";				
		}
		
		//Data
		$get_file_list_sdata[':bprocess']  = $bprocess;
		
		$filter_count++;
	}
	
	if($btask != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where file_last_bulk_task = :btask";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and file_last_bulk_task = :btask";				
		}
		
		//Data
		$get_file_list_sdata[':btask']  = $btask;
		
		$filter_count++;
	}
	
	if($process_master != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." where process_plan_type = :process_master_type";								
		}
		else
		{
			// Query
			$get_file_list_squery_where = $get_file_list_squery_where." and process_plan_type = :process_master_type";				
		}
		
		//Data
		$get_file_list_sdata[':process_master_type']  = $process_master;
		
		$filter_count++;
	}
	
	if($order == "desc_one")
	{
		$get_file_list_sorder = " order by cast(substring(file_number,locate('/',file_number,10)+1) as signed) desc limit 0,1";		
	}
	else if($order == "desc_list")
	{
		$get_file_list_sorder = " order by cast(substring(file_number,locate('/',file_number,10)+1) as signed) desc";		
	}
	
	$get_file_list_squery = $get_file_list_squery_base.$get_file_list_squery_where.$get_file_list_sorder;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_file_list_sstatement = $dbConnection->prepare($get_file_list_squery);
		
		$get_file_list_sstatement -> execute($get_file_list_sdata);
		
		$get_file_type_list_sdetails = $get_file_list_sstatement -> fetchAll();
		
		if(FALSE === $get_file_type_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_file_type_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_file_type_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new file
INPUT 	: File Number, BD File ID, Start Date, No of Days, File Type, Survey Number, Land Owner, PAN Number, Extent, Village, Document, Deal Amount, Brokerage, Added By
OUTPUT 	: File ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_file($file_num,$bd_file_id,$start_date,$num_days,$type,$survey_no,$land_owner,$pan,$extent,$village,$doc_path,$deal_amount,$brokerage,$added_by)
{
	// Query
    $file_iquery = "insert into files (file_number,file_bd_file_id,file_start_date,file_num_days,file_type,file_survey_number,file_land_owner,file_pan_number,file_extent,file_village,file_doc,file_deal_amount,file_brokerage_charges,file_last_process,file_last_process_date,file_last_task,file_last_task_date,file_last_bulk_process,file_last_bulk_process_date,file_last_bulk_task,file_last_bulk_task_date,file_status,file_added_by,file_added_on) values (:file_no,:bd_file_id,:start_date,:file_num_days,:type,:survey_no,:owner,:pan,:extent,:village,:doc,:deal_amount,:brokerage,:process,:process_date,:task,:task_date,:bulk_process,:bulk_process_date,:bulk_task,:bulk_task_date,:status,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $file_istatement = $dbConnection->prepare($file_iquery);
        
        // Data
        $file_idata = array(':file_no'=>$file_num,':bd_file_id'=>$bd_file_id,':start_date'=>$start_date,':file_num_days'=>$num_days,':type'=>$type,':survey_no'=>$survey_no,':owner'=>$land_owner,':pan'=>$pan,':extent'=>$extent,':village'=>$village,':doc'=>$doc_path,':deal_amount'=>$deal_amount,':brokerage'=>$brokerage,':process'=>'',':process_date'=>'',':task'=>'',':task_date'=>'',':bulk_process'=>'',':bulk_process_date'=>'',':bulk_task'=>'',':bulk_task_date'=>'',':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $file_istatement->execute($file_idata);
		$file_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE: To update file details
INPUT  : File ID, Last Process, Last Process Date, Last Task, Last Task Date
OUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_file($file_id,$last_process,$last_process_date,$last_task,$last_task_date)
{
	// Query
    $file_uquery = "update files set file_last_process=:process,file_last_process_date=:process_date, file_last_task=:task, file_last_task_date=:task_date where file_id=:file_id";   		

    try
    {
        $dbConnection = get_conn_handle();
        
        $file_ustatement = $dbConnection->prepare($file_uquery);
        
        // Data
        $file_udata = array(':process'=>$last_process,':process_date'=>$last_process_date,':task'=>$last_task,':task_date'=>$last_task_date,':file_id'=>$file_id);				
        
        $file_ustatement -> execute($file_udata);
		
		$updated_rows = $file_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE: To update file with bulk process details
INPUT  : File ID, Last Process, Last Process Date, Last Task, Last Task Date
OUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_file_for_bulk($file_id,$last_process,$last_process_date,$last_task,$last_task_date)
{
	// Query
    $file_uquery = "update files set file_last_bulk_process=:process, file_last_bulk_process_date=:process_date, file_last_bulk_task=:task, file_last_bulk_task_date=:task_date where file_id=:file_id";   		
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $file_ustatement = $dbConnection->prepare($file_uquery);
        
        // Data
        $file_udata = array(':process'=>$last_process,':process_date'=>$last_process_date,':task'=>$last_task,':task_date'=>$last_task_date,':file_id'=>$file_id);						
        
        $file_ustatement -> execute($file_udata);
		
		$updated_rows = $file_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To handover a file to project
INPUT 	: File ID, Remarks, Added By
OUTPUT 	: File Handover ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_file_project($file_id,$remarks,$added_by)
{
	// Query
    $file_project_iquery = "insert into file_project_handover (file_id,remarks,file_handover_by,file_handover_on) values (:file_id,:remarks,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $file_project_istatement = $dbConnection->prepare($file_project_iquery);
        
        // Data
        $file_project_idata = array(':file_id'=>$file_id,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $file_project_istatement->execute($file_project_idata);
		$file_project_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_project_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get file details
INPUT 	: File id
OUTPUT 	: File Details, success or failure message
BY 		: Nitin Kashyap
*/
function db_get_file_details($file_id)
{
    // Query
	$get_file_det_squery = "select * from files where file_id=:file_id";
	// Data
	$get_file_det_sdata  = array(':file_id'=>$file_id);
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_file_sstatement = $dbConnection->prepare($get_file_det_squery);
		
		$get_file_sstatement -> execute($get_file_det_sdata);
		
		$get_file_sdetails = $get_file_sstatement -> fetch();
		
		if(FALSE === $get_file_sdetails)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_file_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = $e->getMessage();
	}
	
	return $return;
}

function db_get_file_handover_details($file_id)
{
    // Query
	$get_file_handover_squery = "select * from file_project_handover where file_id=:file_id";
	// Data
	$get_file_handover_sdata  = array(':file_id'=>$file_id);
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_file_handover_sstatement = $dbConnection->prepare($get_file_handover_squery);
		
		$get_file_handover_sstatement -> execute($get_file_handover_sdata);
		
		$get_file_handover_sdetails = $get_file_handover_sstatement -> fetch();
		
		if(FALSE === $get_file_handover_sdetails)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_file_handover_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = $e->getMessage();
	}
	
	return $return;
}

/*
PURPOSE : To add file related payment
INPUT 	: Request ID, File ID, Type, Payment Date, Amount, Remarks, Added By
OUTPUT 	: File Payment ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_file_payment($request_id,$file_id,$file_payment_type,$file_payment_payment_mode,$file_payment_cheque_dd_no,$file_payment_bank_name,$payment_date,$amount,$done_to,$remarks,$added_by)
{
	// Query
    $file_payment_iquery = "insert into file_payment (file_payment_request_id,file_payment_file_id,file_payment_type,file_payment_payment_mode,file_payment_cheque_dd_no,file_payment_bank_name,file_payment_payment_status,file_payment_alternative_cash_cheque_issued_against_returned_chq,file_payment_date,file_payment_amount,file_payment_done_to,file_payment_remarks,file_payment_added_by,file_payment_added_on) values (:request_id,:file_id,:file_payment_type,:file_payment_payment_mode,:file_payment_cheque_dd_no,:file_payment_bank_name,:file_payment_payment_status,:file_paymnet_alternative_cash_cheque_issued_against_returned_chq,:payment_date,:amount,:done_to,:remarks,:added_by,:now)";  
	
	try
    {
        $dbConnection = get_conn_handle();
        
        $file_payment_istatement = $dbConnection->prepare($file_payment_iquery);
        
        // Data
        $file_payment_idata = array(':request_id'=>$request_id,':file_id'=>$file_id,':file_payment_type'=>$file_payment_type,':file_payment_payment_mode'=>$file_payment_payment_mode,':file_payment_cheque_dd_no'=>$file_payment_cheque_dd_no,':file_payment_bank_name'=>$file_payment_bank_name,':file_payment_payment_status'=>'1',':file_paymnet_alternative_cash_cheque_issued_against_returned_chq'=>'0',':payment_date'=>$payment_date,':amount'=>$amount,':done_to'=>$done_to,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $file_payment_istatement->execute($file_payment_idata);
		$file_payment_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_payment_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get file payment
INPUT 	: Type Name, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of file types for legal
BY 		: Nitin Kashyap
*/
function db_get_file_payment_list($file_id,$file_payment_type,$file_payment_payment_mode,$file_payment_cheque_dd_no,$file_payment_bank_name,$file_payment_payment_status,$file_paymnet_alternative_cash_cheque_issued_against_returned_chq,$file_payment_amount,$pay_date,$added_by,$start_date,$end_date,$request_id='')
{
	$get_file_payment_squery_base = "select * from file_payment FP left outer join crm_bank_master CBM on CBM.crm_bank_master_id = FP.file_payment_bank_name left outer join payment_mode_master PMM ON PMM.payment_mode_id = FP.file_payment_payment_mode";
	
	$get_file_payment_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_file_payment_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_file_id=:file_id";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_file_id=:file_id";				
		}
		
		// Data
		$get_file_payment_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($file_payment_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_type=:file_payment_type";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_type=:file_payment_type";				
		}
		
		// Data
		$get_file_payment_sdata[':file_payment_type']  = $file_payment_type;
		
		$filter_count++;
	}
	if($file_payment_payment_mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_payment_mode=:file_payment_payment_mode";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_payment_mode=:file_payment_payment_mode";				
		}
		
		// Data
		$get_file_payment_sdata[':file_payment_payment_mode']  = $file_payment_payment_mode;
		
		$filter_count++;
	}
	
	if($file_payment_cheque_dd_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_cheque_dd_no=:file_payment_cheque_dd_no";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_cheque_dd_no=:file_payment_cheque_dd_no";				
		}
		
		// Data
		$get_file_payment_sdata[':file_payment_cheque_dd_no']  = $file_payment_cheque_dd_no;
		
		$filter_count++;
	}
	if($file_payment_bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_bank_name=:file_payment_bank_name";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_bank_name=:file_payment_bank_name";				
		}
		
		// Data
		$get_file_payment_sdata[':file_payment_bank_name']  = $file_payment_bank_name;
		
		$filter_count++;
	}
	
	if($file_payment_amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_amount=:file_payment_amount";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_amount=:file_payment_amount";				
		}
		
		// Data
		$get_file_payment_sdata[':file_payment_amount']  = $file_payment_amount;
		
		$filter_count++;
	}
	
	if($file_payment_payment_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_payment_status=:file_payment_payment_status";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_payment_status=:file_payment_payment_status";				
		}
		
		// Data
		$get_file_payment_sdata[':file_payment_payment_status']  = $file_paymnet_payment_status;
		
		$filter_count++;
	}
	
	if($file_paymnet_alternative_cash_cheque_issued_against_returned_chq != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_paymnet_alternative_cash_cheque_issued_against_returned_chq=:file_paymnet_alternative_cash_cheque_issued_against_returned_chq";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_paymnet_alternative_cash_cheque_issued_against_returned_chq=:file_paymnet_alternative_cash_cheque_issued_against_returned_chq";				
		}
		
		// Data
		$get_file_payment_sdata[':file_paymnet_alternative_cash_cheque_issued_against_returned_chq']  = $file_paymnet_alternative_cash_cheque_issued_against_returned_chq;
		
		$filter_count++;
	}
	
	if($pay_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_date=:pay_date";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_date=:pay_date";				
		}
		
		// Data
		$get_file_payment_sdata[':pay_date']  = $pay_date;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_request_id=:request_id";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_request_id=:request_id";				
		}
		
		// Data
		$get_file_payment_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_added_by=:added_by";				
		}
		
		// Data
		$get_file_payment_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_added_on >= :start_date";				
		}
		
		//Data
		$get_file_payment_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." where file_payment_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_file_payment_squery_where = $get_file_payment_squery_where." and file_payment_added_on <= :end_date";				
		}
		
		//Data
		$get_file_payment_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_file_payment_squery = $get_file_payment_squery_base.$get_file_payment_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_file_payment_sstatement = $dbConnection->prepare($get_file_payment_squery);
		
		$get_file_payment_sstatement -> execute($get_file_payment_sdata);
		
		$get_file_payment_sdetails = $get_file_payment_sstatement -> fetchAll();
		
		if(FALSE === $get_file_payment_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_file_payment_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_file_payment_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

function db_update_file_details($file,$type,$survey_no,$land_owner,$pan,$extent,$village,$start_date,$num_days,$deal_amount,$brokerage)
{
	// Query
    $file_uquery = "update files set file_type=:type, file_survey_number=:survey_no, file_land_owner=:land_owner, file_pan_number=:pan_no, file_extent=:extent, file_village=:village, file_start_date=:start_date, file_num_days=:num_days, file_deal_amount=:deal_amount, file_brokerage_charges=:brokerage where file_id=:file_id";   		

    try
    {
        $dbConnection = get_conn_handle();
        
        $file_ustatement = $dbConnection->prepare($file_uquery);
        
        // Data
        $file_udata = array(':type'=>$type,':survey_no'=>$survey_no,':land_owner'=>$land_owner,':pan_no'=>$pan,':extent'=>$extent,':village'=>$village,':start_date'=>$start_date,':num_days'=>$num_days,':deal_amount'=>$deal_amount,':brokerage'=>$brokerage,':file_id'=>$file);				
        
        $file_ustatement -> execute($file_udata);
		
		$updated_rows = $file_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE: To enable/disable file
INPUT  : File ID, Active (1 for enabling, 0 for disabling)
OUTPUT : SUCCESS if file enabled/disabled; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_enable_disable_file($file_id,$active)
{
	// Query
    $file_uquery = "update files set file_status=:active where file_id=:file_id";   		
	try
    {
        $dbConnection = get_conn_handle();
        
        $file_ustatement = $dbConnection->prepare($file_uquery);
        
        // Data
        $file_udata = array(':active'=>$active,':file_id'=>$file_id);				
        
        $file_ustatement -> execute($file_udata);
		
		$return["status"] = SUCCESS;
		$return["data"]   = '';
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To get bank list
INPUT 	: Bank Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of banks
BY 		: Nitin Kashyap
*/
function db_get_bank_list($bank_name,$active,$added_by,$start_date,$end_date)
{
	$get_bank_list_squery_base = "select * from crm_bank_master CBM inner join users U on U.user_id = CBM.crm_bank_added_by";
	
	$get_bank_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_bank_list_sdata = array();
	
	if($bank_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_name=:bank_name";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_name=:bank_name";				
		}
		
		// Data
		$get_bank_list_sdata[':bank_name']  = $bank_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_active=:active";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_active=:active";				
		}
		
		// Data
		$get_bank_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_added_by=:added_by";				
		}
		
		// Data
		$get_bank_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_added_on >= :start_date";				
		}
		
		//Data
		$get_bank_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." where crm_bank_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_bank_list_squery_where = $get_bank_list_squery_where." and crm_bank_added_on <= :end_date";				
		}
		
		//Data
		$get_bank_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_bank_list_squery = $get_bank_list_squery_base.$get_bank_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_bank_list_sstatement = $dbConnection->prepare($get_bank_list_squery);
		
		$get_bank_list_sstatement -> execute($get_bank_list_sdata);
		
		$get_bank_list_sdetails = $get_bank_list_sstatement -> fetchAll();
		
		if(FALSE === $get_bank_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_bank_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_bank_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get payment mode list
INPUT 	: Payment Mode, Active, Added By, Start Date, End Date
OUTPUT 	: Payment Mode List
BY 		: Nitin Kashyap
*/
function db_get_payment_mode_list($payment_mode,$payment_mode_active,$added_by,$start_date,$end_date)
{
	$get_payment_mode_list_squery_base = "select * from payment_mode_master";
	
	$get_payment_mode_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_payment_mode_list_sdata = array();
	
	if($payment_mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_name=:payment_mode";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_name=:payment_mode";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':payment_mode']  = $payment_mode;
		
		$filter_count++;
	}
	
	if($payment_mode_active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_active=:active";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_active=:active";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':active']  = $payment_mode_active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_added_by=:added_by";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_added_on >= :start_date";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}
	
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." where payment_mode_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_payment_mode_list_squery_where = $get_payment_mode_list_squery_where." and payment_mode_added_on <= :end_date";				
		}
		
		// Data
		$get_payment_mode_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_payment_mode_list_squery = $get_payment_mode_list_squery_base.$get_payment_mode_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_payment_mode_list_sstatement = $dbConnection->prepare($get_payment_mode_list_squery);
		
		$get_payment_mode_list_sstatement -> execute($get_payment_mode_list_sdata);
		
		$get_payment_mode_list_sdetails = $get_payment_mode_list_sstatement -> fetchAll();
		
		if(FALSE === $get_payment_mode_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_payment_mode_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_payment_mode_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new File Notes
INPUT 	: File ID, File Notes, Added by
OUTPUT 	: File notes id, success or failure message
BY 		: Sonakshi D
*/
function db_add_file_notes($file_id,$file_notes,$file_task,$added_by)
{
	// Query
    $file_notes_iquery = "insert into file_notes (file_notes_file_id,file_notes_remarks,file_notes_task,file_notes_added_by,file_notes_added_on) values (:file_id,:file_notes,:file_task,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $file_notes_istatement = $dbConnection->prepare($file_notes_iquery);
        
        // Data
        $file_notes_idata = array(':file_id'=>$file_id,':file_notes'=>$file_notes,':file_task'=>$file_task,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $file_notes_istatement->execute($file_notes_idata);
		$file_notes_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_notes_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get File Notes List
INPUT 	: File ID,File Notes, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of File Notes
BY 		: Sonakshi D
*/
function db_get_file_notes($file_id,$file_notes,$added_by,$start_date,$end_date)
{
	$get_file_notes_list_squery_base = "select * from file_notes FN inner join users U on FN.file_notes_added_by = U.user_id";
	
	$get_file_notes_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_file_notes_list_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." where file_notes_file_id=:file_id";								
		}
		else
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." and file_notes_file_id=:file_id";				
		}
		
		// Data
		$get_file_notes_list_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($file_notes != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." where file_notes_remarks=:file_notes";								
		}
		else
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." and file_notes_remarks=:file_notes";				
		}
		
		// Data
		$get_file_notes_list_sdata[':file_notes']  = $file_notes;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." where file_notes_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." and file_notes_added_by=:added_by";				
		}
		
		// Data
		$get_file_notes_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." where file_notes_added_on >= :start_date";						
		}
		else
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." and file_notes_added_on >= :start_date";				
		}
		
		//Data
		$get_file_notes_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." where file_notes_added_on <= :end_date";						
		}
		else
		{
			// Query
			$get_file_notes_list_squery_where = $get_file_notes_list_squery_where." and file_notes_added_on <= :end_date";				
		}
		
		//Data
		$get_file_notes_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_file_notes_list_squery = $get_file_notes_list_squery_base.$get_file_notes_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_file_notes_list_sstatement = $dbConnection->prepare($get_file_notes_list_squery);
		
		$get_file_notes_list_sstatement -> execute($get_file_notes_list_sdata);
		
		$get_file_notes_list_sdetails = $get_file_notes_list_sstatement -> fetchAll();
		
		if(FALSE === $get_file_notes_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_file_notes_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_file_notes_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To Update File Notes
INPUT 	: File Notes Id, Task
OUTPUT 	: File Notes id, success or failure message
BY 		: Sonakshi D
*/
function db_update_file_notes($file_notes_id,$file_notes_details)
{
	// Query
    $file_notes_uquery_base = "update file_notes set";  
	
	$file_notes_uquery_set = "";
	
	$file_notes_uquery_where = " where file_notes_id = :file_notes_id";
	
	$file_notes_udata = array(":file_notes_id"=>$file_notes_id);
	
	$filter_count = 0;
	
	if(array_key_exists("task",$file_notes_details))
	{
		$file_notes_uquery_set = $file_notes_uquery_set." file_notes_task=:task,";
		$file_notes_udata[":task"] = $file_notes_details["task"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$file_notes_uquery_set = trim($file_notes_uquery_set,',');
	}
	
	$file_notes_uquery = $file_notes_uquery_base.$file_notes_uquery_set.$file_notes_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $file_notes_ustatement = $dbConnection->prepare($file_notes_uquery);		
        
        $file_notes_ustatement -> execute($file_notes_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_notes_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>