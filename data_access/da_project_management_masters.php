<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new Project Management Master
INPUT 	: Name, project Start Date, Location, Remarks, Added by
OUTPUT 	: Project ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_management_master($name, $project_start_date, $location, $remarks, $added_by)
{
    // Query
    $project_management_master_iquery = "insert into project_management_project_master (project_master_name,project_master_start_date,project_master_location,project_master_active,project_master_remarks,project_master_added_by,project_master_added_on) values (:name,:project_start_date,:location,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_management_master_istatement = $dbConnection->prepare($project_management_master_iquery);

        // Data
        $project_management_master_idata = array(':name'=>$name,':project_start_date'=>$project_start_date,':location'=>$location,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_management_master_istatement->execute($project_management_master_idata);
        $project_management_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_management_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Management Master List
INPUT 	: Project Id, Name, Project Start Date, Location, Active, Added by, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Management Master
BY 		: Lakshmi
*/
function db_get_project_management_master_list($project_management_master_search_data)
{
    // Extract all input parameters
  if (array_key_exists("user_id", $project_management_master_search_data)) {
      $user_id = $project_management_master_search_data["user_id"];
  } else {
      $user_id = "";
  }
    // Extract all input parameters
    if (array_key_exists("project_id", $project_management_master_search_data)) {
        $project_id = $project_management_master_search_data["project_id"];
    } else {
        $project_id = "";
    }

    if (array_key_exists("name", $project_management_master_search_data)) {
        $name = $project_management_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("name_check", $project_management_master_search_data)) {
        $name_check = $project_management_master_search_data["name_check"];
    } else {
        $name_check = "";
    }

    if (array_key_exists("project_start_date", $project_management_master_search_data)) {
        $project_start_date = $project_management_master_search_data["project_start_date"];
    } else {
        $project_start_date = "";
    }

    if (array_key_exists("location", $project_management_master_search_data)) {
        $location = $project_management_master_search_data["location"];
    } else {
        $location = "";
    }

    if (array_key_exists("active", $project_management_master_search_data)) {
        $active = $project_management_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_management_master_search_data)) {
        $added_by = $project_management_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_management_master_search_data)) {
        $start_date = $project_management_master_search_data["start_date"];
    } else {
        $start_date = "";
    }

    if (array_key_exists("end_date", $project_management_master_search_data)) {
        $end_date = $project_management_master_search_data["end_date"];
    } else {
        $end_date = "";
    }

    if (array_key_exists("status", $project_management_master_search_data)) {
        $status = $project_management_master_search_data["status"];
    } else {
        $status = "";
    }
    switch($status)
    {
      case "all" :
    // $get_project_management_master_list_squery_base = "select * from project_management_project_master PMPM inner join users U on U.user_id = PMPM.project_master_added_by";
    $get_project_management_master_list_squery_base = "select * FROM project_management_project_master PMPM
                                                       inner join users U on U.user_id = PMPM.project_master_added_by";

     $get_project_management_master_list_squery_where = "";
     break;

     default :
     // $get_project_management_master_list_squery_base = "select * from project_management_project_master PMPM inner join users U on U.user_id = PMPM.project_master_added_by";
     $get_project_management_master_list_squery_base = "select * FROM project_management_project_master PMPM
                                                        inner join users U on U.user_id = PMPM.project_master_added_by
                                                        inner join project_user_mapping PUM ON PUM.project_user_mapping_user_id =".$user_id;

     $get_project_management_master_list_squery_where = " WHERE PMPM.project_management_master_id = PUM.project_user_mapping_project_id";
      break;
    }
    $filter_count = 0;

    // Data
    $get_project_management_master_list_sdata = array();

    if ($project_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_management_master_id = :project_id";
        } else {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_management_master_id = :project_id";
        }

        // Data
        $get_project_management_master_list_sdata[':project_id'] = $project_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            if ($name_check == '1') {
                // Query
                $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_name = :name";

                // Data
                $get_project_management_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_name like :name";

                // Data
                $get_project_management_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_name like :name";

                // Data
                $get_project_management_master_list_sdata[':name'] = '%'.$name.'%';
            }
        } else {
            if ($name_check == '1') {
                // Query
                $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_name = :name";

                // Data
                $get_project_management_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_name like :name";

                // Data
                $get_project_management_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_name like :name";

                // Data
                $get_project_management_master_list_sdata[':name'] = '%'.$name.'%';
            }
        }

        $filter_count++;
    }

    if ($project_start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_start_date = :project_start_date";
        } else {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_start_date = :project_start_date";
        }

        // Data
        $get_project_management_master_list_sdata[':project_start_date'] = $project_start_date;

        $filter_count++;
    }

    if ($location != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_location = :location";
        } else {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_location = :location";
        }

        // Data
        $get_project_management_master_list_sdata[':location']  = $location;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_active = :active";
        } else {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_active = :active";
        }

        // Data
        $get_project_management_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_added_by = :added_by";
        } else {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_added_by = :added_by";
        }

        //Data
        $get_project_management_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_added_on >= :start_date";
        }

        //Data
        $get_project_management_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_management_master_list_squery_where = $get_project_management_master_list_squery_where." and project_master_added_on <= :end_date";
        }

        //Data
        $get_project_management_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_management_master_list_squery_order_by = " order by project_master_name ASC";
    $get_project_management_master_list_squery = $get_project_management_master_list_squery_base.$get_project_management_master_list_squery_where.$get_project_management_master_list_squery_order_by;


    try {
        $dbConnection = get_conn_handle();

        $get_project_management_master_list_sstatement = $dbConnection->prepare($get_project_management_master_list_squery);

        $get_project_management_master_list_sstatement -> execute($get_project_management_master_list_sdata);

        $get_project_management_master_list_sdetails = $get_project_management_master_list_sstatement -> fetchAll();

        if (false === $get_project_management_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_management_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_management_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Management Master
INPUT 	: Project ID, Project Management Master Update Array
OUTPUT 	: Project ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_management_master($project_id, $project_management_master_update_data)
{
    if (array_key_exists("name", $project_management_master_update_data)) {
        $name = $project_management_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("project_start_date", $project_management_master_update_data)) {
        $project_start_date = $project_management_master_update_data["project_start_date"];
    } else {
        $project_start_date = "";
    }

    if (array_key_exists("location", $project_management_master_update_data)) {
        $location = $project_management_master_update_data["location"];
    } else {
        $location = "";
    }

    if (array_key_exists("active", $project_management_master_update_data)) {
        $active = $project_management_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_management_master_update_data)) {
        $remarks = $project_management_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_management_master_update_data)) {
        $added_by = $project_management_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    $added_on = date('Y-m-d H:i:s');

    // Query
    $project_management_master_update_uquery_base = "update project_management_project_master set";

    $project_management_master_update_uquery_set = "";

    $project_management_master_update_uquery_where = " where project_management_master_id = :project_id";

    $project_management_master_update_udata = array(":project_id"=>$project_id);

    $filter_count = 0;

    if ($name != "") {
        $project_management_master_update_uquery_set = $project_management_master_update_uquery_set." project_master_name = :name,";
        $project_management_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($project_start_date != "") {
        $project_management_master_update_uquery_set = $project_management_master_update_uquery_set." project_master_start_date = :project_start_date,";
        $project_management_master_update_udata[":project_start_date"] = $project_start_date;
        $filter_count++;
    }

    if ($location != "") {
        $project_management_master_update_uquery_set = $project_management_master_update_uquery_set." project_master_location = :location,";
        $project_management_master_update_udata[":location"] = $location;
        $filter_count++;
    }

    if ($active != "") {
        $project_management_master_update_uquery_set = $project_management_master_update_uquery_set." project_master_active = :active,";
        $project_management_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_management_master_update_uquery_set = $project_management_master_update_uquery_set." project_master_remarks = :remarks,";
        $project_management_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_management_master_update_uquery_set = $project_management_master_update_uquery_set." project_master_added_by = :added_by,";
        $project_management_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_management_master_update_uquery_set = $project_management_master_update_uquery_set." project_master_added_on = :added_on,";
        $project_management_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_management_master_update_uquery_set = trim($project_management_master_update_uquery_set, ',');
    }

    $project_management_master_update_uquery = $project_management_master_update_uquery_base.$project_management_master_update_uquery_set.$project_management_master_update_uquery_where;


    try {
        $dbConnection = get_conn_handle();

        $project_management_master_update_ustatement = $dbConnection->prepare($project_management_master_update_uquery);

        $project_management_master_update_ustatement -> execute($project_management_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $project_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Process Master
INPUT 	: Name, Method, Remarks, Added By
OUTPUT 	: Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_process_master($name, $road, $method, $remarks, $added_by)
{
    // Query
    $project_process_master_iquery = "insert into project_process_master (project_process_master_name,project_process_master_road,project_process_master_method,project_process_master_active,project_process_master_remarks,project_process_master_added_by,
   project_process_master_added_on) values (:name,:road,:method,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_process_master_istatement = $dbConnection->prepare($project_process_master_iquery);

        // Data
        $project_process_master_idata = array(':name'=>$name,':road'=>$road,':method'=>$method,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_process_master_istatement->execute($project_process_master_idata);
        $project_process_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_process_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get project Process Master list
INPUT 	: Process ID, Name, Name Check, Method, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Process Master
BY 		: Lakshmi
*/
function db_get_project_process_master($project_process_master_search_data)
{
    if (array_key_exists("process_id", $project_process_master_search_data)) {
        $process_id = $project_process_master_search_data["process_id"];
    } else {
        $process_id= "";
    }

    if (array_key_exists("name", $project_process_master_search_data)) {
        $name = $project_process_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("name_check", $project_process_master_search_data)) {
        $name_check = $project_process_master_search_data["name_check"];
    } else {
        $name_check = "";
    }

    if (array_key_exists("method", $project_process_master_search_data)) {
        $method = $project_process_master_search_data["method"];
    } else {
        $method = "";
    }

    if (array_key_exists("project_id", $project_process_master_search_data)) {
        $project_id = $project_process_master_search_data["project_id"];
    } else {
        $project_id = "";
    }

    if (array_key_exists("active", $project_process_master_search_data)) {
        $active = $project_process_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_process_master_search_data)) {
        $added_by = $project_process_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_process_master_search_data)) {
        $start_date= $project_process_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_process_master_search_data)) {
        $end_date= $project_process_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_process_master_list_squery_base = "select * from project_process_master PPM inner join users U on U.user_id = PPM.project_process_master_added_by";


    $get_project_process_master_list_squery_where = "";
    $get_project_process_master_list_squery_order_by = " order by project_process_master_order ASC";

    $filter_count = 0;

    // Data
    $get_project_process_master_list_sdata = array();

    if ($process_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_id = :process_id";
        } else {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_id = :process_id";
        }

        // Data
        $get_project_process_master_list_sdata[':process_id'] = $process_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            if ($name_check == '1') {
                // Query
                $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_name = :name";

                // Data
                $get_project_process_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_name like :name";

                // Data
                $get_project_process_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_name like :name";

                // Data
                $get_project_process_master_list_sdata[':name'] = '%'.$name.'%';
            }
        } else {
            if ($name_check == '1') {
                // Query
                $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_name = :name";

                // Data
                $get_project_process_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_name like :name";

                // Data
                $get_project_process_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_name like :name";

                // Data
                $get_project_process_master_list_sdata[':name'] = '%'.$name.'%';
            }
        }

        $filter_count++;
    }

    if ($method != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_method = :method";
        } else {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_method = :method";
        }

        // Data
        $get_project_process_master_list_sdata[':method'] = $method;

        $filter_count++;
    }

    if ($project_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where PMPM.project_management_master_id = :project_id";
        } else {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and PMPM.project_management_master_id = :project_id";
        }

        // Data
        $get_project_process_master_list_sdata[':project_id'] = $project_id;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_active = :active";
        } else {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_active = :active";
        }

        // Data
        $get_project_process_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_added_by = :added_by";
        } else {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_added_by = :added_by";
        }

        //Data
        $get_project_process_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_added_on >= :start_date";
        }

        //Data
        $get_project_process_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." where project_process_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_process_master_list_squery_where = $get_project_process_master_list_squery_where." and project_process_master_added_on <= :end_date";
        }

        //Data
        $get_project_process_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_process_master_list_squery = $get_project_process_master_list_squery_base.$get_project_process_master_list_squery_where.$get_project_process_master_list_squery_order_by;


    try {
        $dbConnection = get_conn_handle();

        $get_project_process_master_list_sstatement = $dbConnection->prepare($get_project_process_master_list_squery);

        $get_project_process_master_list_sstatement -> execute($get_project_process_master_list_sdata);

        $get_project_process_master_list_sdetails = $get_project_process_master_list_sstatement -> fetchAll();

        if (false === $get_project_process_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_process_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_process_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Process Master
INPUT 	: Process ID, Project Process Master Update Array
OUTPUT 	: Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_process_master($process_id, $project_process_master_update_data)
{
    if (array_key_exists("name", $project_process_master_update_data)) {
        $name = $project_process_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("road", $project_process_master_update_data)) {
        $road = $project_process_master_update_data["road"];
    } else {
        $road = "";
    }

    if (array_key_exists("method", $project_process_master_update_data)) {
        $method = $project_process_master_update_data["method"];
    } else {
        $method = "";
    }

    if (array_key_exists("active", $project_process_master_update_data)) {
        $active = $project_process_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_process_master_update_data)) {
        $remarks = $project_process_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_process_master_update_data)) {
        $added_by = $project_process_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_process_master_update_data)) {
        $added_on = $project_process_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_process_master_update_uquery_base = "update project_process_master set";

    $project_process_master_update_uquery_set = "";

    $project_process_master_update_uquery_where = " where project_process_master_id = :process_id";

    $project_process_master_update_udata = array(":process_id"=>$process_id);

    $filter_count = 0;

    if ($name != "") {
        $project_process_master_update_uquery_set = $project_process_master_update_uquery_set." project_process_master_name = :name,";
        $project_process_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($road != "") {
        $project_process_master_update_uquery_set = $project_process_master_update_uquery_set." project_process_master_road = :road,";
        $project_process_master_update_udata[":road"] = $road;
        $filter_count++;
    }

    if ($method != "") {
        $project_process_master_update_uquery_set = $project_process_master_update_uquery_set." project_process_master_method = :method,";
        $project_process_master_update_udata[":method"] = $method;
        $filter_count++;
    }

    if ($active != "") {
        $project_process_master_update_uquery_set = $project_process_master_update_uquery_set." project_process_master_active = :active,";
        $project_process_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_process_master_update_uquery_set = $project_process_master_update_uquery_set." project_process_master_remarks = :remarks,";
        $project_process_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_process_master_update_uquery_set = $project_process_master_update_uquery_set." project_process_master_added_by = :added_by,";
        $project_process_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_process_master_update_uquery_set = $project_process_master_update_uquery_set." project_process_master_added_on = :added_on,";
        $project_process_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_process_master_update_uquery_set = trim($project_process_master_update_uquery_set, ',');
    }

    $project_process_master_update_uquery = $project_process_master_update_uquery_base.$project_process_master_update_uquery_set.$project_process_master_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();

        $project_process_master_update_ustatement = $dbConnection->prepare($project_process_master_update_uquery);

        $project_process_master_update_ustatement -> execute($project_process_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $process_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Task Master
INPUT 	: Name, Process, Remarks, Added By
OUTPUT 	: Task Master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_master($name, $uom, $process, $remarks, $added_by)
{

    // Query
    $project_task_master_iquery = "insert into project_task_master (project_task_master_name,project_task_master_uom,project_task_master_process,project_task_master_active,project_task_master_remarks,project_task_master_added_by,project_task_master_added_on) values(:name,:uom,:process,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_task_master_istatement = $dbConnection->prepare($project_task_master_iquery);

        // Data
        $project_task_master_idata = array(':name'=>$name,':uom'=>$uom,':process'=>$process,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_task_master_istatement->execute($project_task_master_idata);
        $project_task_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_task_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get project Task Master list
INPUT 	: Task Master ID, Name, Process, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Task Master
BY 		: Lakshmi
*/
function db_get_project_task_master($project_task_master_search_data)
{
    if (array_key_exists("task_master_id", $project_task_master_search_data)) {
        $task_master_id = $project_task_master_search_data["task_master_id"];
    } else {
        $task_master_id= "";
    }

    if (array_key_exists("name", $project_task_master_search_data)) {
        $name = $project_task_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("task_name_check", $project_task_master_search_data)) {
        $task_name_check = $project_task_master_search_data["task_name_check"];
    } else {
        $task_name_check = "";
    }

    if (array_key_exists("process", $project_task_master_search_data)) {
        $process = $project_task_master_search_data["process"];
    } else {
        $process = "";
    }

    if (array_key_exists("user_id", $project_task_master_search_data)) {
        $user_id = $project_task_master_search_data["user_id"];
    } else {
        $user_id = "";
    }

    if (array_key_exists("active", $project_task_master_search_data)) {
        $active = $project_task_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_task_master_search_data)) {
        $added_by = $project_task_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_task_master_search_data)) {
        $start_date= $project_task_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_task_master_search_data)) {
        $end_date= $project_task_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_task_master_list_squery_base = "select * from project_task_master PTM inner join project_process_master PPM on PPM.project_process_master_id = PTM.project_task_master_process inner join users U on U.user_id = PTM.project_task_master_added_by left outer join project_uom_master POM on POM.project_uom_id=PTM.project_task_master_uom";

    $get_project_task_master_list_squery_where = "";
    $get_project_task_master_list_squery_order_by = "";

    $filter_count = 0;

    // Data
    $get_project_task_master_list_sdata = array();

    if ($task_master_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_id = :task_master_id";
        } else {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_id = :task_master_id";
        }

        // Data
        $get_project_task_master_list_sdata[':task_master_id'] = $task_master_id;

        $filter_count++;
    }


    if ($name != "") {
        if ($filter_count == 0) {
            if ($task_name_check == '1') {
                // Query
                $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_name = :name";

                // Data
                $get_project_task_master_list_sdata[':name'] = $name;
            } elseif ($task_name_check == '2') {
                // Query
                $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_name like :name";

                // Data
                $get_project_task_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_name like :name";

                // Data
                $get_project_task_master_list_sdata[':name'] = '%'.$name.'%';
            }
        } else {
            if ($task_name_check == '1') {
                // Query
                $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_name = :name";

                // Data
                $get_project_task_master_list_sdata[':name'] = $name;
            } elseif ($task_name_check == '2') {
                // Query
                $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_name like :name";

                // Data
                $get_project_task_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_name like :name";

                // Data
                $get_project_task_master_list_sdata[':name'] = '%'.$name.'%';
            }
        }

        $filter_count++;
    }


    if ($process != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_process = :process";
        } else {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_process = :process";
        }

        // Data
        $get_project_task_master_list_sdata[':process'] = $process;

        $filter_count++;
    }

    if ($user_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where U.user_id = :user_id";
        } else {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and U.user_id = :user_id";
        }

        // Data
        $get_project_task_master_list_sdata[':user_id'] = $user_id;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_active = :active";
        } else {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_active = :active";
        }

        // Data
        $get_project_task_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_added_by = :added_by";
        } else {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_added_by = :added_by";
        }

        //Data
        $get_project_task_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_added_on >= :start_date";
        }

        //Data
        $get_project_task_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." where project_task_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_task_master_list_squery_where = $get_project_task_master_list_squery_where." and project_task_master_added_on <= :end_date";
        }

        //Data
        $get_project_task_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }
    $get_project_task_master_list_squery_order_by = " order by project_process_master_order ASC,project_task_master_order ASC";
    $get_project_task_master_list_squery = $get_project_task_master_list_squery_base.$get_project_task_master_list_squery_where.$get_project_task_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_task_master_list_sstatement = $dbConnection->prepare($get_project_task_master_list_squery);

        $get_project_task_master_list_sstatement -> execute($get_project_task_master_list_sdata);

        $get_project_task_master_list_sdetails = $get_project_task_master_list_sstatement -> fetchAll();

        if (false === $get_project_task_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_task_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_task_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Task Master
INPUT 	: Task Master ID, Project Task Master Update Array, Process ID
OUTPUT 	: Task Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_master($task_master_id, $project_task_master_update_data, $process_id='')
{
    if (array_key_exists("name", $project_task_master_update_data)) {
        $name = $project_task_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("uom", $project_task_master_update_data)) {
        $uom = $project_task_master_update_data["uom"];
    } else {
        $uom = "";
    }

    if (array_key_exists("process", $project_task_master_update_data)) {
        $process = $project_task_master_update_data["process"];
    } else {
        $process = "";
    }

    if (array_key_exists("active", $project_task_master_update_data)) {
        $active = $project_task_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_task_master_update_data)) {
        $remarks = $project_task_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_task_master_update_data)) {
        $added_by = $project_task_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_task_master_update_data)) {
        $added_on = $project_task_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_task_master_update_uquery_base = "update project_task_master set";

    $project_task_master_update_uquery_set = "";

    if ($process_id != '') {
        $project_task_master_update_uquery_where = " where project_task_master_process = :process_id";

        $project_task_master_update_udata = array(":process_id"=>$process_id);
    } else {
        $project_task_master_update_uquery_where = " where project_task_master_id = :task_master_id";

        $project_task_master_update_udata = array(":task_master_id"=>$task_master_id);
    }

    $filter_count = 0;

    if ($name != "") {
        $project_task_master_update_uquery_set = $project_task_master_update_uquery_set." project_task_master_name = :name,";
        $project_task_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($uom != "") {
        $project_task_master_update_uquery_set = $project_task_master_update_uquery_set." project_task_master_uom = :uom,";
        $project_task_master_update_udata[":uom"] = $uom;
        $filter_count++;
    }

    if ($process != "") {
        $project_task_master_update_uquery_set = $project_task_master_update_uquery_set." project_task_master_process = :process,";
        $project_task_master_update_udata[":process"] = $process;
        $filter_count++;
    }

    if ($active != "") {
        $project_task_master_update_uquery_set = $project_task_master_update_uquery_set." project_task_master_active = :active,";
        $project_task_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_task_master_update_uquery_set = $project_task_master_update_uquery_set." project_task_master_remarks = :remarks,";
        $project_task_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_task_master_update_uquery_set = $project_task_master_update_uquery_set." project_task_master_added_by = :added_by,";
        $project_task_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_task_master_update_uquery_set = $project_task_master_update_uquery_set." project_task_master_added_on = :added_on,";
        $project_task_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_task_master_update_uquery_set = trim($project_task_master_update_uquery_set, ',');
    }

    $project_task_master_update_uquery = $project_task_master_update_uquery_base.$project_task_master_update_uquery_set.$project_task_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_task_master_update_ustatement = $dbConnection->prepare($project_task_master_update_uquery);

        $project_task_master_update_ustatement -> execute($project_task_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $task_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Man Power Type Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Power Type ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_man_power_master($name, $remarks, $added_by)
{
    // Query
    $project_man_power_master_iquery = "insert into project_man_power_type_master (project_man_power_name,project_man_power_active,project_man_power_remarks,project_man_power_added_by,project_man_power_added_on) values(:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_man_power_master_istatement = $dbConnection->prepare($project_man_power_master_iquery);

        // Data
        $project_man_power_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
        ':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_man_power_master_istatement->execute($project_man_power_master_idata);
        $project_man_power_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_man_power_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get project Man Power Type Master list
INPUT 	: Power Type ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Man Power Master
BY 		: Lakshmi
*/
function db_get_project_man_power_master($project_man_power_master_search_data)
{
    if (array_key_exists("power_type_id", $project_man_power_master_search_data)) {
        $power_type_id = $project_man_power_master_search_data["power_type_id"];
    } else {
        $power_type_id= "";
    }

    if (array_key_exists("name", $project_man_power_master_search_data)) {
        $name = $project_man_power_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_man_power_master_search_data)) {
        $active = $project_man_power_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_man_power_master_search_data)) {
        $added_by = $project_man_power_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_man_power_master_search_data)) {
        $start_date= $project_man_power_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_man_power_master_search_data)) {
        $end_date= $project_man_power_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_man_power_master_list_squery_base = "select * from project_man_power_type_master PMPTM inner join users U on U.user_id = PMPTM.project_man_power_added_by";

    $get_project_man_power_master_list_squery_where = "";
    $get_project_man_power_master_list_squery_order_by = " order by project_man_power_name ASC";

    $filter_count = 0;

    // Data
    $get_project_man_power_master_list_sdata = array();

    if ($power_type_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." where project_man_power_master_id = :power_type_id";
        } else {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." and project_man_power_master_id = :power_type_id";
        }

        // Data
        $get_project_man_power_master_list_sdata[':power_type_id'] = $power_type_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." where project_man_power_name = :name";
        } else {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." and project_man_power_name = :name";
        }

        // Data
        $get_project_man_power_master_list_sdata[':name'] = $name;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." where project_man_power_active = :active";
        } else {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." and project_man_power_active = :active";
        }

        // Data
        $get_project_man_power_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." where project_man_power_added_by = :added_by";
        } else {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." and project_man_power_added_by = :added_by";
        }

        //Data
        $get_project_man_power_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." where project_man_power_added_on >= :start_date";
        } else {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." and project_man_power_added_on >= :start_date";
        }

        //Data
        $get_project_man_power_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." where project_man_power_added_on <= :end_date";
        } else {
            // Query
            $get_project_man_power_master_list_squery_where = $get_project_man_power_master_list_squery_where." and project_man_power_added_on <= :end_date";
        }

        //Data
        $get_project_man_power_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_man_power_master_list_squery = $get_project_man_power_master_list_squery_base.$get_project_man_power_master_list_squery_where.$get_project_man_power_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_man_power_master_list_sstatement = $dbConnection->prepare($get_project_man_power_master_list_squery);

        $get_project_man_power_master_list_sstatement -> execute($get_project_man_power_master_list_sdata);

        $get_project_man_power_master_list_sdetails = $get_project_man_power_master_list_sstatement -> fetchAll();

        if (false === $get_project_man_power_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_man_power_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_man_power_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Man Power Type Master
INPUT 	: Power Type ID, Project Man Power Master Update Array
OUTPUT 	: Power Type ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_man_power_master($power_type_id, $project_man_power_master_update_data)
{
    if (array_key_exists("name", $project_man_power_master_update_data)) {
        $name = $project_man_power_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_man_power_master_update_data)) {
        $active = $project_man_power_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_man_power_master_update_data)) {
        $remarks = $project_man_power_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_man_power_master_update_data)) {
        $added_by = $project_man_power_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_man_power_master_update_data)) {
        $added_on = $project_man_power_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_man_power_master_update_uquery_base = "update project_man_power_type_master set";

    $project_man_power_master_update_uquery_set = "";

    $project_man_power_master_update_uquery_where = " where project_man_power_master_id = :power_type_id";

    $project_man_power_master_update_udata = array(":power_type_id"=>$power_type_id);

    $filter_count = 0;

    if ($name != "") {
        $project_man_power_master_update_uquery_set = $project_man_power_master_update_uquery_set." project_man_power_name = :name,";
        $project_man_power_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $project_man_power_master_update_uquery_set = $project_man_power_master_update_uquery_set." project_man_power_active = :active,";
        $project_man_power_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_man_power_master_update_uquery_set = $project_man_power_master_update_uquery_set." project_man_power_remarks = :remarks,";
        $project_man_power_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_man_power_master_update_uquery_set = $project_man_power_master_update_uquery_set." project_man_power_added_by = :added_by,";
        $project_man_power_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_man_power_master_update_uquery_set = $project_man_power_master_update_uquery_set." project_man_power_added_on = :added_on,";
        $project_man_power_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_man_power_master_update_uquery_set = trim($project_man_power_master_update_uquery_set, ',');
    }

    $project_man_power_master_update_uquery = $project_man_power_master_update_uquery_base.$project_man_power_master_update_uquery_set.$project_man_power_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_man_power_master_update_ustatement = $dbConnection->prepare($project_man_power_master_update_uquery);

        $project_man_power_master_update_ustatement -> execute($project_man_power_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $power_type_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Reason ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_reason_master($name, $remarks, $added_by)
{
    // Query
    $project_reason_master_iquery = "insert into project_reason_master (project_reason_master_name,project_reason_master_active,project_reason_master_remarks,project_reason_master_added_by,project_reason_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_reason_master_istatement = $dbConnection->prepare($project_reason_master_iquery);

        // Data
        $project_reason_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_reason_master_istatement->execute($project_reason_master_idata);
        $project_reason_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_reason_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get project Reason Master list
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Reason Master
BY 		: Lakshmi
*/
function db_get_project_reason_master($project_reason_master_search_data)
{
    if (array_key_exists("reason_id", $project_reason_master_search_data)) {
        $reason_id = $project_reason_master_search_data["reason_id"];
    } else {
        $reason_id= "";
    }

    if (array_key_exists("name", $project_reason_master_search_data)) {
        $name = $project_reason_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_reason_master_search_data)) {
        $active = $project_reason_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_reason_master_search_data)) {
        $added_by = $project_reason_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_reason_master_search_data)) {
        $start_date= $project_reason_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_reason_master_search_data)) {
        $end_date= $project_reason_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_reason_master_list_squery_base = "select * from project_reason_master PRM inner join users U on U.user_id = PRM.project_reason_master_added_by";

    $get_project_reason_master_list_squery_where = "";
    $get_project_reason_master_list_squery_order_by = " order by project_reason_master_name ASC";
    $filter_count = 0;

    // Data
    $get_project_reason_master_list_sdata = array();

    if ($reason_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." where project_reason_master_id = :reason_id";
        } else {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." and project_reason_master_id = :reason_id";
        }

        // Data
        $get_project_reason_master_list_sdata[':reason_id'] = $reason_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." where project_reason_master_name = :name";
        } else {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." and project_reason_master_name = :name";
        }

        // Data
        $get_project_reason_master_list_sdata[':name'] = $name;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." where project_reason_master_active = :active";
        } else {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." and project_reason_master_active = :active";
        }

        // Data
        $get_project_reason_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." where project_reason_master_added_by = :added_by";
        } else {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." and project_reason_master_added_by = :added_by";
        }

        //Data
        $get_project_reason_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." where project_reason_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." and project_reason_master_added_on >= :start_date";
        }

        //Data
        $get_project_reason_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." where project_reason_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_reason_master_list_squery_where = $get_project_reason_master_list_squery_where." and project_reason_master_added_on <= :end_date";
        }

        //Data
        $get_project_reason_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_reason_master_list_squery = $get_project_reason_master_list_squery_base.$get_project_reason_master_list_squery_where.$get_project_reason_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_reason_master_list_sstatement = $dbConnection->prepare($get_project_reason_master_list_squery);

        $get_project_reason_master_list_sstatement -> execute($get_project_reason_master_list_sdata);

        $get_project_reason_master_list_sdetails = $get_project_reason_master_list_sstatement -> fetchAll();

        if (false === $get_project_reason_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_reason_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_reason_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Reason Master
INPUT 	: Reason ID, Project Reason Master Update Array
OUTPUT 	: Reason ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_reason_master($reason_id, $project_reason_master_update_data)
{
    if (array_key_exists("name", $project_reason_master_update_data)) {
        $name = $project_reason_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_reason_master_update_data)) {
        $active = $project_reason_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_reason_master_update_data)) {
        $remarks = $project_reason_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_reason_master_update_data)) {
        $added_by = $project_reason_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_reason_master_update_data)) {
        $added_on = $project_reason_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_reason_master_update_uquery_base = "update project_reason_master set";

    $project_reason_master_update_uquery_set = "";

    $project_reason_master_update_uquery_where = " where project_reason_master_id = :reason_id";

    $project_reason_master_update_udata = array(":reason_id"=>$reason_id);

    $filter_count = 0;

    if ($name != "") {
        $project_reason_master_update_uquery_set = $project_reason_master_update_uquery_set." project_reason_master_name = :name,";
        $project_reason_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $project_reason_master_update_uquery_set = $project_reason_master_update_uquery_set." project_reason_master_active = :active,";
        $project_reason_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_reason_master_update_uquery_set = $project_reason_master_update_uquery_set." project_reason_master_remarks = :remarks,";
        $project_reason_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_reason_master_update_uquery_set = $project_reason_master_update_uquery_set." project_reason_master_added_by = :added_by,";
        $project_reason_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_reason_master_update_uquery_set = $project_reason_master_update_uquery_set." project_reason_master_added_on = :added_on,";
        $project_reason_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_reason_master_update_uquery_set = trim($project_reason_master_update_uquery_set, ',');
    }

    $project_reason_master_update_uquery = $project_reason_master_update_uquery_base.$project_reason_master_update_uquery_set.$project_reason_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_reason_master_update_ustatement = $dbConnection->prepare($project_reason_master_update_uquery);

        $project_reason_master_update_ustatement -> execute($project_reason_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $reason_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Machine Master
INPUT 	: Name, Machine No, Use, Machine Type, Added By
OUTPUT 	: Machine ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_machine_master($name, $machine_no, $vendor, $type, $use, $machine_type, $remarks, $added_by)
{
    // Query
    $project_machine_master_iquery = "insert into project_machine_master (project_machine_master_name,project_machine_master_id_number,project_machine_master_vendor,project_machine_type,project_machine_master_use,project_machine_master_machine_type,project_machine_master_active,project_machine_master_remarks,project_machine_master_added_by,project_machine_master_added_on) values (:name,:number,:vendor,:type,:use,:machine_type,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_machine_master_istatement = $dbConnection->prepare($project_machine_master_iquery);

        // Data
        $project_machine_master_idata = array(':name'=>$name,':number'=>$machine_no,':vendor'=>$vendor,':type'=>$type,':use'=>$use,':machine_type'=>$machine_type,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_machine_master_istatement->execute($project_machine_master_idata);
        $project_machine_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get project Machine Master list
INPUT 	: machine ID, Name, Number, Use, Machine Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Machine Master
BY 		: Lakshmi
*/
function db_get_project_machine_master($project_machine_master_search_data)
{
    if (array_key_exists("machine_id", $project_machine_master_search_data)) {
        $machine_id = $project_machine_master_search_data["machine_id"];
    } else {
        $machine_id= "";
    }

    if (array_key_exists("machine_name_check", $project_machine_master_search_data)) {
        $machine_name_check = $project_machine_master_search_data["machine_name_check"];
    } else {
        $machine_name_check = "";
    }

    if (array_key_exists("name", $project_machine_master_search_data)) {
        $name = $project_machine_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("number", $project_machine_master_search_data)) {
        $number = $project_machine_master_search_data["number"];
    } else {
        $number = "";
    }

    if (array_key_exists("vendor", $project_machine_master_search_data)) {
        $vendor = $project_machine_master_search_data["vendor"];
    } else {
        $vendor = "";
    }

    if (array_key_exists("type", $project_machine_master_search_data)) {
        $type = $project_machine_master_search_data["type"];
    } else {
        $type = "";
    }

    if (array_key_exists("use", $project_machine_master_search_data)) {
        $use = $project_machine_master_search_data["use"];
    } else {
        $use = "";
    }

    if (array_key_exists("machine_type", $project_machine_master_search_data)) {
        $machine_type = $project_machine_master_search_data["machine_type"];
    } else {
        $machine_type = "";
    }

    if (array_key_exists("active", $project_machine_master_search_data)) {
        $active = $project_machine_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_machine_master_search_data)) {
        $added_by = $project_machine_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_machine_master_search_data)) {
        $start_date= $project_machine_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_machine_master_search_data)) {
        $end_date= $project_machine_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_machine_master_list_squery_base = "select * from project_machine_master PMM inner join users U on U.user_id = PMM.project_machine_master_added_by inner join project_machine_type_master PMTM on PMTM.project_machine_type_master_id =PMM.project_machine_master_machine_type inner join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id=PMM.project_machine_master_vendor";

    $get_project_machine_master_list_squery_where = "";

    $filter_count = 0;

    // Data
    $get_project_machine_master_list_sdata = array();

    if ($machine_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_id = :machine_id";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_id = :machine_id";
        }

        // Data
        $get_project_machine_master_list_sdata[':machine_id'] = $machine_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            if ($machine_name_check == '1') {
                $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_name = :name";
                // Data
                $get_project_machine_master_list_sdata[':name']  = $name;
            } elseif ($machine_name_check == '2') {
                $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_name like :name";

                // Data
                $get_project_machine_master_list_sdata[':name']  = '%'.$name;
            } else {
                $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_name like :name";

                // Data
                $get_project_machine_master_list_sdata[':name']  = '%'.$name.'%';
            }
        } else {
            // Query
            if ($machine_name_check == '1') {
                $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_name = :name";

                // Data
                $get_project_machine_master_list_sdata[':name']  = $name;
            } elseif ($machine_name_check == '2') {
                $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." or project_machine_master_name like :name";
                // Data
                $get_project_machine_master_list_sdata[':name']  = '%'.$name;
            } else {
                $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_name like :name";

                // Data
                $get_project_machine_master_list_sdata[':name']  = '%'.$name.'%';
            }
        }

        $filter_count++;
    }

    if ($number != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_id_number like :number";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_id_number like :number";
        }

        // Data
        $get_project_machine_master_list_sdata[':number'] = '%'.$number;

        $filter_count++;
    }

    if ($vendor != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_vendor = :vendor";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_vendor = :vendor";
        }

        // Data
        $get_project_machine_master_list_sdata[':vendor'] = $vendor;

        $filter_count++;
    }

    if ($type != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_type = :type";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_type = :type";
        }

        // Data
        $get_project_machine_master_list_sdata[':type'] = $type;

        $filter_count++;
    }

    if ($use != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_use = :use";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_use = :use";
        }

        // Data
        $get_project_machine_master_list_sdata[':use'] = $use;

        $filter_count++;
    }

    if ($machine_type != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_machine_type = :machine_type";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_machine_type = :machine_type";
        }

        // Data
        $get_project_machine_master_list_sdata[':machine_type'] = $machine_type;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_active = :active";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_active = :active";
        }

        // Data
        $get_project_machine_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_added_by = :added_by";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_added_by = :added_by";
        }

        //Data
        $get_project_machine_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_added_on >= :start_date";
        }

        //Data
        $get_project_machine_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." where project_machine_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_machine_master_list_squery_where = $get_project_machine_master_list_squery_where." and project_machine_master_added_on <= :end_date";
        }

        //Data
        $get_project_machine_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_machine_master_list_squery_order = ' order by project_machine_master_name asc,project_machine_master_id_number asc';

    $get_project_machine_master_list_squery = $get_project_machine_master_list_squery_base.$get_project_machine_master_list_squery_where.$get_project_machine_master_list_squery_order;

    try {
        $dbConnection = get_conn_handle();

        $get_project_machine_master_list_sstatement = $dbConnection->prepare($get_project_machine_master_list_squery);

        $get_project_machine_master_list_sstatement -> execute($get_project_machine_master_list_sdata);

        $get_project_machine_master_list_sdetails = $get_project_machine_master_list_sstatement -> fetchAll();

        if (false === $get_project_machine_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_machine_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_machine_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Master
INPUT 	: Machine ID, Project Machine Master Update Array
OUTPUT 	: Machine ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_machine_master($machine_id, $project_machine_master_update_data)
{
    if (array_key_exists("name", $project_machine_master_update_data)) {
        $name = $project_machine_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("number", $project_machine_master_update_data)) {
        $number = $project_machine_master_update_data["number"];
    } else {
        $number = "";
    }

    if (array_key_exists("vendor", $project_machine_master_update_data)) {
        $vendor = $project_machine_master_update_data["vendor"];
    } else {
        $vendor = "";
    }

    if (array_key_exists("type", $project_machine_master_update_data)) {
        $type = $project_machine_master_update_data["type"];
    } else {
        $type = "";
    }


    if (array_key_exists("use", $project_machine_master_update_data)) {
        $use = $project_machine_master_update_data["use"];
    } else {
        $use = "";
    }

    if (array_key_exists("machine_type", $project_machine_master_update_data)) {
        $machine_type = $project_machine_master_update_data["machine_type"];
    } else {
        $machine_type = "";
    }

    if (array_key_exists("active", $project_machine_master_update_data)) {
        $active = $project_machine_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_machine_master_update_data)) {
        $remarks = $project_machine_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_machine_master_update_data)) {
        $added_by = $project_machine_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_machine_master_update_data)) {
        $added_on = $project_machine_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_machine_master_update_uquery_base = "update project_machine_master set ";

    $project_machine_master_update_uquery_set = "";

    $project_machine_master_update_uquery_where = " where project_machine_master_id = :machine_id";

    $project_machine_master_update_udata = array(":machine_id"=>$machine_id);

    $filter_count = 0;

    if ($name != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_name = :name,";
        $project_machine_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($number != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_id_number = :number,";
        $project_machine_master_update_udata[":number"] = $number;
        $filter_count++;
    }

    if ($vendor != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_vendor = :vendor,";
        $project_machine_master_update_udata[":vendor"] = $vendor;
        $filter_count++;
    }

    if ($type != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_type = :type,";
        $project_machine_master_update_udata[":type"] = $type;
        $filter_count++;
    }

    if ($use != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_use = :use,";
        $project_machine_master_update_udata[":use"] = $use;
        $filter_count++;
    }

    if ($machine_type != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_machine_type = :machine_type,";
        $project_machine_master_update_udata[":machine_type"] = $machine_type;
        $filter_count++;
    }

    if ($active != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_active = :active,";
        $project_machine_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_remarks = :remarks,";
        $project_machine_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_added_by = :added_by,";
        $project_machine_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_machine_master_update_uquery_set = $project_machine_master_update_uquery_set." project_machine_master_added_on = :added_on,";
        $project_machine_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_machine_master_update_uquery_set = trim($project_machine_master_update_uquery_set, ',');
    }

    $project_machine_master_update_uquery = $project_machine_master_update_uquery_base.$project_machine_master_update_uquery_set.$project_machine_master_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();

        $project_machine_master_update_ustatement = $dbConnection->prepare($project_machine_master_update_uquery);

        $project_machine_master_update_ustatement -> execute($project_machine_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $machine_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Machine Type Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Machine Type ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_machine_type_master($name, $remarks, $added_by)
{
    // Query
    $project_machine_type_master_iquery = "insert into project_machine_type_master (project_machine_type_master_name,project_machine_type_master_active,project_machine_type_master_remarks,project_machine_type_master_added_by,project_machine_type_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_machine_type_master_istatement = $dbConnection->prepare($project_machine_type_master_iquery);

        // Data
        $project_machine_type_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_machine_type_master_istatement->execute($project_machine_type_master_idata);
        $project_machine_type_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_type_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get project Machine Type Master list
INPUT 	: Machine Type ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Machine Type Master
BY 		: Lakshmi
*/
function db_get_project_machine_type_master($project_machine_type_master_search_data)
{
    if (array_key_exists("machine_type_id", $project_machine_type_master_search_data)) {
        $machine_type_id = $project_machine_type_master_search_data["machine_type_id"];
    } else {
        $machine_type_id= "";
    }

    if (array_key_exists("name", $project_machine_type_master_search_data)) {
        $name = $project_machine_type_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_machine_type_master_search_data)) {
        $active = $project_machine_type_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_machine_type_master_search_data)) {
        $added_by = $project_machine_type_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_machine_type_master_search_data)) {
        $start_date= $project_machine_type_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_machine_type_master_search_data)) {
        $end_date= $project_machine_type_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_machine_type_master_list_squery_base = " select * from project_machine_type_master PMT inner join users U on U.user_id = PMT.project_machine_type_master_added_by";

    $get_project_machine_type_master_list_squery_where = "";
    $get_project_machine_type_master_list_squery_order_by = " order by project_machine_type_master_name ASC";


    $filter_count = 0;

    // Data
    $get_project_machine_type_master_list_sdata = array();

    if ($machine_type_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." where project_machine_type_master_id = :machine_type_id";
        } else {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." and project_machine_type_master_id = :machine_type_id";
        }

        // Data
        $get_project_machine_type_master_list_sdata[':machine_type_id'] = $machine_type_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." where project_machine_type_master_name = :name";
        } else {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." and project_machine_type_master_name = :name";
        }

        // Data
        $get_project_machine_type_master_list_sdata[':name'] = $name;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." where project_machine_type_master_active = :active";
        } else {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." and project_machine_type_master_active = :active";
        }

        // Data
        $get_project_machine_type_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." where project_machine_type_master_added_by = :added_by";
        } else {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." and project_machine_type_master_added_by = :added_by";
        }

        //Data
        $get_project_machine_type_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." where project_machine_type_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." and project_machine_type_master_added_on >= :start_date";
        }

        //Data
        $get_project_machine_type_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." where project_machine_type_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_machine_type_master_list_squery_where = $get_project_machine_type_master_list_squery_where." and project_machine_type_master_added_on <= :end_date";
        }

        //Data
        $get_project_machine_type_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }


    $get_project_machine_type_master_list_squery = $get_project_machine_type_master_list_squery_base.$get_project_machine_type_master_list_squery_where.$get_project_machine_type_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_machine_type_master_list_sstatement = $dbConnection->prepare($get_project_machine_type_master_list_squery);

        $get_project_machine_type_master_list_sstatement -> execute($get_project_machine_type_master_list_sdata);

        $get_project_machine_type_master_list_sdetails = $get_project_machine_type_master_list_sstatement -> fetchAll();

        if (false === $get_project_machine_type_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_machine_type_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_machine_type_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

 /*
PURPOSE : To update Project Machine Type Master
INPUT 	: Machine Type ID, Project Machine Type Master Update Array
OUTPUT 	: Machine Type ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_machine_type_master($machine_type_id, $project_machine_type_master_update_data)
{
    if (array_key_exists("name", $project_machine_type_master_update_data)) {
        $name = $project_machine_type_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_machine_type_master_update_data)) {
        $active = $project_machine_type_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_machine_type_master_update_data)) {
        $remarks = $project_machine_type_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_machine_type_master_update_data)) {
        $added_by = $project_machine_type_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_machine_type_master_update_data)) {
        $added_on = $project_machine_type_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_machine_type_master_update_uquery_base = "update project_machine_type_master set";

    $project_machine_type_master_update_uquery_set = "";

    $project_machine_type_master_update_uquery_where = " where project_machine_type_master_id = :machine_type_id";

    $project_machine_type_master_update_udata = array(":machine_type_id"=>$machine_type_id);

    $filter_count = 0;

    if ($name != "") {
        $project_machine_type_master_update_uquery_set = $project_machine_type_master_update_uquery_set." project_machine_type_master_name = :name,";
        $project_machine_type_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $project_machine_type_master_update_uquery_set = $project_machine_type_master_update_uquery_set." project_machine_type_master_active = :active,";
        $project_machine_type_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_machine_type_master_update_uquery_set = $project_machine_type_master_update_uquery_set." project_machine_type_master_remarks = :remarks,";
        $project_machine_type_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_machine_type_master_update_uquery_set = $project_machine_type_master_update_uquery_set." project_machine_type_master_added_by = :added_by,";
        $project_machine_type_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_machine_type_master_update_uquery_set = $project_machine_type_master_update_uquery_set." project_machine_type_master_added_on = :added_on,";
        $project_machine_type_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_machine_type_master_update_uquery_set = trim($project_machine_type_master_update_uquery_set, ',');
    }

    $project_machine_type_master_update_uquery = $project_machine_type_master_update_uquery_base.$project_machine_type_master_update_uquery_set.$project_machine_type_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_machine_type_master_update_ustatement = $dbConnection->prepare($project_machine_type_master_update_uquery);

        $project_machine_type_master_update_ustatement -> execute($project_machine_type_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $machine_type_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Man Power Rate Master
INPUT 	: Power Type ID, Vendor ID, Cost Per Hours, Applicable Date, Remarks, Added By
OUTPUT 	: Rate ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_man_power_rate_master($power_type_id, $vendor_id, $cost_per_hours, $applicable_date, $remarks, $added_by)
{
    // Query
    $project_man_power_rate_master_iquery = "insert into project_man_power_rate_master (project_man_power_type_id,project_man_power_rate_vendor_id,project_man_power_rate_cost_per_hours,project_man_power_rate_applicable_date,
    project_man_power_rate_active,project_man_power_rate_remarks,project_man_power_rate_added_by,project_man_power_rate_added_on) values(:power_type_id,:vendor_id,:cost_per_hours,:applicable_date,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_man_power_rate_master_istatement = $dbConnection->prepare($project_man_power_rate_master_iquery);

        // Data
        $project_man_power_rate_master_idata = array(':power_type_id'=>$power_type_id,':vendor_id'=>$vendor_id,':cost_per_hours'=>$cost_per_hours,
        ':applicable_date'=>$applicable_date,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_man_power_rate_master_istatement->execute($project_man_power_rate_master_idata);
        $project_man_power_rate_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_man_power_rate_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Man Power Rate Master list
INPUT 	: Rate ID, Power Type ID, Vendor ID, Cost Per Hours, Applicable Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Man Power Rate Master
BY 		: Lakshmi
*/
function db_get_project_man_power_rate_master($project_man_power_rate_search_data)
{
    if (array_key_exists("rate_id", $project_man_power_rate_search_data)) {
        $rate_id = $project_man_power_rate_search_data["rate_id"];
    } else {
        $rate_id= "";
    }

    if (array_key_exists("power_type_id", $project_man_power_rate_search_data)) {
        $power_type_id = $project_man_power_rate_search_data["power_type_id"];
    } else {
        $power_type_id= "";
    }


    if (array_key_exists("vendor_id", $project_man_power_rate_search_data)) {
        $vendor_id= $project_man_power_rate_search_data["vendor_id"];
    } else {
        $vendor_id = "";
    }

    if (array_key_exists("cost_per_hours", $project_man_power_rate_search_data)) {
        $cost_per_hours= $project_man_power_rate_search_data["cost_per_hours"];
    } else {
        $cost_per_hours = "";
    }

    if (array_key_exists("applicable_date", $project_man_power_rate_search_data)) {
        $applicable_date= $project_man_power_rate_search_data["applicable_date"];
    } else {
        $applicable_date= "";
    }

    if (array_key_exists("active", $project_man_power_rate_search_data)) {
        $active= $project_man_power_rate_search_data["active"];
    } else {
        $active= "";
    }

    if (array_key_exists("added_by", $project_man_power_rate_search_data)) {
        $added_by= $project_man_power_rate_search_data["added_by"];
    } else {
        $added_by= "";
    }

    if (array_key_exists("start_date", $project_man_power_rate_search_data)) {
        $start_date= $project_man_power_rate_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $project_man_power_rate_search_data)) {
        $end_date= $project_man_power_rate_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_man_power_rate_list_squery_base = " select *,AU.user_name as updated_by,U.user_name as added_by from project_man_power_rate_master PMPRM inner join users U on U.user_id= PMPRM.project_man_power_rate_added_by inner join project_man_power_type_master PMPTM on PMPTM.project_man_power_master_id = PMPRM.project_man_power_type_id inner join project_manpower_agency PMA on PMA.project_manpower_agency_id = PMPRM.project_man_power_rate_vendor_id left outer join users AU on AU.user_id=PMPRM.project_man_power_rate_updated_by";

    $get_project_man_power_rate_list_squery_where = "";

    $filter_count = 0;

    // Data
    $get_project_man_power_rate_list_sdata = array();

    if ($rate_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_master_id = :rate_id";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_master_id = :rate_id";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':rate_id'] = $rate_id;

        $filter_count++;
    }

    if ($power_type_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_type_id = :power_type_id";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_type_id = :power_type_id";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':power_type_id'] = $power_type_id;

        $filter_count++;
    }

    if ($vendor_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_vendor_id = :vendor_id";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_vendor_id = :vendor_id";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':vendor_id'] = $vendor_id;

        $filter_count++;
    }

    if ($cost_per_hours != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_cost_per_hours = :cost_per_hours";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_cost_per_hours = :cost_per_hours";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':cost_per_hours'] = $cost_per_hours;

        $filter_count++;
    }

    if ($applicable_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_applicable_date = :applicable_date";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_applicable_date = :applicable_date";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':applicable_date'] = $applicable_date;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_active = :active";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_active = :active";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_added_by = :added_by";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_added_by = :added_by";
        }

        //Data
        $get_project_man_power_rate_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_added_on >= :start_date";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_added_on >= :start_date";
        }

        //Data
        $get_project_man_power_rate_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_added_on <= :end_date";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_added_on <= :end_date";
        }

        //Data
        $get_project_man_power_rate_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }
    $get_project_man_power_rate_list_order_by = " order by project_manpower_agency_name ASC";
    $get_project_man_power_rate_list_squery = $get_project_man_power_rate_list_squery_base.$get_project_man_power_rate_list_squery_where.$get_project_man_power_rate_list_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_man_power_rate_list_sstatement = $dbConnection->prepare($get_project_man_power_rate_list_squery);

        $get_project_man_power_rate_list_sstatement -> execute($get_project_man_power_rate_list_sdata);

        $get_project_man_power_rate_list_sdetails = $get_project_man_power_rate_list_sstatement -> fetchAll();

        if (false === $get_project_man_power_rate_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_man_power_rate_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_man_power_rate_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Man Power Rate Master
INPUT 	: Rate ID, Project Man Power Rate Master Update Array
OUTPUT 	: Rate ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_man_power_rate_master($rate_id, $project_man_power_rate_update_data)
{
    if (array_key_exists("power_type_id", $project_man_power_rate_update_data)) {
        $power_type_id = $project_man_power_rate_update_data["power_type_id"];
    } else {
        $power_type_id = "";
    }

    if (array_key_exists("vendor_id", $project_man_power_rate_update_data)) {
        $vendor_id = $project_man_power_rate_update_data["vendor_id"];
    } else {
        $vendor_id = "";
    }

    if (array_key_exists("cost_per_hours", $project_man_power_rate_update_data)) {
        $cost_per_hours = $project_man_power_rate_update_data["cost_per_hours"];
    } else {
        $cost_per_hours = "";
    }

    if (array_key_exists("applicable_date", $project_man_power_rate_update_data)) {
        $applicable_date = $project_man_power_rate_update_data["applicable_date"];
    } else {
        $applicable_date = "";
    }

    if (array_key_exists("active", $project_man_power_rate_update_data)) {
        $active = $project_man_power_rate_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_man_power_rate_update_data)) {
        $remarks = $project_man_power_rate_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_man_power_rate_update_data)) {
        $added_by = $project_man_power_rate_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_man_power_rate_update_data)) {
        $added_on = $project_man_power_rate_update_data["added_on"];
    } else {
        $added_on = "";
    }

    if (array_key_exists("updated_by", $project_man_power_rate_update_data)) {
        $updated_by = $project_man_power_rate_update_data["updated_by"];
    } else {
        $updated_by = "";
    }

    if (array_key_exists("updated_on", $project_man_power_rate_update_data)) {
        $updated_on = $project_man_power_rate_update_data["updated_on"];
    } else {
        $updated_on = "";
    }

    // Query
    $project_man_power_rate_update_uquery_base = "update project_man_power_rate_master set";

    $project_man_power_rate_update_uquery_set = "";

    $project_man_power_rate_update_uquery_where = " where project_man_power_rate_master_id = :rate_id";

    $project_man_power_rate_update_udata = array(":rate_id"=>$rate_id);

    $filter_count = 0;

    if ($power_type_id != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_type_id = :power_type_id,";
        $project_man_power_rate_update_udata[":power_type_id"] = $power_type_id;
        $filter_count++;
    }

    if ($vendor_id != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_vendor_id = :vendor_id,";
        $project_man_power_rate_update_udata[":vendor_id"] = $vendor_id;
        $filter_count++;
    }

    if ($cost_per_hours != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_cost_per_hours = :cost_per_hours,";
        $project_man_power_rate_update_udata[":cost_per_hours"] = $cost_per_hours;
        $filter_count++;
    }

    if ($applicable_date != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_applicable_date = :applicable_date,";
        $project_man_power_rate_update_udata[":applicable_date"] = $applicable_date;
        $filter_count++;
    }

    if ($active != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_active = :active,";
        $project_man_power_rate_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_remarks = :remarks,";
        $project_man_power_rate_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_added_by = :added_by,";
        $project_man_power_rate_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_added_on = :added_on,";
        $project_man_power_rate_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($updated_by != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_updated_by = :updated_by,";
        $project_man_power_rate_update_udata[":updated_by"] = $updated_by;
        $filter_count++;
    }

    if ($updated_on != "") {
        $project_man_power_rate_update_uquery_set = $project_man_power_rate_update_uquery_set." project_man_power_rate_updated_on = :updated_on,";
        $project_man_power_rate_update_udata[":updated_on"] = $updated_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_man_power_rate_update_uquery_set = trim($project_man_power_rate_update_uquery_set, ',');
    }

    $project_man_power_rate_update_uquery = $project_man_power_rate_update_uquery_base.$project_man_power_rate_update_uquery_set.$project_man_power_rate_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_man_power_rate_update_ustatement = $dbConnection->prepare($project_man_power_rate_update_uquery);

        $project_man_power_rate_update_ustatement -> execute($project_man_power_rate_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $rate_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Man Power Rate Master list
INPUT 	: Rate ID, Power Type ID, Vendor ID, Cost Per Hours, Applicable Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Man Power Rate Master
BY 		: Lakshmi
*/
function db_get_project_man_power_max_rate_master($project_man_power_rate_search_data)
{
    if (array_key_exists("rate_id", $project_man_power_rate_search_data)) {
        $rate_id = $project_man_power_rate_search_data["rate_id"];
    } else {
        $rate_id= "";
    }

    if (array_key_exists("power_type_id", $project_man_power_rate_search_data)) {
        $power_type_id = $project_man_power_rate_search_data["power_type_id"];
    } else {
        $power_type_id= "";
    }


    if (array_key_exists("vendor_id", $project_man_power_rate_search_data)) {
        $vendor_id= $project_man_power_rate_search_data["vendor_id"];
    } else {
        $vendor_id = "";
    }

    if (array_key_exists("cost_per_hours", $project_man_power_rate_search_data)) {
        $cost_per_hours= $project_man_power_rate_search_data["cost_per_hours"];
    } else {
        $cost_per_hours = "";
    }

    if (array_key_exists("applicable_date", $project_man_power_rate_search_data)) {
        $applicable_date= $project_man_power_rate_search_data["applicable_date"];
    } else {
        $applicable_date= "";
    }

    if (array_key_exists("active", $project_man_power_rate_search_data)) {
        $active= $project_man_power_rate_search_data["active"];
    } else {
        $active= "";
    }

    if (array_key_exists("added_by", $project_man_power_rate_search_data)) {
        $added_by= $project_man_power_rate_search_data["added_by"];
    } else {
        $added_by= "";
    }

    if (array_key_exists("start_date", $project_man_power_rate_search_data)) {
        $start_date= $project_man_power_rate_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $project_man_power_rate_search_data)) {
        $end_date= $project_man_power_rate_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_man_power_rate_list_squery_base = " select *,MAX(project_man_power_rate_cost_per_hours) as max_rate from project_man_power_rate_master PMPRM inner join users U on U.user_id= PMPRM.project_man_power_rate_added_by inner join project_man_power_type_master PMPTM on PMPTM.project_man_power_master_id = PMPRM.project_man_power_type_id inner join project_manpower_agency PMA on PMA.project_manpower_agency_id = PMPRM.project_man_power_rate_vendor_id";

    $get_project_man_power_rate_list_squery_where = "";
    $filter_count = 0;

    // Data
    $get_project_man_power_rate_list_sdata = array();

    if ($rate_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_master_id = :rate_id";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_master_id = :rate_id";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':rate_id'] = $rate_id;

        $filter_count++;
    }

    if ($power_type_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_type_id = :power_type_id";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_type_id = :power_type_id";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':power_type_id'] = $power_type_id;

        $filter_count++;
    }

    if ($vendor_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_vendor_id = :vendor_id";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_vendor_id = :vendor_id";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':vendor_id'] = $vendor_id;

        $filter_count++;
    }

    if ($cost_per_hours != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_cost_per_hours = :cost_per_hours";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_cost_per_hours = :cost_per_hours";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':cost_per_hours'] = $cost_per_hours;

        $filter_count++;
    }

    if ($applicable_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_applicable_date = :applicable_date";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_applicable_date = :applicable_date";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':applicable_date'] = $applicable_date;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_active = :active";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_active = :active";
        }

        // Data
        $get_project_man_power_rate_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_added_by = :added_by";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_added_by = :added_by";
        }

        //Data
        $get_project_man_power_rate_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_added_on >= :start_date";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_added_on >= :start_date";
        }

        //Data
        $get_project_man_power_rate_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." where project_man_power_rate_added_on <= :end_date";
        } else {
            // Query
            $get_project_man_power_rate_list_squery_where = $get_project_man_power_rate_list_squery_where." and project_man_power_rate_added_on <= :end_date";
        }

        //Data
        $get_project_man_power_rate_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_man_power_rate_list_squery = $get_project_man_power_rate_list_squery_base.$get_project_man_power_rate_list_squery_where;


    try {
        $dbConnection = get_conn_handle();

        $get_project_man_power_rate_list_sstatement = $dbConnection->prepare($get_project_man_power_rate_list_squery);

        $get_project_man_power_rate_list_sstatement -> execute($get_project_man_power_rate_list_sdata);

        $get_project_man_power_rate_list_sdetails = $get_project_man_power_rate_list_sstatement -> fetchAll();

        if (false === $get_project_man_power_rate_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_man_power_rate_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_man_power_rate_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Process User Mapping
INPUT 	: Process ID, User ID, Remarks, Added By
OUTPUT 	: Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_process_user_mapping($process_id, $user_id, $remarks, $added_by)
{
    // Query
    $project_process_user_mapping_iquery = "insert into project_process_user_mapping (project_process_id,project_process_user_id,project_process_user_status,project_process_user_active,project_process_user_remarks,project_process_user_added_by,project_process_user_added_on)values(:process_id,:user_id,:status,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_process_user_mapping_istatement = $dbConnection->prepare($project_process_user_mapping_iquery);

        // Data
        $project_process_user_mapping_idata = array(':process_id'=>$process_id,':user_id'=>$user_id,':status'=>'0',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_process_user_mapping_istatement->execute($project_process_user_mapping_idata);
        $project_process_user_mapping_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_process_user_mapping_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Process User Mapping list
INPUT 	: Mapping Id, Process ID, User ID, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Process User Mapping
BY 		: Lakshmi
*/
function db_get_project_process_user_mapping($project_process_user_mapping_search_data)
{
    if (array_key_exists("mapping_id", $project_process_user_mapping_search_data)) {
        $mapping_id = $project_process_user_mapping_search_data["mapping_id"];
    } else {
        $mapping_id= "";
    }

    if (array_key_exists("process_id", $project_process_user_mapping_search_data)) {
        $process_id = $project_process_user_mapping_search_data["process_id"];
    } else {
        $process_id = "";
    }

    if (array_key_exists("user_id", $project_process_user_mapping_search_data)) {
        $user_id = $project_process_user_mapping_search_data["user_id"];
    } else {
        $user_id = "";
    }

    if (array_key_exists("status", $project_process_user_mapping_search_data)) {
        $status = $project_process_user_mapping_search_data["status"];
    } else {
        $status = "";
    }

    if (array_key_exists("active", $project_process_user_mapping_search_data)) {
        $active = $project_process_user_mapping_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_process_user_mapping_search_data)) {
        $added_by = $project_process_user_mapping_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_process_user_mapping_search_data)) {
        $start_date= $project_process_user_mapping_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_process_user_mapping_search_data)) {
        $end_date= $project_process_user_mapping_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_process_user_mapping_list_squery_base = " select *,U.user_name as added_by,AU.user_name as user_name from project_process_user_mapping PPUM inner join users U on U.user_id = PPUM.project_process_user_added_by inner join users AU on AU.user_id = PPUM.project_process_user_id inner join project_process_master PPM on PPM.project_process_master_id = PPUM.project_process_id";

    $get_project_process_user_mapping_list_squery_where = "";
    $filter_count = 0;

    // Data
    $get_project_process_user_mapping_list_sdata = array();

    if ($mapping_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_user_mapping_id = :mapping_id";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_user_mapping_id = :mapping_id";
        }

        // Data
        $get_project_process_user_mapping_list_sdata[':mapping_id'] = $mapping_id;

        $filter_count++;
    }

    if ($process_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_id = :process_id";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_id = :process_id";
        }

        // Data
        $get_project_process_user_mapping_list_sdata[':process_id'] = $process_id;

        $filter_count++;
    }

    if ($user_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_user_id = :user_id";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_user_id = :user_id";
        }

        // Data
        $get_project_process_user_mapping_list_sdata[':user_id'] = $user_id;

        $filter_count++;
    }

    if ($status != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_user_status = :status";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_user_status = :status";
        }

        // Data
        $get_project_process_user_mapping_list_sdata[':status'] = $status;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_user_active = :active";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_user_active = :active";
        }

        // Data
        $get_project_process_user_mapping_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_user_added_by = :added_by";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_user_added_by = :added_by";
        }

        //Data
        $get_project_process_user_mapping_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_user_added_on >= :start_date";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_user_added_on >= :start_date";
        }

        //Data
        $get_project_process_user_mapping_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." where project_process_user_added_on <= :end_date";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_process_user_added_on <= :end_date";
        }

        //Data
        $get_project_process_user_mapping_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_process_user_mapping_list_squery_order = ' order by PPM.project_process_master_name asc';
    $get_project_process_user_mapping_list_squery = $get_project_process_user_mapping_list_squery_base.$get_project_process_user_mapping_list_squery_where.$get_project_process_user_mapping_list_squery_order;

    try {
        $dbConnection = get_conn_handle();

        $get_project_process_user_mapping_list_sstatement = $dbConnection->prepare($get_project_process_user_mapping_list_squery);

        $get_project_process_user_mapping_list_sstatement -> execute($get_project_process_user_mapping_list_sdata);

        $get_project_process_user_mapping_list_sdetails = $get_project_process_user_mapping_list_sstatement -> fetchAll();

        if (false === $get_project_process_user_mapping_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_process_user_mapping_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_process_user_mapping_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Process User Mapping
INPUT 	: Mapping ID, Project Process User Mapping Update Array,Process ID
OUTPUT 	: Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_process_user_mapping($mapping_id, $project_process_user_mapping_update_data, $dprocess_id='')
{
    if (array_key_exists("process_id", $project_process_user_mapping_update_data)) {
        $process_id = $project_process_user_mapping_update_data["process_id"];
    } else {
        $process_id = "";
    }

    if (array_key_exists("user_id", $project_process_user_mapping_update_data)) {
        $user_id = $project_process_user_mapping_update_data["user_id"];
    } else {
        $user_id = "";
    }

    if (array_key_exists("status", $project_process_user_mapping_update_data)) {
        $status = $project_process_user_mapping_update_data["status"];
    } else {
        $status = "";
    }

    if (array_key_exists("active", $project_process_user_mapping_update_data)) {
        $active = $project_process_user_mapping_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_process_user_mapping_update_data)) {
        $remarks = $project_process_user_mapping_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_process_user_mapping_update_data)) {
        $added_by = $project_process_user_mapping_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_process_user_mapping_update_data)) {
        $added_on = $project_process_user_mapping_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_process_user_mapping_update_uquery_base = "update project_process_user_mapping set";

    $project_process_user_mapping_update_uquery_set = "";

    if ($dprocess_id != '') {
        $project_process_user_mapping_update_uquery_where = " where project_process_id = :dprocess_id";
        $project_process_user_mapping_update_udata = array(":dprocess_id"=>$dprocess_id);
    } else {
        $project_process_user_mapping_update_uquery_where = " where project_process_user_mapping_id = :mapping_id";
        $project_process_user_mapping_update_udata = array(":mapping_id"=>$mapping_id);
    }

    $filter_count = 0;

    if ($process_id != "") {
        $project_process_user_mapping_update_uquery_set = $project_process_user_mapping_update_uquery_set." project_process_id = :process_id,";
        $project_process_user_mapping_update_udata[":process_id"] = $process_id;
        $filter_count++;
    }

    if ($user_id != "") {
        $project_process_user_mapping_update_uquery_set = $project_process_user_mapping_update_uquery_set." project_process_user_id = :user_id,";
        $project_process_user_mapping_update_udata[":user_id"] = $user_id;
        $filter_count++;
    }

    if ($status != "") {
        $project_process_user_mapping_update_uquery_set = $project_process_user_mapping_update_uquery_set." project_process_user_status = :status,";
        $project_process_user_mapping_update_udata[":status"] = $status;
        $filter_count++;
    }

    if ($active != "") {
        $project_process_user_mapping_update_uquery_set = $project_process_user_mapping_update_uquery_set." project_process_user_active = :active,";
        $project_process_user_mapping_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_process_user_mapping_update_uquery_set = $project_process_user_mapping_update_uquery_set." project_process_user_remarks = :remarks,";
        $project_process_user_mapping_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_process_user_mapping_update_uquery_set = $project_process_user_mapping_update_uquery_set." project_process_user_added_by = :added_by,";
        $project_process_user_mapping_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_process_user_mapping_update_uquery_set = $project_process_user_mapping_update_uquery_set." project_process_user_added_on = :added_on,";
        $project_process_user_mapping_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_process_user_mapping_update_uquery_set = trim($project_process_user_mapping_update_uquery_set, ',');
    }

    $project_process_user_mapping_update_uquery = $project_process_user_mapping_update_uquery_base.$project_process_user_mapping_update_uquery_set.$project_process_user_mapping_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_process_user_mapping_update_ustatement = $dbConnection->prepare($project_process_user_mapping_update_uquery);

        $project_process_user_mapping_update_ustatement -> execute($project_process_user_mapping_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $mapping_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Task User Mapping
INPUT 	: Task ID, User ID, Remarks, Added By
OUTPUT 	: Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_user_mapping($task_id, $user_id, $remarks, $added_by)
{
    // Query
    $project_task_user_mapping_iquery = "insert into project_task_user_mapping (project_task_id,project_task_user_id,project_task_user_status,project_task_user_active,project_task_user_remarks,project_task_user_added_by,project_task_user_added_on)values(:task_id,:user_id,:status,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_task_user_mapping_istatement = $dbConnection->prepare($project_task_user_mapping_iquery);

        // Data
        $project_task_user_mapping_idata = array(':task_id'=>$task_id,':user_id'=>$user_id,':status'=>'0',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_task_user_mapping_istatement->execute($project_task_user_mapping_idata);
        $project_task_user_mapping_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_task_user_mapping_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project task User Mapping list
INPUT 	: Mapping Id, Task ID, User ID, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project task User Mapping
BY 		: Lakshmi
*/
function db_get_project_task_user_mapping($project_task_user_mapping_search_data)
{
    if (array_key_exists("mapping_id", $project_task_user_mapping_search_data)) {
        $mapping_id = $project_task_user_mapping_search_data["mapping_id"];
    } else {
        $mapping_id= "";
    }

    if (array_key_exists("task_id", $project_task_user_mapping_search_data)) {
        $task_id = $project_task_user_mapping_search_data["task_id"];
    } else {
        $task_id = "";
    }

    if (array_key_exists("process_id", $project_task_user_mapping_search_data)) {
        $process_id = $project_task_user_mapping_search_data["process_id"];
    } else {
        $process_id = "";
    }

    if (array_key_exists("user_id", $project_task_user_mapping_search_data)) {
        $user_id = $project_task_user_mapping_search_data["user_id"];
    } else {
        $user_id = "";
    }

    if (array_key_exists("status", $project_task_user_mapping_search_data)) {
        $status = $project_task_user_mapping_search_data["status"];
    } else {
        $status = "";
    }

    if (array_key_exists("active", $project_task_user_mapping_search_data)) {
        $active = $project_task_user_mapping_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_task_user_mapping_search_data)) {
        $added_by = $project_task_user_mapping_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_task_user_mapping_search_data)) {
        $start_date= $project_task_user_mapping_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_task_user_mapping_search_data)) {
        $end_date= $project_task_user_mapping_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_task_user_mapping_list_squery_base = "select *,U.user_name as added_by,AU.user_name as task_user from project_task_user_mapping PTUM inner join users U on U.user_id = PTUM.project_task_user_added_by inner join users AU on AU.user_id = PTUM.project_task_user_id inner join project_task_master PTM on PTM.project_task_master_id = PTUM.project_task_id inner join project_process_master PPM on PPM.project_process_master_id = PTM.project_task_master_process";

    $get_project_task_user_mapping_list_squery_where = "";

    $filter_count = 0;

    // Data
    $get_project_task_user_mapping_list_sdata = array();

    if ($mapping_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where project_task_user_mapping_id = :mapping_id";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and project_task_user_mapping_id = :mapping_id";
        }

        // Data
        $get_project_task_user_mapping_list_sdata[':mapping_id'] = $mapping_id;

        $filter_count++;
    }

    if ($task_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where PTUM.project_task_id = :task_id";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and PTUM.project_task_id = :task_id";
        }

        // Data
        $get_project_task_user_mapping_list_sdata[':task_id'] = $task_id;

        $filter_count++;
    }

    if ($process_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where PPM.project_process_master_id = :process_id";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and PPM.project_process_master_id = :process_id";
        }

        // Data
        $get_project_task_user_mapping_list_sdata[':process_id'] = $process_id;

        $filter_count++;
    }

    if ($user_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where project_task_user_id = :user_id";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and project_task_user_id = :user_id";
        }

        // Data
        $get_project_task_user_mapping_list_sdata[':user_id'] = $user_id;

        $filter_count++;
    }

    if ($status != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where project_task_user_status = :status";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and project_task_user_status = :status";
        }

        // Data
        $get_project_task_user_mapping_list_sdata[':status'] = $status;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where project_task_user_active = :active";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and project_task_user_active = :active";
        }

        // Data
        $get_project_task_user_mapping_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where project_task_user_added_by = :added_by";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and project_task_user_added_by = :added_by";
        }

        //Data
        $get_project_task_user_mapping_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where project_task_user_added_on >= :start_date";
        } else {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." and project_task_user_added_on >= :start_date";
        }

        //Data
        $get_project_task_user_mapping_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_task_user_mapping_list_squery_where = $get_project_task_user_mapping_list_squery_where." where project_task_user_added_on <= :end_date";
        } else {
            // Query
            $get_project_process_user_mapping_list_squery_where = $get_project_process_user_mapping_list_squery_where." and project_task_user_added_on <= :end_date";
        }

        //Data
        $get_project_task_user_mapping_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_task_user_mapping_list_squery_order = ' order by PTM.projec_task_master_name asc';
    $get_project_task_user_mapping_list_squery = $get_project_task_user_mapping_list_squery_base.$get_project_task_user_mapping_list_squery_where.$get_project_task_user_mapping_list_squery_order;

    try {
        $dbConnection = get_conn_handle();

        $get_project_task_user_mapping_list_sstatement = $dbConnection->prepare($get_project_task_user_mapping_list_squery);

        $get_project_task_user_mapping_list_sstatement -> execute($get_project_task_user_mapping_list_sdata);

        $get_project_task_user_mapping_list_sdetails = $get_project_task_user_mapping_list_sstatement -> fetchAll();

        if (false === $get_project_task_user_mapping_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_task_user_mapping_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_task_user_mapping_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Task User Mapping
INPUT 	: Mapping ID, Project Task User Mapping Update Array
OUTPUT 	: Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_user_mapping($mapping_id, $project_task_user_mapping_update_data, $dtask_id='')
{
    if (array_key_exists("task_id", $project_task_user_mapping_update_data)) {
        $task_id = $project_task_user_mapping_update_data["task_id"];
    } else {
        $task_id = "";
    }

    if (array_key_exists("user_id", $project_task_user_mapping_update_data)) {
        $user_id = $project_task_user_mapping_update_data["user_id"];
    } else {
        $user_id = "";
    }

    if (array_key_exists("status", $project_task_user_mapping_update_data)) {
        $status = $project_task_user_mapping_update_data["status"];
    } else {
        $status = "";
    }

    if (array_key_exists("active", $project_task_user_mapping_update_data)) {
        $active = $project_task_user_mapping_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_task_user_mapping_update_data)) {
        $remarks = $project_task_user_mapping_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_task_user_mapping_update_data)) {
        $added_by = $project_task_user_mapping_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_task_user_mapping_update_data)) {
        $added_on = $project_task_user_mapping_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_task_user_mapping_update_uquery_base = "update project_task_user_mapping set";

    $project_task_user_mapping_update_uquery_set = "";

    if ($dtask_id != '') {
        $project_task_user_mapping_update_uquery_where = " where project_task_id = :dtask_id";
        $project_task_user_mapping_update_udata = array(":dtask_id"=>$dtask_id);
    } else {
        $project_task_user_mapping_update_uquery_where = " where project_task_user_mapping_id = :mapping_id";
        $project_task_user_mapping_update_udata = array(":mapping_id"=>$mapping_id);
    }

    $filter_count = 0;

    if ($task_id != "") {
        $project_task_user_mapping_update_uquery_set = $project_task_user_mapping_update_uquery_set." project_task_id = :task_id,";
        $project_task_user_mapping_update_udata[":task_id"] = $task_id;
        $filter_count++;
    }

    if ($user_id != "") {
        $project_task_user_mapping_update_uquery_set = $project_task_user_mapping_update_uquery_set." project_task_user_id = :user_id,";
        $project_task_user_mapping_update_udata[":user_id"] = $user_id;
        $filter_count++;
    }

    if ($status != "") {
        $project_task_user_mapping_update_uquery_set = $project_task_user_mapping_update_uquery_set." project_task_user_status = :status,";
        $project_task_user_mapping_update_udata[":status"] = $status;
        $filter_count++;
    }

    if ($active != "") {
        $project_task_user_mapping_update_uquery_set = $project_task_user_mapping_update_uquery_set." project_task_user_active = :active,";
        $project_task_user_mapping_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_task_user_mapping_update_uquery_set = $project_task_user_mapping_update_uquery_set." project_task_user_remarks = :remarks,";
        $project_task_user_mapping_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_task_user_mapping_update_uquery_set = $project_task_user_mapping_update_uquery_set." project_task_user_added_by = :added_by,";
        $project_task_user_mapping_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_task_user_mapping_update_uquery_set = $project_task_user_mapping_update_uquery_set." project_task_user_added_on = :added_on,";
        $project_task_user_mapping_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_task_user_mapping_update_uquery_set = trim($project_task_user_mapping_update_uquery_set, ',');
    }

    $project_task_user_mapping_update_uquery = $project_task_user_mapping_update_uquery_base.$project_task_user_mapping_update_uquery_set.$project_task_user_mapping_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_task_user_mapping_update_ustatement = $dbConnection->prepare($project_task_user_mapping_update_uquery);

        $project_task_user_mapping_update_ustatement -> execute($project_task_user_mapping_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $mapping_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Machine Rate Master
INPUT 	: Machine ID, Machine Rate, Kns Fuel, Vendor Fuel, Kns Bata, Vendor Bata, Remarks, Added By
OUTPUT 	: Machine Rate ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_machine_rate_master($machine_id, $machine_rate, $kns_fuel, $vendor_fuel, $kns_bata, $vendor_bata, $remarks, $added_by)
{
    // Query
    $project_machine_rate_master_iquery = "insert into project_machine_rate_master (project_machine_rate_machine_id,project_machine_rate,project_machine_kns_fuel,project_machine_vendor_fuel,project_machine_kns_bata,project_machine_vendor_bata,
	project_machine_rate_master_active,project_machine_rate_master_remarks,project_machine_rate_master_added_by,project_machine_rate_master_added_on)
	values (:machine_id,:machine_rate,:kns_fuel,:vendor_fuel,:kns_bata,:vendor_bata,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_machine_rate_master_istatement = $dbConnection->prepare($project_machine_rate_master_iquery);

        // Data
        $project_machine_rate_master_idata = array(':machine_id'=>$machine_id,':machine_rate'=>$machine_rate,':kns_fuel'=>$kns_fuel,':vendor_fuel'=>$vendor_fuel,
        ':kns_bata'=>$kns_bata,':vendor_bata'=>$vendor_bata,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_machine_rate_master_istatement->execute($project_machine_rate_master_idata);
        $project_machine_rate_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_rate_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get project Machine Rate Master list
INPUT 	: Machine Rate ID,  Machine ID, Machine Rate, Kns Fuel, Vendor Fuel, Kns Bata, Vendor Bata, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Machine Rate Master
BY 		: Lakshmi
*/
function db_get_project_machine_rate_master($project_machine_rate_master_search_data)
{
    if (array_key_exists("machine_rate_id", $project_machine_rate_master_search_data)) {
        $machine_rate_id = $project_machine_rate_master_search_data["machine_rate_id"];
    } else {
        $machine_rate_id= "";
    }

    if (array_key_exists("machine_id", $project_machine_rate_master_search_data)) {
        $machine_id = $project_machine_rate_master_search_data["machine_id"];
    } else {
        $machine_id = "";
    }

    if (array_key_exists("machine_type", $project_machine_rate_master_search_data)) {
        $machine_type = $project_machine_rate_master_search_data["machine_type"];
    } else {
        $machine_type = "";
    }

    if (array_key_exists("vendor", $project_machine_rate_master_search_data)) {
        $vendor = $project_machine_rate_master_search_data["vendor"];
    } else {
        $vendor = "";
    }

    if (array_key_exists("machine_rate", $project_machine_rate_master_search_data)) {
        $machine_rate = $project_machine_rate_master_search_data["machine_rate"];
    } else {
        $machine_rate = "";
    }

    if (array_key_exists("kns_fuel", $project_machine_rate_master_search_data)) {
        $kns_fuel = $project_machine_rate_master_search_data["kns_fuel"];
    } else {
        $kns_fuel = "";
    }

    if (array_key_exists("vendor_fuel", $project_machine_rate_master_search_data)) {
        $vendor_fuel = $project_machine_rate_master_search_data["vendor_fuel"];
    } else {
        $vendor_fuel = "";
    }

    if (array_key_exists("kns_bata", $project_machine_rate_master_search_data)) {
        $kns_bata = $project_machine_rate_master_search_data["kns_bata"];
    } else {
        $kns_bata = "";
    }

    if (array_key_exists("vendor_bata", $project_machine_rate_master_search_data)) {
        $vendor_bata = $project_machine_rate_master_search_data["vendor_bata"];
    } else {
        $vendor_bata = "";
    }

    if (array_key_exists("active", $project_machine_rate_master_search_data)) {
        $active = $project_machine_rate_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_machine_rate_master_search_data)) {
        $added_by = $project_machine_rate_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_machine_rate_master_search_data)) {
        $start_date= $project_machine_rate_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_machine_rate_master_search_data)) {
        $end_date= $project_machine_rate_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    if (array_key_exists("order", $project_machine_rate_master_search_data)) {
        $order = $project_machine_rate_master_search_data["order"];
    } else {
        $order = "";
    }

    $get_project_machine_rate_master_list_squery_base = "select * from project_machine_rate_master PMRM inner join users U on U.user_id= PMRM.project_machine_rate_master_added_by inner join project_machine_master PMM on PMM.project_machine_master_id = PMRM.project_machine_rate_machine_id inner join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id=PMM.project_machine_master_vendor inner join project_machine_type_master PMTM on PMTM.project_machine_type_master_id = PMM.project_machine_master_machine_type";

    $get_project_machine_rate_master_list_squery_where = "";

    $get_project_machine_rate_master_list_squery_order = '';

    $filter_count = 0;

    // Data
    $get_project_machine_rate_master_list_sdata = array();

    if ($machine_rate_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_rate_master_id = :machine_rate_id";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_rate_master_id = :machine_rate_id";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':machine_rate_id'] = $machine_rate_id;

        $filter_count++;
    }

    if ($machine_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_rate_machine_id = :machine_id";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_rate_machine_id = :machine_id";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':machine_id'] = $machine_id;

        $filter_count++;
    }

    if ($machine_type != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where PMM.project_machine_master_machine_type = :machine_type";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and PMM.project_machine_master_machine_type = :machine_type";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':machine_type'] = $machine_type;

        $filter_count++;
    }

    if ($vendor != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where PMM.project_machine_master_vendor = :vendor";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and PMM.project_machine_master_vendor = :vendor";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':vendor'] = $vendor;

        $filter_count++;
    }

    if ($machine_rate != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_rate = :machine_rate";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_rate = :machine_rate";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':machine_rate'] = $machine_rate;

        $filter_count++;
    }

    if ($kns_fuel != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_kns_fuel = :kns_fuel";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_kns_fuel = :kns_fuel";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':kns_fuel'] = $kns_fuel;

        $filter_count++;
    }

    if ($vendor_fuel != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_vendor_fuel = :vendor_fuel";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_vendor_fuel = :vendor_fuel";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':vendor_fuel'] = $vendor_fuel;

        $filter_count++;
    }

    if ($kns_bata != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_kns_fuel = :kns_bata";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_kns_fuel = :kns_bata";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':kns_bata'] = $kns_bata;

        $filter_count++;
    }

    if ($vendor_bata != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_vendor_bata = :vendor_bata";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_vendor_bata = :vendor_bata";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':vendor_bata'] = $vendor_bata;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_rate_master_active = :active";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_rate_master_active = :active";
        }

        // Data
        $get_project_machine_rate_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_rate_master_added_by = :added_by";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_rate_master_added_by = :added_by";
        }

        //Data
        $get_project_machine_rate_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_rate_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_rate_master_added_on >= :start_date";
        }

        //Data
        $get_project_machine_rate_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." where project_machine_rate_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_machine_rate_master_list_squery_where = $get_project_machine_rate_master_list_squery_where." and project_machine_rate_master_added_on <= :end_date";
        }

        //Data
        $get_project_machine_rate_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    if ($order != '') {
        $get_project_machine_rate_master_list_squery_order = ' order by project_machine_rate desc';
    }

    $get_project_machine_rate_master_list_squery = $get_project_machine_rate_master_list_squery_base.$get_project_machine_rate_master_list_squery_where.$get_project_machine_rate_master_list_squery_order;

    try {
        $dbConnection = get_conn_handle();

        $get_project_machine_rate_master_list_sstatement = $dbConnection->prepare($get_project_machine_rate_master_list_squery);

        $get_project_machine_rate_master_list_sstatement -> execute($get_project_machine_rate_master_list_sdata);

        $get_project_machine_rate_master_list_sdetails = $get_project_machine_rate_master_list_sstatement -> fetchAll();

        if (false === $get_project_machine_rate_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_machine_rate_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_machine_rate_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Rate Master
INPUT 	: Machine Rate ID, Project Machine Rate Master Update Array
OUTPUT 	: Machine Rate ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_machine_rate_master($machine_rate_id, $project_machine_rate_master_update_data)
{
    if (array_key_exists("machine_id", $project_machine_rate_master_update_data)) {
        $machine_id = $project_machine_rate_master_update_data["machine_id"];
    } else {
        $machine_id = "";
    }

    if (array_key_exists("machine_rate", $project_machine_rate_master_update_data)) {
        $machine_rate = $project_machine_rate_master_update_data["machine_rate"];
    } else {
        $machine_rate = "";
    }

    if (array_key_exists("kns_fuel", $project_machine_rate_master_update_data)) {
        $kns_fuel = $project_machine_rate_master_update_data["kns_fuel"];
    } else {
        $kns_fuel = "";
    }

    if (array_key_exists("vendor_fuel", $project_machine_rate_master_update_data)) {
        $vendor_fuel = $project_machine_rate_master_update_data["vendor_fuel"];
    } else {
        $vendor_fuel = "";
    }

    if (array_key_exists("kns_bata", $project_machine_rate_master_update_data)) {
        $kns_bata = $project_machine_rate_master_update_data["kns_bata"];
    } else {
        $kns_bata = "";
    }

    if (array_key_exists("vendor_bata", $project_machine_rate_master_update_data)) {
        $vendor_bata = $project_machine_rate_master_update_data["vendor_bata"];
    } else {
        $vendor_bata = "";
    }

    if (array_key_exists("active", $project_machine_rate_master_update_data)) {
        $active = $project_machine_rate_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_machine_rate_master_update_data)) {
        $remarks = $project_machine_rate_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_machine_rate_master_update_data)) {
        $added_by = $project_machine_rate_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_machine_rate_master_update_data)) {
        $added_on = $project_machine_rate_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_machine_rate_master_update_uquery_base = "update project_machine_rate_master set";

    $project_machine_rate_master_update_uquery_set = "";

    $project_machine_rate_master_update_uquery_where = " where project_machine_rate_master_id = :machine_rate_id";

    $project_machine_rate_master_update_udata = array(":machine_rate_id"=>$machine_rate_id);

    $filter_count = 0;

    if ($machine_id != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_rate_machine_id = :machine_id,";
        $project_machine_rate_master_update_udata[":machine_id"] = $machine_id;
        $filter_count++;
    }

    if ($machine_rate != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_rate = :machine_rate,";
        $project_machine_rate_master_update_udata[":machine_rate"] = $machine_rate;
        $filter_count++;
    }

    if ($kns_fuel != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_kns_fuel = :kns_fuel,";
        $project_machine_rate_master_update_udata[":kns_fuel"] = $kns_fuel;
        $filter_count++;
    }

    if ($vendor_fuel != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_vendor_fuel = :vendor_fuel,";
        $project_machine_rate_master_update_udata[":vendor_fuel"] = $vendor_fuel;
        $filter_count++;
    }

    if ($kns_bata != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_kns_bata = :kns_bata,";
        $project_machine_rate_master_update_udata[":kns_bata"] = $kns_bata;
        $filter_count++;
    }

    if ($vendor_bata != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_vendor_bata = :vendor_bata,";
        $project_machine_rate_master_update_udata[":vendor_bata"] = $vendor_bata;
        $filter_count++;
    }

    if ($active != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_rate_master_active = :active,";
        $project_machine_rate_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_rate_master_remarks = :remarks,";
        $project_machine_rate_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_rate_master_added_by = :added_by,";
        $project_machine_rate_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_machine_rate_master_update_uquery_set = $project_machine_rate_master_update_uquery_set." project_machine_rate_master_added_on = :added_on,";
        $project_machine_rate_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_machine_rate_master_update_uquery_set = trim($project_machine_rate_master_update_uquery_set, ',');
    }

    $project_machine_rate_master_update_uquery = $project_machine_rate_master_update_uquery_base.$project_machine_rate_master_update_uquery_set.$project_machine_rate_master_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();

        $project_machine_rate_master_update_ustatement = $dbConnection->prepare($project_machine_rate_master_update_uquery);

        $project_machine_rate_master_update_ustatement -> execute($project_machine_rate_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $machine_rate_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Delay Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Reason ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_delay_reason($task_id,$road_id,$name,$start_date,$end_date,$remarks,$added_by)
{
	// Query
   $delay_reason_iquery = "insert into project_task_delay_reason (project_task_delay_reason_task_id,project_task_delay_reason_road_id,project_task_delay_reason_name,project_task_delay_reason_start_date,project_task_delay_reason_end_date,project_task_delay_reason_active,project_task_delay_reason_remarks,project_task_delay_reason_added_by,
   project_task_delay_reason_added_on) values(:task_id,:road_id,:name,:start_date,:end_date,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $delay_reason_istatement = $dbConnection->prepare($delay_reason_iquery);

        // Data
        $delay_reason_idata = array(':task_id'=>$task_id,':road_id'=>$road_id,':name'=>$name,':start_date'=>$start_date,':end_date'=>$end_date,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $delay_reason_istatement->execute($delay_reason_idata);
		$delay_reason_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $delay_reason_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Delay Reason Master List
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process Master
BY 		: Lakshmi
*/
function db_get_project_delay_reason($delay_reason_search_data)
{
    if (array_key_exists("reason_id", $delay_reason_search_data)) {
        $reason_id = $delay_reason_search_data["reason_id"];
    } else {
        $reason_id= "";
    }

    if (array_key_exists("task_id", $delay_reason_search_data)) {
        $task_id = $delay_reason_search_data["task_id"];
    } else {
        $task_id= "";
    }

    if (array_key_exists("process_id", $delay_reason_search_data)) {
        $process_id = $delay_reason_search_data["process_id"];
    } else {
        $process_id= "";
    }

    if (array_key_exists("name", $delay_reason_search_data)) {
        $name = $delay_reason_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $delay_reason_search_data)) {
        $active = $delay_reason_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("status", $delay_reason_search_data)) {
        $status = $delay_reason_search_data["status"];
    } else {
        $status = "";
    }

    if (array_key_exists("project", $delay_reason_search_data)) {
        $project= $delay_reason_search_data["project"];
    } else {
        $project= "";
    }

    if (array_key_exists("added_by", $delay_reason_search_data)) {
        $added_by = $delay_reason_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $delay_reason_search_data)) {
        $start_date= $delay_reason_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $delay_reason_search_data)) {
        $end_date= $delay_reason_search_data["end_date"];
    } else {
        $end_date= "";
    }

    if (array_key_exists("delay_end_date", $delay_reason_search_data)) {
        $delay_end_date= $delay_reason_search_data["delay_end_date"];
    } else {
        $delay_end_date= "";
    }

    if (array_key_exists("remarks", $delay_reason_search_data)) {
        $remarks= $delay_reason_search_data["remarks"];
    } else {
        $remarks= "";
    }

    if (array_key_exists("is_road", $delay_reason_search_data)) {
        $is_road= $delay_reason_search_data["is_road"];
    } else {
        $is_road= "";
    }

    $get_delay_reason_list_squery_base = "select * from project_task_delay_reason PTDR inner join project_reason_master DRM on DRM.project_reason_master_id=PTDR.project_task_delay_reason_name inner join project_plan_process_task PPPT on PPPT.project_process_task_id=PTDR.project_task_delay_reason_task_id inner join project_task_master PTM on PTM.project_task_master_id= PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join users U on U.user_id=PTDR.project_task_delay_reason_added_by left outer join project_site_location_mapping_master PSLM on PSLM.project_site_location_mapping_master_id=PTDR.project_task_delay_reason_road_id";

    $get_delay_reason_list_squery_where = "";
    $filter_count = 0;

    // Data
    $get_delay_reason_list_sdata = array();

    if ($reason_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_id = :reason_id";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_id = :reason_id";
        }

        // Data
        $get_delay_reason_list_sdata[':reason_id'] = $reason_id;

        $filter_count++;
    }

    if ($task_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_task_id = :task_id";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_task_id = :task_id";
        }

        // Data
        $get_delay_reason_list_sdata[':task_id'] = $task_id;

        $filter_count++;
    }

    if ($process_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where PPP.project_plan_process_id = :process_id";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and PPP.project_plan_process_id = :process_id";
        }

        // Data
        $get_delay_reason_list_sdata[':process_id'] = $process_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_name = :name";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_name = :name";
        }

        // Data
        $get_delay_reason_list_sdata[':name'] = $name;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_active = :active";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_active = :active";
        }

        // Data
        $get_delay_reason_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($status != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_end_date != :end_date";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_end_date != :end_date";
        }

        // Data
        $get_delay_reason_list_sdata[':end_date']  = "0000-00-00 00:00:00";

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_added_by = :added_by";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_added_by = :added_by";
        }

        //Data
        $get_delay_reason_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($project != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where PP.project_plan_project_id = :project";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and PP.project_plan_project_id = :project";
        }

        //Data
        $get_delay_reason_list_sdata['project']  = $project;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_added_on >= :start_date";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_added_on >= :start_date";
        }

        //Data
        $get_delay_reason_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_added_on <= :end_date";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_added_on <= :end_date";
        }

        //Data
        $get_delay_reason_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    if ($delay_end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_end_date = :delay_end_date";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_end_date = :delay_end_date";
        }

        //Data
        $get_delay_reason_list_sdata['delay_end_date']  = $delay_end_date;

        $filter_count++;
    }

    if ($remarks != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." where project_task_delay_reason_end_date = :remarks";
        } else {
            // Query
            $get_delay_reason_list_squery_where = $get_delay_reason_list_squery_where." and project_task_delay_reason_end_date = :remarks";
        }

        //Data
        $get_delay_reason_list_sdata['remarks']  = $remarks;

        $filter_count++;
    }

    $get_delay_reason_list_order_by = " order by project_task_delay_reason_added_on DESC";
    $get_delay_reason_list_squery = $get_delay_reason_list_squery_base.$get_delay_reason_list_squery_where.$get_delay_reason_list_order_by;


    try {
        $dbConnection = get_conn_handle();

        $get_delay_reason_list_sstatement = $dbConnection->prepare($get_delay_reason_list_squery);

        $get_delay_reason_list_sstatement -> execute($get_delay_reason_list_sdata);

        $get_delay_reason_list_sdetails = $get_delay_reason_list_sstatement -> fetchAll();

        if (false === $get_delay_reason_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_delay_reason_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_delay_reason_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

 /*
PURPOSE : To update Delay Reason Master
INPUT 	: Reason ID, elay Reason Master Update Array
OUTPUT 	: Reason ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_delay_reason($reason_id, $delay_reason_update_data)
{
    if (array_key_exists("name", $delay_reason_update_data)) {
        $name = $delay_reason_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("road_id", $delay_reason_update_data)) {
        $road_id = $delay_reason_update_data["road_id"];
    } else {
        $road_id = "";
    }
    if (array_key_exists("active", $delay_reason_update_data)) {
        $active = $delay_reason_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("status", $delay_reason_update_data)) {
        $status = $delay_reason_update_data["status"];
    } else {
        $status = "";
    }

    if (array_key_exists("remarks", $delay_reason_update_data)) {
        $remarks = $delay_reason_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $delay_reason_update_data)) {
        $added_by = $delay_reason_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $delay_reason_update_data)) {
        $added_on = $delay_reason_update_data["added_on"];
    } else {
        $added_on = "";
    }

    if (array_key_exists("end_date", $delay_reason_update_data)) {
        $end_date = $delay_reason_update_data["end_date"];
    } else {
        $end_date = "";
    }

    // Query
    $delay_reason_update_uquery_base = "update project_task_delay_reason set ";

    $delay_reason_update_uquery_set = "";

    $delay_reason_update_uquery_where = " where project_task_delay_reason_id = :reason_id";

    $delay_reason_update_udata = array(":reason_id"=>$reason_id);

    $filter_count = 0;

    if ($name != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_name = :name,";
        $delay_reason_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($road_id != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_road_id = :road_id,";
        $delay_reason_update_udata[":road_id"] = $road_id;
        $filter_count++;
    }

    if ($active != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_active = :active,";
        $delay_reason_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($status != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_status = :status,";
        $delay_reason_update_udata[":status"] = $status;
        $filter_count++;
    }

    if ($remarks != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_remarks = :remarks,";
        $delay_reason_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_added_by = :added_by,";
        $delay_reason_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_added_on = :added_on,";
        $delay_reason_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($end_date != "") {
        $delay_reason_update_uquery_set = $delay_reason_update_uquery_set." project_task_delay_reason_end_date = :end_date,";
        $delay_reason_update_udata[":end_date"] = $end_date;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $delay_reason_update_uquery_set = trim($delay_reason_update_uquery_set, ',');
    }

    $delay_reason_update_uquery = $delay_reason_update_uquery_base.$delay_reason_update_uquery_set.$delay_reason_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $delay_reason_update_ustatement = $dbConnection->prepare($delay_reason_update_uquery);

        $delay_reason_update_ustatement -> execute($delay_reason_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $reason_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To add new Delay Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Reason ID, success or failure message
BY 		: Lakshmi
*/
function db_add_delay_reason_master($name, $remarks, $added_by)
{
    // Query
    $delay_reason_master_iquery = "insert into delay_reason_master (delay_reason_master_name,delay_reason_master_active,delay_reason_master_remarks,delay_reason_master_added_by,delay_reason_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $delay_reason_master_istatement = $dbConnection->prepare($delay_reason_master_iquery);

        // Data
        $delay_reason_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $delay_reason_master_istatement->execute($delay_reason_master_idata);
        $delay_reason_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $delay_reason_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Delay Reason Master List
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process Master
BY 		: Lakshmi
*/
function db_get_delay_reason_master($delay_reason_master_search_data)
{
    if (array_key_exists("reason_id", $delay_reason_master_search_data)) {
        $reason_id = $delay_reason_master_search_data["reason_id"];
    } else {
        $reason_id= "";
    }

    if (array_key_exists("name", $delay_reason_master_search_data)) {
        $name = $delay_reason_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $delay_reason_master_search_data)) {
        $active = $delay_reason_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $delay_reason_master_search_data)) {
        $added_by = $delay_reason_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $delay_reason_master_search_data)) {
        $start_date= $delay_reason_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $delay_reason_master_search_data)) {
        $end_date= $delay_reason_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_delay_reason_master_list_squery_base = "select * from delay_reason_master";

    $get_delay_reason_master_list_squery_where = "";
    $get_delay_reason_master_list_squery_order_by = " order by delay_reason_master_name ASC";
    $filter_count = 0;

    // Data
    $get_delay_reason_master_list_sdata = array();

    if ($reason_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." where delay_reason_master_id = :reason_id";
        } else {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." and delay_reason_master_id = :reason_id";
        }

        // Data
        $get_delay_reason_master_list_sdata[':reason_id'] = $reason_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." where delay_reason_master_name = :name";
        } else {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." and delay_reason_master_name = :name";
        }

        // Data
        $get_delay_reason_master_list_sdata[':name'] = $name;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." where delay_reason_master_active = :active";
        } else {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." and delay_reason_master_active = :active";
        }

        // Data
        $get_delay_reason_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." where delay_reason_master_added_by = :added_by";
        } else {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." and delay_reason_master_added_by = :added_by";
        }

        //Data
        $get_delay_reason_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." where delay_reason_master_added_on >= :start_date";
        } else {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." and delay_reason_master_added_on >= :start_date";
        }

        //Data
        $get_delay_reason_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." where delay_reason_master_added_on <= :end_date";
        } else {
            // Query
            $get_delay_reason_master_list_squery_where = $get_delay_reason_master_list_squery_where." and delay_reason_master_added_on <= :end_date";
        }

        //Data
        $get_delay_reason_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_delay_reason_master_list_squery = $get_delay_reason_master_list_squery_base.$get_delay_reason_master_list_squery_where.$get_delay_reason_master_list_squery_order_by;
    try {
        $dbConnection = get_conn_handle();

        $get_delay_reason_master_list_sstatement = $dbConnection->prepare($get_delay_reason_master_list_squery);

        $get_delay_reason_master_list_sstatement -> execute($get_delay_reason_master_list_sdata);

        $get_delay_reason_master_list_sdetails = $get_delay_reason_master_list_sstatement -> fetchAll();

        if (false === $get_delay_reason_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_delay_reason_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_delay_reason_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

 /*
PURPOSE : To update Delay Reason Master
INPUT 	: Reason ID, elay Reason Master Update Array
OUTPUT 	: Reason ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_delay_reason_master($reason_id, $delay_reason_master_update_data)
{
    if (array_key_exists("name", $delay_reason_master_update_data)) {
        $name = $delay_reason_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $delay_reason_master_update_data)) {
        $active = $delay_reason_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $delay_reason_master_update_data)) {
        $remarks = $delay_reason_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $delay_reason_master_update_data)) {
        $added_by = $delay_reason_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $delay_reason_master_update_data)) {
        $added_on = $delay_reason_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $delay_reason_master_update_uquery_base = "update delay_reason_master set";

    $delay_reason_master_update_uquery_set = "";

    $delay_reason_master_update_uquery_where = " where delay_reason_master_id = :reason_id";

    $delay_reason_master_update_udata = array(":reason_id"=>$reason_id);

    $filter_count = 0;

    if ($name != "") {
        $delay_reason_master_update_uquery_set = $delay_reason_master_update_uquery_set." delay_reason_master_name = :name,";
        $delay_reason_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $delay_reason_master_update_uquery_set = $delay_reason_master_update_uquery_set." delay_reason_master_active = :active,";
        $delay_reason_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $delay_reason_master_update_uquery_set = $delay_reason_master_update_uquery_set." delay_reason_master_remarks = :remarks,";
        $delay_reason_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $delay_reason_master_update_uquery_set = $delay_reason_master_update_uquery_set." delay_reason_master_added_by = :added_by,";
        $delay_reason_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $delay_reason_master_update_uquery_set = $delay_reason_master_update_uquery_set." delay_reason_master_added_on = :added_on,";
        $delay_reason_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $delay_reason_master_update_uquery_set = trim($delay_reason_master_update_uquery_set, ',');
    }

    $delay_reason_master_update_uquery = $delay_reason_master_update_uquery_base.$delay_reason_master_update_uquery_set.$delay_reason_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $delay_reason_master_update_ustatement = $dbConnection->prepare($delay_reason_master_update_uquery);

        $delay_reason_master_update_ustatement -> execute($delay_reason_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $reason_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To add new Project Manpower Agency
INPUT 	: Manpower Agency ID, Name, Remarks, Added By
OUTPUT 	: Manpower Agency ID, success or failure message
BY 		: Ashwini
*/
function db_add_project_manpower_agency($name, $remarks, $added_by)
{
    // Query
    $project_manpower_agency_iquery = "insert into project_manpower_agency
(project_manpower_agency_name,project_manpower_agency_active,project_manpower_agency_remarks,project_manpower_agency_added_by,project_manpower_agency_added_on)values(:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_manpower_agency_istatement = $dbConnection->prepare($project_manpower_agency_iquery);

        // Data
        $project_manpower_agency_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));


        $dbConnection->beginTransaction();
        $project_manpower_agency_istatement->execute($project_manpower_agency_idata);
        $project_manpower_agency_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_manpower_agency_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Manpower Agency List
INPUT 	: Manpower Agency ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Manpower Agency
BY 		: Ashwini
*/
function db_get_project_manpower_agency($project_manpower_agency_search_data)
{
    if (array_key_exists("agency_id", $project_manpower_agency_search_data)) {
        $agency_id = $project_manpower_agency_search_data["agency_id"];
    } else {
        $agency_id= "";
    }

    if (array_key_exists("name", $project_manpower_agency_search_data)) {
        $name = $project_manpower_agency_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("agency_name_check", $project_manpower_agency_search_data)) {
        $agency_name_check = $project_manpower_agency_search_data["agency_name_check"];
    } else {
        $agency_name_check = "";
    }

    if (array_key_exists("active", $project_manpower_agency_search_data)) {
        $active = $project_manpower_agency_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_manpower_agency_search_data)) {
        $added_by = $project_manpower_agency_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_manpower_agency_search_data)) {
        $start_date= $project_manpower_agency_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_manpower_agency_search_data)) {
        $end_date= $project_manpower_agency_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_manpower_agency_list_squery_base = "select * from project_manpower_agency PMA inner join users U on U.user_id=PMA.project_manpower_agency_added_by";

    $get_project_manpower_agency_list_squery_where = "";
    $get_project_manpower_agency_list_squery_order_by = " order by project_manpower_agency_name ASC";
    $filter_count = 0;

    // Data
    $get_project_manpower_agency_list_sdata = array();

    if ($agency_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_id = :agency_id";
        } else {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." and project_manpower_agency_id = :agency_id";
        }

        // Data
        $get_project_manpower_agency_list_sdata[':agency_id'] = $agency_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            if ($agency_name_check == '1') {
                $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_name = :name";
                // Data
                $get_project_manpower_agency_list_sdata[':name']  = $name;
            } elseif ($agency_name_check == '2') {
                $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_name like :name";

                // Data
                $get_project_manpower_agency_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_name like :name";

                // Data
                $get_project_manpower_agency_list_sdata[':name']  = '%'.$name.'%';
            }
        } else {
            // Query
            if ($agency_name_check == '1') {
                $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." and project_manpower_agency_name = :name";

                // Data
                $get_project_manpower_agency_list_sdata[':name']  = $name;
            } elseif ($agency_name_check == '2') {
                $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." or project_manpower_agency_name like :name";
                // Data
                $get_project_manpower_agency_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." and project_manpower_agency_name like :name";

                // Data
                $get_project_manpower_agency_list_sdata[':name']  = '%'.$name.'%';
            }
        }

        $filter_count++;
    }


    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_active = :active";
        } else {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." and project_manpower_agency_active = :active";
        }

        // Data
        $get_project_manpower_agency_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_added_by = :added_by";
        } else {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." and project_manpower_agency_added_by = :added_by";
        }

        //Data
        $get_project_manpower_agency_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_added_on >= :start_date";
        } else {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." and project_manpower_agency_added_on >= :start_date";
        }

        //Data
        $get_project_manpower_agency_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." where project_manpower_agency_added_on <= :end_date";
        } else {
            // Query
            $get_project_manpower_agency_list_squery_where = $get_project_manpower_agency_list_squery_where." and project_manpower_agency_added_on <= :end_date";
        }

        //Data
        $get_project_manpower_agency_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_manpower_agency_list_squery = $get_project_manpower_agency_list_squery_base.$get_project_manpower_agency_list_squery_where.$get_project_manpower_agency_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_manpower_agency_list_sstatement = $dbConnection->prepare($get_project_manpower_agency_list_squery);

        $get_project_manpower_agency_list_sstatement -> execute($get_project_manpower_agency_list_sdata);

        $get_project_manpower_agency_list_sdetails = $get_project_manpower_agency_list_sstatement -> fetchAll();

        if (false === $get_project_manpower_agency_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_manpower_agency_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_manpower_agency_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Manpower Agency
INPUT 	: Manpower Agency ID, Project Manpower Agency Update Array
OUTPUT 	: Manpower Agency ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_project_manpower_agency($agency_id, $project_manpower_agency_update_data)
{
    if (array_key_exists("name", $project_manpower_agency_update_data)) {
        $name = $project_manpower_agency_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_manpower_agency_update_data)) {
        $active = $project_manpower_agency_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_manpower_agency_update_data)) {
        $remarks = $project_manpower_agency_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_manpower_agency_update_data)) {
        $added_by = $project_manpower_agency_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_manpower_agency_update_data)) {
        $added_on = $project_manpower_agency_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_manpower_agency_update_uquery_base = "update project_manpower_agency set";

    $project_manpower_agency_update_uquery_set = "";

    $project_manpower_agency_update_uquery_where = " where project_manpower_agency_id = :agency_id";

    $project_manpower_agency_update_udata = array(":agency_id"=>$agency_id);

    $filter_count = 0;

    if ($name != "") {
        $project_manpower_agency_update_uquery_set = $project_manpower_agency_update_uquery_set." project_manpower_agency_name = :name,";
        $project_manpower_agency_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $project_manpower_agency_update_uquery_set = $project_manpower_agency_update_uquery_set." project_manpower_agency_active = :active,";
        $project_manpower_agency_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_manpower_agency_update_uquery_set = $project_manpower_agency_update_uquery_set." project_manpower_agency_remarks = :remarks,";
        $project_manpower_agency_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_manpower_agency_update_uquery_set = $project_manpower_agency_update_uquery_set." project_manpower_agency_added_by = :added_by,";
        $project_manpower_agency_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_manpower_agency_update_uquery_set = $project_manpower_agency_update_uquery_set." project_manpower_agency_added_on = :added_on,";
        $project_manpower_agency_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_manpower_agency_update_uquery_set = trim($project_manpower_agency_update_uquery_set, ',');
    }

    $project_manpower_agency_update_uquery = $project_manpower_agency_update_uquery_base.$project_manpower_agency_update_uquery_set.$project_manpower_agency_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_manpower_agency_update_ustatement = $dbConnection->prepare($project_manpower_agency_update_uquery);

        $project_manpower_agency_update_ustatement -> execute($project_manpower_agency_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $agency_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To add new Project Machine Vendor Master
INPUT 	: Name, Remarks, Added by
OUTPUT 	: Master ID, success or failure message
BY 		: Ashwini
*/

function db_add_project_machine_vendor_master($name, $remarks, $added_by)
{
    // Query
    $project_machine_vendor_master_iquery = "insert into project_machine_vendor_master (project_machine_vendor_master_name,project_machine_vendor_master_active,project_machine_vendor_master_remarks,project_machine_vendor_master_added_by,project_machine_vendor_master_added_on) values (:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_machine_vendor_master_istatement = $dbConnection->prepare($project_machine_vendor_master_iquery);

        // Data
        $project_machine_vendor_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_machine_vendor_master_istatement->execute($project_machine_vendor_master_idata);
        $project_machine_vendor_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_vendor_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Machine Vendor Master List
INPUT 	: Master Id, Name, Active, Added by, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Machine Vendor Master
BY 		: Ashwini
*/
function db_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data)
{
    // Extract all input parameters
    if (array_key_exists("master_id", $project_machine_vendor_master_search_data)) {
        $master_id = $project_machine_vendor_master_search_data["master_id"];
    } else {
        $master_id = "";
    }

    if (array_key_exists("name", $project_machine_vendor_master_search_data)) {
        $name = $project_machine_vendor_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("name_check", $project_machine_vendor_master_search_data)) {
        $name_check = $project_machine_vendor_master_search_data["name_check"];
    } else {
        $name_check = "";
    }

    if (array_key_exists("active", $project_machine_vendor_master_search_data)) {
        $active = $project_machine_vendor_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_machine_vendor_master_search_data)) {
        $added_by = $project_machine_vendor_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_machine_vendor_master_search_data)) {
        $start_date = $project_machine_vendor_master_search_data["start_date"];
    } else {
        $start_date = "";
    }

    if (array_key_exists("end_date", $project_machine_vendor_master_search_data)) {
        $end_date = $project_machine_vendor_master_search_data["end_date"];
    } else {
        $end_date = "";
    }

    $get_project_machine_vendor_master_list_squery_base = "select * from project_machine_vendor_master PMVM inner join users U on U.user_id = PMVM.project_machine_vendor_master_added_by";

    $get_project_machine_vendor_master_list_squery_where = "";

    $filter_count = 0;

    // Data
    $get_project_machine_vendor_master_list_sdata = array();

    if ($master_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_id = :master_id";
        } else {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_id = :master_id";
        }

        // Data
        $get_project_machine_vendor_master_list_sdata[':master_id'] = $master_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            if ($name_check == '1') {
                // Query
                $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_name = :name";

                // Data
                $get_project_machine_vendor_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_name like :name";

                // Data
                $get_project_machine_vendor_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_name like :name";

                // Data
                $get_project_machine_vendor_master_list_sdata[':name'] = '%'.$name.'%';
            }
        } else {
            if ($name_check == '1') {
                // Query
                $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_name = :name";

                // Data
                $get_project_machine_vendor_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_name like :name";

                // Data
                $get_project_machine_vendor_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_name like :name";

                // Data
                $get_project_machine_vendor_master_list_sdata[':name'] = '%'.$name.'%';
            }
        }

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_active = :active";
        } else {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_active = :active";
        }

        // Data
        $get_project_machine_vendor_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_added_by = :added_by";
        } else {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_added_by = :added_by";
        }

        //Data
        $get_project_machine_vendor_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_added_on >= :start_date";
        }

        //Data
        $get_project_machine_vendor_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." where project_machine_vendor_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_machine_vendor_master_list_squery_where = $get_project_machine_vendor_master_list_squery_where." and project_machine_vendor_master_added_on <= :end_date";
        }

        //Data
        $get_project_machine_vendor_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_machine_vendor_master_list_squery_order_by = " order by project_machine_vendor_master_name ASC";
    $get_project_machine_vendor_master_list_squery = $get_project_machine_vendor_master_list_squery_base.$get_project_machine_vendor_master_list_squery_where.$get_project_machine_vendor_master_list_squery_order_by;


    try {
        $dbConnection = get_conn_handle();

        $get_project_machine_vendor_master_list_sstatement = $dbConnection->prepare($get_project_machine_vendor_master_list_squery);

        $get_project_machine_vendor_master_list_sstatement -> execute($get_project_machine_vendor_master_list_sdata);

        $get_project_machine_vendor_master_list_sdetails = $get_project_machine_vendor_master_list_sstatement -> fetchAll();

        if (false === $get_project_machine_vendor_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_machine_vendor_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_machine_vendor_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Vendor Master
INPUT 	: Master ID, Project Machine Vendor Master Update Array
OUTPUT 	: Master ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_project_machine_vendor_master($master_id, $project_machine_vendor_master_update_data)
{
    if (array_key_exists("name", $project_machine_vendor_master_update_data)) {
        $name = $project_machine_vendor_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_machine_vendor_master_update_data)) {
        $active = $project_machine_vendor_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_machine_vendor_master_update_data)) {
        $remarks = $project_machine_vendor_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_machine_vendor_master_update_data)) {
        $added_by = $project_machine_vendor_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    $added_on = date('Y-m-d H:i:s');

    // Query
    $project_machine_vendor_master_update_uquery_base = "update project_machine_vendor_master set";

    $project_machine_vendor_master_update_uquery_set = "";

    $project_machine_vendor_master_update_uquery_where = " where project_machine_vendor_master_id = :master_id";

    $project_machine_vendor_master_update_udata = array(":master_id"=>$master_id);

    $filter_count = 0;

    if ($name != "") {
        $project_machine_vendor_master_update_uquery_set = $project_machine_vendor_master_update_uquery_set." project_machine_vendor_master_name = :name,";
        $project_machine_vendor_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $project_machine_vendor_master_update_uquery_set = $project_machine_vendor_master_update_uquery_set." project_machine_vendor_master_active = :active,";
        $project_machine_vendor_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_machine_vendor_master_update_uquery_set = $project_machine_vendor_master_update_uquery_set." project_machine_vendor_master_remarks = :remarks,";
        $project_machine_vendor_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_machine_vendor_master_update_uquery_set = $project_machine_vendor_master_update_uquery_set." project_machine_vendor_master_added_by = :added_by,";
        $project_machine_vendor_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_machine_vendor_master_update_uquery_set = $project_machine_vendor_master_update_uquery_set." project_machine_vendor_master_added_on = :added_on,";
        $project_machine_vendor_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_machine_vendor_master_update_uquery_set = trim($project_machine_vendor_master_update_uquery_set, ',');
    }

    $project_machine_vendor_master_update_uquery = $project_machine_vendor_master_update_uquery_base.$project_machine_vendor_master_update_uquery_set.$project_machine_vendor_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_machine_vendor_master_update_ustatement = $dbConnection->prepare($project_machine_vendor_master_update_uquery);

        $project_machine_vendor_master_update_ustatement -> execute($project_machine_vendor_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To add new Project Contract Rate Master
INPUT 	: Process, Work Task, Aplicable Date, UOM, rate, Remarks, Added By
OUTPUT 	: Contract Rate ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_contract_rate_master($vendor_id, $process, $work_task, $aplicable_date, $uom, $rate, $remarks, $added_by)
{
    // Query
    $project_contract_rate_master_iquery = "insert into project_contract_rate_master
   (project_contract_rate_master_vendor_id,project_contract_rate_master_process,project_contract_rate_master_work_task,project_contract_rate_master_aplicable_date,project_contract_rate_master_uom,
   project_contract_rate_master_rate,project_contract_rate_master_active,project_contract_rate_master_remarks,project_contract_rate_master_added_by,
   project_contract_rate_master_added_on)values(:vendor_id,:process,:work_task,:aplicable_date,:uom,:rate,:active,:remarks,:added_by,:added_on)";


    try {
        $dbConnection = get_conn_handle();
        $project_contract_rate_master_istatement = $dbConnection->prepare($project_contract_rate_master_iquery);

        // Data
        $project_contract_rate_master_idata = array(':vendor_id'=>$vendor_id,':process'=>$process,':work_task'=>$work_task,':aplicable_date'=>$aplicable_date,':uom'=>$uom,':rate'=>$rate,':active'=>'1',
        ':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_contract_rate_master_istatement->execute($project_contract_rate_master_idata);
        $project_contract_rate_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_contract_rate_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Contract Rate Master List
INPUT 	: Contract Rate ID, Process, Work Task, UOM, Rate, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Rate Master
BY 		: Lakshmi
*/
function db_get_project_contract_rate_master($project_contract_rate_master_search_data)
{
    if (array_key_exists("contract_rate_id", $project_contract_rate_master_search_data)) {
        $contract_rate_id = $project_contract_rate_master_search_data["contract_rate_id"];
    } else {
        $contract_rate_id= "";
    }

    if (array_key_exists("vendor_id", $project_contract_rate_master_search_data)) {
        $vendor_id = $project_contract_rate_master_search_data["vendor_id"];
    } else {
        $vendor_id= "";
    }

    if (array_key_exists("process", $project_contract_rate_master_search_data)) {
        $process = $project_contract_rate_master_search_data["process"];
    } else {
        $process = "";
    }

    if (array_key_exists("work_task", $project_contract_rate_master_search_data)) {
        $work_task = $project_contract_rate_master_search_data["work_task"];
    } else {
        $work_task = "";
    }

    if (array_key_exists("aplicable_date", $project_contract_rate_master_search_data)) {
        $aplicable_date = $project_contract_rate_master_search_data["aplicable_date"];
    } else {
        $aplicable_date = "";
    }


    if (array_key_exists("uom", $project_contract_rate_master_search_data)) {
        $uom = $project_contract_rate_master_search_data["uom"];
    } else {
        $uom = "";
    }

    if (array_key_exists("rate", $project_contract_rate_master_search_data)) {
        $rate = $project_contract_rate_master_search_data["rate"];
    } else {
        $rate = "";
    }

    if (array_key_exists("active", $project_contract_rate_master_search_data)) {
        $active = $project_contract_rate_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_contract_rate_master_search_data)) {
        $added_by = $project_contract_rate_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_contract_rate_master_search_data)) {
        $start_date= $project_contract_rate_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_contract_rate_master_search_data)) {
        $end_date= $project_contract_rate_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_contract_rate_master_list_squery_base = "select * from project_contract_rate_master PCRM inner join users U on U.user_id = PCRM.project_contract_rate_master_added_by inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PCRM.project_contract_rate_master_uom inner join project_contract_process PCP on PCP.project_contract_process_id = PCRM.project_contract_rate_master_process  inner join project_manpower_agency PMA on PMA.project_manpower_agency_id= PCRM.project_contract_rate_master_vendor_id";

    $get_project_contract_rate_master_list_squery_where = "";

    $filter_count = 0;

    // Data
    $get_project_contract_rate_master_list_sdata = array();

    if ($contract_rate_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_id = :contract_rate_id";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_id = :contract_rate_id";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':contract_rate_id'] = $contract_rate_id;

        $filter_count++;
    }

    if ($vendor_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_vendor_id = :vendor_id";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_vendor_id = :vendor_id";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':vendor_id'] = $vendor_id;

        $filter_count++;
    }

    if ($process != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_process = :process";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_process = :process";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':process'] = $process;

        $filter_count++;
    }

    if ($work_task != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_work_task = :work_task";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_work_task = :work_task";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':work_task'] = $work_task;

        $filter_count++;
    }

    if ($aplicable_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_aplicable_date = :aplicable_date";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_aplicable_date = :aplicable_date";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':aplicable_date'] = $aplicable_date;

        $filter_count++;
    }

    if ($uom != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_uom = :uom";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_uom = :uom";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':uom'] = $uom;

        $filter_count++;
    }

    if ($rate != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_rate = :rate";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_rate = :rate";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':rate'] = $rate;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_active = :active";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_active = :active";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_added_by = :added_by";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_added_by = :added_by";
        }

        //Data
        $get_project_contract_rate_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_added_on >= :start_date";
        }

        //Data
        $get_project_contract_rate_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_added_on <= :end_date";
        }

        //Data
        $get_project_contract_rate_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_contract_rate_master_list_squery_order_by = " order by project_contract_rate_master_work_task ASC";
    $get_project_contract_rate_master_list_squery = $get_project_contract_rate_master_list_squery_base.$get_project_contract_rate_master_list_squery_where.$get_project_contract_rate_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_contract_rate_master_list_sstatement = $dbConnection->prepare($get_project_contract_rate_master_list_squery);

        $get_project_contract_rate_master_list_sstatement -> execute($get_project_contract_rate_master_list_sdata);

        $get_project_contract_rate_master_list_sdetails = $get_project_contract_rate_master_list_sstatement -> fetchAll();

        if (false === $get_project_contract_rate_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_contract_rate_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_contract_rate_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

  /*
PURPOSE : To get Project Contract Rate Master List
INPUT 	: Contract Rate ID, Process, Work Task, UOM, Rate, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Rate Master
BY 		: Lakshmi
*/
function db_get_project_contract_max_rate_master($project_contract_rate_master_search_data)
{
    if (array_key_exists("contract_rate_id", $project_contract_rate_master_search_data)) {
        $contract_rate_id = $project_contract_rate_master_search_data["contract_rate_id"];
    } else {
        $contract_rate_id= "";
    }

    if (array_key_exists("process", $project_contract_rate_master_search_data)) {
        $process = $project_contract_rate_master_search_data["process"];
    } else {
        $process = "";
    }

    if (array_key_exists("work_task", $project_contract_rate_master_search_data)) {
        $work_task = $project_contract_rate_master_search_data["work_task"];
    } else {
        $work_task = "";
    }

    if (array_key_exists("aplicable_date", $project_contract_rate_master_search_data)) {
        $aplicable_date = $project_contract_rate_master_search_data["aplicable_date"];
    } else {
        $aplicable_date = "";
    }


    if (array_key_exists("uom", $project_contract_rate_master_search_data)) {
        $uom = $project_contract_rate_master_search_data["uom"];
    } else {
        $uom = "";
    }

    if (array_key_exists("rate", $project_contract_rate_master_search_data)) {
        $rate = $project_contract_rate_master_search_data["rate"];
    } else {
        $rate = "";
    }

    if (array_key_exists("active", $project_contract_rate_master_search_data)) {
        $active = $project_contract_rate_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_contract_rate_master_search_data)) {
        $added_by = $project_contract_rate_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_contract_rate_master_search_data)) {
        $start_date= $project_contract_rate_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_contract_rate_master_search_data)) {
        $end_date= $project_contract_rate_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_contract_rate_master_list_squery_base = "select *, MAX(project_contract_rate_master_rate) as max_contract_rate from project_contract_rate_master PCRM inner join users U on U.user_id = PCRM.project_contract_rate_master_added_by inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PCRM.project_contract_rate_master_uom inner join project_contract_process PCP on PCP.project_contract_process_id = PCRM.project_contract_rate_master_process";

    $get_project_contract_rate_master_list_squery_where = "";
    $filter_count = 0;

    // Data
    $get_project_contract_rate_master_list_sdata = array();

    if ($contract_rate_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_id = :contract_rate_id";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_id = :contract_rate_id";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':contract_rate_id'] = $contract_rate_id;

        $filter_count++;
    }

    if ($process != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_process = :process";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_process = :process";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':process'] = $process;

        $filter_count++;
    }

    if ($work_task != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_work_task = :work_task";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_work_task = :work_task";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':work_task'] = $work_task;

        $filter_count++;
    }

    if ($aplicable_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_aplicable_date = :aplicable_date";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_aplicable_date = :aplicable_date";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':aplicable_date'] = $aplicable_date;

        $filter_count++;
    }

    if ($uom != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_uom = :uom";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_uom = :uom";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':uom'] = $uom;

        $filter_count++;
    }

    if ($rate != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_rate = :rate";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_rate = :rate";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':rate'] = $rate;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_active = :active";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_active = :active";
        }

        // Data
        $get_project_contract_rate_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_added_by = :added_by";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_added_by = :added_by";
        }

        //Data
        $get_project_contract_rate_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_added_on >= :start_date";
        }

        //Data
        $get_project_contract_rate_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." where project_contract_rate_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_contract_rate_master_list_squery_where = $get_project_contract_rate_master_list_squery_where." and project_contract_rate_master_added_on <= :end_date";
        }

        //Data
        $get_project_contract_rate_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_contract_rate_master_list_squery = $get_project_contract_rate_master_list_squery_base.$get_project_contract_rate_master_list_squery_where;

    try {
        $dbConnection = get_conn_handle();

        $get_project_contract_rate_master_list_sstatement = $dbConnection->prepare($get_project_contract_rate_master_list_squery);

        $get_project_contract_rate_master_list_sstatement -> execute($get_project_contract_rate_master_list_sdata);

        $get_project_contract_rate_master_list_sdetails = $get_project_contract_rate_master_list_sstatement -> fetchAll();

        if (false === $get_project_contract_rate_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_contract_rate_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_contract_rate_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}
  /*
PURPOSE : To update Project Contract Rate Master
INPUT 	: Contract Rate ID, Project Contract Rate Master Update Array
OUTPUT 	: Contract Rate ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_contract_rate_master($contract_rate_id, $project_contract_rate_master_update_data)
{
    if (array_key_exists("process", $project_contract_rate_master_update_data)) {
        $process = $project_contract_rate_master_update_data["process"];
    } else {
        $process = "";
    }

    if (array_key_exists("vendor_id", $project_contract_rate_master_update_data)) {
        $vendor_id = $project_contract_rate_master_update_data["vendor_id"];
    } else {
        $vendor_id = "";
    }

    if (array_key_exists("work_task", $project_contract_rate_master_update_data)) {
        $work_task = $project_contract_rate_master_update_data["work_task"];
    } else {
        $work_task = "";
    }

    if (array_key_exists("aplicable_date", $project_contract_rate_master_update_data)) {
        $aplicable_date = $project_contract_rate_master_update_data["aplicable_date"];
    } else {
        $aplicable_date = "";
    }

    if (array_key_exists("uom", $project_contract_rate_master_update_data)) {
        $uom = $project_contract_rate_master_update_data["uom"];
    } else {
        $uom = "";
    }

    if (array_key_exists("rate", $project_contract_rate_master_update_data)) {
        $rate = $project_contract_rate_master_update_data["rate"];
    } else {
        $rate = "";
    }

    if (array_key_exists("active", $project_contract_rate_master_update_data)) {
        $active = $project_contract_rate_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_contract_rate_master_update_data)) {
        $remarks = $project_contract_rate_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_contract_rate_master_update_data)) {
        $added_by = $project_contract_rate_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_contract_rate_master_update_data)) {
        $added_on = $project_contract_rate_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_contract_rate_master_update_uquery_base = "update project_contract_rate_master set ";

    $project_contract_rate_master_update_uquery_set = "";

    $project_contract_rate_master_update_uquery_where = " where project_contract_rate_master_id = :contract_rate_id";

    $project_contract_rate_master_update_udata = array(":contract_rate_id"=>$contract_rate_id);

    $filter_count = 0;

    if ($process != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_process = :process,";
        $project_contract_rate_master_update_udata[":process"] = $process;
        $filter_count++;
    }

    if ($vendor_id != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_vendor_id = :vendor_id,";
        $project_contract_rate_master_update_udata[":vendor_id"] = $vendor_id;
        $filter_count++;
    }

    if ($work_task != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_work_task = :work_task,";
        $project_contract_rate_master_update_udata[":work_task"] = $work_task;
        $filter_count++;
    }

    if ($aplicable_date != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_aplicable_date = :aplicable_date,";
        $project_contract_rate_master_update_udata[":aplicable_date"] = $aplicable_date;
        $filter_count++;
    }

    if ($uom != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_uom = :uom,";
        $project_contract_rate_master_update_udata[":uom"] = $uom;
        $filter_count++;
    }

    if ($rate != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_rate = :rate,";
        $project_contract_rate_master_update_udata[":rate"] = $rate;
        $filter_count++;
    }

    if ($active != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_active = :active,";
        $project_contract_rate_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_remarks = :remarks,";
        $project_contract_rate_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_added_by = :added_by,";
        $project_contract_rate_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_contract_rate_master_update_uquery_set = $project_contract_rate_master_update_uquery_set." project_contract_rate_master_added_on = :added_on,";
        $project_contract_rate_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_contract_rate_master_update_uquery_set = trim($project_contract_rate_master_update_uquery_set, ',');
    }

    $project_contract_rate_master_update_uquery = $project_contract_rate_master_update_uquery_base.$project_contract_rate_master_update_uquery_set.$project_contract_rate_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_contract_rate_master_update_ustatement = $dbConnection->prepare($project_contract_rate_master_update_uquery);

        $project_contract_rate_master_update_ustatement -> execute($project_contract_rate_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $contract_rate_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Contract Process Master
INPUT 	: Contract Process ID, Name, Remarks, Added By
OUTPUT 	: Contract Process ID, success or failure message
BY 		: Ashwini
*/
function db_add_project_contract_process($name, $remarks, $added_by)
{
    // Query
    $project_contract_process_iquery = "insert into project_contract_process (project_contract_process_name,project_contract_process_active,project_contract_process_remarks,project_contract_process_added_by,project_contract_process_added_on)
   values(:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_contract_process_istatement = $dbConnection->prepare($project_contract_process_iquery);

        // Data
        $project_contract_process_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_contract_process_istatement->execute($project_contract_process_idata);
        $project_contract_process_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_contract_process_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Contract Process Master List
INPUT 	: Contract Process ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Process Master
BY 		: Ashwini
*/
function db_get_project_contract_process($project_contract_process_search_data)
{
    if (array_key_exists("contract_process_id", $project_contract_process_search_data)) {
        $contract_process_id = $project_contract_process_search_data["contract_process_id"];
    } else {
        $contract_process_id= "";
    }

    if (array_key_exists("name", $project_contract_process_search_data)) {
        $name = $project_contract_process_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("contract_process_name_check", $project_contract_process_search_data)) {
        $contract_process_name_check = $project_contract_process_search_data["contract_process_name_check"];
    } else {
        $contract_process_name_check = "";
    }

    if (array_key_exists("active", $project_contract_process_search_data)) {
        $active = $project_contract_process_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_contract_process_search_data)) {
        $added_by = $project_contract_process_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_contract_process_search_data)) {
        $start_date= $project_contract_process_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_contract_process_search_data)) {
        $end_date= $project_contract_process_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_contract_process_list_squery_base = "select * from project_contract_process  PCP inner join users U on U.user_id = PCP.project_contract_process_added_by ";

    $get_project_contract_process_list_squery_where = "";
    $get_project_contract_process_list_squery_order_by = " order by project_contract_process_name ASC";
    $filter_count = 0;

    // Data
    $get_project_contract_process_list_sdata = array();

    if ($contract_process_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_id = :contract_process_id";
        } else {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." and project_contract_process_id = :contract_process_id";
        }

        // Data
        $get_project_contract_process_list_sdata[':contract_process_id'] = $contract_process_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            if ($contract_process_name_check == '1') {
                $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_name = :name";
                // Data
                $get_project_contract_process_list_sdata[':name']  = $name;
            } elseif ($contract_process_name_check == '2') {
                $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_name like :name";

                // Data
                $get_project_contract_process_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_name like :name";

                // Data
                $get_project_contract_process_list_sdata[':name']  = '%'.$name.'%';
            }
        } else {
            // Query
            if ($contract_process_name_check == '1') {
                $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." and project_contract_process_name = :name";

                // Data
                $get_project_contract_process_list_sdata[':name']  = $name;
            } elseif ($contract_process_name_check == '2') {
                $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." or project_contract_process_name like :name";
                // Data
                $get_project_contract_process_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." and project_contract_process_name like :name";

                // Data
                $get_project_contract_process_list_sdata[':name']  = '%'.$name.'%';
            }
        }

        $filter_count++;
    }


    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_active = :active";
        } else {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." and project_contract_process_active = :active";
        }

        // Data
        $get_project_contract_process_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_added_by = :added_by";
        } else {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." and project_contract_process_added_by = :added_by";
        }

        //Data
        $get_project_contract_process_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_added_on >= :start_date";
        } else {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." and project_contract_process_added_on >= :start_date";
        }

        //Data
        $get_project_contract_process_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." where project_contract_process_added_on <= :end_date";
        } else {
            // Query
            $get_project_contract_process_list_squery_where = $get_project_contract_process_list_squery_where." and project_contract_process_added_on <= :end_date";
        }

        //Data
        $get_project_contract_process_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_contract_process_list_squery = $get_project_contract_process_list_squery_base.$get_project_contract_process_list_squery_where.$get_project_contract_process_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_contract_process_list_sstatement = $dbConnection->prepare($get_project_contract_process_list_squery);

        $get_project_contract_process_list_sstatement -> execute($get_project_contract_process_list_sdata);

        $get_project_contract_process_list_sdetails = $get_project_contract_process_list_sstatement -> fetchAll();

        if (false === $get_project_contract_process_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_contract_process_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_contract_process_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Contract Process Master
INPUT 	: Contract Process ID, Project Contract Process Master Update Array
OUTPUT 	: Contract Process ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_project_contract_process($contract_process_id, $project_contract_process_update_data)
{
    if (array_key_exists("name", $project_contract_process_update_data)) {
        $name = $project_contract_process_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_contract_process_update_data)) {
        $active = $project_contract_process_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_contract_process_update_data)) {
        $remarks = $project_contract_process_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_contract_process_update_data)) {
        $added_by = $project_contract_process_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_contract_process_update_data)) {
        $added_on = $project_contract_process_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_contract_process_update_uquery_base = "update project_contract_process set ";

    $project_contract_process_update_uquery_set = "";

    $project_contract_process_update_uquery_where = " where project_contract_process_id = :contract_process_id";

    $project_contract_process_update_udata = array(":contract_process_id"=>$contract_process_id);

    $filter_count = 0;
    if ($name != "") {
        $project_contract_process_update_uquery_set = $project_contract_process_update_uquery_set." project_contract_process_name = :name,";
        $project_contract_process_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $project_contract_process_update_uquery_set = $project_contract_process_update_uquery_set." project_contract_process_active = :active,";
        $project_contract_process_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_contract_process_update_uquery_set = $project_contract_process_update_uquery_set." project_contract_process_remarks = :remarks,";
        $project_contract_process_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_contract_process_update_uquery_set = $project_contract_process_update_uquery_set." project_contract_process_added_by = :added_by,";
        $project_contract_process_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_contract_process_update_uquery_set = $project_contract_process_update_uquery_set." project_contract_process_added_on = :added_on,";
        $project_contract_process_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_contract_process_update_uquery_set = trim($project_contract_process_update_uquery_set, ',');
    }

    $project_contract_process_update_uquery = $project_contract_process_update_uquery_base.$project_contract_process_update_uquery_set.$project_contract_process_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_contract_process_update_ustatement = $dbConnection->prepare($project_contract_process_update_uquery);

        $project_contract_process_update_ustatement -> execute($project_contract_process_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $contract_process_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Site Location Mapping Master
INPUT 	: Name, project ID, Is Road, Remarks, Added by
OUTPUT 	: Mapping ID, success or failure message
BY 		: Ashwini
*/
function db_add_project_site_location_mapping_master($name, $project_id, $is_road, $remarks, $added_by)
{
    // Query
    $project_site_location_mapping_master_iquery = "insert into project_site_location_mapping_master (project_site_location_mapping_master_name,project_site_location_mapping_master_project_id,project_site_location_mapping_master_is_road,
	project_site_location_mapping_master_active,project_site_location_mapping_master_remarks,project_site_location_mapping_master_added_by,
	project_site_location_mapping_master_added_on) values (:name,:project_id,:is_road,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_site_location_mapping_master_istatement = $dbConnection->prepare($project_site_location_mapping_master_iquery);

        // Data
        $project_site_location_mapping_master_idata = array(':name'=>$name,':project_id'=>$project_id,':is_road'=>$is_road,':active'=>'1',':remarks'=>$remarks,
        ':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_site_location_mapping_master_istatement->execute($project_site_location_mapping_master_idata);
        $project_site_location_mapping_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_site_location_mapping_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Site Location Mapping Master List
INPUT 	: Mapping Id, Name, Project ID, Is Road, Active, Added by, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Site Location Mapping Master
BY 		: Ashwini
*/
function db_get_project_site_location_mapping_master($project_site_location_mapping_master_search_data)
{
    // Extract all input parameters
    if (array_key_exists("mapping_id", $project_site_location_mapping_master_search_data)) {
        $mapping_id = $project_site_location_mapping_master_search_data["mapping_id"];
    } else {
        $mapping_id = "";
    }

    if (array_key_exists("name", $project_site_location_mapping_master_search_data)) {
        $name = $project_site_location_mapping_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("name_check", $project_site_location_mapping_master_search_data)) {
        $name_check = $project_site_location_mapping_master_search_data["name_check"];
    } else {
        $name_check = "";
    }

    if (array_key_exists("project_id", $project_site_location_mapping_master_search_data)) {
        $project_id = $project_site_location_mapping_master_search_data["project_id"];
    } else {
        $project_id = "";
    }
    
    if (array_key_exists("road_id", $project_site_location_mapping_master_search_data)) {
        $road_id = $project_site_location_mapping_master_search_data["road_id"];
    } else {
        $road_id = "";
    }

    if (array_key_exists("is_road", $project_site_location_mapping_master_search_data)) {
        $is_road = $project_site_location_mapping_master_search_data["is_road"];
    } else {
        $is_road = "";
    }

    if (array_key_exists("active", $project_site_location_mapping_master_search_data)) {
        $active = $project_site_location_mapping_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_site_location_mapping_master_search_data)) {
        $added_by = $project_site_location_mapping_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_site_location_mapping_master_search_data)) {
        $start_date = $project_site_location_mapping_master_search_data["start_date"];
    } else {
        $start_date = "";
    }

    if (array_key_exists("end_date", $project_site_location_mapping_master_search_data)) {
        $end_date = $project_site_location_mapping_master_search_data["end_date"];
    } else {
        $end_date = "";
    }

    $get_project_site_location_mapping_master_list_squery_base = "select * from project_site_location_mapping_master PSLM inner join users U on U.user_id = PSLM.project_site_location_mapping_master_added_by inner join project_management_project_master PMPM on PMPM.project_management_master_id = PSLM.project_site_location_mapping_master_project_id";

    $get_project_site_location_mapping_master_list_squery_where = "";

    $filter_count = 0;

    // Data
    $get_project_site_location_mapping_master_list_sdata = array();

    if ($mapping_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_id = :mapping_id";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_id = :mapping_id";
        }

        // Data
        $get_project_site_location_mapping_master_list_sdata[':mapping_id'] = $mapping_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            if ($name_check == '1') {
                // Query
                $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_name = :name";

                // Data
                $get_project_site_location_mapping_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_name like :name";

                // Data
                $get_project_site_location_mapping_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_name like :name";

                // Data
                $get_project_site_location_mapping_master_list_sdata[':name'] = '%'.$name.'%';
            }
        } else {
            if ($name_check == '1') {
                // Query
                $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_name = :name";

                // Data
                $get_project_site_location_mapping_master_list_sdata[':name'] = $name;
            } elseif ($name_check == '2') {
                // Query
                $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_name like :name";

                // Data
                $get_project_site_location_mapping_master_list_sdata[':name'] = $name.'%';
            } else {
                // Query
                $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_name like :name";

                // Data
                $get_project_site_location_mapping_master_list_sdata[':name'] = '%'.$name.'%';
            }
        }

        $filter_count++;
    }

    if ($project_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_project_id = :project_id";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_project_id = :project_id";
        }

        // Data
        $get_project_site_location_mapping_master_list_sdata[':project_id'] = $project_id;

        $filter_count++;
    }

    if ($road_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_id = :road_id";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_id = :road_id";
        }

        // Data
        $get_project_site_location_mapping_master_list_sdata[':road_id'] = $road_id;

        $filter_count++;
    }

    if ($is_road != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_is_road = :is_road";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_is_road = :is_road";
        }

        // Data
        $get_project_site_location_mapping_master_list_sdata[':is_road'] = $is_road;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_active = :active";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_active = :active";
        }

        // Data
        $get_project_site_location_mapping_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_added_by = :added_by";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_added_by = :added_by";
        }

        //Data
        $get_project_site_location_mapping_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_added_on >= :start_date";
        }

        //Data
        $get_project_site_location_mapping_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." where project_site_location_mapping_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_site_location_mapping_master_list_squery_where = $get_project_site_location_mapping_master_list_squery_where." and project_site_location_mapping_master_added_on <= :end_date";
        }

        //Data
        $get_project_site_location_mapping_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_site_location_mapping_master_list_squery_order_by = " order by project_site_location_mapping_master_order ASC";
    $get_project_site_location_mapping_master_list_squery = $get_project_site_location_mapping_master_list_squery_base.$get_project_site_location_mapping_master_list_squery_where.$get_project_site_location_mapping_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_site_location_mapping_master_list_sstatement = $dbConnection->prepare($get_project_site_location_mapping_master_list_squery);

        $get_project_site_location_mapping_master_list_sstatement -> execute($get_project_site_location_mapping_master_list_sdata);

        $get_project_site_location_mapping_master_list_sdetails = $get_project_site_location_mapping_master_list_sstatement -> fetchAll();

        if (false === $get_project_site_location_mapping_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_site_location_mapping_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_site_location_mapping_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Site Location Mapping Master
INPUT 	: Mapping ID, Project Site Location Mapping Master Update Array
OUTPUT 	: Mapping ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_project_site_location_mapping_master($mapping_id, $project_site_location_mapping_master_update_data)
{
    if (array_key_exists("name", $project_site_location_mapping_master_update_data)) {
        $name = $project_site_location_mapping_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("project_id", $project_site_location_mapping_master_update_data)) {
        $project_id = $project_site_location_mapping_master_update_data["project_id"];
    } else {
        $project_id = "";
    }

    if (array_key_exists("is_road", $project_site_location_mapping_master_update_data)) {
        $is_road = $project_site_location_mapping_master_update_data["is_road"];
    } else {
        $is_road = "";
    }

    if (array_key_exists("active", $project_site_location_mapping_master_update_data)) {
        $active = $project_site_location_mapping_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_site_location_mapping_master_update_data)) {
        $remarks = $project_site_location_mapping_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_site_location_mapping_master_update_data)) {
        $added_by = $project_site_location_mapping_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    $added_on = date('Y-m-d H:i:s');

    // Query
    $project_site_location_mapping_master_update_uquery_base = "update project_site_location_mapping_master set ";

    $project_site_location_mapping_master_update_uquery_set = "";

    $project_site_location_mapping_master_update_uquery_where = " where project_site_location_mapping_master_id = :mapping_id";

    $project_site_location_mapping_master_update_udata = array(":mapping_id"=>$mapping_id);

    $filter_count = 0;

    if ($name != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_name = :name,";
        $project_site_location_mapping_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($project_id != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_project_id = :project_id,";
        $project_site_location_mapping_master_update_udata[":project_id"] = $project_id;
        $filter_count++;
    }
    if ($road_id != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_project_id = :project_id,";
        $project_site_location_mapping_master_update_udata[":road_id"] = $road_id;
        $filter_count++;
    }

    if ($is_road != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_is_road = :is_road,";
        $project_site_location_mapping_master_update_udata[":is_road"] = $is_road;
        $filter_count++;
    }

    if ($active != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_active = :active,";
        $project_site_location_mapping_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_remarks = :remarks,";
        $project_site_location_mapping_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_added_by = :added_by,";
        $project_site_location_mapping_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_site_location_mapping_master_update_uquery_set = $project_site_location_mapping_master_update_uquery_set." project_site_location_mapping_master_added_on = :added_on,";
        $project_site_location_mapping_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_site_location_mapping_master_update_uquery_set = trim($project_site_location_mapping_master_update_uquery_set, ',');
    }

    $project_site_location_mapping_master_update_uquery = $project_site_location_mapping_master_update_uquery_base.$project_site_location_mapping_master_update_uquery_set.$project_site_location_mapping_master_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();

        $project_site_location_mapping_master_update_ustatement = $dbConnection->prepare($project_site_location_mapping_master_update_uquery);

        $project_site_location_mapping_master_update_ustatement -> execute($project_site_location_mapping_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $mapping_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To add new Project Man Power Rate History
INPUT 	: Type ID, Vendor ID, Cost Per Hour, Added By
OUTPUT 	: History ID, success or failure message
BY 		: Lakshmi
*/
function db_add_man_power_rate_history($type_id, $vendor_id, $cost_per_hour, $added_by)
{
    // Query
    $man_power_rate_history_iquery = "insert into project_man_power_rate_history
   (project_man_power_rate_history_type_id,project_man_power_rate_history_vendor_id,project_man_power_rate_history_cost_per_hour, project_man_power_rate_history_added_by,
   project_man_power_rate_history_added_on) values (:type_id,:vendor_id,:cost_per_hour,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $man_power_rate_history_istatement = $dbConnection->prepare($man_power_rate_history_iquery);

        // Data
        $man_power_rate_history_idata = array(':type_id'=>$type_id,':vendor_id'=>$vendor_id,':cost_per_hour'=>$cost_per_hour,':added_by'=>$added_by,
       ':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $man_power_rate_history_istatement->execute($man_power_rate_history_idata);
        $man_power_rate_history_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $man_power_rate_history_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Man Power Rate History List
INPUT 	: History ID, Type ID, Vendor ID, Cost Per Hour, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Man Power Rate History
BY 		: Lakshmi
*/
function db_get_man_power_rate_history($man_power_rate_history_search_data)
{
    if (array_key_exists("history_id", $man_power_rate_history_search_data)) {
        $history_id = $man_power_rate_history_search_data["history_id"];
    } else {
        $history_id= "";
    }

    if (array_key_exists("type_id", $man_power_rate_history_search_data)) {
        $type_id = $man_power_rate_history_search_data["type_id"];
    } else {
        $type_id = "";
    }

    if (array_key_exists("vendor_id", $man_power_rate_history_search_data)) {
        $vendor_id = $man_power_rate_history_search_data["vendor_id"];
    } else {
        $vendor_id = "";
    }

    if (array_key_exists("cost_per_hour", $man_power_rate_history_search_data)) {
        $cost_per_hour = $man_power_rate_history_search_data["cost_per_hour"];
    } else {
        $cost_per_hour = "";
    }

    if (array_key_exists("added_by", $man_power_rate_history_search_data)) {
        $added_by = $man_power_rate_history_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $man_power_rate_history_search_data)) {
        $start_date= $man_power_rate_history_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $man_power_rate_history_search_data)) {
        $end_date= $man_power_rate_history_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_man_power_rate_history_list_squery_base = "select * from project_man_power_rate_history ";

    $get_man_power_rate_history_list_squery_where = "";
    $filter_count = 0;

    // Data
    $get_man_power_rate_history_list_sdata = array();

    if ($history_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." where project_man_power_rate_history_id = :history_id";
        } else {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." and project_man_power_rate_history_id = :history_id";
        }

        // Data
        $get_man_power_rate_history_list_sdata[':history_id'] = $history_id;

        $filter_count++;
    }

    if ($type_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." where project_man_power_rate_history_type_id = :type_id";
        } else {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." and project_man_power_rate_history_type_id = :type_id";
        }

        // Data
        $get_man_power_rate_history_list_sdata[':type_id'] = $type_id;

        $filter_count++;
    }

    if ($vendor_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." where project_man_power_rate_history_vendor_id = :vendor_id";
        } else {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." and project_man_power_rate_history_vendor_id = :vendor_id";
        }

        // Data
        $get_man_power_rate_history_list_sdata[':vendor_id'] = $vendor_id;

        $filter_count++;
    }

    if ($cost_per_hour != "") {
        if ($filter_count == 0) {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." where project_man_power_rate_history_cost_per_hour = :cost_per_hour";
        } else {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." and project_man_power_rate_history_cost_per_hour = :cost_per_hour";
        }

        // Data
        $get_man_power_rate_history_list_sdata[':cost_per_hour'] = $cost_per_hour;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." where project_man_power_rate_history_added_by = :added_by";
        } else {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." and project_man_power_rate_history_added_by = :added_by";
        }

        //Data
        $get_man_power_rate_history_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." where project_man_power_rate_history_added_on >= :start_date";
        } else {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." and project_man_power_rate_history_added_on >= :start_date";
        }

        //Data
        $get_man_power_rate_history_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." where project_man_power_rate_history_added_on <= :end_date";
        } else {
            // Query
            $get_man_power_rate_history_list_squery_where = $get_man_power_rate_history_list_squery_where." and project_man_power_rate_history_added_on <= :end_date";
        }

        //Data
        $get_man_power_rate_history_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_man_power_rate_history_list_squery = $get_man_power_rate_history_list_squery_base.$get_man_power_rate_history_list_squery_where;

    try {
        $dbConnection = get_conn_handle();

        $get_man_power_rate_history_list_sstatement = $dbConnection->prepare($get_man_power_rate_history_list_squery);

        $get_man_power_rate_history_list_sstatement -> execute($get_man_power_rate_history_list_sdata);

        $get_man_power_rate_history_list_sdetails = $get_man_power_rate_history_list_sstatement -> fetchAll();

        if (false === $get_man_power_rate_history_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_man_power_rate_history_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_man_power_rate_history_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

  /*
PURPOSE : To update Project Man Power Rate History
INPUT 	: History ID, Project Man Power Rate History  Update Array
OUTPUT 	: History ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_man_power_rate_history($history_id, $man_power_rate_history_update_data)
{
    if (array_key_exists("type_id", $man_power_rate_history_update_data)) {
        $type_id = $man_power_rate_history_update_data["type_id"];
    } else {
        $type_id = "";
    }

    if (array_key_exists("vendor_id", $man_power_rate_history_update_data)) {
        $vendor_id = $man_power_rate_history_update_data["vendor_id"];
    } else {
        $vendor_id = "";
    }

    if (array_key_exists("cost_per_hour", $man_power_rate_history_update_data)) {
        $cost_per_hour = $man_power_rate_history_update_data["cost_per_hour"];
    } else {
        $cost_per_hour = "";
    }

    if (array_key_exists("added_by", $man_power_rate_history_update_data)) {
        $added_by = $man_power_rate_history_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $man_power_rate_history_update_data)) {
        $added_on = $man_power_rate_history_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $man_power_rate_history_update_uquery_base = "update project_man_power_rate_history set";

    $man_power_rate_history_update_uquery_set = "";

    $man_power_rate_history_update_uquery_where = " where project_man_power_rate_history_id = :history_id";

    $man_power_rate_history_update_udata = array(":history_id"=>$history_id);

    $filter_count = 0;

    if ($type_id != "") {
        $man_power_rate_history_update_uquery_set = $man_power_rate_history_update_uquery_set." project_man_power_rate_history_type_id = :type_id,";
        $man_power_rate_history_update_udata[":type_id"] = $type_id;
        $filter_count++;
    }

    if ($vendor_id != "") {
        $man_power_rate_history_update_uquery_set = $man_power_rate_history_update_uquery_set." project_man_power_rate_history_vendor_id = :vendor_id,";
        $man_power_rate_history_update_udata[":vendor_id"] = $vendor_id;
        $filter_count++;
    }

    if ($cost_per_hour != "") {
        $man_power_rate_history_update_uquery_set = $man_power_rate_history_update_uquery_set." project_man_power_rate_history_cost_per_hour = :cost_per_hour,";
        $man_power_rate_history_update_udata[":cost_per_hour"] = $cost_per_hour;
        $filter_count++;
    }

    if ($added_by != "") {
        $man_power_rate_history_update_uquery_set = $man_power_rate_history_update_uquery_set." project_man_power_rate_history_added_by = :added_by,";
        $man_power_rate_history_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $man_power_rate_history_update_uquery_set = $man_power_rate_history_update_uquery_set." project_man_power_rate_history_added_on = :added_on,";
        $man_power_rate_history_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $man_power_rate_history_update_uquery_set = trim($man_power_rate_history_update_uquery_set, ',');
    }

    $man_power_rate_history_update_uquery = $man_power_rate_history_update_uquery_base.$man_power_rate_history_update_uquery_set.$man_power_rate_history_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $man_power_rate_history_update_ustatement = $dbConnection->prepare($man_power_rate_history_update_uquery);

        $man_power_rate_history_update_ustatement -> execute($man_power_rate_history_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $history_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To add new Project TDS Deduction
INPUT 	: Master Type, Deduction, Effective Date, Remarks, Added By
OUTPUT 	: Master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_tds_deduction_master($master_type, $deduction, $effective_date, $remarks, $added_by)
{
    // Query
    $project_tds_deduction_master_iquery = "insert into project_tds_deduction_master
   (project_tds_deduction_master_type,project_tds_deduction_master_deduction,project_tds_deduction_master_effective_date,project_tds_deduction_master_remarks,
   project_tds_deduction_master_added_by,project_tds_deduction_master_added_on) values (:master_type,:deduction,:effective_date,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_tds_deduction_master_istatement = $dbConnection->prepare($project_tds_deduction_master_iquery);

        // Data
        $project_tds_deduction_master_idata = array(':master_type'=>$master_type,':deduction'=>$deduction,':effective_date'=>$effective_date,':remarks'=>$remarks,
        ':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_tds_deduction_master_istatement->execute($project_tds_deduction_master_idata);
        $project_tds_deduction_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_tds_deduction_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project TDS Deduction List
INPUT 	: Master ID, Master Type, Deduction, Effective Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project TDS Deduction
BY 		: Lakshmi
*/
function db_get_project_tds_deduction_master($project_tds_deduction_master_search_data)
{
    if (array_key_exists("master_id", $project_tds_deduction_master_search_data)) {
        $master_id = $project_tds_deduction_master_search_data["master_id"];
    } else {
        $master_id= "";
    }

    if (array_key_exists("master_type", $project_tds_deduction_master_search_data)) {
        $master_type = $project_tds_deduction_master_search_data["master_type"];
    } else {
        $master_type = "";
    }

    if (array_key_exists("deduction", $project_tds_deduction_master_search_data)) {
        $deduction = $project_tds_deduction_master_search_data["deduction"];
    } else {
        $deduction = "";
    }

    if (array_key_exists("effective_date", $project_tds_deduction_master_search_data)) {
        $effective_date = $project_tds_deduction_master_search_data["effective_date"];
    } else {
        $effective_date = "";
    }

    if (array_key_exists("remarks", $project_tds_deduction_master_search_data)) {
        $remarks = $project_tds_deduction_master_search_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_tds_deduction_master_search_data)) {
        $added_by = $project_tds_deduction_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_tds_deduction_master_search_data)) {
        $start_date= $project_tds_deduction_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_tds_deduction_master_search_data)) {
        $end_date= $project_tds_deduction_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_tds_deduction_master_list_squery_base = "select * from project_tds_deduction_master PDM inner join users U on U.user_id = PDM.project_tds_deduction_master_added_by";

    $get_project_tds_deduction_master_list_squery_where = "";
    $filter_count = 0;

    // Data
    $get_project_tds_deduction_master_list_sdata = array();

    if ($master_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_id = :master_id";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_id = :master_id";
        }

        // Data
        $get_project_tds_deduction_master_list_sdata[':master_id'] = $master_id;

        $filter_count++;
    }

    if ($master_type != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_type = :master_type";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_type = :master_type";
        }

        // Data
        $get_project_tds_deduction_master_list_sdata[':master_type'] = $master_type;

        $filter_count++;
    }

    if ($deduction != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_deduction = :deduction";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_deduction = :deduction";
        }

        // Data
        $get_project_tds_deduction_master_list_sdata[':deduction'] = $deduction;

        $filter_count++;
    }

    if ($effective_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_effective_date = :effective_date";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_effective_date = :effective_date";
        }

        // Data
        $get_project_tds_deduction_master_list_sdata[':effective_date'] = $effective_date;

        $filter_count++;
    }

    if ($remarks != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_remarks = :remarks";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_remarks = :remarks";
        }

        // Data
        $get_project_tds_deduction_master_list_sdata[':remarks'] = $remarks;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_added_by = :added_by";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_added_by = :added_by";
        }

        //Data
        $get_project_tds_deduction_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_added_on >= :start_date";
        }

        //Data
        $get_project_tds_deduction_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." where project_tds_deduction_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_tds_deduction_master_list_squery_where = $get_project_tds_deduction_master_list_squery_where." and project_tds_deduction_master_added_on <= :end_date";
        }

        //Data
        $get_project_tds_deduction_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_tds_deduction_master_list_squery = $get_project_tds_deduction_master_list_squery_base.$get_project_tds_deduction_master_list_squery_where;

    try {
        $dbConnection = get_conn_handle();

        $get_project_tds_deduction_master_list_sstatement = $dbConnection->prepare($get_project_tds_deduction_master_list_squery);

        $get_project_tds_deduction_master_list_sstatement -> execute($get_project_tds_deduction_master_list_sdata);

        $get_project_tds_deduction_master_list_sdetails = $get_project_tds_deduction_master_list_sstatement -> fetchAll();

        if (false === $get_project_tds_deduction_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_tds_deduction_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_tds_deduction_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

 /*
PURPOSE : To update Project TDS Deduction
INPUT 	: Master ID,Project TDS Deduction Update Array
OUTPUT 	: Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_tds_deduction_master($master_id, $project_tds_deduction_master_update_data)
{
    if (array_key_exists("master_type", $project_tds_deduction_master_update_data)) {
        $master_type = $project_tds_deduction_master_update_data["master_type"];
    } else {
        $master_type = "";
    }

    if (array_key_exists("deduction", $project_tds_deduction_master_update_data)) {
        $deduction = $project_tds_deduction_master_update_data["deduction"];
    } else {
        $deduction = "";
    }

    if (array_key_exists("effective_date", $project_tds_deduction_master_update_data)) {
        $effective_date = $project_tds_deduction_master_update_data["effective_date"];
    } else {
        $effective_date = "";
    }

    if (array_key_exists("remarks", $project_tds_deduction_master_update_data)) {
        $remarks = $project_tds_deduction_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_tds_deduction_master_update_data)) {
        $added_by = $project_tds_deduction_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_tds_deduction_master_update_data)) {
        $added_on = $project_tds_deduction_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_tds_deduction_master_update_uquery_base = "update project_tds_deduction_master set ";

    $project_tds_deduction_master_update_uquery_set = "";

    $project_tds_deduction_master_update_uquery_where = " where project_tds_deduction_master_id = :master_id";

    $project_tds_deduction_master_update_udata = array(":master_id"=>$master_id);

    $filter_count = 0;

    if ($master_type != "") {
        $project_tds_deduction_master_update_uquery_set = $project_tds_deduction_master_update_uquery_set." project_tds_deduction_master_type = :master_type,";
        $project_tds_deduction_master_update_udata[":master_type"] = $master_type;
        $filter_count++;
    }

    if ($deduction != "") {
        $project_tds_deduction_master_update_uquery_set = $project_tds_deduction_master_update_uquery_set." project_tds_deduction_master_deduction = :deduction,";
        $project_tds_deduction_master_update_udata[":deduction"] = $deduction;
        $filter_count++;
    }

    if ($effective_date != "") {
        $project_tds_deduction_master_update_uquery_set = $project_tds_deduction_master_update_uquery_set." project_tds_deduction_master_effective_date = :effective_date,";
        $project_tds_deduction_master_update_udata[":effective_date"] = $effective_date;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_tds_deduction_master_update_uquery_set = $project_tds_deduction_master_update_uquery_set." project_tds_deduction_master_remarks = :remarks,";
        $project_tds_deduction_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_tds_deduction_master_update_uquery_set = $project_tds_deduction_master_update_uquery_set." project_tds_deduction_master_added_by = :added_by,";
        $project_tds_deduction_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_tds_deduction_master_update_uquery_set = $project_tds_deduction_master_update_uquery_set." project_tds_deduction_master_added_on = :added_on,";
        $project_tds_deduction_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_tds_deduction_master_update_uquery_set = trim($project_tds_deduction_master_update_uquery_set, ',');
    }

    $project_tds_deduction_master_update_uquery = $project_tds_deduction_master_update_uquery_base.$project_tds_deduction_master_update_uquery_set.$project_tds_deduction_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_tds_deduction_master_update_ustatement = $dbConnection->prepare($project_tds_deduction_master_update_uquery);

        $project_tds_deduction_master_update_ustatement -> execute($project_tds_deduction_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project Object Output Master
INPUT 	: Process ID, Task ID, UOM ,Obeject Type,Obeject/hr,Active,Remarks,Added By,Added On
OUTPUT 	: Rework ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_object_output($process_id, $task_id, $uom, $object_type, $reference_id, $object_per_hr, $remarks, $added_by)
{
    // Query
    $project_object_output_iquery = "insert into project_object_output_master
   (project_object_output_process_id,project_object_output_task_id,project_object_output_uom,project_object_output_object_type,project_object_output_object_type_reference_id,project_object_output_obejct_per_hr,project_object_output_active,project_object_output_remarks,project_object_output_added_by,project_object_output_added_on)
   values (:process_id,:task_id,:uom,:object_type,:reference_id,:object_per_hr,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_object_output_istatement = $dbConnection->prepare($project_object_output_iquery);

        // Data
        $project_object_output_idata = array(':process_id'=>$process_id,':task_id'=>$task_id,':uom'=>$uom,':object_type'=>$object_type,
        ':reference_id'=>$reference_id,':object_per_hr'=>$object_per_hr,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
        $dbConnection->beginTransaction();

        $project_object_output_istatement->execute($project_object_output_idata);
        $project_object_output_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_object_output_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Machine Rework list
INPUT 	: Rework ID, Task ID, Vendor ID, Machine ID, Start Date Time, End Date Time, Plan Off Time, Aditional Cost, Number, Fuel Charges, with Fuel Charges, Bata,
          Issued Fuel, Display Status, Completion,  Machine Type, Check Status,, Checked By, Checked On, Approved By, Approved On, Added By Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Machine Rework
BY 		: Lakshmi
*/
function db_get_project_object_output($project_object_output_search_data)
{
    if (array_key_exists("output_id", $project_object_output_search_data)) {
        $output_id = $project_object_output_search_data["output_id"];
    } else {
        $output_id= "";
    }

    if (array_key_exists("process_id", $project_object_output_search_data)) {
        $process_id = $project_object_output_search_data["process_id"];
    } else {
        $process_id= "";
    }

    if (array_key_exists("task_id", $project_object_output_search_data)) {
        $task_id = $project_object_output_search_data["task_id"];
    } else {
        $task_id= "";
    }

    if (array_key_exists("uom", $project_object_output_search_data)) {
        $uom = $project_object_output_search_data["uom"];
    } else {
        $uom= "";
    }

    if (array_key_exists("object_type", $project_object_output_search_data)) {
        $object_type= $project_object_output_search_data["object_type"];
    } else {
        $object_type = "";
    }
    if (array_key_exists("reference_id", $project_object_output_search_data)) {
        $reference_id= $project_object_output_search_data["reference_id"];
    } else {
        $reference_id = "";
    }

    if (array_key_exists("object_per_hr", $project_object_output_search_data)) {
        $object_per_hr= $project_object_output_search_data["object_per_hr"];
    } else {
        $object_per_hr = "";
    }

    if (array_key_exists("active", $project_object_output_search_data)) {
        $active= $project_object_output_search_data["active"];
    } else {
        $active= "";
    }

    if (array_key_exists("added_by", $project_object_output_search_data)) {
        $added_by= $project_object_output_search_data["added_by"];
    } else {
        $added_by= "";
    }

    if (array_key_exists("start_date", $project_object_output_search_data)) {
        $start_date= $project_object_output_search_data["start_date"];
    } else {
        $start_date= "";
    }
    if (array_key_exists("end_date", $project_object_output_search_data)) {
        $end_date= $project_object_output_search_data["end_date"];
    } else {
        $end_date= "";
    }

    if (array_key_exists("sort", $project_object_output_search_data)) {
        $sort= $project_object_output_search_data["sort"];
    } else {
        $sort= "";
    }
    $get_project_object_output_list_squery_base = "select * from project_object_output_master POOM inner join project_process_master PPM on PPM.project_process_master_id=POOM.project_object_output_process_id inner join project_task_master PTM on PTM.project_task_master_id=POOM.project_object_output_task_id inner join users U on U.user_id= POOM.project_object_output_added_by left outer join project_machine_type_master PMTM on PMTM.project_machine_type_master_id=POOM.project_object_output_object_type_reference_id left outer join project_cw_master PCM on PCM.project_cw_master_id=POOM.project_object_output_object_type_reference_id inner join project_uom_master PUM on PUM.project_uom_id= POOM.project_object_output_uom";

    $get_project_object_output_list_squery_where = "";

    $get_project_object_output_list_order_by = "";


    $filter_count = 0;

    // Data
    $get_project_object_output_list_sdata = array();

    if ($output_id != "") {
        if ($filter_count == 0) {
            // Quer
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_master_id = :output_id";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_master_id = :output_id";
        }

        // Data
        $get_project_object_output_list_sdata[':output_id'] = $output_id;

        $filter_count++;
    }

    if ($process_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_process_id = :process_id";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_process_id = :process_id";
        }

        // Data
        $get_project_object_output_list_sdata[':process_id'] = $process_id;

        $filter_count++;
    }

    if ($task_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_task_id = :task_id";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_task_id = :task_id";
        }

        // Data
        $get_project_object_output_list_sdata[':task_id'] = $task_id;

        $filter_count++;
    }

    if ($uom != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_uom = :uom";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_uom = :uom";
        }

        // Data
        $get_project_object_output_list_sdata[':uom'] = $uom;

        $filter_count++;
    }

    if ($object_type != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_object_type = :object_type";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_object_type = :object_type";
        }

        // Data
        $get_project_object_output_list_sdata[':object_type'] = $object_type;

        $filter_count++;
    }

    if ($reference_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_object_type_reference_id = :reference_id";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_object_type_reference_id = :reference_id";
        }

        // Data
        $get_project_object_output_list_sdata[':reference_id'] = $reference_id;

        $filter_count++;
    }

    if ($object_per_hr != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_obejct_per_hr = :object_per_hr";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_obejct_per_hr = :object_per_hr";
        }

        // Data
        $get_project_object_output_list_sdata[':object_per_hr'] = $object_per_hr;

        $filter_count++;
    }



    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_active = :active";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_active = :active";
        }

        // Data
        $get_project_object_output_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_added_by = :added_by";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_added_by = :added_by";
        }

        //Data
        $get_project_object_output_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_added_on >= :start_date";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_added_on >= :start_date";
        }

        //Data
        $get_project_object_output_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." where project_object_output_added_on <= :end_date";
        } else {
            // Query
            $get_project_object_output_list_squery_where = $get_project_object_output_list_squery_where." and project_object_output_added_on <= :end_date";
        }

        //Data
        $get_project_object_output_list_sdata[':end_date']  = $end_date;

        $filter_count++;
    }
    if ($sort != "") {
        $get_project_object_output_list_order_by  = " order by project_object_output_added_on DESC";
    } else {
        $get_project_object_output_list_order_by  = " order by project_process_master_order ASC,project_task_master_order ASC";
    }


    $get_project_object_output_list_squery = $get_project_object_output_list_squery_base.$get_project_object_output_list_squery_where.$get_project_object_output_list_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_object_output_list_sstatement = $dbConnection->prepare($get_project_object_output_list_squery);

        $get_project_object_output_list_sstatement -> execute($get_project_object_output_list_sdata);

        $get_project_object_output_list_sdetails = $get_project_object_output_list_sstatement -> fetchAll();

        if (false === $get_project_object_output_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_object_output_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_object_output_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Object Output
INPUT 	: Rework ID, Project Object Output Update Array
OUTPUT 	: Output ID; Message of success or failure
BY 		: Sonakshi D
*/
function db_update_project_object_output($output_id, $project_object_output_update_data)
{
    if (array_key_exists("process_id", $project_object_output_update_data)) {
        $process_id = $project_object_output_update_data["process_id"];
    } else {
        $process_id = "";
    }

    if (array_key_exists("task_id", $project_object_output_update_data)) {
        $task_id = $project_object_output_update_data["task_id"];
    } else {
        $task_id = "";
    }

    if (array_key_exists("uom", $project_object_output_update_data)) {
        $uom = $project_object_output_update_data["uom"];
    } else {
        $uom = "";
    }

    if (array_key_exists("object_type", $project_object_output_update_data)) {
        $object_type = $project_object_output_update_data["object_type"];
    } else {
        $object_type = "";
    }

    if (array_key_exists("reference_id", $project_object_output_update_data)) {
        $reference_id = $project_object_output_update_data["reference_id"];
    } else {
        $reference_id = "";
    }

    if (array_key_exists("object_per_hr", $project_object_output_update_data)) {
        $object_per_hr = $project_object_output_update_data["object_per_hr"];
    } else {
        $object_per_hr = "";
    }

    if (array_key_exists("active", $project_object_output_update_data)) {
        $active = $project_object_output_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_object_output_update_data)) {
        $remarks = $project_object_output_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_object_output_update_data)) {
        $added_by = $project_object_output_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_object_output_update_data)) {
        $added_on = $project_object_output_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_object_output_update_uquery_base = "update project_object_output_master set";

    $project_object_output_update_uquery_set = "";

    $project_object_output_update_uquery_where = " where project_object_output_master_id = :output_id";

    $project_object_rework_update_udata = array(":output_id"=>$output_id);

    $filter_count = 0;

    if ($task_id != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_task_id = :task_id,";
        $project_object_rework_update_udata[":task_id"] = $task_id;
        $filter_count++;
    }

    if ($task_id != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_task_id = :task_id,";
        $project_object_rework_update_udata[":task_id"] = $task_id;
        $filter_count++;
    }

    if ($uom != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_uom = :uom,";
        $project_object_rework_update_udata[":uom"] = $uom;
        $filter_count++;
    }

    if ($object_type != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_object_type = :object_type,";
        $project_object_rework_update_udata[":object_type"] = $object_type;
        $filter_count++;
    }

    if ($reference_id != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_object_type_reference_id	 = :reference_id,";
        $project_object_rework_update_udata[":reference_id"] = $reference_id;
        $filter_count++;
    }

    if ($object_per_hr != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_obejct_per_hr = :object_per_hr,";
        $project_object_rework_update_udata[":object_per_hr"] = $object_per_hr;
        $filter_count++;
    }

    if ($active != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_active = :active,";
        $project_object_rework_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_remarks = :remarks,";
        $project_object_rework_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }
    if ($added_by != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_added_by = :added_by,";
        $project_object_rework_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_added_on = :added_on,";
        $project_object_rework_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_object_output_update_uquery_set = trim($project_object_output_update_uquery_set, ',');
    }

    $project_machine_rework_update_uquery = $project_object_output_update_uquery_base.$project_object_output_update_uquery_set.$project_object_output_update_uquery_where;
    try {
        $dbConnection = get_conn_handle();

        $project_machine_rework_update_ustatement = $dbConnection->prepare($project_machine_rework_update_uquery);

        $project_machine_rework_update_ustatement -> execute($project_object_rework_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $output_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To add new Project CW Master
INPUT 	: Name, Set No, Rate, Remarks, Added By
OUTPUT 	: CW Master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_cw_master($name, $set_no, $rate, $remarks, $added_by)
{
    // Query
    $project_cw_master_iquery = "insert into project_cw_master (project_cw_master_name,project_cw_master_set_no,project_cw_master_rate,project_cw_master_active,project_cw_master_remarks,project_cw_master_added_by,project_cw_master_added_on) values(:name,:set_no,:rate,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_cw_master_istatement = $dbConnection->prepare($project_cw_master_iquery);

        // Data
        $project_cw_master_idata = array(':name'=>$name,':set_no'=>$set_no,':rate'=>$rate,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
        ':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_cw_master_istatement->execute($project_cw_master_idata);
        $project_cw_master_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_cw_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project CW Master list
INPUT 	: CW Master ID, Name, Set No, Rate, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project CW Master
BY 		: Lakshmi
*/
function db_get_project_cw_master($project_cw_master_search_data)
{
    if (array_key_exists("cw_master_id", $project_cw_master_search_data)) {
        $cw_master_id = $project_cw_master_search_data["cw_master_id"];
    } else {
        $cw_master_id= "";
    }

    if (array_key_exists("name", $project_cw_master_search_data)) {
        $name = $project_cw_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("cw_name_check", $project_cw_master_search_data)) {
        $cw_name_check = $project_cw_master_search_data["cw_name_check"];
    } else {
        $cw_name_check = "";
    }

    if (array_key_exists("set_no", $project_cw_master_search_data)) {
        $set_no = $project_cw_master_search_data["set_no"];
    } else {
        $set_no = "";
    }

    if (array_key_exists("rate", $project_cw_master_search_data)) {
        $rate = $project_cw_master_search_data["rate"];
    } else {
        $rate = "";
    }

    if (array_key_exists("active", $project_cw_master_search_data)) {
        $active = $project_cw_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_cw_master_search_data)) {
        $added_by = $project_cw_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_cw_master_search_data)) {
        $start_date= $project_cw_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_cw_master_search_data)) {
        $end_date= $project_cw_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_cw_master_list_squery_base = "select * from project_cw_master PCWM inner join users U on U.user_id = PCWM.project_cw_master_added_by";

    $get_project_cw_master_list_squery_where = "";
    $get_project_cw_master_list_squery_order_by = " order by project_cw_master_name ASC";

    $filter_count = 0;

    // Data
    $get_project_cw_master_list_sdata = array();

    if ($cw_master_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_id = :cw_master_id";
        } else {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_id = :cw_master_id";
        }

        // Data
        $get_project_cw_master_list_sdata[':cw_master_id'] = $cw_master_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            if ($cw_name_check == '1') {
                $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_name = :name";
                // Data
                $get_project_cw_master_list_sdata[':name']  = $name;
            } elseif ($cw_name_check == '2') {
                $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_name like :name";

                // Data
                $get_project_cw_master_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_name like :name";

                // Data
                $get_project_cw_master_list_sdata[':name']  = '%'.$name.'%';
            }
        } else {
            // Query
            if ($cw_name_check == '1') {
                $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_name = :name";

                // Data
                $get_project_cw_master_list_sdata[':name']  = $name;
            } elseif ($cw_name_check == '2') {
                $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." or project_cw_master_name like :name";
                // Data
                $get_project_cw_master_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_name like :name";

                // Data
                $get_project_cw_master_list_sdata[':name']  = '%'.$name.'%';
            }
        }

        $filter_count++;
    }

    if ($set_no != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_set_no = :set_no";
        } else {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_set_no = :set_no";
        }

        // Data
        $get_project_cw_master_list_sdata[':set_no']  = $set_no;

        $filter_count++;
    }

    if ($rate != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_rate = :rate";
        } else {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_rate = :rate";
        }

        // Data
        $get_project_cw_master_list_sdata[':rate']  = $rate;

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_active = :active";
        } else {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_active = :active";
        }

        // Data
        $get_project_cw_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_added_by = :added_by";
        } else {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_added_by = :added_by";
        }

        //Data
        $get_project_cw_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_added_on >= :start_date";
        } else {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_added_on >= :start_date";
        }

        //Data
        $get_project_cw_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." where project_cw_master_added_on <= :end_date";
        } else {
            // Query
            $get_project_cw_master_list_squery_where = $get_project_cw_master_list_squery_where." and project_cw_master_added_on <= :end_date";
        }

        //Data
        $get_project_cw_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_cw_master_list_squery = $get_project_cw_master_list_squery_base.$get_project_cw_master_list_squery_where.$get_project_cw_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_cw_master_list_sstatement = $dbConnection->prepare($get_project_cw_master_list_squery);

        $get_project_cw_master_list_sstatement -> execute($get_project_cw_master_list_sdata);

        $get_project_cw_master_list_sdetails = $get_project_cw_master_list_sstatement -> fetchAll();

        if (false === $get_project_cw_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_cw_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_cw_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project CW Master
INPUT 	: CW Master ID, Project CW Master Update Array
OUTPUT 	: CW Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_cw_master($cw_master_id, $project_cw_master_update_data)
{
    if (array_key_exists("name", $project_cw_master_update_data)) {
        $name = $project_cw_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("set_no", $project_cw_master_update_data)) {
        $set_no = $project_cw_master_update_data["set_no"];
    } else {
        $set_no = "";
    }

    if (array_key_exists("rate", $project_cw_master_update_data)) {
        $rate = $project_cw_master_update_data["rate"];
    } else {
        $rate = "";
    }

    if (array_key_exists("active", $project_cw_master_update_data)) {
        $active = $project_cw_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_cw_master_update_data)) {
        $remarks = $project_cw_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_cw_master_update_data)) {
        $added_by = $project_cw_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_cw_master_update_data)) {
        $added_on = $project_cw_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_cw_master_update_uquery_base = "update project_cw_master set ";

    $project_cw_master_update_uquery_set = "";

    $project_cw_master_update_uquery_where = " where project_cw_master_id = :cw_master_id";

    $project_cw_master_update_udata = array(":cw_master_id"=>$cw_master_id);

    $filter_count = 0;

    if ($name != "") {
        $project_cw_master_update_uquery_set = $project_cw_master_update_uquery_set." project_cw_master_name = :name,";
        $project_cw_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($set_no != "") {
        $project_cw_master_update_uquery_set = $project_cw_master_update_uquery_set." project_cw_master_set_no = :set_no,";
        $project_cw_master_update_udata[":set_no"] = $set_no;
        $filter_count++;
    }

    if ($rate != "") {
        $project_cw_master_update_uquery_set = $project_cw_master_update_uquery_set." project_cw_master_rate = :rate,";
        $project_cw_master_update_udata[":rate"] = $rate;
        $filter_count++;
    }

    if ($active != "") {
        $project_cw_master_update_uquery_set = $project_cw_master_update_uquery_set." project_cw_master_active = :active,";
        $project_cw_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_cw_master_update_uquery_set = $project_cw_master_update_uquery_set." project_cw_master_remarks = :remarks,";
        $project_cw_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_cw_master_update_uquery_set = $project_cw_master_update_uquery_set." project_cw_master_added_by = :added_by,";
        $project_cw_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_cw_master_update_uquery_set = $project_cw_master_update_uquery_set." project_cw_master_added_on = :added_on,";
        $project_cw_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_cw_master_update_uquery_set = trim($project_cw_master_update_uquery_set, ',');
    }

    $project_cw_master_update_uquery = $project_cw_master_update_uquery_base.$project_cw_master_update_uquery_set.$project_cw_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_cw_master_update_ustatement = $dbConnection->prepare($project_cw_master_update_uquery);

        $project_cw_master_update_ustatement -> execute($project_cw_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $cw_master_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To add new Project UOM Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Unit ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_uom_master($name, $remarks, $added_by)
{
    // Query
    $project_uom_master_iquery = "insert into project_uom_master
	(project_uom_name,project_uom_active,project_uom_remarks,project_uom_added_by,project_uom_added_on) values (:name,:active,:remarks,:added_by,:added_on)";

    try {
        $dbConnection = get_conn_handle();
        $project_uom_master_istatement = $dbConnection->prepare($project_uom_master_iquery);

        // Data
        $project_uom_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

        $dbConnection->beginTransaction();
        $project_uom_master_istatement->execute($project_uom_master_idata);
        $project_uom_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

        $return["status"] = SUCCESS;
        $return["data"]   = $project_uom_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project UOM Master List
INPUT 	: Unit ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project UOM Master
BY 		: Lakshmi
*/
function db_get_project_uom_master($project_uom_master_search_data)
{
    if (array_key_exists("unit_id", $project_uom_master_search_data)) {
        $unit_id = $project_uom_master_search_data["unit_id"];
    } else {
        $unit_id = "";
    }

    if (array_key_exists("name", $project_uom_master_search_data)) {
        $name = $project_uom_master_search_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("unit_name_check", $project_uom_master_search_data)) {
        $unit_name_check = $project_uom_master_search_data["unit_name_check"];
    } else {
        $unit_name_check = "";
    }

    if (array_key_exists("active", $project_uom_master_search_data)) {
        $active = $project_uom_master_search_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("added_by", $project_uom_master_search_data)) {
        $added_by = $project_uom_master_search_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("start_date", $project_uom_master_search_data)) {
        $start_date= $project_uom_master_search_data["start_date"];
    } else {
        $start_date= "";
    }

    if (array_key_exists("end_date", $project_uom_master_search_data)) {
        $end_date= $project_uom_master_search_data["end_date"];
    } else {
        $end_date= "";
    }

    $get_project_uom_master_list_squery_base = "select * from project_uom_master PUM inner join users U on U.user_id = PUM.project_uom_added_by";

    $get_project_uom_master_list_squery_where = "";
    $get_project_uom_master_list_squery_order_by = " order by project_uom_name ASC";
    $filter_count = 0;

    // Data
    $get_project_uom_master_list_sdata = array();

    if ($unit_id != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_id = :unit_id";
        } else {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." and project_uom_id = :unit_id";
        }

        // Data
        $get_project_uom_master_list_sdata[':unit_id'] = $unit_id;

        $filter_count++;
    }

    if ($name != "") {
        if ($filter_count == 0) {
            // Query
            if ($unit_name_check == '1') {
                $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_name = :name";

                // Data
                $get_project_uom_master_list_sdata[':name']  = $name;
            } elseif ($unit_name_check == '2') {
                $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_name like :name";
                // Data
                $get_project_uom_master_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_name like :name";
                // Data
                $get_project_uom_master_list_sdata[':name']  = '%'.$name.'%';
            }
        } else {
            // Query
            if ($unit_name_check == '1') {
                $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." and project_uom_name = :name";

                // Data
                $get_project_uom_master_list_sdata[':name']  = $name;
            } elseif ($unit_name_check == '2') {
                $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." or project_uom_name like :name";

                // Data
                $get_project_uom_master_list_sdata[':name']  = $name.'%';
            } else {
                $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." and project_uom_name like :name";

                // Data
                $get_project_uom_master_list_sdata[':name']  = '%'.$name.'%';
            }
        }

        $filter_count++;
    }

    if ($active != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_active = :active";
        } else {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." and project_uom_active = :active";
        }

        // Data
        $get_project_uom_master_list_sdata[':active']  = $active;

        $filter_count++;
    }

    if ($added_by!= "") {
        if ($filter_count == 0) {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_added_by = :added_by";
        } else {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." and project_uom_added_by = :added_by";
        }

        //Data
        $get_project_uom_master_list_sdata[':added_by']  = $added_by;

        $filter_count++;
    }

    if ($start_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_added_on >= :start_date";
        } else {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." and project_uom_added_on >= :start_date";
        }

        //Data
        $get_project_uom_master_list_sdata[':start_date']  = $start_date;

        $filter_count++;
    }

    if ($end_date != "") {
        if ($filter_count == 0) {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." where project_uom_added_on <= :end_date";
        } else {
            // Query
            $get_project_uom_master_list_squery_where = $get_project_uom_master_list_squery_where." and project_uom_added_on <= :end_date";
        }

        //Data
        $get_project_uom_master_list_sdata['end_date']  = $end_date;

        $filter_count++;
    }

    $get_project_uom_master_list_squery = $get_project_uom_master_list_squery_base.$get_project_uom_master_list_squery_where.$get_project_uom_master_list_squery_order_by;

    try {
        $dbConnection = get_conn_handle();

        $get_project_uom_master_list_sstatement = $dbConnection->prepare($get_project_uom_master_list_squery);

        $get_project_uom_master_list_sstatement -> execute($get_project_uom_master_list_sdata);

        $get_project_uom_master_list_sdetails = $get_project_uom_master_list_sstatement -> fetchAll();

        if (false === $get_project_uom_master_list_sdetails) {
            $return["status"] = FAILURE;
            $return["data"]   = "";
        } elseif (count($get_project_uom_master_list_sdetails) <= 0) {
            $return["status"] = DB_NO_RECORD;
            $return["data"]   = "";
        } else {
            $return["status"] = DB_RECORD_ALREADY_EXISTS;
            $return["data"]   = $get_project_uom_master_list_sdetails;
        }
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Stock UOM Master
INPUT 	: Unit ID, Project Stock UOM Master Update Array
OUTPUT 	: Unit ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_uom_master($unit_id, $project_uom_master_update_data)
{
    if (array_key_exists("name", $project_uom_master_update_data)) {
        $name = $project_uom_master_update_data["name"];
    } else {
        $name = "";
    }

    if (array_key_exists("active", $project_uom_master_update_data)) {
        $active = $project_uom_master_update_data["active"];
    } else {
        $active = "";
    }

    if (array_key_exists("remarks", $project_uom_master_update_data)) {
        $remarks = $project_uom_master_update_data["remarks"];
    } else {
        $remarks = "";
    }

    if (array_key_exists("added_by", $project_uom_master_update_data)) {
        $added_by = $project_uom_master_update_data["added_by"];
    } else {
        $added_by = "";
    }

    if (array_key_exists("added_on", $project_uom_master_update_data)) {
        $added_on = $project_uom_master_update_data["added_on"];
    } else {
        $added_on = "";
    }

    // Query
    $project_uom_master_update_uquery_base = "update project_uom_master set ";

    $project_uom_master_update_uquery_set = "";

    $project_uom_master_update_uquery_where = " where project_uom_id = :unit_id";

    $project_uom_master_update_udata = array(":unit_id"=>$unit_id);

    $filter_count = 0;

    if ($name != "") {
        $project_uom_master_update_uquery_set = $project_uom_master_update_uquery_set." project_uom_name = :name,";
        $project_uom_master_update_udata[":name"] = $name;
        $filter_count++;
    }

    if ($active != "") {
        $project_uom_master_update_uquery_set = $project_uom_master_update_uquery_set." project_uom_active = :active,";
        $project_uom_master_update_udata[":active"] = $active;
        $filter_count++;
    }

    if ($remarks != "") {
        $project_uom_master_update_uquery_set = $project_uom_master_update_uquery_set." project_uom_remarks = :remarks,";
        $project_uom_master_update_udata[":remarks"] = $remarks;
        $filter_count++;
    }

    if ($added_by != "") {
        $project_uom_master_update_uquery_set = $project_uom_master_update_uquery_set." project_uom_added_by = :added_by,";
        $project_uom_master_update_udata[":added_by"] = $added_by;
        $filter_count++;
    }

    if ($added_on != "") {
        $project_uom_master_update_uquery_set = $project_uom_master_update_uquery_set." project_uom_added_on = :added_on,";
        $project_uom_master_update_udata[":added_on"] = $added_on;
        $filter_count++;
    }

    if ($filter_count > 0) {
        $project_uom_master_update_uquery_set = trim($project_uom_master_update_uquery_set, ',');
    }

    $project_uom_master_update_uquery = $project_uom_master_update_uquery_base.$project_uom_master_update_uquery_set.$project_uom_master_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_uom_master_update_ustatement = $dbConnection->prepare($project_uom_master_update_uquery);

        $project_uom_master_update_ustatement -> execute($project_uom_master_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $unit_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To update Project Object Output
INPUT 	: Rework ID, Project Object Output Update Array
OUTPUT 	: Output ID; Message of success or failure
BY 		: Sonakshi D
*/
function db_update_object_uom($process_id, $task_id, $project_object_output_update_data)
{
    if (array_key_exists("uom", $project_object_output_update_data)) {
        $uom = $project_object_output_update_data["uom"];
    } else {
        $uom = "";
    }

    // Query
    $project_object_output_update_uquery_base = "update project_object_output_master set";

    $project_object_output_update_uquery_set = "";

    $project_object_output_update_uquery_where = " where project_object_output_process_id = :process_id AND project_object_output_task_id = :task_id";

    $project_object_rework_update_udata = array(":process_id"=>$process_id,":task_id"=>$task_id);

    $filter_count = 0;


    if ($uom != "") {
        $project_object_output_update_uquery_set = $project_object_output_update_uquery_set." project_object_output_uom = :uom,";
        $project_object_rework_update_udata[":uom"] = $uom;
        $filter_count++;
    }


    if ($filter_count > 0) {
        $project_object_output_update_uquery_set = trim($project_object_output_update_uquery_set, ',');
    }

    $project_machine_rework_update_uquery = $project_object_output_update_uquery_base.$project_object_output_update_uquery_set.$project_object_output_update_uquery_where;

    try {
        $dbConnection = get_conn_handle();

        $project_machine_rework_update_ustatement = $dbConnection->prepare($project_machine_rework_update_uquery);

        $project_machine_rework_update_ustatement -> execute($project_object_rework_update_udata);

        $return["status"] = SUCCESS;
        $return["data"]   = $output_id;
    } catch (PDOException $e) {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"]   = "";
    }

    return $return;
}
