<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/

/*

PURPOSE : To add new stock material master

INPUT 	: Material code, Material name, Material type, Material unit, Material storage, Transportation Mode, Material Manufacturer part no, Material lead time, Material Indicator, Material Price, Material Price As On, Material remarks, Material added by

OUTPUT 	: Stock Material ID, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_material_master($material_code,$material_name,$material_type,$material_unit,$material_storage,$material_transportation_mode,$material_manufacturer_part_number,$material_lead_time,$material_indicator,$material_price,$material_price_as_on,$material_remarks,$material_added_by)

 {

	// Query

    $stock_iquery = "insert into stock_material_master (stock_material_code,stock_material_name,stock_material_type,stock_material_unit_of_measure,stock_material_storage_condition,

	stock_material_transportation_mode,stock_material_manufacturer_part_number,stock_material_lead_time_procurement,stock_material_indicator,

	stock_material_price,stock_material_price_as_on,stock_material_active,stock_material_remarks,stock_material_added_by,stock_material_added_on,

	stock_material_updated_by,stock_material_updated_on) values (:material_code,:material_name,:material_type,:material_unit,:material_storage,

	:material_transportation_mode,:material_manufacturer_part_number,:material_lead_time,:material_indicator,:material_price,

	:material_price_as_on,:material_active,:material_remarks,:material_added_by,:now,:material_updated_by,:now)";



    try

    {

        $dbConnection = get_conn_handle();



        $stock_istatement = $dbConnection->prepare($stock_iquery);

        // Data

        $stock_idata = array(':material_code'=>$material_code,':material_name'=>$material_name,':material_type'=>$material_type,':material_unit'=>$material_unit,':material_storage'=>$material_storage,':material_transportation_mode'=>$material_transportation_mode,':material_manufacturer_part_number'=>$material_manufacturer_part_number,':material_lead_time'=>$material_lead_time,':material_indicator'=>$material_indicator,':material_price'=>$material_price,':material_price_as_on'=>date('Y-m-d'),':material_active'=>'1',':material_remarks'=>$material_remarks,':material_added_by'=>$material_added_by,':now'=>date("Y-m-d H:i:s"),':material_updated_by'=>$material_added_by);



		$dbConnection->beginTransaction();

        $stock_istatement->execute($stock_idata);

		$material_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $material_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get stock material master list

INPUT 	: Material ID, Material code, Material name, Material type, Material unit, Material storage, Material transportation mode, Material manufacturer part no, Material lead time, Material indicator, Material active, Material added by, Start Date(for added on), End Date(for added on), Material Updated By,Order

OUTPUT 	: List of material master

BY 		: Lakshmi

*/

function db_get_stock_material_master_list($stock_material_search_data)

{

    if(array_key_exists("material_id",$stock_material_search_data))

	{

		$material_id = $stock_material_search_data["material_id"];

	}

	else

	{

		$material_id = "";

	}



	if(array_key_exists("material_code",$stock_material_search_data))

	{

		$material_code = $stock_material_search_data["material_code"];

	}

	else

	{

		$material_code = "";

	}



	if(array_key_exists("material_code_check",$stock_material_search_data))

	{

		$material_code_check = $stock_material_search_data["material_code_check"];

	}

	else

	{

		$material_code_check = "";

	}



	if(array_key_exists("material_name",$stock_material_search_data))

	{

		$material_name = $stock_material_search_data["material_name"];

	}

	else

	{

		$material_name = "";

	}



	if(array_key_exists("material_name_check",$stock_material_search_data))

	{

		$material_name_check = $stock_material_search_data["material_name_check"];

	}

	else

	{

		$material_name_check = "";

	}

	if(array_key_exists("material_name_code",$stock_material_search_data))

	{

		$material_name_code = $stock_material_search_data["material_name_code"];

	}

	else

	{

		$material_name_code = "";

	}



	if(array_key_exists("material_type",$stock_material_search_data))

	{

		$material_type = $stock_material_search_data["material_type"];

	}

	else

	{

		$material_type = "";

	}



	if(array_key_exists("material_unit",$stock_material_search_data))

	{

		$material_unit = $stock_material_search_data["material_unit"];

	}

	else

	{

		$material_unit = "";

	}



	if(array_key_exists("material_storage",$stock_material_search_data))

	{

		$material_storage = $stock_material_search_data["material_storage"];

	}

	else

	{

		$material_storage = "";

	}



	if(array_key_exists("material_transportation_mode",$stock_material_search_data))

	{

		$material_transportation_mode = $stock_material_search_data["material_transportation_mode"];

	}

	else

	{

		$material_transportation_mode = "";

	}



	if(array_key_exists("material_manufacturer_part_number",$stock_material_search_data))

	{

		$material_manufacturer_part_number = $stock_material_search_data["material_manufacturer_part_number"];

	}

	else

	{

		$material_manufacturer_part_number = "";

	}



	if(array_key_exists("material_lead_time",$stock_material_search_data))

	{

		$material_lead_time = $stock_material_search_data["material_lead_time"];

	}

	else

	{

		$material_lead_time = "";

	}



	if(array_key_exists("material_indicator",$stock_material_search_data))

	{

		$material_indicator = $stock_material_search_data["material_indicator"];

	}

	else

	{

		$material_indicator = "";

	}



	if(array_key_exists("material_price",$stock_material_search_data))

	{

		$material_price = $stock_material_search_data["material_price"];

	}

	else

	{

		$material_price = "";

	}



	if(array_key_exists("material_price_as_on",$stock_material_search_data))

	{

		$material_price_as_on = $stock_material_search_data["material_price_as_on"];

	}

	else

	{

		$material_price_as_on = "";

	}



	if(array_key_exists("material_active",$stock_material_search_data))

	{

		$material_active = $stock_material_search_data["material_active"];

	}

	else

	{

		$material_active = "";

	}



	if(array_key_exists("material_added_by",$stock_material_search_data))

	{

		$material_added_by = $stock_material_search_data["material_added_by"];

	}

	else

	{

		$material_added_by = "";

	}



	if(array_key_exists("start_date",$stock_material_search_data))

	{

		$start_date = $stock_material_search_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$stock_material_search_data))

	{

		$end_date = $stock_material_search_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	if(array_key_exists("material_updated_by",$stock_material_search_data))

	{

		$material_updated_by = $stock_material_search_data["material_updated_by"];

	}

	else

	{

		$material_updated_by = "";

	}



	if(array_key_exists("order",$stock_material_search_data))

	{

		$order = $stock_material_search_data["order"];

	}

	else

	{

		$order = "";

	}


	$get_stock_material_master_list_squery_base = "select *,AU.user_name as updated_by,U.user_name as added_by from stock_material_master SMM inner join users U on U.user_id = SMM.stock_material_added_by inner join users AU on AU.user_id = SMM.stock_material_updated_by inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id = SMM.stock_material_unit_of_measure inner join stock_transportation_master STM on STM.stock_transportation_id = SMM.stock_material_transportation_mode inner join stock_indicator_master SIM on SIM.stock_indicator_id = SMM.stock_material_indicator inner join stock_storage_condition_master SSCM on SSCM.stock_storage_id = SMM.stock_material_storage_condition";


	$get_stock_material_master_list_squery_where = "";



	$get_stock_material_master_list_squery_order = "";



	$filter_count = 0;



	// Data

	$get_stock_material_list_sdata = array();



	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_id=:material_id";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_id=:material_id";

		}



		// Data

		$get_stock_material_list_sdata[':material_id']  = $material_id;



		$filter_count++;

	}



	if($material_code != "")

	{

		if($filter_count == 0)

		{

			 // Query

			if($material_code_check == '1')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_code = :material_code";

				// Data

				$get_stock_material_list_sdata[':material_code']  = $material_code;

			}

			else if($material_code_check == '2')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_code like :material_code";



				// Data

				$get_stock_material_list_sdata[':material_code']  = $material_code.'%';

			}

			else

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_code like :material_code";



				// Data

				$get_stock_material_list_sdata[':material_code']  = '%'.$material_code.'%';

			}

		}

		else

		{

			// Query

			if($material_code_check == '1')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_code = :material_code";



				// Data

				$get_stock_material_list_sdata[':material_code']  = $material_code;

			}

			else if($material_code_check == '2')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." or stock_material_code like :material_code";

				// Data

				$get_stock_material_list_sdata[':material_code']  = $material_code.'%';

			}

			else

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_code like :material_code";



				// Data

				$get_stock_material_list_sdata[':material_code']  = '%'.$material_code.'%';

			}

		}



		$filter_count++;

	}

	if($material_name_code != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where (stock_material_name like :material_name_code or stock_material_code like :material_name_code)";
		}
		else
		{
			// Query
			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and (stock_material_name like :material_name_code or stock_material_code like :material_name_code)";
		}

		// Data
		$get_stock_material_list_sdata[':material_name_code']  = $material_name_code.'%';

		$filter_count++;
	}



	if($material_name != "")

	{

		if($filter_count == 0)

		{

		    // Query

			if($material_name_check == '1')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_name = :material_name";



				// Data

				$get_stock_material_list_sdata[':material_name']  = $material_name;

			}

			else if($material_name_check == '2')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_name like :material_name";



				// Data

				$get_stock_material_list_sdata[':material_name']  = $material_name.'%';

			}

			else

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_name like :material_name";



				// Data

				$get_stock_material_list_sdata[':material_name']  = '%'.$material_name.'%';

			}

		}

		else

		{

			// Query

			if($material_name_check == '1')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." or stock_material_name = :material_name";



				// Data

				$get_stock_material_list_sdata[':material_name']  = $material_name;

			}

			else if($material_name_check == '2')

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." or stock_material_name like :material_name";



				// Data

				$get_stock_material_list_sdata[':material_name']  = $material_name.'%';

			}

			else

			{

				$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_name like :material_name";



				// Data

				$get_stock_material_list_sdata[':material_name']  = '%'.$material_name.'%';

			}

		}



		$filter_count++;

	}



	if($material_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_type = :material_type";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_type = :material_type";

		}



		// Data

		$get_stock_material_list_sdata[':material_type']  = $material_type;



		$filter_count++;

	}



	if($material_unit != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_unit_of_measure = :material_unit";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_unit_of_measure = :material_unit";

		}



		// Data

		$get_stock_material_list_sdata[':material_unit'] = $material_unit;



		$filter_count++;

	}



	if($material_storage != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_storage_condition = :material_storage";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_storage_condition = :material_storage";

		}



		// Data

		$get_stock_material_list_sdata[':material_storage']  = $material_storage;



		$filter_count++;

	}



	if($material_transportation_mode != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_transportation_mode = :material_transportation_mode";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_transportation_mode = :material_transportation_mode";

		}



		// Data

		$get_stock_material_list_sdata[':material_transportation_mode']  = $material_transportation_mode;



		$filter_count++;

	}



	if($material_manufacturer_part_number != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_manufacturer_part_number = :material_manufacturer_part_number";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_manufacturer_part_number = :material_manufacturer_part_number";

		}



		// Data

		$get_stock_material_list_sdata[':material_manufacturer_part_number']  = $material_manufacturer_part_number;



		$filter_count++;

	}



	if($material_lead_time != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_lead_time_procurement = :material_lead_time";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_lead_time_procurement = :material_lead_time";

		}



		// Data

		$get_stock_material_list_sdata[':material_lead_time']  = $material_lead_time;



		$filter_count++;

	}



	if($material_indicator != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." stock_material_indicator = :material_indicator";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_indicator = :material_indicator";

		}



		// Data

		$get_stock_material_list_sdata[':material_indicator']  = $material_indicator;



		$filter_count++;

	}



	if($material_price != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_price = :material_price";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_price = :material_price";

		}



		// Data

		$get_stock_material_list_sdata[':material_price']  = $material_price;



		$filter_count++;

	}



	if($material_price_as_on != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_price_as_on = :material_price_as_on";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_price_as_on = :material_price_as_on";

		}



		// Data

		$get_stock_material_list_sdata[':material_price_as_on']  = $material_price_as_on;



		$filter_count++;

	}





	if($material_active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_active = :material_active";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_active = :material_active";

		}



		// Data

		$get_stock_material_list_sdata[':material_active']  = $material_active;



		$filter_count++;

	}



	if($material_added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_added_by = :material_added_by";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_added_by = :material_added_by";

		}



		// Data

		$get_stock_material_list_sdata[':material_added_by']  = $material_added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_added_on >= :start_date";

		}



		//Data

		$get_stock_material_list_sdata[':material_added_on']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_added_on <= :end_date";

		}



		//Data

		$get_stock_material_list_sdata[':material_added_on']  = $end_date;



		$filter_count++;

	}



	if($material_updated_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." where stock_material_updated_by = :material_updated_by";

		}

		else

		{

			// Query

			$get_stock_material_master_list_squery_where = $get_stock_material_master_list_squery_where." and stock_material_updated_by = :material_updated_by";

		}



		// Data

		$get_stock_material_list_sdata[':material_updated_by']  = $material_updated_by;



		$filter_count++;

	}



	if($order == 'name_asc')

	{

		$get_stock_material_master_list_squery_order = $get_stock_material_master_list_squery_order.' order by stock_material_name asc';

	}



	$get_stock_material_master_list_squery_order_by = " order by stock_material_added_on DESC";



	$get_stock_material_master_list_squery = $get_stock_material_master_list_squery_base.$get_stock_material_master_list_squery_where.$get_stock_material_master_list_squery_order.$get_stock_material_master_list_squery_order_by;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_material_list_sstatement = $dbConnection->prepare($get_stock_material_master_list_squery);



		$get_stock_material_list_sstatement -> execute($get_stock_material_list_sdata);



		$get_stock_material_list_sdetails = $get_stock_material_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_material_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_material_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_material_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Material Master

INPUT 	: Material ID, Material Master update Array

OUTPUT 	: Material ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_material_list($material_id,$material_master_update_data)

{

	if(array_key_exists("material_code",$material_master_update_data))

	{

		$material_code = $material_master_update_data["material_code"];

	}

	else

	{

		$material_code = "";

	}



	if(array_key_exists("material_name",$material_master_update_data))

	{

		$material_name = $material_master_update_data["material_name"];

	}

	else

	{

		$material_name = "";

	}



	if(array_key_exists("material_type",$material_master_update_data))

	{

		$material_type = $material_master_update_data["material_type"];

	}

	else

	{

		$material_type = "";

	}



	if(array_key_exists("material_unit",$material_master_update_data))

	{

		$material_unit = $material_master_update_data["material_unit"];

	}

	else

	{

		$material_unit = "";

	}



	if(array_key_exists("material_storage",$material_master_update_data))

	{

		$material_storage = $material_master_update_data["material_storage"];

	}

	else

	{

		$material_storage = "";

	}



	if(array_key_exists("material_transportation_mode",$material_master_update_data))

	{

		$material_transportation_mode = $material_master_update_data["material_transportation_mode"];

	}

	else

	{

		$material_transportation_mode = "";

	}



	if(array_key_exists("material_manufacturer_part_number",$material_master_update_data))

	{

		$material_manufacturer_part_number = $material_master_update_data["material_manufacturer_part_number"];

	}

	else

	{

		$material_manufacturer_part_number = "";

	}



	if(array_key_exists("material_lead_time",$material_master_update_data))

	{

		$material_lead_time = $material_master_update_data["material_lead_time"];

	}

	else

	{

		$material_lead_time = "";

	}



	if(array_key_exists("material_indicator",$material_master_update_data))

	{

		$material_indicator = $material_master_update_data["material_indicator"];

	}

	else

	{

		$material_indicator = "";

	}



	if(array_key_exists("material_price",$material_master_update_data))

	{

		$material_price = $material_master_update_data["material_price"];

	}

	else

	{

		$material_price = "";

	}



	if(array_key_exists("material_price_as_on",$material_master_update_data))

	{

		$material_price_as_on = $material_master_update_data["material_price_as_on"];

	}

	else

	{

		$material_price_as_on = "";

	}



	if(array_key_exists("material_active",$material_master_update_data))

	{

		$material_active = $material_master_update_data["material_active"];

	}

	else

	{

		$material_active = "";

	}



	if(array_key_exists("material_remarks",$material_master_update_data))

	{

		$material_remarks = $material_master_update_data["material_remarks"];

	}

	else

	{

		$material_remarks = "";

	}



	if(array_key_exists("material_added_by",$material_master_update_data))

	{

		$material_added_by = $material_master_update_data["material_added_by"];

	}

	else

	{

		$material_added_by = "";

	}



	$material_added_on = date('Y-m-d H:i:s');



	if(array_key_exists("material_updated_by",$material_master_update_data))

	{

		$material_updated_by = $material_master_update_data["material_updated_by"];

	}

	else

	{

		$material_updated_by = "";

	}



	$material_updated_on = date('Y-m-d H:i:s');





	// Query

    $material_master_update_uquery_base = "update stock_material_master set";



	$material_master_update_uquery_set = "";



	$material_master_update_uquery_where = " where stock_material_id = :material_id";



	$material_master_update_udata = array(":material_id"=>$material_id);



	$filter_count = 0;



	if($material_code != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_code = :material_code,";

		$material_master_update_udata[":material_code"] = $material_code;

		$filter_count++;

	}

	if($material_name != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_name = :material_name,";

		$material_master_update_udata[":material_name"] = $material_name;

		$filter_count++;

	}

	if($material_type != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_type = :material_type,";

		$material_master_update_udata[":material_type"] = $material_type;

		$filter_count++;

	}

	if($material_unit != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_unit_of_measure = :material_unit,";

		$material_master_update_udata[":material_unit"] = $material_unit;

		$filter_count++;

	}

	if($material_storage != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_storage_condition = :material_storage,";

		$material_master_update_udata[":material_storage"] = $material_storage;

		$filter_count++;

	}

	if($material_transportation_mode != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_transportation_mode = :material_transportation_mode,";

		$material_master_update_udata[":material_transportation_mode"] = $material_transportation_mode;

		$filter_count++;

	}

	if($material_manufacturer_part_number != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set."stock_material_manufacturer_part_number = :material_manufacturer_part_number,";

		$material_master_update_udata[":material_manufacturer_part_number"] = $material_manufacturer_part_number;

		$filter_count++;

	}

	if($material_lead_time != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_lead_time_procurement = :material_lead_time,";

		$material_master_update_udata[":material_lead_time"] = $material_lead_time;

		$filter_count++;

	}

	if($material_indicator != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_indicator = :material_indicator,";

		$material_master_update_udata[":material_indicator"] = $material_indicator;

		$filter_count++;

	}

	if($material_price != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_price = :material_price,";

		$material_master_update_udata[":material_price"] = $material_price;

		$filter_count++;

	}

	if($material_price_as_on != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_price_as_on = :material_price_as_on,";

		$material_master_update_udata[":material_price_as_on"] = $material_price_as_on;

		$filter_count++;

	}

	if($material_active != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_active = :material_active,";

		$material_master_update_udata[":material_active"] = $material_active;

		$filter_count++;

	}

	if($material_remarks != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_remarks = :material_remarks,";

		$material_master_update_udata[":material_remarks"] = $material_remarks;

		$filter_count++;

	}



	if($material_added_by != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_added_by = :material_added_by,";

		$material_master_update_udata[":material_added_by"] = $material_added_by;

		$filter_count++;

	}

	if($material_added_on != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_added_on = :material_added_on,";

		$material_master_update_udata[":material_added_on"] = $material_added_on;

		$filter_count++;

	}

	if($material_updated_by != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_updated_by = :material_updated_by,";

		$material_master_update_udata[":material_updated_by"] = $material_updated_by;

		$filter_count++;

	}

	if($material_updated_on != "")

	{

		$material_master_update_uquery_set = $material_master_update_uquery_set." stock_material_updated_on = :material_updated_on,";

		$material_master_update_udata[":material_updated_on"] = $material_updated_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$material_master_update_uquery_set = trim($material_master_update_uquery_set,',');

	}



	$material_master_update_uquery = $material_master_update_uquery_base.$material_master_update_uquery_set.$material_master_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $material_master_update_ustatement = $dbConnection->prepare($material_master_update_uquery);



        $material_master_update_ustatement -> execute($material_master_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $material_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Transportation

INPUT 	: Transportation Name, Remarks, Added by

OUTPUT 	: Transportation Id, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_transportation($transportation_name,$remarks,$added_by)

{

	// Query

    $stock_transportation_iquery = "insert into stock_transportation_master (stock_transportation_name,stock_transportation_active,stock_transportation_remarks,stock_transportation_added_by,stock_transportation_added_on) values (:transportation_name,:active,:remarks,:added_by,:added_on)";



    try

    {

        $dbConnection = get_conn_handle();

        $stock_transportation_istatement = $dbConnection->prepare($stock_transportation_iquery);



        // Data

        $stock_transportation_idata = array(':transportation_name'=>$transportation_name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_transportation_istatement->execute($stock_transportation_idata);

		$stock_transportation_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_transportation_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Transportation List

INPUT 	: Transportation Filter Data: Transportation ID, Transportation Name, Name Check, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Transportation Modes

BY 		: Lakshmi

*/

function db_get_stock_transportation_list($transportation_search_data)

{

	if(array_key_exists("transportation_id",$transportation_search_data))

	{

		$transportation_id = $transportation_search_data["transportation_id"];

	}

	else

	{

		$transportation_id = "";

	}



	if(array_key_exists("transportation_name",$transportation_search_data))

	{

		$transportation_name = $transportation_search_data["transportation_name"];

	}

	else

	{

		$transportation_name = "";

	}



	if(array_key_exists("stock_transportation_name_check",$transportation_search_data))

	{

		$transportation_name_check = $transportation_search_data["stock_transportation_name_check"];

	}

	else

	{

		$transportation_name_check = "";

	}



	if(array_key_exists("active",$transportation_search_data))

	{

		$active= $transportation_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("added_by",$transportation_search_data))

	{

		$added_by= $transportation_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("start_date",$transportation_search_data))

	{

		$start_date= $transportation_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}



	if(array_key_exists("end_date",$transportation_search_data))

	{

		$end_date= $transportation_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	if(array_key_exists("order",$transportation_search_data))

	{

		$order = $transportation_search_data["order"];

	}

	else

	{

		$order = "";

	}



	$get_stock_transportation_list_squery_base = "select * from stock_transportation_master STM inner join users U on U.user_id = STM.stock_transportation_added_by";



	$get_stock_transportation_list_squery_where = "";



	$get_stock_transportation_list_squery_order = "";



	$filter_count = 0;



	// Data

	$get_stock_transportation_list_sdata = array();



	if($transportation_id != "")

	{

		if($filter_count == 0)

		{

		   // Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." where stock_transportation_id = :transportation_id";

		}

		else

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." and stock_transportation_id = :transportation_id";

		}



		// Data

		$get_stock_transportation_list_sdata[':transportation_id']  = $transportation_id;



		$filter_count++;

	}



	if($transportation_name != "")

	{

		if($filter_count == 0)

		{

		    // Query

			if($transportation_name_check == '1')

			{

				$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." where stock_transportation_name = :transportation_name";



				// Data

				$get_stock_transportation_list_sdata[':transportation_name']  = $transportation_name;

			}

			else

			{

				$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." where stock_transportation_name like :transportation_name";



				// Data

				$get_stock_transportation_list_sdata[':transportation_name']  = '%'.$transportation_name.'%';

			}

		}

		else

		{

			// Query

			if($transportation_name_check == '1')

			{

				$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." and stock_transportation_name = :transportation_name";



				// Data

				$get_stock_transportation_list_sdata[':transportation_name']  = $transportation_name;

			}

			else

			{

				$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." and stock_transportation_name like :transportation_name";



				// Data

				$get_stock_transportation_list_sdata[':transportation_name']  = '%'.$transportation_name.'%';

			}

		}



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." where stock_transportation_active = :active";

		}

		else

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." and stock_transportation_active = :active";

		}



		// Data

		$get_stock_transportation_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." where stock_transportation_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." and stock_transportation_added_by = :added_by";

		}



		// Data

		$get_stock_transportation_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." where stock_transportation_added_on >= :start_date ";



		}

		else

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." and stock_transportation_added_on >= :start_date";

		}



		//Data

		$get_stock_transportation_list_sdata[':start_date']  = $start_date ;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." where stock_transportation_added_by <= :end_date";

		}

		else

		{

			// Query

			$get_stock_transportation_list_squery_where = $get_stock_transportation_list_squery_where." and stock_transportation_added_by <= :end_date";

		}



		// Data

		$get_stock_transportation_list_sdata[':end_date']  = $end_date;



		$filter_count++;

	}



	$get_stock_transportation_list_squery_order = ' order by stock_transportation_name asc';



	$get_stock_transportation_list_squery = $get_stock_transportation_list_squery_base.$get_stock_transportation_list_squery_where.$get_stock_transportation_list_squery_order;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_transportation_list_sstatement = $dbConnection->prepare($get_stock_transportation_list_squery);



		$get_stock_transportation_list_sstatement -> execute($get_stock_transportation_list_sdata);



		$get_stock_transportation_list_sdetails = $get_stock_transportation_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_transportation_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_transportation_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_transportation_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Transportation mode details

INPUT 	: Transportation ID, Transportation Update Array

OUTPUT 	: Transportation ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_transportation_list($transportation_id,$transportation_update_data)

{

	if(array_key_exists("transportation_name",$transportation_update_data))

	{

		$transportation_name = $transportation_update_data["transportation_name"];

	}

	else

	{

		$transportation_name = "";

	}



	if(array_key_exists("active",$transportation_update_data))

	{

		$active = $transportation_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$transportation_update_data))

	{

		$remarks = $transportation_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$transportation_update_data))

	{

		$added_by = $transportation_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $transportation_update_uquery_base = "update stock_transportation_master set";



	$transportation_update_uquery_set = "";



	$transportation_update_uquery_where = " where stock_transportation_id = :transportation_id";



	$transportation_update_udata = array(":transportation_id"=>$transportation_id);



	$filter_count = 0;



	if($transportation_name != "")

	{

		$transportation_update_uquery_set = $transportation_update_uquery_set." stock_transportation_name = :transportation_name,";

		$transportation_update_udata[":transportation_name"] = $transportation_name;

		$filter_count++;

	}



	if($active != "")

	{

		$transportation_update_uquery_set = $transportation_update_uquery_set." stock_transportation_active = :active,";

		$transportation_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$transportation_update_uquery_set = $transportation_update_uquery_set." stock_transportation_remarks = :remarks,";

		$transportation_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$transportation_update_uquery_set = $transportation_update_uquery_set." stock_transportation_added_by = :added_by,";

		$transportation_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$transportation_update_uquery_set = $transportation_update_uquery_set." stock_transportation_added_on = :added_on,";

		$transportation_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$transportation_update_uquery_set = trim($transportation_update_uquery_set,',');

	}



	$transportation_update_uquery = $transportation_update_uquery_base.$transportation_update_uquery_set.$transportation_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $transportation_update_ustatement = $dbConnection->prepare($transportation_update_uquery);



        $transportation_update_ustatement -> execute($transportation_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $transportation_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Indicator

INPUT 	: Indicator Name, Active, Remarks, Added by

OUTPUT 	: Indicator id, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_indicator($indicator_name,$remarks,$added_by)

{

	// Query

    $stock_indicator_iquery = "insert into stock_indicator_master

	(stock_indicator_name,stock_indicator_active,stock_indicator_remarks,stock_indicator_added_by,stock_indicator_added_on) values (:indicator_name,:active,:remarks,:added_by,:added_on)";



    try

    {

        $dbConnection = get_conn_handle();

        $stock_indicator_istatement = $dbConnection->prepare($stock_indicator_iquery);



        // Data

        $stock_indicator_idata = array(':indicator_name'=>$indicator_name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,

		':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_indicator_istatement->execute($stock_indicator_idata);

		$stock_indicator_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_indicator_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Indicator List

INPUT 	: Indicator ID, Indicator Name, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Indicators

BY 		: Lakshmi

*/

function db_get_stock_indicator_list($stock_indicator_search_data)

{

	// Extract all input parameters

    if(array_key_exists("indicator_id",$stock_indicator_search_data))

	{

		$indicator_id = $stock_indicator_search_data["indicator_id"];

	}

	else

	{

		$indicator_id = "";

	}



	if(array_key_exists("indicator_name",$stock_indicator_search_data))

	{

		$indicator_name = $stock_indicator_search_data["indicator_name"];

	}

	else

	{

		$indicator_name = "";

	}



	if(array_key_exists("indicator_name_check",$stock_indicator_search_data))

	{

		$indicator_name_check = $stock_indicator_search_data["indicator_name_check"];

	}

	else

	{

		$indicator_name_check = "";

	}



	if(array_key_exists("active",$stock_indicator_search_data))

	{

		$active= $stock_indicator_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("added_by",$stock_indicator_search_data))

	{

		$added_by= $stock_indicator_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("start_date",$stock_indicator_search_data))

	{

		$start_date= $stock_indicator_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}



	if(array_key_exists("end_date",$stock_indicator_search_data))

	{

		$end_date= $stock_indicator_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	if(array_key_exists("order",$stock_indicator_search_data))

	{

		$order = $stock_indicator_search_data["order"];

	}

	else

	{

		$order = "";

	}



	$get_stock_indicator_list_squery_base = "select * from stock_indicator_master SIM inner join users U on U.user_id = SIM.stock_indicator_added_by";



	$get_stock_indicator_list_squery_where = "";



	$get_stock_indicator_list_squery_order = "";



	$filter_count = 0;



	// Data

	$get_stock_indicator_list_sdata = array();



	if($indicator_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." where stock_indicator_id = :indicator_id";

		}

		else

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." and stock_indicator_id = :indicator_id";

		}



		// Data

		$get_stock_indicator_list_sdata[':indicator_id']  = $indicator_id;



		$filter_count++;

	}



	if($indicator_name != "")

	{

		if($filter_count == 0)

		{

			if($indicator_name_check == '1')

			{

				// Query

				$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." where stock_indicator_name = :indicator_name";



				// Data

				$get_stock_indicator_list_sdata[':indicator_name']  = $indicator_name;

			}

			else

			{

				// Query

				$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." where stock_indicator_name like :indicator_name";



				// Data

				$get_stock_indicator_list_sdata[':indicator_name']  = '%'.$indicator_name.'%';

			}

		}

		else

		{

			if($indicator_name_check == '1')

			{

				// Query

				$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." and stock_indicator_name = :indicator_name";



				// Data

				$get_stock_indicator_list_sdata[':indicator_name']  = $indicator_name;

			}

			else

			{

				// Query

				$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." and stock_indicator_name like :indicator_name";



				// Data

				$get_stock_indicator_list_sdata[':indicator_name']  = '%'.$indicator_name.'%';

			}

		}



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." where stock_indicator_active = :active";

		}

		else

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." and stock_indicator_active = :active";

		}



		// Data

		$get_stock_indicator_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($start_date!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." where stock_indicator_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." and stock_indicator_added_on >= :start_date";

		}



		//Data

		$get_stock_indicator_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." where stock_indicator_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." and stock_indicator_added_on <= :end_date";

		}



		//Data

		$get_stock_indicator_list_sdata[':end_date']  = $end_date;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." where stock_indicator_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_indicator_list_squery_where = $get_stock_indicator_list_squery_where." and stock_indicator_added_by = :added_by";

		}



		// Data

		$get_stock_indicator_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($order == 'name_asc')

	{

		$get_stock_indicator_list_squery_order = ' order by stock_indicator_name asc';

	}



	$get_stock_indicator_list_squery = $get_stock_indicator_list_squery_base.$get_stock_indicator_list_squery_where.$get_stock_indicator_list_squery_order;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_indicator_list_sstatement = $dbConnection->prepare($get_stock_indicator_list_squery);



		$get_stock_indicator_list_sstatement -> execute($get_stock_indicator_list_sdata);



		$get_stock_indicator_list_sdetails = $get_stock_indicator_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_indicator_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_indicator_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_indicator_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Indicator

INPUT 	: Indicator ID, Indicator Update Array

OUTPUT 	: Indicator ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_indicator_list($indicator_id,$indicator_update_data)

{

	if(array_key_exists("indicator_name",$indicator_update_data))

	{

		$indicator_name = $indicator_update_data["indicator_name"];

	}

	else

	{

		$indicator_name = "";

	}



	if(array_key_exists("active",$indicator_update_data))

	{

		$active = $indicator_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$indicator_update_data))

	{

		$remarks = $indicator_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$indicator_update_data))

	{

		$added_by = $indicator_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $indicator_update_uquery_base = "update stock_indicator_master set";



	$indicator_update_uquery_set = "";



	$indicator_update_uquery_where = " where stock_indicator_id = :indicator_id";



	$indicator_update_udata = array(":indicator_id"=>$indicator_id);



	$filter_count = 0;



	if($indicator_name != "")

	{

		$indicator_update_uquery_set = $indicator_update_uquery_set." stock_indicator_name = :indicator_name,";

		$indicator_update_udata[":indicator_name"] = $indicator_name;

		$filter_count++;

	}



	if($active != "")

	{

		$indicator_update_uquery_set = $indicator_update_uquery_set." stock_indicator_active = :active,";

		$indicator_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$indicator_update_uquery_set = $indicator_update_uquery_set." stock_indicator_remarks = :remarks,";

		$indicator_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$indicator_update_uquery_set = $indicator_update_uquery_set." stock_indicator_added_by = :added_by,";

		$indicator_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$indicator_update_uquery_set = $indicator_update_uquery_set." stock_indicator_added_on = :added_on,";

		$indicator_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$indicator_update_uquery_set = trim($indicator_update_uquery_set,',');

	}



	$indicator_update_uquery = $indicator_update_uquery_base.$indicator_update_uquery_set.$indicator_update_uquery_where;

    try

    {

        $dbConnection = get_conn_handle();



        $indicator_update_ustatement = $dbConnection->prepare($indicator_update_uquery);



        $indicator_update_ustatement -> execute($indicator_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $indicator_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Tax Type

INPUT 	: Vendor Id,Tax Type,Tax Type Details,Remarks,Added By,Added On

OUTPUT 	: Tax Type id, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_tax_type($vendor_id,$tax_type,$tax_type_details,$remarks,$added_by)

{

	// Query

    $stock_tax_type_iquery = "insert into stock_tax_type (stock_tax_vendor_id,stock_tax_type,stock_tax_type_details,stock_tax_remarks,stock_tax_added_by,stock_tax_added_on)

     values (:vendor_id,:tax_type,:tax_type_details,:remarks,:added_by,:added_on)";



    try

    {

        $dbConnection = get_conn_handle();

        $stock_tax_type_istatement = $dbConnection->prepare($stock_tax_type_iquery);



        // Data

		$stock_tax_type_idata = array(':vendor_id'=>$vendor_id,':tax_type'=>$tax_type,':tax_type_details'=>$tax_type_details,':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_tax_type_istatement->execute($stock_tax_type_idata);

		$stock_tax_type_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_tax_type_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Tax Type list

INPUT 	: Tax Type ID, Vendor ID, Tax Type, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Tax Type for a vendor

BY 		: Lakshmi

*/

function db_get_stock_tax_type_list($stock_tax_type_search_data)

{

	if(array_key_exists("tax_type_id",$stock_tax_type_search_data))

	{

		$tax_type_id = $stock_tax_type_search_data["tax_type_id"];

	}

	else

	{

		$tax_type_id = "";

	}



	if(array_key_exists("vendor_id",$stock_tax_type_search_data))

	{

		$vendor_id= $stock_tax_type_search_data["vendor_id"];

	}

	else

	{

		$vendor_id = "";

	}



	if(array_key_exists("tax_type",$stock_tax_type_search_data))

	{

		$tax_type= $stock_tax_type_search_data["tax_type"];

	}

	else

	{

		$tax_type = "";

	}



	if(array_key_exists("active",$stock_tax_type_search_data))

	{

		$active = $stock_tax_type_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("tax_type_details",$stock_tax_type_search_data))

	{

		$tax_type_details= $stock_tax_type_search_data["tax_type_details"];

	}

	else

	{

		$tax_type_details= "";

	}



	if(array_key_exists("added_by",$stock_tax_type_search_data))

	{

		$added_by= $stock_tax_type_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}

	if(array_key_exists("start_date",$stock_tax_type_search_data))

	{

		$start_date= $stock_tax_type_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_tax_type_search_data))

	{

		$end_date= $stock_tax_type_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	$get_stock_tax_type_list_squery_base = "select * from stock_tax_type";



	$get_stock_tax_type_list_squery_where = "";



	$filter_count = 0;



	// Data

	$get_stock_tax_type_list_sdata = array();



	if($tax_type_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_type_id=:tax_type_id";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_type_id=:tax_type_id";

		}



		// Data

		$get_stock_tax_type_list_sdata[':tax_type_id'] = $tax_type_id;



		$filter_count++;

	}

	if($vendor_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_vendor_id=:vendor_id";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_vendor_id=:vendor_id";

		}



		// Data

		$get_stock_tax_type_list_sdata[':vendor_id']  = $vendor_id;



		$filter_count++;

	}

	if($tax_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_type=:tax_type";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_type=:tax_type";

		}



		// Data

		$get_stock_tax_type_list_sdata[':tax_type']  = $tax_type;



		$filter_count++;

	}

	if($tax_type_details != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_type_details=:tax_type_details";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_type_details=:tax_type_details";

		}



		// Data

		$get_stock_tax_type_list_sdata[':tax_type_details']  = $tax_type_details;



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_type_master_active = :active";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_type_master_active = :active";

		}



		//Data

		$get_stock_tax_type_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_added_by = :added_by";

		}



		//Data

		$get_stock_tax_type_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_added_on >= :start_date";

		}



		//Data

		$get_stock_tax_type_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." where stock_tax_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_tax_type_list_squery_where = $get_stock_tax_type_list_squery_where." and stock_tax_added_on <= :end_date";

		}



		//Data

		$get_stock_tax_type_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}



	$get_stock_tax_type_list_squery = $get_stock_tax_type_list_squery_base.$get_stock_tax_type_list_squery_where;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_tax_type_list_sstatement = $dbConnection->prepare($get_stock_tax_type_list_squery);



		$get_stock_tax_type_list_sstatement -> execute($get_stock_tax_type_list_sdata);



		$get_stock_tax_type_list_sdetails = $get_stock_tax_type_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_tax_type_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_tax_type_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_tax_type_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

 }



 	  /*

PURPOSE : To update Tax Type

INPUT 	: Tax Type ID, Tax Type Update Array

OUTPUT 	: Tax Type ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_tax_type($tax_type_id,$tax_type_update_data)

{

	if(array_key_exists("vendor_id",$tax_type_update_data))

	{

		$vendor_id = $tax_type_update_data["vendor_id"];

	}

	else

	{

		$vendor_id = "";

	}



	if(array_key_exists("tax_type",$tax_type_update_data))

	{

		$tax_type = $tax_type_update_data["tax_type"];

	}

	else

	{

		$tax_type = "";

	}

	if(array_key_exists("tax_type_details",$tax_type_update_data))

	{

		$tax_type_details = $tax_type_update_data["tax_type_details"];

	}

	else

	{

		$tax_type_details = "";

	}



	if(array_key_exists("added_by",$tax_type_update_data))

	{

		$added_by = $tax_type_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $tax_type_update_uquery_base = "update stock_tax_type set";



	$tax_type_update_uquery_set = "";



	$tax_type_update_uquery_where = " where stock_tax_type_id=:tax_type_id";



	$tax_type_update_udata = array(":tax_type_id"=>$tax_type_id);



	$filter_count = 0;



	if($vendor_id != "")

	{

		$tax_type_update_uquery_set = $tax_type_update_uquery_set." stock_tax_vendor_id=:vendor_id,";

		$tax_type_update_udata[":vendor_id"] = $vendor_id;

		$filter_count++;

	}



	if($tax_type != "")

	{

		$tax_type_update_uquery_set = $tax_type_update_uquery_set." stock_tax_type=:tax_type,";

		$tax_type_update_udata[":tax_type"] = $tax_type;

		$filter_count++;

	}

	if($tax_type_details != "")

	{

		$tax_type_update_uquery_set = $tax_type_update_uquery_set." stock_tax_type_details=:tax_type_details,";

		$tax_type_update_udata[":tax_type_details"] = $tax_type_details;

		$filter_count++;

	}



	if($added_by != "")

	{

		$tax_type_update_uquery_set = $tax_type_update_uquery_set." stock_tax_added_by=:added_by,";

		$tax_type_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}

	if($added_on != "")

	{

		$tax_type_update_uquery_set = $tax_type_update_uquery_set." stock_tax_added_on=:added_on,";

		$tax_type_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$tax_type_update_uquery_set = trim($tax_type_update_uquery_set,',');

	}



	$tax_type_update_uquery = $tax_type_update_uquery_base.$tax_type_update_uquery_set.$tax_type_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $tax_type_update_ustatement = $dbConnection->prepare($tax_type_update_uquery);



        $tax_type_update_ustatement -> execute($tax_type_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $tax_type_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Project

INPUT 	: Project Name, Project Active, Project Remarks, Added by

OUTPUT 	: Project ID, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_project($project_name,$remarks,$added_by)

{

	// Query

    $stock_project_iquery = "insert into stock_project (stock_project_name,stock_project_active,stock_project_remarks,stock_project_added_by,stock_project_added_on)

    values (:project_name,:active,:remarks,:added_by,:added_on)";



    try

    {

        $dbConnection = get_conn_handle();

        $stock_project_istatement = $dbConnection->prepare($stock_project_iquery);



        // Data

        $stock_project_idata = array(':project_name'=>$project_name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,

		':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_project_istatement->execute($stock_project_idata);

		$stock_project_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_project_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Project List

INPUT 	: Project Id, Project Name, Active, Added by, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of stock projects

BY 		: Lakshmi

*/

function db_get_stock_project_list($stock_project_search_data)

{

	// Extract all input parameters

	if(array_key_exists("project_id",$stock_project_search_data))

	{

		$project_id = $stock_project_search_data["project_id"];

	}

	else

	{

		$project_id = "";

	}



	if(array_key_exists("project_name",$stock_project_search_data))

	{

		$project_name = $stock_project_search_data["project_name"];

	}

	else

	{

		$project_name = "";

	}



	if(array_key_exists("project_name_check",$stock_project_search_data))

	{

		$project_name_check = $stock_project_search_data["project_name_check"];

	}

	else

	{

		$project_name_check = "";

	}



	if(array_key_exists("active",$stock_project_search_data))

	{

		$active = $stock_project_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("added_by",$stock_project_search_data))

	{

		$added_by = $stock_project_search_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	if(array_key_exists("start_date",$stock_project_search_data))

	{

		$start_date = $stock_indent_project_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$stock_project_search_data))

	{

		$end_date = $stock_indent_project_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	if(array_key_exists("order",$stock_project_search_data))

	{

		$order = $stock_indent_project_data["order"];

	}

	else

	{

		$order = "name_asc";

	}



	$get_stock_project_list_squery_base = "select * from stock_project SP inner join users U on U.user_id= SP.stock_project_added_by";



	$get_stock_project_list_squery_where = "";



	$get_stock_project_list_squery_order = "";



	$filter_count = 0;



	// Data

	$get_stock_project_list_sdata = array();



	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." where stock_project_id = :project_id";

		}

		else

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." and stock_project_id = :project_id";

		}



		// Data

		$get_stock_project_list_sdata[':project_id'] = $project_id;



		$filter_count++;

	}



	if($project_name != "")

	{

		if($filter_count == 0)

		{

			if($project_name_check == '1')

			{

				// Query

				$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." where stock_project_name = :project_name";



				// Data

				$get_stock_project_list_sdata[':project_name']  = $project_name;

			}

			else

			{

				// Query

				$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." where stock_project_name like :project_name";



				// Data

				$get_stock_project_list_sdata[':project_name']  = '%'.$project_name.'%';

			}

		}

		else

		{

			if($project_name_check == '1')

			{

				// Query

				$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." and stock_project_name = :project_name";



				// Data

				$get_stock_project_list_sdata[':project_name']  = $project_name;

			}

			else

			{

				// Query

				$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." and stock_project_name like :project_name";



				// Data

				$get_stock_project_list_sdata[':project_name']  = '%'.$project_name.'%';

			}

		}



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." where stock_project_active = :active";

		}

		else

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." and stock_project_active = :active";

		}



		// Data

		$get_stock_project_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." where stock_project_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." and stock_project_added_by = :added_by";

		}



		//Data

		$get_stock_project_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." where stock_project_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." and stock_project_added_on >= :start_date";

		}



		//Data

		$get_stock_project_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." where stock_project_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_project_list_squery_where = $get_stock_project_list_squery_where." and stock_project_added_on <= :end_date";

		}



		//Data

		$get_stock_project_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}



	if($order == 'name_asc')

	{

		$get_stock_project_list_squery_order = ' order by stock_project_name asc';

	}



	$get_stock_project_list_squery = $get_stock_project_list_squery_base.$get_stock_project_list_squery_where.$get_stock_project_list_squery_order;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_project_list_sstatement = $dbConnection->prepare($get_stock_project_list_squery);



		$get_stock_project_list_sstatement -> execute($get_stock_project_list_sdata);



		$get_stock_project_list_sdetails = $get_stock_project_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_project_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_project_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_project_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

 }



/*

PURPOSE : To update Stock Project

INPUT 	: Project ID, Stock Project Update Array

OUTPUT 	: Project ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_project($project_id,$project_update_data)

{

	if(array_key_exists("project_name",$project_update_data))

	{

		$project_name = $project_update_data["project_name"];

	}

	else

	{

		$project_name = "";

	}



	if(array_key_exists("active",$project_update_data))

	{

		$active = $project_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$project_update_data))

	{

		$remarks = $project_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$project_update_data))

	{

		$added_by = $project_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $project_update_uquery_base = "update stock_project set";



	$project_update_uquery_set = "";



	$project_update_uquery_where = " where stock_project_id = :project_id";



	$project_update_udata = array(":project_id"=>$project_id);



	$filter_count = 0;



	if($project_name != "")

	{

		$project_update_uquery_set = $project_update_uquery_set." stock_project_name = :project_name,";

		$project_update_udata[":project_name"] = $project_name;

		$filter_count++;

	}



	if($active != "")

	{

		$project_update_uquery_set = $project_update_uquery_set." stock_project_active = :active,";

		$project_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$project_update_uquery_set = $project_update_uquery_set." stock_project_remarks = :remarks,";

		$project_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$project_update_uquery_set = $project_update_uquery_set." stock_project_added_by = :added_by,";

		$project_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$project_update_uquery_set = $project_update_uquery_set." stock_project_added_on = :added_on,";

		$project_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$project_update_uquery_set = trim($project_update_uquery_set,',');

	}



	$project_update_uquery = $project_update_uquery_base.$project_update_uquery_set.$project_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $project_update_ustatement = $dbConnection->prepare($project_update_uquery);



        $project_update_ustatement -> execute($project_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $project_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Location Master

INPUT 	: Location Code, Location Name, Address, Remarks, Added By, Added On

OUTPUT 	: Location ID, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_location_master($location_code,$location_name,$address,$remarks,$added_by)

{

	// Query

    $stock_location_master_iquery = "insert into stock_location_master

	(stock_location_code,stock_location_name,stock_location_address,stock_location_active,stock_location_remarks,stock_location_added_by,

	stock_location_added_on) values (:location_code,:location_name,:address,:active,:remarks,:added_by,:added_on)";



    try

    {

        $dbConnection = get_conn_handle();

        $stock_location_master_istatement = $dbConnection->prepare($stock_location_master_iquery);



        // Data

        $stock_location_master_idata = array(':location_code'=>$location_code,':location_name'=>$location_name,':address'=>$address,':active'=>'1',

		':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_location_master_istatement->execute($stock_location_master_idata);

		$stock_location_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_location_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Location Master List

INPUT 	: Location ID, Location Code, Location Name, Address, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Location Master

BY 		: Lakshmi

*/

function db_get_stock_location_master_list($stock_location_master_search_data)

{

	// Extract all input parameters

    if(array_key_exists("location_id",$stock_location_master_search_data))

	{

		$location_id = $stock_location_master_search_data["location_id"];

	}

	else

	{

		$location_id = "";

	}



	if(array_key_exists("location_code",$stock_location_master_search_data))

	{

		$location_code = $stock_location_master_search_data["location_code"];

	}

	else

	{

		$location_code = "";

	}



	if(array_key_exists("location_name",$stock_location_master_search_data))

	{

		$location_name = $stock_location_master_search_data["location_name"];

	}

	else

	{

		$location_name = "";

	}



	if(array_key_exists("location_name_check",$stock_location_master_search_data))

	{

		$location_name_check = $stock_location_master_search_data["location_name_check"];

	}

	else

	{

		$location_name_check = "";

	}



	if(array_key_exists("address",$stock_location_master_search_data))

	{

		$address = $stock_location_master_search_data["address"];

	}

	else

	{

		$address = "";

	}



	if(array_key_exists("active",$stock_location_master_search_data))

	{

		$active = $stock_location_master_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("added_by",$stock_location_master_search_data))

	{

		$added_by = $stock_location_master_search_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	if(array_key_exists("start_date",$stock_location_master_search_data))

	{

		$start_date = $stock_location_master_search_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$stock_location_master_search_data))

	{

		$end_date = $stock_location_master_search_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	if(array_key_exists("order",$stock_location_master_search_data))

	{

		$order = $stock_location_master_search_data["order"];

	}

	else

	{

		$order = "";

	}





	$get_stock_location_master_list_squery_base = "select * from stock_location_master SLM inner join users U on U.user_id = SLM.stock_location_added_by";



	$get_stock_location_master_list_squery_where = "";



	$get_stock_location_master_list_squery_order = "";



	$filter_count = 0;



	// Data

	$get_stock_location_master_list_sdata = array();



	if($location_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_id = :location_id";

		}

		else

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_id = :location_id";

		}



		// Data

		$get_stock_location_master_list_sdata[':location_id']  = $location_id;



		$filter_count++;

	}



	if($location_code != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_code = :location_code";

		}

		else

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_code = :location_code";

		}



		// Data

		$get_stock_location_master_list_sdata[':location_code']  = $location_code;



		$filter_count++;

	}



	if($location_name != "")

	{

		if($filter_count == 0)

		{

			if($location_name_check == '1')

			{

				// Query

				$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_name = :location_name";



				// Data

				$get_stock_location_master_list_sdata[':location_name']  = $location_name;

			}

			else

			{

				// Query

				$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_name like :location_name";



				// Data

				$get_stock_location_master_list_sdata[':location_name']  = '%'.$location_name.'%';

			}

		}

		else

		{

			if($location_name_check == '1')

			{

				// Query

				$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_name = :location_name";



				// Data

				$get_stock_location_master_list_sdata[':location_name']  = $location_name;

			}

			else

			{

				// Query

				$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_name like :location_name";



				// Data

				$get_stock_location_master_list_sdata[':location_name']  = '%'.$location_name.'%';

			}

		}



		$filter_count++;

	}



	if($address != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_address like :address";

		}

		else

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_address like :address";

		}



		// Data

		$get_stock_location_master_list_sdata[':address']  = '%'.$address.'%';



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_active = :active";

		}

		else

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_active = :active";

		}



		// Data

		$get_stock_location_master_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_added_by = :added_by";

		}



		// Data

		$get_stock_location_master_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_added_on >= :start_date";

		}



		//Data

		$get_stock_location_master_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." where stock_location_added_on >= :end_date";

		}

		else

		{

			// Query

			$get_stock_location_master_list_squery_where = $get_stock_location_master_list_squery_where." and stock_location_added_on >= :end_date";

		}



		//Data

		$get_stock_location_master_list_sdata[':end_date']  = $end_date;



		$filter_count++;

	}



	if($order == 'name_asc')

	{

		$get_stock_location_master_list_squery_order = ' order by stock_location_name asc';

	}



	$get_stock_location_master_list_squery_order_by  = " order by stock_location_added_on DESC";

	$get_stock_location_master_list_squery = $get_stock_location_master_list_squery_base.$get_stock_location_master_list_squery_where.$get_stock_location_master_list_squery_order.$get_stock_location_master_list_squery_order_by;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_location_master_list_sstatement = $dbConnection->prepare($get_stock_location_master_list_squery);



		$get_stock_location_master_list_sstatement -> execute($get_stock_location_master_list_sdata);



		$get_stock_location_master_list_sdetails = $get_stock_location_master_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_location_master_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_location_master_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_location_master_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Location Master

INPUT 	: Location ID, Location Master Update Array

OUTPUT 	: Location ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_location_master($location_id,$location_master_update_data)

{

	if(array_key_exists("location_code",$location_master_update_data))

	{

		$location_code = $location_master_update_data["location_code"];

	}

	else

	{

		$location_code = "";

	}



	if(array_key_exists("location_name",$location_master_update_data))

	{

		$location_name = $location_master_update_data["location_name"];

	}

	else

	{

		$location_name = "";

	}



	if(array_key_exists("address",$location_master_update_data))

	{

		$address = $location_master_update_data["address"];

	}

	else

	{

		$address = "";

	}



	if(array_key_exists("active",$location_master_update_data))

	{

		$active = $location_master_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$location_master_update_data))

	{

		$remarks = $location_master_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$location_master_update_data))

	{

		$added_by = $location_master_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $location_master_update_uquery_base = "update stock_location_master set";



	$location_master_update_uquery_set = "";



	$location_master_update_uquery_where = " where stock_location_id = :location_id";



	$location_master_update_udata = array(":location_id"=>$location_id);



	$filter_count = 0;



	if($location_code != "")

	{

		$location_master_update_uquery_set = $location_master_update_uquery_set." stock_location_code = :location_code,";

		$location_master_update_udata[":location_code"] = $location_code;

		$filter_count++;

	}

	if($location_name != "")

	{

		$location_master_update_uquery_set = $location_master_update_uquery_set." stock_location_name = :location_name,";

		$location_master_update_udata[":location_name"] = $location_name;

		$filter_count++;

	}



	if($address != "")

	{

		$location_master_update_uquery_set = $location_master_update_uquery_set." stock_location_address = :address,";

		$location_master_update_udata[":address"] = $address;

		$filter_count++;

	}



	if($active != "")

	{

		$location_master_update_uquery_set = $location_master_update_uquery_set." stock_location_active = :active,";

		$location_master_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$location_master_update_uquery_set = $location_master_update_uquery_set." stock_location_remarks = :remarks,";

		$location_master_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$location_master_update_uquery_set = $location_master_update_uquery_set." stock_location_added_by = :added_by,";

		$location_master_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$location_master_update_uquery_set = $location_master_update_uquery_set." stock_location_added_on = :added_on,";

		$location_master_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$location_master_update_uquery_set = trim($location_master_update_uquery_set,',');

	}



	$location_master_update_uquery = $location_master_update_uquery_base.$location_master_update_uquery_set.$location_master_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $location_master_update_ustatement = $dbConnection->prepare($location_master_update_uquery);



        $location_master_update_ustatement -> execute($location_master_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $location_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Tax Type master

INPUT 	: Name, Value, Jurisdiction, Remarks, Added by

OUTPUT 	: Tax Type Master ID, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_tax_type_master($name,$value,$jurisdiction,$remarks,$added_by)

{

	// Query

    $stock_tax_type_master_iquery = "insert into stock_tax_type_master (stock_tax_type_master_name,stock_tax_type_master_value,stock_tax_type_master_jurisdiction,stock_tax_type_master_active,stock_tax_type_master_remarks,

	stock_tax_type_master_added_by,stock_tax_type_master_added_on) values (:name,:value,:jurisdiction,:active,:remarks,:added_by,:added_on)";



    try

    {

        $dbConnection = get_conn_handle();

        $stock_tax_type_master_istatement = $dbConnection->prepare($stock_tax_type_master_iquery);



        // Data

        $stock_tax_type_master_idata = array(':name'=>$name,':value'=>$value,':jurisdiction'=>$jurisdiction,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_tax_type_master_istatement->execute($stock_tax_type_master_idata);

		$stock_tax_type_master_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_tax_type_master_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Tax Type Master List

INPUT 	: Tax Type Master ID, Name, Value, Jurisdiction, Active, Added by, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of stock Tax Type Master

BY 		: Lakshmi

*/

function db_get_stock_tax_type_master_list($stock_tax_type_master_search_data)

{

	if(array_key_exists("tax_type_master_id",$stock_tax_type_master_search_data))

	{

		$tax_type_master_id = $stock_tax_type_master_search_data["tax_type_master_id"];

	}

	else

	{

		$tax_type_master_id = "";

	}



	if(array_key_exists("name",$stock_tax_type_master_search_data))

	{

		$name = $stock_tax_type_master_search_data["name"];

	}

	else

	{

		$name = "";

	}



	if(array_key_exists("name_check",$stock_tax_type_master_search_data))

	{

		$name_check = $stock_tax_type_master_search_data["name_check"];

	}

	else

	{

		$name_check = "";

	}



	if(array_key_exists("value",$stock_tax_type_master_search_data))

	{

		$value = $stock_tax_type_master_search_data["value"];

	}

	else

	{

		$value = "";

	}


	if(array_key_exists("jurisdiction",$stock_tax_type_master_search_data))

	{

		$jurisdiction = $stock_tax_type_master_search_data["jurisdiction"];

	}

	else

	{

		$jurisdiction = "";

	}


	if(array_key_exists("active",$stock_tax_type_master_search_data))

	{

		$active = $stock_tax_type_master_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("added_by",$stock_tax_type_master_search_data))

	{

		$added_by = $stock_tax_type_master_search_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	if(array_key_exists("start_date",$stock_tax_type_master_search_data))

	{

		$start_date = $stock_tax_type_master_search_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$stock_tax_type_master_search_data))

	{

		$end_date = $stock_tax_type_master_search_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	if(array_key_exists("order",$stock_tax_type_master_search_data))

	{

		$order = $stock_tax_type_master_search_data["order"];

	}

	else

	{

		$order = "";

	}



	$get_stock_tax_type_master_list_squery_base = "select * from stock_tax_type_master STTM inner join users U on U.user_id = STTM.stock_tax_type_master_added_by";



	$get_stock_tax_type_master_list_squery_where = "";



	$get_stock_tax_type_master_list_squery_order = "";



	$filter_count = 0;



	// Data

	$get_stock_tax_type_master_list_sdata = array();



	if($tax_type_master_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_id = :tax_type_master_id";

		}

		else

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_id = :tax_type_master_id";

		}



		// Data

		$get_stock_tax_type_master_list_sdata[':tax_type_master_id'] = $tax_type_master_id;



		$filter_count++;

	}



	if($name != "")

	{

		if($filter_count == 0)

		{

			if($name_check == '1')

			{

				// Query

				$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_name = :name";



				// Data

				$get_stock_tax_type_master_list_sdata[':name']  = $name;

			}

			else

			{

				// Query

				$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_name like :name";



				// Data

				$get_stock_tax_type_master_list_sdata[':name']  = '%'.$name.'%';

			}

		}

		else

		{

			if($name_check == '1')

			{

				// Query

				$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_name = :name";



				// Data

				$get_stock_tax_type_master_list_sdata[':name']  = $name;

			}

			else

			{

				// Query

				$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_name like :name";



				// Data

				$get_stock_tax_type_master_list_sdata[':name']  = '%'.$name.'%';

			}

		}



		$filter_count++;

	}



	if($value != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_value = :value";

		}

		else

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_value = :value";

		}



		// Data

		$get_stock_tax_type_master_list_sdata[':value']  = $value;



		$filter_count++;

	}


	if($jurisdiction != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_jurisdiction = :jurisdiction";

		}

		else

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_jurisdiction = :jurisdiction";

		}



		// Data

		$get_stock_tax_type_master_list_sdata[':jurisdiction']  = $jurisdiction;



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_active = :active";

		}

		else

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_active = :active";

		}



		// Data

		$get_stock_tax_type_master_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_added_by = :added_by";

		}



		//Data

		$get_stock_tax_type_master_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_added_on >= :start_date";

		}



		//Data

		$get_stock_tax_type_master_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." where stock_tax_type_master_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_tax_type_master_list_squery_where = $get_stock_tax_type_master_list_squery_where." and stock_tax_type_master_added_on <= :end_date";

		}



		//Data

		$get_stock_tax_type_master_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}



	if($order == 'name_asc')

	{

		$get_stock_tax_type_master_list_squery_order = ' order by stock_tax_type_master_name asc';

	}

	$get_stock_tax_type_master_list_squery = $get_stock_tax_type_master_list_squery_base.$get_stock_tax_type_master_list_squery_where.$get_stock_tax_type_master_list_squery_order;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_tax_type_master_list_sstatement = $dbConnection->prepare($get_stock_tax_type_master_list_squery);



		$get_stock_tax_type_master_list_sstatement -> execute($get_stock_tax_type_master_list_sdata);



		$get_stock_tax_type_master_list_sdetails = $get_stock_tax_type_master_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_tax_type_master_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_tax_type_master_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_tax_type_master_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Stock Tax Type Master

INPUT 	: Tax Type Master ID,Stock Project Update Array

OUTPUT 	: Tax Type Master ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_tax_type_master($tax_type_master_id,$tax_type_master_update_data)

{

	if(array_key_exists("name",$tax_type_master_update_data))

	{

		$name = $tax_type_master_update_data["name"];

	}

	else

	{

		$name = "";

	}



	if(array_key_exists("value",$tax_type_master_update_data))

	{

		$value = $tax_type_master_update_data["value"];

	}

	else

	{

		$value = "";

	}

	if(array_key_exists("jurisdiction",$tax_type_master_update_data))

	{

		$jurisdiction = $tax_type_master_update_data["jurisdiction"];

	}

	else

	{

		$jurisdiction = "";

	}



	if(array_key_exists("active",$tax_type_master_update_data))

	{

		$active = $tax_type_master_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$tax_type_master_update_data))

	{

		$remarks = $tax_type_master_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$tax_type_master_update_data))

	{

		$added_by = $tax_type_master_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $tax_type_master_update_uquery_base = "update stock_tax_type_master set";



	$tax_type_master_update_uquery_set = "";



	$tax_type_master_update_uquery_where = " where stock_tax_type_master_id = :tax_type_master_id";



	$tax_type_master_update_udata = array(":tax_type_master_id"=>$tax_type_master_id);



	$filter_count = 0;



	if($name != "")

	{

		$tax_type_master_update_uquery_set = $tax_type_master_update_uquery_set." stock_tax_type_master_name =:name,";

		$tax_type_master_update_udata[":name"] = $name;

		$filter_count++;

	}



	if($value != "")

	{

		$tax_type_master_update_uquery_set = $tax_type_master_update_uquery_set." stock_tax_type_master_value = :value,";

		$tax_type_master_update_udata[":value"] = $value;

		$filter_count++;

	}

	if($jurisdiction != "")

	{

		$tax_type_master_update_uquery_set = $tax_type_master_update_uquery_set." stock_tax_type_master_jurisdiction = :jurisdiction,";

		$tax_type_master_update_udata[":jurisdiction"] = $jurisdiction;

		$filter_count++;

	}



	if($active != "")

	{

		$tax_type_master_update_uquery_set = $tax_type_master_update_uquery_set." stock_tax_type_master_active = :active,";

		$tax_type_master_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$tax_type_master_update_uquery_set = $tax_type_master_update_uquery_set." stock_tax_type_master_remarks = :remarks,";

		$tax_type_master_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$tax_type_master_update_uquery_set = $tax_type_master_update_uquery_set." stock_tax_type_master_added_by = :added_by,";

		$tax_type_master_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$tax_type_master_update_uquery_set = $tax_type_master_update_uquery_set." stock_tax_type_master_added_on = :added_on,";

		$tax_type_master_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$tax_type_master_update_uquery_set = trim($tax_type_master_update_uquery_set,',');

	}



	$tax_type_master_update_uquery = $tax_type_master_update_uquery_base.$tax_type_master_update_uquery_set.$tax_type_master_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $tax_type_master_update_ustatement = $dbConnection->prepare($tax_type_master_update_uquery);



        $tax_type_master_update_ustatement -> execute($tax_type_master_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $tax_type_master_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Storage Condition Master

INPUT 	: Storage Name, Remarks, Added By

OUTPUT 	: Storage ID, success or failure message

BY 		: Lakshmi

*/

function db_add_storage_condition($name,$remarks,$added_by)

{

	// Query

    $storage_iquery = "insert into stock_storage_condition_master (stock_storage_name,stock_storage_active,stock_storage_remarks,stock_storage_added_by,stock_storage_added_on) values (:name,:active,:remarks,:added_by,:now)";



    try

    {

        $dbConnection = get_conn_handle();



        $storage_istatement = $dbConnection->prepare($storage_iquery);



        // Data

        $storage_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();

        $storage_istatement->execute($storage_idata);

		$stock_storage_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_storage_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Storage Condition Master List

INPUT 	: Storage Search Array: Storage ID, Name, Name Check, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: SUCCESS or Failure message,if it is Success it will give Existing Storage Condition data.

BY 		: Lakshmi

*/

function db_get_storage_condition($storage_search_data)

{

	if(array_key_exists("storage_id",$storage_search_data))

	{

		$storage_id = $storage_search_data["storage_id"];

	}

	else

	{

		$storage_id = "";

	}



	if(array_key_exists("name",$storage_search_data))

	{

		$name = $storage_search_data["name"];

	}

	else

	{

		$name = "";

	}



	if(array_key_exists("stock_storage_name_check",$storage_search_data))

	{

		$storage_name_check = $storage_search_data["stock_storage_name_check"];

	}

	else

	{

		$storage_name_check = "";

	}



	if(array_key_exists("active",$storage_search_data))

	{

		$active = $storage_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("added_by",$storage_search_data))

	{

		$added_by = $storage_search_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	if(array_key_exists("start_date",$storage_search_data))

	{

		$start_date = $storage_search_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$storage_search_data))

	{

		$end_date = $storage_search_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	$get_storage_squery_base = "select * from stock_storage_condition_master SSCM inner join users U on U.user_id = SSCM.stock_storage_added_by";



	$get_storage_squery_where = "";



	$filter_count = 0;



	// Data

	$get_storage_sdata = array();



	if($storage_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." where stock_storage_id = :storage_id";

		}

		else

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." and stock_storage_id = :storage_id";

		}



		// Data

		$get_storage_sdata[':storage_id']  = $storage_id;



		$filter_count++;

	}



	if($name != "")

	{

		if($filter_count == 0)

		{

			if($storage_name_check == '1')

			{

				// Query

				$get_storage_squery_where   = $get_storage_squery_where." where stock_storage_name = :name";

				// Data

				$get_storage_sdata[':name'] = $name;

			}

			else

			{

				// Query

				$get_storage_squery_where   = $get_storage_squery_where." where stock_storage_name like :name";

				// Data

				$get_storage_sdata[':name'] = '%'.$name.'%';

			}

		}

		else

		{

			if($storage_name_check == '1')

			{

				// Query

				$get_storage_squery_where   = $get_storage_squery_where." and stock_storage_name = :name";

				// Data

				$get_storage_sdata[':name'] = $name;

			}

			else

			{

				// Query

				$get_storage_squery_where   = $get_storage_squery_where." and stock_storage_name like :name";

				// Data

				$get_storage_sdata[':name'] = '%'.$name.'%';

			}

		}



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." where stock_storage_active = :active";

		}

		else

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." and stock_storage_active = :active";

		}



		// Data

		$get_storage_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." where stock_storage_added_by = :added_by";

		}

		else

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." and stock_storage_added_by = :added_by";

		}



		// Data

		$get_storage_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." where stock_storage_added_on = :start_date";

		}

		else

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." and stock_storage_added_on = :start_date";

		}



		// Data

		$get_storage_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." where stock_storage_added_on = :end_date";

		}

		else

		{

			// Query

			$get_storage_squery_where = $get_storage_squery_where." and stock_storage_added_on = :end_date";

		}



		// Data

		$get_storage_sdata[':end_date']  = $end_date;



		$filter_count++;

	}



	$get_storage_order = " order by stock_storage_name ASC";

	$get_storage_squery = $get_storage_squery_base.$get_storage_squery_where.$get_storage_order;



	try

	{

		$dbConnection = get_conn_handle();



		$get_storage_sstatement = $dbConnection->prepare($get_storage_squery);



		$get_storage_sstatement -> execute($get_storage_sdata);



		$get_storage_sdetails = $get_storage_sstatement -> fetchAll();



		if(FALSE === $get_storage_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_storage_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_storage_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"]   = "";

	}



	return $return;

}



/*

PURPOSE : To update Storage Condition Master

INPUT 	: Storage ID, Storage Update Array

OUTPUT 	: Storage ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_storage_condition($storage_id,$storage_update_data)

{

	if(array_key_exists("name",$storage_update_data))

	{

		$name = $storage_update_data["name"];

	}

	else

	{

		$name = "";

	}



	if(array_key_exists("active",$storage_update_data))

	{

		$active = $storage_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$storage_update_data))

	{

		$remarks = $storage_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$storage_update_data))

	{

		$added_by = $storage_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $storage_update_uquery_base = "update stock_storage_condition_master set";



	$storage_update_uquery_set = "";



	$storage_update_uquery_where = " where stock_storage_id = :storage_id";



	$storage_update_udata = array(":storage_id"=>$storage_id);



	$filter_count = 0;



	if($name != "")

	{

		$storage_update_uquery_set = $storage_update_uquery_set." stock_storage_name = :name,";

		$storage_update_udata[":name"] = $name;

		$filter_count++;

	}



	if($active != "")

	{

		$storage_update_uquery_set = $storage_update_uquery_set." stock_storage_active = :active,";

		$storage_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$storage_update_uquery_set = $storage_update_uquery_set." stock_storage_remarks = :remarks,";

		$storage_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$storage_update_uquery_set = $storage_update_uquery_set." stock_storage_added_by = :added_by,";

		$storage_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$storage_update_uquery_set = $storage_update_uquery_set." stock_storage_added_on = :added_on,";

		$storage_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$storage_update_uquery_set = trim($storage_update_uquery_set,',');

	}



	$storage_update_uquery = $storage_update_uquery_base.$storage_update_uquery_set.$storage_update_uquery_where;



	try

    {

        $dbConnection = get_conn_handle();



        $storage_update_ustatement = $dbConnection->prepare($storage_update_uquery);



        $storage_update_ustatement -> execute($storage_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $storage_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To Add New Stock Unit Measure Master

INPUT 	: Name,Remarks,Added by

OUTPUT 	: Unit id, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_unit_measure($name,$remarks,$added_by)

{

	// Query

    $stock_unit_measure_iquery = "insert into stock_unit_measure_master(stock_unit_name,stock_unit_active,stock_remarks,stock_added_by,stock_added_on) values (:name,:active,:remarks,:added_by,:added_on)";



    try

    {

        $dbConnection = get_conn_handle();

        $stock_unit_measure_istatement = $dbConnection->prepare($stock_unit_measure_iquery);



        // Data

        $stock_unit_measure_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();

        $stock_unit_measure_istatement->execute($stock_unit_measure_idata);

		$stock_unit_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_unit_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To Get Unit Measure Master list

INPUT 	: Unit ID,Name,Active,,Added By,Start Date(for added on), End Date(for added on)

OUTPUT 	: List of stock Unit Measure Master

BY 		: Lakshmi

*/

function db_get_stock_unit_measure_list($stock_unit_measure_search_data)

{

	if(array_key_exists("unit_id",$stock_unit_measure_search_data))

	{

		$unit_id = $stock_unit_measure_search_data["unit_id"];

	}

	else

	{

		$unit_id = "";

	}



	if(array_key_exists("name",$stock_unit_measure_search_data))

	{

		$name= $stock_unit_measure_search_data["name"];

	}

	else

	{

		$name = "";

	}



	if(array_key_exists("unit_name_check",$stock_unit_measure_search_data))

	{

		$unit_name_check = $stock_unit_measure_search_data["unit_name_check"];

	}

	else

	{

		$unit_name_check = "";

	}



	if(array_key_exists("active",$stock_unit_measure_search_data))

	{

		$active= $stock_unit_measure_search_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("added_by",$stock_unit_measure_search_data))

	{

		$added_by= $stock_unit_measure_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("start_date",$stock_unit_measure_search_data))

	{

		$start_date = $stock_unit_measure_search_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$stock_unit_measure_search_data))

	{

		$end_date = $stock_unit_measure_search_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	$get_stock_unit_measure_list_squery_base = "select * from stock_unit_measure_master SUMM inner join users U on U.user_id = SUMM.stock_added_by";



	$get_stock_unit_measure_list_squery_where = "";



	$filter_count = 0;



	// Data

	$get_stock_unit_measure_list_sdata = array();



	if($unit_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." where stock_unit_id = :unit_id";

		}

		else

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." and stock_unit_id = :unit_id";

		}



		// Data

		$get_stock_unit_measure_list_sdata[':unit_id']  = $unit_id;



		$filter_count++;

	}



	if($name != "")

	{

		if($filter_count == 0)

		{

			if($unit_name_check == '1')

			{

				// Query

				$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." where stock_unit_name = :name";



				// Data

				$get_stock_unit_measure_list_sdata[':name']  = $name;

			}

			else

			{

				// Query

				$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." where stock_unit_name like :name";



				// Data

				$get_stock_unit_measure_list_sdata[':name']  = '%'.$name.'%';

			}

		}

		else

		{

			if($unit_name_check == '1')

			{

				// Query

				$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." and stock_unit_name = :name";



				// Data

				$get_stock_unit_measure_list_sdata[':name']  = $name;

			}

			else

			{

				// Query

				$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." and stock_unit_name like :name";



				// Data

				$get_stock_unit_measure_list_sdata[':name']  = '%'.$name.'%';

			}

		}



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." where stock_unit_active = :active";

		}

		else

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." and stock_unit_active = :active";

		}



		// Data

		$get_stock_unit_measure_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." where stock_added_by >= :added_by";

		}

		else

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." and stock_added_by >= :added_by";

		}



		//Data

		$get_stock_unit_measure_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



    if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." where stock_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." and stock_added_on >= :start_date";

		}



		//Data

		$get_stock_unit_measure_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." where stock_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_unit_measure_list_squery_where = $get_stock_unit_measure_list_squery_where." and stock_added_on <= :end_date";

		}



		//Data

		$get_stock_unit_measure_list_sdata[':end_date']  = $end_date;



		$filter_count++;

	}



	$get_stock_unit_measure_list_squery = $get_stock_unit_measure_list_squery_base.$get_stock_unit_measure_list_squery_where;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_unit_measure_list_sstatement = $dbConnection->prepare($get_stock_unit_measure_list_squery);



		$get_stock_unit_measure_list_sstatement -> execute($get_stock_unit_measure_list_sdata);



		$get_stock_unit_measure_list_sdetails = $get_stock_unit_measure_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_unit_measure_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_unit_measure_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_unit_measure_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Unit Measure Master

INPUT 	: Unit ID, Unit Update Array

OUTPUT 	: Unit ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_unit_measure_list($unit_id,$unit_measure_update_data)

{

	if(array_key_exists("name",$unit_measure_update_data))

	{

		$name = $unit_measure_update_data["name"];

	}

	else

	{

		$name = "";

	}



	if(array_key_exists("active",$unit_measure_update_data))

	{

		$active = $unit_measure_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$unit_measure_update_data))

	{

		$remarks = $unit_measure_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$unit_measure_update_data))

	{

		$added_by = $unit_measure_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $unit_measure_update_uquery_base = "update stock_unit_measure_master set";



	$unit_measure_update_uquery_set = "";



	$unit_measure_update_uquery_where = " where stock_unit_id = :unit_id";



	$unit_measure_update_udata = array(":unit_id"=>$unit_id);



	$filter_count = 0;





	if($name != "")

	{

		$unit_measure_update_uquery_set = $unit_measure_update_uquery_set." stock_unit_name = :name,";

		$unit_measure_update_udata[":name"] = $name;

		$filter_count++;

	}



	if($active != "")

	{

		$unit_measure_update_uquery_set = $unit_measure_update_uquery_set." stock_unit_active = :active,";

		$unit_measure_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$unit_measure_update_uquery_set = $unit_measure_update_uquery_set." stock_remarks = :remarks,";

		$unit_measure_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$unit_measure_update_uquery_set = $unit_measure_update_uquery_set." stock_added_by = :added_by,";

		$unit_measure_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$unit_measure_update_uquery_set = $unit_measure_update_uquery_set." stock_added_on = :added_on,";

		$unit_measure_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)



	{

		$unit_measure_update_uquery_set = trim($unit_measure_update_uquery_set,',');

	}



	$unit_measure_update_uquery = $unit_measure_update_uquery_base.$unit_measure_update_uquery_set.$unit_measure_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $unit_measure_update_ustatement = $dbConnection->prepare($unit_measure_update_uquery);



        $unit_measure_update_ustatement -> execute($unit_measure_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $unit_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To Add New Stock Rate History

INPUT 	: Rate, Applicable Date, Remarks, Added by

OUTPUT 	: Rate Id, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_rate_history($rate,$applicable_date,$remarks,$added_by)

{

	// Query

  $stock_rate_iquery = "insert into stock_rate_history(stock_rate,stock_rate_applicable_date,stock_rate_remarks,stock_rate_added_by,stock_rate_added_on) values(:rate,:applicable_date,:remarks,:added_by,:added_on)";

    try

    {

        $dbConnection = get_conn_handle();

        $stock_rate_istatement = $dbConnection->prepare($stock_rate_iquery);



        // Data

        $stock_rate_idata = array(':rate'=>$rate,':applicable_date'=>$applicable_date,':remarks'=>$remarks,':added_by'=>$added_by,

		':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_rate_istatement->execute($stock_rate_idata);

		$stock_rate_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_rate_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Rate History List

INPUT 	: Rate,Applicable Date,Remarks,Added By,Start Date(for added on),End Date(for added on)

OUTPUT 	: List of Stock Rate

BY 		: Lakshmi

*/

function db_get_stock_rate_list($stock_rate_search_data)

{

    if(array_key_exists("rate_id",$stock_rate_search_data))

	{

		$rate_id = $stock_rate_search_data["rate_id"];

	}

	else

	{

		$rate_id= "";

	}

	if(array_key_exists("rate",$stock_rate_search_data))

	{

		$rate = $stock_rate_search_data["rate"];

	}

	else

	{

		$rate= "";

	}



	if(array_key_exists("applicable_date",$stock_rate_search_data))

	{

		$applicable_date= $stock_rate_search_data["applicable_date"];

	}

	else

	{

		$applicable_date = "";

	}



	if(array_key_exists("added_by",$stock_rate_search_data))

	{

		$added_by= $stock_rate_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}

	if(array_key_exists("start_date",$stock_rate_search_data))

	{

		$start_date= $stock_rate_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_rate_search_data))

	{

		$end_date= $stock_rate_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}

	$get_stock_rate_list_squery_base = "select * from stock_rate_history";



	$get_stock_rate_list_squery_where = "";



	$filter_count = 0;



	// Data

	$get_stock_rate_list_sdata = array();



	if($rate_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." where stock_rate_id = :rate_id";

		}

		else

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." and stock_rate_id = :rate_id";

		}



		// Data

		$get_stock_rate_list_sdata[':rate_id']  = $rate_id;



		$filter_count++;

	}

	if($rate != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." where stock_rate = :rate";

		}

		else

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." and stock_rate = :rate";

		}



		// Data

		$get_stock_rate_list_sdata[':rate']  = $rate;



		$filter_count++;

	}

	if($applicable_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." where stock_rate_applicable_date = :applicable_date";

		}

		else

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." and stock_rate_applicable_date = :applicable_date";

		}



		// Data

		$get_stock_rate_list_sdata[':applicable_date']  = $applicable_date;



		$filter_count++;

	}

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." where stock_rate_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." and stock_rate_added_by = :added_by";

		}



		// Data

		$get_stock_rate_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}





	if($start_date!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." where stock_rate_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." and stock_rate_added_on >= :start_date";

		}



		//Data

		$get_stock_rate_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." where stock_rate_added_by = :end_date";

		}

		else

		{

			// Query

			$get_stock_rate_list_squery_where = $get_stock_rate_list_squery_where." and stock_rate_added_by = :send_date";

		}



		// Data

		$get_stock_rate_list_sdata[':end_date']  = $end_date;



		$filter_count++;

	}



	$get_stock_rate_list_squery = $get_stock_rate_list_squery_base.$get_stock_rate_list_squery_where;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_rate_list_sstatement = $dbConnection->prepare($get_stock_rate_list_squery);



		$get_stock_rate_list_sstatement -> execute($get_stock_rate_list_sdata);



		$get_stock_rate_list_sdetails = $get_stock_rate_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_rate_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_rate_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_rate_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

 }



/*

PURPOSE : To update Stock Rate History

INPUT 	: Rate ID, Stock Rate Update Array

OUTPUT 	: Rate ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_rate($rate_id,$rate_update_data)

{

	if(array_key_exists("rate",$rate_update_data))

	{

		$rate = $rate_update_data["rate"];

	}

	else

	{

		$rate = "";

	}



	if(array_key_exists("applicable_date",$rate_update_data))

	{

		$applicable_date = $rate_update_data["applicable_date"];

	}

	else

	{

		$applicable_date = "";

	}



	if(array_key_exists("added_by",$rate_update_data))

	{

		$added_by = $rate_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $rate_update_uquery_base = "update stock_rate_history set";



	$rate_update_uquery_set = "";



	$rate_update_uquery_where = " where stock_rate_id = :rate_id";



	$rate_update_udata = array(":rate_id"=>$rate_id);



	$filter_count = 0;



	if($rate != "")

	{

		$rate_update_uquery_set = $rate_update_uquery_set." stock_rate = :rate,";

		$rate_update_udata[":rate"] = $rate;

		$filter_count++;

	}



	if($applicable_date != "")

	{

		$rate_update_uquery_set = $rate_update_uquery_set." stock_rate_applicable_date = :applicable_date,";

		$rate_update_udata[":applicable_date"] = $applicable_date;

		$filter_count++;

	}



	if($added_by != "")

	{

		$rate_update_uquery_set = $rate_update_uquery_set." stock_rate_added_by = :added_by,";

		$rate_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$rate_update_uquery_set = $rate_update_uquery_set." stock_rate_added_on = :added_on,";

		$rate_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$rate_update_uquery_set = trim($rate_update_uquery_set,',');

	}



	$rate_update_uquery = $rate_update_uquery_base.$rate_update_uquery_set.$rate_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $rate_update_ustatement = $dbConnection->prepare($rate_update_uquery);



        $rate_update_ustatement -> execute($rate_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $rate_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Rate Card

INPUT 	: Material Id, Applicable Start Date, Applicable End Date, Added By

OUTPUT 	: Storage id, success or failure message

BY 		: Sonakshi D

*/

function db_add_rate_card($material_id,$start_date,$end_date,$uom,$remarks,$added_by)

{

	// Query

    $rate_iquery = "insert into stock_rate_card_master (stock_rate_card_material_id,stock_rate_card_applicable_start_date,stock_rate_card_applicable_end_date,stock_rate_card_uom,stock_rate_card_active,stock_rate_card_remarks,stock_rate_card_added_by,stock_rate_card_added_on) values (:material_id,:start_date,:end_date,:uom,:active,:remarks,:added_by,:now)";



    try

    {

        $dbConnection = get_conn_handle();



        $rate_istatement = $dbConnection->prepare($rate_iquery);



        // Data

        $rate_idata = array(':material_id'=>$material_id,':start_date'=>$start_date,':end_date'=>$end_date,':uom'=>$uom,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();

        $rate_istatement->execute($rate_idata);

		$stock_rate_card_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_rate_card_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



	/*

PURPOSE : To get Rate Card List

INPUT 	: Rate Array

OUTPUT 	: SUCCESS or Failure message,if it is Success it will give Existing data.

BY 		: Sonakshi

*/

function db_get_rate_card($rate_search_data)

{

	if(array_key_exists("rate_card_id",$rate_search_data))

	{

		$rate_card_id = $rate_search_data["rate_card_id"];

	}

	else

	{

		$rate_card_id = "";

	}



	if(array_key_exists("material_id",$rate_search_data))

	{

		$material_id = $rate_search_data["material_id"];

	}

	else

	{

		$material_id = "";

	}



	if(array_key_exists("start_date",$rate_search_data))

	{

		$start_date = $rate_search_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$rate_search_data))

	{

		$end_date = $rate_search_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	if(array_key_exists("uom",$rate_search_data))

	{

		$uom= $rate_search_data["uom"];

	}

	else

	{

		$uom= "";

	}



	if(array_key_exists("uom",$rate_search_data))

	{

		$uom= $rate_search_data["uom"];

	}

	else

	{

		$uom= "";

	}



	if(array_key_exists("active",$rate_search_data))

	{

		$active= $rate_search_data["active"];

	}

	else

	{

		$active= "";

	}

	if(array_key_exists("added_by",$rate_search_data))

	{

		$added_by= $rate_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}

	if(array_key_exists("added_on",$rate_search_data))

	{

		$added_on= $rate_search_data["added_on"];

	}

	else

	{

		$added_on= "";

	}



	$get_rate_squery_base = "select * from stock_rate_card_master SRCM inner join users U on U.user_id = SRCM.stock_rate_card_added_by inner join stock_material_master SMM on SMM.stock_material_id = SRCM.stock_rate_card_material_id inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id = SRCM.stock_rate_card_uom";



	$get_rate_squery_where = "";



	$filter_count = 0;



	// Data

	$get_rate_sdata = array();



	if($rate_card_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_id = :rate_card_id";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_id = :rate_card_id";

		}



		// Data

		$get_rate_sdata[':rate_card_id']  = $rate_card_id;



		$filter_count++;

	}



	if($material_id!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_material_id = :material_id";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_material_id = :material_id";

		}



		// Data

		$get_rate_sdata[':material_id']  = $material_id;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_applicable_start_date = :start_date	";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_applicable_start_date = :start_date	";

		}



		// Data

		$get_rate_sdata[':start_date']  = $start_date	;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_applicable_end_date = :end_date";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_applicable_end_date = :end_date";

		}



		// Data

		$get_rate_sdata[':end_date']  = $end_date;



		$filter_count++;

	}



	if($uom != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_uom = :uom";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_uom = :uom";

		}



		// Data

		$get_rate_sdata[':uom']  = $uom;



		$filter_count++;

	}



	 if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_active = :active";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_active = :active";

		}



		// Data

		$get_rate_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_added_by = :added_by";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_added_by = :added_by";

		}



		// Data

		$get_rate_sdata[':added_by']  = $added_by;



		$filter_count++;

	}

	if($added_on != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." where stock_rate_card_added_on = :added_on";

		}

		else

		{

			// Query

			$get_rate_squery_where = $get_rate_squery_where." and stock_rate_card_added_on = :added_on";

		}



		// Data

		$get_rate_sdata[':added_on']  = $added_on;



		$filter_count++;

	}



	$get_rate_squery = $get_rate_squery_base.$get_rate_squery_where;



	try

	{

		$dbConnection = get_conn_handle();



		$get_rate_sstatement = $dbConnection->prepare($get_rate_squery);



		$get_rate_sstatement -> execute($get_rate_sdata);



		$get_rate_sdetails = $get_rate_sstatement -> fetchAll();



		if(FALSE === $get_rate_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_rate_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_rate_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}



/*

PURPOSE : To update Stock Rate Card Master

INPUT 	: Rate Card ID, Stock Rate Card Update Array

OUTPUT 	: Rate Card ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_rate_card($rate_card_id,$rate_card_update_data)

{

	if(array_key_exists("material_id",$rate_card_update_data))

	{

		$material_id = $rate_card_update_data["material_id"];

	}

	else

	{

		$material_id = "";

	}



	if(array_key_exists("start_date",$rate_card_update_data))

	{

		$start_date = $rate_card_update_data["start_date"];

	}

	else

	{

		$start_date = "";

	}



	if(array_key_exists("end_date",$rate_card_update_data))

	{

		$end_date = $rate_card_update_data["end_date"];

	}

	else

	{

		$end_date = "";

	}



	if(array_key_exists("uom",$rate_card_update_data))

	{

		$uom = $rate_card_update_data["uom"];

	}

	else

	{

		$uom = "";

	}



	if(array_key_exists("active",$rate_card_update_data))

	{

		$active = $rate_card_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$rate_card_update_data))

	{

		$remarks = $rate_card_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$rate_card_update_data))

	{

		$added_by = $rate_card_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $rate_card_update_uquery_base = "update stock_rate_card_master set";



	$rate_card_update_uquery_set = "";



	$rate_card_update_uquery_where = " where stock_rate_card_id = :rate_card_id";



	$rate_card_update_udata = array(":rate_card_id"=>$rate_card_id);



	$filter_count = 0;



	if($material_id != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_material_id = :material_id,";

		$rate_card_update_udata[":material_id"] = $material_id;

		$filter_count++;

	}



	if($start_date != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_applicable_start_date = :start_date,";

		$rate_card_update_udata[":start_date"] = $start_date;

		$filter_count++;

	}



	if($end_date != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_applicable_end_date = :end_date,";

		$rate_card_update_udata[":end_date"] = $end_date;

		$filter_count++;

	}



	if($uom != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_uom = :uom,";

		$rate_card_update_udata[":uom"] = $uom;

		$filter_count++;

	}



	if($active != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_active = :active,";

		$rate_card_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_remarks = :remarks,";

		$rate_card_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_added_by = :added_by,";

		$rate_card_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}



	if($added_on != "")

	{

		$rate_card_update_uquery_set = $rate_card_update_uquery_set." stock_rate_card_added_on = :added_on,";

		$rate_card_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$rate_card_update_uquery_set = trim($rate_card_update_uquery_set,',');

	}



	$rate_card_update_uquery = $rate_card_update_uquery_base.$rate_card_update_uquery_set.$rate_card_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $rate_card_update_ustatement = $dbConnection->prepare($rate_card_update_uquery);



        $rate_card_update_ustatement -> execute($rate_card_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $rate_card_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To add new Stock Company Masters

INPUT 	: Name, Active, Remarks, Added by, Added on

OUTPUT 	: Company ID, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_company_master($company_name,$contact_person,$address,$tin_no,$remarks,$added_by)

{

	// Query

    $stock_company_master_iquery = "insert into stock_company_master (stock_company_master_name,stock_company_master_contact_person,stock_company_master_address,stock_company_master_tin_no,stock_company_master_active,stock_company_master_remarks,

	stock_company_master_added_by,stock_company_master_added_on)values(:company_name,:contact_person,:address,:tin_no,:active,:remarks,:added_by,:added_on)";

    try

    {

        $dbConnection = get_conn_handle();

        $stock_company_master_istatement = $dbConnection->prepare($stock_company_master_iquery);



        // Data

        $stock_company_master_idata = array(':company_name'=>$company_name,':contact_person'=>$contact_person,':address'=>$address,':tin_no'=>$tin_no,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));



		$dbConnection->beginTransaction();

        $stock_company_master_istatement->execute($stock_company_master_idata);

		$stock_company_master_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_company_master_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Company Masters List

INPUT 	: Company ID, Name, Active, Added by, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of stock Company Master

BY 		: Lakshmi

*/

function db_get_stock_company_master_list($stock_company_master_search_data)

{

	if(array_key_exists("company_id",$stock_company_master_search_data))

	{

		$company_id = $stock_company_master_search_data["company_id"];

	}

	else

	{

		$company_id= "";

	}



	if(array_key_exists("company_name",$stock_company_master_search_data))

	{

		$company_name= $stock_company_master_search_data["company_name"];

	}

	else

	{

		$company_name= "";

	}



	if(array_key_exists("contact_person",$stock_company_master_search_data))

	{

		$contact_person= $stock_company_master_search_data["contact_person"];

	}

	else

	{

		$contact_person= "";

	}



	if(array_key_exists("address",$stock_company_master_search_data))

	{

		$address= $stock_company_master_search_data["address"];

	}

	else

	{

		$address= "";

	}



	if(array_key_exists("tin_no",$stock_company_master_search_data))

	{

		$tin_no= $stock_company_master_search_data["tin_no"];

	}

	else

	{

		$tin_no= "";

	}



	if(array_key_exists("active",$stock_company_master_search_data))

	{

		$active= $stock_company_master_search_data["active"];

	}

	else

	{

		$active= "";

	}



	if(array_key_exists("added_by",$stock_company_master_search_data))

	{

		$added_by= $stock_company_master_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("start_date",$stock_company_master_search_data))

	{

		$start_date= $stock_company_master_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}



	if(array_key_exists("end_date",$stock_company_master_search_data))

	{

		$end_date= $stock_company_master_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	$get_stock_company_master_list_squery_base = "select * from stock_company_master SCM inner join users U on U.user_id = SCM.stock_company_master_added_by";



	$get_stock_company_master_list_squery_where = "";



	$filter_count = 0;



	// Data

	$get_stock_company_master_list_sdata = array();



	if($company_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_id = :company_id";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_id = :company_id";

		}

		// Data

		$get_stock_company_master_list_sdata[':company_id'] = $company_id;



		$filter_count++;

	}



	if($company_name != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_name = :company_name";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_name = :company_name";

		}

		// Data

		$get_stock_company_master_list_sdata[':company_name'] = $company_name;



		$filter_count++;

	}



	if($contact_person != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_contact_person = :contact_person";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_contact_person = :contact_person";

		}

		// Data

		$get_stock_company_master_list_sdata[':contact_person'] = $contact_person;



		$filter_count++;

	}



	if($address != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_address = :address";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_address = :address";

		}

		// Data

		$get_stock_company_master_list_sdata[':address'] = $address;



		$filter_count++;

	}



	if($tin_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_tin_no = :tin_no";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_tin_no = :tin_no";

		}

		// Data

		$get_stock_company_master_list_sdata[':tin_no'] = $tin_no;



		$filter_count++;

	}



	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_active=:active";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_active=:active";

		}



		// Data

		$get_stock_company_master_list_sdata[':active']  = $active;



		$filter_count++;

	}



	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_added_by = :added_by";

		}



		//Data

		$get_stock_company_master_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_added_on >= :start_date";

		}



		//Data

		$get_stock_company_master_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." where stock_company_master_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_company_master_list_squery_where = $get_stock_company_master_list_squery_where." and stock_company_master_added_on <= :end_date";

		}



		//Data

		$get_stock_company_master_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}

	$get_stock_company_list_squery = $get_stock_company_master_list_squery_base.$get_stock_company_master_list_squery_where;







	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_company_list_sstatement = $dbConnection->prepare($get_stock_company_list_squery);



		$get_stock_company_list_sstatement -> execute($get_stock_company_master_list_sdata);



		$get_stock_company_list_sdetails = $get_stock_company_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_company_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_company_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_company_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

 }



/*

PURPOSE : To update Stock Company Master

INPUT 	: Company ID, Stock Company Update Array

OUTPUT 	: Rate ID; Message of success or failure

BY 		: Sonakshi

*/

function db_update_stock_company_master($company_id,$company_update_data)

{

	if(array_key_exists("company_name",$company_update_data))

	{

		$company_name = $company_update_data["company_name"];

	}

	else

	{

		$company_name = "";

	}



	if(array_key_exists("contact_person",$company_update_data))

	{

		$contact_person = $company_update_data["contact_person"];

	}

	else

	{

		$contact_person = "";

	}



	if(array_key_exists("address",$company_update_data))

	{

		$address = $company_update_data["address"];

	}

	else

	{

		$address = "";

	}



	if(array_key_exists("tin_no",$company_update_data))

	{

		$tin_no = $company_update_data["tin_no"];

	}

	else

	{

		$tin_no = "";

	}



	if(array_key_exists("active",$company_update_data))

	{

		$active = $company_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$company_update_data))

	{

		$remarks = $company_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$company_update_data))

	{

		$added_by = $company_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	$added_on = date('Y-m-d H:i:s');



	// Query

    $stock_company_update_uquery_base = "update stock_company_master set";



	$stock_company_update_uquery_set = "";



	$stock_company_update_uquery_where = " where stock_company_master_id = :company_id";



	$stock_company_update_udata = array(":company_id"=>$company_id);



	$filter_count = 0;



	if($company_name != "")

	{

		$stock_company_update_uquery_set = $stock_company_update_uquery_set." stock_company_master_name = :company_name,";

		$stock_company_update_udata[":company_name"] = $company_name;

		$filter_count++;

	}



	if($contact_person != "")

	{

		$stock_company_update_uquery_set = $stock_company_update_uquery_set." stock_company_master_contact_person = :contact_person,";

		$stock_company_update_udata[":contact_person"] = $contact_person;

		$filter_count++;

	}



	if($address != "")

	{

		$stock_company_update_uquery_set = $stock_company_update_uquery_set." stock_company_master_address = :address,";

		$stock_company_update_udata[":address"] = $address;

		$filter_count++;

	}



	if($tin_no != "")

	{

		$stock_company_update_uquery_set = $stock_company_update_uquery_set." stock_company_master_tin_no = :tin_no,";

		$stock_company_update_udata[":tin_no"] = $tin_no;

		$filter_count++;

	}



	if($active != "")

	{

		$stock_company_update_uquery_set = $stock_company_update_uquery_set." stock_company_master_active = :active,";

		$stock_company_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$stock_company_update_uquery_set = $stock_company_update_uquery_set." stock_company_master_remarks = :remarks,";

		$stock_company_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$stock_company_update_uquery_set = $stock_company_update_uquery_set." stock_company_master_added_by = :added_by,";

		$stock_company_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}





	if($filter_count > 0)

	{

		$stock_company_update_uquery_set = trim($stock_company_update_uquery_set,',');

	}



	$stock_company_update_uquery = $stock_company_update_uquery_base.$stock_company_update_uquery_set.$stock_company_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $stock_company_update_ustatement = $dbConnection->prepare($stock_company_update_uquery);



        $stock_company_update_ustatement -> execute($stock_company_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $company_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}
/*
PURPOSE : To add new stock Machine Master
INPUT 	: Name, Machine No, Use, Machine Type, Added By
OUTPUT 	: Machine ID, success or failure message
BY 		: Lakshmi
*/
function db_add_stock_machine_master($name,$machine_no,$vendor,$type,$remarks,$added_by)
{
	// Query
	$stock_machine_master_iquery = "insert into stock_machine_master (stock_machine_master_name,stock_machine_master_id_number,stock_machine_master_vendor,stock_machine_type,stock_machine_master_active,stock_machine_master_remarks,stock_machine_master_added_by,stock_machine_master_added_on) values (:name,:number,:vendor,:type,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $stock_machine_master_istatement = $dbConnection->prepare($stock_machine_master_iquery);

        // Data
        $stock_machine_master_idata = array(':name'=>$name,':number'=>$machine_no,':vendor'=>$vendor,':type'=>$type,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $stock_machine_master_istatement->execute($stock_machine_master_idata);
		$stock_machine_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $stock_machine_master_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get stock Machine Master list
INPUT 	: machine ID, Name, Number, Use, Machine Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of stock Machine Master
BY 		: Lakshmi
*/
function db_get_stock_machine_master($stock_machine_master_search_data)
{
	if(array_key_exists("machine_id",$stock_machine_master_search_data))
	{
		$machine_id = $stock_machine_master_search_data["machine_id"];
	}
	else
	{
		$machine_id= "";
	}

	if(array_key_exists("machine_name_check",$stock_machine_master_search_data))
	{
		$machine_name_check = $stock_machine_master_search_data["machine_name_check"];
	}
	else
	{
		$machine_name_check = "";
	}

	if(array_key_exists("name",$stock_machine_master_search_data))
	{
		$name = $stock_machine_master_search_data["name"];
	}
	else
	{
		$name = "";
	}

	if(array_key_exists("number",$stock_machine_master_search_data))
	{
		$number = $stock_machine_master_search_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("vendor",$stock_machine_master_search_data))
	{
		$vendor = $stock_machine_master_search_data["vendor"];
	}
	else
	{
		$vendor = "";
	}

	if(array_key_exists("type",$stock_machine_master_search_data))
	{
		$type = $stock_machine_master_search_data["type"];
	}
	else
	{
		$type = "";
	}

	if(array_key_exists("use",$stock_machine_master_search_data))
	{
		$use = $stock_machine_master_search_data["use"];
	}
	else
	{
		$use = "";
	}

	if(array_key_exists("machine_type",$stock_machine_master_search_data))
	{
		$machine_type = $stock_machine_master_search_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("active",$stock_machine_master_search_data))
	{
		$active = $stock_machine_master_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$stock_machine_master_search_data))
	{
		$added_by = $stock_machine_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$stock_machine_master_search_data))
	{
		$start_date= $stock_machine_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$stock_machine_master_search_data))
	{
		$end_date= $stock_machine_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_stock_machine_master_list_squery_base = "select *,U.user_name as added_by,AU.user_name as employee_name from stock_machine_master PMM inner join users U on U.user_id = PMM.stock_machine_master_added_by left outer join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id=PMM.stock_machine_master_vendor left outer join users AU on AU.user_id= PMM.stock_machine_master_vendor";

	$get_stock_machine_master_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_stock_machine_master_list_sdata = array();

	if($machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_id = :machine_id";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_id = :machine_id";
		}

		// Data
		$get_stock_machine_master_list_sdata[':machine_id'] = $machine_id;

		$filter_count++;
	}

	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($machine_name_check == '1')
			{
				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_name = :name";
				// Data
				$get_stock_machine_master_list_sdata[':name']  = $name;
			}
			else if($machine_name_check == '2')
			{
				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_name like :name";

				// Data
				$get_stock_machine_master_list_sdata[':name']  = '%'.$name;
			}
			else
			{
				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_name like :name";

				// Data
				$get_stock_machine_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($machine_name_check == '1')
			{
				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_name = :name";

				// Data
				$get_stock_machine_master_list_sdata[':name']  = $name;
			}
			else if($machine_name_check == '2')
			{
				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." or stock_machine_master_name like :name";
				// Data
				$get_stock_machine_master_list_sdata[':name']  = '%'.$name;
			}
			else
			{
				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_name like :name";

				// Data
				$get_stock_machine_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}

		$filter_count++;
	}

	if($number_check != "")

	{

		if($filter_count == 0)

		{

			 // Query

			if($machine_name_check == '1')

			{

				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_id_number = :number_check";

				// Data

				$get_stock_machine_master_list_sdata[':number_check']  = $number_check;

			}

			else if($machine_name_check == '2')

			{

				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_id_number like :number_check";



				// Data

				$get_stock_machine_master_list_sdata[':number_check']  = $number_check.'%';

			}

			else

			{

				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_id_number like :number_check";



				// Data

				$get_stock_machine_master_list_sdata[':number_check']  = '%'.$number_check.'%';

			}

		}

		else

		{

			// Query

			if($machine_name_check == '1')

			{

				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_id_number = :number_check";



				// Data

				$get_stock_machine_master_list_sdata[':number_check']  = $number_check;

			}

			else if($machine_name_check == '2')

			{

				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." or stock_machine_master_id_number like :number_check";

				// Data

				$get_stock_machine_master_list_sdata[':number_check']  = $number_check.'%';

			}

			else

			{

				$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_id_number like :number_check";



				// Data

				$get_stock_machine_master_list_sdata[':number_check']  = '%'.$number_check.'%';

			}

		}



		$filter_count++;

	}

	if($number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_id_number like :number";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_id_number like :number";
		}

		// Data
		$get_stock_machine_master_list_sdata[':number'] = '%'.$number;

		$filter_count++;
	}

	if($vendor != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_vendor = :vendor";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_vendor = :vendor";
		}

		// Data
		$get_stock_machine_master_list_sdata[':vendor'] = $vendor;

		$filter_count++;
	}

	if($type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_type = :type";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_type = :type";
		}

		// Data
		$get_stock_machine_master_list_sdata[':type'] = $type;

		$filter_count++;
	}

	if($use != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_use = :use";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_use = :use";
		}

		// Data
		$get_stock_machine_master_list_sdata[':use'] = $use;

		$filter_count++;
	}

	if($machine_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_machine_type = :machine_type";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_machine_type = :machine_type";
		}

		// Data
		$get_stock_machine_master_list_sdata[':machine_type'] = $machine_type;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_active = :active";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_active = :active";
		}

		// Data
		$get_stock_machine_master_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_added_by = :added_by";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_added_by = :added_by";
		}

		//Data
		$get_stock_machine_master_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_added_on >= :start_date";
		}

		//Data
		$get_stock_machine_master_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." where stock_machine_master_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_stock_machine_master_list_squery_where = $get_stock_machine_master_list_squery_where." and stock_machine_master_added_on <= :end_date";
		}

		//Data
		$get_stock_machine_master_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_stock_machine_master_list_squery_order = " order by stock_machine_master_added_on DESC";

	$get_stock_machine_master_list_squery = $get_stock_machine_master_list_squery_base.$get_stock_machine_master_list_squery_where.$get_stock_machine_master_list_squery_order;

	try
	{
		$dbConnection = get_conn_handle();

		$get_stock_machine_master_list_sstatement = $dbConnection->prepare($get_stock_machine_master_list_squery);

		$get_stock_machine_master_list_sstatement -> execute($get_stock_machine_master_list_sdata);

		$get_stock_machine_master_list_sdetails = $get_stock_machine_master_list_sstatement -> fetchAll();

		if(FALSE === $get_stock_machine_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stock_machine_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stock_machine_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update stock Machine Master
INPUT 	: Machine ID, stock Machine Master Update Array
OUTPUT 	: Machine ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_stock_machine_master($machine_id,$stock_machine_master_update_data)
{
	if(array_key_exists("name",$stock_machine_master_update_data))
	{
		$name = $stock_machine_master_update_data["name"];
	}
	else
	{
		$name = "";
	}

	if(array_key_exists("number",$stock_machine_master_update_data))
	{
		$number = $stock_machine_master_update_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("vendor",$stock_machine_master_update_data))
	{
		$vendor = $stock_machine_master_update_data["vendor"];
	}
	else
	{
		$vendor = "";
	}

	if(array_key_exists("type",$stock_machine_master_update_data))
	{
		$type = $stock_machine_master_update_data["type"];
	}
	else
	{
		$type = "";
	}


	if(array_key_exists("use",$stock_machine_master_update_data))
	{
		$use = $stock_machine_master_update_data["use"];
	}
	else
	{
		$use = "";
	}

	if(array_key_exists("machine_type",$stock_machine_master_update_data))
	{
		$machine_type = $stock_machine_master_update_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("active",$stock_machine_master_update_data))
	{
		$active = $stock_machine_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$stock_machine_master_update_data))
	{
		$remarks = $stock_machine_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$stock_machine_master_update_data))
	{
		$added_by = $stock_machine_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$stock_machine_master_update_data))
	{
		$added_on = $stock_machine_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $stock_machine_master_update_uquery_base = "update stock_machine_master set ";

	$stock_machine_master_update_uquery_set = "";

	$stock_machine_master_update_uquery_where = " where stock_machine_master_id = :machine_id";

	$stock_machine_master_update_udata = array(":machine_id"=>$machine_id);

	$filter_count = 0;

	if($name != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_name = :name,";
		$stock_machine_master_update_udata[":name"] = $name;
		$filter_count++;
	}

	if($number != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_id_number = :number,";
		$stock_machine_master_update_udata[":number"] = $number;
		$filter_count++;
	}

	if($vendor != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_vendor = :vendor,";
		$stock_machine_master_update_udata[":vendor"] = $vendor;
		$filter_count++;
	}

	if($type != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_type = :type,";
		$stock_machine_master_update_udata[":type"] = $type;
		$filter_count++;
	}

	if($use != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_use = :use,";
		$stock_machine_master_update_udata[":use"] = $use;
		$filter_count++;
	}

	if($machine_type != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_machine_type = :machine_type,";
		$stock_machine_master_update_udata[":machine_type"] = $machine_type;
		$filter_count++;
	}

	if($active != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_active = :active,";
		$stock_machine_master_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_remarks = :remarks,";
		$stock_machine_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_added_by = :added_by,";
		$stock_machine_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$stock_machine_master_update_uquery_set = $stock_machine_master_update_uquery_set." stock_machine_master_added_on = :added_on,";
		$stock_machine_master_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$stock_machine_master_update_uquery_set = trim($stock_machine_master_update_uquery_set,',');
	}

	$stock_machine_master_update_uquery = $stock_machine_master_update_uquery_base.$stock_machine_master_update_uquery_set.$stock_machine_master_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $stock_machine_master_update_ustatement = $dbConnection->prepare($stock_machine_master_update_uquery);

        $stock_machine_master_update_ustatement -> execute($stock_machine_master_update_udata);

        $return["status"] = SUCCESS;
		    $return["data"]   = $machine_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
//Material Percentage calculation
function db_add_stock_material_percentage($material_id,$material_code,$material_name,$material_unit,$percenatge,$remarks,$added_by)
{
// Query
  $stock_iquery = "insert into material_stock_percentage (stock_percentage_material_id,stock_percentage_material_name,stock_percentage_material_code,stock_percentage_material_uom,stock_percentage,
  stock_percentage_active,stock_percentage_remarks,stock_percentage_added_by,stock_percentage_added_on)
  values (:material_id,:material_name,:material_code,:material_unit,:percenatge,
          :active,:remarks,:added_by,:now)";
    try
    {
      $dbConnection = get_conn_handle();
      $stock_istatement = $dbConnection->prepare($stock_iquery);
      // Data
      $stock_idata = array(':material_id'=>$material_id,
                       ':material_code'=>$material_code,
                       ':material_name'=>$material_name,
                       ':material_unit'=>$material_unit,
                       ':percenatge'=>$percenatge,
                       ':active'=>'1',
                       ':remarks'=>$remarks,
                       ':added_by'=>$added_by,
                       ':now'=>date("Y-m-d H:i:s"));
      $dbConnection->beginTransaction();
      $stock_istatement->execute($stock_idata);
      $material_id = $dbConnection->lastInsertId();
      $dbConnection->commit();
      $return["status"] = SUCCESS;
      $return["data"]   = $material_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
  	$return["data"]   = "";
    }
    return $return;
}

/*

PURPOSE : To get stock material master list

INPUT 	: Material ID, Material code, Material name, Material type, Material unit, Material storage, Material transportation mode, Material manufacturer part no, Material lead time, Material indicator, Material active, Material added by, Start Date(for added on), End Date(for added on), Material Updated By,Order

OUTPUT 	: List of material master

BY 		: Lakshmi

*/

function db_get_stock_material_percentage_list($stock_material_percenatge_search_data)
{
    if(array_key_exists("percentage_id",$stock_material_percenatge_search_data))

    {
      $percentage_id = $stock_material_percenatge_search_data["percentage_id"];
    }
    else
    {
      $percentage_id = "";
    }

    if(array_key_exists("material_id",$stock_material_percenatge_search_data))

  	{
  		$material_id = $stock_material_percenatge_search_data["material_id"];
  	}
  	else
  	{
  		$material_id = "";
  	}

  	$get_stock_material_percenatge_list_squery_base = "select * from material_stock_percentage MSP";
	  $get_stock_material_percenatge_list_squery_where = "";
	  $get_stock_material_percentage_list_squery_order = "";
	  $filter_count = 0;

	  // Data
	  $get_stock_material_percenatge_list_sdata = array();

    if($percentage_id != "")
	  {
  		if($filter_count == 0)
  		{
  			// Query
  			$get_stock_material_percenatge_list_squery_where = $get_stock_material_percenatge_list_squery_where." where stock_percentage_id=:percentage_id";
  		}
  		else
  		{
  			// Query
  			$get_stock_material_percenatge_list_squery_where = $get_stock_material_percenatge_list_squery_where." and stock_percentage_id=:percentage_id";
  		}
  		// Data
  		$get_stock_material_percenatge_list_sdata[':percentage_id']  = $percentage_id;
  		$filter_count++;
	  }

	  if($material_id != "")
	  {
  		if($filter_count == 0)
  		{
  			// Query
  			$get_stock_material_percenatge_list_squery_where = $get_stock_material_percenatge_list_squery_where." where stock_percentage_material_id=:material_id";
  		}
  		else
  		{
  			// Query
  			$get_stock_material_percenatge_list_squery_where = $get_stock_material_percenatge_list_squery_where." and stock_percentage_material_id=:material_id";
  		}
  		// Data
  		$get_stock_material_percenatge_list_sdata[':material_id']  = $material_id;
  		$filter_count++;
	   }

	$get_stock_material_percentage_list_squery_order = " order by stock_percentage_added_on DESC";
	$get_stock_material_percentage_list_squery = $get_stock_material_percenatge_list_squery_base.$get_stock_material_percenatge_list_squery_where.$get_stock_material_percentage_list_squery_order;
	try
	{
		$dbConnection = get_conn_handle();
		$get_stock_material_list_sstatement = $dbConnection->prepare($get_stock_material_percentage_list_squery);
		$get_stock_material_list_sstatement -> execute($get_stock_material_percenatge_list_sdata);
		$get_stock_material_list_sdetails = $get_stock_material_list_sstatement -> fetchAll();
		if(FALSE === $get_stock_material_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stock_material_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stock_material_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	return $return;
}

// Update Material Percentage
function db_update_material_percentage_list($percentage_id,$material_percentage_update_data)
{
	if(array_key_exists("percentage",$material_percentage_update_data))
	{
		$percentage = $material_percentage_update_data["percentage"];
	}
	else
	{
		$percentage = "";
	}

	if(array_key_exists("remarks",$material_percentage_update_data))
	{
		$remarks = $material_percentage_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

  $material_percentage_update_uquery_base = "update material_stock_percentage set";
	$material_percentage_update_uquery_set = "";
	$material_percentage_update_uquery_where = " where stock_percentage_id = :percentage_id";
	$material_percentage_update_udata = array(":percentage_id"=>$percentage_id);
	$filter_count = 0;

	if($percentage != "")
	{
		$material_percentage_update_uquery_set = $material_percentage_update_uquery_set." stock_percentage = :percentage,";
		$material_percentage_update_udata[":percentage"] = $percentage;
		$filter_count++;
	}

	if($remarks != "")
	{
		$material_percentage_update_uquery_set = $material_percentage_update_uquery_set." stock_percentage_remarks = :remarks,";
		$material_percentage_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$material_percentage_update_uquery_set = trim($material_percentage_update_uquery_set,',');
	}
	$material_percentage_update_uquery = $material_percentage_update_uquery_base.$material_percentage_update_uquery_set.$material_percentage_update_uquery_where;
  try
  {
      $dbConnection = get_conn_handle();
      $material_percentage_update_ustatement = $dbConnection->prepare($material_percentage_update_uquery);
      $material_percentage_update_ustatement -> execute($material_percentage_update_udata);
      $return["status"] = SUCCESS;
	    $return["data"]   = $percentage_id;
  }
  catch(PDOException $e)
  {
      // Log the error
      $return["status"] = FAILURE;
	    $return["data"]   = "";
  }
  return $return;
}

?>
