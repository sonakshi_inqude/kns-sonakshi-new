<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
PURPOSE : To add new Reg Client Request
INPUT 	: Client ID, Booking ID, Request Date, Requested By, Requested On, Remarks, Added By
OUTPUT 	: Request ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_client_request($client_id,$booking_id,$request_date,$request_for,$requested_on,$remarks,$added_by)
{
	// Query
   $reg_client_request_iquery = "insert into reg_client_request 
   (reg_client_request_client_id,reg_client_request_booking_id,reg_client_request_date,reg_client_request_for,reg_client_requested_on,reg_client_request_status,
   reg_client_request_active,reg_client_request_remarks,reg_client_request_added_by,reg_client_request_added_on)
   values(:client_id,:booking_id,:request_date,:request_for,:requested_on,:status,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $reg_client_request_istatement = $dbConnection->prepare($reg_client_request_iquery);
        
        // Data
        $reg_client_request_idata = array(':client_id'=>$client_id,':booking_id'=>$booking_id,':request_date'=>$request_date,':request_for'=>$request_for,
		':requested_on'=>$requested_on,':status'=>'Pending',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $reg_client_request_istatement->execute($reg_client_request_idata);
		$reg_client_request_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_client_request_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Client Request List
INPUT 	: Request ID, Client ID, Booking ID, Request Date, Request For, Requested On, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Client Request
BY 		: Lakshmi
*/
function db_get_reg_client_request($reg_client_request_search_data)
{  
	if(array_key_exists("request_id",$reg_client_request_search_data))
	{
		$request_id = $reg_client_request_search_data["request_id"];
	}
	else
	{
		$request_id= "";
	}
	
	if(array_key_exists("client_id",$reg_client_request_search_data))
	{
		$client_id = $reg_client_request_search_data["client_id"];
	}
	else
	{
		$client_id = "";
	}
	
	if(array_key_exists("booking_id",$reg_client_request_search_data))
	{
		$booking_id = $reg_client_request_search_data["booking_id"];
	}
	else
	{
		$booking_id = "";
	}
	
	if(array_key_exists("request_start_date",$reg_client_request_search_data))
	{
		$request_start_date = $reg_client_request_search_data["request_start_date"];
	}
	else
	{
		$request_start_date = "";
	}
	
	if(array_key_exists("request_end_date",$reg_client_request_search_data))
	{
		$request_end_date = $reg_client_request_search_data["request_end_date"];
	}
	else
	{
		$request_end_date = "";
	}
	
	if(array_key_exists("request_for",$reg_client_request_search_data))
	{
		$request_for = $reg_client_request_search_data["request_for"];
	}
	else
	{
		$request_for = "";
	}
	
	if(array_key_exists("requested_on",$reg_client_request_search_data))
	{
		$requested_on = $reg_client_request_search_data["requested_on"];
	}
	else
	{
		$requested_on = "";
	}
	
	if(array_key_exists("status",$reg_client_request_search_data))
	{
		$status = $reg_client_request_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$reg_client_request_search_data))
	{
		$active = $reg_client_request_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_client_request_search_data))
	{
		$added_by = $reg_client_request_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$reg_client_request_search_data))
	{
		$start_date= $reg_client_request_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$reg_client_request_search_data))
	{
		$end_date= $reg_client_request_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_reg_client_request_list_squery_base = "select * from reg_client_request RCR inner join users U on U.user_id = RCR.reg_client_request_added_by inner join bd_own_account_master AM on AM.bd_own_accunt_master_id = RCR.reg_client_request_for";
	
	$get_reg_client_request_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_reg_client_request_list_sdata = array();
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_id = :request_id";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($client_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_client_id = :client_id";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_client_id = :client_id";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':client_id'] = $client_id;
		
		$filter_count++;
	}
	
	if($booking_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_booking_id = :booking_id";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_booking_id = :booking_id";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':booking_id']  = $booking_id;
		
		$filter_count++;
	}
	
	if($request_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_date >= :request_start_date";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_date >= :request_start_date";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':request_start_date']  = $request_start_date;
		
		$filter_count++;
	}
	
	if($request_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_date <= :request_end_date";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_date < :request_end_date";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':request_end_date']  = $request_end_date;
		
		$filter_count++;
	}
	
	if($request_for != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_for = :request_for";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_for = :request_for";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':request_for']  = $request_for;
		
		$filter_count++;
	}
	
	if($requested_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_requested_on = :requested_on";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_requested_on = :requested_on";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':requested_on']  = $requested_on;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_status = :status";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_status = :status";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_active = :active";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_active = :active";				
		}
		
		// Data
		$get_reg_client_request_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_added_by = :added_by";
		}
		
		//Data
		$get_reg_client_request_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_added_on >= :start_date";				
		}
		
		//Data
		$get_reg_client_request_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." where reg_client_request_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reg_client_request_list_squery_where = $get_reg_client_request_list_squery_where." and reg_client_request_added_on <= :end_date";
		}
		
		//Data
		$get_reg_client_request_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_client_request_list_squery = $get_reg_client_request_list_squery_base.$get_reg_client_request_list_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_client_request_list_sstatement = $dbConnection->prepare($get_reg_client_request_list_squery);
		
		$get_reg_client_request_list_sstatement -> execute($get_reg_client_request_list_sdata);
		
		$get_reg_client_request_list_sdetails = $get_reg_client_request_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_client_request_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_client_request_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_client_request_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Reg Client Request
INPUT 	: Request ID, Reg Client Request Update Array
OUTPUT 	: Request ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_client_request($request_id,$reg_client_request_update_data)
{
	if(array_key_exists("client_id",$reg_client_request_update_data))
	{	
		$client_id = $reg_client_request_update_data["client_id"];
	}
	else
	{
		$client_id = "";
	}
	
	if(array_key_exists("booking_id",$reg_client_request_update_data))
	{	
		$booking_id = $reg_client_request_update_data["booking_id"];
	}
	else
	{
		$booking_id = "";
	}
	
	if(array_key_exists("request_date",$reg_client_request_update_data))
	{	
		$request_date = $reg_client_request_update_data["request_date"];
	}
	else
	{
		$request_date = "";
	}
	
	if(array_key_exists("request_for",$reg_client_request_update_data))
	{	
		$request_for = $reg_client_request_update_data["request_for"];
	}
	else
	{
		$request_for = "";
	}
	
	if(array_key_exists("requested_on",$reg_client_request_update_data))
	{	
		$requested_on = $reg_client_request_update_data["requested_on"];
	}
	else
	{
		$requested_on = "";
	}
	
	if(array_key_exists("status",$reg_client_request_update_data))
	{	
		$status = $reg_client_request_update_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$reg_client_request_update_data))
	{	
		$active = $reg_client_request_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_client_request_update_data))
	{	
		$remarks = $reg_client_request_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_client_request_update_data))
	{	
		$added_by = $reg_client_request_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_client_request_update_data))
	{	
		$added_on = $reg_client_request_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_client_request_update_uquery_base = "update reg_client_request set";  
	
	$reg_client_request_update_uquery_set = "";
	
	$reg_client_request_update_uquery_where = " where reg_client_request_id = :request_id";
	
	$reg_client_request_update_udata = array(":request_id"=>$request_id);
	
	$filter_count = 0;
	
	if($client_id != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_client_id = :client_id,";
		$reg_client_request_update_udata[":client_id"] = $client_id;
		$filter_count++;
	}
	
	if($booking_id != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_booking_id = :booking_id,";
		$reg_client_request_update_udata[":booking_id"] = $booking_id;
		$filter_count++;
	}
	
	if($request_date != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_date = :request_date,";
		$reg_client_request_update_udata[":request_date"] = $request_date;
		$filter_count++;
	}
	
	if($request_for != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_for = :request_for,";
		$reg_client_request_update_udata[":request_for"] = $request_for;
		$filter_count++;
	}
	
	if($requested_on != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_requested_on = :requested_on,";
		$reg_client_request_update_udata[":requested_on"] = $requested_on;
		$filter_count++;
	}
	
	if($status != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_status = :status,";
		$reg_client_request_update_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_active = :active,";
		$reg_client_request_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_remarks = :remarks,";
		$reg_client_request_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_added_by = :added_by,";
		$reg_client_request_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_client_request_update_uquery_set = $reg_client_request_update_uquery_set." reg_client_request_added_on = :added_on,";
		$reg_client_request_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_client_request_update_uquery_set = trim($reg_client_request_update_uquery_set,',');
	}
	
	$reg_client_request_update_uquery = $reg_client_request_update_uquery_base.$reg_client_request_update_uquery_set.$reg_client_request_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_client_request_update_ustatement = $dbConnection->prepare($reg_client_request_update_uquery);		
        
        $reg_client_request_update_ustatement -> execute($reg_client_request_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $request_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Reg Client Reg Process
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Remarks, Added By
OUTPUT 	: Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_client_reg_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	// Query
   $reg_client_reg_process_iquery = "insert into reg_client_reg_process 
   (reg_client_reg_process_master_id,reg_client_reg_process_request_id,reg_client_reg_process_start_date,reg_client_reg_process_end_date,reg_client_reg_process_active,
   reg_client_reg_process_remarks,reg_client_reg_process_added_by,reg_client_reg_process_added_on)
   values(:process_master_id,:request_id,:process_start_date,:process_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $reg_client_reg_process_istatement = $dbConnection->prepare($reg_client_reg_process_iquery);
        
        // Data
        $reg_client_reg_process_idata = array(':process_master_id'=>$process_master_id,':request_id'=>$request_id,':process_start_date'=>$process_start_date,
		':process_end_date'=>$process_end_date,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $reg_client_reg_process_istatement->execute($reg_client_reg_process_idata);
		$reg_client_reg_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_client_reg_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Client Reg Process List
INPUT 	: Process ID, Client ID, Process Master ID, Request ID, Process Start Date, Process End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Client Reg Process
BY 		: Lakshmi
*/
function db_get_reg_client_reg_process($reg_client_reg_process_search_data)
{  
	if(array_key_exists("process_id",$reg_client_reg_process_search_data))
	{
		$process_id = $reg_client_reg_process_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}
	
	if(array_key_exists("process_master_id",$reg_client_reg_process_search_data))
	{
		$process_master_id = $reg_client_reg_process_search_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$reg_client_reg_process_search_data))
	{
		$request_id = $reg_client_reg_process_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$reg_client_reg_process_search_data))
	{
		$process_start_date = $reg_client_reg_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$reg_client_reg_process_search_data))
	{
		$process_end_date = $reg_client_reg_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$reg_client_reg_process_search_data))
	{
		$active = $reg_client_reg_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_client_reg_process_search_data))
	{
		$added_by = $reg_client_reg_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$reg_client_reg_process_search_data))
	{
		$start_date= $reg_client_reg_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$reg_client_reg_process_search_data))
	{
		$end_date= $reg_client_reg_process_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_reg_client_reg_process_list_squery_base = "select * from reg_client_reg_process RCRP inner join users U on U.user_id=RCRP.reg_client_reg_process_added_by inner join reg_client_process_master RCPM on RCPM.reg_client_process_master_id=RCRP.reg_client_reg_process_master_id inner join reg_client_request RCR on RCR.reg_client_request_id=RCRP.reg_client_reg_process_request_id";
	
	$get_reg_client_reg_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_reg_client_reg_process_list_sdata = array();
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_id = :process_id";				
		}
		
		// Data
		$get_reg_client_reg_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_master_id = :process_master_id";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_master_id = :process_master_id";				
		}
		
		// Data
		$get_reg_client_reg_process_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_request_id = :request_id";				
		}
		
		// Data
		$get_reg_client_reg_process_list_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_start_date = :process_start_date";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_start_date = :process_start_date";				
		}
		
		// Data
		$get_reg_client_reg_process_list_sdata[':process_start_date']  = $process_start_date;
		
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_end_date = :process_end_date";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_end_date = :process_end_date";				
		}
		
		// Data
		$get_reg_client_reg_process_list_sdata[':process_end_date']  = $process_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_active = :active";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_active = :active";				
		}
		
		// Data
		$get_reg_client_reg_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_added_by = :added_by";
		}
		
		//Data
		$get_reg_client_reg_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_added_on >= :start_date";				
		}
		
		//Data
		$get_reg_client_reg_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." where reg_client_reg_process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reg_client_reg_process_list_squery_where = $get_reg_client_reg_process_list_squery_where." and reg_client_reg_process_added_on <= :end_date";
		}
		
		//Data
		$get_reg_client_reg_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_client_reg_process_list_squery = $get_reg_client_reg_process_list_squery_base.$get_reg_client_reg_process_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_client_reg_process_list_sstatement = $dbConnection->prepare($get_reg_client_reg_process_list_squery);
		
		$get_reg_client_reg_process_list_sstatement -> execute($get_reg_client_reg_process_list_sdata);
		
		$get_reg_client_reg_process_list_sdetails = $get_reg_client_reg_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_client_reg_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_client_reg_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_client_reg_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Reg Client Reg Process
INPUT 	: Process ID, Reg Client Reg Process Update Array
OUTPUT 	: Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_client_reg_process($process_id,$reg_client_reg_process_update_data)
{
	if(array_key_exists("process_master_id",$reg_client_reg_process_update_data))
	{	
		$process_master_id = $reg_client_reg_process_update_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$reg_client_reg_process_update_data))
	{	
		$request_id = $reg_client_reg_process_update_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$reg_client_reg_process_update_data))
	{	
		$process_start_date = $reg_client_reg_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$reg_client_reg_process_update_data))
	{	
		$process_end_date = $reg_client_reg_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$reg_client_reg_process_update_data))
	{	
		$active = $reg_client_reg_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_client_reg_process_update_data))
	{	
		$remarks = $reg_client_reg_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_client_reg_process_update_data))
	{	
		$added_by = $reg_client_reg_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_client_reg_process_update_data))
	{	
		$added_on = $reg_client_reg_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_client_reg_process_update_uquery_base = "update reg_client_reg_process set";  
	
	$reg_client_reg_process_update_uquery_set = "";
	
	$reg_client_reg_process_update_uquery_where = " where reg_client_reg_process_id = :process_id";
	
	$reg_client_reg_process_update_udata = array(":process_id"=>$process_id);
	
	$filter_count = 0;
	
	if($process_master_id != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_master_id = :process_master_id,";
		$reg_client_reg_process_update_udata[":process_master_id"] = $process_master_id;
		$filter_count++;
	}
	
	if($request_id != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_request_id = :request_id,";
		$reg_client_reg_process_update_udata[":request_id"] = $request_id;
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_start_date = :process_start_date,";
		$reg_client_reg_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_end_date = :process_end_date,";
		$reg_client_reg_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_active = :active,";
		$reg_client_reg_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_remarks = :remarks,";
		$reg_client_reg_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_added_by = :added_by,";
		$reg_client_reg_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_client_reg_process_update_uquery_set = $reg_client_reg_process_update_uquery_set." reg_client_reg_process_added_on = :added_on,";
		$reg_client_reg_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_client_reg_process_update_uquery_set = trim($reg_client_reg_process_update_uquery_set,',');
	}
	
	$reg_client_reg_process_update_uquery = $reg_client_reg_process_update_uquery_base.$reg_client_reg_process_update_uquery_set.$reg_client_reg_process_update_uquery_where;
    
    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_client_reg_process_update_ustatement = $dbConnection->prepare($reg_client_reg_process_update_uquery);		
        
        $reg_client_reg_process_update_ustatement -> execute($reg_client_reg_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Reg Client Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Process Master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_client_process_master($name,$remarks,$added_by)
{
	// Query
   $reg_client_process_master_iquery = "insert into reg_client_process_master (reg_client_process_master_name,reg_client_process_master_active,reg_client_process_master_remarks,reg_client_process_master_added_by,reg_client_process_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $reg_client_process_master_istatement = $dbConnection->prepare($reg_client_process_master_iquery);
        
        // Data
        $reg_client_process_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $reg_client_process_master_istatement->execute($reg_client_process_master_idata);
		$reg_client_process_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_client_process_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Client Process Master list
INPUT 	: Process Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Client Process Master
BY 		: Lakshmi
*/
function db_get_reg_client_process_master($reg_client_process_master_search_data)
{  
	if(array_key_exists("process_master_id",$reg_client_process_master_search_data))
	{
		$process_master_id = $reg_client_process_master_search_data["process_master_id"];
	}
	else
	{
		$process_master_id= "";
	}
	
	if(array_key_exists("name",$reg_client_process_master_search_data))
	{
		$name = $reg_client_process_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("process_name_check",$reg_client_process_master_search_data))
	{
		$process_name_check = $reg_client_process_master_search_data["process_name_check"];
	}
	else
	{
		$process_name_check = "";
	}
	
	if(array_key_exists("active",$reg_client_process_master_search_data))
	{
		$active = $reg_client_process_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_client_process_master_search_data))
	{
		$added_by = $reg_client_process_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$reg_client_process_master_search_data))
	{
		$start_date= $reg_client_process_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$reg_client_process_master_search_data))
	{
		$end_date= $reg_client_process_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_reg_client_process_master_list_squery_base = "select * from reg_client_process_master RCPM inner join users U on U.user_id = RCPM.reg_client_process_master_added_by";
	
	$get_reg_client_process_master_list_squery_where = "";
	$get_reg_client_process_master_list_squery_order_by = " order by reg_client_process_master_name ASC";
	
	$filter_count = 0;
	
	// Data
	$get_reg_client_process_master_list_sdata = array();
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_id = :process_master_id";
		}
		else
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." and reg_client_process_master_id = :process_master_id";
		}
		
		// Data
		$get_reg_client_process_master_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($process_name_check == '1')
			{
				$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_name = :name";		
				// Data
				$get_reg_client_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_name like :name";						

				// Data
				$get_reg_client_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_name like :name";						

				// Data
				$get_reg_client_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($process_name_check == '1')
			{
				$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." and reg_client_process_master_name = :name";	
					
				// Data
				$get_reg_client_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." or reg_client_process_master_name like :name";				
				// Data
				$get_reg_client_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." and reg_client_process_master_name like :name";
				
				// Data
				$get_reg_client_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_active = :active";
		}
		else
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." and reg_client_process_master_active = :active";
		}
		
		// Data
		$get_reg_client_process_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_added_by = :added_by";
		}
		else
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." and reg_client_process_master_added_by = :added_by";
		}
		
		//Data
		$get_reg_client_process_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." and reg_client_process_master_added_on >= :start_date";
		}
		
		//Data
		$get_reg_client_process_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." where reg_client_process_master_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_reg_client_process_master_list_squery_where = $get_reg_client_process_master_list_squery_where." and reg_client_process_master_added_on <= :end_date";
		}
		
		//Data
		$get_reg_client_process_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_client_process_master_list_squery = $get_reg_client_process_master_list_squery_base.$get_reg_client_process_master_list_squery_where.$get_reg_client_process_master_list_squery_order_by;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_client_process_master_list_sstatement = $dbConnection->prepare($get_reg_client_process_master_list_squery);
		
		$get_reg_client_process_master_list_sstatement -> execute($get_reg_client_process_master_list_sdata);
		
		$get_reg_client_process_master_list_sdetails = $get_reg_client_process_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_client_process_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_client_process_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_client_process_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update Reg Client Process Master
INPUT 	: Process Master ID, Reg Client Process Master Update Array
OUTPUT 	: Process Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_client_process_master($process_master_id,$reg_client_process_master_update_data)
{
	if(array_key_exists("name",$reg_client_process_master_update_data))
	{	
		$name = $reg_client_process_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$reg_client_process_master_update_data))
	{	
		$active = $reg_client_process_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_client_process_master_update_data))
	{	
		$remarks = $reg_client_process_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_client_process_master_update_data))
	{	
		$added_by = $reg_client_process_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_client_process_master_update_data))
	{	
		$added_on = $reg_client_process_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_client_process_master_update_uquery_base = "update reg_client_process_master set ";  
	
	$reg_client_process_master_update_uquery_set = "";
	
	$reg_client_process_master_update_uquery_where = " where reg_client_process_master_id = :process_master_id";
	
	$reg_client_process_master_update_udata = array(":process_master_id"=>$process_master_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$reg_client_process_master_update_uquery_set = $reg_client_process_master_update_uquery_set." reg_client_process_master_name = :name,";
		$reg_client_process_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_client_process_master_update_uquery_set = $reg_client_process_master_update_uquery_set." reg_client_process_master_active = :active,";
		$reg_client_process_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_client_process_master_update_uquery_set = $reg_client_process_master_update_uquery_set." reg_client_process_master_remarks = :remarks,";
		$reg_client_process_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_client_process_master_update_uquery_set = $reg_client_process_master_update_uquery_set." reg_client_process_master_added_by = :added_by,";
		$reg_client_process_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_client_process_master_update_uquery_set = $reg_client_process_master_update_uquery_set." reg_client_process_master_added_on = :added_on,";
		$reg_client_process_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_client_process_master_update_uquery_set = trim($reg_client_process_master_update_uquery_set,',');
	}
	
	$reg_client_process_master_update_uquery = $reg_client_process_master_update_uquery_base.$reg_client_process_master_update_uquery_set.$reg_client_process_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_client_process_master_update_ustatement = $dbConnection->prepare($reg_client_process_master_update_uquery);		
        
        $reg_client_process_master_update_ustatement -> execute($reg_client_process_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_master_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Reg Client Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function db_add_reg_client_document($process_id,$file_path_id,$remarks,$added_by)
{
	// Query
   $reg_client_document_iquery = "insert into reg_client_document
   (reg_client_document_process_id,reg_client_document_file_path,reg_client_document_active,reg_client_document_remarks,reg_client_document_added_by,reg_client_document_added_on) values(:process_id,:file_path_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $reg_client_document_istatement = $dbConnection->prepare($reg_client_document_iquery);
        
        // Data
        $reg_client_document_idata = array(':process_id'=>$process_id,':file_path_id'=>$file_path_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $reg_client_document_istatement->execute($reg_client_document_idata);
		$reg_client_document_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $reg_client_document_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Reg Client Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Reg Client Document
BY 		: Lakshmi
*/
function db_get_reg_client_document($reg_client_document_search_data)
{  
	if(array_key_exists("document_id",$reg_client_document_search_data))
	{
		$document_id = $reg_client_document_search_data["document_id"];
	}
	else
	{
		$document_id = "";
	}
	
	if(array_key_exists("process_id",$reg_client_document_search_data))
	{
		$process_id = $reg_client_document_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("request_id",$reg_client_document_search_data))
	{
		$request_id = $reg_client_document_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("file_path_id",$reg_client_document_search_data))
	{
		$file_path_id = $reg_client_document_search_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$reg_client_document_search_data))
	{
		$active = $reg_client_document_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$reg_client_document_search_data))
	{
		$added_by = $reg_client_document_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$reg_client_document_search_data))
	{
		$start_date = $reg_client_document_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$reg_client_document_search_data))
	{
		$end_date = $reg_client_document_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_reg_client_document_list_squery_base = "select * from reg_client_document RCD inner join users U on U.user_id = RCD.reg_client_document_added_by inner join reg_client_reg_process RCRP on RCRP.reg_client_reg_process_id=RCD.reg_client_document_process_id inner join reg_client_request RCR on RCR.reg_client_request_id=
	RCRP.reg_client_reg_process_request_id inner join reg_client_process_master RCPM on RCPM.reg_client_process_master_id=RCRP.reg_client_reg_process_master_id";
	
	$get_reg_client_document_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_reg_client_document_list_sdata = array();
	
	if($document_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where reg_client_document_id = :document_id";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and reg_client_document_id = :document_id";				
		}
		
		// Data
		$get_reg_client_document_list_sdata[':document_id'] = $document_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where reg_client_document_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and reg_client_document_process_id = :process_id";				
		}
		
		// Data
		$get_reg_client_document_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where RCRP.reg_client_reg_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and RCRP.reg_client_reg_process_request_id = :request_id";				
		}
		
		// Data
		$get_reg_client_document_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where reg_client_document_file_path = :file_path_id";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and reg_client_document_file_path = :file_path_id";				
		}
		
		// Data
		$get_reg_client_document_list_sdata[':file_path_id'] = $file_path_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where reg_client_document_active = :active";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and reg_client_document_active = :active";				
		}
		
		// Data
		$get_reg_client_document_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where reg_client_document_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and reg_client_document_added_by = :added_by";
		}
		
		//Data
		$get_reg_client_document_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where reg_client_document_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and reg_client_document_added_on >= :start_date";				
		}
		
		//Data
		$get_reg_client_document_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." where reg_client_document_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reg_client_document_list_squery_where = $get_reg_client_document_list_squery_where." and reg_client_document_added_on <= :end_date";
		}
		
		//Data
		$get_reg_client_document_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reg_client_document_list_squery = " order by reg_client_document_added_on DESC";
	$get_reg_client_document_list_squery = $get_reg_client_document_list_squery_base.$get_reg_client_document_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reg_client_document_list_sstatement = $dbConnection->prepare($get_reg_client_document_list_squery);
		
		$get_reg_client_document_list_sstatement -> execute($get_reg_client_document_list_sdata);
		
		$get_reg_client_document_list_sdetails = $get_reg_client_document_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reg_client_document_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reg_client_document_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reg_client_document_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To update Reg Client Document 
INPUT 	: Document ID, Reg Client Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_reg_client_document($document_id,$reg_client_document_update_data)
{
	
	if(array_key_exists("process_id",$reg_client_document_update_data))
	{	
		$process_id = $reg_client_document_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$reg_client_document_update_data))
	{	
		$file_path_id = $reg_client_document_update_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$reg_client_document_update_data))
	{	
		$active = $reg_client_document_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$reg_client_document_update_data))
	{	
		$remarks = $reg_client_document_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$reg_client_document_update_data))
	{	
		$added_by = $reg_client_document_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$reg_client_document_update_data))
	{	
		$added_on = $reg_client_document_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $reg_client_document_update_uquery_base = "update reg_client_document set";  
	
	$reg_client_document_update_uquery_set = "";
	
	$reg_client_document_update_uquery_where = " where reg_client_document_id = :document_id";
	
	$reg_client_document_update_udata = array(":document_id"=>$document_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$reg_client_document_update_uquery_set = $reg_client_document_update_uquery_set." reg_client_document_process_id = :process_id,";
		$reg_client_document_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		$reg_client_document_update_uquery_set = $reg_client_document_update_uquery_set." reg_client_document_file_path = :file_path_id,";
		$reg_client_document_update_udata[":file_path_id"] = $file_path_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$reg_client_document_update_uquery_set = $reg_client_document_update_uquery_set." reg_client_document_active = :active,";
		$reg_client_document_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$reg_client_document_update_uquery_set = $reg_client_document_update_uquery_set." reg_client_document_remarks = :remarks,";
		$reg_client_document_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$reg_client_document_update_uquery_set = $reg_client_document_update_uquery_set." reg_client_document_added_by = :added_by,";
		$reg_client_document_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$reg_client_document_update_uquery_set = $reg_client_document_update_uquery_set." reg_client_document_added_on = :added_on,";
		$reg_client_document_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$reg_client_document_update_uquery_set = trim($reg_client_document_update_uquery_set,',');
	}
	
	$reg_client_document_update_uquery = $reg_client_document_update_uquery_base.$reg_client_document_update_uquery_set.$reg_client_document_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $reg_client_document_update_ustatement = $dbConnection->prepare($reg_client_document_update_uquery);		
        
        $reg_client_document_update_ustatement -> execute($reg_client_document_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $document_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

?>
