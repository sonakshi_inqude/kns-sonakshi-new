<?php
/* FILE HEADER - START */

// LAST UPDATED BY: Nitin Kashyap
// LAST UPDATED ON: 7th June 2015

/* FILE HEADER - END */
/* TBD - START */

// 1. Password Reset Request
// 2. Change Password
// 3. Get email for change password

/* TBD - END*/
/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once ($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'status_codes.php');

include_once ($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'data_access' . DIRECTORY_SEPARATOR . 'connection.php');

include_once ($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'utilities' . DIRECTORY_SEPARATOR . 'utilities_functions.php');

/* INCLUDES - END */
/*

PURPOSE : To add new user

INPUT 	: Name, email id, password, deparment, location, role, added by

OUTPUT 	: User id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_user($user_name, $user_email_id, $user_password, $user_role, $department, $user_location, $user_manager, $added_by)
	{

	// Query

	$user_iquery = "insert into users (user_id,user_name,user_email_id,user_password,user_role,user_department,user_location,user_manager,user_active,user_added_by,user_added_on) values (:user_id,:name,:email,:password,:role,:department,:location,:manager,:active,:added_by,:now)";
	try
		{
		$dbConnection = get_conn_handle();
		$user_istatement = $dbConnection->prepare($user_iquery);

		// Data

		$user_id = generate_unique_id();
		$user_idata = array(
			':user_id' => $user_id,
			':name' => $user_name,
			':email' => $user_email_id,
			':password' => $user_password,
			':role' => $user_role,
			':department' => $department,
			':location' => $user_location,
			':manager' => $user_manager,
			':active' => '1',
			':added_by' => $added_by,
			':now' => date("Y-m-d H:i:s")
		);
		$dbConnection->beginTransaction();
		$user_istatement->execute($user_idata);
		$dbConnection->commit();
		$return["status"] = SUCCESS;
		$return["data"] = $user_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

function db_get_designation_list()
	{
	$query = "select * from designation";
	$get_user_list_sdata = array();
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_salary_bank_list()
	{
	$query = "select * from salary_bank_master";
	$get_user_list_sdata = array();
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_persona($persona_user_id)
	{
	$query = "select * from persona where persona_user_id = " . $persona_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':persona_user_id'] = $persona_user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_bank_details($bank_id)
	{
	$query = "select * from salary_bank_master where bank_id = " . $bank_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':bank_id'] = $bank_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_mobile_deduction($user_id, $start_date, $end_date)
	{
	$query = "select * from mobile_deduction where mobile_deduction_user_id = " . $user_id . " and mobile_deduction_bill_date >= '" . $start_date . "' and mobile_deduction_bill_date <= '" . $end_date . "' ";
	$get_user_list_sdata = array();
	$get_user_list_sdata[':mobile_deduction_user_id'] = $user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_pli($pli_user_id, $start_date, $end_date)
	{
	$query = "select pli_amount from pli_form where pli_user_id = " . $pli_user_id . " and pli_date >= '" . $start_date . "' and pli_date <= '" . $end_date . "' ";
	$get_user_list_sdata = array();
	$get_user_list_sdata[':pli_user_id'] = $pli_user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_pending_loan($user_id)
	{
	$query = "select * from pending_loan where pending_loan_user_id = " . $user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':pending_loan_user_id'] = $user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_persona_basic_details($persona_user_id)
	{
	$query = "select * from persona_basic_details where user_id = " . $persona_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':user_id'] = $persona_user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_get_persona_salary($persona_user_id)
	{
	$query = "select * from persona_salary where persona_salary_user_id = " . $persona_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':persona_salary_user_id'] = $persona_user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_persona_drop($persona_user_id)
	{
	$query = "delete from persona where persona_user_id = " . $persona_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':persona_user_id'] = $persona_user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_pending_loan_drop($user_id)
	{
	$query = "delete from pending_loan where pending_loan_user_id = " . $user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':pending_loan_user_id'] = $user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_persona_salary_drop($persona_user_id)
	{
	$query = "delete from persona_salary where persona_salary_user_id = " . $persona_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':persona_salary_user_id'] = $persona_user_id;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_mb_deduction_exists($user_id, $bill_date)
	{
	$query = "select mobile_deduction_user_id from mobile_deduction where mobile_deduction_user_id = " . $user_id . " and mobile_deduction_bill_date >= " . $bill_date . " and " . $bill_date . " <= mobile_deduction_bill_date";
	$get_user_list_sdata = array();
	$get_user_list_sdata[':mobile_deduction_user_id'] = $user_id;

	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = 'FAILURE';
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = 'DB_NO_RECORD';
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = 'DB_RECORD_ALREADY_EXISTS';
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_persona_exists($persona_user_id)
	{
	$query = "select persona_user_id from persona where persona_user_id = " . $persona_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':persona_user_id'] = $persona_user_id;

	// return $query;

	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_pending_loan_exists($pending_loan_user_id)
	{
	$query = "select pending_loan_user_id from pending-loan where pending_loan_user_id = " . $pending_loan_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':pending_loan_user_id'] = $pending_loan_user_id;

	// return $query;

	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_pli_exists($pli_user_id)
	{
	$query = "select pli_user_id from pli_form where pli_user_id = " . $pli_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':pli_user_id'] = $pli_user_id;

	// return $query;

	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

// check Persona salary a/c exists

function db_persona_salary_exists($persona_salary_user_id)
	{
	$query = "select persona_salary_user_id from persona_salary where persona_salary_user_id = " . $persona_salary_user_id;
	$get_user_list_sdata = array();
	$get_user_list_sdata[':persona_user_id'] = $persona_salary_user_id;

	// return $query;

	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($query);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = 'FAILURE';
		$return["data"] = "";
		}

	return $return;
	}

function db_add_persona($persona_department_id, $persona_user_id, $persona_designation, $persona_dob, $persona_user_phone, $persona_gender, $persona_maritial_status, $persona_blood_group, $persona_pan_number, $persona_aadhar_number, $persona_present_address, $persona_present_city, $persona_present_state, $persona_present_pincode, $persona_permanent_address, $persona_permanent_city, $persona_permanent_state, $persona_permanent_pincode, $persona_company_phone, $persona_sim_number, $persona_voice_limit, $persona_data_limit, $persona_pf_number, $persona_insurance_name, $persona_insurance_number, $persona_insurance_amount, $persona_uan, $persona_esi_number)
	{

	// Query

	$user_iquery = "insert into persona (persona_department_id, persona_user_id, persona_designation, persona_dob, persona_user_phone, persona_gender, persona_maritial_status, persona_blood_group, persona_pan_number, persona_aadhar_number, persona_present_address, persona_present_city, persona_present_state, persona_present_pincode, persona_permanent_address, persona_permanent_city, persona_permanent_state, persona_permanent_pincode, persona_company_phone, persona_sim_number, persona_voice_limit, persona_data_limit, persona_pf_number, persona_insurance_name, persona_insurance_number, persona_insurance_amount, persona_uan, persona_esi_number) values (:persona_department_id,
    :persona_user_id,
    :persona_designation,
    :persona_dob,
    :persona_user_phone,
    :persona_gender,
    :persona_maritial_status,
    :persona_blood_group,
    :persona_pan_number,
    :persona_aadhar_number,
    :persona_present_address,
    :persona_present_city,
    :persona_present_state,
    :persona_present_pincode,
    :persona_permanent_address,
    :persona_permanent_city,
    :persona_permanent_state,
    :persona_permanent_pincode,
    :persona_company_phone,
    :persona_sim_number,
    :persona_voice_limit,
    :persona_data_limit,
    :persona_pf_number,
    :persona_insurance_name,
    :persona_insurance_number,
    :persona_insurance_amount,
    :persona_uan,
    :persona_esi_number
    )";
	try
		{
		$dbConnection = get_conn_handle();
		$user_istatement = $dbConnection->prepare($user_iquery);

		// Data
		// $user_id = generate_unique_id();

		$user_idata = array(
			':persona_department_id' => $persona_department_id,
			':persona_user_id' => $persona_user_id,
			':persona_designation' => $persona_designation,
			':persona_dob' => $persona_dob,
			':persona_user_phone' => $persona_user_phone,
			':persona_gender' => $persona_gender,
			':persona_maritial_status' => $persona_maritial_status,
			':persona_blood_group' => $persona_blood_group,
			':persona_pan_number' => $persona_pan_number,
			':persona_aadhar_number' => $persona_aadhar_number,
			':persona_present_address' => $persona_present_address,
			':persona_present_city' => $persona_present_city,
			':persona_present_state' => $persona_present_state,
			':persona_present_pincode' => $persona_present_pincode,
			':persona_permanent_address' => $persona_permanent_address,
			':persona_permanent_city' => $persona_permanent_city,
			':persona_permanent_state' => $persona_permanent_state,
			':persona_permanent_pincode' => $persona_permanent_pincode,
			':persona_company_phone' => $persona_company_phone,
			':persona_sim_number' => $persona_sim_number,
			':persona_voice_limit' => $persona_voice_limit,
			':persona_data_limit' => $persona_data_limit,
			':persona_pf_number' => $persona_pf_number,
			':persona_insurance_name' => $persona_insurance_name,
			':persona_insurance_number' => $persona_insurance_number,
			':persona_insurance_amount' => $persona_insurance_amount,
			':persona_uan' => $persona_uan,
			':persona_esi_number' => $persona_esi_number
		);
		$dbConnection->beginTransaction();
		$user_istatement->execute($user_idata);
		$dbConnection->commit();
		$return["status"] = 'SUCCESS';
		$return["data"] = $persona_department_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

function db_add_pli($pli_department_id, $pli_user_id, $pli_amount, $pli_date, $pli_remarks)
	{

	// Query

	$user_iquery = "insert into pli_form (pli_department_id, pli_user_id, pli_amount, pli_date, pli_remarks) values (:pli_department_id,
    :pli_user_id,
    :pli_amount,
    :pli_date,
    :pli_remarks
    )";
	try
		{
		$dbConnection = get_conn_handle();
		$user_istatement = $dbConnection->prepare($user_iquery);

		// Data
		// $user_id = generate_unique_id();

		$user_idata = array(
			':pli_department_id' => $pli_department_id,
			':pli_user_id' => $pli_user_id,
			':pli_amount' => $pli_amount,
			':pli_date' => $pli_date,
			':pli_remarks' => $pli_remarks
		);
		$dbConnection->beginTransaction();
		$user_istatement->execute($user_idata);
		$dbConnection->commit();
		$return["status"] = 'SUCCESS';
		$return["data"] = $pli_department_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

function db_add_mobile_deduction($mobile_deduction_department_id, $mobile_deduction_user_id, $mobile_deduction_bill_date, $mobile_deduction_voice_bill, $mobile_deduction_data_bill, $mobile_deduction_remarks)
	{

	// Query

	$user_iquery = "insert into mobile_deduction (mobile_deduction_department_id, mobile_deduction_user_id, mobile_deduction_bill_date, mobile_deduction_voice_bill, mobile_deduction_data_bill, mobile_deduction_remarks) values (:mobile_deduction_department_id,
    :mobile_deduction_user_id,
    :mobile_deduction_bill_date,
    :mobile_deduction_voice_bill,
    :mobile_deduction_data_bill,
    :mobile_deduction_remarks
    )";
	try
		{
		$dbConnection = get_conn_handle();
		$user_istatement = $dbConnection->prepare($user_iquery);

		// Data
		// $user_id = generate_unique_id();

		$user_idata = array(
			':mobile_deduction_department_id' => $mobile_deduction_department_id,
			':mobile_deduction_user_id' => $mobile_deduction_user_id,
			':mobile_deduction_bill_date' => $mobile_deduction_bill_date,
			':mobile_deduction_voice_bill' => $mobile_deduction_voice_bill,
			':mobile_deduction_data_bill' => $mobile_deduction_data_bill,
			':mobile_deduction_remarks' => $mobile_deduction_remarks
		);
		$dbConnection->beginTransaction();
		$user_istatement->execute($user_idata);
		$dbConnection->commit();
		$return["status"] = 'SUCCESS';
		$return["data"] = $mobile_deduction_department_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

function db_add_pending_loan_amount($pending_loan_department_id, $pending_loan_user_id, $pending_loan_total_amount, $pending_loan_total_tenure, $pending_loan_pending_amount, $pending_loan_remarks)
	{

	// Query

	$user_iquery = "insert into pending_loan (pending_loan_department_id, pending_loan_user_id, pending_loan_total_amount, pending_loan_total_tenure, pending_loan_pending_amount, pending_loan_remarks) values (:pending_loan_department_id,
    :pending_loan_user_id,
    :pending_loan_total_amount,
    :pending_loan_total_tenure,
    :pending_loan_pending_amount,
    :pending_loan_remarks
    )";
	try
		{
		$dbConnection = get_conn_handle();
		$user_istatement = $dbConnection->prepare($user_iquery);

		// Data
		// $user_id = generate_unique_id();

		$user_idata = array(
			':pending_loan_department_id' => $pending_loan_department_id,
			':pending_loan_user_id' => $pending_loan_user_id,
			':pending_loan_total_amount' => $pending_loan_total_amount,
			':pending_loan_total_tenure' => $pending_loan_total_tenure,
			':pending_loan_pending_amount' => $pending_loan_pending_amount,
			':pending_loan_remarks' => $pending_loan_remarks
		);
		$dbConnection->beginTransaction();
		$user_istatement->execute($user_idata);
		$dbConnection->commit();
		$return["status"] = 'SUCCESS';
		$return["data"] = $pending_loan_department_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

// Persona Salary Details insert

function db_add_persona_salary($persona_department_id, $persona_user_id, $basic, $hra, $conveyance_allowance, $special_allowance, $educational_allowance, $medical_reimbursement, $pf_amount, $statutory_bonus, $gratuity, $lta, $pli, $ddl_company, $bank_name, $bank_account_number, $ifsc_code, $bank_branch, $bank_city, $esi_employee_amount, $esi_company_amount)
	{

	// Query

	$query = "insert into persona_salary (persona_salary_department_id,
    persona_salary_user_id,
    persona_salary_basic,
    persona_salary_hra,
    persona_salary_conveyance_allowance,
    persona_salary_special_allowance,
    persona_salary_educational_allowance,
    persona_salary_medical_reimbursement,
    persona_salary_pf_amount,
    persona_salary_statutory_bonus,
    persona_salary_gratuity,
    persona_salary_lta,
    persona_salary_pli,
    persona_salary_company,
    persona_salary_bank_name,
    persona_salary_bank_account_number,
    persona_salary_ifsc_code,
    persona_salary_bank_branch,
    persona_salary_bank_city,
    persona_esi_employee_amount,
    persona_esi_company_amount
) values (:persona_salary_department_id,
    :persona_salary_user_id,
    :persona_salary_basic,
    :persona_salary_hra,
    :persona_salary_conveyance_allowance,
    :persona_salary_special_allowance,
    :persona_salary_educational_allowance,
    :persona_salary_medical_reimbursement,
    :persona_salary_pf_amount,
    :persona_salary_statutory_bonus,
    :persona_salary_gratuity,
    :persona_salary_lta,
    :persona_salary_pli,
    :persona_salary_company,
    :persona_salary_bank_name,
    :persona_salary_bank_account_number,
    :persona_salary_ifsc_code,
    :persona_salary_bank_branch,
    :persona_salary_bank_city,
    :persona_esi_employee_amount,
    :persona_esi_company_amount
    )";
	$data = array(
		':persona_salary_department_id' => $persona_department_id,
		':persona_salary_user_id' => $persona_user_id,
		':persona_salary_basic' => $basic,
		':persona_salary_hra' => $hra,
		':persona_salary_conveyance_allowance' => $conveyance_allowance,
		':persona_salary_special_allowance' => $special_allowance,
		':persona_salary_educational_allowance' => $educational_allowance,
		':persona_salary_medical_reimbursement' => $medical_reimbursement,
		':persona_salary_pf_amount' => $pf_amount,
		':persona_salary_statutory_bonus' => $statutory_bonus,
		':persona_salary_gratuity' => $gratuity,
		':persona_salary_lta' => $lta,
		':persona_salary_pli' => $pli,
		':persona_salary_company' => $ddl_company,
		':persona_salary_bank_name' => $bank_name,
		':persona_salary_bank_account_number' => $bank_account_number,
		':persona_salary_ifsc_code' => $ifsc_code,
		':persona_salary_bank_branch' => $bank_branch,
		':persona_salary_bank_city' => $bank_city,
		':persona_esi_employee_amount' => $esi_employee_amount,
		':persona_esi_company_amount' => $esi_company_amount
	);
	try
		{
		$dbConnection = get_conn_handle();
		$user_istatement = $dbConnection->prepare($query);

		// Data
		// $user_id = generate_unique_id();

		$dbConnection->beginTransaction();
		$user_istatement->execute($data);
		$dbConnection->commit();
		$return["status"] = 'SUCCESS';
		$return["data"] = $persona_department_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To get user details

INPUT 	: User id

OUTPUT 	: User Details, success or failure message

BY 		: Nitin Kashyap

*/

function db_get_user_details($user_id)
	{

	// Query

	$get_user_det_squery = "select * from users where user_id=:user_id";

	// Data

	$get_user_det_sdata = array(
		':user_id' => $user_id
	);
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_sstatement = $dbConnection->prepare($get_user_det_squery);
		$get_user_sstatement->execute($get_user_det_sdata);
		$get_user_sdetails = $get_user_sstatement->fetch();
		if (FALSE === $get_user_sdetails)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = $e->getMessage();
		}

	return $return;
	}

/*

PURPOSE : To get user details list

INPUT 	: User ID, User Name, User Email, User Role, Start Date, End Date, Active, Department, Manager User ID

OUTPUT 	: User List

BY 		: Nitin Kashyap

*/

function db_get_user_list($user_id, $user_name, $user_email, $user_role, $start_date, $end_date, $user_active, $user_department = '', $user_manager = '')
	{
	$get_user_list_squery_base = "select * from users U left outer join general_task_department_master GTDM on GTDM.general_task_department_id = U.user_department left outer join stock_location_master SLM on SLM.stock_location_id = U.user_location";
	$get_user_list_squery_where = "";
	$filter_count = 0;

	// Data

	$get_user_list_sdata = array();
	if ($user_id != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_id=:user_id";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_id=:user_id";
			}

		// Data

		$get_user_list_sdata[':user_id'] = $user_id;
		$filter_count++;
		}

	if ($user_name != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_name=:user_name";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_name=:user_name";
			}

		// Data

		$get_user_list_sdata[':user_name'] = $user_name;
		$filter_count++;
		}

	if ($user_email != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_email_id=:user_email";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_email_id=:user_email";
			}

		// Data

		$get_user_list_sdata[':user_email'] = $user_email;
		$filter_count++;
		}

	if ($user_role != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_role=:user_role";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_role=:user_role";
			}

		// Data

		$get_user_list_sdata[':user_role'] = $user_role;
		$filter_count++;
		}

	if ($user_manager != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_manager=:user_manager";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_manager=:user_manager";
			}

		// Data

		$get_user_list_sdata[':user_manager'] = $user_manager;
		$filter_count++;
		}

	if ($start_date != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_added_date_time >= :start_date";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_added_date_time >= :start_date";
			}

		// Data

		$get_user_list_sdata[':start_date'] = $start_date;
		$filter_count++;
		}

	if ($end_date != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_added_date_time >= :end_date";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_added_date_time >= :end_date";
			}

		// Data

		$get_user_list_sdata[':end_date'] = $end_date;
		$filter_count++;
		}

	if ($user_active != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_active=:user_active";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_active=:user_active";
			}

		// Data

		$get_user_list_sdata[':user_active'] = $user_active;
		$filter_count++;
		}

	if ($user_department != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " where user_department = :department";
			}
		  else
			{

			// Query

			$get_user_list_squery_where = $get_user_list_squery_where . " and user_department = :department";
			}

		// Data

		$get_user_list_sdata[':department'] = $user_department;
		$filter_count++;
		}

	$get_user_list_squery_order = " order by user_name asc";
	$get_user_list_squery = $get_user_list_squery_base . $get_user_list_squery_where . $get_user_list_squery_order;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_list_sstatement = $dbConnection->prepare($get_user_list_squery);
		$get_user_list_sstatement->execute($get_user_list_sdata);
		$get_user_list_sdetails = $get_user_list_sstatement->fetchAll();
		if (FALSE === $get_user_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

function db_add_pw_reset_request($email, $ip_addr, $random_code)
	{

	// Query

	$pw_reset_iquery = "insert into user_password_reset_requests (user_password_reset_request_email,user_password_reset_request_date_time,user_password_reset_request_ip,user_password_reset_request_code) values (:email,:now,:ip_addr,:code)";
	try
		{
		$dbConnection = get_conn_handle();
		$pw_reset_istatement = $dbConnection->prepare($pw_reset_iquery);

		// Data

		$pw_reset_idata = array(
			':email' => $email,
			':now' => date("Y-m-d H:i:s") ,
			'ip_addr' => $ip_addr,
			':code' => $random_code
		);
		$dbConnection->beginTransaction();
		$pw_reset_istatement->execute($pw_reset_idata);
		$dbConnection->commit();
		$return["status"] = SUCCESS;
		$return["data"] = "";
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

function db_get_email_for_change_pw($code)
	{

	// Query

	$email_cpw_squery = "select * from user_password_reset_requests where user_password_reset_request_code=:code";

	// Data

	$email_cpw_sdata = array(
		':code' => $code
	);
	try
		{
		$dbConnection = get_conn_handle();
		$email_cpw_sstatement = $dbConnection->prepare($email_cpw_squery);
		$email_cpw_sstatement->execute($email_cpw_sdata);
		$email_cpw_sdetails = $email_cpw_sstatement->fetch();
		if (FALSE === $email_cpw_sdetails)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $email_cpw_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = $e->getMessage();
		}

	return $return;
	}

function db_change_password($user_id, $password)
	{

	// Query

	$password_uquery = "update users set user_password=:password where user_id=:user_id";
	try
		{
		$dbConnection = get_conn_handle();
		$password_ustatement = $dbConnection->prepare($password_uquery);

		// Data

		$password_udata = array(
			':password' => $password,
			':user_id' => $user_id
		);
		$password_ustatement->execute($password_udata);
		$return["status"] = SUCCESS;
		$return["data"] = $user_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

function db_enable_disable_user($user_id, $action)
	{

	// Query

	$user_uquery = "update users set user_active=:action where user_id=:user_id";
	try
		{
		$dbConnection = get_conn_handle();
		$user_ustatement = $dbConnection->prepare($user_uquery);

		// Data

		$user_udata = array(
			':action' => $action,
			':user_id' => $user_id
		);
		$user_ustatement->execute($user_udata);
		$return["status"] = SUCCESS;
		$return["data"] = $user_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To update user details

INPUT 	: User ID, User Data Array

OUTPUT 	: User ID; Message of success or failure

BY 		: Nitin Kashyap

*/

function db_update_user_details($user_id, $user_details)
	{

	// Query

	$user_uquery_base = "update users set";
	$user_uquery_set = "";
	$user_uquery_where = " where user_id=:user_id";
	$user_udata = array(
		":user_id" => $user_id
	);
	$filter_count = 0;
	if (array_key_exists("name", $user_details))
		{
		$user_uquery_set = $user_uquery_set . " user_name=:name,";
		$user_udata[":name"] = $user_details["name"];
		$filter_count++;
		}

	if (array_key_exists("email", $user_details))
		{
		$user_uquery_set = $user_uquery_set . " user_email_id=:email,";
		$user_udata[":email"] = $user_details["email"];
		$filter_count++;
		}

	if (array_key_exists("role", $user_details))
		{
		$user_uquery_set = $user_uquery_set . " user_role=:role,";
		$user_udata[":role"] = $user_details["role"];
		$filter_count++;
		}

	if (array_key_exists("department", $user_details))
		{
		$user_uquery_set = $user_uquery_set . " user_department=:department,";
		$user_udata[":department"] = $user_details["department"];
		$filter_count++;
		}

	if (array_key_exists("location", $user_details))
		{
		$user_uquery_set = $user_uquery_set . " user_location=:location,";
		$user_udata[":location"] = $user_details["location"];
		$filter_count++;
		}

	if (array_key_exists("manager", $user_details))
		{
		$user_uquery_set = $user_uquery_set . " user_manager=:mgr,";
		$user_udata[":mgr"] = $user_details["manager"];
		$filter_count++;
		}

	if ($filter_count > 0)
		{
		$user_uquery_set = trim($user_uquery_set, ',');
		}

	$user_uquery = $user_uquery_base . $user_uquery_set . $user_uquery_where;
	try
		{
		$dbConnection = get_conn_handle();
		$user_ustatement = $dbConnection->prepare($user_uquery);
		$user_ustatement->execute($user_udata);
		$return["status"] = SUCCESS;
		$return["data"] = $user_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To add new user login details

INPUT 	: User Id, Date Time,Ip address.

OUTPUT 	: User login schedule id, success or failure message

BY 		: Sonakshi D

*/

function db_add_login_schedule($user_id)
	{

	// Query

	$login_details_iquery = "insert into user_login_schedule (user_login_schedule_user_id,user_login_schedule_login_date_time,user_login_schedule_ip_address) values (:user_id,:date_time,:ip_address)";
	try
		{
		$dbConnection = get_conn_handle();
		$login_details_istatement = $dbConnection->prepare($login_details_iquery);

		// Data

		$plogin_details_idata = array(
			':user_id' => $user_id,
			':date_time' => date("Y-m-d H:i:s") ,
			':ip_address' => GetIP()
		);
		$dbConnection->beginTransaction();
		$login_details_istatement->execute($plogin_details_idata);
		$user_login_schedule_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		$return["status"] = SUCCESS;
		$return["data"] = $user_login_schedule_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To get user login schedule list

INPUT 	: User ID,Ip address.

OUTPUT 	: List of Login Schedule

BY 		: Sonakshi D

*/

function db_get_login_schedule_list($user_id, $start_date_time, $end_date_time, $ip_address)
	{
	$get_login_schedule_list_squery_base = "select * from user_login_schedule ULS inner join users U on U.user_id = ULS.user_login_schedule_user_id";
	$get_login_schedule_list_squery_where = "";
	$filter_count = 0;

	// Data

	$get_login_schedule_list_sdata = array();
	if ($user_id != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " where user_login_schedule_user_id=:user_id";
			}
		  else
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " and user_login_schedule_user_id=:user_id";
			}

		// Data

		$get_login_schedule_list_sdata[':user_id'] = $user_id;
		$filter_count++;
		}

	if ($start_date_time != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " where user_login_schedule_login_date_time >= :start_date_time";
			}
		  else
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " and user_login_schedule_login_date_time >= :start_date_time";
			}

		// Data

		$get_login_schedule_list_sdata[':start_date_time'] = $start_date_time;
		$filter_count++;
		}

	if ($end_date_time != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " where user_login_schedule_login_date_time <= :end_date_time";
			}
		  else
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " and user_login_schedule_login_date_time <= :end_date_time";
			}

		// Data

		$get_login_schedule_list_sdata[':end_date_time'] = $end_date_time;
		$filter_count++;
		}

	if ($ip_address != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " where user_login_schedule_ip_address=:ip_address";
			}
		  else
			{

			// Query

			$get_login_schedule_list_squery_where = $get_login_schedule_list_squery_where . " and user_login_schedule_ip_address=:ip_address";
			}

		// Data

		$get_login_schedule_list_sdata[':ip_address'] = $ip_address;
		$filter_count++;
		}

	$get_login_schedule_squery = $get_login_schedule_list_squery_base . $get_login_schedule_list_squery_where;
	try
		{
		$dbConnection = get_conn_handle();
		$get_login_schedule_sstatement = $dbConnection->prepare($get_login_schedule_squery);
		$get_login_schedule_sstatement->execute($get_login_schedule_list_sdata);
		$get_login_schedule_sdetails = $get_login_schedule_sstatement->fetchAll();
		if (FALSE === $get_login_schedule_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_login_schedule_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_login_schedule_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To add new permission

INPUT 	: User ID, Functionality, Rights

OUTPUT 	: Permission ID, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_user_permission($user_id, $functionality, $rights)
	{

	// Query

	$user_perms_iquery = "insert into permissions_master (permission_user,permission_functionality,permission_crud_type,permission_active,permission_added_on) values (:user_id,:functionality,:crud,:active,:now)";
	try
		{
		$dbConnection = get_conn_handle();
		$user_perms_istatement = $dbConnection->prepare($user_perms_iquery);

		// Data

		$user_perms_idata = array(
			':user_id' => $user_id,
			':functionality' => $functionality,
			':crud' => $rights,
			':active' => '1',
			':now' => date("Y-m-d H:i:s")
		);
		$dbConnection->beginTransaction();
		$user_perms_istatement->execute($user_perms_idata);
		$user_perms_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		$return["status"] = SUCCESS;
		$return["data"] = $user_perms_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To get user perms list

INPUT 	: User ID, Module, Functionality ID, CRUD Type (i.e. Create, Run, Update or Destroy), Active, Is part of menu

OUTPUT 	: User Perms List

BY 		: Nitin Kashyap

*/

function db_get_user_perms_list($user_id, $module, $functionality, $crud_type, $active, $is_menu = '')
	{
	$get_user_perms_list_squery_base = "select * from permissions_master PM inner join functionality_master FM on FM.functionality_master_id = PM.permission_functionality";
	$get_user_perms_list_squery_where = "";
	$filter_count = 0;

	// Data

	$get_user_perms_list_sdata = array();
	if ($user_id != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_user = :user_id";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_user = :user_id";
			}

		// Data

		$get_user_perms_list_sdata[':user_id'] = $user_id;
		$filter_count++;
		}

	if ($module != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where FM.functionality_master_department = :module";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and FM.functionality_master_department = :module";
			}

		// Data

		$get_user_perms_list_sdata[':module'] = $module;
		$filter_count++;
		}

	if ($functionality != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_functionality = :functionality";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_functionality = :functionality";
			}

		// Data

		$get_user_perms_list_sdata[':functionality'] = $functionality;
		$filter_count++;
		}

	if ($crud_type != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_crud_type = :crud_type";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_crud_type = :crud_type";
			}

		// Data

		$get_user_perms_list_sdata[':crud_type'] = $crud_type;
		$filter_count++;
		}

	if ($active != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_active = :active";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_active = :active";
			}

		// Data

		$get_user_perms_list_sdata[':active'] = $active;
		$filter_count++;
		}

	if ($is_menu != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where FM.functionality_master_is_menu = :is_menu";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and FM.functionality_master_is_menu = :is_menu";
			}

		// Data

		$get_user_perms_list_sdata[':is_menu'] = $is_menu;
		$filter_count++;
		}

	$get_user_perms_list_squery_order = ' order by functionality_master_order desc,FM.functionality_master_id asc';
	$get_user_perms_list_squery = $get_user_perms_list_squery_base . $get_user_perms_list_squery_where . $get_user_perms_list_squery_order;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_perms_list_sstatement = $dbConnection->prepare($get_user_perms_list_squery);
		$get_user_perms_list_sstatement->execute($get_user_perms_list_sdata);
		$get_user_perms_list_sdetails = $get_user_perms_list_sstatement->fetchAll();
		if (FALSE === $get_user_perms_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_perms_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_perms_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To get master functions

INPUT 	: Function, Department, URL, Active Status, Added by, Start date, End date

OUTPUT 	: List of master functions

BY 		: Punith

*/

function db_get_functionality_master($function, $department, $url, $active, $added_by, $start_date, $end_date)
	{
	$get_functionality_master_query_base = "select * from functionality_master";
	$get_functionality_master_query_where = "";
	$filter_count = 0;

	// Data

	$get_functionality_master_data = array();
	if ($function != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where functionality_master_function=:function";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and functionality_master_function=:function";
			}

		// Data

		$get_functionality_master_data[':function'] = $function;
		$filter_count++;
		}

	if ($department != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where functionality_master_department=:department";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and functionality_master_department=:department";
			}

		// Data

		$get_functionality_master_data[':department'] = $department;
		$filter_count++;
		}

	if ($url != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where functionality_master_url=:url";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and functionality_master_url=:url";
			}

		// Data

		$get_functionality_master_data[':url'] = $url;
		$filter_count++;
		}

	if ($active != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where functionality_master_active = :active";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and functionality_master_active = :active";
			}

		// Data

		$get_functionality_master_data[':active'] = $active;
		$filter_count++;
		}

	if ($added_by != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where functionality_master_added_by=:added_by";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and functionality_master_added_by=:added_by";
			}

		// Data

		$get_functionality_master_data[':added_by'] = $added_by;
		$filter_count++;
		}

	if ($start_date != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where functionality_master_added_on >= :start_date";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and functionality_master_added_on >= :start_date";
			}

		// Data

		$get_functionality_master_data[':start_date'] = $start_date;
		$filter_count++;
		}

	if ($end_date != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where functionality_master_added_on <= :end_date";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and functionality_master_added_on <= :end_date";
			}

		// Data

		$get_functionality_master_data[':end_date'] = $end_date;
		$filter_count++;
		}

	$get_functionality_master_query = $get_functionality_master_query_base . $get_functionality_master_query_where;
	try
		{
		$dbConnection = get_conn_handle();
		$get_functionality_master_statement = $dbConnection->prepare($get_functionality_master_query);
		$get_functionality_master_statement->execute($get_functionality_master_data);
		$get_functionality_master_details = $get_functionality_master_statement->fetchAll();
		if (FALSE === $get_functionality_master_details)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_functionality_master_details) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_functionality_master_details;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*

PURPOSE : To enable or disble an already set permission

INPUT 	: User ID, Functionality, CRUD Type, Status

OUTPUT 	: Success or Failure

BY 		: Punith

*/

function db_enable_disable_perms($user_id, $functionality, $crud_type, $active)
	{

	// Query

	$user_perms_uquery = "update permissions_master set permission_active=:action where permission_user=:user_id and permission_functionality=:func and permission_crud_type=:crud";
	try
		{
		$dbConnection = get_conn_handle();
		$user_perms_ustatement = $dbConnection->prepare($user_perms_uquery);

		// Data

		$user_perms_udata = array(
			':action' => $active,
			':user_id' => $user_id,
			':func' => $functionality,
			':crud' => $crud_type
		);
		$user_perms_ustatement->execute($user_perms_udata);
		$return["status"] = SUCCESS;
		$return["data"] = $user_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*
PURPOSE : To add new permission for email
INPUT 	: User ID, Functionality, Rights
OUTPUT 	: Permission ID, success or failure message
BY 		: Nitin Kashyap
*/

function db_add_user_email_permission($user_id, $functionality, $rights)
	{

	// Query

	$user_perms_iquery = "insert into email_permissions_master (permission_user,permission_functionality,permission_crud_type,permission_active,permission_added_on) values (:user_id,:functionality,:crud,:active,:now)";
	try
		{
		$dbConnection = get_conn_handle();
		$user_perms_istatement = $dbConnection->prepare($user_perms_iquery);

		// Data

		$user_perms_idata = array(
			':user_id' => $user_id,
			':functionality' => $functionality,
			':crud' => $rights,
			':active' => '1',
			':now' => date("Y-m-d H:i:s")
		);
		$dbConnection->beginTransaction();
		$user_perms_istatement->execute($user_perms_idata);
		$user_perms_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		$return["status"] = SUCCESS;
		$return["data"] = $user_perms_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*
PURPOSE : To get user perms list for email
INPUT 	: User ID, Module, Functionality ID, CRUD Type (i.e. Assignee, Assigner, Manager or Power User), Active
OUTPUT 	: User Perms List for Email
BY 		: Nitin Kashyap
*/

function db_get_user_email_perms_list($user_id, $module, $functionality, $crud_type, $active)
	{
	$get_user_perms_list_squery_base = "select * from email_permissions_master PM inner join email_master EM on EM.email_master_id = PM.permission_functionality";
	$get_user_perms_list_squery_where = "";
	$filter_count = 0;

	// Data

	$get_user_perms_list_sdata = array();
	if ($user_id != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_user = :user_id";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_user = :user_id";
			}

		// Data

		$get_user_perms_list_sdata[':user_id'] = $user_id;
		$filter_count++;
		}

	if ($module != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where EM.email_master_department = :module";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and EM.email_master_department = :module";
			}

		// Data

		$get_user_perms_list_sdata[':module'] = $module;
		$filter_count++;
		}

	if ($functionality != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_functionality = :functionality";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_functionality = :functionality";
			}

		// Data

		$get_user_perms_list_sdata[':functionality'] = $functionality;
		$filter_count++;
		}

	if ($crud_type != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_crud_type = :crud_type";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_crud_type = :crud_type";
			}

		// Data

		$get_user_perms_list_sdata[':crud_type'] = $crud_type;
		$filter_count++;
		}

	if ($active != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " where permission_active = :active";
			}
		  else
			{

			// Query

			$get_user_perms_list_squery_where = $get_user_perms_list_squery_where . " and permission_active = :active";
			}

		// Data

		$get_user_perms_list_sdata[':active'] = $active;
		$filter_count++;
		}

	$get_user_perms_list_squery_order = ' order by EM.email_master_id';
	$get_user_perms_list_squery = $get_user_perms_list_squery_base . $get_user_perms_list_squery_where . $get_user_perms_list_squery_order;
	try
		{
		$dbConnection = get_conn_handle();
		$get_user_perms_list_sstatement = $dbConnection->prepare($get_user_perms_list_squery);
		$get_user_perms_list_sstatement->execute($get_user_perms_list_sdata);
		$get_user_perms_list_sdetails = $get_user_perms_list_sstatement->fetchAll();
		if (FALSE === $get_user_perms_list_sdetails)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_user_perms_list_sdetails) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_user_perms_list_sdetails;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*
PURPOSE : To get email master functions
INPUT 	: Function, Department, Active Status, Added by, Start date, End date
OUTPUT 	: List of email master functions
BY 		: Punith
*/

function db_get_email_functionality_master($function, $department, $active, $added_by, $start_date = '', $end_date = '')
	{
	$get_functionality_master_query_base = "select * from email_master";
	$get_functionality_master_query_where = "";
	$filter_count = 0;

	// Data

	$get_functionality_master_data = array();
	if ($function != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where email_master_function=:function";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and email_master_function=:function";
			}

		// Data

		$get_functionality_master_data[':function'] = $function;
		$filter_count++;
		}

	if ($department != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where email_master_department=:department";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and email_master_department=:department";
			}

		// Data

		$get_functionality_master_data[':department'] = $department;
		$filter_count++;
		}

	if ($active != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where email_master_active = :active";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and email_master_active = :active";
			}

		// Data

		$get_functionality_master_data[':active'] = $active;
		$filter_count++;
		}

	if ($added_by != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where email_master_added_by=:added_by";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and email_master_added_by=:added_by";
			}

		// Data

		$get_functionality_master_data[':added_by'] = $added_by;
		$filter_count++;
		}

	if ($start_date != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where email_master_added_on >= :start_date";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and email_master_added_on >= :start_date";
			}

		// Data

		$get_functionality_master_data[':start_date'] = $start_date;
		$filter_count++;
		}

	if ($end_date != "")
		{
		if ($filter_count == 0)
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " where email_master_added_on <= :end_date";
			}
		  else
			{

			// Query

			$get_functionality_master_query_where = $get_functionality_master_query_where . " and email_master_added_on <= :end_date";
			}

		// Data

		$get_functionality_master_data[':end_date'] = $end_date;
		$filter_count++;
		}

	$get_functionality_master_query = $get_functionality_master_query_base . $get_functionality_master_query_where;
	try
		{
		$dbConnection = get_conn_handle();
		$get_functionality_master_statement = $dbConnection->prepare($get_functionality_master_query);
		$get_functionality_master_statement->execute($get_functionality_master_data);
		$get_functionality_master_details = $get_functionality_master_statement->fetchAll();
		if (FALSE === $get_functionality_master_details)
			{
			$return["status"] = FAILURE;
			$return["data"] = "";
			}
		  else
		if (count($get_functionality_master_details) <= 0)
			{
			$return["status"] = DB_NO_RECORD;
			$return["data"] = "";
			}
		  else
			{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"] = $get_functionality_master_details;
			}
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

/*
PURPOSE : To enable or disble an already set permission for email
INPUT 	: User ID, Functionality, CRUD Type, Status
OUTPUT 	: Success or Failure
BY 		: Punith
*/

function db_enable_disable_email_perms($user_id, $functionality, $crud_type, $active)
	{

	// Query

	$user_perms_uquery = "update email_permissions_master set permission_active=:action where permission_user=:user_id and permission_functionality=:func and permission_crud_type=:crud";
	try
		{
		$dbConnection = get_conn_handle();
		$user_perms_ustatement = $dbConnection->prepare($user_perms_uquery);

		// Data

		$user_perms_udata = array(
			':action' => $active,
			':user_id' => $user_id,
			':func' => $functionality,
			':crud' => $crud_type
		);
		$user_perms_ustatement->execute($user_perms_udata);
		$return["status"] = SUCCESS;
		$return["data"] = $user_id;
		}

	catch(PDOException $e)
		{

		// Log the error

		$return["status"] = FAILURE;
		$return["data"] = "";
		}

	return $return;
	}

?>
