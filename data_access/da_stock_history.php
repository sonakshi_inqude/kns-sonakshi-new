<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/

/*

PURPOSE : To add new Stock Histroy

INPUT 	: Material Id,Transaction Type,Location,Quantity,Remarks,Added By

OUTPUT 	: History ID, success or failure message

BY 		: Sonakshi D

*/
function db_add_stock_history($material_id,$transaction_type,$project,$qty,$reference,$remarks,$added_by)
{

	// Query
   $stock_histroy_iquery = "insert into stock_history (stock_history_material_id,stock_history_transaction_type,stock_history_project,stock_history_quantity,stock_history_reference_id,stock_history_remarks,stock_history_added_by,stock_history_added_on) values(:material_id,:transaction_type,:project,:qty,:reference,:remarks,:added_by,:added_on)";

    try

    {

        $dbConnection = get_conn_handle();

        $stock_history_istatement = $dbConnection->prepare($stock_histroy_iquery);



        // Data
        $stock_history_idata = array(':material_id'=>$material_id,':transaction_type'=>$transaction_type,':project'=>$project,':qty'=>$qty,':reference'=>$reference,':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));


		$dbConnection->beginTransaction();

        $stock_history_istatement->execute($stock_history_idata);

		$stock_history_id = $dbConnection->lastInsertId();

		$dbConnection->commit();



        $return["status"] = SUCCESS;

		$return["data"]   = $stock_history_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



    return $return;

}



/*

PURPOSE : To get Stock Hisroy list

INPUT 	: Material Id,Transaction Type,Added By,,Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock History

BY 		: Sonakshi

*/

function db_get_stock_history_list($stock_history_search_data)

{

	if(array_key_exists("history_id",$stock_history_search_data))

	{

		$history_id = $stock_history_search_data["history_id"];

	}

	else

	{

		$history_id= "";

	}



	if(array_key_exists("material_id",$stock_history_search_data))

	{

		$material_id = $stock_history_search_data["material_id"];

	}

	else

	{

		$material_id= "";

	}



	if(array_key_exists("transaction_type",$stock_history_search_data))

	{

		$transaction_type = $stock_history_search_data["transaction_type"];

	}

	else

	{

		$transaction_type= "";

	}


	if(array_key_exists("project",$stock_history_search_data))
	{
		$project = $stock_history_search_data["project"];
	}

	else

	{
		$project = "";
	}



	if(array_key_exists("added_by",$stock_history_search_data))

	{

		$added_by= $stock_history_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("active",$stock_history_search_data))

	{

		$active= $stock_history_search_data["active"];

	}

	else

	{

		$active= "";

	}

	if(array_key_exists("start_date",$stock_history_search_data))

	{

		$start_date= $stock_history_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_history_search_data))

	{

		$end_date= $stock_history_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}

  if(array_key_exists("group_by",$stock_history_search_data))

	{

		$group_by= $stock_history_search_data["group_by"];

	}

	else

	{

		$group_by= "";

	}
  
	$get_stock_history_list_squery_base = "select * from stock_history SH inner join stock_material_master SMM on SMM.stock_material_id= SH.stock_history_material_id inner join stock_project SP on SP.stock_project_id=SH.stock_history_project inner join users U on U.user_id=SH.stock_history_added_by inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id=SMM.stock_material_unit_of_measure";

	$get_stock_history_squery_where = "";
	$get_stock_history_squery_order_by = "";
	$get_stock_history_squery_group_by = "";

	$filter_count = 0;



	// Data

	$get_stock_history_list_sdata = array();



	if($history_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_id=:history_id";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_id=:history_id";

		}



		// Data

		$get_stock_history_list_sdata[':history_id'] = $history_id;



		$filter_count++;

	}



	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_material_id=:material_id";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_material_id=:material_id";

		}



		// Data

		$get_stock_history_list_sdata[':material_id']  = $material_id;



		$filter_count++;

	}



	if($transaction_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_transaction_type=:transaction_type";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_transaction_type=:transaction_type";

		}



		// Data

		$get_stock_history_list_sdata[':transaction_type']  = $transaction_type;



		$filter_count++;

	}


	if($project != "")
	{

		if($filter_count == 0)

		{

			// Query
			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_project=:project";
		}

		else

		{

			// Query
			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_project=:project";
		}



		// Data
		$get_stock_history_list_sdata[':project']  = $project;


		$filter_count++;

	}



	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_by = :added_by";

		}



		//Data

		$get_stock_history_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_on >= :start_date";

		}



		//Data

		$get_stock_history_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_on <= :end_date";

		}



		//Data

		$get_stock_history_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}

	$get_stock_history_list_order_by = " order by stock_history_added_on ASC";
  if($group_by == 1)
  {
    $get_stock_history_squery_group_by = "  group by stock_history_material_id";

  }
  else {
    $get_stock_history_squery_group_by = "";
  }
	$get_stock_history_list_squery = $get_stock_history_list_squery_base.$get_stock_history_squery_where.$get_stock_history_squery_group_by.$get_stock_history_list_order_by;

	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_history_list_sstatement = $dbConnection->prepare($get_stock_history_list_squery);



		$get_stock_history_list_sstatement -> execute($get_stock_history_list_sdata);



		$get_stock_history_list_sdetails = $get_stock_history_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_history_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_history_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_history_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

 }



/*

PURPOSE : To update Material Master

INPUT 	: Material ID, Material Master update Array

OUTPUT 	: Material ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_history_list($material_id,$stock_history_search_data)

{

	if(array_key_exists("transaction_type",$stock_history_search_data))

	{

		$transaction_type = $stock_history_search_data["transaction_type"];

	}

	else

	{

		$transaction_type = "";

	}



	if(array_key_exists("remarks",$stock_history_search_data))

	{

		$remarks = $stock_history_search_data["remarks"];

	}

	else

	{

		$remarks = "";

	}



	if(array_key_exists("added_by",$stock_history_search_data))

	{

		$added_by = $stock_history_search_data["added_by"];

	}

	else

	{

		$added_by = "";

	}



	if(array_key_exists("added_on",$stock_history_search_data))

	{

		$added_on = $stock_history_search_data["added_on"];

	}

	else

	{

		$added_on = "";

	}



	// Query

    $stock_history_update_uquery_base = "update stock_history set";



	$stock_history_update_uquery_set = "";



	$stock_history_update_uquery_where = " where stock_history_material_id = :material_id";



	$stock_history_update_udata = array(":material_id"=>$material_id);



	$filter_count = 0;



	if($transaction_type != "")

	{

		$stock_history_update_uquery_set = $stock_history_update_uquery_set." stock_history_transaction_type = :transaction_type,";

		$stock_history_update_udata[":transaction_type"] = $transaction_type;

		$filter_count++;

	}



	if($remarks != "")

	{

		$stock_history_update_uquery_set = $stock_history_update_uquery_set." stock_history_remarks = :remarks,";

		$stock_history_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}



	if($added_by != "")

	{

		$stock_history_update_uquery_set = $stock_history_update_uquery_set." stock_history_added_by = :added_by,";

		$stock_history_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}

	if($added_on != "")

	{

		$stock_history_update_uquery_set = $stock_history_update_uquery_set." stock_history_added_on = :added_on,";

		$stock_history_update_udata[":added_on"] = $added_on;

		$filter_count++;

	}



	if($filter_count > 0)

	{

		$stock_history_update_uquery_set = trim($stock_history_update_uquery_set,',');

	}



	$stock_history_update_uquery = $stock_history_update_uquery_base.$stock_history_update_uquery_set.$stock_history_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();



        $stock_history_update_ustatement = $dbConnection->prepare($stock_history_update_uquery);



        $stock_history_update_ustatement -> execute($stock_history_update_udata);



        $return["status"] = SUCCESS;

		$return["data"]   = $material_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }



	return $return;

}



/*

PURPOSE : To get Sum Stock History list

INPUT 	: Material Id,Transaction Type,Added By,,Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock History

BY 		: Sonakshi

*/

function db_get_sum_stock_history_list($stock_history_search_data)

{

	if(array_key_exists("history_id",$stock_history_search_data))

	{

		$history_id = $stock_history_search_data["history_id"];

	}

	else

	{

		$history_id= "";

	}



	if(array_key_exists("material_id",$stock_history_search_data))

	{

		$material_id = $stock_history_search_data["material_id"];

	}

	else

	{

		$material_id= "";

	}



	if(array_key_exists("transaction_type",$stock_history_search_data))

	{

		$transaction_type = $stock_history_search_data["transaction_type"];

	}

	else

	{

		$transaction_type= "";

	}


	if(array_key_exists("project",$stock_history_search_data))

	{

		$project = $stock_history_search_data["project"];

	}

	else

	{
		$project = "";
	}



	if(array_key_exists("added_by",$stock_history_search_data))

	{

		$added_by= $stock_history_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}



	if(array_key_exists("active",$stock_history_search_data))

	{

		$active= $stock_history_search_data["active"];

	}

	else

	{

		$active= "";

	}

	if(array_key_exists("start_date",$stock_history_search_data))

	{

		$start_date= $stock_history_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_history_search_data))

	{

		$end_date= $stock_history_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	$get_stock_history_list_squery_base = "select * ,sum(stock_history_quantity) as total_stock_history_quantity  from stock_history SH inner join stock_material_master SMM on SMM.stock_material_id= SH.stock_history_material_id";



	$get_stock_history_squery_where = "";





	$filter_count = 0;



	// Data

	$get_stock_history_list_sdata = array();



	if($history_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_id=:history_id";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_id=:history_id";

		}



		// Data

		$get_stock_history_list_sdata[':history_id'] = $history_id;



		$filter_count++;

	}



	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_material_id=:material_id";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_material_id=:material_id";

		}



		// Data

		$get_stock_history_list_sdata[':material_id']  = $material_id;



		$filter_count++;

	}



	if($transaction_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_transaction_type=:transaction_type";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_transaction_type=:transaction_type";

		}



		// Data

		$get_stock_history_list_sdata[':transaction_type']  = $transaction_type;



		$filter_count++;

	}


	if($project != "")
	{

		if($filter_count == 0)

		{

			// Query
			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_project=:project";
		}

		else

		{

			// Query
			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_project=:project";
		}



		// Data
		$get_stock_history_list_sdata[':project']  = $project;


		$filter_count++;

	}



	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_by = :added_by";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_by = :added_by";

		}



		//Data

		$get_stock_history_list_sdata[':added_by']  = $added_by;



		$filter_count++;

	}



	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_on >= :start_date";

		}



		//Data

		$get_stock_history_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_on <= :end_date";

		}



		//Data

		$get_stock_history_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}



	$get_stock_indent_items_list_squery_group_by = " group by stock_history_transaction_type";

	$get_stock_history_list_squery = $get_stock_history_list_squery_base.$get_stock_history_squery_where.$get_stock_indent_items_list_squery_group_by;


	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_history_list_sstatement = $dbConnection->prepare($get_stock_history_list_squery);



		$get_stock_history_list_sstatement -> execute($get_stock_history_list_sdata);



		$get_stock_history_list_sdetails = $get_stock_history_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_history_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_history_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_history_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

}

/*

PURPOSE : To get Stock Hisroy list

INPUT 	: Material Id,Transaction Type,Added By,,Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock History

BY 		: Sonakshi

*/

function db_get_closing_stock($stock_history_search_data)

{




	if(array_key_exists("material_id",$stock_history_search_data))

	{

		$material_id = $stock_history_search_data["material_id"];

	}

	else

	{

		$material_id= "";

	}



	if(array_key_exists("transaction_type",$stock_history_search_data))

	{

		$transaction_type = $stock_history_search_data["transaction_type"];

	}

	else

	{

		$transaction_type= "";

	}


	if(array_key_exists("project",$stock_history_search_data))
	{
		$project = $stock_history_search_data["project"];
	}

	else

	{
		$project = "";
	}


	if(array_key_exists("start_date",$stock_history_search_data))

	{

		$start_date= $stock_history_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_history_search_data))

	{

		$end_date= $stock_history_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	$get_stock_history_list_squery_base = "select *,sum(stock_history_quantity) as total_qty from stock_history SH inner join stock_material_master SMM on SMM.stock_material_id= SH.stock_history_material_id inner join stock_project SP on SP.stock_project_id=SH.stock_history_project inner join users U on U.user_id=SH.stock_history_added_by inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id=SMM.stock_material_unit_of_measure";



	$get_stock_history_squery_where = "";
	$get_stock_history_squery_order_by = "";





	$filter_count = 0;



	// Data

	$get_stock_history_list_sdata = array();



	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_material_id=:material_id";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_material_id=:material_id";

		}



		// Data

		$get_stock_history_list_sdata[':material_id']  = $material_id;



		$filter_count++;

	}



	if($transaction_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_transaction_type=:transaction_type";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_transaction_type=:transaction_type";

		}



		// Data

		$get_stock_history_list_sdata[':transaction_type']  = $transaction_type;



		$filter_count++;

	}


	if($project != "")
	{

		if($filter_count == 0)

		{

			// Query
			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_project=:project";
		}

		else

		{

			// Query
			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_project=:project";
		}



		// Data
		$get_stock_history_list_sdata[':project']  = $project;


		$filter_count++;

	}


	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_on >= :start_date";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_on >= :start_date";

		}



		//Data

		$get_stock_history_list_sdata[':start_date']  = $start_date;



		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." where stock_history_added_on <= :end_date";

		}

		else

		{

			// Query

			$get_stock_history_squery_where = $get_stock_history_squery_where." and stock_history_added_on <= :end_date";

		}



		//Data

		$get_stock_history_list_sdata['end_date']  = $end_date;



		$filter_count++;

	}

	$get_stock_history_list_squery = $get_stock_history_list_squery_base.$get_stock_history_squery_where;



	try

	{

		$dbConnection = get_conn_handle();



		$get_stock_history_list_sstatement = $dbConnection->prepare($get_stock_history_list_squery);



		$get_stock_history_list_sstatement -> execute($get_stock_history_list_sdata);



		$get_stock_history_list_sdetails = $get_stock_history_list_sstatement -> fetchAll();



		if(FALSE === $get_stock_history_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_history_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_history_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}



	return $return;

 }
?>
