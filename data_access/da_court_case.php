<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

/*
PURPOSE : To add new case
INPUT 	: Case File ID, Case Date, Case Type, Case No, Survey No, Village, Establishment, Status, year, Plaintiff, Defendant, Case Details, Case Date.
OUTPUT 	: Case ID, success or failure message
BY 		: Sonakshi D
*/
function db_add_legal_court_case($legal_court_case_file_id,$legal_court_case_date,$legal_court_case_type,$case_number,$survey_no,$village,$case_establishment,$case_status,$case_year,$case_plaintiff,$case_diffident,$legal_court_case_details,$legal_court_case_added_by)
{
	$case_iquery = "insert into legal_court_case (legal_court_case_file_id,legal_court_case_date,legal_court_case_type,legal_court_case_number,legal_court_case_survey_no,legal_court_case_village,legal_court_case_establishment,legal_court_case_status,legal_court_case_year,legal_court_case_plaintiff,legal_court_case_diffident,legal_court_case_details,legal_court_case_added_by,legal_court_case_added_on) values (:legal_court_case_file_id,:legal_court_case_date,:legal_court_case_type,:case_number,:survey_no,:village,:case_establishment,:case_status,:case_year,:case_plaintiff,:case_diffident,:legal_court_case_details,:legal_court_case_added_by,:legal_court_case_added_on)";
	
	try
    {
        $dbConnection = get_conn_handle();
        
        $case_istatement = $dbConnection->prepare($case_iquery);
        
        // Data
        $case_idata = array(':legal_court_case_file_id'=>$legal_court_case_file_id,':legal_court_case_date'=>$legal_court_case_date,':legal_court_case_type'=>$legal_court_case_type,':case_number'=>$case_number,':survey_no'=>$survey_no,':village'=>$village,':case_establishment'=>$case_establishment,':case_status'=>$case_status,':case_year'=>$case_year,':case_plaintiff'=>$case_plaintiff,':case_diffident'=>$case_diffident,':legal_court_case_details'=>$legal_court_case_details,':legal_court_case_added_by'=>$legal_court_case_added_by,':legal_court_case_added_on'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $case_istatement->execute($case_idata);
		$legal_court_case_file_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $legal_court_case_file_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Case Details
INPUT 	: Case Details Search Data Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Sonakshi D
*/
function db_get_legal_court_case($case_search_data)
{
	// Extract all input parameters
	if(array_key_exists("legal_court_case_id",$case_search_data))
	{
		$legal_court_case_id = $case_search_data["legal_court_case_id"];
	}
	else
	{
		$legal_court_case_id = "";
	}
	
	if(array_key_exists("legal_court_case_file_id",$case_search_data))
	{
		$legal_court_case_file_id = $case_search_data["legal_court_case_file_id"];
	}
	else
	{
		$legal_court_case_file_id = "";
	}
	
	if(array_key_exists("legal_court_case_type",$case_search_data))
	{
		$legal_court_case_type = $case_search_data["legal_court_case_type"];
	}
	else
	{
		$legal_court_case_type = "";
	}
	
	if(array_key_exists("case_number",$case_search_data))
	{
		$case_number = $case_search_data["case_number"];
	}
	else
	{
		$case_number = "";
	}
	
	if(array_key_exists("case_establishment",$case_search_data))
	{
		$case_establishment = $case_search_data["case_establishment"];
	}
	else
	{
		$case_establishment = "";
	}
	
	if(array_key_exists("case_status",$case_search_data))
	{
		$case_status = $case_search_data["case_status"];
	}
	else
	{
		$case_status = "";
	}
	
	if(array_key_exists("case_year",$case_search_data))
	{
		$case_year = $case_search_data["case_year"];
	}
	else
	{
		$case_year = "";
	}
	
	if(array_key_exists("case_plaintiff",$case_search_data))
	{
		$case_plaintiff = $case_search_data["case_plaintiff"];
	}
	else
	{
		$case_plaintiff = "";
	}
	
	if(array_key_exists("case_diffident",$case_search_data))
	{
		$case_diffident = $case_search_data["case_diffident"];
	}
	else
	{
		$case_diffident = "";
	}
	
	if(array_key_exists("legal_court_case_date",$case_search_data))
	{
		$legal_court_case_date = $case_search_data["legal_court_case_date"];
	}
	else
	{
		$legal_court_case_date = "";
	}
	
	if(array_key_exists("legal_court_case_added_by",$case_search_data))
	{
		$legal_court_case_added_by = $case_search_data["legal_court_case_added_by"];
	}
	else
	{
		$legal_court_case_added_by = "";
	}
	if(array_key_exists("legal_court_case_added_on",$case_search_data))
	{
		$legal_court_case_added_on = $case_search_data["legal_court_case_added_on"];
	}
	else
	{
		$legal_court_case_added_on = "";
	}
	
	$get_case_list_squery_base = "select * from legal_court_case LCC inner join bd_court_case_type_master BCCTM on BCCTM.bd_court_case_type_master_id = LCC.legal_court_case_type inner join users U on U.user_id = LCC.legal_court_case_added_by left outer join bd_project_files BPF on BPF.bd_project_file_id = LCC.legal_court_case_file_id join bd_court_status_master BCSM on LCC.legal_court_case_status= BCSM.bd_court_status_master_id inner join bd_court_establishment_master BCEM on LCC.legal_court_case_establishment = BCEM.bd_court_establishment_id left outer join village_master VM on VM.village_id = LCC.legal_court_case_village";
	 
	$get_case_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_case_list_sdata = array();
	
	if($legal_court_case_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_id = :legal_court_case_id";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_id = :legal_court_case_id";				
		}
		
		// Data
		$get_case_list_sdata[':legal_court_case_id']  = $legal_court_case_id;
		
		$filter_count++;
	}
	
	if($legal_court_case_file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_file_id=:legal_court_case_file_id";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_file_id=:legal_court_case_file_id";				
		}
		
		// Data
		$get_case_list_sdata[':legal_court_case_file_id']  = $legal_court_case_file_id;
		
		$filter_count++;
	}
	
	if($legal_court_case_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_date=:legal_court_case_date";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_date=:legal_court_case_date";				
		}
		
		// Data
		$get_case_list_sdata[':legal_court_case_date']  = $legal_court_case_date;
		
		$filter_count++;
	}
	
	if($legal_court_case_added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_added_by=:legal_court_case_added_by";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_added_by=:legal_court_case_added_by";				
		}
		
		// Data
		$get_case_list_sdata[':legal_court_case_added_by']  = $legal_court_case_added_by;
		
		$filter_count++;
	}
	
	if($legal_court_case_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_type=:legal_court_case_type";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_type=:legal_court_case_type";				
		}
		
		// Data
		$get_case_list_sdata[':legal_court_case_type']  = $legal_court_case_type;
		
		$filter_count++;
	}
	
	if($case_establishment != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_establishment=:case_establishment";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_establishment=:case_establishment";				
		}
		
		// Data
		$get_case_list_sdata[':case_establishment']  = $case_establishment;
		
		$filter_count++;
	}
	if($case_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_status=:case_status";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_status=:case_status";				
		}
		
		// Data
		$get_case_list_sdata[':case_status']  = $case_status;
		
		$filter_count++;
	}
	
	if($legal_court_case_added_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." where legal_court_case_added_on=:legal_court_case_added_on";								
		}
		else
		{
			// Query
			$get_case_list_squery_where = $get_case_list_squery_where." and legal_court_case_added_on=:legal_court_case_added_on";				
		}
		
		// Data
		$get_case_list_sdata[':legal_court_case_added_on']  = $legal_court_case_added_on;
		
		$filter_count++;
	}
	
	
	$get_case_list_squery = $get_case_list_squery_base.$get_case_list_squery_where;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_case_list_sstatement = $dbConnection->prepare($get_case_list_squery);
		
		$get_case_list_sstatement -> execute($get_case_list_sdata);
		
		$get_case_list_sdetails = $get_case_list_sstatement -> fetchAll();
		
		if(FALSE === $get_case_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_case_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_case_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add follow up to a court case
INPUT 	: Court Case ID, Follow Up Date, Status, Follow Up Remarks, Added By
OUTPUT 	: Follow Up ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_legal_court_case_fup($legal_court_case_id,$legal_court_case_fup_date,$legal_court_case_fup_status,$legal_court_case_fup_remarks,$legal_court_case_fup_added_by)
{
	$case_fup_iquery = "insert into legal_court_case_follow_up (legal_court_case_id,legal_court_case_follow_up_date,legal_court_case_follow_up_status,legal_court_case_follow_up_remarks,legal_court_case_follow_up_added_by,legal_court_case_follow_up_added_on,legal_court_case_follow_up_updated_by,legal_court_case_follow_up_updated_on) values (:case_id,:fup_date,:status,:remarks,:added_by,:added_on,:updated_by,:updated_on)";
	
	try
    {
        $dbConnection = get_conn_handle();
        
        $case_fup_istatement = $dbConnection->prepare($case_fup_iquery);
        
        // Data
        $case_fup_idata = array(':case_id'=>$legal_court_case_id,':fup_date'=>$legal_court_case_fup_date,':status'=>$legal_court_case_fup_status,':remarks'=>$legal_court_case_fup_remarks,':added_by'=>$legal_court_case_fup_added_by,':added_on'=>date("Y-m-d H:i:s"),':updated_by'=>$legal_court_case_fup_added_by,':updated_on'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $case_fup_istatement->execute($case_fup_idata);
		$court_case_fup_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $court_case_fup_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Case Follow Up Details
INPUT 	: Case Follow Up Details Search Data Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Nitin Kashyap
*/
function db_get_court_case_fup($case_fup_data)
{
	// Extract all input parameters
	if(array_key_exists("fup_id",$case_fup_data))
	{
		$fup_id = $case_fup_data["fup_id"];
	}
	else
	{
		$fup_id = "";
	}
	
	if(array_key_exists("case_id",$case_fup_data))
	{
		$case_id = $case_fup_data["case_id"];
	}
	else
	{
		$case_id = "";
	}
	
	if(array_key_exists("fup_date_start",$case_fup_data))
	{
		$fup_date_start = $case_fup_data["fup_date_start"];
	}
	else
	{
		$fup_date_start = "";
	}
	
	if(array_key_exists("fup_date_end",$case_fup_data))
	{
		$fup_date_end = $case_fup_data["fup_date_end"];
	}
	else
	{
		$fup_date_end = "";
	}
	
	if(array_key_exists("added_by",$case_fup_data))
	{
		$added_by = $case_fup_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	$get_case_fup_list_squery_base = "select * from legal_court_case_follow_up LCCFU inner join users U on U.user_id = LCCFU.legal_court_case_follow_up_updated_by left outer join bd_court_status_master BCSM on BCSM.bd_court_status_master_id = LCCFU.legal_court_case_follow_up_status";
	
	$get_case_fup_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_case_fup_list_sdata = array();
	
	if($fup_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where legal_court_case_follow_up_id = :fup_id";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and legal_court_case_follow_up_id = :fup_id";				
		}
		
		// Data
		$get_case_fup_list_sdata[':fup_id']  = $fup_id;
		
		$filter_count++;
	}
	
	if($case_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where legal_court_case_id = :case_id";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and legal_court_case_id = :case_id";				
		}
		
		// Data
		$get_case_fup_list_sdata[':case_id']  = $case_id;
		
		$filter_count++;
	}

	if($fup_date_start != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where legal_court_case_follow_up_date >= :fup_date_start";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and legal_court_case_follow_up_date >= :fup_date_start";				
		}
		
		// Data
		$get_case_fup_list_sdata[':fup_date_start']  = $fup_date_start;
		
		$filter_count++;
	}
	
	if($fup_date_end != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where legal_court_case_follow_up_date <= :fup_date_end";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and legal_court_case_follow_up_date <= :fup_date_end";				
		}
		
		// Data
		$get_case_fup_list_sdata[':fup_date_end']  = $fup_date_end;
		
		$filter_count++;
	}
	
	$get_case_fup_list_squery_order = " order by legal_court_case_follow_up_updated_on desc, legal_court_case_follow_up_date";
	$get_case_fup_list_squery = $get_case_fup_list_squery_base.$get_case_fup_list_squery_where.$get_case_fup_list_squery_order;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_case_fup_list_sstatement = $dbConnection->prepare($get_case_fup_list_squery);
		
		$get_case_fup_list_sstatement -> execute($get_case_fup_list_sdata);
		
		$get_case_fup_list_sdetails = $get_case_fup_list_sstatement -> fetchAll();
		
		if(FALSE === $get_case_fup_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_case_fup_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_case_fup_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get court case follow up list
INPUT 	: Case Follow Up Details Search Data Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Nitin Kashyap
*/
function db_get_court_case_fup_latest($case_fup_data)
{
	// Extract all input parameters
	if(array_key_exists("case_id",$case_fup_data))
	{
		$case_id = $case_fup_data["case_id"];
	}
	else
	{
		$case_id = "";
	}
	
	if(array_key_exists("fup_date_start",$case_fup_data))
	{
		$fup_date_start = $case_fup_data["fup_date_start"];
	}
	else
	{
		$fup_date_start = "";
	}
	
	if(array_key_exists("fup_date_end",$case_fup_data))
	{
		$fup_date_end = $case_fup_data["fup_date_end"];
	}
	else
	{
		$fup_date_end = "";
	}
	
	if(array_key_exists("added_by",$case_fup_data))
	{
		$added_by = $case_fup_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	$get_case_fup_list_squery_base = "select * from legal_court_case_follow_up PLCCFU inner join users U on U.user_id = PLCCFU.legal_court_case_follow_up_updated_by inner join bd_court_status_master BCSM on BCSM.bd_court_status_master_id = PLCCFU.legal_court_case_follow_up_status inner join legal_court_case LCC on LCC.legal_court_case_id = PLCCFU.legal_court_case_id left outer join bd_court_case_type_master BCCTM on BCCTM.bd_court_case_type_master_id = LCC.legal_court_case_type left outer join village_master VM on VM.village_id = LCC.legal_court_case_village left outer join bd_court_establishment_master BCEM on BCEM.bd_court_establishment_id = LCC.legal_court_case_establishment";
	
	$get_case_fup_list_squery_where = " where legal_court_case_follow_up_id not in (select JPLCCFU.legal_court_case_follow_up_id from legal_court_case_follow_up JPLCCFU inner join legal_court_case_follow_up JTLCCFU on (JPLCCFU.legal_court_case_id = JTLCCFU.legal_court_case_id and JPLCCFU.legal_court_case_follow_up_added_on < JTLCCFU.legal_court_case_follow_up_added_on))";
	
	$filter_count = 1;
	
	// Data
	$get_case_fup_list_sdata = array();		
	
	if($case_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where PLCCFU.legal_court_case_id = :case_id";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and PLCCFU.legal_court_case_id = :case_id";				
		}
		
		// Data
		$get_case_fup_list_sdata[':case_id']  = $case_id;
		
		$filter_count++;
	}

	if($fup_date_start != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where PLCCFU.legal_court_case_follow_up_date >= :fup_date_start";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and PLCCFU.legal_court_case_follow_up_date >= :fup_date_start";				
		}
		
		// Data
		$get_case_fup_list_sdata[':fup_date_start']  = $fup_date_start;
		
		$filter_count++;
	}
	
	if($fup_date_end != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where PLCCFU.legal_court_case_follow_up_date <= :fup_date_end";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and PLCCFU.legal_court_case_follow_up_date <= :fup_date_end";				
		}
		
		// Data
		$get_case_fup_list_sdata[':fup_date_end']  = $fup_date_end;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." where PLCCFU.legal_court_case_follow_up_added_by = :added_by";
		}
		else
		{
			// Query
			$get_case_fup_list_squery_where = $get_case_fup_list_squery_where." and PLCCFU.legal_court_case_follow_up_added_by = :added_by";				
		}
		
		// Data
		$get_case_fup_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	$get_case_fup_list_squery_order = " order by legal_court_case_follow_up_date";
	$get_case_fup_list_squery = $get_case_fup_list_squery_base.$get_case_fup_list_squery_where.$get_case_fup_list_squery_order;			
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_case_fup_list_sstatement = $dbConnection->prepare($get_case_fup_list_squery);
		
		$get_case_fup_list_sstatement -> execute($get_case_fup_list_sdata);
		
		$get_case_fup_list_sdetails = $get_case_fup_list_sstatement -> fetchAll();
		
		if(FALSE === $get_case_fup_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_case_fup_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_case_fup_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update court case follow up details
INPUT 	: Follow Up ID, Follow Up Data
OUTPUT 	: Follow Up ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_court_case_fup($case_fup_id,$case_fup_data)
{
	// Query
    $case_fup_uquery_base = "update legal_court_case_follow_up set";  
	
	$case_fup_uquery_set = "";
	
	$case_fup_uquery_where = " where legal_court_case_follow_up_id = :case_fup_id";
	
	$case_fup_udata = array(":case_fup_id"=>$case_fup_id);
	
	$filter_count = 0;
	
	if(array_key_exists('date',$case_fup_data))
	{
		$case_fup_uquery_set = $case_fup_uquery_set." legal_court_case_follow_up_date = :date,";
		$case_fup_udata[":date"] = $case_fup_data['date'];
		$filter_count++;
	}
	
	if(array_key_exists('remarks',$case_fup_data))
	{
		$case_fup_uquery_set = $case_fup_uquery_set." legal_court_case_follow_up_remarks = :remarks,";
		$case_fup_udata[":remarks"] = $case_fup_data['remarks'];
		$filter_count++;
	}
	
	if(array_key_exists('updated_by',$case_fup_data))
	{
		$case_fup_uquery_set = $case_fup_uquery_set." legal_court_case_follow_up_updated_by = :updated_by,";
		$case_fup_udata[":updated_by"] = $case_fup_data['updated_by'];
		$filter_count++;
	}
	
	// Updated time
	$case_fup_uquery_set = $case_fup_uquery_set." legal_court_case_follow_up_updated_on = :now,";
	$case_fup_udata[":now"] = date('Y-m-d H:i:s');
	$filter_count++;
	
	
	if($filter_count > 0)
	{
		$case_fup_uquery_set = trim($case_fup_uquery_set,',');
	}
	
	$case_fup_uquery = $case_fup_uquery_base.$case_fup_uquery_set.$case_fup_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $case_fup_ustatement = $dbConnection->prepare($case_fup_uquery);		
        
        $case_fup_ustatement -> execute($case_fup_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $case_fup_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new documents
INPUT 	: Case ID, File Path,Added By
OUTPUT 	: Case ID, success or failure message
BY 		: Sonakshi D
*/
function db_add_legal_court_case_documents($file_path,$case_id,$remarks,$added_by)
{
	$documents_iquery = "insert into legal_court_case_documents (legal_court_case_documents_file_path,legal_court_case_documents_case_id,legal_court_case_documents_remarks,legal_court_case_documents_added_by,legal_court_case_documents_added_on) values (:file_path,:case_id,:remarks,:added_by,:now)";
	
	try
    {
        $dbConnection = get_conn_handle();
        
        $documents_istatement = $dbConnection->prepare($documents_iquery);
        
        // Data
        $documents_idata = array(':file_path'=>$file_path,':case_id'=>$case_id,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $documents_istatement->execute($documents_idata);
		$legal_court_case_documents_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $legal_court_case_documents_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Documents Details
INPUT 	: Document Details Search Data Array
OUTPUT 	: SUCCESS or Failure message,if it is success it will give existing data.
BY 		: Sonakshi D
*/
function db_get_legal_court_case_documents($documents_search_data)
{
	// Extract all input parameters
	if(array_key_exists("file_path",$documents_search_data))
	{
		$file_path = $documents_search_data["file_path"];
	}
	else
	{
		$file_path = "";
	}
	
	if(array_key_exists("case_id",$documents_search_data))
	{
		$case_id = $documents_search_data["case_id"];
	}
	else
	{
		$case_id = "";
	}
	
	if(array_key_exists("added_by",$documents_search_data))
	{
		$added_by = $documents_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$documents_search_data))
	{
		$added_on = $documents_search_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	
	$get_document_list_squery_base = "select * from legal_court_case_documents LCCD inner join users U on U.user_id = LCCD.legal_court_case_documents_added_by";
	 
	$get_document_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_case_list_sdata = array();
	
	if($file_path != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." where legal_court_case_documents_file_path = :file_path";								
		}
		else
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." and legal_court_case_documents_file_path = :file_path";				
		}
		
		// Data
		$get_case_list_sdata[':file_path']  = $file_path;
		
		$filter_count++;
	}
	
	if($case_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." where legal_court_case_documents_case_id=:case_id";
		}
		else
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." and legal_court_case_documents_case_id=:case_id";				
		}
		
		// Data
		$get_case_list_sdata[':case_id']  = $case_id;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." where legal_court_case_documents_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." and legal_court_case_documents_added_by=:added_by";				
		}
		
		// Data
		$get_case_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($added_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." where legal_court_case_documents_added_on=:added_on";								
		}
		else
		{
			// Query
			$get_document_list_squery_where = $get_document_list_squery_where." and legal_court_case_documents_added_on=:added_on";				
		}
		
		// Data
		$get_case_list_sdata[':added_on']  = $added_on;
		
		$filter_count++;
	}
	
	$get_document_list_squery_order = ' order by legal_court_case_documents_added_on desc';
	$get_document_list_squery = $get_document_list_squery_base.$get_document_list_squery_where.$get_document_list_squery_order;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_document_list_sstatement = $dbConnection->prepare($get_document_list_squery);
		
		$get_document_list_sstatement -> execute($get_case_list_sdata);
		
		$get_documents_list_sdetails = $get_document_list_sstatement -> fetchAll();
		
		if(FALSE === $get_documents_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_documents_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_documents_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new payment request for court case related expenses
INPUT 	: Case ID, Amount, Reason, Remarks, Payable to,added_by
OUTPUT 	: Payment Request ID, success or failure message
BY 		: Punith
*/
function db_add_court_payment_request($case_id,$amount,$reason,$remarks,$payable_to,$added_by)
{
	// Query
    $court_payment_request_iquery = "insert into bd_court_payment_requests (bd_payment_court_case_id,bd_court_payment_requests_amount,bd_court_payment_requests_reason,bd_court_payment_requests_remarks,bd_court_payment_requests_payable_to,bd_court_payment_requests_status,bd_court_payment_requests_added_by,bd_court_payment_requests_added_on,bd_court_payment_requests_updated_by,bd_court_payment_requests_updated_on) values (:case_id,:amount,:reason,:remarks,:payable_to,:status,:added_by,:now,:updated_by,:now)";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $court_payment_request_istatement = $dbConnection->prepare($court_payment_request_iquery);
        
        // Data
        $court_payment_request_idata = array(':case_id'=>$case_id,':amount'=>$amount,':reason'=>$reason,':remarks'=>$remarks,':payable_to'=>$payable_to,':status'=>'1',':added_by'=>$added_by,':updated_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $court_payment_request_istatement->execute($court_payment_request_idata);
		$court_payment_requests_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $court_payment_requests_id;		
    }
    catch(PDOException $e)
    {
	// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}
/*
PURPOSE : To get court payment request list
INPUT 	: Case ID, Amount, Reason, Remarks, Payable to, Status, Added_by, Start Date, End Date
OUTPUT 	: List of court related payment requests
BY 		: Punith
*/
function db_get_court_payment_request($case_id,$amount,$reason,$remarks,$payable_to,$status,$added_by,$start_date,$end_date,$request_id='')
{
	$get_court_payment_request_squery_base = "select * from bd_court_payment_requests BCPR inner join legal_court_case LCC on LCC.legal_court_case_id = BCPR.bd_payment_court_case_id inner join users U on U.user_id = BCPR.bd_court_payment_requests_added_by left outer join bd_court_establishment_master BCEM on BCEM.bd_court_establishment_id = LCC.legal_court_case_establishment left outer join bd_court_status_master BCSM on BCSM.bd_court_status_master_id = LCC.legal_court_case_status";
	
	$get_court_payment_request_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_court_payment_request_sdata = array();
	
	if($case_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_payment_court_case_id=:case_id";								
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_payment_court_case_id=:case_id";				
		}
		
		// Data
		$get_court_payment_request_sdata[':case_id']  = $case_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_id = :request_id";								
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_id = :request_id";				
		}
		
		// Data
		$get_court_payment_request_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_amount=:amount";								
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_amount=:amount";				
		}
		
		// Data
		$get_court_payment_request_sdata[':amount']  = $amount;
		
		$filter_count++;
	}
	
	
	if($reason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_reason=:reason";								
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_reason=:reason";				
		}
		
		// Data
		$get_court_payment_request_sdata[':reason']  = $reason;
		
		$filter_count++;
	}
	
	if($remarks != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_remarks=:remarks";								
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_remarks=:remarks";				
		}
		
		// Data
		$get_court_payment_request_sdata[':remarks']  = $remarks;
		
		$filter_count++;
	}
	
	if($payable_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_payable_to=:payable_to";
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_payable_to=:payable_to";
		}
		
		// Data
		$get_court_payment_request_sdata[':payable_to']  = $payable_to;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_status=:status";
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_status=:status";
		}
		
		// Data
		$get_court_payment_request_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_added_by=:added_by";
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_added_by=:added_by";
		}
		
		// Data
		$get_court_payment_request_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_added_on >= :start_date";
		}
		
		//Data
		$get_court_payment_request_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." where bd_court_payment_requests_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_court_payment_request_squery_where = $get_court_payment_request_squery_where." and bd_court_payment_requests_added_on <= :end_date";
		}
		
		//Data
		$get_court_payment_request_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_court_payment_request_squery = $get_court_payment_request_squery_base.$get_court_payment_request_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_court_payment_request_sstatement = $dbConnection->prepare($get_court_payment_request_squery);
		
		$get_court_payment_request_sstatement -> execute($get_court_payment_request_sdata);
		
		$get_court_payment_request_sdetails = $get_court_payment_request_sstatement -> fetchAll();
		
		if(FALSE === $get_court_payment_request_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_court_payment_request_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_court_payment_request_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update court payment request details
INPUT 	: Court Payment Request ID, Case ID, Amount, Reason, Remarks, Payable To, Status, Added By, Added On
OUTPUT 	: Payment Request ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_court_payment_request($request_id,$case_id,$amount,$reason,$remarks,$payable_to,$status,$updated_by,$updated_on)
{
	// Query
    $court_pay_request_uquery_base = "update bd_court_payment_requests set";  
	
	$court_pay_request_uquery_set = "";
	
	$court_pay_request_uquery_where = " where bd_court_payment_requests_id = :request_id";
	
	$court_pay_request_udata = array(":request_id"=>$request_id);
	
	$filter_count = 0;
	
	if($case_id != "")
	{
		$court_pay_request_uquery_set = $court_pay_request_uquery_set." bd_court_payment_requests_case_id=:case_id,";
		$court_pay_request_udata[":case_id"] = $case_id;
		$filter_count++;
	}
	
	if($amount != "")
	{
		$court_pay_request_uquery_set = $court_pay_request_uquery_set." bd_court_payment_requests_amount=:amount,";
		$court_pay_request_udata[":amount"] = $amount;
		$filter_count++;
	}
	
	if($reason != "")
	{
		$court_pay_request_uquery_set = $court_pay_request_uquery_set." bd_court_payment_requests_reason=:reason,";
		$court_pay_request_udata[":reason"] = $reason;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$court_pay_request_uquery_set = $court_pay_request_uquery_set." bd_court_payment_requests_remarks=:remarks,";
		$court_pay_request_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($payable_to != "")
	{
		$bd_file_uquery_set = $bd_file_uquery_set." bd_court_payment_requests_payable_to=:payable_to,";
		$court_pay_request_udata[":payable_to"] = $payable_to;
		$filter_count++;
	}
	
	if($status != "")
	{
		$court_pay_request_uquery_set = $court_pay_request_uquery_set." bd_court_payment_requests_status=:status,";
		$court_pay_request_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		$court_pay_request_uquery_set = $court_pay_request_uquery_set." bd_court_payment_requests_updated_by=:updated_by,";
		$court_pay_request_udata[":updated_by"] = $updated_by;
		$filter_count++;
	}
	
	if($updated_on != "")
	{
		$court_pay_request_uquery_set = $court_pay_request_uquery_set." bd_court_payment_requests_updated_on=:updated_on,";
		$court_pay_request_udata[":updated_on"] = $updated_on;
		$filter_count++;
	}
			
	if($filter_count > 0)
	{
		$court_pay_request_uquery_set = trim($court_pay_request_uquery_set,',');
	}
	
	$court_pay_request_uquery = $court_pay_request_uquery_base.$court_pay_request_uquery_set.$court_pay_request_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $court_pay_request_ustatement = $dbConnection->prepare($court_pay_request_uquery);		
        
        $court_pay_request_ustatement -> execute($court_pay_request_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $case_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new court payment release
INPUT 	: Request ID, Amount, Mode, Remarks, Paid To, Added By
OUTPUT 	: Payment Release ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_court_payment_release($request_id,$amount,$mode,$remarks,$paid_to,$added_by)
{
	// Query
    $court_payment_release_iquery = "insert into legal_court_payments_issued (legal_court_payment_issued_request,legal_court_payment_issued_amount,legal_court_payment_issued_mode,legal_court_payment_issued_remarks,legal_court_payment_done_to,legal_court_payment_issued_by,legal_court_payment_issued_on) values (:request,:amount,:mode,:remarks,:done_to,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $court_payment_release_istatement = $dbConnection->prepare($court_payment_release_iquery);
        
        // Data		
        $court_payment_release_idata = array(':request'=>$request_id,':amount'=>$amount,':mode'=>$mode,':remarks'=>$remarks,':done_to'=>$paid_to,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $court_payment_release_istatement->execute($court_payment_release_idata);
		$court_payment_release_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $court_payment_release_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To get court payment released list
INPUT 	: Payment Released Data Array
OUTPUT 	: Payment Released List
BY 		: Nitin Kashyap
*/
function db_get_court_payment_released_list($pay_release_data)
{
	// Extract all data
	$pay_release_id = "";
	$request_id     = "";
	$case_id        = "";
	$mode           = "";
	$reason         = "";
	$added_by       = "";
	$start_date     = "";	
	$end_date       = "";
	
	if(array_key_exists("release",$pay_release_data))
	{	
		$pay_release_id = $pay_release_data["release"];
	}
	
	if(array_key_exists("request",$pay_release_data))
	{	
		$request_id = $pay_release_data["request"];
	}
	
	if(array_key_exists("case",$pay_release_data))
	{	
		$case_id = $pay_release_data["case"];
	}
	
	if(array_key_exists("mode",$pay_release_data))
	{	
		$mode = $pay_release_data["mode"];
	}

	if(array_key_exists("reason",$pay_release_data))
	{	
		$reason = $pay_release_data["reason"];
	}	
	
	if(array_key_exists("added_by",$pay_release_data))
	{	
		$added_by = $pay_release_data["added_by"];
	}
	
	if(array_key_exists("start_date",$pay_release_data))
	{	
		$start_date = $pay_release_data["start_date"];
	}
	
	if(array_key_exists("end_date",$pay_release_data))
	{	
		$end_date = $pay_release_data["end_date"];
	}

	$get_pay_release_squery_base = "select * from legal_court_payments_issued LCPI inner join bd_court_payment_requests BCPR on BCPR.bd_court_payment_requests_id = LCPI.legal_court_payment_issued_request left outer join payment_mode_master PMM on PMM.payment_mode_id = LCPI.legal_court_payment_issued_mode inner join legal_court_case LCC on LCC.legal_court_case_id = BCPR.bd_payment_court_case_id inner join users U on U.user_id = BCPR.bd_court_payment_requests_added_by left outer join bd_court_establishment_master BCEM on BCEM.bd_court_establishment_id = LCC.legal_court_case_establishment left outer join bd_court_status_master BCSM on BCSM.bd_court_status_master_id = LCC.legal_court_case_status";		
	
	$get_pay_release_squery_where = "";
	
	$get_pay_release_squery_order = "";		
	
	$filter_count = 0;
	
	// Data
	$get_pay_release_sdata = array();
	
	if($pay_release_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_court_payment_issued_id = :release_id";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_court_payment_issued_id = :release_id";				
		}
		
		// Data
		$get_pay_release_sdata[':release_id']  = $pay_release_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_court_payment_issued_request = :request_id";
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_court_payment_issued_request = :request_id";				
		}
		
		// Data
		$get_pay_release_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($case_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where BCPR.bd_payment_court_case_id = :case_id";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and BCPR.bd_payment_court_case_id = :case_id";				
		}
		
		// Data
		$get_pay_release_sdata[':case_id']  = $case_id;
		
		$filter_count++;
	}
	
	if($mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_court_payment_issued_mode = :mode";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_court_payment_issued_mode = :mode";				
		}
		
		// Data
		$get_pay_release_sdata[':mode']  = $mode;
		
		$filter_count++;
	}
	
	if($reason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where BCPR.bd_court_payment_requests_reason = :reason";
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and BCPR.bd_court_payment_requests_reason = :reason";				
		}
		
		// Data
		$get_pay_release_sdata[':reason']  = $reason;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_court_payment_issued_by = :added_by";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_court_payment_issued_by = :added_by";				
		}
		
		// Data
		$get_pay_release_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_court_payment_issued_on >= :start_date";
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_court_payment_issued_on >= :start_date";				
		}
		
		// Data
		$get_pay_release_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}
	
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." where legal_court_payment_issued_on <= :end_date";								
		}
		else
		{
			// Query
			$get_pay_release_squery_where = $get_pay_release_squery_where." and legal_court_payment_issued_on <= :end_date";				
		}
		
		// Data
		$get_pay_release_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_pay_release_squery_order = ' order by legal_court_payment_issued_on desc';
	$get_pay_release_squery = $get_pay_release_squery_base.$get_pay_release_squery_where.$get_pay_release_squery_order;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_pay_release_sstatement = $dbConnection->prepare($get_pay_release_squery);
		
		$get_pay_release_sstatement -> execute($get_pay_release_sdata);
		
		$get_pay_release_sdetails = $get_pay_release_sstatement -> fetchAll();
		
		if(FALSE === $get_pay_release_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_pay_release_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_pay_release_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update court case details
INPUT 	: Case ID, Case Data
OUTPUT 	: Case ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_court_case($case_id,$case_data)
{
	// Query
    $case_uquery_base = "update legal_court_case set";  
	
	$case_uquery_set = "";
	
	$case_uquery_where = " where legal_court_case_id = :case_id";
	
	$case_udata = array(":case_id"=>$case_id);
	
	$filter_count = 0;
	
	if(array_key_exists('date',$case_data))
	{
		$case_uquery_set     = $case_uquery_set." legal_court_case_date = :date,";
		$case_udata[":date"] = $case_data['date'];
		$filter_count++;
	}
	
	if(array_key_exists('type',$case_data))
	{
		$case_uquery_set     = $case_uquery_set." legal_court_case_type = :type,";
		$case_udata[":type"] = $case_data['type'];
		$filter_count++;
	}
	
	if(array_key_exists('case_no',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_number = :case_no,";
		$case_udata[":case_no"] = $case_data['case_no'];
		$filter_count++;
	}	

	if(array_key_exists('survey_no',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_survey_no = :survey_no,";
		$case_udata[":survey_no"] = $case_data['survey_no'];
		$filter_count++;
	}	
	
	if(array_key_exists('village',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_village = :village,";
		$case_udata[":village"] = $case_data['village'];
		$filter_count++;
	}

	if(array_key_exists('establishment',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_establishment = :establishment,";
		$case_udata[":establishment"] = $case_data['establishment'];
		$filter_count++;
	}
	
	if(array_key_exists('status',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_status = :status,";
		$case_udata[":status"] = $case_data['status'];
		$filter_count++;
	}
	
	if(array_key_exists('year',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_year = :year,";
		$case_udata[":year"] = $case_data['year'];
		$filter_count++;
	}	

	if(array_key_exists('plaintiff',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_plaintiff = :plaintiff,";
		$case_udata[":plaintiff"] = $case_data['plaintiff'];
		$filter_count++;
	}	
	
	if(array_key_exists('diffident',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_diffident = :diffident,";
		$case_udata[":diffident"] = $case_data['diffident'];
		$filter_count++;
	}	
	
	if(array_key_exists('details',$case_data))
	{
		$case_uquery_set = $case_uquery_set." legal_court_case_details = :details,";
		$case_udata[":details"] = $case_data['details'];
		$filter_count++;
	}	
	
	if($filter_count > 0)
	{
		$case_uquery_set = trim($case_uquery_set,',');
	}
	
	$case_uquery = $case_uquery_base.$case_uquery_set.$case_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $case_ustatement = $dbConnection->prepare($case_uquery);		
        
        $case_ustatement -> execute($case_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $case_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>