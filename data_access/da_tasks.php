<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. In db_get_task_type_list, name search should be wild card search
*/

/*
PURPOSE : To get task type list
INPUT 	: Task Type Name, Process, Active Status, Added By, Start Date(for added on), End Date(for added on), Task Type ID
OUTPUT 	: List of processes
BY 		: Nitin Kashyap
*/
function db_get_task_type_list($task_type_name,$process,$active,$added_by,$start_date,$end_date,$task_type_id="")
{
	$get_task_type_list_squery_base = "select * from task_type_master TTM inner join process_master PM on PM.process_master_id = TTM.task_type_process left outer join users U on U.user_id = TTM.task_type_added_by";
	
	$get_task_type_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_task_type_list_sdata = array();
	
	if($task_type_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where task_type_name=:task_type_name";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and task_type_name=:task_type_name";				
		}
		
		// Data
		$get_task_type_list_sdata[':task_type_name']  = $task_type_name;
		
		$filter_count++;
	}
	
	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where task_type_process=:process";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and task_type_process=:process";				
		}
		
		// Data
		$get_task_type_list_sdata[':process']  = $process;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where task_type_active=:active";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and task_type_active=:active";				
		}
		
		// Data
		$get_task_type_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where task_type_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and task_type_added_by=:added_by";				
		}
		
		// Data
		$get_task_type_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where task_type_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and task_type_added_on >= :start_date";				
		}
		
		//Data
		$get_task_type_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where task_type_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and task_type_added_on <= :end_date";				
		}
		
		//Data
		$get_task_type_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($task_type_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where task_type_id = :type_id";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and task_type_id = :type_id";				
		}
		
		//Data
		$get_task_type_list_sdata[':type_id']  = $task_type_id;
		
		$filter_count++;
	}
	
	$get_task_type_list_squery = $get_task_type_list_squery_base.$get_task_type_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_task_type_list_sstatement = $dbConnection->prepare($get_task_type_list_squery);
		
		$get_task_type_list_sstatement -> execute($get_task_type_list_sdata);
		
		$get_task_type_list_sdetails = $get_task_type_list_sstatement -> fetchAll();
		
		if(FALSE === $get_task_type_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_task_type_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_task_type_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get reason list
INPUT 	: Reason, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of reasons
BY 		: Nitin Kashyap
*/
function db_get_reason_list($reason_name,$active,$added_by,$start_date,$end_date)
{
	$get_reason_list_squery_base = "select * from reason_master RM left outer join users U on U.user_id = RM.reason_master_added_by";
	
	$get_reason_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_reason_list_sdata = array();
	
	if($reason_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason=:reason_name";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason=:reason_name";				
		}
		
		// Data
		$get_reason_list_sdata[':reason_name']  = $reason_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_master_active=:active";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_master_active=:active";				
		}
		
		// Data
		$get_reason_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_added_by=:added_by";				
		}
		
		// Data
		$get_reason_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_added_on >= :start_date";				
		}
		
		//Data
		$get_reason_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where reason_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and reason_added_on <= :end_date";				
		}
		
		//Data
		$get_reason_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_reason_list_squery_order = " order by reason_priority desc,reason asc";
	$get_reason_list_squery = $get_reason_list_squery_base.$get_reason_list_squery_where.$get_reason_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reason_list_sstatement = $dbConnection->prepare($get_reason_list_squery);
		
		$get_reason_list_sstatement -> execute($get_reason_list_sdata);
		
		$get_reason_list_sdetails = $get_reason_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get delay reason list
INPUT 	: Reason, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of reasons
BY 		: Nitin Kashyap
*/
function db_get_delay_reason_list($task,$reason)
{
	$get_reason_list_squery_base = "select * from reasons R inner join reason_master RM on RM.reason_master_id = R.reason";
	
	$get_reason_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_reason_list_sdata = array();
	
	if($task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where R.reason_task=:task";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and R.reason_task=:task";				
		}
		
		// Data
		$get_reason_list_sdata[':task']  = $task;
		
		$filter_count++;
	}
	
	if($reason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." where R.reason=:reason";								
		}
		else
		{
			// Query
			$get_reason_list_squery_where = $get_reason_list_squery_where." and R.reason=:reason";				
		}
		
		// Data
		$get_reason_list_sdata[':reason']  = $reason;
		
		$filter_count++;
	}
	
	$get_reason_list_squery = $get_reason_list_squery_base.$get_reason_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_reason_list_sstatement = $dbConnection->prepare($get_reason_list_squery);
		
		$get_reason_list_sstatement -> execute($get_reason_list_sdata);
		
		$get_reason_list_sdetails = $get_reason_list_sstatement -> fetchAll();
		
		if(FALSE === $get_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get task list
INPUT 	: Task Type, Process Plan ID, Planned End Date, Actual End Date, Added By, Start Date(for added on), End Date(for added on), Order (Sort Order - 1 for asc, 0 for desc), Task Active, Bulk Process Plan ID
OUTPUT 	: List of tasks
BY 		: Nitin Kashyap
*/
function db_get_task_plan_list($task_id,$task_type,$process_plan,$planned_end_date,$actual_end_date,$applicable,$added_by,$start_date,$end_date,$order,$task_status="1",$bprocess_plan="")
{
	$get_task_plan_legal_list_squery_base = "select * from task_plan_legal TLP";
	
	$get_task_plan_legal_list_squery_join = " inner join task_type_master TTM on TTM.task_type_id = TLP.task_plan_legal_type inner join users U on U.user_id = TLP.task_plan_added_by inner join reason_master RM on RM.reason_master_id = TLP.task_plan_delay_reason inner join process_master PM on PM.process_master_id = TTM.task_type_process";
	
	$get_task_plan_legal_list_squery_where = "";
	
	$get_task_plan_legal_list_squery_order = "";
	
	$filter_count = 0;
	
	// Data
	$get_task_plan_legal_list_sdata = array();
	
	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_legal_id=:id";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_legal_id=:id";				
		}
		
		// Data
		$get_task_plan_legal_list_sdata[':id']  = $task_id;
		
		$filter_count++;
	}
	
	if($task_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_legal_type=:type";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_legal_type=:type";				
		}
		
		// Data
		$get_task_plan_legal_list_sdata[':type']  = $task_type;
		
		$filter_count++;
	}
	
	if($process_plan != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_legal_process_plan_id=:process_plan";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_legal_process_plan_id=:process_plan";				
		}
		
		// Data
		$get_task_plan_legal_list_sdata[':process_plan']  = $process_plan;
		
		$filter_count++;
	}
	
	if($bprocess_plan != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_legal_bulk_process_plan_id=:bprocess_plan";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_legal_bulk_process_plan_id=:bprocess_plan";				
		}
		
		// Data
		$get_task_plan_legal_list_sdata[':bprocess_plan']  = $bprocess_plan;
		
		$filter_count++;
	}
	
	if($planned_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_planned_end_date=:planned_end";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_planned_end_date=:planned_end";				
		}
		
		// Data
		$get_task_plan_legal_list_sdata[':planned_end']  = $planned_end_date;
		
		$filter_count++;
	}
	
	if($actual_end_date != "")
	{
		if($actual_end_date == "9999-99-99")
		{
			$actual_end_date = "0000-00-00";
		
			if($filter_count == 0)
			{
				// Query
				$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_actual_end_date != :actual_end";								
			}
			else
			{
				// Query
				$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_actual_end_date != :actual_end";				
			}
		}
		else
		{
			if($filter_count == 0)
			{
				// Query
				$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_actual_end_date = :actual_end";								
			}
			else
			{
				// Query
				$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_actual_end_date = :actual_end";				
			}
		}
		
		// Data
		$get_task_plan_legal_list_sdata[':actual_end']  = $actual_end_date;
		
		$filter_count++;
	}	
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_added_by=:added_by";				
		}
		
		// Data
		$get_task_plan_legal_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_added_on >= :start_date";				
		}
		
		//Data
		$get_task_plan_legal_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_added_on <= :end_date";				
		}
		
		//Data
		$get_task_plan_legal_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($task_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." where task_plan_active = :task_status";								
		}
		else
		{
			// Query
			$get_task_plan_legal_list_squery_where = $get_task_plan_legal_list_squery_where." and task_plan_active = :task_status";				
		}
		
		//Data
		$get_task_plan_legal_list_sdata[':task_status']  = $task_status;
		
		$filter_count++;
	}
	
	// Sorting Order
	if($order != "")
	{
		if($order == "asc")
		{
			$get_task_plan_legal_list_squery_order = " order by task_plan_planned_end_date asc";
		}
		else if($order == "slno")
		{
			$get_task_plan_legal_list_squery_order = " order by task_plan_legal_id asc";
		}
		else		
		{
			$get_task_plan_legal_list_squery_order = " order by task_plan_planned_end_date desc";
		}
	}
	
	$get_task_plan_legal_list_squery = $get_task_plan_legal_list_squery_base.$get_task_plan_legal_list_squery_join.$get_task_plan_legal_list_squery_where.$get_task_plan_legal_list_squery_order;		

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_task_plan_legal_list_sstatement = $dbConnection->prepare($get_task_plan_legal_list_squery);
		
		$get_task_plan_legal_list_sstatement -> execute($get_task_plan_legal_list_sdata);
		
		$get_task_plan_legal_list_sdetails = $get_task_plan_legal_list_sstatement -> fetchAll();
		
		if(FALSE === $get_task_plan_legal_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_task_plan_legal_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_task_plan_legal_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new legal task plan
INPUT 	: Task Type, Process Plan ID, Planned End Date, Planned Start Date, Document Path, Added By
OUTPUT 	: Task Plan id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_task_plan_legal($type,$process_plan,$bprocess_plan,$planned_end_date,$planned_start_date,$document,$added_by)
{
	// Query
    $task_plan_iquery = "insert into task_plan_legal (task_plan_legal_type,task_plan_legal_process_plan_id,task_plan_legal_bulk_process_plan_id,task_plan_planned_end_date,task_plan_planned_start_date,task_plan_actual_start_date,task_plan_actual_end_date,task_plan_document_path,task_plan_delay_reason,task_plan_delay_remarks,task_plan_active,task_plan_added_by,task_plan_added_on,task_plan_approved_on) values (:type,:process_plan,:bulk_process_plan,:planned_end,:planned_start,:actual_start,:actual_end,:document,:delay_reason,:delay_remarks,:active,:added_by,:now,:approved_on)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $task_plan_istatement = $dbConnection->prepare($task_plan_iquery);
        
        // Data		
        $task_plan_idata = array(':type'=>$type,':process_plan'=>$process_plan,':bulk_process_plan'=>$bprocess_plan,':planned_end'=>$planned_end_date,':planned_start'=>$planned_start_date,':actual_start'=>'',':actual_end'=>'',':document'=>$document,':delay_reason'=>'1',':delay_remarks'=>'',':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"),':approved_on'=>'');		
		$dbConnection->beginTransaction();
        $task_plan_istatement->execute($task_plan_idata);
		$task_plan_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $task_plan_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To add new reason to reason master
INPUT 	: Reason Type, Added By
OUTPUT 	: Reason Master id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_reason_master($reason_type,$added_by)
{
	// Query
    $reason_master_iquery = "insert into reason_master (reason,reason_priority,reason_master_active,reason_master_added_by,reason_master_added_on) values (:reason,:priority,:active,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $reason_master_istatement = $dbConnection->prepare($reason_master_iquery);
        
        // Data		
        $reason_master_idata = array(':reason'=>$reason_type,':priority'=>'0',':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $reason_master_istatement->execute($reason_master_idata);
		$reason_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $reason_master_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE : To add new task type
INPUT 	: Task type, Process Type, Max Duration, Is Document, Added By
OUTPUT 	: Process Type id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_task_type_legal($task_type,$process_type_id,$max_duration,$is_document,$added_by)
{
	// Query
    $task_type_iquery = "insert into task_type_master (task_type_name,task_type_process,task_type_active,task_type_maximum_expected_duration,task_type_is_document_mandatory,task_type_added_by,task_type_added_on) values (:task_type,:process_type,:active,:max_duration,:document,:added_by,:now)";  	
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $task_type_istatement = $dbConnection->prepare($task_type_iquery);
        
        // Data
        $task_type_idata = array(':task_type'=>$task_type,':process_type'=>$process_type_id,':active'=>'1',':max_duration'=>$max_duration,':document'=>$is_document,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			
		$dbConnection->beginTransaction();
        $task_type_istatement->execute($task_type_idata);
		$task_type_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $task_type_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE: To update task plan
INPUT  : Task Plan ID, Planned End Date, Start Date, Actual End Date, Reason, Remarks, Document, Approved On
OUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_task_plan($task_plan_id,$planned_end,$start_date,$actual_end,$reason,$remarks,$document,$approved_on)
{
	// Query
    $task_plan_uquery = "update task_plan_legal set task_plan_planned_end_date=:planned_end,task_plan_actual_start_date=:start_date, task_plan_actual_end_date=:actual_end, task_plan_delay_reason=:reason, task_plan_delay_remarks=:remarks, task_plan_document_path=:document, task_plan_approved_on=:approved_on where task_plan_legal_id=:task_plan_id";   		
    try
    {
        $dbConnection = get_conn_handle();
        
        $task_plan_ustatement = $dbConnection->prepare($task_plan_uquery);
        
        // Data
        $task_plan_udata = array(':planned_end'=>$planned_end,':start_date'=>$start_date,':actual_end'=>$actual_end,':reason'=>$reason,':remarks'=>$remarks,':document'=>$document,':approved_on'=>$approved_on,':task_plan_id'=>$task_plan_id);			
        
        $task_plan_ustatement -> execute($task_plan_udata);
		
		$updated_rows = $task_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add task delay reason
INPUT 	: Task, Reason, Remarks, Added By
OUTPUT 	: Delay Reason id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_task_delay_reason($task_id,$sub_task,$reason_id,$remarks,$added_by)
{
	// Query
    $delay_reason_iquery = "insert into reasons (reason_task,reason_sub_task,reason,reason_remarks,reason_added_by,reason_added_on) values (:task,:sub_task,:reason,:remarks,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $delay_reason_istatement = $dbConnection->prepare($delay_reason_iquery);
        
        // Data		
        $delay_reason_idata = array(':task'=>$task_id,':sub_task'=>$sub_task,':reason'=>$reason_id,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		
		$dbConnection->beginTransaction();
        $delay_reason_istatement->execute($delay_reason_idata);
		$delay_reason_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $delay_reason_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE: To update assignee of a task
INPUT  : Task Plan ID, Assigned To
OUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_assignee($task_plan_id,$assigned_to)
{
	// Query
    $task_plan_uquery = "update task_plan_legal set task_plan_added_by=:assigned_to where task_plan_legal_id=:task_plan_id";   

    try
    {
        $dbConnection = get_conn_handle();
        
        $task_plan_ustatement = $dbConnection->prepare($task_plan_uquery);
        
        // Data
        $task_plan_udata = array(':assigned_to'=>$assigned_to,':task_plan_id'=>$task_plan_id);
        
        $task_plan_ustatement -> execute($task_plan_udata);
		
		$updated_rows = $task_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE: To enable/disable task plan
INPUT  : Task Plan ID, Active Status
OUTPUT : SUCCESS if task plan enabled/disabled; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_enable_disable_task_plan($task_plan_id,$process_plan_id,$active)
{
	// Query
	if($process_plan_id != "")
	{
		$task_plan_uquery = "update task_plan_legal set task_plan_active=:active where task_plan_legal_process_plan_id=:process_plan_id";   
		// Data
        $task_plan_udata = array(':process_plan_id'=>$process_plan_id,':active'=>$active);
	}
	else if($task_plan_id != "")
	{
		$task_plan_uquery = "update task_plan_legal set task_plan_active=:active where task_plan_legal_id=:task_plan_id";   
		// Data
        $task_plan_udata = array(':task_plan_id'=>$task_plan_id,':active'=>$active);
	}

    try
    {
        $dbConnection = get_conn_handle();
        
        $task_plan_ustatement = $dbConnection->prepare($task_plan_uquery);                			
        
        $task_plan_ustatement -> execute($task_plan_udata);		
		
		$return["status"] = SUCCESS;
		$return["data"]   = '';
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE: To enable/disable task type
INPUT  : Task Type ID, Active
OUTPUT : SUCCESS if task type enabled/disabled; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_enable_disable_task_type($task_type_id,$active)
{
	// Query 	
	$task_type_uquery = "update task_type_master set task_type_active = :active where task_type_id = :task_type";   
	// Data
	$task_type_udata = array(':task_type'=>$task_type_id,':active'=>$active);
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $task_type_ustatement = $dbConnection->prepare($task_type_uquery);       		
        
        $task_type_ustatement -> execute($task_type_udata);
		
		$return["status"] = DB_NO_RECORD;
		$return["data"]   = '';
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE: To update reason master
INPUT  : Reason Master ID, Reason Master Update Data
OUTPUT : SUCCESS if reason updated;  FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_reason_master($reason_id,$reason_data)
{
	$reason_uquery_base = "update reason_master set";
	
	$reason_uquery_set = "";
	
	$reason_uquery_where = " where reason_master_id = :reason";
	
	$reason_udata = array();
	$reason_udata[":reason"] = $reason_id;
	
	// Query
    if(array_key_exists('status',$reason_data))
	{
		$reason_uquery_set = $reason_uquery_set." reason_master_active = :status,";
		$reason_udata[":status"] = $reason_data['status'];
	}
	
	$reason_uquery_set = trim($reason_uquery_set,',');
	
	$reason_uquery = $reason_uquery_base.$reason_uquery_set.$reason_uquery_where;	
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $reason_ustatement = $dbConnection->prepare($reason_uquery);                		
        
        $reason_ustatement -> execute($reason_udata);
		
		$updated_rows = $reason_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
/*
PURPOSE: To update task plan
INPUT  : Reason Id,Sub_task
OUTPUT : SUCCESS if updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Sonakshi 
*/
function db_update_sub_task($sub_task,$reason_id)
{
	// Query
    $task_update_uquery = "update reasons set reason_sub_task=:sub_task where reason_id=:reason_id";  		
    try
    {
        $dbConnection = get_conn_handle();
        
        $task_update_ustatement = $dbConnection->prepare($task_update_uquery);
        
        // Data
        $task_update_udata = array(':sub_task'=>$sub_task,':reason_id'=>$reason_id);			
        
        $task_update_ustatement -> execute($task_update_udata);
		
		$updated_rows = $task_update_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>