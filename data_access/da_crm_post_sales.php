<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/



/*

PURPOSE : To add new site blocking

INPUT 	: Client, Site ID, Remarks, Added By

OUTPUT 	: Site Blocking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_blocking($client,$site_id,$remarks,$added_by)

{

	// Query

    $site_block_iquery = "insert into crm_site_blocking (crm_blocking_client_profile,crm_blocking_site_id,crm_blocking_remarks,crm_blocking_status,crm_block_request_added_by,crm_block_request_added_on) values (:client,:site,:remarks,:status,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $site_block_istatement = $dbConnection->prepare($site_block_iquery);

        

        // Data

        $site_block_idata = array(':client'=>$client,':site'=>$site_id,':remarks'=>$remarks,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $site_block_istatement->execute($site_block_idata);

		$site_block_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $site_block_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To add new site blocking history

INPUT 	: Blocking ID, Action (Blocking or Unblocking), Remarks, Added By

OUTPUT 	: Site Blocking History id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_blocking_history($blocking_id,$action,$remarks,$added_by)

{

	// Query

    $site_block_history_iquery = "insert into crm_site_blocking_history (crm_site_blocking_history_blocking_id,crm_site_blocking_history_action,crm_site_blocking_history_remarks,crm_site_blocking_history_blocked_by,crm_site_blocking_history_blocked_on) values (:blocking_id,:action,:remarks,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $site_block_history_istatement = $dbConnection->prepare($site_block_history_iquery);

        

        // Data

        $site_block_history_idata = array(':blocking_id'=>$blocking_id,':action'=>$action,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $site_block_history_istatement->execute($site_block_history_idata);

		$site_block_history_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $site_block_history_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get site blocking list

INPUT 	: 

OUTPUT 	: List of sites currently blocked

BY 		: Nitin Kashyap

*/

function db_get_site_blocking($blocking_id,$profile_id,$project_id,$site_id,$status,$added_by,$start_date,$end_date,$project_user="")

{

	$get_blocked_list_squery_base = "select * from crm_site_blocking SB inner join crm_site_master SM on SM.crm_site_id = SB.crm_blocking_site_id inner join crm_project_master PM on PM.project_id = SM.crm_project_id inner join crm_prospective_profile PP on PP.crm_prospective_profile_id = SB.crm_blocking_client_profile inner join crm_enquiry CE on CE.enquiry_id = PP.crm_prospective_profile_enquiry inner join users U on U.user_id = SB.crm_block_request_added_by inner join crm_dimension_master CDM on CDM.crm_dimension_id = SM.crm_site_dimension inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source";		

	

	$get_blocked_list_squery_where = "";

	

	$get_blocked_list_squery_order = "";		

	

	$filter_count = 0;

	

	// Data

	$get_blocked_list_sdata = array();

	

	if($blocking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where crm_blocking_id=:blocking_id";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and crm_blocking_id=:blocking_id";				

		}

		

		// Data

		$get_blocked_list_sdata[':blocking_id']  = $blocking_id;

		

		$filter_count++;

	}

	

	if($profile_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where crm_blocking_client_profile=:profile_id";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and crm_blocking_client_profile=:profile_id";				

		}

		

		// Data

		$get_blocked_list_sdata[':profile_id']  = $profile_id;

		

		$filter_count++;

	}

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where SM.crm_project_id=:project_id";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and SM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_blocked_list_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($site_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where crm_blocking_site_id=:site_id";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and crm_blocking_site_id=:site_id";				

		}

		if($project_user != "")
		{
			if($filter_count == 0)
			{
				// Query
				$get_blocked_list_squery_where = $get_blocked_list_squery_where." where CE.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";								
			}
			else
			{
				// Query
				$get_blocked_list_squery_where = $get_blocked_list_squery_where." and CSM.crm_project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id)";				
			}

			// Data
			$get_blocked_list_squery_where[':project_user_id']  = $project_user;

			$filter_count++;
		}

		

		// Data

		$get_blocked_list_sdata[':site_id']  = $site_id;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where crm_blocking_status=:status";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and crm_blocking_status=:status";				

		}

		

		// Data

		$get_blocked_list_sdata[':status']  = $status;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where crm_block_request_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and crm_block_request_added_by=:added_by";				

		}

		

		// Data

		$get_blocked_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where crm_block_request_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and crm_block_request_added_on >= :start_date";				

		}

		

		//Data

		$get_blocked_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." where crm_block_request_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_blocked_list_squery_where = $get_blocked_list_squery_where." and crm_block_request_added_on <= :end_date";				

		}

		

		//Data

		$get_blocked_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_blocked_list_squery = $get_blocked_list_squery_base.$get_blocked_list_squery_where.$get_blocked_list_squery_order;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_blocked_list_sstatement = $dbConnection->prepare($get_blocked_list_squery);

		

		$get_blocked_list_sstatement -> execute($get_blocked_list_sdata);

		

		$get_blocked_list_sdetails = $get_blocked_list_sstatement -> fetchAll();

		

		if(FALSE === $get_blocked_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_blocked_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_blocked_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update blocking ID  

INPUT 	: Blocking ID, Status

OUTPUT 	: Blocking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_update_blocking_id($blocking_id,$blocked_status)

{

	// Query

    $blocking_uquery = "update crm_site_blocking set crm_blocking_status=:status where crm_blocking_id=:blocking_id";  



    try

    {

        $dbConnection = get_conn_handle();

        

        $blocking_ustatement = $dbConnection->prepare($blocking_uquery);

        

        // Data

        $blocking_udata = array(':status'=>$blocked_status,':blocking_id'=>$blocking_id);		

        

        $blocking_ustatement -> execute($blocking_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $blocking_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add new site booking

INPUT 	: Client, Site ID, Initial Amount, Initial Pay Details, Consideration Area, Sq. Ft Rate, Premium Type, Remarks, Added By

OUTPUT 	: Site Booking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_booking($client,$site_id,$initial_amount,$initial_pay_details,$consideration_area,$rate_sqft,$premium_type,$remarks,$added_by)

{

	// Query

    $site_book_iquery = "insert into crm_booking (crm_booking_site_id,crm_booking_client_profile,crm_booking_initial_amount,crm_booking_initial_payment_details,crm_booking_consideration_area,crm_booking_rate_per_sq_ft,crm_booking_premium_type,crm_booking_remarks,crm_booking_status,crm_booking_date,crm_booking_added_by,crm_booking_added_on,crm_booking_approved_by,crm_booking_approved_on) values (:site,:client,:initial_amount,:initial_pay_details,:consideration_area,:rate_sqft,:premium_type,:remarks,:status,:booking_date,:added_by,:now,:approved_by,:approved_on)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $site_book_istatement = $dbConnection->prepare($site_book_iquery);

        

        // Data

        $site_book_idata = array(':client'=>$client,':site'=>$site_id,':initial_amount'=>$initial_amount,':initial_pay_details'=>$initial_pay_details,':consideration_area'=>$consideration_area,':rate_sqft'=>$rate_sqft,':premium_type'=>$premium_type,':remarks'=>$remarks,':status'=>'0',':booking_date'=>'',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"),':approved_by'=>'',':approved_on'=>'');			

		

		$dbConnection->beginTransaction();

        $site_book_istatement->execute($site_book_idata);

		$site_book_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $site_book_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get site booking list

INPUT 	: Booking ID, Project ID, Site ID, Profile ID, Premium, Status, Added By, Start Date (Added On), End Date (Added On), Approved By, Start Date (Approved On), End Date (Approved On), Enquiry ID, Is the current state of site BOOKED, Booking Date Filter Start, Booking Date Filter End, Site No, Start Date for Enquiry Date filter (to identify when this lead got into KNS system), End Date for Enquiry Date Filter, Enquiry Source, Order for sorting, Source Type

OUTPUT 	: List of sites currently booked

BY 		: Nitin Kashyap

*/

function db_get_site_booking($booking_id,$project_id,$site_id,$profile_id,$premium,$status,$added_by,$start_date,$end_date,$approved_by,$approved_start_date,$approved_end_date,$enquiry_id="",$is_active="",$booking_date_start="",$booking_date_end="",$site_no="",$enquiry_date_start="",$enquiry_date_end="",$source="",$order="",$source_type="",$project_user="")

{

	$get_booked_list_squery_base = "select *,SU.user_name as stm from crm_booking CB inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master PM on PM.project_id = CSM.crm_project_id inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status left outer join users SU on SU.user_id = CE.assigned_to";		

	

	$get_booked_list_squery_where = "";

	

	$get_booked_list_squery_order = "";		

	

	$filter_count = 0;

	

	// Data

	$get_booked_list_sdata = array();

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CB.crm_booking_id=:booking_id";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CB.crm_booking_id=:booking_id";				

		}

		

		// Data

		$get_booked_list_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CSM.crm_project_id=:project_id";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_booked_list_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($site_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_site_id=:site_id";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_site_id=:site_id";				

		}

		

		// Data

		$get_booked_list_sdata[':site_id']  = $site_id;

		

		$filter_count++;

	}

	

	if($profile_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_client_profile=:profile_id";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_client_profile=:profile_id";				

		}

		

		// Data

		$get_booked_list_sdata[':profile_id']  = $profile_id;

		

		$filter_count++;

	}

	

	if($premium != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_premium_type=:premium_type";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_premium_type=:premium_type";				

		}

		

		// Data

		$get_booked_list_sdata[':premium_type']  = $premium;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_status=:status";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_status=:status";				

		}

		

		// Data

		$get_booked_list_sdata[':status']  = $status;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CE.assigned_to = :added_by";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CE.assigned_to = :added_by";				

		}

		

		// Data

		$get_booked_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_added_on >= :start_date";				

		}

		

		//Data

		$get_booked_list_sdata[':start_date']  = $start_date." 00:00:00";

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_added_on <= :end_date";				

		}

		

		//Data

		$get_booked_list_sdata[':end_date']  = $end_date." 23:59:59";

		

		$filter_count++;

	}

	

	if($approved_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_approved_by=:added_by";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_approved_by=:added_by";				

		}

		

		// Data

		$get_booked_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($approved_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_approved_on >= :app_start_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_approved_on >= :app_start_date";				

		}

		

		//Data

		$get_booked_list_sdata[':app_start_date']  = $approved_start_date." 00:00:00";

		

		$filter_count++;

	}



	if($approved_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_approved_on <= :app_end_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_approved_on <= :app_end_date";				

		}

		

		//Data

		$get_booked_list_sdata[':app_end_date']  = $approved_end_date." 23:59:59";

		

		$filter_count++;

	}

	

	if($booking_date_start != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date >= :bk_start_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date >= :bk_start_date";				

		}

		

		//Data

		$get_booked_list_sdata[':bk_start_date']  = $booking_date_start." 00:00:00";

		

		$filter_count++;

	}

	

	if($booking_date_end != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date <= :bk_end_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date <= :bk_end_date";				

		}

		

		//Data

		$get_booked_list_sdata[':bk_end_date']  = $booking_date_end." 23:59:59";

		

		$filter_count++;

	}

	

	if($enquiry_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CE.enquiry_id = :enquiry_id";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CE.enquiry_id = :enquiry_id";				

		}

		

		//Data

		$get_booked_list_sdata[':enquiry_id']  = $enquiry_id;

		

		$filter_count++;

	}

	

	if($is_active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CSM.crm_site_status = :site_status";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CSM.crm_site_status = :site_status";				

		}

		

		//Data

		$get_booked_list_sdata[':site_status']  = '3';

		

		$filter_count++;

	}

	

	if($site_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CSM.crm_site_no = :site_no";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CSM.crm_site_no = :site_no";				

		}

		

		//Data

		$get_booked_list_sdata[':site_no']  = $site_no;

		

		$filter_count++;

	}

	

	if($enquiry_date_start != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CE.added_on >= :enq_start_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CE.added_on >= :enq_start_date";				

		}

		

		//Data

		$get_booked_list_sdata[':enq_start_date']  = $enquiry_date_start." 00:00:00";

		

		$filter_count++;

	}

	

	if($enquiry_date_end != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CE.added_on <= :enq_end_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CE.added_on <= :enq_end_date";				

		}

		

		//Data

		$get_booked_list_sdata[':enq_end_date']  = $enquiry_date_end." 23:59:59";

		

		$filter_count++;

	}

	

	if($source != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CE.source = :enq_source";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CE.source = :enq_source";				

		}

		

		//Data

		$get_booked_list_sdata[':enq_source']  = $source;

		

		$filter_count++;

	}

	

	if($source_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CESM.enquiry_source_master_type = :enq_source_type";

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CESM.enquiry_source_master_type = :enq_source_type";				

		}

		

		//Data

		$get_booked_list_sdata[':enq_source_type']  = $source_type;

		

		$filter_count++;

	}
	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_booked_list_squery_where = $get_booked_list_squery_where." where PM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";								
		}
		else
		{
			// Query
			$get_booked_list_squery_where = $get_booked_list_squery_where." and PM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";				
		}

		// Data
		$get_booked_list_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}

	if($order == '')

	{

		$get_booked_list_squery_order = " order by crm_booking_date asc";

	}

	else if($order == 'site_no')	

	{

		$get_booked_list_squery_order = " order by cast(CSM.crm_site_no as decimal) asc,CSM.crm_site_no asc";

	}
	else if($order == 'approval_date')	

	{

		$get_booked_list_squery_order = " order by crm_booking_approved_on asc";

	}

	

	$get_booked_list_squery = $get_booked_list_squery_base.$get_booked_list_squery_where.$get_booked_list_squery_order;
	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_booked_list_sstatement = $dbConnection->prepare($get_booked_list_squery);

		

		$get_booked_list_sstatement -> execute($get_booked_list_sdata);

		

		$get_booked_list_sdetails = $get_booked_list_sstatement -> fetchAll();

		

		if(FALSE === $get_booked_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_booked_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_booked_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add site cost

INPUT 	: Project, Value, Effective From Date, Added By

OUTPUT 	: Project Cost id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_cost($project,$value,$effective_date,$added_by)

{

	// Query

    $site_cost_iquery = "insert into crm_cost_master (crm_cost_project,crm_cost_value,crm_cost_effective_from,crm_cost_added_by,crm_cost_added_on) values (:project,:value,:effect_date,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $site_cost_istatement = $dbConnection->prepare($site_cost_iquery);

        

        // Data

        $site_cost_idata = array(':project'=>$project,':value'=>$value,':effect_date'=>$effective_date,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			

		

		$dbConnection->beginTransaction();

        $site_cost_istatement->execute($site_cost_idata);

		$site_cost_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $site_cost_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To add site discount levels based on role

INPUT 	: Role, Project, Value, Added By

OUTPUT 	: Project Discount id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_discount_perms($role,$project,$value,$added_by)

{

	// Query

    $site_discount_iquery = "insert into crm_discount_master (crm_discount_master_role,crm_discount_master_project,crm_discount_master_value,crm_discount_master_added_by,crm_discount_master_added_on) values (:role,:project,:value,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $site_discount_istatement = $dbConnection->prepare($site_discount_iquery);

        

        // Data

        $site_discount_idata = array(':role'=>$role,':project'=>$project,':value'=>$value,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			

		

		$dbConnection->beginTransaction();

        $site_discount_istatement->execute($site_discount_idata);

		$site_discount_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $site_discount_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get site cost

INPUT 	: Project ID, Added By, Start Date (Added On), End Date (Added On)

OUTPUT 	: List of costs for this project

BY 		: Nitin Kashyap

*/

function db_get_site_cost($project_id,$effective_start_date,$effective_end_date,$added_by,$start_date,$end_date)

{

	$get_site_cost_list_squery_base = "select * from crm_cost_master";		

	

	$get_site_cost_list_squery_where = "";

	

	$get_site_cost_list_squery_order = "";		

	

	$filter_count = 0;

	

	// Data

	$get_site_cost_list_sdata = array();

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." where crm_cost_project=:project_id";								

		}

		else

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." and crm_cost_project=:project_id";				

		}

		

		// Data

		$get_site_cost_list_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($effective_start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." where crm_cost_effective_from >= :effective_start_date";					

		}

		else

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." and crm_cost_effective_from >= :effective_start_date";				

		}

		

		//Data

		$get_site_cost_list_sdata[':effective_start_date']  = $effective_start_date;

		

		$filter_count++;

	}



	if($effective_end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." where crm_cost_effective_from <= :effective_end_date";					

		}

		else

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." and crm_cost_effective_from <= :effective_end_date";				

		}

		

		//Data

		$get_site_cost_list_sdata[':effective_end_date']  = $effective_end_date;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." where crm_cost_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." and crm_cost_added_by=:added_by";				

		}

		

		// Data

		$get_site_cost_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." where crm_cost_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." and crm_cost_added_on >= :start_date";				

		}

		

		//Data

		$get_site_cost_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." where crm_cost_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_site_cost_list_squery_where = $get_site_cost_list_squery_where." and crm_cost_added_on <= :end_date";				

		}

		

		//Data

		$get_site_cost_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_site_cost_list_squery_order = " order by crm_cost_id desc";

	

	$get_site_cost_list_squery = $get_site_cost_list_squery_base.$get_site_cost_list_squery_where.$get_site_cost_list_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_site_cost_list_sstatement = $dbConnection->prepare($get_site_cost_list_squery);

		

		$get_site_cost_list_sstatement -> execute($get_site_cost_list_sdata);

		

		$get_site_cost_list_sdetails = $get_site_cost_list_sstatement -> fetchAll();

		

		if(FALSE === $get_site_cost_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_site_cost_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_site_cost_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get site discount threshold

INPUT 	: Project ID, Role, Added By, Start Date (Added On), End Date (Added On)

OUTPUT 	: List of discount thresholds

BY 		: Nitin Kashyap

*/

function db_get_site_discount($project_id,$role,$added_by,$start_date,$end_date)

{

	$get_site_discount_list_squery_base = "select * from crm_discount_master";		

	

	$get_site_discount_list_squery_where = "";

	

	$get_site_discount_list_squery_order = "";		

	

	$filter_count = 0;

	

	// Data

	$get_site_discount_list_sdata = array();

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." where crm_discount_master_project=:project_id";				

		}

		else

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." and crm_discount_master_project=:project_id";				

		}

		

		// Data

		$get_site_discount_list_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($role != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." where crm_discount_master_role=:role_id";				

		}

		else

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." and crm_discount_master_role=:role_id";				

		}

		

		// Data

		$get_site_discount_list_sdata[':role_id']  = $role;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." where crm_discount_master_added_by=:added_by";					

		}

		else

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." and crm_discount_master_added_by=:added_by";				

		}

		

		// Data

		$get_site_discount_list_sdata[':added_by'] = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." where crm_discount_master_added_on >= :start_date";				

		}

		else

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." and crm_discount_master_added_on >= :start_date";				

		}

		

		//Data

		$get_site_discount_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." where crm_discount_master_added_on <= :end_date";					

		}

		else

		{

			// Query

			$get_site_discount_list_squery_where = $get_site_discount_list_squery_where." and crm_discount_master_added_on <= :end_date";				

		}

		

		//Data

		$get_site_discount_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_site_discount_list_squery_order = " order by crm_discount_master_id desc";

	

	$get_site_discount_list_squery = $get_site_discount_list_squery_base.$get_site_discount_list_squery_where.$get_site_discount_list_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_site_discount_list_sstatement = $dbConnection->prepare($get_site_discount_list_squery);

		

		$get_site_discount_list_sstatement -> execute($get_site_discount_list_sdata);

		

		$get_site_discount_list_sdetails = $get_site_discount_list_sstatement -> fetchAll();

		

		if(FALSE === $get_site_discount_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_site_discount_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_site_discount_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update booking status  

INPUT 	: Booking ID, Approval Status, User, Booking Date, Booking user, Consideration Area, Booking Rate

OUTPUT 	: Booking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_update_booking_status($booking_id,$approval,$approved_by,$booking_date,$booking_user,$consideration_area,$booking_rate)

{

	// Query

    $booking_uquery_base = "update crm_booking set";

	

	$booking_uquery_set = "";

	

	$booking_uquery_where = " where crm_booking_id=:booking_id";

	$booking_udata = array(":booking_id"=>$booking_id);

	

	$filter_count = 0;

	

	if($approval != "")

	{

		$booking_uquery_set = $booking_uquery_set." crm_booking_status=:status,";

		$booking_udata[":status"] = $approval;

		$filter_count++;

	}

	

	if($approved_by != "")

	{

		$booking_uquery_set = $booking_uquery_set." crm_booking_approved_by=:approved_by,crm_booking_approved_on=:approved_on,";

		$booking_udata[":approved_by"] = $approved_by;

		$booking_udata[":approved_on"] = date("Y-m-d H:i:s");

		$filter_count++;

		$filter_count++;

	}

	

	if($booking_date != "")

	{

		$booking_uquery_set = $booking_uquery_set." crm_booking_date=:booking_date,";

		$booking_udata[":booking_date"] = $booking_date;

		$filter_count++;

	}

	

	if($booking_user != "")

	{

		$booking_uquery_set = $booking_uquery_set." crm_booking_added_by=:added_by,";

		$booking_udata[":added_by"] = $booking_user;

		$filter_count++;

	}

	

	if($consideration_area != "")

	{

		$booking_uquery_set = $booking_uquery_set." crm_booking_consideration_area=:area,";

		$booking_udata[":area"] = $consideration_area;

		$filter_count++;

	}

	

	if($booking_rate != "")

	{

		$booking_uquery_set = $booking_uquery_set." crm_booking_rate_per_sq_ft=:rate,";

		$booking_udata[":rate"] = $booking_rate;

		$filter_count++;

	}

	

	$booking_uquery = $booking_uquery_base.trim($booking_uquery_set,',').$booking_uquery_where;	



    try

    {

        $dbConnection = get_conn_handle();

        

        $booking_ustatement = $dbConnection->prepare($booking_uquery);            

        

        $booking_ustatement -> execute($booking_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $booking_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add site premium values

INPUT 	: Project, Value, Added By

OUTPUT 	: Project Premium id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_premium_value($project,$premium,$value,$added_by)

{

	// Query

    $site_premium_iquery = "insert into crm_premium (crm_premium_project,crm_premium_type,crm_premium_value,crm_premium_added_by,crm_premium_added_on) values (:project,:type,:value,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $site_premium_istatement = $dbConnection->prepare($site_premium_iquery);

        

        // Data

        $site_premium_idata = array(':project'=>$project,':type'=>$premium,':value'=>$value,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			

		

		$dbConnection->beginTransaction();

        $site_premium_istatement->execute($site_premium_idata);

		$site_premium_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $site_premium_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get site premium values

INPUT 	: Project ID, Premium Type, Added By, Start Date (Added On), End Date (Added On)

OUTPUT 	: List of premium

BY 		: Nitin Kashyap

*/

function db_get_site_premium($project_id,$premium_type,$added_by,$start_date,$end_date)

{

	$get_site_premium_list_squery_base = "select * from crm_premium";		

	

	$get_site_premium_list_squery_where = "";

	

	$get_site_premium_list_squery_order = "";		

	

	$filter_count = 0;

	

	// Data

	$get_site_premium_list_sdata = array();

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." where crm_premium_project=:project_id";				

		}

		else

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." and crm_premium_project=:project_id";				

		}

		

		// Data

		$get_site_premium_list_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($premium_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." where crm_premium_type=:premium_type";				

		}

		else

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." and crm_premium_type=:premium_type";				

		}

		

		// Data

		$get_site_premium_list_sdata[':premium_type']  = $premium_type;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." where crm_premium_added_by=:added_by";					

		}

		else

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." and crm_premium_added_by=:added_by";				

		}

		

		// Data

		$get_site_premium_list_sdata[':added_by'] = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." where crm_premium_added_on >= :start_date";				

		}

		else

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." and crm_premium_added_on >= :start_date";				

		}

		

		//Data

		$get_site_premium_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." where crm_premium_added_on <= :end_date";					

		}

		else

		{

			// Query

			$get_site_premium_list_squery_where = $get_site_premium_list_squery_where." and crm_premium_added_on <= :end_date";				

		}

		

		//Data

		$get_site_premium_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}

	

	$get_site_premium_list_squery_order = " order by crm_premium_id desc";

	

	$get_site_premium_list_squery = $get_site_premium_list_squery_base.$get_site_premium_list_squery_where.$get_site_premium_list_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_site_premium_list_sstatement = $dbConnection->prepare($get_site_premium_list_squery);

		

		$get_site_premium_list_sstatement -> execute($get_site_premium_list_sdata);

		

		$get_site_premium_list_sdetails = $get_site_premium_list_sstatement -> fetchAll();

		

		if(FALSE === $get_site_premium_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_site_premium_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_site_premium_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add new management site blocking

INPUT 	: Site ID, Management User, Reason, Remarks, Added By

OUTPUT 	: Site Blocking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_mgmt_blocking($site_id,$mgmt_user,$reason,$remarks,$added_by)

{

	// Query

    $mgmt_site_block_iquery = "insert into crm_management_block (crm_management_block_site_id,crm_management_block_user_id,crm_management_block_site_remarks,crm_management_block_reason,crm_management_block_status,crm_management_block_added_by,crm_management_block_added_on) values (:site,:mgmt_user,:remarks,:reason,:status,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $mgmt_site_block_istatement = $dbConnection->prepare($mgmt_site_block_iquery);

        

        // Data

        $mgmt_site_block_idata = array(':site'=>$site_id,':mgmt_user'=>$mgmt_user,':remarks'=>$remarks,':reason'=>$reason,':status'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	

		$dbConnection->beginTransaction();

        $mgmt_site_block_istatement->execute($mgmt_site_block_idata);

		$mgmt_site_block_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $mgmt_site_block_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get management site blocking

INPUT 	: Mgmt Block ID, Project ID, Site ID, Mgmt User, Reason, Status, Added By Start Date filter, End Date filter, Project User, Order of sorting

OUTPUT 	: List of premium

BY 		: Nitin Kashyap

*/

function db_get_site_mgmt_block_list($mgmt_block_id,$project_id,$site_id,$mgmt_user,$reason,$status,$added_by,$start_date,$end_date,$project_user="",$order="")

{

	$get_mgmt_block_list_squery_base = "select *,U.user_name as blocked_user,AU.user_name as added_user from crm_management_block CMB inner join crm_site_master CSM on CSM.crm_site_id = CMB.crm_management_block_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_management_block_reason_master CMBRM on CMBRM.crm_management_block_reason_id = CMB.crm_management_block_reason inner join users U on U.user_id = CMB.crm_management_block_user_id inner join users AU on AU.user_id = CMB.crm_management_block_added_by inner join crm_dimension_master CDM on CDM.crm_dimension_id = CSM.crm_site_dimension";		

	

	$get_mgmt_block_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_mgmt_block_list_sdata = array();

	

	if($mgmt_block_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_id=:block_id";				

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_id=:block_id";				

		}

		

		// Data

		$get_mgmt_block_list_sdata[':block_id']  = $mgmt_block_id;

		

		$filter_count++;

	}

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where CSM.crm_project_id=:project_id";				

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_mgmt_block_list_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($site_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_site_id=:site_id";				

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_site_id=:site_id";				

		}

		

		// Data

		$get_mgmt_block_list_sdata[':site_id']  = $site_id;

		

		$filter_count++;

	}

	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";								
		}
		else
		{
			// Query
			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";				
		}

		// Data
		$get_mgmt_block_list_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}

	if($mgmt_user != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_user_id=:mgmt_user";				

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_user_id=:mgmt_user";				

		}

		

		// Data

		$get_mgmt_block_list_sdata[':mgmt_user']  = $mgmt_user;

		

		$filter_count++;

	}

	

	if($reason != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_reason=:reason";				

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_reason=:reason";				

		}

		

		// Data

		$get_mgmt_block_list_sdata[':reason']  = $reason;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_status=:status";				

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_status=:status";				

		}

		

		// Data

		$get_mgmt_block_list_sdata[':status']  = $status;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_added_by=:added_by";					

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_added_by=:added_by";				

		}

		

		// Data

		$get_mgmt_block_list_sdata[':added_by'] = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_added_on >= :start_date";				

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_added_on >= :start_date";				

		}

		

		//Data

		$get_mgmt_block_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." where crm_management_block_added_on <= :end_date";					

		}

		else

		{

			// Query

			$get_mgmt_block_list_squery_where = $get_mgmt_block_list_squery_where." and crm_management_block_added_on <= :end_date";				

		}

		

		//Data

		$get_mgmt_block_list_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}	

	
	if($order == 'blocked_date')
	{
		$get_mgmt_block_list_squery_order = " order by crm_management_block_added_on";
	}
	else
	{
		$get_mgmt_block_list_squery_order = " order by cast(CSM.crm_site_no as decimal)";
	}

	$get_mgmt_block_list_squery = $get_mgmt_block_list_squery_base.$get_mgmt_block_list_squery_where.$get_mgmt_block_list_squery_order;	
	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_mgmt_block_list_sstatement = $dbConnection->prepare($get_mgmt_block_list_squery);

		

		$get_mgmt_block_list_sstatement -> execute($get_mgmt_block_list_sdata);

		

		$get_mgmt_block_list_sdetails = $get_mgmt_block_list_sstatement -> fetchAll();

		

		if(FALSE === $get_mgmt_block_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_mgmt_block_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_mgmt_block_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update mgmnt blocking ID  

INPUT 	: Mgmnt Blocking ID, Status

OUTPUT 	: Mgmnt Blocking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_update_mgmnt_blocking_id($mgmnt_blocking_id,$blocked_status)

{

	// Query

    $blocking_uquery = "update crm_management_block set crm_management_block_status=:status where crm_management_block_id=:blocking_id";  

    try

    {

        $dbConnection = get_conn_handle();

        

        $blocking_ustatement = $dbConnection->prepare($blocking_uquery);

        

        // Data

        $blocking_udata = array(':status'=>$blocked_status,':blocking_id'=>$mgmnt_blocking_id);		        

        $blocking_ustatement -> execute($blocking_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $mgmnt_blocking_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add agreement

INPUT 	: Booking ID, Registering Person, Remarks, Agreement Date, Added By

OUTPUT 	: Agreement ID, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_agreement($booking_id,$registerer,$remarks,$agreement_date,$added_by)

{

	// Query

    $agreement_iquery = "insert into crm_agreements (crm_agreement_booking_id,crm_agreement_registerer,crm_agreement_remarks,crm_agreement_status,crm_agreement_date,crm_agreement_added_by,crm_agreement_added_on) values (:booking_id,:registerer,:remarks,:status,:date,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $agreement_istatement = $dbConnection->prepare($agreement_iquery);

        

        // Data

        $agreement_idata = array(':booking_id'=>$booking_id,':registerer'=>$registerer,':remarks'=>$remarks,':status'=>'1',':date'=>$agreement_date,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		

		$dbConnection->beginTransaction();

        $agreement_istatement->execute($agreement_idata);

		$agreement_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $agreement_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get agreement list

INPUT 	: Agreement ID, Booking ID, Project ID, Site ID, Status, Agreement Date filter start, Agreement Date filter end, Added By, Start Date filter, End Date filter, Is the site in agreement status now, Site Number

OUTPUT 	: Agreement List

BY 		: Nitin Kashyap

*/

function db_get_agreement_list($agreement_id,$booking_id,$project_id,$site_id,$status,$agreement_date_start,$agreement_date_end,$added_by,$start_date,$end_date,$is_agreement,$site_no="",$project_user="")

{

	$get_agreement_squery_base = "select *,SU.user_name as stm from crm_agreements CA inner join crm_booking CB on CB.crm_booking_id = CA.crm_agreement_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join users U on U.user_id = CA.crm_agreement_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status left outer join users SU on SU.user_id = CE.assigned_to";		

	

	$get_agreement_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_agreement_sdata = array();

	

	if($agreement_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where crm_agreement_id=:agreement_id";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and crm_agreement_id=:agreement_id";				

		}

		

		// Data

		$get_agreement_sdata[':agreement_id']  = $agreement_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where crm_agreement_booking_id=:booking_id";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and crm_agreement_booking_id=:booking_id";				

		}

		

		// Data

		$get_agreement_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where CSM.crm_project_id=:project_id";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_agreement_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($site_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where CB.crm_booking_site_id=:site_id";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and CB.crm_booking_site_id=:site_id";				

		}

		

		// Data

		$get_agreement_sdata[':site_id']  = $site_id;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where CB.crm_booking_status=:status";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and CB.crm_booking_status=:status";				

		}

		

		// Data

		$get_agreement_sdata[':status']  = $status;

		

		$filter_count++;

	}

	

	if($agreement_date_start != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where crm_agreement_date >= :agreement_date_start";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and crm_agreement_date >= :agreement_date_start";				

		}

		

		// Data

		$get_agreement_sdata[':agreement_date_start']  = $agreement_date_start;

		

		$filter_count++;

	}

	

	if($agreement_date_end != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where crm_agreement_date <= :agreement_date_end";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and crm_agreement_date <= :agreement_date_end";				

		}

		

		// Data

		$get_agreement_sdata[':agreement_date_end']  = $agreement_date_end;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where CE.assigned_to = :added_by";					

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and CE.assigned_to = :added_by";				

		}

		

		// Data

		$get_agreement_sdata[':added_by'] = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where crm_agreement_added_on >= :start_date";				

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and crm_agreement_added_on >= :start_date";				

		}

		

		//Data

		$get_agreement_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where crm_agreement_added_on <= :end_date";					

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and crm_agreement_added_on <= :end_date";				

		}

		

		//Data

		$get_agreement_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}	
	
	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_agreement_squery_where = $get_agreement_squery_where." where CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";								
		}
		else
		{
			// Query
			$get_agreement_squery_where = $get_agreement_squery_where." and CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";				
		}

		// Data
		$get_agreement_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}
	
	

	if($is_agreement != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where CSM.crm_site_status = :site_status";					

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and CSM.crm_site_status = :site_status";				

		}

		

		//Data

		$get_agreement_sdata[':site_status']  = '4';

		

		$filter_count++;

	}



	if($site_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." where CSM.crm_site_no = :site_no";					

		}

		else

		{

			// Query

			$get_agreement_squery_where = $get_agreement_squery_where." and CSM.crm_site_no = :site_no";				

		}

		

		//Data

		$get_agreement_sdata[':site_no']  = $site_no;

		

		$filter_count++;

	}

	

	$get_agreement_squery_order = " order by cast(CSM.crm_site_no as decimal) asc, CSM.crm_site_no asc";

	$get_agreement_squery = $get_agreement_squery_base.$get_agreement_squery_where.$get_agreement_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_agreement_sstatement = $dbConnection->prepare($get_agreement_squery);

		

		$get_agreement_sstatement -> execute($get_agreement_sdata);

		

		$get_agreement_sdetails = $get_agreement_sstatement -> fetchAll();

		

		if(FALSE === $get_agreement_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_agreement_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_agreement_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add site booking cancellation

INPUT 	: Booking ID, Cancellation Date, Reason, Remarks, Added By

OUTPUT 	: Site Booking Cancellation id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_site_booking_cancellation($booking_id,$cancel_date,$reason,$remarks,$added_by)

{

	// Query

    $site_book_cancel_iquery = "insert into crm_cancellation (crm_cancellation_booking_id,crm_cancellation_date,crm_cancellation_reason,crm_cancellation_remarks,crm_cancellation_added_by,crm_cancellation_added_on) values (:booking_id,:cancel_date,:reason,:remarks,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $site_book_cancel_istatement = $dbConnection->prepare($site_book_cancel_iquery);

        

        // Data

        $site_book_cancel_idata = array(':booking_id'=>$booking_id,':cancel_date'=>$cancel_date,':reason'=>$reason,':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			

		

		$dbConnection->beginTransaction();

        $site_book_cancel_istatement->execute($site_book_cancel_idata);

		$site_book_cancel_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $site_book_cancel_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get booking cancellation list

INPUT 	: Cancellation Filter Data: Cancellation ID, Booking ID, Reason, Project

OUTPUT 	: Cancelled Booking List

BY 		: Nitin Kashyap

*/

function db_get_booking_cancellation_list($cancel_filter_data)

{

	$get_booking_cancellation_squery_base = "select *,CCRM.crm_cancellation_reason as cancel_reason_name from crm_cancellation CC inner join crm_booking CB on CB.crm_booking_id = CC.crm_cancellation_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join users U on U.user_id = CC.crm_cancellation_added_by inner join crm_cancellation_reason_master CCRM on CCRM.crm_cancellation_reason_id = CC.crm_cancellation_reason";		

	

	$get_booking_cancellation_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_booking_cancellation_sdata = array();

	

	if(array_key_exists('cancellation_id',$cancel_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." where crm_cancellation_id = :cancellation_id";				

		}

		else

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." and crm_cancellation_id = :cancellation_id";				

		}

		

		// Data

		$get_booking_cancellation_sdata[':cancellation_id']  = $cancel_filter_data["cancellation_id"];

		

		$filter_count++;

	}

	

	if(array_key_exists('booking_id',$cancel_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." where crm_cancellation_booking_id = :booking_id";				

		}

		else

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." and crm_cancellation_booking_id = :booking_id";				

		}

		

		// Data

		$get_booking_cancellation_sdata[':booking_id']  = $cancel_filter_data["booking_id"];

		

		$filter_count++;

	}

	

	if(array_key_exists('reason',$cancel_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." where crm_cancellation_reason = :reason";				

		}

		else

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." and crm_cancellation_reason = :reason";				

		}

		

		// Data

		$get_booking_cancellation_sdata[':reason']  = $cancel_filter_data["reason"];

		

		$filter_count++;

	}

	

	if(array_key_exists('project',$cancel_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." where CSM.crm_project_id = :project_id";				

		}

		else

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." and CSM.crm_project_id = :project_id";				

		}

		

		// Data

		$get_booking_cancellation_sdata[':project_id']  = $cancel_filter_data["project"];

		

		$filter_count++;

	}

	

	if(array_key_exists('site',$cancel_filter_data))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." where CB.crm_booking_site_id = :site_id";				

		}

		else

		{

			// Query

			$get_booking_cancellation_squery_where = $get_booking_cancellation_squery_where." and CB.crm_booking_site_id = :site_id";				

		}

		

		// Data

		$get_booking_cancellation_sdata[':site_id']  = $cancel_filter_data["site"];

		

		$filter_count++;

	}

	

	$get_booking_cancellation_squery_order = " order by cast(CSM.crm_site_no as decimal) asc, CSM.crm_site_no asc";

	$get_booking_cancellation_squery = $get_booking_cancellation_squery_base.$get_booking_cancellation_squery_where.$get_booking_cancellation_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_booking_cancellation_sstatement = $dbConnection->prepare($get_booking_cancellation_squery);

		

		$get_booking_cancellation_sstatement -> execute($get_booking_cancellation_sdata);

		

		$get_booking_cancellation_sdetails = $get_booking_cancellation_sstatement -> fetchAll();

		

		if(FALSE === $get_booking_cancellation_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_booking_cancellation_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_booking_cancellation_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add registration

INPUT 	: Booking ID, Remarks, Registration Date, Added By

OUTPUT 	: Registration ID, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_registration($booking_id,$remarks,$registration_date,$added_by)

{

	// Query

    $registration_iquery = "insert into crm_registration (crm_registration_booking_id,crm_registration_remarks,crm_registration_status,crm_registration_date,crm_registration_added_by,crm_registration_added_on) values (:booking_id,:remarks,:status,:date,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $registration_istatement = $dbConnection->prepare($registration_iquery);

        

        // Data

        $registration_idata = array(':booking_id'=>$booking_id,':remarks'=>$remarks,':status'=>'1',':date'=>$registration_date,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		

		$dbConnection->beginTransaction();

        $registration_istatement->execute($registration_idata);

		$registration_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $registration_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get registration list

INPUT 	: Registration ID, Booking ID, Project ID, Site ID, Status, Registration Date filter start, Registration Date filter end, Added By, Start Date filter, End Date filter, Is the site in registration status now, Site Number

OUTPUT 	: Registration List

BY 		: Nitin Kashyap

*/

function db_get_registration_list($registration_id,$booking_id,$project_id,$site_id,$status,$registration_date_start,$registration_date_end,$added_by,$start_date,$end_date,$is_registration,$site_no="",$project_user="")

{

	$get_registration_squery_base = "select *,SU.user_name as stm from crm_registration CR inner join crm_booking CB on CB.crm_booking_id = CR.crm_registration_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join users U on U.user_id = CR.crm_registration_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status left outer join users SU on SU.user_id = CE.assigned_to";		

	

	$get_registration_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_registration_sdata = array();

	

	if($registration_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where crm_registration_id=:registration_id";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and crm_registration_id=:registration_id";				

		}

		

		// Data

		$get_registration_sdata[':registration_id']  = $registration_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where crm_registration_booking_id=:booking_id";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and crm_registration_booking_id=:booking_id";				

		}

		

		// Data

		$get_registration_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where CSM.crm_project_id=:project_id";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_registration_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($site_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where CB.crm_booking_site_id=:site_id";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and CB.crm_booking_site_id=:site_id";				

		}

		

		// Data

		$get_registration_sdata[':site_id']  = $site_id;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where CB.crm_booking_status = :status";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and CB.crm_booking_status = :status";				

		}

		

		// Data

		$get_registration_sdata[':status']  = $status;

		

		$filter_count++;

	}

	

	if($registration_date_start != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where crm_registration_date >= :registration_date_start";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and crm_registration_date >= :registration_date_start";				

		}

		

		// Data

		$get_registration_sdata[':registration_date_start']  = $registration_date_start;

		

		$filter_count++;

	}

	

	if($registration_date_end != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where crm_registration_date <= :registration_date_end";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and crm_registration_date <= :registration_date_end";				

		}

		

		// Data

		$get_registration_sdata[':registration_date_end']  = $registration_date_end;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where CE.assigned_to = :added_by";					

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and CE.assigned_to = :added_by";				

		}

		

		// Data

		$get_registration_sdata[':added_by'] = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where crm_registration_added_on >= :start_date";				

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and crm_registration_added_on >= :start_date";				

		}

		

		//Data

		$get_registration_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where crm_registration_added_on <= :end_date";					

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and crm_registration_added_on <= :end_date";				

		}

		

		//Data

		$get_registration_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}	

	

	if($is_registration != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where CSM.crm_site_status = :site_status";					

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and CSM.crm_site_status = :site_status";				

		}

		

		//Data

		$get_registration_sdata[':site_status']  = '5';

		

		$filter_count++;

	}	

	

	if($site_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." where CSM.crm_site_no = :site_no";					

		}

		else

		{

			// Query

			$get_registration_squery_where = $get_registration_squery_where." and CSM.crm_site_no = :site_no";				

		}

		

		//Data

		$get_registration_sdata[':site_no']  = $site_no;

		

		$filter_count++;

	}	
	
	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_registration_squery_where = $get_registration_squery_where." where CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";								
		}
		else
		{
			// Query
			$get_registration_squery_where = $get_registration_squery_where." and CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";				
		}

		// Data
		$get_registration_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}

	

	$get_registration_squery_order = " order by cast(CSM.crm_site_no as decimal) asc, CSM.crm_site_no asc";

	$get_registration_squery = $get_registration_squery_base.$get_registration_squery_where.$get_registration_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_registration_sstatement = $dbConnection->prepare($get_registration_squery);

		

		$get_registration_sstatement -> execute($get_registration_sdata);

		

		$get_registration_sdetails = $get_registration_sstatement -> fetchAll();

		

		if(FALSE === $get_registration_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_registration_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_registration_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update agreement status  

INPUT 	: Agreement ID, Status, Agreement Date

OUTPUT 	: Booking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_update_agreement_status($agreement_id,$status,$agreement_date)

{

	// Query

    $agreement_uquery_base = "update crm_agreements set";

	

	$agreement_uquery_set = "";

	

	$agreement_uquery_where = " where crm_agreement_id=:agreement_id";

	$agreement_udata = array(":agreement_id"=>$agreement_id);

	

	$filter_count = 0;

	

	if($status != "")

	{

		$agreement_uquery_set = $agreement_uquery_set." crm_agreement_status=:status,";

		$agreement_udata[":status"] = $status;

		$filter_count++;

	}

	

	if($agreement_date != "")

	{

		$booking_uquery_set = $agreement_uquery_set." crm_agreement_date=:agreement_date,";

		$agreement_udata[":agreement_date"] = $agreement_date;

		$filter_count++;

	}

	

	$agreement_uquery = $agreement_uquery_base.trim($agreement_uquery_set,',').$agreement_uquery_where;	



    try

    {

        $dbConnection = get_conn_handle();

        

        $agreement_ustatement = $dbConnection->prepare($agreement_uquery);            

        

        $agreement_ustatement -> execute($agreement_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $agreement_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add Katha Transfer

INPUT 	: Booking ID, Remarks, Katha Transferred Date, Added By

OUTPUT 	: Katha Transfer ID, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_katha_transfer($booking_id,$remarks,$kt_date,$added_by)

{

	// Query

    $kt_iquery = "insert into crm_katha_transfer (crm_katha_transfer_booking_id,crm_katha_transfer_remarks,crm_katha_transfer_status,crm_katha_transfer_date,crm_katha_transfer_added_by,crm_katha_transfer_added_on) values (:booking_id,:remarks,:status,:date,:added_by,:now)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $kt_istatement = $dbConnection->prepare($kt_iquery);

        

        // Data

        $kt_idata = array(':booking_id'=>$booking_id,':remarks'=>$remarks,':status'=>'1',':date'=>$kt_date,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));				

		$dbConnection->beginTransaction();

        $kt_istatement->execute($kt_idata);

		$kt_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $kt_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Katha Transfer list

INPUT 	: Katha Transfer ID, Booking ID, Project ID, Site ID, Status, Katha Tansfer Date filter start, Katha Transfer Date filter end, Added By, Start Date filter, End Date filter, Is the site in Katha Transfer status now, Site Number

OUTPUT 	: Katha Transfer List

BY 		: Nitin Kashyap

*/

function db_get_katha_transfer_list($kt_id,$booking_id,$project_id,$site_id,$status,$kt_date_start,$kt_date_end,$added_by,$start_date,$end_date,$is_kt,$site_no="",$project_user="")

{

	$get_kt_squery_base = "select *,SU.user_name as stm from crm_katha_transfer CKT inner join crm_booking CB on CB.crm_booking_id = CKT.crm_katha_transfer_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join users U on U.user_id = CKT.crm_katha_transfer_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status left outer join users SU on SU.user_id = CE.assigned_to";		

	

	$get_kt_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_kt_sdata = array();

	

	if($kt_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where crm_katha_transfer_id=:kt_id";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and crm_katha_transfer_id=:kt_id";				

		}

		

		// Data

		$get_kt_sdata[':kt_id']  = $kt_id;

		

		$filter_count++;

	}

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where crm_katha_transfer_booking_id=:booking_id";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and crm_katha_transfer_booking_id=:booking_id";				

		}

		

		// Data

		$get_kt_sdata[':booking_id']  = $booking_id;

		

		$filter_count++;

	}

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where CSM.crm_project_id=:project_id";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_kt_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($site_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where CB.crm_booking_site_id=:site_id";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and CB.crm_booking_site_id=:site_id";				

		}

		

		// Data

		$get_kt_sdata[':site_id']  = $site_id;

		

		$filter_count++;

	}
	
	if($project_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_squery_where = $get_kt_squery_where." where CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";								
		}
		else
		{
			// Query
			$get_kt_squery_where = $get_kt_squery_where." and CPM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";				
		}

		// Data
		$get_kt_sdata[':project_user_id']  = $project_user;

		$filter_count++;
	}
	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where CB.crm_booking_status = :status";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and CB.crm_booking_status = :status";				

		}

		

		// Data

		$get_kt_sdata[':status']  = $status;

		

		$filter_count++;

	}

	

	if($kt_date_start != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where crm_katha_transfer_date >= :kt_date_start";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and crm_katha_transfer_date >= :kt_date_start";				

		}

		

		// Data

		$get_kt_sdata[':kt_date_start']  = $kt_date_start;

		

		$filter_count++;

	}

	

	if($kt_date_end != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where crm_katha_transfer_date <= :kt_date_end";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and crm_katha_transfer_date <= :kt_date_end";				

		}

		

		// Data

		$get_kt_sdata[':kt_date_end']  = $kt_date_end;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where CE.assigned_to = :added_by";					

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and CE.assigned_to = :added_by";				

		}

		

		// Data

		$get_kt_sdata[':added_by'] = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where crm_katha_transfer_added_on >= :start_date";				

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and crm_katha_transfer_added_on >= :start_date";				

		}

		

		//Data

		$get_kt_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where crm_katha_transfer_added_on <= :end_date";					

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and crm_katha_transfer_added_on <= :end_date";				

		}

		

		//Data

		$get_kt_sdata[':end_date']  = $end_date;

		

		$filter_count++;

	}	

	

	if($is_kt != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where CSM.crm_site_status = :site_status";					

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and CSM.crm_site_status = :site_status";				

		}

		

		//Data

		$get_kt_sdata[':site_status']  = '6';

		

		$filter_count++;

	}



	if($site_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." where CSM.crm_site_no = :site_no";					

		}

		else

		{

			// Query

			$get_kt_squery_where = $get_kt_squery_where." and CSM.crm_site_no = :site_no";				

		}

		

		//Data

		$get_kt_sdata[':site_no']  = $site_no;

		

		$filter_count++;

	}

	

	$get_kt_squery_order = " order by cast(CSM.crm_site_no as decimal) asc, CSM.crm_site_no asc";

	$get_kt_squery = $get_kt_squery_base.$get_kt_squery_where.$get_kt_squery_order;	

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_kt_sstatement = $dbConnection->prepare($get_kt_squery);

		

		$get_kt_sstatement -> execute($get_kt_sdata);

		

		$get_kt_sdetails = $get_kt_sstatement -> fetchAll();

		

		if(FALSE === $get_kt_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_kt_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_kt_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get CRM delay reason list

INPUT 	: Booking ID, Delay Reason ID, Is Delay still active

OUTPUT 	: List of delay reasons

BY 		: Nitin Kashyap

*/

function db_get_crm_delay_reason_list($booking_id,$reason_id,$active='')

{

	$get_reason_list_squery_base = "select * from crm_delay_reasons CDR inner join crm_delay_reason_master CDRM on CDRM.crm_delay_reason_master_id = CDR.crm_delay_reason inner join users U on U.user_id = CDR.crm_delay_reason_added_by";

	

	$get_reason_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_reason_list_sdata = array();

	

	if($booking_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_reason_list_squery_where = $get_reason_list_squery_where." where CDR.crm_delay_reason_booking = :booking";								

		}

		else

		{

			// Query

			$get_reason_list_squery_where = $get_reason_list_squery_where." and CDR.crm_delay_reason_booking = :booking";				

		}

		

		// Data

		$get_reason_list_sdata[':booking']  = $booking_id;

		

		$filter_count++;

	}

	

	if($reason_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_reason_list_squery_where = $get_reason_list_squery_where." where CDR.crm_delay_reason = :reason";								

		}

		else

		{

			// Query

			$get_reason_list_squery_where = $get_reason_list_squery_where." and CDR.crm_delay_reason = :reason";				

		}

		

		// Data

		$get_reason_list_sdata[':reason']  = $reason_id;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_reason_list_squery_where = $get_reason_list_squery_where." where CDR.crm_delay_reason_active = :active";								

		}

		else

		{

			// Query

			$get_reason_list_squery_where = $get_reason_list_squery_where." and CDR.crm_delay_reason_active = :active";				

		}

		

		// Data

		$get_reason_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	$get_reason_list_squery_order = " order by crm_delay_reason_added_on desc";

	

	$get_reason_list_squery = $get_reason_list_squery_base.$get_reason_list_squery_where.$get_reason_list_squery_order;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_reason_list_sstatement = $dbConnection->prepare($get_reason_list_squery);

		

		$get_reason_list_sstatement -> execute($get_reason_list_sdata);

		

		$get_reason_list_sdetails = $get_reason_list_sstatement -> fetchAll();

		

		if(FALSE === $get_reason_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_reason_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_reason_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To add CRM delay reason

INPUT 	: Booking ID, Reason, Remarks, Added By

OUTPUT 	: Delay Reason id, success or failure message

BY 		: Nitin Kashyap

*/

function db_add_crm_delay_reason($booking_id,$reason_id,$remarks,$added_by)

{

	// Query

    $delay_reason_iquery = "insert into crm_delay_reasons (crm_delay_reason_booking,crm_delay_reason,crm_delay_reason_active,crm_delay_reason_remarks,crm_delay_reason_added_by,crm_delay_reason_added_on) values (:booking,:reason,:active,:remarks,:added_by,:now)";

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $delay_reason_istatement = $dbConnection->prepare($delay_reason_iquery);

        

        // Data		

        $delay_reason_idata = array(':booking'=>$booking_id,':reason'=>$reason_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));		

		$dbConnection->beginTransaction();

        $delay_reason_istatement->execute($delay_reason_idata);

		$delay_reason_id = $dbConnection->lastInsertId();

		$dbConnection->commit();

		

		$return["status"] = SUCCESS;

		$return["data"]   = $delay_reason_id;		

	}

    catch(PDOException $e)

    {

		// Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

	}

    return $return;

}



/*

PURPOSE : To get booking details based on delay reason

INPUT 	: Project, Site Status, Reason, Added By

OUTPUT 	: List of bookings with delay reasons

BY 		: Nitin Kashyap

*/

function db_get_delay_reason_latest($project,$status,$reason,$added_by)

{

	$get_delay_reason_latest_squery_base = "select PT.*,CB.*,CAG.*,CSM.*,CPM.*,CPP.*,U.*,CESM.*,CRSM.*,CE.*,SU.user_name as stm from crm_delay_reasons PT left outer join crm_delay_reasons TT on (PT.crm_delay_reason_booking = TT.crm_delay_reason_booking and PT.crm_delay_reason_added_on < TT.crm_delay_reason_added_on) inner join crm_booking CB on CB.crm_booking_id = PT.crm_delay_reason_booking inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id left outer join crm_agreements CAG on CAG.crm_agreement_booking_id = CB.crm_booking_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status left outer join users SU on SU.user_id = CE.assigned_to where TT.crm_delay_reason_booking is NULL and CB.crm_booking_status = '1' and PT.crm_delay_reason_active = '1'";

	

	$get_delay_reason_latest_squery_where = "";

	

	$get_delay_reason_latest_squery_order = "";	

	

	$filter_count = 1;

	

	// Data

	$get_delay_reason_latest_sdata = array();

	

	if($project != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." where CSM.crm_project_id=:project_id";								

		}

		else

		{

			// Query

			$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_delay_reason_latest_sdata[':project_id']  = $project;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($status == '2')

		{

			if($filter_count == 0)

			{

				// Query

				$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." where CAG.crm_agreement_date != '' and CAG.crm_agreement_date is not NULL";								

			}

			else

			{

				// Query

				$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." and CAG.crm_agreement_date != '' and CAG.crm_agreement_date is not NULL";				

			}

		}

		else if($status == '1')

		{

			if($filter_count == 0)

			{

				// Query

				$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." where CAG.crm_agreement_date = '' or CAG.crm_agreement_date is NULL";								

			}

			else

			{

				// Query

				$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." and CAG.crm_agreement_date = '' or CAG.crm_agreement_date is NULL";				

			}



		}

	}

	

	if($reason != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." where PT.crm_delay_reason=:reason";								

		}

		else

		{

			// Query

			$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." and PT.crm_delay_reason=:reason";				

		}

		

		// Data

		$get_delay_reason_latest_sdata[':reason']  = $reason;

		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." where PT.crm_delay_reason_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_delay_reason_latest_squery_where = $get_delay_reason_latest_squery_where." and PT.crm_delay_reason_added_by=:added_by";				

		}

		

		// Data

		$get_delay_reason_latest_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	$get_delay_reason_latest_squery_order = ' order by cast(CSM.crm_site_no as decimal) asc,CSM.crm_site_no asc';

	$get_delay_reason_latest_squery = $get_delay_reason_latest_squery_base.$get_delay_reason_latest_squery_where.$get_delay_reason_latest_squery_order;		



	try

	{

		$dbConnection = get_conn_handle();

		

		$get_delay_reason_latest_sstatement = $dbConnection->prepare($get_delay_reason_latest_squery);

		

		$get_delay_reason_latest_sstatement -> execute($get_delay_reason_latest_sdata);

		

		$get_delay_reason_latest_sdetails = $get_delay_reason_latest_sstatement -> fetchAll();

		

		if(FALSE === $get_delay_reason_latest_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_delay_reason_latest_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_delay_reason_latest_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get pending payment collection sites

INPUT 	: Project, Site Status, Added By

OUTPUT 	: List of pending payment collections

BY 		: Nitin Kashyap

*/

function db_get_pending_collection($project,$status,$added_by)

{

	$get_pending_collection_squery_base = "select CB.*,CSM.*,CE.*,CPM.*,CRSM.*,U.*,CESM.* from crm_booking CB inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id left outer join crm_agreements CAG on CAG.crm_agreement_booking_id = CB.crm_booking_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source where CB.crm_booking_id not in (select crm_delay_reason_booking from crm_delay_reasons where crm_delay_reason_active = '1') and CB.crm_booking_id not in (select crm_registration_booking_id from crm_registration) and CB.crm_booking_status = 1";

	

	$get_pending_collection_squery_where = "";		

	

	$filter_count = 1;

	

	// Data

	$get_pending_collection_sdata = array();

	

	if($project != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pending_collection_squery_where = $get_pending_collection_squery_where." where CSM.crm_project_id=:project_id";								

		}

		else

		{

			// Query

			$get_pending_collection_squery_where = $get_pending_collection_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_pending_collection_sdata[':project_id']  = $project;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($status == '2')

		{

			if($filter_count == 0)

			{

				// Query

				$get_pending_collection_squery_where = $get_pending_collection_squery_where." where CAG.crm_agreement_date != '' and CAG.crm_agreement_date is not NULL";								

			}

			else

			{

				// Query

				$get_pending_collection_squery_where = $get_pending_collection_squery_where." and CAG.crm_agreement_date != '' and CAG.crm_agreement_date is not NULL";				

			}

		}

		else if($status == '1')

		{

			if($filter_count == 0)

			{

				// Query

				$get_pending_collection_squery_where = $get_pending_collection_squery_where." where CB.crm_booking_id not in (select crm_agreement_booking_id from crm_agreements)";								

			}

			else

			{

				// Query

				$get_pending_collection_squery_where = $get_pending_collection_squery_where." and CB.crm_booking_id not in (select crm_agreement_booking_id from crm_agreements)";				

			}



		}

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_pending_collection_squery_where = $get_pending_collection_squery_where." where CB.crm_booking_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_pending_collection_squery_where = $get_pending_collection_squery_where." and CB.crm_booking_added_by=:added_by";				

		}

		

		// Data

		$get_pending_collection_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	$get_pending_collection_squery_order = ' order by cast(CSM.crm_site_no as decimal) asc,CSM.crm_site_no asc';

	$get_pending_collection_squery = $get_pending_collection_squery_base.$get_pending_collection_squery_where.$get_pending_collection_squery_order;		



	try

	{

		$dbConnection = get_conn_handle();

		

		$get_pending_collection_sstatement = $dbConnection->prepare($get_pending_collection_squery);

		

		$get_pending_collection_sstatement -> execute($get_pending_collection_sdata);

		

		$get_pending_collection_sdetails = $get_pending_collection_sstatement -> fetchAll();

		

		if(FALSE === $get_pending_collection_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_pending_collection_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_pending_collection_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To get site bookings with no dates

INPUT 	: Project ID, Status, Added By, Booking Date Filter Start, Booking Date Filter End, Site No

OUTPUT 	: List of sites with no dates

BY 		: Nitin Kashyap

*/

function db_get_site_no_date_data($project_id,$status,$added_by,$booking_date_start,$booking_date_end,$site_no)

{

	$get_booked_list_squery_base = "select *,SU.user_name as stm from crm_booking CB inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status left outer join users SU on SU.user_id = CE.assigned_to where CB.crm_booking_status = '1'";		

	$get_booked_list_squery_where = "";

	

	$get_booked_list_squery_order = "";		

	

	$filter_count = 1;

	

	// Data

	$get_booked_list_sdata = array();

	

	if($project_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CSM.crm_project_id=:project_id";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CSM.crm_project_id=:project_id";				

		}

		

		// Data

		$get_booked_list_sdata[':project_id']  = $project_id;

		

		$filter_count++;

	}

	

	if($status != "-1")

	{

		switch($status)

		{

			case '1': // Booking Date not present

			if($filter_count == 0)

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date = '0000-00-00'";								

			}

			else

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date = '0000-00-00'";				

			}

			break;

			

			case '2': // Agreement Date not present

			if($filter_count == 0)

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_id not in (select crm_agreement_booking_id from crm_agreements)";								

			}

			else

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_id not in (select crm_agreement_booking_id from crm_agreements)";				

			}

			break;

			

			case '3': // Registration Date not present

			if($filter_count == 0)

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_id not in (select crm_registration_booking_id from crm_registration)";								

			}

			else

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_id not in (select crm_registration_booking_id from crm_registration)";				

			}

			break;

			

			case '4': // Katha Transfer Date not present

			if($filter_count == 0)

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_id not in (select crm_katha_transfer_booking_id from crm_katha_transfer)";								

			}

			else

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_id not in (select crm_katha_transfer_booking_id from crm_katha_transfer)";				

			}

			break;

			

			default: // Default: Booking Date not present

			if($filter_count == 0)

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date = '0000-00-00'";								

			}

			else

			{

				// Query

				$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date = '0000-00-00'";				

			}

			break;

		}	

	}

	

	if($added_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_added_by=:added_by";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_added_by=:added_by";				

		}

		

		// Data

		$get_booked_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($booking_date_start != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date >= :bk_start_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date >= :bk_start_date";				

		}

		

		//Data

		$get_booked_list_sdata[':bk_start_date']  = $booking_date_start." 00:00:00";

		

		$filter_count++;

	}

	

	if($booking_date_end != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date <= :bk_end_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date <= :bk_end_date";				

		}

		

		//Data

		$get_booked_list_sdata[':bk_end_date']  = $booking_date_end." 23:59:59";

		

		$filter_count++;

	}

	

	if($site_no != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CSM.crm_site_no = :site_no";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CSM.crm_site_no = :site_no";				

		}

		

		//Data

		$get_booked_list_sdata[':site_no']  = $site_no;

		

		$filter_count++;

	}

	

	$get_booked_list_squery_order = " order by crm_booking_date asc";

	

	$get_booked_list_squery = $get_booked_list_squery_base.$get_booked_list_squery_where.$get_booked_list_squery_order;			

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_booked_list_sstatement = $dbConnection->prepare($get_booked_list_squery);

		

		$get_booked_list_sstatement -> execute($get_booked_list_sdata);

		

		$get_booked_list_sdetails = $get_booked_list_sstatement -> fetchAll();

		

		if(FALSE === $get_booked_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_booked_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_booked_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To untag a booking from delays

INPUT 	: Booking ID

OUTPUT 	: Booking id, success or failure message

BY 		: Nitin Kashyap

*/

function db_untag_delay_reason($booking_id)

{

	// Query

    $untag_uquery = "update crm_delay_reasons set crm_delay_reason_active = :status where crm_delay_reason_booking = :booking_id";  



    try

    {

        $dbConnection = get_conn_handle();

        

        $untag_ustatement = $dbConnection->prepare($untag_uquery);

        

        // Data

        $untag_udata = array(':status'=>'0',':booking_id'=>$booking_id);		

        

        $untag_ustatement -> execute($untag_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $booking_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To get booking summary details

INPUT 	: Booking Filter

OUTPUT 	: No of bookings, Total Area

BY 		: Nitin Kashyap

*/

function db_get_site_booking_summary_data($booking_filter)

{

	$get_booked_list_squery_base = "select count(*) as booking_qty,sum(CSM.crm_site_area) as booking_area from crm_booking CB inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master PM on PM.project_id = CSM.crm_project_id inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source inner join crm_release_status_master CRSM on CRSM.release_status_id = CSM.crm_site_release_status";		

	

	$get_booked_list_squery_where = "";

	

	$get_booked_list_squery_order = "";		

	

	$filter_count = 0;

	

	// Data

	$get_booked_list_sdata = array();

	

	if(array_key_exists('booking_start_date',$booking_filter))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date >= :start_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date >= :start_date";				

		}

		

		//Data

		$get_booked_list_sdata[':start_date']  = $booking_filter['booking_start_date'];

		

		$filter_count++;

	}

	

	if(array_key_exists('booking_end_date',$booking_filter))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_date <= :end_date";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_date <= :end_date";				

		}

		

		//Data

		$get_booked_list_sdata[':end_date']  = $booking_filter['booking_end_date'];

		

		$filter_count++;

	}

	

	if(array_key_exists('added_by',$booking_filter))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where CE.assigned_to = :added_by";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and CE.assigned_to = :added_by";				

		}

		

		//Data

		$get_booked_list_sdata[':added_by']  = $booking_filter['added_by'];

		

		$filter_count++;

	}
	
	if(array_key_exists('status',$booking_filter))

	{

		if($filter_count == 0)

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." where crm_booking_status = :status";								

		}

		else

		{

			// Query

			$get_booked_list_squery_where = $get_booked_list_squery_where." and crm_booking_status = :status";				

		}

		

		//Data

		$get_booked_list_sdata[':status']  = $booking_filter['status'];

		

		$filter_count++;

	}

	

	$get_booked_list_squery = $get_booked_list_squery_base.$get_booked_list_squery_where;				

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_booked_list_sstatement = $dbConnection->prepare($get_booked_list_squery);

		

		$get_booked_list_sstatement -> execute($get_booked_list_sdata);

		

		$get_booked_list_sdetails = $get_booked_list_sstatement -> fetchAll();

		

		if(FALSE === $get_booked_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_booked_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_booked_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;
}

/*
PURPOSE : To get approved booking list for which customer profile is not yet added
INPUT 	: Booking Filter
OUTPUT 	: List of approved bookings with no customer profile
BY 		: Nitin Kashyap
*/
function db_get_approved_booking_no_cust_profile($booking_filter)
{
	$get_no_profile_list_squery_base = "select * from crm_booking CB inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_dimension_master CDM on CDM.crm_dimension_id = CSM.crm_site_dimension inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source";		
	
	$get_no_profile_list_squery_where = "";
	
	$get_no_profile_list_squery_order = " order by crm_booking_approved_on";		
	
	$filter_count = 0;
	
	// Data
	$get_no_profile_list_sdata = array();
	
	if($filter_count == 0)
	{
		// Query
		$get_no_profile_list_squery_where = $get_no_profile_list_squery_where." where crm_booking_id NOT IN (select crm_customer_details_booking_id from crm_customer_details)";								
	}
	else
	{
		// Query
		$get_no_profile_list_squery_where = $get_no_profile_list_squery_where." and crm_booking_id NOT IN (select crm_customer_details_booking_id from crm_customer_details)";				
	}
	$filter_count++;
	
	if(array_key_exists('status',$booking_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$get_no_profile_list_squery_where = $get_no_profile_list_squery_where." where crm_booking_status = :status";								
		}
		else
		{
			// Query
			$get_no_profile_list_squery_where = $get_no_profile_list_squery_where." and crm_booking_status = :status";				
		}
		
		//Data
		$get_no_profile_list_sdata[':status']  = $booking_filter['status'];
		
		$filter_count++;
	}
	
	$get_no_profile_list_squery = $get_no_profile_list_squery_base.$get_no_profile_list_squery_where.$get_no_profile_list_squery_order;				
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_no_profile_list_sstatement = $dbConnection->prepare($get_no_profile_list_squery);
		
		$get_no_profile_list_sstatement -> execute($get_no_profile_list_sdata);
		
		$get_no_profile_list_sdetails = $get_no_profile_list_sstatement -> fetchAll();
		
		if(FALSE === $get_no_profile_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_no_profile_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_no_profile_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"]	  = "";
	}
	
	return $return;
}

/*
PURPOSE : To get approved booking list for which there is no agreement
INPUT 	: Booking Filter
OUTPUT 	: List of approved bookings with no agreement
BY 		: Nitin Kashyap
*/
function db_get_approved_booking_no_agreement($booking_filter)
{
	$get_no_agreement_list_squery_base = "select * from crm_booking CB inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_dimension_master CDM on CDM.crm_dimension_id = CSM.crm_site_dimension inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source";		
	
	$get_no_agreement_list_squery_where = "";
	
	$get_no_agreement_list_squery_order = " order by crm_booking_date,crm_booking_approved_on";		
	
	$filter_count = 0;
	
	// Data
	$get_no_agreement_list_sdata = array();
	
	// No agreement check
	if($filter_count == 0)
	{
		// Query
		$get_no_agreement_list_squery_where = $get_no_agreement_list_squery_where." where crm_booking_id NOT IN (select crm_agreement_booking_id from crm_agreements)";								
	}
	else
	{
		// Query
		$get_no_agreement_list_squery_where = $get_no_agreement_list_squery_where." and crm_booking_id NOT IN (select crm_agreement_booking_id from crm_agreements)";				
	}
	$filter_count++;
	
	// Duration expiry check
	if(array_key_exists('no_of_days',$booking_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$get_no_agreement_list_squery_where = $get_no_agreement_list_squery_where." where crm_booking_date < :deadline";
		}
		else
		{
			// Query
			$get_no_agreement_list_squery_where = $get_no_agreement_list_squery_where." and crm_booking_date < :deadline";				
		}
		
		//Data
		$get_no_agreement_list_sdata[':deadline']  = date("Y-m-d",strtotime(date('Y-m-d').' -'.$booking_filter['no_of_days'].' days'));
		
		$filter_count++;
	}
	
	if(array_key_exists('status',$booking_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$get_no_agreement_list_squery_where = $get_no_agreement_list_squery_where." where crm_booking_status = :status";
		}
		else
		{
			// Query
			$get_no_agreement_list_squery_where = $get_no_agreement_list_squery_where." and crm_booking_status = :status";				
		}
		
		//Data
		$get_no_agreement_list_sdata[':status']  = $booking_filter['status'];
		
		$filter_count++;
	}
	
	$get_no_agreement_list_squery = $get_no_agreement_list_squery_base.$get_no_agreement_list_squery_where.$get_no_agreement_list_squery_order;				
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_no_agreement_list_sstatement = $dbConnection->prepare($get_no_agreement_list_squery);
		
		$get_no_agreement_list_sstatement -> execute($get_no_agreement_list_sdata);
		
		$get_no_agreement_list_sdetails = $get_no_agreement_list_sstatement -> fetchAll();
		
		if(FALSE === $get_no_agreement_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_no_agreement_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_no_agreement_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"]	  = "";
	}
	
	return $return;
}

/*
PURPOSE : To get approved booking list for which there is an agreement but not registration
INPUT 	: Booking Filter
OUTPUT 	: List of approved bookings with agreement present but registration not present
BY 		: Nitin Kashyap
*/
function db_get_agreement_no_registration($booking_filter)
{
	$get_no_registration_list_squery_base = "select * from crm_agreements CA inner join crm_booking CB on CB.crm_booking_id = CA.crm_agreement_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_dimension_master CDM on CDM.crm_dimension_id = CSM.crm_site_dimension inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source";		
	
	$get_no_registration_list_squery_where = "";
	
	$get_no_registration_list_squery_order = " order by CA.crm_agreement_date";		
	
	$filter_count = 0;
	
	// Data
	$get_no_registration_list_sdata = array();
	
	// No registration check
	if($filter_count == 0)
	{
		// Query
		$get_no_registration_list_squery_where = $get_no_registration_list_squery_where." where CB.crm_booking_id NOT IN (select crm_registration_booking_id from crm_registration)";								
	}
	else
	{
		// Query
		$get_no_registration_list_squery_where = $get_no_registration_list_squery_where." and CB.crm_booking_id NOT IN (select crm_registration_booking_id from crm_registration)";				
	}
	$filter_count++;
	
	// Duration expiry check
	if(array_key_exists('no_of_days',$booking_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$get_no_registration_list_squery_where = $get_no_registration_list_squery_where." where CA.crm_agreement_date < :deadline";
		}
		else
		{
			// Query
			$get_no_registration_list_squery_where = $get_no_registration_list_squery_where." and CA.crm_agreement_date < :deadline";				
		}
		
		//Data
		$get_no_registration_list_sdata[':deadline']  = date("Y-m-d",strtotime(date('Y-m-d').' -'.$booking_filter['no_of_days'].' days'));
		
		$filter_count++;
	}
	
	if(array_key_exists('status',$booking_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$get_no_registration_list_squery_where = $get_no_registration_list_squery_where." where CB.crm_booking_status = :status";
		}
		else
		{
			// Query
			$get_no_registration_list_squery_where = $get_no_registration_list_squery_where." and CB.crm_booking_status = :status";				
		}
		
		//Data
		$get_no_registration_list_sdata[':status']  = $booking_filter['status'];
		
		$filter_count++;
	}
	
	$get_no_registration_list_squery = $get_no_registration_list_squery_base.$get_no_registration_list_squery_where.$get_no_registration_list_squery_order;				
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_no_registration_list_sstatement = $dbConnection->prepare($get_no_registration_list_squery);
		
		$get_no_registration_list_sstatement -> execute($get_no_registration_list_sdata);
		
		$get_no_registration_list_sdetails = $get_no_registration_list_sstatement -> fetchAll();
		
		if(FALSE === $get_no_registration_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_no_registration_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_no_registration_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"]	  = "";
	}
	
	return $return;
}

/*
PURPOSE : To get approved booking list for which there is a registration but not katha transfer
INPUT 	: Booking Filter
OUTPUT 	: List of approved bookings with registration present but registration not katha transfer
BY 		: Nitin Kashyap
*/
function db_get_registration_no_kt($booking_filter)
{
	$get_no_kt_list_squery_base = "select * from crm_registration CR inner join crm_booking CB on CB.crm_booking_id = CR.crm_registration_booking_id inner join crm_site_master CSM on CSM.crm_site_id = CB.crm_booking_site_id inner join crm_project_master CPM on CPM.project_id = CSM.crm_project_id inner join crm_prospective_profile CPP on CPP.crm_prospective_profile_id = CB.crm_booking_client_profile inner join crm_enquiry CE on CE.enquiry_id = CPP.crm_prospective_profile_enquiry inner join crm_dimension_master CDM on CDM.crm_dimension_id = CSM.crm_site_dimension inner join users U on U.user_id = CB.crm_booking_added_by inner join crm_enquiry_source_master CESM on CESM.enquiry_source_master_id = CE.source";		
	
	$get_no_kt_list_squery_where = "";
	
	$get_no_kt_list_squery_order = " order by CR.crm_registration_date";		
	
	$filter_count = 0;
	
	// Data
	$get_no_kt_list_sdata = array();
	
	// No registration check
	if($filter_count == 0)
	{
		// Query
		$get_no_kt_list_squery_where = $get_no_kt_list_squery_where." where CB.crm_booking_id NOT IN (select crm_katha_transfer_booking_id from crm_katha_transfer)";								
	}
	else
	{
		// Query
		$get_no_kt_list_squery_where = $get_no_kt_list_squery_where." and CB.crm_booking_id NOT IN (select crm_katha_transfer_booking_id from crm_katha_transfer)";				
	}
	$filter_count++;
	
	// Duration expiry check
	if(array_key_exists('no_of_days',$booking_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$get_no_kt_list_squery_where = $get_no_kt_list_squery_where." where CR.crm_registration_date < :deadline";
		}
		else
		{
			// Query
			$get_no_kt_list_squery_where = $get_no_kt_list_squery_where." and CR.crm_registration_date < :deadline";				
		}
		
		//Data
		$get_no_kt_list_sdata[':deadline']  = date("Y-m-d",strtotime(date('Y-m-d').' -'.$booking_filter['no_of_days'].' days'));
		
		$filter_count++;
	}
	
	if(array_key_exists('status',$booking_filter))
	{
		if($filter_count == 0)
		{
			// Query
			$get_no_kt_list_squery_where = $get_no_kt_list_squery_where." where CB.crm_booking_status = :status";
		}
		else
		{
			// Query
			$get_no_kt_list_squery_where = $get_no_kt_list_squery_where." and CB.crm_booking_status = :status";				
		}
		
		//Data
		$get_no_kt_list_sdata[':status']  = $booking_filter['status'];
		
		$filter_count++;
	}
	
	$get_no_kt_list_squery = $get_no_kt_list_squery_base.$get_no_kt_list_squery_where.$get_no_kt_list_squery_order;				
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_no_kt_list_sstatement = $dbConnection->prepare($get_no_kt_list_squery);
		
		$get_no_kt_list_sstatement -> execute($get_no_kt_list_sdata);
		
		$get_no_kt_list_sdetails = $get_no_kt_list_sstatement -> fetchAll();
		
		if(FALSE === $get_no_kt_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_no_kt_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_no_kt_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"]	  = "";
	}
	
	return $return;
}
?>