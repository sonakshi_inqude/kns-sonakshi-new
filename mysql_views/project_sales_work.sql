-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 27, 2018 at 03:35 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `project_sales_work`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_sales_work`  AS  select `PT`.`enquiry_follow_up_id` AS `enquiry_follow_up_id`,`PT`.`enquiry_id_for_follow_up` AS `enquiry_id_for_follow_up`,`PT`.`enquiry_follow_up_remarks` AS `enquiry_follow_up_remarks`,`PT`.`enquiry_follow_up_int_status` AS `enquiry_follow_up_int_status`,`PT`.`enquiry_follow_up_date` AS `enquiry_follow_up_date`,`PT`.`enquiry_follow_up_added_by` AS `enquiry_follow_up_added_by`,`PT`.`enquiry_follow_up_added_on` AS `enquiry_follow_up_added_on`,`CIS`.`crm_cust_interest_status_id` AS `crm_cust_interest_status_id`,`CIS`.`crm_cust_interest_status_name` AS `crm_cust_interest_status_name`,`CIS`.`crm_cust_interest_status_active` AS `crm_cust_interest_status_active`,`CIS`.`crm_customer_interest_status_order` AS `crm_customer_interest_status_order`,`CIS`.`crm_cust_interest_status_added_by` AS `crm_cust_interest_status_added_by`,`CIS`.`crm_cust_interest_status_added_on` AS `crm_cust_interest_status_added_on`,`CE`.`enquiry_id` AS `enquiry_id`,`CE`.`enquiry_number` AS `enquiry_number`,`CE`.`project_id` AS `project_id`,`CE`.`site_id` AS `site_id`,`CE`.`name` AS `name`,`CE`.`cell` AS `cell`,`CE`.`email` AS `email`,`CE`.`company` AS `company`,`CE`.`location` AS `location`,`CE`.`source` AS `source`,`CE`.`remarks` AS `remarks`,`CE`.`interest_status` AS `interest_status`,`CE`.`walk_in` AS `walk_in`,`CE`.`follow_up_date` AS `follow_up_date`,`CE`.`assigned_to` AS `assigned_to`,`CE`.`assigned_on` AS `assigned_on`,`CE`.`added_by` AS `added_by`,`CE`.`added_on` AS `added_on`,`CPM`.`project_name` AS `project_name`,`U`.`user_name` AS `user_name`,`U`.`user_id` AS `user_id`,`ESM`.`enquiry_source_master_name` AS `enquiry_source_master_name`,`ESM`.`enquiry_source_master_id` AS `enquiry_source_master_id` from (((((((`crm_enquiry_follow_up` `PT` left join `crm_enquiry_follow_up` `TT` on(((`PT`.`enquiry_id_for_follow_up` = `TT`.`enquiry_id_for_follow_up`) and (`PT`.`enquiry_follow_up_date` < `TT`.`enquiry_follow_up_date`)))) join `crm_enquiry` `CE` on((`CE`.`enquiry_id` = `PT`.`enquiry_id_for_follow_up`))) join `crm_customer_interest_status` `CIS` on((`CIS`.`crm_cust_interest_status_id` = `PT`.`enquiry_follow_up_int_status`))) left join `crm_project_master` `CPM` on((`CPM`.`project_id` = `CE`.`project_id`))) join `crm_enquiry_source_master` `ESM` on((`ESM`.`enquiry_source_master_id` = `CE`.`source`))) join `crm_customer_interest_status` `CISS` on((`CISS`.`crm_cust_interest_status_id` = `CE`.`interest_status`))) join `users` `U` on((`U`.`user_id` = `CE`.`assigned_to`))) where isnull(`TT`.`enquiry_id_for_follow_up`) ;

--
-- VIEW  `project_sales_work`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
