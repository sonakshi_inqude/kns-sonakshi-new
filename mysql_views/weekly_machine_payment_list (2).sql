-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2018 at 08:19 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `weekly_machine_payment_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `weekly_machine_payment_list`  AS  select `PM`.`project_payment_machine_id` AS `project_payment_machine_id`,`PM`.`project_payment_machine_vendor_id` AS `project_payment_machine_vendor_id`,`PM`.`project_payment_machine_amount` AS `project_payment_machine_amount`,`PM`.`project_payment_machine_tds` AS `project_payment_machine_tds`,`PM`.`project_payment_machine_status` AS `project_payment_machine_status`,`PM`.`project_payment_machine_bill_no` AS `project_payment_machine_bill_no`,`PM`.`project_payment_machine_billing_address` AS `project_payment_machine_billing_address`,`PM`.`project_payment_machine_from_date` AS `project_payment_machine_from_date`,`PM`.`project_payment_machine_to_date` AS `project_payment_machine_to_date`,`PM`.`project_payment_machine_active` AS `project_payment_machine_active`,`PM`.`project_payment_machine_remarks` AS `project_payment_machine_remarks`,`PM`.`project_payment_machine_accepted_by` AS `project_payment_machine_accepted_by`,`PM`.`project_payment_machine_accepted_on` AS `project_payment_machine_accepted_on`,`PM`.`project_payment_machine_approved_by` AS `project_payment_machine_approved_by`,`PM`.`project_payment_machine_approved_on` AS `project_payment_machine_approved_on`,`PM`.`project_payment_machine_added_by` AS `project_payment_machine_added_by`,`PM`.`project_payment_machine_added_on` AS `project_payment_machine_added_on`,`PM`.`project_bata_payment_machine_status` AS `project_bata_payment_machine_status`,`PM`.`project_bata_payment_accepted_by` AS `project_bata_payment_accepted_by`,`PM`.`project_bata_payment_accepted_on` AS `project_bata_payment_accepted_on`,`PM`.`project_payment_machine_bata` AS `project_payment_machine_bata`,`PM`.`project_payment_machine_bata_bill_no` AS `project_payment_machine_bata_bill_no`,`PMVM`.`project_machine_vendor_master_id` AS `project_machine_vendor_master_id`,`PMVM`.`project_machine_vendor_master_name` AS `project_machine_vendor_master_name`,`PMVM`.`project_machine_vendor_master_active` AS `project_machine_vendor_master_active`,`PMVM`.`project_machine_vendor_master_remarks` AS `project_machine_vendor_master_remarks`,`PMVM`.`project_machine_vendor_master_added_by` AS `project_machine_vendor_master_added_by`,`PMVM`.`project_machine_vendor_master_added_on` AS `project_machine_vendor_master_added_on`,`SCM`.`stock_company_master_id` AS `stock_company_master_id`,`SCM`.`stock_company_master_name` AS `stock_company_master_name`,`SCM`.`stock_company_master_contact_person` AS `stock_company_master_contact_person`,`SCM`.`stock_company_master_address` AS `stock_company_master_address`,`SCM`.`stock_company_master_tin_no` AS `stock_company_master_tin_no`,`SCM`.`stock_company_master_active` AS `stock_company_master_active`,`SCM`.`stock_company_master_remarks` AS `stock_company_master_remarks`,`SCM`.`stock_company_master_added_by` AS `stock_company_master_added_by`,`SCM`.`stock_company_master_added_on` AS `stock_company_master_added_on`,`U`.`user_id` AS `user_id`,`AU`.`user_id` AS `accepted_by_user`,`AU`.`user_name` AS `accepted_by`,`BU`.`user_name` AS `bata_accepted_by`,`U`.`user_name` AS `added_by` from (((((`project_payment_machine` `PM` join `users` `U` on((`U`.`user_id` = `PM`.`project_payment_machine_added_by`))) join `project_machine_vendor_master` `PMVM` on((`PMVM`.`project_machine_vendor_master_id` = `PM`.`project_payment_machine_vendor_id`))) left join `stock_company_master` `SCM` on((`SCM`.`stock_company_master_id` = `PM`.`project_payment_machine_billing_address`))) left join `users` `BU` on((`BU`.`user_id` = `PM`.`project_bata_payment_accepted_by`))) left join `users` `AU` on((`AU`.`user_id` = `PM`.`project_payment_machine_accepted_by`))) ;

--
-- VIEW  `weekly_machine_payment_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
