<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 29th Sep 2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	

	if(isset($_REQUEST['grn_insp_id']))

	{

		$grn_insp_id = $_REQUEST['grn_insp_id'];

	}

	else

	{

		// Do nothing

		$grn_insp_id = '-1';

	}

	

	// Capture the form data

	if(isset($_POST["move_to_payment_submit"]))

	{		

		$grn_insp_id   = $_POST["hd_grn_insp_id"];

		$amount_one    = $_POST["num_additional_amount_one"];		

		$remarks_one   = $_POST["txt_remarks_one"];

		$amount_two    = $_POST["num_additional_amount_two"];		

		$remarks_two   = $_POST["txt_remarks_two"];

		$amount_three  = $_POST["num_additional_amount_three"];		

		$remarks_three = $_POST["txt_remarks_three"];

		$amount_four   = $_POST["num_additional_amount_four"];		

		$remarks_four  = $_POST["txt_remarks_four"];

		$amount_five   = $_POST["num_additional_amount_five"];		

		$remarks_five  = $_POST["txt_remarks_five"];

				

		// Check for mandatory fields

		if($grn_insp_id != "")

		{

			$status = 1;

			if($amount_one != 0)

			{

				$grn_account_iresult = i_add_stock_grn_accounts($grn_insp_id,$amount_one,$remarks_one,$user);

				if($grn_account_iresult['status'] != SUCCESS)

				{					

					$status = $status & 0;

				}

			}

			

			if($amount_two != 0)

			{

				$grn_account_iresult = i_add_stock_grn_accounts($grn_insp_id,$amount_two,$remarks_two,$user);

				if($grn_account_iresult['status'] != SUCCESS)

				{					

					$status = $status & 0;

				}

			}

			

			if($amount_three != 0)

			{

				$grn_account_iresult = i_add_stock_grn_accounts($grn_insp_id,$amount_three,$remarks_three,$user);

				if($grn_account_iresult['status'] != SUCCESS)

				{										

					$status = $status & 0;

				}

			}

			

			if($amount_four != 0)

			{

				$grn_account_iresult = i_add_stock_grn_accounts($grn_insp_id,$amount_four,$remarks_four,$user);

				if($grn_account_iresult['status'] != SUCCESS)

				{					

					$status = $status & 0;

				}

			}

			

			if($amount_five != 0)

			{

				$grn_account_iresult = i_add_stock_grn_accounts($grn_insp_id,$amount_five,$remarks_five,$user);

				if($grn_account_iresult['status'] != SUCCESS)

				{					

					$status = $status & 0;

				}

			}

			

			if($status == 1)

			{	
				//header('location:stock_move_to_payments_list.php');
			}

			else

			{

				$alert_type = 0;

				$alert      = 'Internal Error. Please try again later';

			}						

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}

	
	// Get Stock Additional payment Master List
	$stock_additional_payment_master_search_data = array();
	$stock_additional_payment_list =  i_get_stock_additional_payment_master($stock_additional_payment_master_search_data);
	if($stock_additional_payment_list['status'] == SUCCESS)
	{
		$stock_additional_payment_list_data = $stock_additional_payment_list['data'];
	}	
	else
	{
		$alert = $stock_additional_payment_list["data"];
		$alert_type = 0;		
	}
	
	// Get Engineer Inspection and other data

	$stock_grn_engineer_inspection_search_data = array('inspection_id'=>$grn_insp_id);

	$grn_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);

	

	if($grn_inspection_list['status'] == SUCCESS)

	{

		$grn_inspection_list_data = $grn_inspection_list['data'];

		

		// Get PO details				

		$stock_purchase_order_items_search_data = array('item'=>$grn_inspection_list_data[0]['stock_grn_item'],'order_id'=>$grn_inspection_list_data[0]['stock_grn_purchase_order_id']);

		$po_sresult = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);

		

		$item_cost    = $po_sresult['data'][0]["stock_purchase_order_item_cost"];

		$accepted_qty = $grn_inspection_list_data[0]["stock_grn_engineer_inspection_approved_quantity"];

				

		$grn_no     = $grn_inspection_list_data[0]['stock_grn_no'];

		$item_name  = $grn_inspection_list_data[0]['stock_material_name'];

		

		$item_value = ($item_cost * $accepted_qty);

		$po_no      = $po_sresult['data'][0]['stock_purchase_order_number'];

		$vendor     = $grn_inspection_list_data[0]['stock_vendor_name'];
		
		$trans_charge = $po_sresult['data'][0]["stock_purchase_order_transportation_charges"];
		
		$excise_value = ($po_sresult['data'][0]["stock_purchase_order_item_excise_duty"] * $item_value)/100;
		
		$tax_rate     = $po_sresult['data'][0]["stock_tax_type_master_value"];

		$tax_value    = ($po_sresult['data'][0]["stock_tax_type_master_value"] * $item_value)/100;
		
		$total_value = $item_value + $tax_value + $trans_charge + $excise_value ;

	}

	else

	{

		$grn_no     = '';

		$item_name  = '';

		$item_value = '';

		$po_no      = '';

		$vendor     = '';

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Add GRN Account</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>

    

<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>    



<div class="main">

	

	<div class="main-inner">



	    <div class="container">

	

	      <div class="row">

	      	

	      	<div class="span12">      		

	      		

	      		<div class="widget ">

	      			

	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>GRN No: <?php echo $grn_no; ?>&nbsp;&nbsp;&nbsp;Item: <?php echo $item_name; ?>&nbsp;&nbsp;&nbsp;Item Value: Rs. <?php echo $total_value; ?>&nbsp;&nbsp;&nbsp;PO No: <?php echo $po_no; ?>&nbsp;&nbsp;&nbsp;Vendor: <?php echo $vendor; ?></h3>

	  				</div> <!-- /widget-header -->

					

					<div class="widget-content">

						

						

						

						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Add Account Executive Remarks</a>

						  </li>	

						</ul>

						<br>

							<div class="control-group">												

								<div class="controls">

								<?php 

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>  

								<?php

								}

								?>

                                

								<?php 

								if($alert_type == 1) // Success

								{

								?>								

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->	                                                

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="move_to_payments_form" class="form-horizontal" method="post" action="stock_move_to_payments.php">

								<input type="hidden" name="hd_grn_insp_id" value="<?php echo $grn_insp_id; ?>" />

									<fieldset>										

															

										<div class="control-group">											

											<label class="control-label" for="num_additional_amount">Additional Amount 1</label>

											<div class="controls">

												<input type="number" step="0.01" class="span4" name="num_additional_amount_one" placeholder="Additional amount to be paid (if any)" value="0">&nbsp;&nbsp;
												<select name="txt_remarks_one" required>
												<option>- - -Select Additional Payment- - -</option>
												<?php
												for($count = 0; $count < count($stock_additional_payment_list_data); $count++)
												{
													?>
												<option value="<?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_id"]; ?>"><?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_payment_type"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_additional_amount_two">Additional Amount 2</label>

											<div class="controls">

												<input type="number" step="0.01" class="span4" name="num_additional_amount_two" placeholder="Additional amount to be paid (if any)" value="0">&nbsp;&nbsp;
												<select name="txt_remarks_two" required>
												<option>- - -Select Additional Payment- - -</option>
												<?php
												for($count = 0; $count < count($stock_additional_payment_list_data); $count++)
												{
													?>
												<option value="<?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_id"]; ?>"><?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_payment_type"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_additional_amount_three">Additional Amount 3</label>

											<div class="controls">

												<input type="number" step="0.01" class="span4" name="num_additional_amount_three" placeholder="Additional amount to be paid (if any)" value="0">&nbsp;&nbsp;
												<select name="txt_remarks_three" required>
												<option>- - -Select Additional Payment- - -</option>
												<?php
												for($count = 0; $count < count($stock_additional_payment_list_data); $count++)
												{
													?>
												<option value="<?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_id"]; ?>"><?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_payment_type"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_additional_amount_four">Additional Amount 4</label>

											<div class="controls">

												<input type="number" step="0.01" class="span4" name="num_additional_amount_four" placeholder="Additional amount to be paid (if any)" value="0">&nbsp;&nbsp;
												<select name="txt_remarks_four" required>
												<option>- - -Select Additional Payment- - -</option>
												<?php
												for($count = 0; $count < count($stock_additional_payment_list_data); $count++)
												{
													?>
												<option value="<?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_id"]; ?>"><?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_payment_type"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

										

										<div class="control-group">											

											<label class="control-label" for="num_additional_amount_five">Additional Amount 5</label>

											<div class="controls">

												<input type="number" step="0.01" class="span4" name="num_additional_amount_five" placeholder="Additional amount to be paid (if any)" value="0">&nbsp;&nbsp;
												<select name="txt_remarks_five" required>
												<option>- - -Select Additional Payment- - -</option>
												<?php
												for($count = 0; $count < count($stock_additional_payment_list_data); $count++)
												{
												?>
												<option value="<?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_id"]; ?>"><?php echo $stock_additional_payment_list_data[$count]["stock_additional_payment_master_payment_type"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					

										</div> <!-- /control-group -->

                                                                                                                                                               										 <br />

										

										<div class="form-actions">

											<input type="submit" class="btn btn-primary" name="move_to_payment_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>

							</div> 

							

					</div> <!-- /widget-content -->

						

				</div> <!-- /widget -->

	      		

		    </div> <!-- /span8 -->

	      	

	      	

	      	

	      	

	      </div> <!-- /row -->

	

	    </div> <!-- /container -->

	    

	</div> <!-- /main-inner -->

    

</div> <!-- /main -->

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>





  </body>



</html>