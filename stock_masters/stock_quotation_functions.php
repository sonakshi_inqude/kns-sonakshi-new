<?php

/**

 * @author Nitin kashyap

 * @copyright 2015

 */



$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_quotation_compare.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');



/*

PURPOSE : To add Stock Quotation Compare

INPUT 	: Indent No,Amount,Quotation No,Quotation Vendor,Quotation Qty,Location,Received Date,Remarks,Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_add_stock_quotation_compare($quote_no,$indent_id,$amount,$quotation_no,$quotation_vendor,$quotation_qty,$project,$received_date,$doc,$remarks,$added_by)
{
	$quotation_compare_iresult = db_add_stock_quotation_compare($quote_no,$indent_id,$amount,$quotation_no,$quotation_vendor,$quotation_qty,$project,$received_date,$doc,$remarks,$added_by);


	if($quotation_compare_iresult['status'] == SUCCESS)

	{

		$return["data"]   = $quotation_compare_iresult['data'];

		$return["status"] = SUCCESS;

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}



	return $return;

}



/*

PURPOSE : To get Quotation Compare list

INPUT 	: Quotation ID, Active

OUTPUT 	: Quotation Compare List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_quotation_compare_list($stock_quotation_compare_search_data)

{

	$quotation_compare_sresult = db_get_stock_quotation_compare_list($stock_quotation_compare_search_data);



	if($quotation_compare_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$quotation_compare_sresult["data"];

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No quotation compare added. Please contact the system admin";

    }



	return $return;

}



/*

PURPOSE : To update To update Quotation Compare List

INPUT 	: Quotation ID, Quotation Compare Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/



function i_update_quotation_compare($quotation_id,$indent_id,$quotation_compare_update_data)

{

	$quotation_compare_sresult = db_update_quotation_compare($quotation_id,$indent_id,$quotation_compare_update_data);



	if($quotation_compare_sresult['status'] == SUCCESS)

	{

		$return["data"]   = "Quotation Compare Successfully added";

		$return["status"] = SUCCESS;

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}



	return $return;

}



/*

PURPOSE : To add Stock Quote Reset

INPUT 	: Material ID, Location, Date Time, PO ID, Remarks, Added by

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_add_stock_quote_reset($material_id,$project,$date_time,$po_id,$remarks,$added_by)
{
	$stock_quote_reset_search_data = array("material_id"=>$material_id,"project"=>$project);

	$material_quote_sresult = db_get_stock_quote_reset($stock_quote_reset_search_data);

	if($material_quote_sresult["status"] == DB_NO_RECORD)

	{
		$quote_reset_iresult = db_add_stock_quote_reset($material_id,$project,$date_time,$po_id,$remarks,$added_by);


		if($quote_reset_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "Quote Reset Successfully Added";

			$return["status"] = SUCCESS;

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}
	}
	else
	{
		$return["data"]   = "Material Already Exists";

		$return["status"] = FAILURE;
	}



	return $return;

}



/*

PURPOSE : To get Stock Quote Reset List

INPUT 	: Reset ID, Material ID, Date Time, PO ID, Active, Added by, Start Date(for added on), End Date(for added on)

OUTPUT 	: Stock Quote Reset List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_stock_quote_reset($stock_quote_reset_search_data)

{

	$quote_reset_sresult = db_get_stock_quote_reset($stock_quote_reset_search_data);



	if($quote_reset_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$quote_reset_sresult["data"];

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No Quote Reset Added. Please contact the system admin";

    }



	return $return;

}



/*

PURPOSE : To update Stock Quote Reset

INPUT 	: Material ID, Location ID, Stock Quote Reset Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/
function i_update_stock_quote_reset($material_id,$project_id,$stock_quote_reset_update_data)
{
	$stock_quote_reset_sresult = db_update_stock_quote_reset($material_id,$project_id,$stock_quote_reset_update_data);


	if($stock_quote_reset_sresult['status'] == SUCCESS)

	{

		$return["data"]   = "Stock Quote Reset Successfully done";

		$return["status"] = SUCCESS;

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}



	return $return;

}

function i_add_quote()
{
	$stock_material_search_data = array("active"=>'1');
	$stock_material_list = i_get_stock_material_master_list($stock_material_search_data);
	for($material_count = 0 ; $material_count < count($stock_material_list["data"]) ; $material_count++)
	{
		$quote_add_reset_iresults = i_add_stock_quote_reset($stock_material_list["data"][$material_count]["stock_material_id"],'33',date("Y-m-d H:i:s"),'','','143620071466608200');
		if($quote_add_reset_iresults["status"] == SUCCESS)
		{
			var_dump("added");
		}
	}
}
/* PRIVATE FUNCTIONS - START */
function p_generate_quotation_no()
{
    // Get the last bill no
    $stock_quotation_compare_search_data = array("sort"=>'1',"empty_check"=>'1',"active"=>'1');
    $quote_no_data = db_get_stock_quotation_compare_list($stock_quotation_compare_search_data);
    if ($quote_no_data["status"] == DB_RECORD_ALREADY_EXISTS) {
        $invoice_numeric_value_data = $quote_no_data["data"][0]["stock_quote_no"];
        $invoice_numeric_value_array = explode(BILL_NO_DELIMITER, $invoice_numeric_value_data);
        $invoice_numeric_value = ($invoice_numeric_value_array[count($invoice_numeric_value_array)-1]) + 1;
    } else {
        $invoice_numeric_value = 1;
    }
    // // KNS/NMR/2018-2019/06/1215
    // if(isset($bill_substr) && !empty($bill_substr)){
    //   return BILL_NO_ROOT.BILL_NO_DELIMITER.$bill_substr.BILL_NO_DELIMITER.BILL_NO_MANPOWER.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$invoice_numeric_value;
    // }
    return BILL_NO_ROOT.BILL_NO_DELIMITER.BILL_NO_QUOATTION.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$invoice_numeric_value;
}
/* PRIVATE FUNCTIONS - END */
?>
