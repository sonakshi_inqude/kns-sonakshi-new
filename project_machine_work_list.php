<?php

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

// What is the date today
$start_date = date("Y-m-d 00:00:00");
$end_date = date("Y-m-d 23:59:59");
// Get list of approved bookings with no profile
$max_wait_days = 30;
// Get Project Task BOQ modes already added

// Project data
$project_management_master_search_data = array("active"=>'1',"status"=>"all");
$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
if($project_management_master_list["status"] == SUCCESS)
{
	$project_management_master_list_data = $project_management_master_list["data"];
}

	for($pcount = 0 ; $pcount < count($project_management_master_list_data) ; $pcount++)
	{
		$sl_no = 0;
		$project_email_search_data = array("project_id"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$email_list = db_get_project_email_list($project_email_search_data);
		if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
				$email_list_data = $email_list["data"];
				$to  = $email_list_data[0]["project_email_details_to"];
				$cc  = $email_list_data[0]["project_email_details_cc"];
				$bcc = $email_list_data[0]["project_email_details_bcc"];
		}
		//Contract Data
		$actual_machine_plan_search_data = array("active"=>'1',"start_date_time"=>$start_date,"end_date_time"=>$end_date,"project"=>$project_management_master_list_data[$pcount]["project_management_master_id"]);
		$actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
		if($actual_machine_plan_list['status'] == SUCCESS)
		{

			$total_cost  = 0 ;
			$message_heading = "" ;
			$message_content = "" ;
			$message_end = "" ;
			$heading = "" ;
			$message = "" ;
			$machine_array = [] ;

			// $message_end = "" ;
			$actual_machine_plan_list_data = $actual_machine_plan_list['data'];
			$subject = $project_management_master_list_data[$pcount]["project_master_name"].'  '.'Machine Work List';
			$message = 'Dear Sir/Madam,<br><br>Daily Machine work list:<br><br>';

			$message_heading .="<table border='1' style='border-collapse:collapse;border-width:2px;'>";
				// Header row - start
			$message_heading .= "<tr style='border-width:2px;text-align:center;'>
			<td style='border-width:2px;text-align:center;'><strong>SL No.</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Agency</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Process</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Task</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Road</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>WorkType</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Machine</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Remarks<strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Added By</strong></td>
			<td style='border-width:2px;text-align:center;'><strong>Added On</strong></td>";

			$message_heading = $message_heading.'</tr>';
			for($count = 0; $count < count($actual_machine_plan_list_data); $count++)
			{
				$sl_no++;
				$total_value = 0;
				array_push($machine_array,$actual_machine_plan_list_data[$count]["project_machine_master_name"]);
				//get stock quantity
				$project_machine_rate_master_search_data = array("machine_id"=>$actual_machine_plan_list_data[$count]["project_task_machine_id"]);
				$project_machine_rate_master_data =  i_get_project_machine_rate_master($project_machine_rate_master_search_data);
				if($project_machine_rate_master_data["status"] == SUCCESS)
				{
					$vendor_machine_number = $project_machine_rate_master_data["data"][0]["project_machine_master_id_number"];
					$machine_type = $project_machine_rate_master_data["data"][0]["project_machine_type"];
				}
				else
				{
					$vendor_machine_number = 'NA';
					$machine_type = 'NA';
				}
				// Compose the message
				$project	     = $actual_machine_plan_list_data[$count]["project_master_name"];
				$process  	   = $actual_machine_plan_list_data[$count]["project_process_master_name"];
				$task	  	     = $actual_machine_plan_list_data[$count]["project_task_master_name"];
				$vendor	 			 = $actual_machine_plan_list_data[$count]["project_machine_vendor_master_name"];
				$work_type	   = $actual_machine_plan_list_data[$count]["project_task_actual_machine_work_type"];
				$machine	    = $actual_machine_plan_list_data[$count]["project_machine_master_name"] .$vendor_machine_number;
				if($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_road_id"] == "No Roads")
				{
					$location = "No Roads";
				}
				else {
					$location	 = $actual_machine_plan_list_data[$count]["project_site_location_mapping_master_name"];
				}
				$remarks 					= $actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_remarks"];
				$user	  	  			= $actual_machine_plan_list_data[$count]["user_name"];
				$added_on	  	 	  = date("d-M-Y",strtotime($actual_machine_plan_list_data[$count]["project_task_actual_machine_plan_added_on"]));
				if($work_type == "Rework")
				{
						$css = "yellow";
				}
				else {
					$css = "";
				}
				$message_content .= "<tr style='border-width:2px; text-align:center;'>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$sl_no."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$vendor."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$process."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$task."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$location."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$work_type."</td>
					<td style='border-width:2px;text-align:center;width:20%;background-color:$css;'>".$machine."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$remarks."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$user."</td>
					<td style='border-width:2px;text-align:center;background-color:$css;'>".$added_on."</td>
				</tr>";
			}
			$count_of_machine = array_count_values($machine_array);
			$heading = $heading."<br>
			<table style='border: 1px solid black;'>
			<tr>
				<th style='border: 1px solid #ddd;width:20%;colspan:2;'>Project : ".$project_management_master_list_data[$pcount]["project_master_name"] ."</th>
				<td style='border: 1px solid #ddd;width:20%'>Date : ".date("d-M-Y")."</td>
			</tr>
			<tr>
				<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Machine</th>
				<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Head Count</th>
			</tr>";

			foreach($count_of_machine as $key => $value) {
				$total_value += $value ;
				$heading = $heading."<tr>
					<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>".$key."</th>
					<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>".$value."</th>
				</tr>";
			}
			$heading = $heading."<tr>
				<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>Total Machine</th>
				<th style='color:#00ba8b;border: 1px solid #ddd;width:20%'>".$total_value."</th>
			</tr>";
			$heading = $heading."</table><br>";
			$message_end = "</table>";
			$message_end = $message_end.'<br>Regards,<br>KNS ERP';

			$message .= $heading ;
			$message .= $message_heading ;
			$message .= $message_content;
			$message .= $message_end;
			$cc = explode(',',$cc);
			$cc_string = '[';
			for($count = 0; $count < count($cc); $count++)
			{
				$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
			}
			$cc_string = trim($cc_string,',');
			$cc_string = $cc_string.']';

			$bcc = explode(',',$bcc);
				$bcc_string = '[';
				for($count = 0; $count < count($bcc); $count++)
				{
					$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
				}
				$bcc_string = trim($bcc_string,',');
				$bcc_string = $bcc_string.']';

			$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
				"personalizations": [
					{
						"to": [
							{
								"email": "'.$to.'"
							}
						],
						"cc": '.$cc_string.',
						"bcc": '.$bcc_string.',
						"subject": "'.$subject.'"
					}
				],
				"from": {
					"email": "venkataramanaiah@knsgroup.in"
				},
				"content": [
					{
						"type": "text/html",
						"value": "'.$message.'"
					}
				]
			}'));
			$apiKey = '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';
			$sg = new \SendGrid($apiKey);
			$response = $sg->client->mail()->send()->post($request_body);
		}
	}
?>
