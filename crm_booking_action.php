<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 8th July 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
/* INCLUDES - END */

// Session Data
$user = $_SESSION["loggedin_user"];
$role = $_SESSION["loggedin_role"];

/* QUERY STRING DATA - START */
$booking_id = $_GET["booking"];
$profile_id = $_GET["client_profile"];
$action     = $_GET["action"];
/* QUERY STRING DATA - END */

/* Get booking details */
$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','');

/* Check if this user is authorized to perform this action */
// Get site cost
$cost_details = i_get_site_cost($booking_list["data"][0]["project_id"],'',$booking_list["data"][0]["crm_booking_added_on"],'','','');
$site_cost = $cost_details["data"][0]["crm_cost_value"];
// Get premium related data
$premium_details = i_get_site_premium($booking_list["data"][0]["project_id"],$booking_list["data"][0]["crm_booking_premium_type"],'','','');
if($premium_details["status"] == SUCCESS)
{
	$premium_value = $premium_details["data"][0]["crm_premium_value"];
}
else
{
	$premium_value = 0;
}

// Get max. allowed discount
$discount_details = i_get_site_discount_perms($booking_list["data"][0]["project_id"],$role,'','','');
$max_discount = $discount_details["data"][0]["crm_discount_master_value"];

// Calculate max. allowed for approval by this user
$max_discounted_value = ($site_cost + $premium_value) - $max_discount;

if($booking_list["data"][0]["crm_booking_rate_per_sq_ft"] >= $max_discounted_value)
{
	if($action == "1") // Approve
	{
		// Update booking as approved
		$booking_uresult = i_update_booking($booking_list["data"][0]["crm_booking_id"],'1',$user);
		$client_profile_uresult = db_update_prospective_profile_sold_status($profile_id,"SOLD");
		header("location:crm_approved_booking_list.php");
	}
	else // Reject
	{
		// Update booking as rejected
		$booking_uresult = i_update_booking($booking_list["data"][0]["crm_booking_id"],'2',$user);

		$site_update_uresult = i_update_site_status($booking_list["data"][0]["crm_booking_site_id"],AVAILABLE_STATUS,$user);
		header("location:crm_pending_booking_list.php");
	}
}
else
{
	header("location:crm_pending_booking_list.php");
}
?>
