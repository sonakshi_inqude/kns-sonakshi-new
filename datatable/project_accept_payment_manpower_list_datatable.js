var table;

var columnsMapping = {
  '3': 'project',
  '11': 'issued_amount',
  '12': 'balance_amount',
}

function tableDraw() {
  table.fnDraw();
}
var commonCellDefinition = {
  orderable: false,
  data: getCellValue,
}

function manpower_payment(vendor_id) {
  $('#ajax_loading').show();
  $.ajax({
    url: "tools/manpower_list/getVenderPaymentDetails.php?search_vendor=" + vendor_id,
    success: function(result) {
      $('#ajax_loading').hide();
      $("#details").html(result);
      $("#myModal").modal();
      $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
        ]
      });
    }
  });
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}


function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

var columns = [{
    className: 'noVis',
    "orderable": false,
    "data": function() {
      return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
    }
  },
  {
    "orderable": false,
    "data": "project_manpower_agency_name"
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.project_actual_payment_manpower_bill_no;
    }
  },
  commonCellDefinition,
  {
    "orderable": false,
    data: function(data, type) {
      return moment(data.project_actual_payment_manpower_from_date).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      return moment(data.project_actual_payment_manpower_to_date).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "data": "stock_company_master_name"
  },
  {
    "orderable": false,
    "data": "project_actual_payment_manpower_amount"
  },
  {
    "orderable": false,
    "data": "project_actual_payment_manpower_tds"
  },
  {
    "orderable": false,
    data: function(data, type) {
      return ((data.project_actual_payment_manpower_tds / 100) * data.project_actual_payment_manpower_amount).toFixed(2);
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      return (data.project_actual_payment_manpower_amount - ((data.project_actual_payment_manpower_tds / 100) * data.project_actual_payment_manpower_amount)).toFixed(2);
    }
  },
  commonCellDefinition,
  commonCellDefinition,
  {
    "orderable": false,
    data: function(data, type) {
      if (data.project_actual_payment_manpower_approved_on == '0000-00-00 00:00:00') {
        return '00-00-0000';
      }
      return moment(data.project_actual_payment_manpower_approved_on).format('DD/MM/YYYY');
    }
  },
  {
    "orderable": false,
    "data": "approved_by"
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.edit) {
        return `<a target = _blank href=project_manpower_print.php?manpower_payment_id=${data.project_actual_payment_manpower_id}&print_status=duplicate>
        <span class="glyphicon glyphicon-Print"></span></a>`
      }
      return '***';
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.edit) {
        return `<a href=# onclick=manpower_payment(${data.project_actual_payment_manpower_vendor_id})>
        <span style=font-size:16px>₹</span></a>`;
      }
      return '***';
    }
  },
];

$(document).ready(function() {
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_accept_payment_manpower_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_manpower_payment_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnCreatedRow: function(row, full, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_manpower_details.php',
        data: "payment_id=" + full.project_actual_payment_manpower_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
        }
      });

      $.ajax({
        url: 'ajax/get_manpower_payment_details.php',
        data: "payment_id=" + full.project_actual_payment_manpower_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'issued_amount' + '_' + index).html(response.issued_amount);
          var payable = full.project_actual_payment_manpower_amount -
            ((full.project_actual_payment_manpower_tds / 100) *
              full.project_actual_payment_manpower_amount).toFixed(2);
          $('span.' + 'balance_amount' + '_' + index).html((payable - response.issued_amount).toFixed(2));
        }
      });
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 2
    },
    scrollX: true,
    "columns": columns,
  });
});