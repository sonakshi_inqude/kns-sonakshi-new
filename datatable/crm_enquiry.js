var table;
var columnsMapping = {
  '7': 'follow_up_count',
  '9': 'site_count',
  '13': 'sold_status',
  '15': 'follow_up_date'
};

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function openSiteVisitPlanModal(enquiry_id) {
  $('#exampleModal').modal({
    remote: 'crm_add_site_visit_plan.php?enquiry=' + enquiry_id
  });

  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}


function submitPlan() {
  $.ajax({
    url: 'crm_add_site_visit_plan.php',
    type: 'POST',
    data: {
      hd_enquiry: $('#hd_enquiry').val(),
      dt_site_visit: $('#dt_site_visit').val(),
      stxt_visit_time: $('#stxt_visit_time').val(),
      stxt_pickup_location: $('#stxt_pickup_location').val(),
      ddl_project: $('#ddl_project').val(),
      rd_confirm_status: $("input[id='rd_confirm_status']:checked").val(),
      rd_drive_type: $("input[id='rd_drive_type']:checked").val(),
      add_site_visit_plan_submit: 'yes'
    },
    success: function(response) {
      if (response == '') {
        $('#exampleModal').modal('hide');
        alert("succes");
      } else {
        alert(response);
      }
    }
  })
}

$(document).ready(function() {
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "name"
    },
    {
      "orderable": false,
      "data": "cell"
    },
    {
      "orderable": false,
      "data": function(data) {
        if (window.permissions.edit) {
          return `<span>${data.enquiry_number}</span><a target="_blank"
          href=crm_edit_enquiry.php?enquiry=${data.enquiry_id}>
          <span class='glyphicon glyphicon-pencil'></span></a>`
        }
        return data.enquiry_number;
      }
    },
    {
      "orderable": false,
      "data": "project_name"
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return data.email;
      }
    },
    {
      "orderable": false,
      "data": "enquiry_source_master_name"
    },
    {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    },
    {
      "orderable": true,
      "data": function(data) {
        return moment(data.added_on).format("DD/MM/YYYY");
      }
    },
    {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    },
    {
      "orderable": false,
      "data": function(data) {
        return moment(data.assigned_on).format("DD/MM/YYYY");
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.walk_in == "1") {
          return 'Yes';
        }
        return 'No';
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        if (data.crm_cust_interest_status_name == 'Prospective' && window.permissions.edit) {
          return `<span>${data.crm_cust_interest_status_name}</span><a target="_blank"
          href=crm_add_prospective_profile.php?enquiry=${data.enquiry_id}>
          <span class='glyphicon glyphicon-user'></span></a>`
        }
        return data.crm_cust_interest_status_name;
      }
    },
    {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        return createToolTip(data.user_name, 20);
      }
    },
    {
      "orderable": false,
      data: function(data, type, full, meta) {
        var classname = columnsMapping[meta.col] + '_' + meta.row;
        return '<span class="' + classname + '">loading..</span>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full, meta) {
        if (window.permissions.role == 1 || window.permissions.role == 5) {
          return '<a target="_blank" href="crm_add_enquiry_fup.php?enquiry=' + data.enquiry_id + '"><span class="glyphicon glyphicon-calendar"></span></a>';
        }
        if (!window.permissions.edit || data.assigned_to != window.permissions.user) {
          return '***';
        }
        return '<a target="_blank" href="crm_add_enquiry_fup.php?enquiry=' + data.enquiry_id + '"><span class="glyphicon glyphicon-calendar"></span></a>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full, meta) {
        if (window.permissions.role == 1 || window.permissions.role == 5) {
          return '<a href="#"><span id="visit_plan" class="fas fa-car"></span></a>';
        }
        if (!window.permissions.edit || data.assigned_to != window.permissions.user) {
          return '***';
        }
        return '<a href="#"><span id="visit_plan" class="fas fa-car"></span></a>';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full, meta) {
        if (window.permissions.role == 1 || window.permissions.role == 5) {
          return `<a style="color:red" target="_blank" href=crm_add_unqualified_enquiry.php?enquiry=${data.enquiry_id}><span class="glyphicon glyphicon-warning-sign"></span></a>`;
        }
        if (!window.permissions.edit || data.assigned_to != window.permissions.user) {
          return '***';
        }
        return `<a style="color:red" target="_blank" href=crm_add_unqualified_enquiry.php?enquiry=${data.enquiry_id}><span class="glyphicon glyphicon-warning-sign"></span></a>`;
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/crm_enquiry.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      $.ajax({
        url: 'ajax/get_follow_up_data.php',
        data: "enquiry_id=" + full.enquiry_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'follow_up_date' + '_' + index).html(moment(response.follow_up_date).format("DD/MM/YYYY"));
          $('span.' + 'follow_up_count' + '_' + index).html(response.fup_count);
          $('span.' + 'site_count' + '_' + index).html(response.sv_count);
          if (full.crm_cust_interest_status_name == 'Unqualified') {
            $(row).css("color", "red");
          }
          if (full.crm_cust_interest_status_id == "4") {
            var ref = 'div.DTFC_RightBodyWrapper table tr:nth-child(' + (index + 1) + ')';
            $(ref).find("td span.glyphicon-warning-sign").remove();
            $(ref).find("td span.glyphicon-calendar").remove();
            $(ref).find("td span.fas").remove();
          }
          $('span.' + 'sold_status' + '_' + index).html(response.isSold);
        }
      });
    },
    "language": {
      "infoFiltered": " "
    },
    "order": [
      [8, "asc"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_crm_enquiry";
      aoData.assigned_to = $('#ddl_search_assigned_to').val();
      aoData.project_id = $('#ddl_project').val();
      aoData.search_cell = $('#cell').val();
      aoData.enquiry_number = $('#enquiry_number').val();
      aoData.search_source = $('#ddl_search_source').val();
      aoData.search_status = $('#ddl_search_int_status').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },

    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 4
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'visit_plan') {
      openSiteVisitPlanModal(rowData['enquiry_id']);
    }
  });
});