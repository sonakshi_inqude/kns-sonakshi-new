<?php
  session_start();
  $base = $_SERVER['DOCUMENT_ROOT'];
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
  $dbConnection = get_conn_handle();

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
  $user = $_SESSION["loggedin_user"];
  $aColumns = array(
    'project_master_name',
    'project_task_planning_uom',
    'project_task_planning_id',
    'project_process_master_name',
    'project_task_master_name',
    'project_task_planning_per_day_out',
    'project_machine_master_name',
    'project_cw_master_name',
    'project_task_planning_object_type',
    'project_site_location_mapping_master_order',
    'project_task_planning_no_of_object',
    'project_task_planning_project_id',
    'project_task_planning_no_of_roads',
    'project_site_location_mapping_master_name',
    'uom_name',
    'pause_status',
    'project_task_planning_measurment',
    'user_name',
    'project_task_planning_added_on',
    'project_task_planning_start_date',
    'project_plan_process_name',
    'project_task_planning_task_id',
    'project_task_planning_total_days',
    'project_management_master_id',
    'project_process_master_order',
    'project_task_master_order',
    'project_process_master_id',
    'project_task_master_id',
    'project_task_planning_end_date'
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "project_task_planning_id";

   /* DB table to use */
   $sTable = 'task_planning';

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */

    /*
     * Local functions
     */
    function fatal_error($sErrorMessage = '')
    {
        header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error');
        die($sErrorMessage);
    }

    /*
     * Paging
     */
    $sLimit = "";
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = "LIMIT ".intval($_GET['iDisplayStart']).", ".
            intval($_GET['iDisplayLength']);
    }

    /*
     * Ordering
     */
     $sOrder = "ORDER BY `project_process_master_order`,`project_task_master_order`,`project_site_location_mapping_master_order` asc";
    /*
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */
     $sWhere = "";
     if($sWhere == "") {
       $sWhere .= "Where `project_management_master_id` IN (select `project_user_mapping_project_id` from `project_user_mapping`
       where `project_user_mapping_user_id`= $user  and `project_user_mapping_active` = 1)";
        }

      if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
          $sWhere = "Where (";
          for ($i=0 ; $i<count($aColumns) ; $i++) {
              $sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch'])."%' OR ";
          }
          $sWhere = substr_replace($sWhere, "", -3);
          $sWhere .= ") AND `project_management_master_id` IN (select `project_user_mapping_project_id` from `project_user_mapping`
          where `project_user_mapping_user_id`= $user  and `project_user_mapping_active` = 1)";
      }

    if (isset($_GET['project_id']) && !empty($_GET['project_id'])) {
        if (!empty($sWhere)) {
            $sWhere .= " AND ";
        }
        $sWhere .= "project_management_master_id = ". $_GET['project_id'];
    }

    if (isset($_GET['search_process']) && $_GET['search_process'] != '') {
        if ($sWhere == "") {
            $sWhere .= " WHERE `project_process_master_id` = ". $_GET['search_process'];
        } else {
            $sWhere .= " AND `project_process_master_id` = ". $_GET['search_process'];
        }
    }

    if (isset($_GET['search_task']) && $_GET['search_task'] != '') {
        if (!empty($sWhere)) {
            $sWhere .= " AND ";
        }
        $sWhere .= "project_task_master_id = ". $_GET['search_task'];
    }

    if (isset($_GET['start_date']) && $_GET['start_date'] != '') {
        if (!empty($sWhere)) {
            $sWhere .= " AND ";
        }
        $sWhere .= "project_task_planning_end_date >= '".$_GET['start_date']."'";
    }
    if (isset($_GET['end_date']) && $_GET['end_date'] != '') {
        if (!empty($sWhere)) {
            $sWhere .= " AND ";
        }
        $sWhere .= "project_task_planning_end_date <= '".$_GET['end_date']."'";
    }
    /*
     * SQL queries
     * Get data to display
     */
    $sQuery = "
		SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
		FROM   $sTable
		$sWhere
    $sOrder
		$sLimit
		";

    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $rResult = $statement -> fetchAll();

    /* Data set length after filtering */

    $sQuery = "SELECT FOUND_ROWS() as iFilteredTotal";
    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];

    /* Total data set length */
    $sQuery = "
		SELECT COUNT(".$sIndexColumn.") as iTotal
		FROM   $sTable
	";
    $statement = $dbConnection->prepare($sQuery);
    $statement -> execute();
    $iTotal = $statement -> fetch()['iTotal'];
    /*
     * Output
     */
     $output = array(
         "draw" => intval($_GET['draw']),
         "iTotalRecords" => $iTotal,
         "iTotalDisplayRecords" => $iFilteredTotal,
         "aaData" => $rResult,
     "where" => $sQuery,
     );
     echo json_encode($output);
