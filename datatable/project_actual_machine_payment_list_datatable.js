var table;
var columnsMapping = {
  '2': 'project',
  '6': 'machine',
  '7': 'machine_name',
  '8': 'bata_amount'
}

function tableDraw() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function setBataAmount(row_id, amount, payment_id, bill_no) {
  var bata = parseInt($('span.' + 'bata_amount' + '_' + row_id).html());
  bill_no = bill_no + "/B"
  if (bata > 0) {
    var ok = confirm("Are you sure you want to Accept?");
    if (ok) {
      // amount -= bata;
      $.ajax({
        url: 'ajax/update_machine_payment.php',
        data: "total_amount=" + amount + "&bata_amount=" + bata + "&payment_id=" + payment_id + "&bill_no=" + bill_no,
        success: function(response) {
          response = JSON.parse(response);
          console.log(response.status);
          if (response.status == 'SUCCESS') {
            tableDraw();
            alert("SUCCESS");
          } else {
            alert(response.status);
          }
        }
      });
    }
  } else {
    alert("bata amount is nil");
  }
}

function project_approve_task_actual_machine_payment(machine_payment_id) {
  var ok = confirm("Are you sure you want to Accept?");
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        alert(xmlhttp.responseText);
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          tableDraw();
        }
      }
    }
    xmlhttp.open("POST", "project_approve_machine_payment.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("machine_payment_id=" + machine_payment_id + "&action=Accepted");
  }
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project' || columnsMapping[meta.col] == 'machine_name') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_machine_vendor_master_name"
    },
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_payment_machine_bill_no;
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_from_date).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_payment_machine_to_date).
        format('DD-MM-YYYY');
      }
    },
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_amount;
        }
        return `₹ ${data.project_payment_machine_amount}`
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_payment_machine_remarks;
        }
        return createToolTip(data.project_payment_machine_remarks);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.added_by;
        }
        return createToolTip(data.added_by);
      }
    },
    {
      "orderable": true,
      data: function(data, type) {
        return moment(data.project_payment_machine_added_on).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.aprove) {
          return `<a href="#" onclick=project_approve_task_actual_machine_payment(${data.project_payment_machine_id})>
          <span class="glyphicon glyphicon-ok"></span></a>`;
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_machine_weekly_print.php?machine_payment_id=${data.project_payment_machine_id}&bata_status=${data.project_bata_payment_machine_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full, meta) {
        if (!window.permissions.edit) {
          return '***';
        } else if (data.project_bata_payment_machine_status != 'Bill Generated') {
          return `<button type="button" class="btn btn-primary btn-xs"
          onclick=setBataAmount(${meta.row},${data.project_payment_machine_amount},${data.project_payment_machine_id},'${data.project_payment_machine_bill_no}')>
          Issue Bata</button>`;
        } else {
          return 'Bill Generated';
        }
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit && data.project_bata_payment_machine_status == 'Bill Generated') {
          return `<a target = _blank href=project_machine_bata_print.php?machine_payment_id=${data.project_payment_machine_id}&bata_status=${data.project_bata_payment_machine_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_actual_machine_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_machine_payment_list";
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, full, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_machine_details.php',
        data: "payment_id=" + full.project_payment_machine_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
          $('span.' + 'machine' + '_' + index).html(response.machine);
          $('span.' + 'machine_name' + '_' + index).html(createToolTip(response.machine_name));
          $('span.' + 'bata_amount' + '_' + index).html(response.bata_amount);
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 4
    },
    scrollX: true,
    "columns": columns,
  });
});