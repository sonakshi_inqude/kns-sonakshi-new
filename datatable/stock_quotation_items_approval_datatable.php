<?php
  $base = $_SERVER['DOCUMENT_ROOT'];
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'db_config.php');

  /* Database connection information */
  $gaSql['user']       = DATABASE_USERNAME;
  $gaSql['password']   = DATABASE_PASSWORD;
  $gaSql['db']         = DATABASE_NAME;
  $gaSql['server']     = DATABASE_HOST;

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */

    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
  $aColumns = array(
    'quote_id',
    'system_quote_no',
    'stock_material_name',
    'stock_material_code',
    'quote_qty',
    'user_name',
    'added_on',
    'quote_project',
    'user_quote_no',
    'stock_vendor_name',
    'recieved_date',
    'stock_unit_name',
    'quote_amount',
    'stock_project_name',
    );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "quote_id";

   /* DB table to use */
   $sTable = 'stock_quotation_list';

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */

    /*
     * Local functions
     */
    function fatal_error($sErrorMessage = '')
    {
        header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error');
        die($sErrorMessage);
    }

    /*
     * MySQL connection
     */
  $con  =  mysqli_connect($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db']);

   if (mysqli_connect_errno()) {
       echo "Failed to connect to MySQL: " . mysqli_connect_error();
   }

   mysqli_select_db($con, $gaSql['db']);

    /*
     * Paging
     */
    $sLimit = "";
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = "LIMIT ".intval($_GET['iDisplayStart']).", ".
            intval($_GET['iDisplayLength']);
    }


    /*
     * Ordering
     */
    $sOrder = " order by added_on DESC";
    if (isset($_GET['iSortCol_0'])) {
        $sOrder = "ORDER BY";
        for ($i=0 ; $i<intval($_GET['iSortingCols']) ; $i++) {
            if ($_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true") {
                $sOrder .= "`".$aColumns[ intval($_GET['iSortCol_'.$i]) ]."` ".
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
        }

        $sOrder = substr_replace($sOrder, "", -2);
        if ($sOrder == "ORDER BY") {
            $sOrder = "";
        }
    }


    /*
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */
    $sWhere = "";
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
        $sWhere = "WHERE (";
        for ($i=0 ; $i<count($aColumns) ; $i++) {
            $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysqli_real_escape_string($con, $_GET['sSearch'])."%' OR ";
        }
        $sWhere = substr_replace($sWhere, "", -3);
        $sWhere .= ")";
    }

    if (isset($_GET['search_status']) && $_GET['search_status'] != '') {
        if ($sWhere == "") {
            $sWhere .= " WHERE `quote_status` = '". $_GET['search_status']."'";
        } else {
            $sWhere .= " AND `quote_status` = '". $_GET['search_status']."'";
        }
    }
    if (isset($_REQUEST['hd_material_id']) && $_REQUEST['hd_material_id'] != '') {
        if ($sWhere == "") {
            $sWhere .= " WHERE `material_id` = ". $_REQUEST['hd_material_id'];
        } else {
            $sWhere .= " AND `material_id` = ". $_REQUEST['hd_material_id'];
        }
    }
    // $sGroup = " group by material_id,project_id,item_active" ;

    /*
     * SQL queries
     * Get data to display
     */
    $sQuery = "
		SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
		FROM   $sTable
		$sWhere
    $sOrder
		$sLimit
		";
    $rResult = mysqli_query($con, $sQuery) or fatal_error('MySQL Error: '.$sQuery);
    /* Data set length after filtering */
    $sQuery = "
		SELECT FOUND_ROWS()
	";
    $rResultFilterTotal = mysqli_query($con, $sQuery) or fatal_error('MySQL Error: ' . 'mysqli_errno');
    $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
    $iFilteredTotal = $aResultFilterTotal[0];

    /* Total data set length */
    $sQuery = "
		SELECT COUNT(`".$sIndexColumn."`)
		FROM   $sTable
	";
    $rResultTotal = mysqli_query($con, $sQuery) or fatal_error('MySQL Error: ' . 'mysqli_errno');
    $aResultTotal = mysqli_fetch_array($rResultTotal);
    $iTotal = $aResultTotal[0];


    /*
     * Output
     */
    $output = array(
        // "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => $sWhere,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => array()
    );
    while ($aRow = mysqli_fetch_array($rResult)) {
        $row = array();
        for ($i=0 ; $i<count($aColumns) ; $i++) {
            if ($aColumns[$i] == "version") {
                /* Special output formatting for 'version' column */
                $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
            } elseif ($aColumns[$i] != ' ') {
                /* General output */
                $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
            }
        }
        $row[0] = 'index';
        $output['aaData'][] = $row;
        $output['where'] = $sWhere;
    }
    echo json_encode($output);
