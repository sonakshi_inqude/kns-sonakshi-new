var table;

function tableDraw() {
  table.draw();
}
$(document).ready(function() {

  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_machine_vendor_master_name"
    },
    {
      "orderable": false,
      "data": "project_master_name"
    },
    {
      "orderable": false,
      "data": "project_process_master_name"
    },
    {
      "orderable": false,
      "data": "project_task_master_name"
    },
    {
      "orderable": false,
      "data": "project_uom_name"
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_work_type"
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_machine_plan_road_id == "No Roads" ||
          data.project_task_actual_machine_plan_road_id == "") {
          return 'No Roads';
        }
        return data.project_site_location_mapping_master_name;
      }
    },
    {
      "orderable": false,
      "data": "project_machine_type"
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return data.project_machine_master_name + " " + data.project_machine_master_id_number;
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        var end_date;
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_machine_plan_end_date_time == "0000-00-00 00:00:00") {
          end_date = moment().format('x');
        } else {
          end_date = moment(data.project_task_actual_machine_plan_end_date_time).format('x');;
        }
        var diff_time = end_date - moment(data.project_task_actual_machine_plan_start_date_time).format('x');
        return (diff_time / (3600 * 1000)).toFixed(2);
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return Math.round(data.project_task_actual_machine_plan_off_time / 60);
      }
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_fuel_charges"
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_msmrt"
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_machine_work_type === 'Regular') {
          return data.project_task_actual_machine_completion;
        } else {
          return data.project_task_actual_machine_rework_completion;
        }
      }
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return Math.round(data.project_task_actual_machine_plan_additional_cost);
      }
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_bata"
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_issued_fuel"
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_plan_remarks"
    },
    {
      "orderable": false,
      data: function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return moment(data.project_task_actual_machine_plan_added_on).
        format('DD-MM-YYYY hh:mm:ss');
      }
    },
    {
      "orderable": false,
      "data": "user_name"
    },
    {
      "orderable": true,
      "orderData": [22, 23],
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return moment(data.project_task_actual_machine_plan_start_date_time).
        format('DD-MM-YYYY hh:mm:ss');
      }
    },
    {
      "orderable": true,
      "orderData": [23, 22],
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_machine_plan_end_date_time != "0000-00-00 00:00:00") {
          return moment(data.project_task_actual_machine_plan_end_date_time)
            .format('DD-MM-YYYY hh:mm:ss');
        } else {
          return '';
        }
      }
    },
    {
      "orderable": false,
      data: function(data, type, full) {
        var end_date;
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_machine_plan_end_date_time == "0000-00-00 00:00:00") {
          end_date = moment().format('x');
        } else {
          end_date = moment(data.project_task_actual_machine_plan_end_date_time).format('x');;
        }
        var diff_time = Math.round(end_date - moment(data.project_task_actual_machine_plan_start_date_time).format('x'));
        var eff_hrs = parseFloat((diff_time / (3600 * 1000))) - parseFloat(data.project_task_actual_machine_plan_off_time / 60);
        return (parseFloat(data.project_task_actual_machine_fuel_charges * eff_hrs) + parseFloat(data.project_task_actual_machine_bata) +
          parseFloat(data.project_task_actual_machine_plan_additional_cost) - parseFloat(data.project_task_actual_machine_issued_fuel)).toFixed(2);
      }
    },
    {
      "orderable": false,
      "className": 'noVis',
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return '<a href="#" id="edit">Edit</a>';
      }
    },
    {
      "orderable": false,
      "className": 'noVis delete',
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (window.permissions.delete) {
          return '<a href="#" id="delete">Delete</a>';
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "className": 'noVis',
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        var percentage;
        if (data.project_task_actual_machine_work_type == 'Regular') {
          percentage = parseFloat(data.project_task_actual_machine_completion);
        } else {
          percentage = parseFloat(data.project_task_actual_machine_rework_completion);
        }
        if (data.project_task_actual_machine_plan_check_status == "0" && percentage > 0) {
          if (window.permissions.ok) {
            return '<a href="#" id="ok">OK</a>';
          }
          return 'OK Pending';
        } else if (data.project_task_actual_machine_plan_check_status == "1" && percentage > 0) {
          if (window.permissions.aprove) {
            return '<a href="#" id="approve">Approve</a>';
          }
          return 'Approval Pending';
        } else if (percentage == 0) {
          return 'Enter	completed msrmnt'
        }
      }
    },
  ];

  if (!window.permissions.delete) {
    columns.splice(25, 1);
  }

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_machine_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_machine_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.search_process = $('#search_process').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: ((window.permissions.delete) ? 5 : 4)
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'A' && event.target.id == 'edit') {
      go_to_project_edit_task_actual_machine_plan(rowData['project_task_actual_machine_plan_id'], rowData['project_task_actual_machine_plan_task_id'], rowData['project_task_actual_machine_plan_road_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'delete') {
      project_delete_machine_planning(table, rowData['project_task_actual_machine_plan_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'ok') {
      project_check_task_actual_machine(table, rowData['project_task_actual_machine_plan_id'], rowData['project_task_actual_machine_plan_task_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'approve') {
      project_approve_task_actual_machine(table, rowData['project_task_actual_machine_plan_id'], rowData['project_task_actual_machine_plan_task_id']);
    }
    // });
  })
});

// get hidden values saved from filters


function project_delete_machine_planning(table, planning_id) {
  var ok = confirm("Are you sure you want to Delete?")
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.draw(false);
        }
      }
    }
    xmlhttp.open("POST", "project_delete_task_actual_machine_plan.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("plan_id=" + planning_id + "&action=0");
  }
}



function go_to_project_edit_task_actual_machine_plan(plan_id, task_id, road_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "project_edit_task_actual_machine_plan.php");
  form.setAttribute("target", "blank");

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "plan_id");
  hiddenField1.setAttribute("value", plan_id);

  var hiddenField2 = document.createElement("input");
  hiddenField2.setAttribute("type", "hidden");
  hiddenField2.setAttribute("name", "task_id");
  hiddenField2.setAttribute("value", task_id);

  var hiddenField3 = document.createElement("input");
  hiddenField3.setAttribute("type", "hidden");
  hiddenField3.setAttribute("name", "road_id");
  hiddenField3.setAttribute("value", road_id);

  form.appendChild(hiddenField1);
  form.appendChild(hiddenField2);
  form.appendChild(hiddenField3);

  document.body.appendChild(form);
  form.submit();
}

function project_check_task_actual_machine(table, machine_id, task_id) {
  var ok = confirm("Are you sure you want to Ok?")
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.draw(false);
        }
      }
    }

    xmlhttp.open("POST", "project_check_machine.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("machine_actuals_id=" + machine_id + "&task_id=" + task_id + "&action=approved");
  }
}

function project_approve_task_actual_machine(table, machine_id, task_id) {
  var ok = confirm("Are you sure you want to Approve?")
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.draw(false);
        }
      }
    }

    xmlhttp.open("POST", "project_approve_actual_machine_plan.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("machine_id=" + machine_id + "&action=approved");
  }
}