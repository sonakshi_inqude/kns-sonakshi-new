var table;

function redrawTable() {
  table.draw();
}

$(document).ready(function() {
  var columnsMapping = {
    '17': 'planned_msmt',
    '18': 'actual_msmt',
    '20': 'pending_msmt'
  };

  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_manpower_agency_name"
    },
    {
      "orderable": false,
      "data": "project_master_name"
    },
    {
      "orderable": false,
      "data": "project_process_master_name"
    },
    {
      "orderable": false,
      "data": "project_task_master_name"
    },
    {
      "orderable": false,
      "data": "stock_unit_name"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_work_type"
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.road_name == "No Roads") {
          return 'No Roads';
        }
        return data.road_name;
      }
    },
    {
      "orderable": false,
      "data": "project_contract_process_name"
    },
    {
      "orderable": false,
      "data": "project_contract_rate_master_work_task"
    },
    {
      "orderable": false,
      "data": "location_name"
    },
    {
      "orderable": false,
      "data": "project_task_boq_actual_number"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_length"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_breadth"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_depth"
    },
    // {
    //   "orderable": false,
    //   data: function(data, type, full, meta) {
    //     var classname = columnsMapping[meta.col] + '_' + meta.row;
    //     return '<span class="' + classname + '">loading..</span>';
    //   }
    // },
    // {
    //   "orderable": false,
    //   data: function(data, type, full, meta) {
    //     var classname = columnsMapping[meta.col] + '_' + meta.row;
    //     return '<span class="' + classname + '">loading..</span>';
    //   }
    // },
    {
      "orderable": false,
      "data": "project_task_actual_boq_total_measurement"
    },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_boq_work_type === 'Regular') {
          return data.project_task_actual_boq_completion;
        } else {
          return data.project_task_actual_boq_rework_completion;
        }
      }
    },
    // {
    //   "orderable": false,
    //   data: function(data, type, full, meta) {
    //     var classname = columnsMapping[meta.col] + '_' + meta.row;
    //     return '<span class="' + classname + '">loading..</span>';
    //   }
    // },
    {
      "orderable": false,
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return Math.round(parseFloat(data.project_task_actual_boq_amount));
      }
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_lumpsum"
    },
    {
      "orderable": false,
      "data": "project_task_actual_boq_remarks"
    },
    {
      "orderable": false,
      "data": "added_by"
    },
    {
      "orderable": true,
      "orderData": [21, 22],
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return moment(data.project_task_actual_boq_added_on).format('DD-MM-YYYY');
      }
    },
    {
      "orderable": true,
      "orderData": [22, 21],
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return moment(data.project_task_actual_boq_date).format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      "className": 'noVis',
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        return '<a href="#" id="edit">Edit</a>';
      }
    },
    {
      "orderable": false,
      "className": 'noVis delete',
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        if (window.permissions.delete) {
          return '<a href="#" id="delete">Delete</a>';
        }
        return '***';
      }
    },
    {
      "orderable": false,
      "className": 'noVis',
      "data": function(data, type, full) {
        if (type !== 'display') {
          return '';
        }
        var percentage;
        if (data.project_task_actual_boq_work_type == 'Regular') {
          percentage = parseFloat(data.project_task_actual_boq_completion);
        } else {
          percentage = parseFloat(data.project_task_actual_boq_rework_completion);
        }
        if (data.project_task_actual_boq_status == 'Pending' && percentage > 0) {
          if (window.permissions.ok) {
            return '<a href="#" id="ok">OK</a>';
          }
          return 'Ok Pending';
        } else if (data.project_task_actual_boq_status == 'Checked' && percentage > 0) {
          if (window.permissions.aprove) {
            return '<a href="#" id="approve">Approve</a>';
          }
          return 'Approval Pending';
        } else if (percentage == 0) {
          return 'Enter	completed msrmnt'
        }
      }
    },
  ];

  if (!window.permissions.delete) {
    columns.splice(24, 1);
  }

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_contract_work.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_contract_work";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.search_contract = $('#search_contract').val();
      aoData.search_task = $('#search_task').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      if (Math.round(full.project_task_actual_boq_lumpsum) !=
        Math.round(full.project_task_actual_boq_total_measurement * full.project_task_actual_boq_amount)) {
        $(row).css("color", "red");
      }
      // $.ajax({
      //   url: 'ajax/get_total_measurement.php',
      //   data: "task_id=" + full.project_boq_actual_task_id +
      //     "&location=" + full.project_task_actual_boq_location +
      //     "&contract_task=" + full.project_task_actual_contract_task,
      //   // dataType: 'html',
      //   success: function(response) {
      //     $('span.' + 'planned_msmt' + '_' + index).html(response);
      //   }
      // });
      // $.ajax({
      //   url: 'ajax/get_measurement.php',
      //   data: "task_id=" + full.project_boq_actual_task_id +
      //     "&contract_task=" + full.project_task_actual_contract_task,
      //   // dataType: 'html',
      //   success: function(response) {
      //     response = parseFloat(response);
      //     $('span.' + 'actual_msmt' + '_' + index).html(response);
      //     var pending_msmt = response - full.project_task_actual_boq_total_measurement;
      //     $('span.' + 'pending_msmt' + '_' + index).html(pending_msmt);
      //   }
      // });
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: ((window.permissions.delete) ? 4 : 3)
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
    if (event.target.tagName == 'A' && event.target.id == 'edit') {
      go_to_project_edit_task_boq(rowData['project_task_boq_actual_id'], rowData['project_boq_actual_task_id'], rowData['project_task_actual_boq_road_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'delete') {
      project_delete_task_boq(table, rowData['project_task_boq_actual_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'ok') {
      project_check_task_actual_contract(table, rowData['project_task_boq_actual_id'], rowData['project_boq_actual_task_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'approve') {
      project_approve_task_actual_contract(table, rowData['project_task_boq_actual_id'], rowData['project_boq_actual_task_id']);
    }
  })
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(elements) {
  for (var element in elements) {
    if (element && elements[element].length) {
      var hiddenField1 = document.createElement("input");
      hiddenField1.setAttribute("type", "hidden");
      hiddenField1.setAttribute("id", element);
      hiddenField1.setAttribute("value", elements[element]);
      document.body.appendChild(hiddenField1);
    }
  }
}


function project_delete_task_boq(table, boq_actual_id) {
  var ok = confirm("Are you sure you want to Delete?")
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          // window.location = "project_contract_work.php";
          table.draw(false);
        }
      }
    }

    xmlhttp.open("POST", "project_delete_task_boq_actuals.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("boq_actual_id=" + boq_actual_id + "&action=0");
  }
}



function go_to_project_edit_task_boq(boq_actual_id, task_id, road_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "project_edit_task_boq_actuals.php");
  form.setAttribute("target", "blank");

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "boq_actual_id");
  hiddenField1.setAttribute("value", boq_actual_id);

  var hiddenField2 = document.createElement("input");
  hiddenField2.setAttribute("type", "hidden");
  hiddenField2.setAttribute("name", "task_id");
  hiddenField2.setAttribute("value", task_id);

  var hiddenField3 = document.createElement("input");
  hiddenField3.setAttribute("type", "hidden");
  hiddenField3.setAttribute("name", "road_id");
  hiddenField3.setAttribute("value", road_id);

  form.appendChild(hiddenField1);
  form.appendChild(hiddenField2);
  form.appendChild(hiddenField3);

  document.body.appendChild(form);
  form.submit();
}

function project_check_task_actual_contract(table, boq_id, task_id, search_project, search_vendor) {
  var ok = confirm("Are you sure you want to Ok?")
  if (ok)
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
  else { // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      if (xmlhttp.responseText != "SUCCESS") {
        document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
        document.getElementById("span_msg").style.color = "red";
      } else {
        // window.location = "project_contract_work.php?search_project=" + search_project;
        table.draw(false);
      }
    }
  }
  xmlhttp.open("POST", "project_check_contract_work.php"); // file name where delete code is written
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send("boq_id=" + boq_id + "&task_id=" + task_id + "&action=approved");
}

function project_approve_task_actual_contract(table, boq_id, task_id, search_project, search_vendor) {
  var ok = confirm("Are you sure you want to Approve?")
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          // window.location = "project_task_boq_actuals_list.php?search_project=" + search_project;
          table.draw(false);
        }
      }
    }
    xmlhttp.open("POST", "project_approve_contract_work.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("boq_id=" + boq_id + "&task_id=" + task_id + "&action=approved");
  }
}