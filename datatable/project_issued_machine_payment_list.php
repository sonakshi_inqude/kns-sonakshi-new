<?php
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
     session_start();
   $base = $_SERVER['DOCUMENT_ROOT'];
   include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
   $dbConnection = get_conn_handle();
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
  $user = $_SESSION["loggedin_user"];
  $aColumns = array(
    'accepted_by',
    'stock_company_master_name',
    'project_payment_machine_id',
    'project_payment_machine_tds',
    'project_payment_machine_status',
    'project_payment_machine_bill_no',
    'project_payment_machine_vendor_id',
    'project_machine_vendor_master_name',
    'project_payment_machine_from_date',
    'project_payment_machine_amount',
    'project_payment_machine_to_date',
    'project_payment_machine_remarks',
    'project_payment_machine_accepted_on',
    'project_bata_payment_machine_status',
    'project_payment_machine_status',
    'project_payment_machine_active',
    'project_payment_machine_bata',
    'project_machine_vendor_master_id',
    'project_payment_machine_approved_on',
  );

   /* Indexed column (used for fast and accurate table cardinality) */
   $sIndexColumn = "project_payment_machine_id";

   /* DB table to use */
   $sTable = $_GET['table'];

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */

	/*
	 * Local functions
	 */
	function fatal_error ( $sErrorMessage = '' )
	{
		header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
		die( $sErrorMessage );
	}

	/*
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
			intval( $_GET['iDisplayLength'] );
	}


	/*
	 * Ordering
	 */
   $sOrder = "";
 	if ( isset( $_GET['order'][0]["column"] ) )
 	{
 		$sOrder = "ORDER BY ";
 		for ( $i=0 ; $i<count($_GET['order']) ; $i++ )
 		{
 				$sOrder .= "`".$aColumns[ intval( $_GET['order'][$i]["column"] ) ]."` ".
 				($_GET['order'][$i]["dir"]==='asc' ? 'asc' : 'desc') .", ";
 		}

 		$sOrder = substr_replace( $sOrder, "", -2 );
 		if ( $sOrder == "ORDER BY" )
 		{
 			$sOrder = "";
 		}
 	}

	/*
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
    if($sWhere == "") {
      $sWhere .= "Where `project_payment_machine_active` = 1 AND `project_payment_machine_status` = 'Payment Issued'";
               }

               if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
             	{
             		$sWhere = "WHERE (";
             		for ( $i=0 ; $i<count($aColumns) ; $i++ )
             		{
             			$sWhere .= "`".$aColumns[$i]."` LIKE '%".( $_GET['sSearch'] )."%' OR ";
             		}
             		$sWhere = substr_replace( $sWhere, "", -3 );
             		$sWhere .= ") AND `project_payment_machine_active` = 1 AND `project_payment_machine_status` = 'Payment Issued'";
             	}

             	/* Individual column filtering */
             	for ( $i=0 ; $i<count($aColumns) ; $i++ )
             	{
             		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
             		{
             			if ( $sWhere == "" )
             			{
             				$sWhere = "WHERE ";
             			}
             			else
             			{
             				$sWhere .= " AND ";
             			}
             			$sWhere .= "`".$aColumns[$i]."` LIKE '%".($_GET['sSearch_'.$i])."%' ";
             		}
             	}

    if(isset($_GET['search_vendor']) && $_GET['search_vendor'] != '') {
      $sWhere .= " AND `project_machine_vendor_master_id` = ". $_GET['search_vendor'];
    }

    if(isset($_GET['start_date']) && $_GET['start_date'] != '') {
      $sWhere .= " AND `project_payment_machine_added_on` >= '". $_GET['start_date']." 00:00:00'";
    }

    if(isset($_GET['end_date']) && $_GET['end_date'] != '') {
      $sWhere .= " AND `project_payment_machine_added_on` <= '". $_GET['end_date']." 11:59:00'";
    }
	/*
	 * SQL queries
	 * Get data to display
	 */
   $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
    FROM $sTable
    $sWhere
    $sOrder
    $sLimit
    ";
   $statement = $dbConnection->prepare($sQuery);
   $statement -> execute();
   $rResult = $statement -> fetchAll();

 // get 'iFilteredTotal'
   $statement = $dbConnection->prepare("SELECT FOUND_ROWS() as iFilteredTotal");
   $statement -> execute();
   $iFilteredTotal = $statement -> fetch()['iFilteredTotal'];

 // get 'iTotal' Total data set length
   $statement = $dbConnection->prepare("SELECT COUNT(`".$sIndexColumn."`) as iTotal FROM $sTable");
   $statement -> execute();
   $iTotal = $statement -> fetch()['iTotal'];

  /*
   * Output
   */
  $output = array(
    "iTotalRecords" => $iTotal,
    "query" => $sQuery,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
  );

   foreach($rResult as $aRow){
    $row = array();
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
      if ( $aColumns[$i] == "version" )
      {
        /* Special output formatting for 'version' column */
        $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
      }
      else if ( $aColumns[$i] != ' ' )
      {
        /* General output */
        $row[$aColumns[$i]] = $aRow[ $aColumns[$i] ];
      }
    }
     $row[0] = 'index';
    $output['aaData'][] = $row;
  }
  echo json_encode( $output );
 ?>
