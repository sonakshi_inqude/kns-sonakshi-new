var table;

var columnsMapping = {
  '3': 'project',
  '8': 'men_rate',
  '9': 'men_total',
  '12': 'women_rate',
  '13': 'women_total',
  '16': 'mason_rate',
  '17': 'mason_total'
}

function tableDraw() {
  table.fnDraw();
}
var commonCellDefinition = {
  orderable: false,
  data: getCellValue,
  createdCell: colorizeCell,
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project') {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

function colorizeCell() {
  if (arguments[4] > 7 && arguments[4] < 10) {
    $(arguments[0]).addClass('blue');
  } else if (arguments[4] > 11 && arguments[4] < 14) {
    $(arguments[0]).addClass('red');
  } else if (arguments[4] > 15) {
    $(arguments[0]).addClass('green');
  }
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 10) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function project_approve_payment_manpower(manpower_id) {
  var ok = confirm("Are you sure you want to Approve?")
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        alert(xmlhttp.responseText);
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          tableDraw();
        }
      }
    }

    xmlhttp.open("POST", "project_approve_actual_payment_manpower.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("manpower_id=" + manpower_id + "&action=Approved");
  }
}

var columns = [{
    className: 'noVis',
    "orderable": false,
    "data": function() {
      return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
    }
  },
  {
    "orderable": false,
    "data": "project_manpower_agency_name"
  },
  {
    "orderable": false,
    "data": function(data, type) {
      return data.project_actual_payment_manpower_bill_no;
    }
  },
  commonCellDefinition,
  {
    "orderable": false,
    data: function(data, type) {
      return moment(data.project_actual_payment_manpower_from_date).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      return moment(data.project_actual_payment_manpower_to_date).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "className": "blue",
    "data": "project_actual_payment_men_hrs"
  },
  {
    "orderable": false,
    "className": "blue",
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_actual_payment_men_hrs / 8;
    },
  },
  commonCellDefinition,
  commonCellDefinition,
  {
    "orderable": false,
    "className": "red",
    "data": "project_actual_payment_women_hrs"
  },
  {
    "orderable": false,
    "className": "red",
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_actual_payment_women_hrs / 8;
    },
  },
  commonCellDefinition,
  commonCellDefinition,
  {
    "orderable": false,
    "className": "green",
    "data": "project_actual_payment_mason_hrs"
  },
  {
    "orderable": false,
    "className": "green",
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_actual_payment_mason_hrs / 8;
    }
  },
  commonCellDefinition,
  commonCellDefinition,
  {
    "orderable": false,
    "data": "project_actual_payment_manpower_hrs"
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type === 'export') {
        return data.project_actual_payment_manpower_remarks;
      }
      return createToolTip(data.project_actual_payment_manpower_remarks);
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type === 'export') {
        return data.added_by;
      }
      return createToolTip(data.added_by);
    }
  },
  {
    "orderable": false,
    data: function(data, type) {
      return moment(data.project_actual_payment_manpower_added_on).
      format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "data": "project_actual_payment_manpower_amount"
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.aprove) {
        return `<a href="#" onclick=project_approve_payment_manpower(${data.project_actual_payment_manpower_id})>
        <span class="glyphicon glyphicon-ok"></span></a>`;
      }
      return '***';
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (window.permissions.edit) {
        return `<a target = _blank href=project_manpower_print.php?manpower_payment_id=${data.project_actual_payment_manpower_id}>
        <span class="glyphicon glyphicon-Print"></span></a>`;
      }
      return '***';
    }
  },
];

$(document).ready(function() {
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_actual_payment_manpower_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_manpower_payment_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnCreatedRow: function(row, full, index) {
      if ($('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_manpower_details.php',
        data: "payment_id=" + full.project_actual_payment_manpower_id,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'men_rate' + '_' + index).html(response.men_rate);
          $('span.' + 'men_total' + '_' + index).html(((full.project_actual_payment_men_hrs / 8) * response.men_rate).toFixed(2));
          $('span.' + 'women_rate' + '_' + index).html(response.women_rate);
          $('span.' + 'women_total' + '_' + index).html(((full.project_actual_payment_women_hrs / 8) * response.women_rate).toFixed());
          $('span.' + 'mason_rate' + '_' + index).html(response.mason_rate);
          $('span.' + 'mason_total' + '_' + index).html(((full.project_actual_payment_mason_hrs / 8) * response.mason_rate).toFixed(2));
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
        }
      });
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    "columns": columns,
  });
});