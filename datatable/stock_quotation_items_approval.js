var table;
// function toggleSelection(ele) {
//   $("input.input-checkbox").prop("checked", ele.checked);
// }

function redrawTable() {
  table.fnDraw(false);
}

$(document).ready(function() {
  function createToolTip(str, length) {
    var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_quotation_items_approval_datatable.php',
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      // get the stock qty
      $.ajax({
        url: 'ajax/stock_get_material_stock.php',
        data: "project_id=" + full['project_id'] + "&material_id=" + full['material_id'],
        dataType: 'json',
        success: function(stock_response) {
          $('span#stock_qty_' + index).html(stock_response);
        }
      });
    },
    fnServerParams: function(aoData) {
      aoData.search_status = $('#search_status').val();
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 5
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.stock_material_name, 10);
        },
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.stock_material_code, 10);
        },
      },
      {
        orderable: false,
        data: 'stock_project_name'
      },
      {
        orderable: false,
        data: 'quote_qty'
      },
      {
        orderable: false,
        data: 'stock_unit_name'
      },
      {
        orderable: false,
        data: 'system_quote_no'
      },
      {
        orderable: false,
        data: 'user_quote_no'
      },
      {
        orderable: false,
        data: 'stock_vendor_name'
      },
      {
        orderable: true,
        data: function(data) {
          return moment(data.recieved_date).format('DD/MM/YYYY');
        }
      },
      {
        orderable: false,
        data: 'user_name'
      },
      {
        orderable: false,
        data: 'quote_amount'
      },
      {
        orderable: true,
        data: function(data) {
          return moment(data.added_on).format('DD/MM/YYYY');
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data, type, full) {
          if (!window.permissions.view) {
            return "***";
          }
          return '<a  href="#"><span id="view" class="glyphicon glyphicon-folder-open"></span></a>';
        }
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if ($("#search_status").val() == 'Approved' || !window.permissions.edit || $("#search_status").val() == 'Rejected') {
            return "***";
          }
          return `<a href="#"><span id="approve" class="glyphicon glyphicon-ok"></span></a>`;
        }
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if ($("#search_status").val() == 'Rejected' || !window.permissions.edit) {
            return "***";
          }
          return `<a style=color:red href="#"><span id=reject class="glyphicon glyphicon-remove-circle"></span></a>`;
        }
      },
    ]
  });
  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.api().row(this).data();
    if (event.target.tagName == 'SPAN' && event.target.id == 'approve') {
      approve_or_reject_quotation(rowData.quote_id,rowData.quote_project,'Approved');
    }
  else if (event.target.tagName == 'SPAN' && event.target.id == 'reject') {
  approve_or_reject_quotation(rowData.quote_id,rowData.quote_project,'Rejected');
    }
    else {
      if (event.target.tagName == 'SPAN' && event.target.id == 'view') {
          showQuoteItems(rowData['quote_id'],rowData['stock_vendor_name']);
        }
    }
  });
});
function showQuoteItems(quote_id,vendor_name) {
  var temp = {
    id: quote_id,
    name: vendor_name
  };
  console.log('temp ', temp);
  $('#myModal1').modal({
    remote: 'stock_quotation_items_list.php?'+ $.param(temp)
  });

  $('#myModal1').on('hidden.bs.modal', function(event) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}

function approve_or_reject_quotation(quotation_id,projectid,status)
{if(status == "Approved")
{
  var display_status = " Approve"
}
else if(status == "Rejected")
{
  display_status = " Reject";
}
	var ok = confirm("Are you sure you want to" + display_status+ "?")
	{
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
          console.log(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 redrawTable();
					}
				}
			}
			xmlhttp.open("POST", "stock_approve_quotation.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("quotation_id=" + quotation_id + "&projectid=" + projectid + "&action="+ status);
		}
	}
}
