var table;
var columnsMapping = {
  '6': 'planned_mp',
  '7': 'planned_mc',
  '8': 'planned_cw',
  '9': 'planned_material',

  '10': 'total_mp_cost',
  '11': 'total_mc_cost',
  '12': 'total_actual_cw_cost',
  '13': 'total_actual_material_cost',

  '14': 'variance_mp_cost',
  '15': 'variance_mc_cost',
  '16': 'variance_actual_cw_cost',
  '17': 'variance_actual_material_cost'
};

var diffMapping = {
  "total_mp_cost": "planned_mp",
  "total_mc_cost": "planned_mc",
  "total_actual_cw_cost": "planned_cw",
  "total_actual_material_cost": "planned_material",
};

function checkFileAttached() {
  $('#formImport').submit();
  var project = $('#Project_id option:selected').text().trim();
  alert("WARNING: This action will delete all the existing planned budget for" + ' ' + project);
}

function colorizeCell() {
  if (arguments[4] < 10) {
    $(arguments[0]).addClass('blue');
  } else if (arguments[4] > 13) {
    $(arguments[0]).addClass('red');
  }
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  return '<span class="' + classname + '">loading..</span>';
}

$(document).ready(function() {
  get_process($('#Project_id').val());
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue,
    createdCell: colorizeCell
  };

  $('#alert_show').addClass('hide');

  function createToolTip(str, length) {
    var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    searchDelay: 1500,
    dataSrc: 'aaData',
    processing: true,
    ajax: 'datatable/project_budget_report_datatable.php',
    scrollX: true,
    scrollY: 600,
    scrollCollapse: true,
    fixedHeader: true,
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 1
    },
    fnServerParams: function(aoData) {
      console.log($('#ddl_process_id').val());
      aoData.project_id = $('#Project_id').val();
      aoData.search_process = $('#ddl_process_id').val();
      aoData.table = "project_budget_details";
      aoData.search_project = getVars('search_project');
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);

      // get the planned
      $.ajax({
        url: 'ajax/project_get_budget_planned_data.php',
        data: "task_id=" + full['task_id'] + "&road_id=" + full['road_id'],
        dataType: 'json',
        success: function(planned_response) {
          $.each(planned_response, function(key, value) {
            $('span.' + key + '_' + index).html(value);
          });

          // get the actuals
          $.ajax({
            url: 'ajax/project_get_actual_data.php',
            data: "task_id=" + full['task_id'] + "&road_id=" + full['road_id'],
            dataType: 'json',
            success: function(actual_response) {
              var data = table.api().row(index).data();
              data.total_mp_cost = actual_response["total_mp_cost"];
              data.total_mc_cost = actual_response["total_mc_cost"];
              data.total_cw_cost = actual_response["total_actual_cw_cost"];
              data.total_material_cost = actual_response["total_actual_material_cost"];
              data.planned_manpower = planned_response["planned_mp"];
              data.planned_machine = planned_response["planned_mc"];
              data.planned_contract = planned_response["planned_cw"];
              data.planned_material = planned_response["planned_material"];
              $.each(actual_response, function(key, value) {
                // first print the actual
                $('span.' + key + '_' + index).html(value);
                // next print the diff
                // diff = planned - actual
                var diff_value = planned_response[diffMapping[key]] - value;
                var className = key.replace('total', 'variance') + '_' + index;
                $('span.' + className).html(diff_value);
                var data = table.api().row(index).data();
                if (diff_value < 0) {
                  $(row).css("color", "red");
                }
              })
            }
          });
        }
      });
    },
    drawCallback: function() {
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: true,
        data: 'project_master_name'
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_process_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data) {
          return createToolTip(data.project_task_master_name, 20);
        },
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (data['road_id'] == 'No Roads') {
            return 'No Roads';
          }
          return data['project_site_location_mapping_master_name'];
        }
      },
      {
        orderable: false,
        data: `project_uom_name`
      },
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      commonCellDefinition,
      {
        orderable: false,
        data: function() {
          if ($('#edit_perm').val() == 0) {
            return '<button  onclick="showModal(' + arguments[3].row + ')"> <span class="glyphicon glyphicon-pencil"></span></button>';
          }
          return '***';
        }
      }
    ]
  });
  $("#Project_id").change(function() {
    var project_id = $(this).val();
    get_process(project_id);
  })
});

function get_process(project_id) {
  $.ajax({
    url: 'ajax/project_get_process.php',
    data: {
      project_id: project_id
    },
    dataType: 'json',
    success: function(response) {
      $("#search_process").empty();
      $("#search_process").append("<option value=''>Select Process</option>");
      for (var i = 0; i < response.length; i++) {
        var id = response[i]['process_master_id'];
        var name = response[i]['process_name'];
        $("#search_process").append("<option value='" + id + "'>" + name + "</option>");
        var selectedVal = $('#ddl_process_id').val();
        $('#search_process option').map(function() {
          if ($(this).val() == selectedVal) return this;
        }).attr('selected', 'selected');
      }
    }
  })
}

function showModal(row_id) {
  $('#alert_show').addClass('hide');
  var rowData = table.api().data()[row_id];
  $('#myModal').modal();
  $('#selected_row_id').val(row_id);
  $('#myModal').on('shown.bs.modal', function() {
    $('span#header_project_name').html(rowData.project_master_name);
    $('span#header_process_name').html(rowData.project_process_master_name);
    $('span#header_task_name').html(rowData.project_task_master_name);
    $('span#uom').html(rowData.project_uom_name);
    if (rowData.road_id == "No Roads") {
      $('span#road').html('No Roads');
    } else {
      $('span#road').html(rowData.project_site_location_mapping_master_name);
    }
    $('#mp_budget').val(rowData.planned_manpower);
    $('#mc_budget').val(rowData.planned_machine);
    $('#cw_budget').val(rowData.planned_contract);
    $('#material_budget').val(rowData.planned_material);
    $('#project_id').val(rowData.project_id);
    $('#process_id').val(rowData.Plan_process_id, );
    $('#task_id').val(rowData.task_id);
    $('#uom').val(rowData.project_uom_name);
    $('#road_id').val(rowData.road_id);
    $('#total_mp_cost').val(rowData.total_mp_cost);
    $('#total_mc_cost').val(rowData.total_mc_cost);
    $('#total_material_cost').val(rowData.total_material_cost);
    $('#total_cw_cost').val(rowData.total_cw_cost);
    $('#project_name').val(rowData.project_master_name);
    $('#process_name').val(rowData.project_process_master_name);
    $('#task_name').val(rowData.project_task_master_name);
    $('#road_name').val(rowData.project_site_location_mapping_master_name);
    $('#planned_contract').val(rowData.planned_contract);
    $('#planned_machine').val(rowData.planned_machine);
    $('#planned_manpower').val(rowData.planned_manpower);
    $('#planned_material').val(rowData.planned_material);
  });
  // remove the Modal content from DOM
  $('#myModal').on('hide.bs.modal', function() {
    document.forms[0].reset();
  });
}

function closeModalWindow(response) {
  $('#myModal').modal('hide');
  if (response == "Failed to update") {
    $('#alert_msg').text(response);
    $('#alert_show').removeClass('hide');
    $('#alert_show').removeClass('alert alert-success');
    $('#alert_show').addClass('alert alert-danger');
  } else {
    $('#alert_msg').text(response + " " + $("#process_name").val());
    $('#alert_show').removeClass('hide');
    $('#alert_show').removeClass('alert alert-danger');
    $('#alert_show').addClass('alert alert-success');
  }
  table.fnDraw(false);
}
// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}