var table;
var purchaseable_qty ;
var columnsMapping = {
  '7': 'stock_qty'
}

function toggleSelection(ele) {
  if (!enableGenerateBilling()) {
    $(ele).prop("checked", false);
  }
  $("input.input-checkbox").prop("checked", ele.checked);
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

function drawTable() {
  table.draw();
}

var tmpQuotationData = [];

function add_quotation() {

  var indent_items = [];
  var tableData = table.data();
  // get all the 'checked' input:checkbox(s)
  $("table.DTFC_Cloned input.input-checkbox:checked").each(function(i, ele) {
    indent_items.push(tableData[parseInt(ele.value)]);
  });
  if ($('input#selectAllCheckbox').is(':checked')) {
    indent_items = tableData;
  } else if (!indent_items.length) {
    indent_items = [];
  }

  if ($('#hd_material_id').val() == '') {
    alert('Error: Select Project and Material');
    return false;
  }

  if ($('#project_id').val() == '') {
    alert('Error: Select Project');
    return false;
  }

  if (!indent_items.length) {
    alert('Error: Select atleast one row to Bill');
    return false;
  }

  $.each(indent_items, function(i, value) {
    var keys = ["material_id", "indent_no",
      "material_name", "material_code",
      "project_name", "indent_item_qty",
      "indent_id", "indent_item_id","project_id"
    ];
    var data = {};
    for (var key in keys) {
      data[keys[key]] = value[keys[key]];
    }
    tmpQuotationData.push(data);
  });
  $('#exampleModal').modal('show');
  $('#exampleModal').on('shown.bs.modal', function(){
    $('span#material_name').html(tmpQuotationData[0]["material_name"]);
    $('span#material_code').html(tmpQuotationData[0]["material_code"]);
    $('span#project_name').html(tmpQuotationData[0]["project_name"]);
  });

  // flush the previously loaded modal content
  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $('form#add-bill-form')[0].reset();
  });
}

function isEmpty(data){
  data = (data || '').toString();
  return !!!data.length;
}

function generateQuotation() {
  var vendor = $("#ddl_quotation_vendor option:selected").val();
  var amount = $("#num_amount").val();
  var quote_no = $("#num_quotation_no").val();
  var remarks = $("#remarks").val();
  var received_date = $("#date_received_date").val();
  var CurrentDate = new Date();
  var SelectedDate = new Date(
  $("#date_received_date").val());
  if(SelectedDate > CurrentDate){
    alert("Error: Selected date can not be greater than today");
    return false;
  }
  if(vendor == "" || amount == "" || quote_no == "" || remarks == "" || received_date =="")
  {
    alert("Error: Please provide required fields");
    return false;
  }
  $.ajax({
    url: 'ajax/stock_add_items_quotation.php',
    type: 'POST',
    data: {
      val: tmpQuotationData,
      vendor: vendor,
      amount: amount,
      quote_no: quote_no,
      remarks: remarks,
      received_date: received_date,
    },
    dataType: 'json',
    success: function(response) {
    $('#exampleModal').modal('hide');
    drawTable();
    }
  });
}

function submitItemsQuotation(quote_id) {
  $.ajax({
    url: 'project_add_bill_no.php',
    type: 'POST',
    data: {
      add_bill_no_submit: 'yes',
      manpower_id: manpower_id,
      billing_address: $('#billing_address').val(),
      remarks: $('#remarks').val()
    },
    success: function(bill_number) {
      $('#exampleModal').modal('hide');
      drawTable();
      setTimeout(function() {
        alert('Bill generated: ' + bill_number);
      }, 750);
    }
  });
}

function removeModal() {
  $(this).find('.modal-content').empty();
  $(this).data('bs.modal', null);
}

function hasValue(name) {
  var value = $(name).val();
  return (value !== '' && value.length);
}

function enableGenerateBilling() {
  return hasValue('#search_project') &&
    hasValue('#search_vendor') &&
    hasValue('#start_date') &&
    hasValue('#end_date') &&
    table.data().length;
}

function disable_checkbox(index) {
  $("input#input-checkbox" +index).attr("disabled", true);
  $("input#input-checkbox" +index).attr('title', 'Please update your stock percenatge');
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
    };
  // get hidden values saved from filters
  function getVars(key) {
    var element = document.getElementById(key);
    if (element) {
      return element.value;
    } else {
      return '';
    }
  }

  // set search filters' as hidden value
  function setVars(elements) {

    for (var element in elements) {
      if (element && elements[element].length) {
        var hiddenField1 = document.createElement("input");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("id", element);
        hiddenField1.setAttribute("value", elements[element]);
        document.body.appendChild(hiddenField1);
      }
    }
  }

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_quotation_datatable.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    drawCallback: function() {
      manpower = [];
      $("input#selectAllCheckbox").prop("checked", false);
      $("input.input-checkbox").prop("checked", false);
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "stock_approved_indent_items_list";
      aoData.project_id = $('#project_id').val();
      aoData.hd_material_id = $('#hd_material_id').val();
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnRowCallback: function(row, full, index) {
      $(row).attr('id', 'row_' + index);
      $.ajax({
       url: 'ajax/get_material_stock_and_percentage.php',
       data: "project_id=" + full['project_id'] + "&material_id=" + full['material_id'],
       dataType: 'json',
       success: function(stock_response) {
           $('span.stock_qty_' + index).html(stock_response["stock_qty"]);
           available_percentage = stock_response["purchaseable_qty"];
           stock_qty = stock_response["stock_qty"];
           var row_data = table.data()[index];
           var indent_qty = row_data.indent_item_qty ;
           available_qty = (indent_qty * (available_percentage/100));
           console.log(available_qty);
           // available_qty = (indent_qty - availabilty);
           if((stock_qty > available_qty) && (stock_qty != 0) && (available_percentage !=0))
           {
             disable_checkbox(index);
           }
         }
         });

    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 4
    },
    scrollX: true,
    columns: [{
        className: 'noVis',
        "orderable": false,
        "data": function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        "data": "indent_no",
        "orderable": false
      },
      {
        "data": "material_name",
        "orderable": false
      },
      {
        "data": "material_code",
        "orderable": false
      },
      {
        "data": "project_name",
        "orderable": false
      },
      {
        "data": "indent_item_qty",
        "orderable": false
      },
      {
        "data": "unit_name",
        "orderable": false
      },
      commonCellDefinition,
      {
        "data": "indent_by",
        "orderable": false
      },
      {
        orderable: false,
          data : function(data, type, full) {
              return moment(data.item_added_on).format('DD-MMM-YYYY');
          }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data, type, full) {
          if (!window.permissions.view) {
            return "***";
          }
          return '<a  href="#"><span id="view" class="glyphicon glyphicon-folder-open"></span></a>';
        }
      },
      {
        orderable: false,
        data: function(data, type, full) {
          if (!window.permissions.delete) {
            return "***";
          }
          return `<a style=color:red href=# ><span id="delete" class="glyphicon glyphicon-trash"></span></a>`;
        }
      },
    {
      "orderable": false,
      "className": 'noVis',
      "data": function() {
        if (!window.permissions.edit) {
          return "***";
        }
        return '<input type="checkbox" class="input-checkbox" id="input-checkbox' + arguments[3].row + '" value="' + arguments[3].row + '" >';
      }
    }
  ]
  });
  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.row(this).data();
  if (event.target.tagName == 'SPAN' && event.target.id == 'view') {
      showVendor(rowData['material_id']);
    }
    else {
      if (event.target.tagName == 'SPAN' && event.target.id == 'delete') {
        delete_indent_item(rowData.indent_item_id, rowData.indent_id);
      }
    }
  });
});

function delete_indent_item(indent_item_id, indent_id) {
  var ok = confirm("Are you sure you want to Delete from quotation?");
  if (ok) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          drawTable();
        }
      }
    }
    xmlhttp.open("POST", "ajax/stock_delete_indent_items_from_quotation.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("indent_item_id=" + indent_item_id + "&indent_id=" + indent_id + "&action=0");
  }
}

function showVendor(material_id) {
  $('#myModal1').modal({
    remote: 'stock_get_vendor_item_mapping_list.php?material_id=' + material_id
  });

  $('#myModal1').on('hidden.bs.modal', function(event) {
    $(this).find('.modal-content').empty();
    $(this).data('bs.modal', null);
  });
}
