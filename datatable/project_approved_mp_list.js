var table;

function drawTable() {
  table.api().draw();
}

function createToolTip(str, length) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

var columns = [{
    className: 'noVis',
    "orderable": false,
    "data": function() {
      return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (type === 'export') {
        return data.project_manpower_agency_name;
      }
      return createToolTip(data.project_manpower_agency_name);
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (type === 'export') {
        return data.project_master_name;
      }
      return createToolTip(data.project_master_name);
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (type === 'export') {
        return data.project_process_master_name;
      }
      return createToolTip(data.project_process_master_name);
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (type === 'export') {
        return data.project_task_master_name;
      }
      return createToolTip(data.project_task_master_name);
    }
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (data.project_task_actual_manpower_road_id == 'No Roads') {
        return 'No Roads';
      }
      return data.project_site_location_mapping_master_name;
    }
  },
  {
    "orderable": false,
    "data": `project_task_actual_manpower_work_type`
  },
  {
    "orderable": false,
    "data": function(data, type) {
      if (data.project_task_actual_manpower_work_type === 'Regular') {
        return data.project_task_actual_manpower_completion_percentage;
      } else {
        return data.project_task_actual_manpower_rework_completion;
      }
    }
  },
  {
    "orderable": false,
    "data": `project_task_actual_manpower_completed_msmrt`
  },
  // Men
  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_no_of_men;
    },
    "orderable": false
  },

  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_men_rate;
    },
    "orderable": false
  },

  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_no_of_men / 8;
    },
    "orderable": false
  },
  // Women
  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_no_of_women;
    },
    "orderable": false
  },
  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_women_rate;
    },
    "orderable": false
  },
  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_no_of_women / 8;
    },
    "orderable": false
  },
  // Mason
  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_no_of_mason;
    },
    "orderable": false
  },
  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_mason_rate;
    },
    "orderable": false
  },
  {
    "data": function(data, type) {
      return data.project_task_actual_manpower_no_of_mason / 8;
    },
    "orderable": false
  },
  {
    // No of People
    "orderable": false,
    "data": function(data, type) {
      return parseFloat(data.project_task_actual_manpower_no_of_men / 8) + parseFloat(data.project_task_actual_manpower_no_of_women / 8) + parseFloat(data.project_task_actual_manpower_no_of_mason / 8);
    }
  },
  {
    // Amount
    "orderable": false,
    "data": function(data, type) {
      return (parseFloat(data.project_task_actual_manpower_no_of_men) * parseFloat(data.project_task_actual_manpower_men_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_women) * parseFloat(data.project_task_actual_manpower_women_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_mason) * parseFloat(data.project_task_actual_manpower_mason_rate));
    }
  },
  {
    // Remarks
    "orderable": false,
    "data": function(data, type) {
      if (type === 'export') {
        return data.project_task_actual_manpower_remarks;
      }
      return createToolTip(data.project_task_actual_manpower_remarks);
    }
  },
  {
    "orderable": false,
    "data": `user_name`
  },
  {
    "orderable": true,
    "data": function(data, type) {
      return moment(data.project_task_actual_manpower_added_on).format('DD-MM-YYYY');
    }
  },
  {
    "orderable": true,
    "data": function(data, type) {
      return moment(data.project_task_actual_manpower_date).format('DD-MM-YYYY');
    }
  }
];

$(document).ready(function() {
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_approved_mp_list_datatable.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_task_actual_manpower_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    "fnCreatedRow": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      if ($('#search_project').val() != "" || $('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $(nRow).attr('data-row-index', iDisplayIndex);
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    scrollX: true,
    "columns": columns,
  });
});