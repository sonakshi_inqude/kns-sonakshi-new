function checkFileAttached(){
  $('#formImport').submit();
}

var table = null;
$(document).ready(function() {
  function createToolTip(str, length){
    var substr = (str.length <= 20)? str : str.substr(0, 20)+'...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="'+str+'">'+substr+'</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/project_email_datatable.php',
    fnRowCallback: function(row, full, index){
      $(row).attr('id', 'row_'+index);
    },
    fnServerParams: function(aoData) {
      aoData.search_project = $('#search_project').val();
      aoData.table = "project_email_details";
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
     aoData.sSearch = aoData.search.value;
   }
    },
    drawCallback: function(){
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function(){
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'project_email_details_project_name'
      },
      {
        orderable: false,
        data: 'project_email_details_to'
      },
      {
        orderable: false,
        data : function() {
          return '<a href=# onclick="showCc('+arguments[3].row+')">CC</a>';
        }
      },
      {
        orderable: false,
        data : function(data) {
          return '<a href=# onclick="showBcc('+arguments[3].row+')">BCC</button>';
        }
      },
      {
        orderable: false,
        data : function(data, type, full) {
          if ($('#edit_perm').val() == 0) {
            return '<a href="project_add_email_ui.php?project_id='+data.project_email_details_project_id+'" target=_blank> <span class="glyphicon glyphicon-pencil"></span></a>';

          }
          return '***';
        }
      }
    ]
  });
});
function showCc(row_id){
  var rowData = table.api().data()[row_id];
  $('#myModal').modal();
  $('#myModal').on('shown.bs.modal', function(){
  $("#email_cc").empty();
  $("#email_bcc").empty();
  var cc_string = rowData.project_email_details_cc.split(',');
   for(var cc_count=0; cc_count< cc_string.length; cc_count++)
   {
  	  $("#email_cc").append("<li>"+cc_string[cc_count]+"</li>");
   }
    $('span#project_name').html(rowData.project_email_details_project_name);
  });
}
function showBcc(row_id){
  var rowData = table.api().data()[row_id];
  $('#myModal_bcc').modal();
  $('#myModal_bcc').on('shown.bs.modal', function(){
  $("#email_bcc").empty();
  var bcc_string = rowData.project_email_details_bcc.split(',');
   for(var bcc_count=0; bcc_count< bcc_string.length; bcc_count++)
   {
  	  $("#email_bcc").append("<li>"+bcc_string[bcc_count]+"</li>");
   }
    $('span#project_name').html(rowData.project_email_details_project_name);
  });
}
