var table;

function tableDraw() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 20) ? str : str.substr(0, 20) + '...';
  return '<abbr data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

$(document).ready(function() {

  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_machine_vendor_master_name"
    },
    {
      "orderable": false,
      "data": "project_master_name"
    },
    {
      "orderable": false,
      "data": "project_process_master_name"
    },
    {
      "orderable": false,
      "data": "project_task_master_name"
    },
    {
      "orderable": false,
      "data": "project_uom_name"
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_work_type"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_machine_plan_road_id == "No Roads" ||
          data.project_task_actual_machine_plan_road_id == "") {
          return 'No Roads';
        }
        return data.project_site_location_mapping_master_name;
      }
    },
    {
      "orderable": false,
      "data": "project_machine_type"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type !== 'display') {
          return '';
        }
        return data.project_machine_master_name + " " + data.project_machine_master_id_number;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        var end_date;
        if (type !== 'display') {
          return '';
        }
        if (data.project_task_actual_machine_plan_end_date_time == "0000-00-00 00:00:00") {
          end_date = moment().format('x');
        } else {
          end_date = moment(data.project_task_actual_machine_plan_end_date_time).format('x');;
        }
        var diff_time = end_date - moment(data.project_task_actual_machine_plan_start_date_time).format('x');
        return (diff_time / (3600 * 1000)).toFixed(2);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        return Math.round(data.project_task_actual_machine_plan_off_time / 60);
      }
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_fuel_charges"
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_msmrt"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (data.project_task_actual_machine_work_type === 'Regular') {
          return data.project_task_actual_machine_completion;
        } else {
          return data.project_task_actual_machine_rework_completion;
        }
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        return Math.round(data.project_task_actual_machine_plan_additional_cost);
      }
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_bata"
    },
    {
      "orderable": false,
      "data": "project_task_actual_machine_issued_fuel"
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type === 'export') {
          return data.project_task_actual_machine_plan_remarks;
        }
        return createToolTip(data.project_task_actual_machine_plan_remarks);
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_task_actual_machine_plan_added_on).
        format('DD-MM-YYYY hh:mm:ss');
      }
    },
    {
      "orderable": false,
      "data": "user_name"
    },
    {
      "orderable": true,
      "orderData": [22, 23],
      "data": function(data, type) {
        return moment(data.project_task_actual_machine_plan_start_date_time).
        format('DD-MM-YYYY hh:mm:ss');
      }
    },
    {
      "orderable": true,
      "orderData": [23, 22],
      "data": function(data, type) {
        if (data.project_task_actual_machine_plan_end_date_time != "0000-00-00 00:00:00") {
          return moment(data.project_task_actual_machine_plan_end_date_time)
            .format('DD-MM-YYYY hh:mm:ss');
        } else {
          return '';
        }
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        var end_date;
        if (data.project_task_actual_machine_plan_end_date_time == "0000-00-00 00:00:00") {
          end_date = moment().format('x');
        } else {
          end_date = moment(data.project_task_actual_machine_plan_end_date_time).format('x');;
        }
        var diff_time = Math.round(end_date - moment(data.project_task_actual_machine_plan_start_date_time).format('x'));
        var eff_hrs = parseFloat((diff_time / (3600 * 1000))) - parseFloat(data.project_task_actual_machine_plan_off_time / 60);
        return (parseFloat(data.project_task_actual_machine_fuel_charges * eff_hrs) + parseFloat(data.project_task_actual_machine_bata) +
          parseFloat(data.project_task_actual_machine_plan_additional_cost) - parseFloat(data.project_task_actual_machine_issued_fuel)).toFixed(2);
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_edit_approved_actual_machine_list.php?plan_id=${data.project_task_actual_machine_plan_id}&task_id=${data.project_task_actual_machine_plan_task_id}>
          <span class="glyphicon glyphicon-pencil"></span></a>`;
        }
        return '***';
      }
    },
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_approved_machine_work.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_machine_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.search_process = $('#search_process').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, index, full) {
      if ($('#search_project').val() != "" || $('#search_vendor').val() != "" ||
        $('#start_date').val() != "" || $('#end_date').val() != "" ||
        $('#search_process').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
    },
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 4
    },
    scrollX: true,
    "columns": columns,
  });
});