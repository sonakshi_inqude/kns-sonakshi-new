var table;
var columnsMapping = {
  '2': 'project',
  '8': 'issued_amount',
  '9': 'balance_amount',
  '10': 'remarks',
  '11': 'payment_mode',
  '12': 'instrument_details',
  '13': 'payment_by',
  '14': 'payment_on'
}

function tableDraw() {
  table.draw();
}

function createToolTip(str) {
  var substr = (str.length <= 15) ? str : str.substr(0, 6) + '...';
  return '<abbr id="tool_tip" data-toggle="tooltip" data-placement="right" title="' + str + '">' + substr + '</abbr>'
}

function getCellValue() {
  var meta = arguments[3];
  var classname = columnsMapping[meta.col] + '_' + meta.row;
  if (arguments[1] === 'export') {
    if (columnsMapping[meta.col] == 'project' || meta.col > 9) {
      return $("tr").find('span.' + classname).children('abbr#tool_tip').attr('title');
    }
    return $("tr").find('span.' + classname).html();
  }
  return '<span class="' + classname + '">Loading..</span>';
}

$(document).ready(function() {
  var commonCellDefinition = {
    orderable: false,
    data: getCellValue
  }
  var columns = [{
      className: 'noVis',
      "orderable": false,
      "data": function() {
        return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
      }
    },
    {
      "orderable": false,
      "data": "project_manpower_agency_name"
    },
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        return data.project_actual_contract_payment_bill_no;
      }
    },
    {
      "orderable": false,
      "data": function(data, type) {
        if (type == 'export') {
          return data.stock_company_master_name;
        }
        return createToolTip(data.stock_company_master_name);
      }
    },

    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_from_date).
        format('DD-MM-YYYY');
      }
    },
    {
      "orderable": false,
      data: function(data, type) {
        return moment(data.project_actual_contract_payment_to_date).
        format('DD-MM-YYYY');
      }
    },
    {
      orderable: false,
      data: `project_actual_contract_deposit_amount`
    },
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    commonCellDefinition,
    {
      "orderable": false,
      "data": function(data, type) {
        if (window.permissions.edit) {
          return `<a target = _blank href=project_contract_weekly_print.php?contract_payment_id=${data.project_actual_contract_payment_id}&deposit_status=${data.project_actual_contract_payment_deposit_status}>
          <span class="glyphicon glyphicon-Print"></span></a>`;
        }
        return '***';
      }
    }
  ];

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_issued_contract_deposit_payment_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    buttons: [{
      extend: 'excelHtml5',
      text: 'Export',
      exportOptions: {
        orthogonal: 'export'
      }
    }, ],
    dom: 'lBfrtip',
    lengthMenu: [
      [10, 25, 50, 100, -1],
      [10, 25, 50, 100, "All"]
    ],
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "weekly_contract_payment_list";
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    fnCreatedRow: function(row, full, index) {
      if ($('#search_vendor').val() != "") {
        $("button.btn.btn-default.buttons-excel.buttons-html5").show();
      } else {
        $("button.btn.btn-default.buttons-excel.buttons-html5").hide();
      }
      $.ajax({
        url: 'ajax/get_contract_deposit_details.php',
        data: "payment_id=" + full.project_actual_contract_payment_id + "&payment_status=" + full.project_actual_contract_payment_deposit_status,
        success: function(response) {
          response = JSON.parse(response);
          $('span.' + 'project' + '_' + index).html(createToolTip(response.project));
          $('span.' + 'issued_amount' + '_' + index).html(response.issued_amount.toFixed(2));
          $('span.' + 'balance_amount' + '_' + index).html((full.project_actual_contract_deposit_amount - response.issued_amount).toFixed(2));
          $('span.' + 'payment_by' + '_' + index).html(createToolTip(response.payment_added_by));
          $('span.' + 'payment_on' + '_' + index).html(createToolTip(response.payment_added_on));
          $('span.' + 'instrument_details' + '_' + index).html(createToolTip(response.instrument_details));
          $('span.' + 'payment_mode' + '_' + index).html(createToolTip(response.payment_mode));
          $('span.' + 'remarks' + '_' + index).html(createToolTip(response.remarks));
        }
      });
    },
    fixedColumns: {
      leftColumns: 2,
      rightColumns: 3
    },
    scrollX: true,
    "columns": columns,
  });
});