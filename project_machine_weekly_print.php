<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

// Session Data
$user 		   = $_SESSION["loggedin_user"];
$role 		   = $_SESSION["loggedin_role"];
$loggedin_name = $_SESSION["loggedin_user_name"];

// Temp Data
$alert_type = -1;
$alert = "";

$machine_payment_id = $_REQUEST["machine_payment_id"];
$bata_status = $_REQUEST["bata_status"];

// Get billing details
$project_payment_machine_search_data = array('payment_machine_id'=>$machine_payment_id,"active"=>'1');
$machine_payment_sresult = i_get_project_payment_machine($project_payment_machine_search_data);

$project_payment_machine_mapping_search_data = array("payment_id"=>$machine_payment_id,"active"=>'1');
$machine_payment_mapping_list = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data); if ($machine_payment_mapping_list['status'] == SUCCESS) {
    $machine_payment_mapping_list_data = $machine_payment_mapping_list["data"];
    $added_by_user	 = $machine_payment_mapping_list_data[0]["project_task_actual_machine_plan_added_by"];
    $approved_by_user = $machine_payment_mapping_list_data[0]["project_task_actual_machine_plan_approved_by"];
    $checked_by_user = $machine_payment_mapping_list_data[0]["project_task_actual_machine_plan_checked_by"];
    $project_process_task_search_data = array('task_id'=>$machine_payment_mapping_list['data'][0]['project_task_actual_machine_plan_task_id']);
    $task_sresult = i_get_project_process_task($project_process_task_search_data);
    $project_name = $task_sresult['data'][0]['project_master_name'];
} else {
    $project_name = '';
}

if ($machine_payment_sresult["status"] == SUCCESS) {
    $machine_payment_list_data = $machine_payment_sresult["data"];
    $from_date 		 = date("d-M-Y", strtotime($machine_payment_list_data[0]["project_payment_machine_from_date"]));
    $to_date 		 = date("d-M-Y", strtotime($machine_payment_list_data[0]["project_payment_machine_to_date"]));
    $bill_date	     = date("d-M-Y", strtotime($machine_payment_list_data[0]["project_payment_machine_approved_on"]));
    $bill_no 		 = $machine_payment_list_data[0]["project_payment_machine_bill_no"];
    $billing_company = $machine_payment_list_data[0]["stock_company_master_name"];
    $billing_address = $machine_payment_list_data[0]["stock_company_master_address"];
    $billing_remarks = $machine_payment_list_data[0]["project_payment_machine_remarks"];
    $vendor 		 = $machine_payment_list_data[0]["project_machine_vendor_master_name"];
} else {
    $alert = $alert."Alert: ".$contract_payment_work_list["data"];
}

//Get Added by
$added_by_result = i_get_user_list($added_by_user, '', '', '', '', '');
if ($added_by_result["status"] == SUCCESS) {
    $added_by = $added_by_result["data"][0]["user_name"];
}

//Get Approved by
$approved_by_result = i_get_user_list($approved_by_user, '', '', '', '', '');
if ($approved_by_result["status"] == SUCCESS) {
    $approved_by = $approved_by_result["data"][0]["user_name"];
}

//Get Checked by
$checked_by_result = i_get_user_list($checked_by_user, '', '', '', '', '');
if ($checked_by_result["status"] == SUCCESS) {
    $checked_by = $checked_by_result["data"][0]["user_name"];
}
$machine_print = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Machine Bill</title>
</head>

<body>

<table width="100%" border="1" cellpadding="5" frame="box" rules="none" style="border:2px solid #000;">

  <tr>
    <td colspan="8"  align="center"><h2 style="margin:0px;">'.$billing_company.',</h2>
    <span style="font-size:12px;">'.$billing_address.'</span>

    </td>
    </tr>

  <tr>
    <td colspan="8" align="center"><strong><u>MACHINERY UTILIZATION DETAILS (Weekly Report)</u></strong></td>
    </tr>
  <tr>
    <td colspan="2" width="25%">Project Name</td>
    <td colspan="2" width="25%">'.$project_name.'</td>
    <td colspan="2" width="25%">Bill No</td>
	<td colspan="2" width="25%">'.$bill_no.'</td>
    </tr>
  <tr>
    <td colspan="2">Project Location</td>
    <td colspan="2">&nbsp;</td>
    <td colspan="2">Vendor</td>
	<td colspan="2">'.$vendor.'</td>
  </tr>
  <tr>
    <td colspan="2" valign="top">Project Engineer</td>
    <td colspan="2">&nbsp;</td>
    <td colspan="2">Machine</td>
	<td colspan="2">'.$machine_payment_mapping_list['data'][0]['project_machine_master_name'].'-'.$machine_payment_mapping_list['data'][0]['project_machine_master_id_number'].'</td>
  </tr>
  <tr>
	<td colspan="2" valign="top">Site Engineer</td>
    <td colspan="2">&nbsp;</td>
    <td colspan="2">Period</td>
	<td colspan="2">'.date('d-M-Y', strtotime($from_date)).' TO '.date('d-M-Y', strtotime($to_date)).'</td>
  </tr>';

$machine_print = $machine_print.'
 <tr>
 <td colspan="14">
 <table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; border-top:3px solid #000">
 <tr>
   <td width="3%" align="center"><strong>Sl.no</strong></td>
   <td width="27%" align="center"><strong>Process & Task</strong></td>
   <td width="27%" align="center"><strong>Road</strong></td>
   <td width="27%" align="center"><strong>Measurement</strong></td>
   <td width="27%" align="center"><strong>UOM</strong></td>
   <td width="10%" align="center"><strong>Starting Time</strong></td>
   <td width="10%" align="center"><strong>Ending Time</strong></td>
   <td width="6%" align="center"><strong>Off Time</strong></td>
   <td width="7%" align="center"><strong>Total Hrs</strong></td>
   <td width="7%" align="center"><strong>Rate/PH</strong></td>
   <td width="7%" align="center"><strong>Bata</strong></td>
   <td width="7%" align="center"><strong>Addnl Amount</strong></td>
   <td width="7%" align="center"><strong>Fuel</strong></td>
   <td width="9%" align="center"><strong>Amount</strong></td>
   </tr>';
   $grand_total = 0;
   $bata_total = 0;
for ($count = 0; $count < count($machine_payment_mapping_list['data']); $count++) {
    $get_details_data = i_get_details($machine_payment_mapping_list['data'][$count]["project_task_actual_machine_work_type"], $machine_payment_mapping_list['data'][$count]["project_task_actual_machine_plan_task_id"], $machine_payment_mapping_list['data'][$count]["project_task_actual_machine_plan_road_id"]);
    $road_name = $get_details_data["road"];
    $uom = $get_details_data["uom"];
    // Get task details
    $project_process_task_search_data = array('task_id'=>$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_task_id']);
    $task_sresult = i_get_project_process_task($project_process_task_search_data);
    $total_hours = (strtotime($machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_end_date_time']) - strtotime($machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_start_date_time']))/3600;

    $off_hours = $machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_off_time']/60;

    $working_hours = round(($total_hours - $off_hours), 2);

    $machine_print = $machine_print.'
	<tr>
	<td width="3%" align="center"><strong>'.($count + 1).'</strong></td>
	<td width="27%" align="center"><strong>Process: '.$task_sresult['data'][0]['project_process_master_name'].' || Task: '.$task_sresult['data'][0]['project_task_master_name'].'</strong></td>
	<td width="10%" align="center"><strong>'.$road_name.'</strong></td>
	<td width="10%" align="center"><strong>'.number_format((float)$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_msmrt'], 2, '.', '').'</strong></td>
	<td width="10%" align="center"><strong>'.$uom.'</strong></td>
	<td width="10%" align="center"><strong>'.date('d-M-Y H:i:s', strtotime($machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_start_date_time'])).'</strong></td>
  <td width="10%" align="center"><strong>'.date('d-M-Y H:i:s', strtotime($machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_end_date_time'])).'</strong></td>
	<td width="6%" align="center"><strong>'.$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_off_time'].'</strong></td>
	<td width="7%" align="center"><strong>'.number_format((float)$working_hours, 2, '.', '').'</strong></td>
	<td width="7%" align="center"><strong>'.number_format((float)$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_fuel_charges'], 2, '.', '').'</strong></td>
	<td width="7%" align="center"><strong>'.number_format((float)$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_bata'], 2, '.', '').'</strong></td>
	<td width="7%" align="center"><strong>'.number_format((float)$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_additional_cost'], 2, '.', '').'</strong></td>
	<td width="7%" align="center"><strong>'.number_format((float)$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_issued_fuel'], 2, '.', '').'</strong></td>
	<td width="9%" align="center"><strong>'.number_format((float)(($machine_payment_mapping_list['data'][$count]['project_task_actual_machine_fuel_charges'] * $working_hours) + $machine_payment_mapping_list['data'][$count]['project_task_actual_machine_bata'] + $machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_additional_cost'] - $machine_payment_mapping_list['data'][$count]['project_task_actual_machine_issued_fuel']), 2, '.', '').'</strong></td>
	</tr>';
    $bata_total  = $bata_total+$machine_payment_mapping_list['data'][$count]['project_task_actual_machine_bata'];
    $grand_total = $grand_total + (($machine_payment_mapping_list['data'][$count]['project_task_actual_machine_fuel_charges'] * $working_hours) + $machine_payment_mapping_list['data'][$count]['project_task_actual_machine_bata'] + $machine_payment_mapping_list['data'][$count]['project_task_actual_machine_plan_additional_cost'] - $machine_payment_mapping_list['data'][$count]['project_task_actual_machine_issued_fuel']);
    $machine_print = $machine_print.'<tr><td colspan="14">REMARKS: '.$machine_payment_mapping_list['data'][$count]["project_task_actual_machine_plan_remarks"].'</td></tr>';
}
if($bata_status=='Bill'){
  $total_amount=$grand_total;
  $grand_total=$grand_total-$bata_total;
  $amount_in_words = convert_num_to_words($grand_total);
  $machine_print = $machine_print.'
     <tr>
       <td colspan="11" align="center"><strong>GRAND TOTAL</strong></td>
       <td style=min-width:380px><strong>grand total = total amount - bata amount</strong></td>
       <td style=min-width:110px><strong>'.number_format((float)$total_amount) . - $bata_total .' =</strong></td>
       <td align="center"><strong>'.number_format((float)$grand_total).'</strong></td>
     </tr>
   </table>
   </td>
   </tr>';
}
else{
  $amount_in_words = convert_num_to_words($grand_total);
$machine_print = $machine_print.'
   <tr>
     <td colspan="12" align="center"><strong>GRAND TOTAL</strong></td>
     <td>&nbsp;</td>
     <td align="center"><strong>'.number_format((float)$grand_total).'</strong></td>
   </tr>
 </table>
 </td>

 </tr>';
}

$machine_print = $machine_print.'
<tr>
 <td colspan="8">
   <strong>Amount in words: '.$amount_in_words.'</strong>
 </td>
</tr><tr> <td colspan="8">   <strong>Remarks: '.$billing_remarks.'</strong> </td></tr>
<tr>
  <td colspan="2" align="center">(Prepared by)</td>
  <td colspan="2" align="center">(Checked by)</td>
  <td colspan="2" align="center">(Verified By)</td>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" align="center">'.$added_by.'</td>
  <td colspan="2" align="center">'.$checked_by.'</td>
  <td colspan="2" align="center">'.$approved_by.'</td>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
  <td colspan="2" align="center">&nbsp;</td>
  <td colspan="2" align="center">&nbsp;</td>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
  <td colspan="2" align="center">&nbsp;</td>
  <td colspan="2" align="center">&nbsp;</td>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" align="center">Head-Projects</td>
  <td colspan="2" align="center">Accountant</td>
  <td colspan="2" align="center">&nbsp;</td>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
</table>



</body>
</html>';
//$mpdf = new mPDF();
//$mpdf->WriteHTML($machine_print);
//$mpdf->Output('Machine Utilization Details.pdf','I');
echo $machine_print;
