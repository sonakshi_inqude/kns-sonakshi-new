<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_files.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');

/*
PURPOSE : To add file
INPUT 	: BD File ID, File Type, Survey Number, land Owner, PAN Number, Extent, Village, Start Date, No of Days, Document Path
OUTPUT 	: File ID or Error Details, success or failure message
BY 		: Nitin Kashyap
REMARKS : A record is considered duplicate if survey number, village and and number are same
*/
function i_add_file($bd_file_id,$type,$survey_no,$land_owner,$pan,$extent,$village,$start_date,$num_days,$doc_path,$deal_amount,$brokerage,$added_by)
{
	$file_sresult = db_get_legal_file_list('','','',$survey_no,'',$pan,'',$village,'','','','','','');
	
	if($file_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = FAILURE;
        $return["data"]   = "A file already exists for this survey number and land owner details"; 
    }
    else
    {
		// Get the file number
		$file_num_sresult = db_get_legal_file_list('','','','','','','','','','','','','','desc_one');
		
		if($file_num_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$file_num_old = $file_num_sresult["data"][0]["file_number"];
		}
		else
		{
			$file_num_old = "";
		}
		
		$file_number_new = get_file_num($file_num_old);
		
	    // Add the file
		$file_iresult = db_add_file($file_number_new,$bd_file_id,$start_date,$num_days,$type,$survey_no,$land_owner,$pan,$extent,$village,$doc_path,$deal_amount,$brokerage,$added_by);
		
		if($file_iresult["status"] == SUCCESS)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = "The file with file number <strong>$file_number_new<strong> was successfully created"; 
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "There was an internal error. Please try again later!"; 
		}
    }
	
	return $return;
}

/*
PURPOSE : To get file type list
INPUT 	: Process Name, Module
OUTPUT 	: Process Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_file_type_list($type_name,$type_active)
{
	$file_type_sresult = db_get_legal_file_type_list($type_name,$type_active,'','','');
	
	if($file_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $file_type_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No file type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get file list
INPUT 	: File Number, BD File ID, Type, Survey Number, Land Owner, PAN, Extent, Village, Process, Task, File ID, BD Project ID, Bulk Process ID, Bulk Task ID, Process Type
OUTPUT 	: Process Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_file_list($file_number,$bd_file_id,$file_type,$survey_number,$land_owner,$pan_number,$extent,$village,$process,$task,$file_id="",$bd_project_id="",$bprocess="",$btask="",$process_master="")
{
	$files_sresult = db_get_legal_file_list($file_number,$bd_file_id,$file_type,$survey_number,$land_owner,$pan_number,$extent,$village,$process,$task,'','','','desc_list',$file_id,'1',$bd_project_id,$bprocess,$btask,$process_master);
	
	if($files_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $files_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No file added. Please contact the HOD or system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : Update last process and last task details of a file
INPUT 	: Task ID, Process Plan ID, File ID
OUTPUT 	: Success or Failure message
BY 		: Nitin Kashyap
*/
function i_update_file_details($task_id,$process_plan_id,$file_id)
{
	// Initialize
	$file_update = true;

	// check if there are any more pending tasks for this process
	$task_list = i_get_task_plan_list('','',$process_plan_id,'','0000-00-00','','','asc');
	
	// if yes, update the next task and its planned date in file
	if($task_list["status"] == SUCCESS)
	{
		$current_task_list = i_get_task_plan_list($task_id,'','','','','','','');
		$current_task_date = date("Y-m-d",strtotime($current_task_list["data"][0]["task_plan_actual_end_date"]." + 1 days"));
	
		$current_task      = $task_list["data"][0]["task_plan_legal_id"];
		
		$process_data = db_get_legal_process_plan_list($process_plan_id,'','','','','','','','','','','','','');
		$current_process = $process_data["data"][0]["process_plan_legal_id"];		
		$current_process_date = $process_data["data"][0]["process_plan_legal_start_date"];
	}
	// else, update the next process and its first task in file
	else if($task_list["status"] == FAILURE)
	{
		// Update this process as complete
		$process_update = i_update_project_plan($process_plan_id,'1');
		
		$current_task_list = i_get_task_plan_list($task_id,'','','','','','','');
		$current_task_date = date("Y-m-d",strtotime($current_task_list["data"][0]["task_plan_actual_end_date"]." + 1 days"));
		
		// Get the next process
		$process_latest = i_get_legal_process_plan_list('',$file_id,'','','','','','','','0','asc');			
		
		if($process_latest["status"] == SUCCESS)
		{
			$current_process = $process_latest["data"][0]["process_plan_legal_id"];	
			$current_process_date = $current_task_date;
			
			$ftask_list = i_get_task_plan_list('','',$process_latest["data"][0]["process_plan_legal_id"],'','0000-00-00','','','asc');
			$current_task = $ftask_list["data"][0]["task_plan_legal_id"];	
		}
		else
		{
			$file_update = false;
		}
	}
	
	if($file_update == true)
	{
		$result = db_update_file($file_id,$current_process,$current_process_date,$current_task,$current_task_date);
	}	
	else
	{
		$result["data"]   = "";
		$result["status"] = SUCCESS;
	}
	
	return $result;
}

/*
PURPOSE : Update last bulk process and last bulk task details of a file
INPUT 	: Task ID, Process Plan ID, File ID
OUTPUT 	: Success or Failure message
BY 		: Nitin Kashyap
*/
function i_update_file_bulk_details($task_id,$process_plan_id,$file_id)
{
	// Initialize
	$file_update = true;

	// check if there are any more pending tasks for this process
	$task_list = i_get_task_plan_list('','','','','0000-00-00','','','asc',$process_plan_id);
	
	// if yes, update the next task and its planned date in file
	if($task_list["status"] == SUCCESS)
	{
		$current_task_list = i_get_task_plan_list($task_id,'','','','','','','');
		$current_task_date = date("Y-m-d",strtotime($current_task_list["data"][0]["task_plan_actual_end_date"]." + 1 days"));		
	
		$current_task      = $task_list["data"][0]["task_plan_legal_id"];
		
		$legal_bulk_search_data = array("bulk_process_id"=>$process_plan_id);
		$process_data = db_get_legal_bulk_process($legal_bulk_search_data);
		$current_process = $process_data["data"][0]["legal_bulk_process_id"];		
		$current_process_date = $process_data["data"][0]["legal_process_start_date"];
	}
	// else, update the next process and its first task in file
	else if($task_list["status"] == FAILURE)
	{
		// Update this process as complete
		$process_update = i_update_bulk_project_plan($process_plan_id,'1');
		
		$file_update = false;
	}
	
	if($file_update == true)
	{
		$result = db_update_file_for_bulk($file_id,$current_process,$current_process_date,$current_task,$current_task_date);
	}	
	else
	{
		$result["data"]   = "";
		$result["status"] = SUCCESS;
	}
	
	return $result;
}

/*
PURPOSE : To handover file to project
INPUT 	: File Type, Survey Number, land Owner, PAN Number, Extent, Village, Start Date, No of Days, Document Path
OUTPUT 	: File ID or Error Details, success or failure message
BY 		: Nitin Kashyap
REMARKS : A record is considered duplicate if survey number, village and and number are same
*/
function i_add_file_project_handover($file_id,$remarks,$added_by)
{
	$file_project_iresult = db_add_file_project($file_id,$remarks,$added_by);
	
	if($file_project_iresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "File successfully handed over to project"; 
    }
    else
    {
		$return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

function i_get_file_details($file_id)
{
	$file_sresult = db_get_file_details($file_id);
	
	if($file_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $file_sresult["data"]; 
    }
    else
    {
		$return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

function i_get_file_handover_details($file_id)
{
	$file_handover_sresult = db_get_file_handover_details($file_id);
	
	if($file_handover_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $file_handover_sresult["data"]; 
    }
    else
    {
		$return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add file payment details
INPUT 	: Request ID, File ID, Payment Date, Amount, Done to, Remarks, Added By
OUTPUT 	: File Payment ID or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_file_payment($request_id,$file_id,$file_payment_type,$file_payment_payment_mode,$file_payment_cheque_dd_no,$file_payment_bank_name,$payment_date,$amount,$done_to,$remarks,$added_by)
{
	// Initialize
	$payment_exceeds = false;
	
	// Get BD file ID
	$file_sresult = i_get_file_details($file_id);
	$bd_file_id = $file_sresult['data']['file_bd_file_id'];
	
	// Get total amount already paid - for loop
	$total_already_paid = 0;
	$amount_paid_sresult = db_get_file_payment_list($file_id,$file_payment_type,'','','','','','','','','','');
	
	if($amount_paid_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
	{
		for($count = 0; $count < count($amount_paid_sresult['data']); $count++)
		{
			if(($amount_paid_sresult["data"][$count]['file_payment_payment_status'] == '0') || ($amount_paid_sresult["data"][$count]['file_payment_payment_status'] == '1'))
			{
				$total_already_paid = $total_already_paid + $amount_paid_sresult['data'][$count]['file_payment_amount'];
			}
		}
	}
	else
	{
		$total_already_paid = 0;
	}
	
	switch($file_payment_type)
	{
		case(1): // Get total amount to be paid
		$bd_file_sresult = i_get_bd_files_list($bd_file_id,'','','','','','','');
		$total_amount_payable = $bd_file_sresult['data'][0]['bd_file_land_cost'];

		if($total_amount_payable >= ($total_already_paid + $amount))
		{				
			$payment_exceeds = false;
		}
		else
		{
			$payment_exceeds = true;
		}	
		break;
			
		case(2): 
		// Get total amount to be paid
		$bd_file_sresult = i_get_bd_files_list($bd_file_id,'','','','','','','');
		$total_amount_payable = (($bd_file_sresult['data'][0]['bd_file_land_cost'])*CHANGE_OF_LAND_USE)/100;

		if($total_amount_payable >= ($total_already_paid + $amount))
		{
			$payment_exceeds = false;
		}
		else
		{
			$payment_exceeds = true;
		}
		break;			
			
		case(3) :  // Get total amount to be paid
		$bd_file_sresult = i_get_bd_files_list($bd_file_id,'','','','','','','');
		$total_amount_payable = ($bd_file_sresult['data'][0]['bd_file_land_cost']*CONVERSION_FEE)/100;

		if($total_amount_payable >= ($total_already_paid + $amount))
		{
			$payment_exceeds = false;
		}
		else
		{
			$payment_exceeds = true;
		}
		break;			
			
			
		case(4) : // Get total amount to be paid
		$bd_brokerage_sresult = i_get_file_details($file_id);
		$total_amount_payable = $bd_brokerage_sresult['data']['file_brokerage_charges'];			
		if($total_amount_payable >= ($total_already_paid + $amount))
		{
			$payment_exceeds = false;
		}
		else
		{
			$payment_exceeds = true;
		}
		break;
			
		case(5) :  // Get total amount to be paid
		$bd_file_sresult = i_get_bd_files_list($bd_file_id,'','','','','','','');
		$total_amount_payable = ($bd_file_sresult['data'][0]['bd_file_land_cost']*STAMP_DUTY)/100;

		if($total_amount_payable >= ($total_already_paid + $amount))
		{
			$payment_exceeds = false;
		}
		else
		{
			$payment_exceeds = true;
		}
		break;
			
		case(6) :  // Get total amount to be paid
		$bd_file_sresult = i_get_bd_files_list($bd_file_id,'','','','','','','');
		$total_amount_payable = ($bd_file_sresult['data'][0]['bd_file_land_cost']*REGISTARTION_FEE)/100;

		if($total_amount_payable >= ($total_already_paid + $amount))
		{
			$payment_exceeds = false;
		}
		else
		{
			$payment_exceeds = true;
		}
		break;
			
		case(7) : // Get total amount to be paid
		$bd_file_sresult = i_get_bd_files_list($bd_file_id,'','','','','','','');
		$total_amount_payable = ($bd_file_sresult['data'][0]['bd_file_land_cost']*OTHER_EXPENSION)/100;

		if($total_amount_payable >= ($total_already_paid + $amount))
		{
			$payment_exceeds = false;
		}
		else
		{
			$payment_exceeds = true;
		}
		break;			
	}
	
	if($payment_exceeds == false)
	{
		$file_payment_iresult = db_add_file_payment($request_id,$file_id,$file_payment_type,$file_payment_payment_mode,$file_payment_cheque_dd_no,$file_payment_bank_name,$payment_date,$amount,$done_to,$remarks,$added_by);
		
		if($file_payment_iresult['status'] == SUCCESS)
		{
			$return['status'] = SUCCESS;
			$return['data']   = 'Payment Details successfully added!';
		}
		else
		{
			$return['status'] = FAILURE;
			$return['data']   = 'There was an internal error. Please try again later!';
		}
	}
	else
	{
		$return['status'] = FAILURE;
		$return['data']   = 'Total payment exceeds what is supposed to be spent';
	}
	
	return $return;
}

/*
PURPOSE : To Update Status
INPUT 	: Payment Status,File ID
OUTPUT 	: Status
BY 		: Sonakshi D
*/
function db_update_file_payment($file_payment_payment_status,$file_payment_id)
{
	$file_uquery="UPDATE file_payment SET file_payment_payment_status=:file_payment_payment_status where file_payment_id=:file_payment_id";
	
	try{
		$dbconnection = get_conn_handle();
		$file_payment_ustatement=$dbconnection->prepare($file_uquery);
		
		$file_udata=array(':file_payment_payment_status'=>$file_payment_payment_status,':file_payment_id'=>$file_payment_id);
		
		$dbconnection->beginTransaction();
		$file_payment_ustatement->execute($file_udata);
		$file_payment_id = $dbconnection->lastInsertId();
		$dbconnection->commit(); 
        
		$return["status"] = SUCCESS;
		$return["data"]   = $file_payment_id;		
	}
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "error";
    }
    
    return $return;
}

/*
PURPOSE : To get bank list
INPUT 	: Bank Name, Active
OUTPUT 	: Bank List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_bank_list($bank_name,$active)
{
	$bank_sresult = db_get_bank_list($bank_name,$active,'','','');
	
	if($bank_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $bank_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No bank added. Please contact the system admin"; 
    }
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To get payment mode list
// Input  : Payment Mode, Active
// Output : Payment Mode list
function i_get_payment_mode_list($payment_mode,$active)
{
	// get filing mode list
	$payment_mode_sresult = db_get_payment_mode_list($payment_mode,$active,'','','');
	
	if($payment_mode_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $payment_mode_sresult["data"];
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "";
	}
	
	return $return;
}

/*
PURPOSE : To get file payment list
INPUT 	: File ID, Pay Date
OUTPUT 	: File Payment List
BY 		: Nitin Kashyap
*/
function i_get_file_payment_list($file_id,$pay_type,$request_id='')
{
	$file_pay_sresult = db_get_file_payment_list($file_id,$pay_type,'','','','','','','','','','',$request_id);
	
	if($file_pay_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $file_pay_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No file type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update file
INPUT 	: File ID, Pay Date
OUTPUT 	: Status
BY 		: Nitin Kashyap
*/
function i_update_file($file,$type,$survey_no,$land_owner,$pan,$extent,$village,$start_date,$num_days,$deal_amount,$brokerage)
{
	$file_uresult = db_update_file_details($file,$type,$survey_no,$land_owner,$pan,$extent,$village,$start_date,$num_days,$deal_amount,$brokerage);
	
	if($file_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "File Details Updated Successfully"; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No file type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To delee file
INPUT 	: File ID
OUTPUT 	: Status
BY 		: Nitin Kashyap
*/
function i_delete_file($file)
{
	$file_uresult = db_enable_disable_file($file,$type,'0');
	
	if($file_uresult['status'] == SUCCESS)
    {				
		$process_dresult = i_delete_project_plan('',$file);
		if($process_dresult["status"] == SUCCESS)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = "File Deleted Successfully"; 
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "File was deleted, but all the processes under it were not deleted. Please contact the admin";
		}
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "File was not deleted. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add File Notes
INPUT 	: File ID, File Notes, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_file_notes($file_id,$file_notes,$file_task,$added_by)
{
	$file_notes_iresult = db_add_file_notes($file_id,$file_notes,$file_task,$added_by);
	
	if($file_notes_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Note Successfully Added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To get File Notes list
INPUT 	: File ID, File Notes
OUTPUT 	: File Notes List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_file_notes_list($file_id,$file_notes,$added_by)
{
	$file_notes_sresult = db_get_file_notes($file_id,$file_notes,$added_by,'','');
	
	if($file_notes_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $file_notes_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Notes added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update File Notes Task
INPUT 	: File Notes ID, Added By
OUTPUT 	: Success or Failure Message
BY 		: Sonakshi D
*/
function i_update_file_notes($file_notes_id,$task)
{
	$file_notes_details = array("task"=>$task);
	$file_notes_uresult = db_update_file_notes($file_notes_id,$file_notes_details);
	if($file_notes_uresult["status"] == SUCCESS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = "Task updated successfully";
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "There was an internal error. Please try again later!";
	}
	
	return $return;
}

/* PRIVATE FUNCTIONS - START */
function get_file_num($old_file_num)
{
	if($old_file_num != "")
	{
		$file_num_array = explode(FILE_ID_SEPARATOR,$old_file_num);	
	
	    $file_id = $file_num_array[3];
	}
	else
	{
		$file_id = 0;
	}
	
	$new_file_id = $file_id + 1;
	
	$new_file_num = FILE_ID_ORG.FILE_ID_SEPARATOR.date("Y").FILE_ID_SEPARATOR.date("m").FILE_ID_SEPARATOR.$new_file_id;
	
	return $new_file_num;
}

// Functions
function upload($file_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=date("YmdHis")."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
/* PRIVATE FUNCTIONS - END */
?>