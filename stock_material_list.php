<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_grn_accont_list.php

CREATED ON	: 29-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of grn account for customer withdrawals

*/



/*

TBD:

*/
$_SESSION['module'] = 'Stock Masters';


/* DEFINES - START */
define('MATERIAL_STOCK_FUNC_ID','153');
/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{


	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];


	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',MATERIAL_STOCK_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',MATERIAL_STOCK_FUNC_ID,'3','1');



	if(isset($_POST["hd_material_id"]))

	{

		$search_material      = $_POST["hd_material_id"];

		$search_material_name = $_POST["stxt_material"];

		$project              = $_POST["ddl_project"];
	}

	else

	{

		$search_material 	  = "";

		$search_material_name = "";

		$project		 	  = "-1";
	}



	// Temp data

	// Get grn account modes already added

	$material_stock_search_data = array("active"=>'1');

	if($search_material != "")

	{

		$material_stock_search_data["material_id"] = $search_material;

	}

	if($project != "")
	{
		$material_stock_search_data["project"] = $project;
	}

	$material_stock_list = i_get_material_stock($material_stock_search_data);

	if($material_stock_list['status'] == SUCCESS)

	{

		$material_stock_list_data = $material_stock_list['data'];

	}


	// Get Project List
	$stock_project_search_data = array();
	$project_result_list = i_get_project_list($stock_project_search_data);
	if($project_result_list['status'] == SUCCESS)
	{
		$project_result_list_data = $project_result_list['data'];
	}

	else

	{
		$alert = $project_result_list["data"];
		$alert_type = 0;

	}

}

else

{

	header("location:login.php");

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Stock Material List</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">

    <link href="css/style1.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>





<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">



          <div class="span6" style="width:100%;">



          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Stock Material List&nbsp;&nbsp;&nbsp;&nbsp;Total Quantity: <span id="span_total_qty"><i>Calculating</i></span></h3>

            </div>

			<div class="widget-header" style="height:50px; padding-top:10px;">

			  <form method="post" id="file_search_form" action="stock_material_list.php">

			  <input type="hidden" name="hd_material_id" id="hd_material_id" value="<?php echo $search_material; ?>" />

			  <span style="padding-right:20px;">

					<input type="text" name="stxt_material" class="span6" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search by material name or code" value="<?php echo $search_material_name; ?>" />

					<div id="search_results" class="dropdown-content"></div>

			  </span>

			  <span style="padding-right:20px;">
			  <select name="ddl_project">
				<option value="">- - Select Project - -</option>
				<?php
				for($count = 0; $count < count($project_result_list_data); $count++)
				{

				?>
				<option value="<?php echo $project_result_list_data[$count]["stock_project_id"]; ?>"<?php if($project_result_list_data[$count]["stock_project_id"] == $project){?> selected="selected" <?php } ?>><?php echo $project_result_list_data[$count]["stock_project_name"]; ?></option>
				<?php

				}

				?>

			  </select>

			  </span>

			  <input type="submit" name="file_search_submit" />

			  </form>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">



              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				    <th style="word-wrap:break-word;">SL No</th>

					<th style="word-wrap:break-word;">Material</th>

					<th style="word-wrap:break-word;">Material Code</th>

					<th style="word-wrap:break-word;">Project</th>

					<th style="word-wrap:break-word;">Quantity</th>

					<th style="word-wrap:break-word;">Unit Of Measure</th>

					<th style="word-wrap:break-word;">Re order Level</th>

					<th style="word-wrap:break-word;">Last Updated by</th>

					<th style="word-wrap:break-word;">Last Updated On</th>

					<th style="word-wrap:break-word;" colspan="2" style="text-align:center;">Action</th>

				</tr>

				</thead>

				<tbody>

				<?php
				if($material_stock_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_quantity = 0;
					for($count = 0; $count < count($material_stock_list_data); $count++)
					{
							$sl_no++;
							$total_quantity = $total_quantity + $material_stock_list_data[$count]["material_stock_quantity"];
							$stock_material_search_data = array("material_id"=>$material_stock_list_data[$count]["material_id"]);
							$material_master_list_data = i_get_stock_material_master_list($stock_material_search_data);
							if($material_master_list_data["status"] == SUCCESS)
							{
								$uom = $material_master_list_data["data"][0]["stock_unit_name"];
							}

						?>

						<tr>

						<td><?php echo $sl_no; ?></td>

						<td><?php echo $material_stock_list_data[$count]["stock_material_name"]; ?></td>

						<td><?php echo $material_stock_list_data[$count]["stock_material_code"]; ?></td>

						<td><?php echo $material_stock_list_data[$count]["stock_project_name"]; ?></td>

						<td><?php echo $material_stock_list_data[$count]["material_stock_quantity"]; ?></td>

						<td style="word-wrap:break-word;"><?php echo $uom ; ?></td>

						<td style="word-wrap:break-word;"><?php echo $material_stock_list_data[$count]["material_stock_re_order_level"]; ?></td>

						<td style="word-wrap:break-word;"><?php echo $material_stock_list_data[$count]["user_name"]; ?></td>

						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($material_stock_list_data[$count]["material_stock_last_updated_on"])); ?></td>

						<td><?php

						if($edit_perms_list['status'] == SUCCESS)

						{

						?><a href="stock_edit_material_stock.php?material_id=<?php echo $material_stock_list_data[$count]["material_id"]; ?>">Update Re order Level</a>

						<?php

						}

						?></td>

						<td><?php

						if($edit_perms_list['status'] == SUCCESS)

						{

						?><a href="stock_add_material_stock.php?material_id=<?php echo $material_stock_list_data[$count]["material_id"]; ?>&project=<?php echo $material_stock_list_data[$count]["stock_project_id"]; ?>">Opening Stock</a>

						<?php

						}

						?></td>

						</tr>

						<?php
					}

					}
				else

				{

				?>

				<td colspan="11">No stock added yet!</td>

				<?php

				}

				 ?>



                </tbody>

              </table>

			  <script>

			  document.getElementById('span_total_qty').innerHTML = <?php echo $total_quantity; ?>;

			  </script>

            </div>

            <!-- /widget-content -->

          </div>

          <!-- /widget -->



          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 -->

      </div>

      <!-- /row -->

    </div>

    <!-- /container -->

  </div>

  <!-- /main-inner -->

</div>









<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->









<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_account(account_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else

					{

					 window.location = "stock_grn_account_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_account.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("account_id=" + account_id + "&action=0");

		}

	}

}



</script>

<script>

function get_material_list()

{

	var searchstring = document.getElementById('stxt_material').value;

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{
				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);

	}

	else

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;



	document.getElementById('search_results').style.display = 'none';

}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>



</html>
