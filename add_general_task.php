<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */
$_SESSION['module'] = 'General Task';

/*

TBD:

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Initialization

	$alert            = "";

	$alert_type       = -1; // No alert

	$assigned_to      = "";

	$gen_task_type    = "";

	$department       = "";

	$gen_task_details = "";

	$planned_end      = "";



	// Query String

	if(isset($_GET['details']))

	{

		$gen_task_details = $_GET['details'];

	}

	// Nothing here


	if(isset($_POST["add_general_task_submit"]))

	{

		// Capture all form data
		$gen_task_type    = $_POST["ddl_gen_task_type"];

		if($user != '143736679571416700')

		{

			$assigned_to  = $_POST["ddl_assigned_to"];

		}

		else

		{

			$assigned_to  = $_POST["cb_assigned_to"];

		}


		$gen_task_details = $_POST["txt_gen_task_details"];

		$department       = $_POST["ddl_department"];

		$planned_end      = $_POST["dt_planned_end_date"];
		if($_POST["ddl_project_id"] != '')
		{
			$project	      = $_POST["ddl_project_id"];
		}
		else
		{
			$project	      = 36;
		}


		if(($gen_task_type != "") && (count($assigned_to) > 0) && ($gen_task_details != "") && ($department != ""))

		{

			$task_add_status = 1;

			for($count = 0; $count < count($assigned_to); $count++)

			{

				$gen_task_add_result = i_add_task_plan_general($gen_task_type,$assigned_to[$count],$gen_task_details,$department,$project,$planned_end,$user);



				if($gen_task_add_result["status"] == SUCCESS)

				{

					$alert = "Task Successfully Added";

					$alert_type = 1; // Success

				}

				else

				{

					$alert = $alert.$gen_task_add_result["data"];

					$alert_type = 0; // Failure



					$task_add_status = $task_add_status & 0;

				}

			}



			if($task_add_status == 1)

			{

				//header("location:add_general_task.php");

			}

		}

		else

		{

			$alert = "Please fill all the mandatory fields. Mandatory fields are marked with *";

			$alert_type = 0; // Failure

		}

	}



	// Get task type list

	$gen_task_type_list = i_get_gen_task_type_list('','1');

	if($gen_task_type_list["status"] == SUCCESS)

	{

		$gen_task_type_list_data = $gen_task_type_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$gen_task_type_list["data"];

		$alert_type = 0; // Failure

	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1',"user_id"=>$user);

	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);

	if($project_management_master_list['status'] == SUCCESS)

	{

		$project_management_master_list_data = $project_management_master_list['data'];

	}



	else

	{

		$alert = $project_management_master_list["data"];

		$alert_type = 0;

	}


	// Get list of users

	$user_list = i_get_user_list('','','','','1');

	if($user_list["status"] == SUCCESS)

	{

		$user_list_data = $user_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$user_list["data"];

		$alert_type = 0; // Failure

	}



	// Get department list

	$department_list = i_get_department_list('','1');

	if($department_list["status"] == SUCCESS)

	{

		$department_list_data = $department_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$department_list["data"];

		$alert_type = 0; // Failure

	}

}

else

{

	header("location:login.php");

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Assign Task to User</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



<div class="main">



	<div class="main-inner">



	    <div class="container">



	      <div class="row">



	      	<div class="span12">



	      		<div class="widget ">



	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Your Account</h3>
						<a href="general_task_summary.php" target="_blank" style="color:red;">(Mob View)</a>&nbsp;&nbsp;&nbsp;
						<a href="general_task_summary_desktop_view.php" target="_blank" style="color:blue;" >(Desk View)</a>

	  				</div> <!-- /widget-header -->



					<div class="widget-content">







						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Assign Task to User</a>

						  </li>

						</ul>



						<br>

						    <div class="control-group">

								<div class="controls">

								<?php

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>



								<?php

								if($alert_type == 1) // Success

								{

								?>

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>

								</div> <!-- /controls -->

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="assign_task" class="form-horizontal" method="post" action="add_general_task.php">

									<fieldset>



										<div class="control-group">

											<label class="control-label" for="ddl_gen_task_type">Task Type</label>

											<div class="controls">

												<select name="ddl_gen_task_type" required>

												<?php

												for($count = 0; $count < count($gen_task_type_list_data); $count++)

												{

												?>

												<option value="<?php echo $gen_task_type_list_data[$count]["general_task_type_id"]; ?>" <?php

												if($gen_task_type_list_data[$count]["general_task_type_id"] == 3)

												{

												?>



												selected="selected"

												<?php

												}?>><?php echo $gen_task_type_list_data[$count]["general_task_type_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->																														<div class="control-group">

											<label class="control-label" for="ddl_assigned_to">Assign To</label>

											<div class="controls">

												<?php

												if($user != '143736679571416700')

												{

													?>

													<select name="ddl_assigned_to[]" multiple="multiple" required>

													<?php

													for($count = 0; $count < count($user_list_data); $count++)

													{

														?>

														<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_list_data[$count]["user_id"] == $user)

														{

														?>

														selected="selected"

														<?php

														}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>

														<?php

													}

													?>

													</select>

													<?php

												}

												else

												{

													for($count = 0; $count < count($user_list_data); $count++)

													{

														if(($user_list_data[$count]["user_id"] == '144860117291627300') || ($user_list_data[$count]["user_id"] == '143841424055011700') || ($user_list_data[$count]["user_id"] == '144134480975277200') || ($user_list_data[$count]["user_id"] == '14636341281437900') || ($user_list_data[$count]["user_id"] == '143841429031701100') || ($user_list_data[$count]["user_id"] == '144102554647936900') || ($user_list_data[$count]["user_id"] == '14411073276815800') || ($user_list_data[$count]["user_id"] == '144110755226769100') || ($user_list_data[$count]["user_id"] == '144128013799301700') || ($user_list_data[$count]["user_id"] == '144134636241439400') || ($user_list_data[$count]["user_id"] == '144134651212194200') || ($user_list_data[$count]["user_id"] == '145862543096294000') || ($user_list_data[$count]["user_id"] == '145899173064360500') || ($user_list_data[$count]["user_id"] == '143620071466608200') || ($user_list_data[$count]["user_id"] == '143736679571416700')  || ($user_list_data[$count]["user_id"] == '14384120858785500') || ($user_list_data[$count]["user_id"] == '14388461769223300')  || ($user_list_data[$count]["user_id"] == '143884634583343100') || ($user_list_data[$count]["user_id"] == '143886021344160200')  || ($user_list_data[$count]["user_id"] == '143956187469638500') || ($user_list_data[$count]["user_id"] == '14410244536444700')  || ($user_list_data[$count]["user_id"] == '144103287161642300') || ($user_list_data[$count]["user_id"] == '144103293276609500')  || ($user_list_data[$count]["user_id"] == '144111481690966800') || ($user_list_data[$count]["user_id"] == '144127982324430100'))

														{

															?>

															<input type="checkbox" name="cb_assigned_to[]" value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user == $user_list_data[$count]["user_id"]){ ?> checked <?php } ?>/>&nbsp;&nbsp;&nbsp;<?php echo $user_list_data[$count]["user_name"]; ?>&nbsp;&nbsp;&nbsp;<br />

															<?php

														}

													}

												}

												?>

												</select>

												<p class="help-block">The user(s) to whom this task has to be assigned</p>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->


										<div class="control-group">

											<label class="control-label" for="ddl_department">Department</label>

											<div class="controls">

												<select name="ddl_department" id="ddl_department"  required onchange="return makeDisable()">

												<option>- - Select Department - -</option>

												<?php

												for($count = 0; $count < count($department_list_data); $count++)

												{

												?>

												<option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>"><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->

										 <div class="control-group">

											<label class="control-label" for="ddl_project_id">Project*</label>

											<div class="controls">

												<select name="ddl_project_id" disabled id="ddl_project_id" required>

												<option>- - Select Project - -</option>

												<?php

												for($count = 0; $count < count($project_management_master_list_data); $count++)

												{

												?>

												<option value="<?php echo $project_management_master_list_data[$count]["project_management_master_id"]; ?>"><?php echo $project_management_master_list_data[$count]["project_master_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->

										<div class="control-group">

											<label class="control-label" for="txt_gen_task_details">Details</label>

											<div class="controls">

												<textarea name="txt_gen_task_details" rows="3" required><?php echo $gen_task_details; ?></textarea>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->







										<div class="control-group">

											<label class="control-label" for="dt_planned_end_date">Planned End Date</label>

											<div class="controls">

												<input type="date" required name="dt_planned_end_date" <?php if($role != "1"){ ?>  <?php } ?> />

											</div> <!-- /controls -->

										</div> <!-- /control-group -->

                                                                                                                                                               										 <br />





										<div class="form-actions">

											<input type="submit" class="btn btn-primary" name="add_general_task_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										</div> <!-- /form-actions -->

									</fieldset>

								</form>

								</div>



							</div>





						</div>











					</div> <!-- /widget-content -->



				</div> <!-- /widget -->



		    </div> <!-- /span8 -->









	      </div> <!-- /row -->



	    </div> <!-- /container -->



	</div> <!-- /main-inner -->



</div> <!-- /main -->









<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->









<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
function makeDisable()
{
	var department_id = document.getElementById("ddl_department").value	;
	var project_ddl = document.getElementById("ddl_project_id");
	if(department_id == 3)
	{
		project_ddl.disabled = false;
	}
	else
	{
		project_ddl.disabled = true;
	}
}
</script>
  </body>
</html>
