<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/* FILE HEADER - START */

// LAST UPDATED ON: 4th oct 2016

// LAST UPDATED BY: Lakshmi

/* FILE HEADER - END */



/* TBD - START */

/* TBD - END */



/* DEFINES - START */
define('QUOTATION_FUNC_ID','167');
/* DEFINES - END */



/* INCLUDES - START */

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'2','1');

	$add_perms_list    = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'1','1');

	$edit_perms_list   = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'4','1');



	/* DATA INITIALIZATION - START */

	$alert_type = -1;

	$alert = "";

	/* DATA INITIALIZATION - END */

	if(isset($_REQUEST["indent_item_id"]))

	{

		$indent_item_id = $_REQUEST["indent_item_id"];

	}

	else

	{

		$indent_item_id = "";

	}


	if(isset($_REQUEST["project"]))
	{
		$project = $_REQUEST["project"];
	}

	else

	{
		$project = "-1";
	}

	// Get Indent Item Details
	$stock_indent_search_data = array("material_id"=>$indent_item_id,"status"=>'Approved',"active"=>'1',"approved_on"=>'1',"project"=>$project);
	$indent_item_list = i_get_indent_sum_of_items_list($stock_indent_search_data);

	if($indent_item_list["status"] == SUCCESS)

	{

		$indent_item_list_data = $indent_item_list["data"];

		$indent_id   = $indent_item_list_data[0]["stock_indent_id"];

		$indent_item = $indent_item_list_data[0]["stock_material_name"];

		$item_id     = $indent_item_list_data[0]["stock_indent_item_material_id"];

		$item_qty    = $indent_item_list_data[0]["total_stock_indent_item_quantity"];

		//Get Indent List

		$stock_indent_search_data = array("indent_id"=>$indent_id);

		$indent_list = i_get_stock_indent_list($stock_indent_search_data);

		if($indent_list["status"] == SUCCESS)

		{

			$indent_list_data = $indent_list["data"];

			$indent_project   = $indent_list_data[0]["stock_project_name"];

		}

		else

		{

			$indent_project   = "";

		}

	}

	else

	{

		$indent_id   = "";

		$indent_item = "";

		$item_qty    = "";



	}



	// Capture the form data

	if(isset($_POST["add_quotation_compare_submit"]))

	{

		$indent_item_id   = $_POST["hd_indent_id"];
		$project         = $_POST["hd_project"];
		$amount           = $_POST["num_amount"];

		$quotation_no     = $_POST["num_quotation_no"];

		$quotation_vendor = $_POST["ddl_quotation_vendor"];

		$item_qty		  = $_POST["hd_item_qty"];

		$received_date    = $_POST["date_received_date"];

		$remarks 	      = $_POST["txt_remarks"];



		// Check for mandatory fields

		$is_valid = false;

		if(isset($_POST['cb_is_no_quote']))

		{

			if(($indent_item_id != "") && ($amount != "") && ($quotation_vendor != ""))

			{

				$is_valid = true;

			}

		}

		else

		{

			if(($indent_item_id != "") && ($amount != "") && ($quotation_no != "") && ($quotation_vendor != "") && ($received_date != ""))

			{

				$is_valid = true;

			}

		}



		if($is_valid == true)

		{

			if(strtotime($received_date) <= strtotime(date('Y-m-d')))

			{
				$quotation_compare_iresult = i_add_stock_quotation_compare($quote_no,$indent_item_id,$amount,$quotation_no,$quotation_vendor,$item_qty,$project,$received_date,'',$remarks,$user);			
				if($quotation_compare_iresult["status"] == SUCCESS)

				{

					$alert_type = 1;

					$alert      = 'Quotation Successfully Added';

				}

				else

				{

					$alert      = $quotation_compare_iresult["data"];

					$alert_type = 0;

				}

			}

			else

			{

				$alert_type = 0;

				$alert      = 'Quotation Received Date cannot be greater than today';

			}

		}

		else

		{

			$alert = "Please fill all the mandatory fields";

			$alert_type = 0;

		}

	}



	// Get Venor Item Mapping already added

	$stock_vendor_item_mapping_search_data = array("item_id"=>$indent_item_id);

	$vendor_master_list = i_get_stock_vendor_item_mapping($stock_vendor_item_mapping_search_data);

	if($vendor_master_list['status'] == SUCCESS)

	{

		$vendor_master_list_data = $vendor_master_list['data'];

	}

	else

	{

		$alert = $vendor_master_list["data"];

		$alert_type = 0;

	}



	// Get quotation compare already added
	$stock_quotation_compare_search_data = array("active"=>'1',"indent_id"=>$indent_item_id,"status"=>'Waiting',"project"=>$project);
	$quotation_compare_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

	if($quotation_compare_list['status'] == SUCCESS)

	{

		$quotation_compare_list_data = $quotation_compare_list['data'];

	}



	// Get Indent Item List





}

else

{

	header("location:login.php");

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Add Quotation Compare</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



<div class="main">



	<div class="main-inner">



	    <div class="container">



	      <div class="row">



	      	<div class="span12">



	      		<div class="widget ">



	      			<div class="widget-header">

	      				<i class="icon-user"></i>

	      				<h3>Your Quotation Compare</h3><span style="float:right; padding-right:20px;"><!--<a href="stock_quotation_compare_list.php?indent_item_id=<?php echo $indent_item_id ; ?>">Quotation Compare List</a>--></span>

	      				<h3>Material Name :&nbsp;<?php echo $indent_item  ;?>&nbsp;&nbsp;&nbsp;&nbsp; Qty : &nbsp;<?php echo $item_qty ;?></h3>

						<span style="padding-right:10px; float:right;"><a href="stock_indent_items_approved.php">Back to Approved Indent Items</a></span>

	  				</div> <!-- /widget-header -->



					<div class="widget-content">







						<div class="tabbable">

						<ul class="nav nav-tabs">

						  <li>

						    <a href="#formcontrols" data-toggle="tab">Add Quotation Compare</a>

						  </li>

						</ul>

						<br>

							<div class="control-group">

								<div class="controls">

								<?php

								if($alert_type == 0) // Failure

								{

								?>

									<div class="alert">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <strong><?php echo $alert; ?></strong>

                                    </div>

								<?php

								}

								?>



								<?php

								if($alert_type == 1) // Success

								{

								?>

                                    <div class="alert alert-success">

                                        <button type="button" class="close" data-dismiss="alert">&times;</button>

                                        <?php echo $alert; ?>

								<?php

								}

								?>

								</div> <!-- /controls -->

							</div> <!-- /control-group -->

							<div class="tab-content">

								<div class="tab-pane active" id="formcontrols">

								<form id="add_quotation_compare_form" class="form-horizontal" method="post" action="stock_add_quotation_compare.php">

								<input type="hidden" name="hd_indent_id" value="<?php echo $indent_item_id; ?>" />

								<input type="hidden" name="hd_item_qty" value="<?php echo $item_qty; ?>" />
								<input type="hidden" name="hd_project" value="<?php echo $project; ?>" />
									<fieldset>



										<div class="control-group">

											<label class="control-label" for="cb_is_no_quote">No Quotation</label>

											<div class="controls">

												<input type="checkbox" name="cb_is_no_quote" id="cb_is_no_quote" onclick="return get_rate(<?php echo $indent_item_id ;?>)" value="1">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="num_amount">Amount*</label>

											<div class="controls">

												<input type="number" class="span6" name="num_amount" min="0.01" step="0.01" id="num_amount" placeholder="Amount" required="required">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="num_quotation_no">Quotation No</label>

											<div class="controls">

												<input type="text" class="span6" name="num_quotation_no" placeholder="Enter Quotation No">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="ddl_quotation_vendor">Quotation Vendor*</label>

											<div class="controls">

												<select name="ddl_quotation_vendor" required="required">

												<option value=''>- - -Select Vendor Name- - -</option>

												<?php

												for($count = 0; $count < count($vendor_master_list_data); $count++)

												{

													?>

												<option value="<?php echo $vendor_master_list_data[$count]["stock_vendor_id"]; ?>"><?php echo $vendor_master_list_data[$count]["stock_vendor_name"]; ?></option>

												<?php

												}

												?>

												</select>

											</div> <!-- /controls -->

										</div> <!-- /control-group -->



										<div class="control-group">

											<label class="control-label" for="date_received_date">Received Date</label>

											<div class="controls">

												<input type="date" class="span6" name="date_received_date" placeholder="Received Date">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->





										<div class="control-group">

											<label class="control-label" for="txt_remarks">Remarks</label>

											<div class="controls">

												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">

											</div> <!-- /controls -->

										</div> <!-- /control-group -->

                                                                                                                                                               										 <br />



										<?php

										if($add_perms_list['status'] == SUCCESS)

										{

										?>

										<div class="form-actions">

											<input type="submit" class="btn btn-primary" name="add_quotation_compare_submit" value="Submit" />

											<button type="reset" class="btn">Cancel</button>

										</div> <!-- /form-actions -->

										<?php

										}

										else

										{

											?>

											<div class="form-actions">

											<?php echo 'You are not authorized to add a quotation'; ?>

											</div> <!-- /form-actions -->

											<?php

										}

										?>

									</fieldset>

								</form>

								</div>

							</div>



							<div class="widget-content">



              <table class="table table-bordered">

                <thead>

                  <tr>

				    <th>SL No</th>

					<th>Item</th>

					<th>Item Code</th>

					<th>Quantity</th>

					<th>Amount</th>

					<th>Quotation No</th>

					<th>Vendor</th>

				    <th>Received Date</th>

					<th>Remarks</th>

					<th>Quotation Added  By</th>

					<th>Quotation Added  On</th>

					<th>Status</th>

					<th colspan="2" style="text-align:center;">Action</th>

				</tr>

				</thead>

				<tbody>

				<?php

				if($quotation_compare_list["status"] == SUCCESS)

				{

					/*if($purcahse_order_list['status'] == SUCCESS)

					{*/

						$sl_no = 0;

						for($count = 0; $count < count($quotation_compare_list_data); $count++)

						{

							$sl_no++;

							?>

								<tr>

								<td><?php echo $sl_no; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_material_name"]; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_material_code"]; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_quantity"]; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_amount"]; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_no"]; ?></td>

								<td style="word-wrap:break-word;"><?php echo $quotation_compare_list_data[$count]["stock_vendor_name"]; ?></td>

								<td style="word-wrap:break-word;"><?php echo get_formatted_date($quotation_compare_list_data[$count]

								["stock_quotation_received_date"],'d-M-Y'); ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_remarks"]; ?></td>

								<td style="word-wrap:break-word;"><?php echo $quotation_compare_list_data[$count]["user_name"]; ?></td>

								<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($quotation_compare_list_data[$count]["stock_quotation_added_on"])); ?></td>

								<td style="word-wrap:break-word;"><?php echo $quotation_compare_list_data[$count]["stock_quotation_status"]; ?></td>

								<td style="word-wrap:break-word;"><?php if(($quotation_compare_list_data[$count]["stock_quotation_active"] == "1") || ($edit_perms_list['status'] == SUCCESS)){?><a style="padding-right:10px" href="#" onclick="return go_to_edit_quotation_compare('<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>',<?php echo $quotation_compare_list_data[$count]["stock_quotation_indent_id"]; ?>);">Edit </a><?php } ?></td>

								<!-- <td><?php if(($quotation_compare_list_data[$count]["stock_quotation_active"] == "1")){?><a href="#" onclick="return delete_quotation_compare('<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>','<?php echo $indent_item_id; ?>');">Delete</a><?php } ?></td>	-->

								<td><?php if(($quotation_compare_list_data[$count]["stock_quotation_status"] ==

								"Waiting")  && ($edit_perms_list['status'] == SUCCESS)){?><a href="#" onclick="return approve_quotation(<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>);">Approve</a><?php } ?></td>

								</tr>

							<?php

						//}

					}

				}

				else

				{

				?>

				<td colspan="14">No quotation compare added yet!</td>

				<?php

				}

				 ?>



                </tbody>

              </table>

            </div>



					</div> <!-- /widget-content -->



				</div> <!-- /widget -->



		    </div> <!-- /span8 -->









	      </div> <!-- /row -->



	    </div> <!-- /container -->



	</div> <!-- /main-inner -->



</div> <!-- /main -->









<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->









<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function approve_quotation(quotation_id)

{

	var ok = confirm("Are you sure you want to Approve?")

	{

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else

					{

					 window.location = "stock_indent_items_approved.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_approve_quotation.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("quotation_id=" + quotation_id + "&action=Approved");

		}

	}

}

function go_to_edit_quotation_compare(quotation_id,item_id)

{

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_edit_quotation_compare.php");



	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","quotation_id");

	hiddenField1.setAttribute("value",quotation_id);



	var hiddenField2 = document.createElement("input");

	hiddenField2.setAttribute("type","hidden");

	hiddenField2.setAttribute("name","item_id");

	hiddenField2.setAttribute("value",item_id);



	form.appendChild(hiddenField1);

	form.appendChild(hiddenField2);



	document.body.appendChild(form);

    form.submit();

}

function get_rate(material_id)

{

	if (window.XMLHttpRequest)

	{// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();

	}

	else

	{// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

	}



	xmlhttp.onreadystatechange = function()

	{

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

		{

			if(xmlhttp.responseText == "-1")

			{

			 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

			}

			else

			{

				document.getElementById('num_amount').value = xmlhttp.responseText;

			}

		}

	}



	xmlhttp.open("POST", "ajax/stock_get_price.php");   // file name where delete code is written

	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	xmlhttp.send("material_id=" + material_id);

}

</script>



  </body>



</html>
