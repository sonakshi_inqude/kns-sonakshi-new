<?php
session_start();
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	if(isset($_REQUEST['payment_contract_id']))
	{
		$contract_payment_id = $_REQUEST['payment_contract_id'];
	}
	else
	{
		$contract_payment_id = '';
	}


	// Capture the form data
	if(isset($_POST["add_bill_no_submit"]))
	{
		$contract_payment_id  = $_POST["hd_contract_payment_id"];
		$billing_address      = $_POST["ddl_company"];
		$security_deposit     = $_POST["num_sec_deposit"];
		$remarks     		  = $_POST["stxt_remarks"];
		$approved_by 		  = $user;
		$approved_on 		  = date("Y-m-d H:i:s");
		$bill_no = p_generate_contract_bill_no($_POST['bill_substr']);
		$project_actual_contract_payment_update_data = array("billing_address"=>$billing_address,"sec_dep_amount"=>$security_deposit,"approved_by"=>$user,"approved_on"=>$approved_on,"remarks"=>$remarks,"bill_no"=>$bill_no);
		$approve_payment_manpower_result = i_update_project_actual_contract_payment($contract_payment_id,$project_actual_contract_payment_update_data);
		if($approve_payment_manpower_result["status"] == SUCCESS)
		{
			$project_actual_contract_payment_search_data = array("contract_payment_id"=>$contract_payment_id,"start"=>'-1');
			$payment_conatrct_list = i_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
			if($payment_conatrct_list["status"] == SUCCESS )
			{
				$payment_conatrct_list_data = $payment_conatrct_list["data"];
				$contract_bill_no = $payment_conatrct_list_data[0]["project_actual_contract_payment_bill_no"];
			}
			echo $contract_bill_no; exit;
		}
	}

	// Get details of the payables
	$project_actual_contract_payment_search_data = array('contract_payment_id'=>$contract_payment_id);
	$contract_payment_sresult = i_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
	if($contract_payment_sresult['status'] == SUCCESS)
	{
		$payable = $contract_payment_sresult['data'][0]['project_actual_contract_payment_amount'];
	}
	else
	{
		$payable = 0;
	}

	//Get Bill No


	//Get Company List
	$stock_company_master_search_data = array();
	$company_list = i_get_company_list($stock_company_master_search_data);
	if($company_list["status"] == SUCCESS)
	{
		$company_list_data = $company_list["data"];
	}
}
else {
	header("location:login.php");
}
?>


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Add Bill No</h4>
</div>
<div class="modal-body">
	<form id="add-bill-form">
		  <input type="hidden" value="<?php echo $contract_bill_no; ?>" />
		  <input type="hidden" id="payable_limit"  value="<?php echo $payable; ?>" />

			<div class="form-group">
				<input type="hidden" id="hd_contract_payment_id" class="form-control" disabled value="<?php echo $contract_payment_id ;?>" placeholder="Enter Bill No">
			</div>

			<div class="form-group">
				<label for="BillAddress" class="control-label">Bill Address</label>
				<select name="ddl_company" required id="ddl_company" class="form-control">
				<option value="">- - -Select Billing Address- - -</option>
				<?php foreach($company_list['data'] as $item){  ?>
				<option value="<?php echo $item['stock_company_master_id']; ?>">
					<?php echo $item['stock_company_master_name']; ?>
				</option>
				<?php } ?>
				</select>
			</div>

			<div class="form-group">
				<label class="control-label">Security Deposit</label>
				<!-- <input type="number" class="form-control"  id="num_sec_deposit" name="num_sec_deposit" value="<?php echo (5/100*$payable); ?>" onblur="check_deposit_validity('<?php echo $payable; ?>');"> -->
				<input type="number" class="form-control"  id="num_sec_deposit" name="num_sec_deposit" value="<?php echo (5/100*$payable); ?>">
			</div>

			<div class="form-group">
				<label for="remarks" class="control-label">Remarks</label>
				<input type="text" class="form-control" id="stxt_remarks" name="stxt_remarks" class="form-control">
			</div>

		</form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	<button type="button" class="btn btn-primary" onclick="submitBilling()">Submit</button>
</div>
