<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: kt_client_process_master_list.php
CREATED ON	: 10-Jan-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
define('KT_CLIENT_PROCESS_MASTER_LIST_FUNC_ID','226');
/* DEFINES - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'katha_transfer'.DIRECTORY_SEPARATOR.'kt_client_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',KT_CLIENT_PROCESS_MASTER_LIST_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',KT_CLIENT_PROCESS_MASTER_LIST_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',KT_CLIENT_PROCESS_MASTER_LIST_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',KT_CLIENT_PROCESS_MASTER_LIST_FUNC_ID,'1','1');

	// Query String Data
	// Nothing

	// Get kt Client already added
	$kt_client_process_master_search_data = array("active"=>'1');
	$kt_client_process_master_list = i_get_kt_client_process_master($kt_client_process_master_search_data);
	if($kt_client_process_master_list['status'] == SUCCESS)
	{
		$kt_client_process_master_list_data = $kt_client_process_master_list['data'];
	}
	else
	{
		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>KT Client Master Process List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>KT Client Master Process List</h3><?php if($add_perms_list["status"] == SUCCESS) {?><span style="float:right; padding-right:20px;"><a href="kt_client_master_add_process.php">KT Client Master Add Process</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php 
			if($view_perms_list["status"] == SUCCESS)
			{
				?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Name</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2" style="text-align:center;">Actions</th>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($kt_client_process_master_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($kt_client_process_master_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $kt_client_process_master_list_data[$count]["kt_client_process_master_name"]; ?></td>
					<td><?php echo $kt_client_process_master_list_data[$count]["kt_client_process_master_remarks"]; ?></td>
					<td><?php echo $kt_client_process_master_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($kt_client_process_master_list_data[$count][
					"kt_client_process_master_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list["status"] == SUCCESS) {?><a style="padding-right:10px" href="#" onclick="return go_to_kt_client_edit_process_master('<?php echo $kt_client_process_master_list_data[$count]["kt_client_process_master_id"]; ?>');">Edit </a><?php } ?></td>
					<td><?php if(($kt_client_process_master_list_data[$count]["kt_client_process_master_active"] == "1") && ($delete_perms_list["status"] == SUCCESS)){ ?><a href="#" onclick="return kt_client_delete_process_master(<?php echo $kt_client_process_master_list_data[$count]["kt_client_process_master_id"]; ?>);">Delete</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Process Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   <?php
			}
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function kt_client_delete_process_master(process_master_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "kt_client_master_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/kt_client_master_delete_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_master_id=" + process_master_id + "&action=0");
		}
	}	
}
function go_to_kt_client_edit_process_master(process_master_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "kt_client_master_edit_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_master_id");
	hiddenField1.setAttribute("value",process_master_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>